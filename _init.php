<?php

REQUIRE_ONCE(SCRIPTPATH . 'lib/commons.php'); //d�finitions des fonctions de base
if(INTYPO3) REQUIRE_ONCE(SCRIPTPATH . 'lib/t3proxy.php');
REQUIRE_ONCE(SCRIPTPATH . 'config/mapping.php'); //classe d�finissant le mapping des controllers
REQUIRE_ONCE(SCRIPTPATH . 'config/constants.php'); //r�cup�re les constantes de l'application
REQUIRE_ONCE(SCRIPTPATH . 'config/database.php'); //r�cup�re les constantes de l'application

REQUIRE_ONCE(SCRIPTPATH . 'domain/session.domain.class.php');
REQUIRE_ONCE(SCRIPTPATH . 'lib/autoload.php');
REQUIRE_ONCE(SCRIPTPATH . 'lib/env.class.php');

REQUIRE_ONCE(SCRIPTPATH . 'lib/eximporter.class.php');

if(I18NENABLE) REQUIRE_ONCE(SCRIPTPATH . 'lib/i18n.php');

$session = new Session();

ENV::initialize(DEBUGMODE);

function debugVar($label, $value)
{
	var_dump($label . ' : <pre>' . var_export($value, true) . '</pre>');
}
