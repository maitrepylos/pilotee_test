<?php

	define('FROM_CLI', !isset($_SERVER['HTTP_HOST']));
	
	define('I18NENABLE', false);
	define('I18NDEBUG', true);
	define('I18NUSECOMMON', true);
	
	if(!defined('DEBUGMODE')) define('DEBUGMODE', true);
	if(!defined('DB_ECHO')) define('DB_ECHO', DEBUGMODE && false);
	
	define('UPLOAD_DIR', 'files/');
	define('UPLOAD_MAX_SIZE_READABLE', 3.5); //en mo
	define('UPLOAD_MAX_SIZE', UPLOAD_MAX_SIZE_READABLE * 1048576);
	
	ini_set('error_reporting',E_ALL); // & ~E_NOTICE);
	ini_set('display_errors',E_ALL);
	ini_set("memory_limit","500M");
	$extensionOK = array("xls");
	define("EXTENSION", serialize($extensionOK));
	
	define("LEVENSHTEIN_RATIO", 90);
	
	if(!FROM_CLI)
	{
		define('MODULE', isset($_GET['module']) ? $_GET['module'] : null);
		define('STYLEPATH', 'styles/');
		if(!INTYPO3)
		{
			DEFINE('LANGUAGE', 'fr');
		}
		else
		{
			die('<b><u>config/CONSTANTS.PHP :</u></b> Faire le mapping lang_id => langue');			
			$langMap = array(
				1 => 'fr',
				0 => 'en'
			);			
			DEFINE('TYPO3PAGE', isset($_GET['id']) ? $_GET['id'] + 0 : 1);
			DEFINE('TYPO3LANG', isset($_GET['L']) ? $_GET['L'] + 0 : 1);
			DEFINE('LANGUAGE', $langMap[TYPO3LANG]);
		}
		
		define('WEBSITE_ADDRESS', 'http://'.$_SERVER['SERVER_NAME'].($_SERVER['SERVER_PORT'] != 80 ? ':'.$_SERVER['SERVER_PORT'] : '').substr($_SERVER['REQUEST_URI'],0,strrpos($_SERVER['REQUEST_URI'],'/')+1));
		define('SITEPATH', (INTYPO3 ? 'index.php' : 'index.php'));
	}
	
	/*Origine constants*/
		define('AGENT_ORIGINE', "AGT");
		define('OPERATEUR_ORIGINE', "OP");
		define('ADMIN_ORIGINE', 1);
	
	
	/* USER related constants */
		define('USER_STATE_BLOQUE', 0);
		define('USER_STATE_ACTIF', 1);
		
		define('USER_TYPE_AGENT', 'AGE');
		define('USER_TYPE_OPERATEUR', 'OPE');
		define('USER_TYPE_COORDINATEUR', 'COO');
		define('USER_TYPE_AGENT_COORDINATEUR', 'AGC');
		define('USER_TYPE_ADMIN', 'ADM');
		define('USER_TYPE_FORMATEUR', 'FOR');
	
	/* EXEMPLE constants */
		define('DATE_EXEMPLE', '_ _ / _ _ / _ _ _ _');
		define('BCE_EXEMPLE', '_ _ _ _ _ _ _ _ _ _');
		define('TVA_EXEMPLE', 'BE_ _ _ - _ _ _ - _ _ _');
		define('MOIS_ANNEE_EXEMPLE', 'MM/AAAA');
		define('SITEWEB_EXEMPLE','http://');
		
	/* FORMAT constants */		
		define('REGEX_TEL','^[0-9\+\(\)/\. ]{0,}$');
		define('REGEX_DATE','^(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[0-2])/[12][0-9]{3}$');
		define('REGEX_MOIS_ANNEE','^(0?[1-9]|1[0-2])/[12][0-9]{3}$');
		define('REGEX_MAIL', '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');
		define('REGEX_URL', '^(http://|https://)[a-zA-Z0-9_.-]{3,}\.[a-zA-Z]{2,}[/]{0,1}$');
		define('REGEX_BCE', '^[0-9]{10}$');
		define('REGEX_TVA', '^BE[ ]{0,1}[0-9]{3}-[0-9]{3}-[0-9]{3}$');
		
		
	/* Action */
		define('ACTION_LABELLISEES', 'LABEL');
		define('ACTION_NON_LABELLISEES', 'NONLA');
		define('ACTION_MICRO', 'AMICR');
		define('BOURSES_PI', 'BOUPI');
		
		// FFI : 
		define('FORMATIONS', 'FFOR');
		define('STAC', 'FSTAC');
		define('ATELIERS', 'FATE');
		
		define('ACTION_VALID', 'VALID');
	
	/*Statut subvention*/
		define('SUBVENTION_OBTENU','OBTEN');
		define('SUBVENTION_EVALUATION','EVALU');
		define('SUBVENTION_REJET','REJET');
		
	// Google maps
		define('GOOGLE_MAP_KEY', 'ABQIAAAA58Mr0Fj0DJ4OdfiJzY1sYRTGSOpjg8Ol7OJKkU36k49AQXB01hRLhNt92erZognxcz5k7TShWfF8sg');
		//Nombre maximum d'établissements affichées dans la map google
		define('GOOGLE_NB_MAX_MAP', 1000);
?>
