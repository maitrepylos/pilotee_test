<?php
class Mapping
{
	static public $layout = 'main';

	static public function getModuleData($module)
	{
		$array = array();
		//$module = 'accueil';
		if(!isset(Mapping::$map['modules'][$module])) $module = Mapping::$map['default'];
		Mapping::Clean($module);
		$root = Mapping::$map['root'];
		$data = Mapping::$map['modules'][$module];
		$data = explode('|', $data);
		$array['layout'] = (isset($data[1]) ? $data[1] : 'normal');
		$array['path'] = $root.$data[0];

		return $array;
	}

	static $map = array
	(
		'root' => 'controller/',
		'default' => 'accueil',
		'modules' => array
		(
			'accueil' => 'home.php|main',
			'action' => 'action.php|main',
			'actionDetail' => 'actionDetail.php|main',
			'actionPopup' => 'action.php|popup',
			'agentDetail' => 'agentDetail.php|main',
			'ajax' => 'ajax.php|txt',
			'bourse' => 'bourse.php|main',
			'bourseDetail' => 'bourseDetail.php|main',
			'boursePopup' => 'bourse.php|popup',
			'commenterBourse' => 'commenterBourse.php|main',
			'contact' => 'contact.php|main',
			'contactDetail' => 'contactDetail.php|main',
			'contactDetailPopup' => 'contactDetail.php|popup',
			'contactListPopup' => 'contact.php|popup',
			'contactPopup' => 'contact.php|popup',
			'entiteAdmin' => 'entiteAdmin.php|main',
			'entiteAdminDetail' => 'entiteAdminDetail.php|main',
			'entiteAdminPopup' => 'entiteAdmin.php|popup',
			'etablissement' => 'etablissement.php|main',
			'etablissementDetail' => 'etablissementDetail.php|main',
			'etablissementPopup' => 'etablissement.php|popup',
			'exportXLS' => 'export_xls.php|main',
			'ficheSuiviDetail' => 'ficheSuiviDetail.php|main',
			'ficheSuiviEdit' => 'ficheSuiviEdit.php|main',
			'ficheSuiviEditPopup' => 'ficheSuiviEdit.php|popup',
			'forgot' => 'forgot.php|login_layout',
			'formations' => 'formations.php|main',
			'formationDetail' => 'formationDetail.php|main',
			'formationPopup' => 'formations.php|popup',
			'formateurPopup' => 'formateur.php|popup',
			'formationParticipants' => 'formationParticipants.php|popup',
			'formationParticipantsNone' => 'formationParticipants.php|none',
			'formationParticipantsTxt' => 'formationParticipants.php|txt',
			'google_maps_admin' => 'google_maps_admin.php|main',
			'historiquePopup' => 'historiqueDetail.php|popup',
			'import' => 'import.php|main',
			'incoherence' => 'incoherence.php|main',
			'login' => 'login.php|login_layout',
			'materielPedagDetail' => 'materielPedagDetail.php|main',
			'materielPedagEdit' => 'materielPedagEdit.php|main',
			'materielPedagEditPopup' => 'materielPedagEdit.php|popup',
			'migration' => 'migration.php|main',
			'operateurDetail' => 'operateurDetail.php|main',
			'operateurPopup' => 'operateur.php|popup',
			'formateurPopup' => 'formateur.php|popup',		
			'popup_google_maps' => 'popup_google_maps.php|popup',
			'popup_select_agent' => 'popup_select_agent.php|popup',
			'report' => 'reporting.php|main',
			/* Marucci 23/12/2013 Ajout du controller administration.php */
			'administration' => 'administration.php|main',
			'administration_ajax' => 'administration.php|ajax_callback',
			/* Marucci 27/12/2013 Ajout du controller session popup */
			'session_timelife' => 'session_timelife.php|ajax_callback',
            'session_restart' => 'session_restart.php|ajax_callback'
		)
	);

	static function Execute($module = null)
	{
		$data = Mapping::getModuleData($module);
		Mapping::$layout = $data['layout'];
		return SCRIPTPATH.$data['path'];
	}

	static function Clean(&$module)
	{
		if(empty($module) || !isset(Mapping::$map['modules'][$module]))
			$module = Mapping::$map['default'];
	}

	static function RedirectTo($module = null, $supplement = '', $code = 302)
	{
		$data = Mapping::getModuleData($module);
		header('Location: '.WEBSITE_ADDRESS.SITEPATH.'?module='.$module.$supplement, true, $code);
		exit;
	}

	static function GoBack()
	{
		$ref = $_SERVER['HTTP_REFERER'];
		header('Location: '.$ref, true, $code);
		exit;
	}
}
