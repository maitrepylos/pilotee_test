<?php
if(!$session->isLogged()) $session->redirect();
REQUIRE_ONCE(SCRIPTPATH.'model/ase_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/action_view_class.php');


$view = new ActionView();
if ($_REQUEST['module'] == 'action')
{
	$title = "Actions";
	$shortcut = $view->raccourcisPanel($_REQUEST['module']);
	$menu = $view->createMenu();
	$login = $view->createLogin();
}
$ec = new ActionController($view);
$ec->Render();


class ActionController
{
	private $view;
	private $user;
	private $notices;
	private $errors;
	private $fieldserror;
	private $session;
	private $actionId;

	public function __construct($view)
	{
		$this->view = $view;
		$this->session = Session::GetInstance();
		$this->user = $this->session->getCurrentUser();
		$this->errors = array();
		
		if($this->session->exist('erreurs'))
		{
			$this->errors[] = $this->session->get('erreurs');
			$this->session->reset('erreurs'); 
		}
		
		$this->notices = array();
		if($this->session->exist('notices'))
		{
			$this->notices[] = $this->session->get('notices');
			$this->session->reset('notices'); 
		}
		$this->fieldserror = array();

	}

	public function Render()
	{
		if(!isset($_REQUEST['action']))
		{
			if (isset($_POST['searchActions']))//Recherche
			{
				$this->view->Notices($this->notices);
				$this->view->Errors($this->errors);
				$this->view->Search($this->user->isOperateur(), $this->SetCloseCallBack());
				$this->Search();
			}
			else
				$this->viewSearchForm();
		}
		elseif($_REQUEST['action'] == 'delete')
		{
			if(isset($_REQUEST['deleteId']))
				$this->delete();
		}
		elseif($_REQUEST['action'] == 'update')
			$this->RenderUpdate();
		elseif($_REQUEST['action'] == 'add')
			$this->RenderAdd();
	}

	function RenderUpdate()
	{
		if(isset($_REQUEST['updateId']))
		{
			$action = $this->GetAction($_REQUEST['updateId']);
			$this->setPostAction($action);
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->AddAction(true);
		}
		elseif(isset($_POST['updateForm']))
		{
			$idAction = $_POST['action_id'];
			$this->insert_update($idAction);
			Mapping::RedirectTo('actionDetail&id='.$idAction.'&error=0');
		}
	}
	
	function viewSearchForm()
	{
		$this->Initialize();
		if(isset($_REQUEST['delete']))
			$this->notices[] = 'L\'action a �t� supprim�e avec succ�s';
			
		$this->view->Notices($this->notices);
		$this->view->Search($this->user->isOperateur(), $this->SetCloseCallBack());

	}

	private function delete()
	{
		try
		{
			$action = $this->GetAction($_REQUEST['deleteId']);
			$DB = new ActionDatabase();
			$DB->open();
			$DB->startTransaction();

			$DB->deleteActionOperateur($action->GetActionId());
			$DB->deleteActionPeda($action->GetActionId());
			$DB->deleteActionSection($action->GetActionId());
			$DB->deleteActionFiliereDegreNiveau($action->GetActionId());
			$DB->deleteActionContact($action->GetActionId());
			$DB->deleteActionEtablissement($action->GetActionId());
			$DB->deleteAction($action->GetActionId());

			$DB->commitTransaction();

		}
		catch(Exception $e)
		{
			$DB->rollbackTransaction();
			$DB->close();
		}
		$DB->close();
		Mapping::RedirectTo('action&delete=1');
	}

	private function GetAction($id)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
		$db = new ActionDatabase();
		$db->open();
		$rs = $db->GetById($id);
		$db->close();
		$ets = new Actions($rs);
		return $ets->items(0);
	}

	public function RenderAdd()
	{
		if (isset($_POST['addForm']) )//Si le formulaire action est post�
		{
			$this->insert_update();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			//$this->actionId
			$this->view->AddAction(false, $this->actionId);
		}
		else
		{
			$this->InitializeAddForm();
			$this->view->AddAction();
		}
		
	}

	private function insert_update($idAction = null)
	{
		$this->actionId = $idAction; $statutSubventionId = null;
		$refOp = null; $refAse = null; 
		$categ = null; $operateurOrg = null; $dateAction = null;
		$nomGen = null; $nomSpec = null; $commentaire = null;
		$nbEnseignants = null; $nbApprenants = null; $web = null; $swPeda = 0;
		$montantDemande = null; $montantSubvention=null; $categorieBourse = null;
		$projetRealise = null; $groupeClasseConcernee = null; $collaborationInterEtab = null;
		
		$statutActionId = 'ENCOU';
		$anneeScolaireId = $_POST['anneesScolaire'];
		$typeActionId = $_POST['actionType'];
		

		if(!empty($_POST['refASE']))
			$refAse = $_POST['refASE'];

		if($_REQUEST['statut'] == 'valider')
			$statutActionId = 'VALID';

		if(!empty($_POST['date']))
			$dateAction = $_POST['date'];

		if(!empty($_POST['nomSpecifique']))
			$nomSpec = $_POST['nomSpecifique'];

		if(!empty($_POST['commentaire']))
			$commentaire = $_POST['commentaire'];

		$web  = !empty($_POST['web']) ?$_POST['web'] : null;

		$nbEnseignants = $_POST['nbGlobalEns'];
		$nbApprenants = $_POST['nbGlobalApp'];

		if($typeActionId == 'LABEL')
		{
			$actionTypeLab = $_POST['actionTypeLab'];
			$operateurOrg = $_POST['organisateur'];
			if($actionTypeLab!='-1')
			$nomGen = Dictionnaries::GetActionLabelList($actionTypeLab)->items(0)->getLabel();
			/*
			if(!empty($_POST['refOp']))
				$refOp = $_POST['refOp'];
			*/			
		}
		elseif($typeActionId == 'NONLA')
		{
			$categ = $_POST['categorie'];
			$nomGen = !empty($_POST['nomGeneral']) ?$_POST['nomGeneral'] : null;
		}
		elseif($typeActionId == 'AMICR')
			$nomGen = !empty($_POST['nomGeneral']) ?$_POST['nomGeneral'] : null;

		$global = isset($_POST['swGlobal']) ?$_POST['swGlobal'] : 0;
		$nbEtabs = !empty($_POST['nbLignes']) ?$_POST['nbLignes'] : null;
		$nbContacts = !empty($_POST['nbLignesContact']) ?$_POST['nbLignesContact'] : null;
		$contactAction = array();
		
		for($i=0; $i<$nbContacts; $i++)
		{
			if(isset($_POST['roleActionContact_'.$i]))
				$contactAction[] = array("id"=>$_POST['contactId_'.$i],"role" => $_POST['roleActionContact_'.$i]);
		}

		$fdn = Dictionnaries::getFiliereDegreNiveauList();
		$niveauDegreFiliere = array();
		for($i=0; $i<$fdn->Count(); $i++)
		{
			if(!empty($_POST[$fdn->items($i)->GetFiliereDegreNiveauId()]))
				$niveauDegreFiliere[] = array("id"=>$fdn->items($i)->GetFiliereDegreNiveauId(), "nb"=>$_POST[$fdn->items($i)->GetFiliereDegreNiveauId()]);
		}

		$section = Dictionnaries::getSectionActionList();
		$sections = array();
		for($i=0; $i<$section->Count(); $i++)
		{
			if(!empty($_POST[$section->items($i)->getSectionActionId()]))
				$sections[] = array("id"=>$section->items($i)->getSectionActionId(), "nb"=>$_POST[$section->items($i)->getSectionActionId()]);
		}

		$pedas = array();
		if(isset($_POST['swPeda'])&& $_POST['swPeda'] == '1')
		{
			$swPeda = $_POST['swPeda'];
			$peda = Dictionnaries::getFinalitePedagogList();
			for($i=0; $i<$peda->Count(); $i++)
			{
				if(!empty($_POST[$peda->items($i)->GetFinalitePedagogId()]))
					$pedas[] = array("id"=>$peda->items($i)->GetFinalitePedagogId(), "nb"=>$_POST[$peda->items($i)->GetFinalitePedagogId()]);
			}
		}
		//INSERT
		$dbAction = new ActionDatabase();
		try
		{
			$dbAction->open();
			$dbAction->startTransaction();

			$action = new Action();
			$action->init($this->actionId, $statutSubventionId, $_POST['actionTypeLab'], $refOp, $refAse,
							$_POST['actionType'], $_POST['anneesScolaire'], $statutActionId, $categ, $operateurOrg,
							$dateAction, $nomGen, $nomSpec, $commentaire, $global, $swPeda, $nbApprenants, $nbEnseignants,
							$web, $montantDemande, $montantSubvention, $categorieBourse, $projetRealise, $groupeClasseConcernee, $collaborationInterEtab,
							date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
			
			if(!isset($_POST['updateForm']))
				$this->actionId = $dbAction->insertAction($action);
			else
			{
				$dbAction->deleteActionOperateur($action->GetActionId());
				$dbAction->deleteActionPeda($action->GetActionId());
				$dbAction->deleteActionSection($action->GetActionId());
				$dbAction->deleteActionFiliereDegreNiveau($action->GetActionId());
				$dbAction->deleteActionContact($action->GetActionId());
				$dbAction->deleteActionEtablissement($action->GetActionId());
				$this->actionId = $dbAction->updateAction($action);
			}
			
			//ActionEtablissement
			if($_POST['nbLignes']!='0' && !empty($_POST['etabIds']))
			{
				$etabIds = explode(";", $_POST['etabIds']);
				foreach($etabIds as $etabId)
				{
					$actionEtab = new ActionEtablissement();
					$actionEtab->init($this->actionId, $etabId, date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
					$dbAction->insertActionEtablissement($actionEtab);
				}
			}
			
			//ActionContact
			for($i=0; $i< count($contactAction); $i++)
			{
				$actionC = new ActionContact();
				$actionC->init($this->actionId,$contactAction[$i]['id'], $contactAction[$i]['role'], date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
				$dbAction->insertActionContact($actionC);
			}
			
			//R?partition par niveau-degr�-fili�re
			for($i=0; $i< count($niveauDegreFiliere); $i++)
			{
				$afdn = new ActionFiliereDegreNiveau();
				$afdn->init($this->actionId, $niveauDegreFiliere[$i]['id'], $niveauDegreFiliere[$i]['nb']);
				$dbAction->insertActionFiliereDegreNiveau($afdn);
			}

			for($i=0; $i< count($sections); $i++)
			{
				$actionSection = new ActionSectionAction();
				$actionSection->init($this->actionId, $sections[$i]['id'], $sections[$i]['nb']);
				$dbAction->insertActionSection($actionSection);
			}

			if($swPeda!=null)
			{
				for($i=0; $i< count($pedas); $i++)
				{
					$actionPeda = new ActionFinalitePedagog();
					$actionPeda->init($this->actionId, $pedas[$i]['id'], $pedas[$i]['nb']);
					$dbAction->insertActionPeda($actionPeda);
				}
			}

			//AssocOperateur
			if($typeActionId == 'LABEL')
			{
				if(!empty($_POST["selectedOps"]))
				{
					$ops = explode(";", $_POST["selectedOps"]);
					foreach($ops as $op)
					{
						$actionOp = new ActionOperateur();
						$actionOp->init($this->actionId, $op);
						$dbAction->insertActionOperateur($actionOp);
					}
				}
			}

			$dbAction->commitTransaction();
			$dbAction->close();
		}
		catch(Exception $e)
		{
			//echo $e->getMessage();
			$dbAction->rollbackTransaction();
			
			$dbAction->close();
			
			if(!isset($_POST['updateForm']))
				$this->errors[] = 'Une erreur s\'est produite lors de l\'ajout de l\'action !';
			else
				Mapping::RedirectTo('actionDetail&id='.$idAction.'&error=1');
		}
	}

	private function Search()
	{
		$actionLab  = false;
		$actionNonLab  = false;
		$actionMicro  = false;
		$nom = $_POST['nom'];

		if(!$this->user->isOperateur())
		{
			if(isset($_POST['actionNonLab']))
				$actionNonLab = true;

			if(isset($_POST['actionLab']))
				$actionLab = true;

			if(isset($_POST['actionMicro']))
				$actionMicro = true;
		}
		else //Op�rateur voit uniquement les actions de type d'actions labellis�es� qui ont �t� organis�es par lui-m�me.
			$actionLab = true;
			
		$actionValide = null;
		if(isset($_POST['actionValide']))
			$actionValide = $_POST['actionValide'];

		$actionEnCours = null;
		if(isset($_POST['actionEnCours']))
			$actionEnCours = $_POST['actionEnCours'];

		$refAse = $_POST['refAse'];
		//$refOp = $_POST['refOp'];
		$refOp = '';
		$niveau = $_POST['niveauAction'];
		$degreNiveau = $_POST['niveauDegreAction'];
		$degreNiveauFiliere = $_POST['niveauDegreFilereAction'];
		$section = $_POST['actionSection'];

		$pedaAvec = null;
		if(isset($_POST['pedaAvec']))
			$pedaAvec = $_POST['pedaAvec'];

		$pedaSans = null;
		if(isset($_POST['pedaSans']))
			$pedaSans = $_POST['pedaSans'];

		$anneeScolaire = $_POST['anneesScolaire'];
		$anneeAnte = $_POST['annee_ante'];
		$anneePost = $_POST['annee_post'];
		$etabIds =  $_POST['selectedEts'];
		$operateurId = null;
		if($actionLab)
			$operateurId = $_POST['operateur'];

		$global = null;
		if(isset($_POST['actionGlob']))
			$global = $_POST['actionGlob'];


		$DB_ACTION = new ActionDatabase();
		$actions = new Actions($DB_ACTION->get($nom, $actionLab, $actionNonLab, $actionMicro, $anneeScolaire, $anneeAnte, $anneePost, $etabIds, $operateurId, $actionValide, $actionEnCours, $refAse, $refOp, $niveau, $degreNiveau, $degreNiveauFiliere, $section, $pedaAvec, $pedaSans, $global));

		if ($_REQUEST['module'] == 'action')
		{
			if(isset($actions))
			{
				if($actions->count()==1)
					Mapping::RedirectTo('actionDetail', '&id=' . $actions->items(0)->GetActionId());

				$this->view->ActionsList($actions);
			}
		}
		else if ($_REQUEST['module'] == 'actionPopup')
		{
			if(isset($actions))
				$this->view->ActionsList($actions);
		}
	}

	private function InitializeAddForm()
	{
		if($this->getAnneeEnCours()->items(0)) $_POST['anneesScolaire']= $this->getAnneeEnCours()->items(0)->getId();
		else $this->notices[] = "Les encodages ont �t� cl�tur�s pour  l'ann�e acad�mique";
	}

	private function Initialize()
	{
		if($this->getAnneeEnCours()->items(0)) $_POST['anneesScolaire']= $this->getAnneeEnCours()->items(0)->getId();
		else $this->notices[] = "Les encodages ont �t� cl�tur�s pour  l'ann�e acad�mique";
		$_POST["actionValide"] = 'on';
		if($this->user->isOperateur())
		{
			REQUIRE_ONCE(SCRIPTPATH.'model/operateur_database_model_class.php');
			$DB_OP = new OperateurDatabase();
			$res = $DB_OP->getOperateurIdWithUtilisateurId($this->user->getUtilisateurId());
			$row = mysqli_fetch_assoc($res);
			$_POST['operateur'] = $row['operateurId'];
		}
		if(!$this->user->isAgent())
			$_POST['actionLab'] = 'on';
			
		$_POST['actionNonLab'] = 'on';
		$_POST['actionMicro'] = 'on';
	}

	private function getAnneeEnCours()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/annee_database_model_class.php');
		$DB_ANNEE = new AnneeDatabase();
		$annees = new Annees($DB_ANNEE->GetAnneeEnCoursId());
		return $annees;

	}

	private function SetCloseCallBack()
	{
		$closeCallBack = "none";

		if (isset($_REQUEST['close'])) $closeCallBack = $_REQUEST['close'];
		elseif (isset($_POST['CloseCallBack'])) $closeCallBack = $_POST['CloseCallBack'];

		return $closeCallBack;
	}

	private function setPostAction($action)
	{
		$_POST['action_id'] = $action->GetActionId();
		$_POST['refOp'] = $action->GetRefChezOperateur();
		$_POST['refASE'] = $action->GetRefChezASE();
		$_POST['web'] = $action->GetWebsite();
		$_POST['anneesScolaire'] = $action->GetAnneeId();
		$_POST['commentaire'] = $action->GetCommentaire();
		$_POST['nomGeneral'] = $action->GetNomGen();
		$_POST['nomSpecifique'] = $action->GetNomSpec();
		$_POST['nbGlobalApp'] = $action->GetNbApprenants();
		$_POST['nbGlobalEns'] = $action->GetNbEnseignants();
		$_POST['actionType'] = $action->GetTypeActionId();
		$_POST['categorie'] = $action->GetCategActionNonLabelId();
		$_POST['organisateur'] = $action->GetOperateurId();
		$_POST['date'] = $action->GetDateAction(true);
		$_POST['actionTypeLab'] = $action->GetActionLabelId();
		if($action->GetSwGlobal()==1)
		$_POST['swGlobal'] = 1;

		$_POST["selectedOps"] = '';
		$_POST["selectedOpsName"] = '';
		
		$_POST['statutActionId'] = $action->GetStatutActionId();

		if($action->GetTypeActionId()=='LABEL')
		{
			$ops = $action->GetOperateurs();
			for($i=0; $i<$ops->count(); $i++)
			{
				$_POST["selectedOps"] .= ($_POST["selectedOps"] == '' ? '':';').$ops->items($i)->GetOperateurId();
				$_POST["selectedOpsName"] .= ($_POST["selectedOpsName"] == '' ? '':';').$ops->items($i)->GetOperateur()->GetNom();
			}
		}

		$etabs = $action->GetActionEtablissment();
		$_POST['nbLignes'] = $etabs->count();
		$_POST['etabIds'] = '';
		for($i=0; $i<$etabs->count(); $i++)
		{
			$_POST['etabIds'] .= ($_POST["etabIds"] == '' ? '':';').$etabs->items($i)->GetEtablissementId();
			$_POST['etabName_'.$i] = $etabs->items($i)->GetNom();
			$_POST['etabId_'.$i] = $etabs->items($i)->GetEtablissementId();
		}

		$contacts = $action->GetActionContact();
		$_POST['nbLignesContact'] = $contacts->count();
		$_POST['contactIds'] = '';

		for($i=0; $i<$contacts->count(); $i++)
		{
			$_POST['contactIds'] .= ($_POST['contactIds'] == '' ? '':';').$contacts->items($i)->GetContactId();
			$c = $contacts->items($i)->GetContact();
			$_POST['contactName_'.$i] = $c->GetNom(). ' ' .$c->GetPrenom();
			$_POST['contactId_'.$i] = $contacts->items($i)->GetContactId();
			$_POST['roleActionContact_'.$i] = $contacts->items($i)->GetRoleContactActionId();
		}


		$filiere = $action->getFiliereDegreNiveau();
		for($i=0; $i<$filiere->count();$i++)
		{
			$_POST[$filiere->items($i)->GetFiliereDegreNiveauId()] = $filiere->items($i)->GetNbApprenants();
		}

		$section = $action->GetSections();
		for($i=0; $i<$section->count();$i++)
		{
			$_POST[$section->items($i)->getSectionActionId()] = $section->items($i)->GetNbApprenants();
		}

		$swPeda = $action->GetSwPedagogie();
		if($swPeda == 1)
		{
			$_POST['swPeda'] = '1';
			$peda = $action->GetFinalitePedagog();
			for($i=0; $i<$peda->count();$i++)
			{
				$_POST[$peda->items($i)->GetFinalitePedagogId()] = $peda->items($i)->GetNbApprenants();
			}
		}
		else
		{
			$_POST['swPeda'] = '0';
		}
	}
}
?>
