<?php

if(!$session->isLogged()) $session->redirect();

REQUIRE_ONCE(SCRIPTPATH.'view/actionDetail_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');

$ctrl = new ActionDetailController($title, $shortcut, $menu, $login, $notices);
$ctrl->Render();

class ActionDetailController
{
	private $view = null;
	private $title = null;
	private $notices = null;
	private $errors  = null;
	private $id = 0;

	public function __construct(&$title, &$shortcut, &$menu, &$login, &$notices)
	{
	
		$this->view = new ActionDetailView();
		$this->notices = array();
		$this->errors = array();
		$this->title = &$title;
		$shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->view->Notices($this->notices);
		$menu = $this->view->createMenu();
		$login = $this->view->createLogin();
		$this->title = 'Action ';
	}

	public function Render()
	{
		
		if (isset($_REQUEST["id"]) && is_numeric($_REQUEST["id"]))
		{
			if(isset($_REQUEST["error"]) && $_REQUEST["error"]=='0')
			{
				$this->notices[] = 'L\'action a �t� mise � jour avec succ�s.';
			}
			elseif(isset($_REQUEST["error"]) && $_REQUEST["error"]=='1')
			{
				$this->errors[] = 'Une erreur s\'est produite lors de la mise � jour de l\'action';
			}
			
			$this->id = $_REQUEST["id"];
			if(isset($this->id))
			{
				$this->RenderAction();
			}
			elseif (isset($_REQUEST["addAction"]) && $_REQUEST["addAction"]=='1')
			{
				$this->view->Errors($this->errors);	
			}
		}
		else
		{
			Mapping::RedirectTo('accueil');
		}
	}

	private function RenderAction()
	{
		$ets = $this->GetAction();
		if($ets->count()>0)
		{
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->Render($ets->items(0));
		}
		else
		{
			$this->errors[]= 'L\' action n\'existe pas !';
			$this->view->Errors($this->errors);
		}
	}

	private function GetAction()
	{
		$db = new ActionDatabase();
		$db->open();
		$rs = $db->GetById($this->id);
		$db->close();
		return new Actions($rs);
	}
}

?>
