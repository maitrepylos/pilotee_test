<?php
/* Create By Marucci 24/12/2013 Happy christmaes ! */ 
if(!$session->isLogged() && ! $session -> isAdmin() ) $session->redirect();

REQUIRE_ONCE(SCRIPTPATH.'model/ase_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/administration_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/users_search_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/utilisateur_searchresult_domain_class.php');

$view = new AdmninistrationView();
if ($_REQUEST['module'] == 'administration')
{
	$title = "Administration";
	$shortcut = $view->raccourcisPanel($_REQUEST['module']);
	$menu = $view->createMenu();
	$login = $view->createLogin();
}
if (isset ($_REQUEST['action']) ){
	$title = "Gestion des utilisateurs";
	$imported_scripts = $view -> importScripts(array("jquery-1.8.3.min.js"), "");
	$imported_scripts .= $view -> importScripts(array("administration.js"), "/administration");
	echo $imported_scripts;
}
$ec = new AdministrationController($view);
$ec->Render();


class AdministrationController
{
	private $view;
	private $user;
	private $notices;
	private $errors;
	private $fieldserror;
	private $session;
	private $actionId;

	public function __construct($view)
	{
		$this->view = $view;
		$this->session = Session::GetInstance();
		$this->user = $this->session->getCurrentUser();
		$this->errors = array();
		
		if($this->session->exist('erreurs'))
		{
			$this->errors[] = $this->session->get('erreurs');
			$this->session->reset('erreurs'); 
		}
		
		$this->notices = array();
		if($this->session->exist('notices'))
		{
			$this->notices[] = $this->session->get('notices');
			$this->session->reset('notices'); 
		}
		$this->fieldserror = array();

	}

	public function Render()
	{
		if (isset ($_REQUEST['action']) && $_REQUEST['action'] == 'user_search'){
			$title = "Gestion des utilisateurs";
			$this->PerformSearch(null, null);
		}
		else if (isset ($_REQUEST['action']) && $_REQUEST['action'] == 'user_search_populate'){
			$title = "Gestion des utilisateurs";
			$this->PerformSearchAjax(null, null);
		}
		else if (isset ($_REQUEST['action']) && $_REQUEST['action'] == 'view'){
			$title = "Fiche d�taill�e de l'utilisateur";
			$this->viewOneUsers($_REQUEST['id']);
		}
		else if (isset ($_REQUEST['action']) && ($_REQUEST['action'] == 'saveuser' || $_REQUEST['action'] == 'addsaveuser')){
			$errors = array();
			$ud = new UtilisateurDatabase();
			$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;
			if(!isset($_REQUEST['login']) || empty($_REQUEST['login']))
			{
				$errors[] = 'Veuillez indiquer un login';
				$fieldserror[] = 'login';
			}
			if(!isset($_REQUEST['nom']) || empty($_REQUEST['nom']))
			{
				$errors[] = 'Veuillez indiquer un nom';
				$fieldserror[] = 'nom';
			}
			if(!$ud->isUniqueLogin($_REQUEST['login'],$id)){
				$errors[] = 'Le login  "'. $_REQUEST['login'] . '" est d�j� utilis�';
				$fieldserror[] = 'login';
			}
			
			if(!preg_match('#' . REGEX_MAIL . '#i', $_REQUEST['email']))
			{
				$errors[] = 'Le format de l\'adresse de courriel est invalide';
				$fieldserror[] = 'email';
			}
			if(!preg_match('#' . REGEX_MAIL . '#i', $_REQUEST['login']))
			{
				$errors[] = 'Le format du login ne correspond pas � un courriel';
				$fieldserror[] = 'login';
			}
			if ($_REQUEST['typeUtilisateur'] == -1){
				$errors[] = 'Veuillez indiquer un type d\'utilisateur';
				$fieldserror[] = 'typeUtilisateur';
			}
			if($_REQUEST['typeUtilisateur'] == "OPE" &&  $_REQUEST['operatorslist'] == -1){
				$errors[] = 'Veuillez s�lectionner l\'op�rateur';
				$fieldserror[] = 'typeUtilisateur';
			}
			if(!$errors) $this->saveUser();
			else {
				if ($_REQUEST['action'] == 'saveuser') {
					$this-> viewOneUsers($id, $errors);
				}
				else if ($_REQUEST['action'] == 'addsaveuser') {
					$this->viewUsersAdd($errors);
				}
			}
		}
		else if (isset ($_REQUEST['action']) && $_REQUEST['action'] == 'user_add'){
			$this->viewUsersAdd();
		}

				
	}
	
	public function viewUsersAdd($errors = ""){
		$this -> view -> viewAddUser($errors,$_REQUEST['action']);
	}
	
	public function saveUser(){
		$userDatabase = new UtilisateurDatabase();
		//Update
		if(!($_REQUEST['action']=='addsaveuser'))
		{
			$utilisateur = new Utilisateur(array(	"utilisateurId"		=>$_REQUEST['id'],
												"typeUtilisateurId"	=>$_REQUEST['typeUtilisateur'],
												"swActif"			=>isset($_REQUEST['actif'])?($_REQUEST['actif']=='on'?1:0):0,
												"login"				=>$_REQUEST['login'],
												"nom"				=>$_REQUEST['nom'],
												"prenom"			=>$_REQUEST['prenom'],
												"tel"				=>$_REQUEST['tel'],
												"email"				=>$_REQUEST['email'],));
			$userDatabase -> updateUtilisateur($utilisateur,$_REQUEST["operatorslist"]);
			$utilisateurId = intval($_REQUEST['id']);
			
			echo "<script>new Notification('success', \"L'utilisateur a �t� sauvegard�\", 4000).show();</script>";
		}
		//Create
		else
		{
			$utilisateur = new Utilisateur(array(	"typeUtilisateurId"	=>$_REQUEST['typeUtilisateur'],
													"swActif"			=>isset($_REQUEST['actif'])?($_REQUEST['actif']=='on'?1:0):0,
													"login"				=>$_REQUEST['login'],
													"nom"				=>$_REQUEST['nom'],
													"prenom"			=>$_REQUEST['prenom'],
													"tel"				=>$_REQUEST['tel'],
													"email"				=>$_REQUEST['email'],));
			$utilisateurId = $userDatabase -> createUtilisateur($utilisateur,$_REQUEST["operatorslist"]);
			//Pour ne pas recr�er un utilisateur
			$_REQUEST['id'] = $utilisateurId;
			
			REQUIRE_ONCE(SCRIPTPATH.'model/ase_database_model_class.php');
			$DB = new AseDatabase();
			$DB->open();
			$password_sanitize	 = GeneratePassword();
			$DB->setPassword($_REQUEST['login'],$password_sanitize);
			
			$email = 'Vous avez demandez la cr�ation d\'un nouvel utilisateur.
Vous trouverez ci-dessous les informations associ�es � l\'utilisateur :
';

			$email.='
Identifiant  :    '.$_REQUEST['login'].'
Mot de passe :    '.$password_sanitize.'
';

			REQUIRE_ONCE(SCRIPTPATH.'lib/mailsender.class.php');

			MailSender::initialize('CTi@as-e.be','Inscription nouvel utilisateur', $email, null, '', 'ASE');
			MailSender::send();
			
			MailSender::initialize('nicolas.marucci@aznetwork.eu','Inscription nouvel utilisateur', $email, null, '', 'ASE');
			MailSender::send();
			
			echo "<script>new Notification('success', \"L'utilisateur a �t� cr��\", 4000).show();</script>";
			
			$DB->close();
		}
		
		$this -> viewOneUsers($utilisateurId);
	}
	
	public function viewOneUsers($utilisateurId, $errors = ''){
		$userDatabase = new UtilisateurDatabase();
		$user = new Utilisateurs($userDatabase-> get($utilisateurId));
		$user = $user->items(0);
		$this -> view -> viewOneUser($user, $errors);
	}
	
	public function PerformSearchAjax(){
		$array = json_decode($_REQUEST['data']);
		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get(null, null,  $array->nom, $array->prenom, $array->email, $array->login, $array->typeUtilisateur, $array->begin, $array->offset);
		
		$users = new UtilisateurSearchResults($rs);
		
		$this -> view -> viewUsersSearchTable($users, $array->begin, $array->offset, $db->getCount());
		
		$db->close();
	}
	
	public function PerformSearch($userId, $userType, $begin = 0, $offset = 15)
	{
		$nom = isset($_REQUEST['nom'])?($_REQUEST['nom']):null;
		$prenom = isset($_REQUEST['prenom'])?($_REQUEST['prenom']):null;
		$email = isset($_REQUEST['email'])?($_REQUEST['email']):null;
		$login = isset($_REQUEST['login'])?($_REQUEST['login']):null;
		$typeUtilisateur = isset($_REQUEST['typeUtilisateur'])?($_REQUEST['typeUtilisateur']):null;
		
		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get($userId, $userType, $nom, $prenom, $email, $login, $typeUtilisateur, $begin, $offset);
		
		$users = new UtilisateurSearchResults($rs);
		
		$this -> view -> viewUsersSearchFilter();
		
		$this -> view -> viewUsersSearchTable($users, 0, 15, $db->getCount());
		
		$db->close();
	}
}
?>
