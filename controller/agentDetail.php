<?php

    if(!$session->isLogged()) $session->redirect();

	REQUIRE_ONCE(SCRIPTPATH.'view/agentDetail_view_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
	
	$ctrl = new AgentDetailController($title, $shortcut, $menu, $login, $notices);
	$ctrl->Render();

	class AgentDetailController
	{
		private $view = null;
		private $title = null;
		private $notices = null;
		private $errors = null;
		private $id = null; 
		
		public function __construct(&$title, &$shortcut, &$menu, &$login, &$notices)
		{
			$this->view = new AgentDetailView();
			$this->notices = array();
			$this->errors = array();
			$this->title = &$title;
			$shortcut = $this->view->raccourcisPanel();
			$this->view->Notices($this->notices);
			$menu = $this->view->createMenu();
			$login = $this->view->createLogin();
			$this->title = 'Agent : ';
		}
		
		public function Render()
		{
			if (isset($_REQUEST["id"]))
			{
				$this->id = $_REQUEST["id"];
				if(isset($this->id)&& is_numeric($this->id))$this->RenderAgent(); 
				else Mapping::RedirectTo('accueil');
				
			}
		}
		
		private function RenderAgent()
		{
			$ets = $this->GetAgent();
			if($ets->Count()>0)
			{
				$this->title .= $ets->items(0)->GetNom(). ' ' . $ets->items(0)->GetPrenom() ;
				$this->view->Render($ets->items(0));
			}
			else
			{
				$this->errors[]= 'L\'agent n\'existe pas !';
				$this->view->Errors($this->errors);
			}
		}

		private function GetAgent()
		{
			$db = new UtilisateurDatabase();
			$db->open();
			$rs = $db->get($this->id, 'AGE');
			$db->close();
			
			return new Utilisateurs($rs);
		}
	}

?>
	