<?php

// disabling logging for this
ENV::$active = false;

REQUIRE_ONCE(SCRIPTPATH . 'model/codepostal_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'model/contact_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'config/constants.php');
REQUIRE_ONCE(SCRIPTPATH . 'config/database.php');
REQUIRE_ONCE(SCRIPTPATH . 'lib/env.class.php');

if(isset($_REQUEST['contact']))
{
	$DB = new ContactDatabase();
	$DB->open();
	$id = $DB->getLastContact();
	$DB->close();
	echo $id;
}
elseif(isset($_REQUEST['doublon']))
{
	$nom = $_REQUEST['nom'];
	$prenom = $_REQUEST['prenom'];
	$DB = new ContactDatabase();
	$DB->open();
	$arrayDoublon = $DB->exist($nom,$prenom);
	$DB->close();
	if(count($arrayDoublon)>0)
	{
		echo "1";
	}
	else
		echo "0";
}
elseif(isset($_REQUEST['report']))
{
	$idProvince = $_REQUEST['val'];
	if(strlen($idProvince)>0)
	{
		$idProvince = substr($idProvince, 0, strlen($idProvince)-2);
		$ids = explode('||', $idProvince);
	}
	else
		$ids = null;
	$db = new CodePostalDatabase();
	$db->open();
	$res = $db->getArrondissements($ids);
	$db->close();
	if ($res && mysqli_num_rows($res) > 0)
	{
		$liste_total=array();
		while ($row = mysqli_fetch_assoc($res))
		{
			$liste_total[]=$row["dic_arrondissementId"]."||".$row["label"];
		}
		echo utf8_encode(join('/#-#/',$liste_total));
	}
}
elseif(isset($_REQUEST['actionLabelise']))
{
	$anneeScolaire = trim($_REQUEST['val']);

	require_once SCRIPTPATH . 'model/operateur_database_model_class.php';
	// require_once SCRIPTPATH . 'domain/operateur_domain_class.php';
	$db = new OperateurDatabase();
	$operateurs = null;
	if (!empty($anneeScolaire))
	{
		$operateurs = $db->getOperateurInOneAnneeScolaire($anneeScolaire);
	}
	else
	{
		$operateurs = $db->get();
	}

	// $operateurs = new Operateurs($operateurs);
	if (!empty($operateurs) && mysqli_num_rows($operateurs) > 0)
	{
		$list = array();
		while ($row = mysqli_fetch_assoc($operateurs))
		{
			$list[] = $row['operateurId'] . '||' . $row['nom'];
		}
		echo utf8_encode(join('/#-#/', $list));
	}
	$db->free($operateurs);
	$db->close(true);
}
else
{
	$idProvince = $_REQUEST['val'];
	if($idProvince=='-1')
		$idProvince = null;

	$db = new CodePostalDatabase();
	$db->open();
	$res = $db->getArrondissements($idProvince);
	$db->close();

	if ($res && mysqli_num_rows($res) > 0)
	{
		$liste_total=array();
		while ($row = mysqli_fetch_assoc($res))
		{
			$liste_total[]=$row["dic_arrondissementId"]."||".$row["label"];
		}
		echo utf8_encode(join('/#-#/',$liste_total));
	}
}
