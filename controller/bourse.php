<?php
if(!$session->isLogged()) $session->redirect();
REQUIRE_ONCE(SCRIPTPATH.'model/ase_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/bourse_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/bourse_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/annee_database_model_class.php');

$view = new BourseView();

if ($_REQUEST['module'] == 'bourse')
{
	$title = "Projets entrepreneuriaux";
	$shortcut = $view->raccourcisPanel($_REQUEST['module']);
	$menu = $view->createMenu();
	$login = $view->createLogin();
}
$ec = new BourseController($view);
$ec->Render();

class BourseController
{
	private $view;
	private $user;
	private $notices;
	private $errors;
	private $fieldserror;
	private $actionId;
	
	
	public function __construct($view)
	{
		 $this->view = $view;
		 $this->errors = array();
		 $this->notices = array();
		 $this->fieldserror = array();
		 $this->user = Session::GetInstance()->getCurrentUser();
	}

	public function Render()
	{
		if(!isset($_REQUEST['action']))
		{
			if (isset($_POST['searchBourses']))
			{
				$this->view->Search($this->user->isOperateur(), $this->SetCloseCallBack());
				$this->Search();
			}
			else
			{
				$this->Initialize();
				if(isset($_REQUEST['delete']))
					$this->notices[] = 'Le projet entrepreneurial � �t� supprim� avec succ�s';
					
				$this->view->Notices($this->notices);
				$this->view->Errors($this->errors);
				$this->view->Search($this->user->isOperateur(), $this->SetCloseCallBack());
			}
		}
		elseif($_REQUEST['action'] == 'delete')
		{
			if(isset($_REQUEST['deleteId']))
				$this->delete();
		}
		elseif($_REQUEST['action'] == 'update')
		{
			$this->RenderUpdate();
		}
		elseif($_REQUEST['action'] == 'add')
		{
			$this->RenderAdd();
		}
		
	}
	
	function RenderUpdate()
	{
		if(isset($_REQUEST['updateId']))
		{
			$action = $this->GetAction($_REQUEST['updateId']);
			$this->setPostAction($action);
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->AddBourse(true);
		}
		elseif(isset($_POST['updateForm']))
		{
			$idAction = $_POST['action_id'];
			$this->insert_update($idAction);
			Mapping::RedirectTo('bourseDetail&id='.$idAction.'&error=0');
		}
	}
		
	public function RenderAdd()
	{
		if (isset($_POST['addForm']) )//Si le formulaire action est post�
		{
			$this->insert_update();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->AddBourse(false, $this->actionId);
		}
		else
		{
			$this->InitializeAddForm();
			
			$this->view->AddBourse();
		}
	}
	
	private function insert_update($idAction = null)
	{
		$this->actionId= $idAction; $statutSubventionId = null;
		$refOp = null; $refAse = null; 
		$categ = null; $operateurOrg = null; $dateAction = null;
		$nomGen = null; $nomSpec = null; $commentaire = null;
		$nbEnseignants = null; $nbApprenants = null; $web = null; $swPeda = 0;
		$montantDemande = null; $montantSubvention=null; $categorieBourse = null;
		$projetRealise = null; $groupeClasseConcernee = null; $collaborationInterEtab = null;
		$actionTypeLab = null;
		
		$statutSubventionId = 'EVALU';
		if(isset($_POST['updateForm']))
		{
			if(isset($_POST['subvention']) && $_POST['subvention'] == 'OBTEN')
			{
				$montantSubvention =  $_POST['montanSub'];
			}
			$statutSubventionId = $_POST['subvention'];
		}
		
		
		$statutActionId = 'ENCOU';
		$anneeScolaireId = $_POST['anneesScolaire'];
		$typeActionId = 'BOUPI';

		

		if(!empty($_POST['categBourse']))
			$categorieBourse = $_POST['categBourse'];

		if(!empty($_POST['montantDemande']))
			$montantDemande = $_POST['montantDemande'];

		if(!empty($_POST['projetReal']))
			$projetRealise = $_POST['projetReal'];
			
		if(!empty($_POST['nbClasseC']))
			$groupeClasseConcernee = $_POST['nbClasseC'];

		if(!empty($_POST['collabInter']))
			$collaborationInterEtab = $_POST['collabInter'];			

		if(!empty($_POST['refASE']))
			$refAse = $_POST['refASE'];

		if($_REQUEST['statut'] == 'valider')
			$statutActionId = 'VALID';

		if(!empty($_POST['date']))
			$dateAction = $_POST['date'];

		$nomGen = !empty($_POST['nomGeneral']) ?$_POST['nomGeneral'] : null;
			
		if(!empty($_POST['nomSpecifique']))
			$nomSpec = $_POST['nomSpecifique'];

		if(!empty($_POST['commentaire']))
			$commentaire = $_POST['commentaire'];

		$web  = !empty($_POST['web']) ?$_POST['web'] : null;

		$nbEnseignants = $_POST['nbGlobalEns'];
		$nbApprenants = $_POST['nbGlobalApp'];

		//$global = isset($_POST['swGlobal']) ?$_POST['swGlobal'] : 0;
		$global = 0;
		$nbEtabs = !empty($_POST['nbLignes']) ?$_POST['nbLignes'] : null;
		$nbContacts = !empty($_POST['nbLignesContact']) ?$_POST['nbLignesContact'] : null;
		$contactAction = array();
		
		for($i=0; $i<$nbContacts; $i++)
		{
			if(isset($_POST['roleActionContact_'.$i]))
				$contactAction[] = array("id"=>$_POST['contactId_'.$i],"role" => $_POST['roleActionContact_'.$i]);
		}

		$fdn = Dictionnaries::getFiliereDegreNiveauList();
		$niveauDegreFiliere = array();
		for($i=0; $i<$fdn->Count(); $i++)
		{
			if(!empty($_POST[$fdn->items($i)->GetFiliereDegreNiveauId()]))
				$niveauDegreFiliere[] = array("id"=>$fdn->items($i)->GetFiliereDegreNiveauId(), "nb"=>$_POST[$fdn->items($i)->GetFiliereDegreNiveauId()]);
		}

		$section = Dictionnaries::getSectionActionList();
		$sections = array();
		for($i=0; $i<$section->Count(); $i++)
		{
			if(!empty($_POST[$section->items($i)->getSectionActionId()]))
				$sections[] = array("id"=>$section->items($i)->getSectionActionId(), "nb"=>$_POST[$section->items($i)->getSectionActionId()]);
		}

		$pedas = array();
		if(isset($_POST['swPeda'])&& $_POST['swPeda'] == '1')
		{
			$swPeda = $_POST['swPeda'];
			$peda = Dictionnaries::getFinalitePedagogList();
			for($i=0; $i<$peda->Count(); $i++)
			{
				if(!empty($_POST[$peda->items($i)->GetFinalitePedagogId()]))
					$pedas[] = array("id"=>$peda->items($i)->GetFinalitePedagogId(), "nb"=>$_POST[$peda->items($i)->GetFinalitePedagogId()]);
			}
		}
		//INSERT
		$dbAction = new ActionDatabase();
		try
		{
			
			$dbAction->open();
			$dbAction->startTransaction();
			$action = new Action();
			$action->init($this->actionId, $statutSubventionId, $actionTypeLab, $refOp, $refAse,
							$typeActionId, $_POST['anneesScolaire'], $statutActionId, $categ, $operateurOrg,
							$dateAction, $nomGen, $nomSpec, $commentaire, $global, $swPeda, $nbApprenants, $nbEnseignants,
							$web, $montantDemande, $montantSubvention, $categorieBourse, $projetRealise, $groupeClasseConcernee, $collaborationInterEtab,
							date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
			
			if(!isset($_POST['updateForm']))
				$this->actionId = $dbAction->insertAction($action);
			else
			{
				$dbAction->deleteActionOperateur($action->GetActionId());
				$dbAction->deleteActionPeda($action->GetActionId());
				$dbAction->deleteActionSection($action->GetActionId());
				$dbAction->deleteActionFiliereDegreNiveau($action->GetActionId());
				$dbAction->deleteActionContact($action->GetActionId());
				$dbAction->deleteActionEtablissement($action->GetActionId());
				$this->actionId = $dbAction->updateAction($action);
			}
			
			//ActionEtablissement
			if($_POST['nbLignes']!='0' && !empty($_POST['etabIds']))
			{
				$etabIds = explode(";", $_POST['etabIds']);
				foreach($etabIds as $etabId)
				{
					$actionEtab = new ActionEtablissement();
					$actionEtab->init($this->actionId, $etabId, date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
					$dbAction->insertActionEtablissement($actionEtab);
				}
			}
			
			//ActionContact
			for($i=0; $i< count($contactAction); $i++)
			{
				$actionC = new ActionContact();
				$actionC->init($this->actionId,$contactAction[$i]['id'], $contactAction[$i]['role'], date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
				$dbAction->insertActionContact($actionC);
			}
			
			//R�partition par niveau-degr�-fili�re
			for($i=0; $i< count($niveauDegreFiliere); $i++)
			{
				$afdn = new ActionFiliereDegreNiveau();
				$afdn->init($this->actionId, $niveauDegreFiliere[$i]['id'], $niveauDegreFiliere[$i]['nb']);
				$dbAction->insertActionFiliereDegreNiveau($afdn);
			}

			for($i=0; $i< count($sections); $i++)
			{
				$actionSection = new ActionSectionAction();
				$actionSection->init($this->actionId, $sections[$i]['id'], $sections[$i]['nb']);
				$dbAction->insertActionSection($actionSection);
			}

			if($swPeda!=null)
			{
				for($i=0; $i< count($pedas); $i++)
				{
					$actionPeda = new ActionFinalitePedagog();
					$actionPeda->init($this->actionId, $pedas[$i]['id'], $pedas[$i]['nb']);
					$dbAction->insertActionPeda($actionPeda);
				}
			}

			//AssocOperateur
			if($typeActionId == 'LABEL')
			{
				if(!empty($_POST["selectedOps"]))
				{
					$ops = explode(";", $_POST["selectedOps"]);
					foreach($ops as $op)
					{
						$actionOp = new ActionOperateur();
						$actionOp->init($this->actionId, $op);
						$dbAction->insertActionOperateur($actionOp);
					}
				}
			}

			$dbAction->commitTransaction();
			$dbAction->close();
		}
		catch(Exception $e)
		{
			//echo $e->getMessage();
			$dbAction->rollbackTransaction();
			
			$dbAction->close();
			
			if(!isset($_POST['updateForm']))
				$this->errors[] = 'Une erreur s\'est produite lors de l\'ajout de l\'action !';
			else
				Mapping::RedirectTo('bourseDetail&id='.$idAction.'&error=1');
		}
	}
	
	private function getAnneeEnCours()
	{
		$DB_ANNEE = new AnneeDatabase();
		$annees = new Annees($DB_ANNEE->GetAnneeEnCoursId());
		return $annees;

	}
		
	private function InitializeAddForm()
	{
		$_POST['anneesScolaire']= $this->getAnneeEnCours()->items(0)->getId();
	}
		
	private function delete()
	{
		
		try
		{
			$action = $this->GetAction($_REQUEST['deleteId']);
			$DB = new ActionDatabase();
			$DB->open();
			$DB->startTransaction();

			$DB->deleteActionOperateur($action->GetActionId());
			$DB->deleteActionPeda($action->GetActionId());
			$DB->deleteActionSection($action->GetActionId());
			$DB->deleteActionFiliereDegreNiveau($action->GetActionId());
			$DB->deleteActionContact($action->GetActionId());
			$DB->deleteActionEtablissement($action->GetActionId());
			$DB->deleteAction($action->GetActionId());

			$DB->commitTransaction();

		}
		catch(Exception $e)
		{
			$DB->rollbackTransaction();
			$DB->close();
		}
		$DB->close();
		Mapping::RedirectTo('bourse&delete=1');
		
	}

	private function GetAction($id)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
		$db = new ActionDatabase();
		$db->open();
		$rs = $db->GetById($id);
		$db->close();
		$ets = new Actions($rs);
		return $ets->items(0);
	}	
	
	private function Search()
	{
		$projetInno = $_POST['projetInnovant'];
	
		$sub_obtenu = (isset($_POST['sub_obtenu']))?true : false;
		$sub_refus  = (isset($_POST['sub_refus']))?true : false;
		$sub_eval = (isset($_POST['sub_eval']))?true:false;
		$actionValide = (isset($_POST['bourseValide']))? $_POST['bourseValide'] : null;
		$actionEnCours = (isset($_POST['bourseEnCours']))?$_POST['bourseEnCours']:null;
		$refAse = $_POST['refAse'];
		$anneeScolaire = $_POST['anneesScolaire'];
		$anneeAnte = $_POST['annee_ante'];
		$anneePost = $_POST['annee_post'];
		$etabIds =  $_POST['selectedEts'];
		$niveau = $_POST['niveauAction']; 
		$degreNiveau = $_POST['niveauDegreAction'];
		$degreNiveauFiliere = $_POST['niveauDegreFilereAction'];
		$section = $_POST['actionSection'];
		$pedaAvec = (isset($_POST['pedaAvec']))?$_POST['pedaAvec']:null;
		$pedaSans = (isset($_POST['pedaSans']))?$_POST['pedaSans']:null;

		//Commentaires
		$aCommenter = null;
		$commenter = null;
		
		if(isset($_POST['aCommenter']) && isset($_POST['commente'])){}
		elseif(isset($_POST['aCommenter']))
			$aCommenter = $_POST['aCommenter'];
		elseif(isset($_POST['commente']))
			$commenter = $_POST['commente'];	

		$global = null;
		/*
		if(isset($_POST['actionGlob']))
			$global = $_POST['actionGlob'];			
		*/	
			
		$DB_BOURSE = new BourseDatabase();
		$bourses = new Actions($DB_BOURSE->get($projetInno, $sub_obtenu, $sub_refus, $sub_eval, $anneeScolaire, $anneeAnte, $anneePost, $etabIds, $actionValide, $actionEnCours, $refAse, $niveau, $degreNiveau, $degreNiveauFiliere, $section, $pedaAvec, $pedaSans, $aCommenter, $commenter, $global));
	
		if ($_REQUEST['module'] == 'bourse')
		{
			if(isset($bourses))
			{
				if($bourses->count()==1)
					Mapping::RedirectTo('bourseDetail', '&id=' . $bourses->items(0)->GetActionId());
				
				$this->view->BoursesList($bourses);
			}
		}
		else if ($_REQUEST['module'] == 'boursePopup')
		{
			if(isset($bourses))
				$this->view->BoursesList($bourses);
		}		
	}
	
	private function Initialize()
	{
		$DB_ANNEE = new AnneeDatabase();
    	$annees = new Annees($DB_ANNEE->GetAnneeEnCoursId()); 
		$_POST['anneesScolaire']= $annees->items(0)->getId();
 		$_POST["sub_eval"] = 'on';
 		$_POST["bourseValide"] = 'on';
	}
	
	private function SetCloseCallBack()
	{
		$closeCallBack = "none";
		
		if (isset($_REQUEST['close'])) $closeCallBack = $_REQUEST['close'];
		elseif (isset($_POST['CloseCallBack'])) $closeCallBack = $_POST['CloseCallBack'];
		
		return $closeCallBack;
	}	
	
	private function setPostAction($action)
	{
		$_POST['action_id'] = $action->GetActionId();
		//$_POST['refOp'] = $action->GetRefChezOperateur();
		$_POST['refASE'] = $action->GetRefChezASE();
		$_POST['web'] = $action->GetWebsite();
		$_POST['anneesScolaire'] = $action->GetAnneeId();
		$_POST['commentaire'] = $action->GetCommentaire();
		$_POST['nomGeneral'] = $action->GetNomGen();
		$_POST['nomSpecifique'] = $action->GetNomSpec();
		$_POST['nbGlobalApp'] = $action->GetNbApprenants();
		$_POST['nbGlobalEns'] = $action->GetNbEnseignants();
		$_POST['date'] = $action->GetDateAction(true);
		$_POST['montantDemande'] = $action->GetMontantDemande();
		$_POST['categBourse'] = $action->GetCategorieBourse();
		$_POST['projetReal'] = $action->GetProjetRealise();
		$_POST['nbClasseC'] = $action->GetGroupeClasseConcernee();
		$_POST['collabInter'] = $action->GetCollaborationInterEtab();
		$_POST['subvention'] = $action->GetStatutSubventionId();
		$_POST['montanSub'] = $action->GetMontantSubvention();
		if($action->GetSwGlobal()==1)
			$_POST['swGlobal'] = 1;
		
		$_POST['statutActionId'] = $action->GetStatutActionId();

		$etabs = $action->GetActionEtablissment();
		$_POST['nbLignes'] = $etabs->count();
		$_POST['etabIds'] = '';
		for($i=0; $i<$etabs->count(); $i++)
		{
			$_POST['etabIds'] .= ($_POST["etabIds"] == '' ? '':';').$etabs->items($i)->GetEtablissementId();
			$_POST['etabName_'.$i] = $etabs->items($i)->GetNom();
			$_POST['etabId_'.$i] = $etabs->items($i)->GetEtablissementId();
		}

		$contacts = $action->GetActionContact();
		$_POST['nbLignesContact'] = $contacts->count();
		$_POST['contactIds'] = '';

		for($i=0; $i<$contacts->count(); $i++)
		{
			$_POST['contactIds'] .= ($_POST['contactIds'] == '' ? '':';').$contacts->items($i)->GetContactId();
			$c = $contacts->items($i)->GetContact();
			$_POST['contactName_'.$i] = $c->GetNom(). ' ' .$c->GetPrenom();
			$_POST['contactId_'.$i] = $contacts->items($i)->GetContactId();
			$_POST['roleActionContact_'.$i] = $contacts->items($i)->GetRoleContactActionId();
		}


		$filiere = $action->getFiliereDegreNiveau();
		for($i=0; $i<$filiere->count();$i++)
		{
			$_POST[$filiere->items($i)->GetFiliereDegreNiveauId()] = $filiere->items($i)->GetNbApprenants();
		}

		$section = $action->GetSections();
		for($i=0; $i<$section->count();$i++)
		{
			$_POST[$section->items($i)->getSectionActionId()] = $section->items($i)->GetNbApprenants();
		}

		$swPeda = $action->GetSwPedagogie();
		if($swPeda == 1)
		{
			$_POST['swPeda'] = '1';
			$peda = $action->GetFinalitePedagog();
			for($i=0; $i<$peda->count();$i++)
			{
				$_POST[$peda->items($i)->GetFinalitePedagogId()] = $peda->items($i)->GetNbApprenants();
			}
		}
		else
		{
			$_POST['swPeda'] = '0';
		}
	}
	
}



 





?>