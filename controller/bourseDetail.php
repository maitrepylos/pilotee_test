<?php

    if(!$session->isLogged()) $session->redirect();

	REQUIRE_ONCE(SCRIPTPATH.'view/bourseDetail_view_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/bourse_database_model_class.php');
	
	$ctrl = new BourseDetailController($title, $shortcut, $menu, $login, $notices);
	$ctrl->Render();

	class BourseDetailController
	{
		private $view = null;
		private $title = null;
		private $notices = null;
		private $errors  = null;
		private $id = 0; 
		
		public function __construct(&$title, &$shortcut, &$menu, &$login, &$notices)
		{
			$this->view = new BourseDetailView();
			$this->notices = array();
			$this->errors = array();
			$this->title = &$title;
			$shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
			$this->view->Notices($this->notices);
			$menu = $this->view->createMenu();
			$login = $this->view->createLogin();
			$this->title = 'Projet entrepreneurial';
		}
		
		public function Render()
		{
			if(isset($_REQUEST['addComm']))
			{
				$this->notices[] = "Le commentaire a �t� ajout� avec succ�s.";
			}
			if (isset($_REQUEST["id"]) && is_numeric($_REQUEST["id"]))
			{
				if(isset($_REQUEST["error"]) && $_REQUEST["error"]=='0')
				{
					$this->notices[] = 'L\'action a �t� mise � jour avec succ�s';
				}
				elseif(isset($_REQUEST["error"]) && $_REQUEST["error"]=='1')
				{
					$this->errors[] = 'Une erreur s\'est produite lors de la mise � jour de l\'action';
				}
				
				$this->id = $_REQUEST["id"];
				$this->RenderBourse(); 
			}
			else
			{
				Mapping::RedirectTo('accueil');
			}
		}
		
		private function RenderBourse()
		{
			$ets = $this->GetBourse();
			if($ets->Count()>0)
			{
				$this->view->Notices($this->notices);
				$this->view->Errors($this->errors);
				$this->view->Render($ets->items(0));
			}
			else
			{
				$this->errors[]= 'Le projet entrepreneurial n\'existe pas !';
				$this->view->Errors($this->errors);
			}
		}

		private function GetBourse()
		{
			$db = new BourseDatabase();
			$db->open();
			$rs = $db->GetById($this->id);
			$db->close();
			
			return new Actions($rs);
		}
	}

?>
	