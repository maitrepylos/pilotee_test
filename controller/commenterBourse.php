<?php
if(!$session->isLogged()) $session->redirect();
REQUIRE_ONCE(SCRIPTPATH.'model/ase_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/bourse_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/commentairebourse_view_class.php');

$view = new CommentaireView();
if ($_REQUEST['module'] == 'commenterBourse')
{
	$title = "Commenter : ";
	$shortcut = $view->raccourcisPanel($_REQUEST['module']);
	$menu = $view->createMenu();
	$login = $view->createLogin();
}
$ec = new CommentaireController($view, $title);
$ec->Render();

class CommentaireController
{
	private $view;
	private $user;
	private $title;
	private $bourseId = null;
	private $agentId = null;
	private $errors;
	private $notices;


	public function __construct($view, &$title)
	{
		$this->view = $view;
		$this->user = Session::GetInstance()->getCurrentUser();
		$this->title = &$title;
		$errors = array();
		$notices = array();
	}

	public function Render()
	{

		if (isset($_REQUEST["bourseId"]) && isset($_REQUEST["agentId"]) && is_numeric($_REQUEST["bourseId"]) && is_numeric($_REQUEST["agentId"]))
		{
			$this->bourseId = $_REQUEST["bourseId"];
			$this->agentId = $_REQUEST["agentId"];
			if(isset($_POST['addCommentaire']))
			{
				$syntheseAvis = $_POST['syntheseAvis'];
				$avisQualitatif = $_POST['avisQualitatif'];
				$eltImportant = $_POST['eltImportant'];
				$soutientFiancier = $_POST['soutientFiancier'];
				//Ajout du commentaire dans la table bourse_agent
				$bourseAgent = new BourseAgent();
				$bourseAgent->init($this->user->getUtilisateurId(), $this->bourseId, date('Y-m-d H:i:s'), $syntheseAvis, $avisQualitatif,$eltImportant, $soutientFiancier,  date('Y-m-d H:i:s'),
				$this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
				$DB_BOURSE = new BourseDatabase();
				if($DB_BOURSE->insertBourseAgent($bourseAgent))
					Mapping::RedirectTo('bourseDetail&id='.$this->bourseId.'&addComm=1');
			}
			else
				$this->RenderBourseAgent();
		}
		else
		{
			Mapping::RedirectTo('accueil');
		}
	}

	private function RenderBourseAgent()
	{
		//recherche les info de la bourse
		$bourse = $this->GetBourse();
		if(isset($bourse))
		{
			$this->title .= $bourse->GetNom();
			$this->view->addCommentaire($this->bourseId, $this->agentId);
		}
		else
		{
			$this->errors[]= 'Impossible d\'ajouter un commentaire !';
			$this->view->Errors($this->errors);
		}
		
	}

	private function GetBourse()
	{
		$db = new BourseDatabase();
		$db->open();
		$rs = $db->GetById($this->bourseId);
		$db->close();
		$actions = new Actions($rs);
		if($actions->count()>0)
			return $actions->items(0);
		else return null;
	}

}
?>