<?php
if(!$session->isLogged()) $session->redirect();
REQUIRE_ONCE(SCRIPTPATH.'model/ase_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/contact_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/historiqueEncodage_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'lib/util.php');
REQUIRE_ONCE(SCRIPTPATH.'view/contact_view_class.php');

$view = new ContactView();
if ($_REQUEST['module'] == 'contact')
{
	$title = "Contacts";
	$shortcut = $view->raccourcisPanel($_REQUEST['module']);
	$menu = $view->createMenu();
	$login = $view->createLogin();
}
$ec = new ContactController($view);
$ec->Render();

class ContactController
{
	private $view;
	private $user;
	private $notices;
	private $errors;
	private $fieldserror;
	private $contacts;
	private $doublon;
	private $contactsDoublons = null;
	private $session;
	private $idContact;


	public function __construct($view)
	{
		$this->view = $view;
		$this->session = Session::GetInstance();
		$this->user = $this->session->getCurrentUser();
		$this->errors = array();
		$this->notices = array();
		$this->fieldserror = array();
		$this->doublon = false;
		$this->contacts = null;
		$this->session = Session::GetInstance();
		$this->contactsDoublons = null;

	}

	public function RenderSearchContact()
	{
		if (isset($_POST['searchContact']))//Recherche
		{
			$this->search();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->Search($this->SetCloseCallBack());
			//Affichage des r�sultats
			if(isset($this->contacts) && $this->contacts->count()==1 && $_REQUEST['module']=='contact')//Si le nombre de r�sultat est == 1 ==> redirection vers la fiche d�tail du contact
			{
				$this->idContact = $this->contacts->items(0)->GetContactId();
				Mapping::RedirectTo('contactDetail&id='.$this->idContact);
			}
			elseif(isset($this->contacts))
				$this->view->ContactList($this->contacts);
		}
		else
		{
			$this->Initialize();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->Search($this->SetCloseCallBack());
		}
	}
	
	public function Render()
	{
		if(!isset($_REQUEST['action']))
		{
			if(isset($_REQUEST['delete']) && $_REQUEST['delete']=='1')
			{
				$this->notices[] = 'Le contact � �t� supprim� avec succ�s';
			}
			$this->RenderSearchContact();
			$this->view->framePopup();
		}
		elseif($_REQUEST['action'] == 'delete')
		{
			if(isset($_REQUEST['deleteId']) && is_numeric($_REQUEST['deleteId']))
			{ 
				$this->delete();
			}
			else
			{
				Mapping::RedirectTo('accueil');
			}
		}
		elseif($_REQUEST['action'] == 'update')
		{
			$this->RenderUpdate();
		}
		elseif($_REQUEST['action'] == 'add')
		{
			$this->RenderAdd();
		}
		elseif($_REQUEST['action'] == 'doublon')
		{
			$DB = new ContactDatabase();
			$DB->open();
				$arrayDoublon = $DB->exist($_REQUEST['nom'],$_REQUEST['prenom']);
			
			if(count($arrayDoublon)>0)
			{
				//Doublons d�tect�s
				$this->doublon = true;
				$this->contactsDoublons = array();
				for($i=0; $i<count($arrayDoublon); $i++)
				{
					$res = $DB->get($arrayDoublon[$i]['id']);
					$row = mysqli_fetch_assoc($res);
					$contact = new Contact();
					$contact->init($row['contactId'], $row['nom'], $row['prenom'], $row['adresseId'], $row['genreId'], $row['ageId'], $row['statutId'], $row['contactGlobal'], $row['swActif'],null, null, null, null);
					$this->contactsDoublons[] = $contact;
				}
				$this->view->doublonContacts($this->contactsDoublons);
			}
			$DB->close();
		}
		elseif($_REQUEST['action']=='contactList')
		{
			$this->RenderContactList();
		}
	}
	
	function RenderUpdate()
	{
		if(isset($_REQUEST['updateId']) && is_numeric($_REQUEST['updateId']))
		{
			$contact = $this->GetContact($_REQUEST['updateId']);
			if($contact!=null)
			{
				$this->setPostContact($contact);
				$this->view->Notices($this->notices);
				$this->view->Errors($this->errors);
				$this->view->formContact(false, null);
				$this->view->framePopup();
			}
			else
			{
				$this->errors[]= 'Le contact n\'existe pas !';
				$this->view->Errors($this->errors);
			}
		}
		elseif(isset($_POST['updateForm']))
		{
			$this->insert_updateContact();
		}
	}
	
	function RenderContactList()
	{
		$etab = null;
		if(isset($_REQUEST['etabs']))
			$etab = $_REQUEST['etabs'];
					
		$global = null;
		if(isset($_REQUEST['global']))
			$global = $_REQUEST['global'];
					
		if(isset($_REQUEST['etabs']) || isset($_REQUEST['global']))
		{
			$DBContact = new ContactDatabase();
			$DBContact->open();
			$res = $DBContact->SearchContact(null, null, null, $etab, 1, null, $global);
			$this->contacts = new Contacts($res);
			$DBContact->close();
			$this->view->ContactList($this->contacts,$this->SetCloseCallBack());
		}
	}
	
	function RenderAdd()
	{
		$boExitPopup = false;
		if(isset($_REQUEST['exitPopup']))
			$boExitPopup = true;
		
		if (isset($_POST['addForm']) )//Si le formulaire action est post�
			$this->insert_updateContact();
			
		$this->view->Notices($this->notices);
		$this->view->Errors($this->errors);
		
		$this->view->formContact(true, $this->idContact, $boExitPopup);
		$this->view->framePopup();
	}
	
	function Initialize()
	{
		$_POST["swActif"]='on';
	}

	function search()
	{
		$swActif = (isset($_POST['swActif']))?$_POST["swActif"]:null;
		$swInactif = (isset($_POST['swInactif']))? $_POST['swInactif']:null;
		$glob = (isset($_POST['etabGlob']))? $_POST['etabGlob']:null;

		$DBContact = new ContactDatabase();
		$DBContact->open();
			$res = $DBContact->SearchContact($_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['selectedEts'], $swActif, $swInactif, $glob);
			$this->contacts = new Contacts($res);
		$DBContact->close();
	}

	function delete()
	{
		try
		{

			$contact = $this->GetContact($_REQUEST['deleteId']);
			$DB = new ContactDatabase();
			$DB->open();
			$DB->startTransaction();
			$DBADR = new AdresseDatabase();
			$DBADR->deleteAdresse($contact->GetAdresseId());
			
			$DB->deleteContactEtablissement($contact->GetContactId());
			$DB->deleteContactAction($contact->GetContactId());
			$DB->deleteContactFicheSuivi($contact->GetContactId());
			$DB->deleteRemiseMaterielPeda($contact->GetContactId());
			$DB->deleteContact($contact->GetContactId());
			$DB->commitTransaction();
		}
		catch(Exception $e)
		{
			$DB->rollbackTransaction();
			$DB->close();
		}
		$DB->close();
		Mapping::RedirectTo('contact&delete=1');
	}

	function insert_updateContact()
	{
		//Si pas d'erreurs dans le formulaire
		$DB = new ContactDatabase();
		$DB->open();

		if($_POST['ignoreDoublon']==1 || count($this->contactsDoublons)<=0 || isset($_POST['updateForm']) )
		{
			try
			{
				//Constructions et insertions des Objets
				$DB->startTransaction();
				$adresse = new Adresse();
				$adrId = null;
				if(isset($_POST['adr_id']) &&!empty($_POST['adr_id']))
				$adrId = $_POST['adr_id'];

				$adresse->init($adrId, null, null, null, null, $_REQUEST['tel1'],
				$_REQUEST['tel2'], $_REQUEST['mail1'], $_REQUEST['mail2'],null,date('Y-m-d H:i:s'), $this->user->getUtilisateurId(),
				date('Y-m-d H:i:s'), $this->user->getUtilisateurId());

				$DBADR = new AdresseDatabase();
				$DBADR->open();
				$DBADR->startTransaction();
				if(isset($_POST['addForm']))
					$idAdresse = $DBADR->insertAdresse($adresse);
				else
					$idAdresse = $DBADR->updateAdresse($adresse);
					$DBADR->commitTransaction();
				$DBADR->close();
				
				$contact = new Contact();
					
				$contactId = null;
				if(isset($_POST['contact_id']) && !empty($_POST['contact_id']))
				$contactId = $_POST['contact_id'];
					
				$global = (isset($_POST["global_contact"]))?1:0;

				$contact->init($contactId, $_POST['nom_contact'],$_POST['prenom_contact'],$idAdresse, $_POST['genre'], $_POST['age'], /*$_POST['statut']*/null,$global, 1,
				date('Y-m-d H:i:s'), $this->user->getUtilisateurId(),
				date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
				if(isset($_POST['addForm']))
					$this->idContact = $DB->insertContact($contact);
				else
					$this->idContact = $DB->updateContact($contact);
					
				if(!isset($_POST['addForm']))
				{
					$DB->deleteContactEtablissement($this->idContact);
					$DB->deleteContactAction($this->idContact);
				}
				
				if(!isset($_POST['addForm']))
				{
					if(!empty ($_POST['origine_old']))
					$origine = $_POST['origine_old'];
					else
					$origine = getOrigine($this->user);
				}
				else
				$origine = getOrigine($this->user);
				
				//Contact-Etablissement
				if (isset($_POST["nbLignes"]) && $_POST["nbLignes"]>0)
				{
					for($i=0; $i<$_POST["nbLignes"]; $i++)
					{
						if(isset($_POST['etabId_'.$i]))
						{
							$contactEtablissement = new ContactEtablissement();
							$participation = $_POST['participation_'.$i];
							if($this->user->isOperateur())
							$participation = 1;

							$contactEtablissement->init(null, $_POST['etabId_'.$i], $this->idContact, $origine, $_POST['specialite_'.$i],  $_POST['titre_'.$i], $participation,
							date('Y-m-d H:i:s'), $this->user->getUtilisateurId(),date('Y-m-d H:i:s'), $this->user->getUtilisateurId());

							if(!isset($_POST['addForm']))
								$DB->insertContactEtablissement($contactEtablissement,false);
							else
								$DB->insertContactEtablissement($contactEtablissement);
						}
					}
				}
				//Contact global
				if(isset($_POST["global_contact"]))
				{
					if(!isset($_POST['addForm']))
					{
						if(!empty ($_POST['origine_global_old']))
							$origine = $_POST['origine_global_old'];
						else
							$origine = getOrigine($this->user);
					}
					else
					$origine = getOrigine($this->user);
					$contactEtablissement = new ContactEtablissement();
					$participation = 1;

					if(isset($_POST['participation_global']))
						$participation = $_POST['participation_global'];
					if($this->user->isOperateur())
						$participation = 1;

					$contactEtablissement->init(null,null, $this->idContact, $origine, $_POST['specialite_global'],  $_POST['titre_global'], $participation,
					date('Y-m-d H:i:s'), $this->user->getUtilisateurId(),date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
					$DB->insertContactEtablissement($contactEtablissement, true);
				}
					
				//Contact action
				if (isset($_POST['nbLignesAction']) && $_POST['nbLignesAction'] > 0)
				{
					for($i=0; $i<$_POST['nbLignesAction']; $i++)
					{
						if(isset($_POST['actionName_'.$i]))
						{
							$actionContact = new ActionContact();
							$actionContact->init($_POST['actionId_'.$i], $this->idContact, $_POST['roleAction_'.$i], date('Y-m-d H:i:s'), $this->user->getUtilisateurId(),date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
							$DB->insertContactAction($actionContact);
						}
					}
				}
				
				//Contact formatino
				if (isset($_POST['nbLignesFormation']) && $_POST['nbLignesFormation'] > 0)
				{
					for($i=0; $i<$_POST['nbLignesFormation']; $i++)
					{
						if(isset($_POST['formationName_'.$i]))
						{
							$actionContact = new ActionContact();
							$actionContact->init($_POST['formationId_'.$i], $this->idContact, $_POST['roleFormation_'.$i], date('Y-m-d H:i:s'), $this->user->getUtilisateurId(),date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
							$DB->insertContactAction($actionContact);
						}
					}
				}
					
				//Contact bourse
				if (isset($_POST['nbLignesBourse']) && $_POST['nbLignesBourse'] > 0)
				{
					for($i=0; $i<$_POST['nbLignesBourse']; $i++)
					{
						if(isset($_POST['bourseName_'.$i]))
						{
							$actionContact = new ActionContact();
							$actionContact->init($_POST['bourseId_'.$i], $this->idContact, $_POST['roleBourse_'.$i], date('Y-m-d H:i:s'), $this->user->getUtilisateurId(),date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
							$DB->insertContactAction($actionContact);
						}
					}
				}
					
				$DB->commitTransaction();
				
				if(isset($_POST['updateForm']))
				{
					Mapping::RedirectTo('contactDetail&id='.$this->idContact.'&updateContact=1');
				}
				
			}
			catch(Exception $e)
			{
				if(isset($_POST['addForm']))
				{
					$this->errors[]='Une erreur s\'est produite lors de l\'ajout du contact !'; 
				}
				$DB->rollbackTransaction();
			}
		}
		$DB->close();

	}

	function GetContact($id)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/contact_database_model_class.php');
		$db = new ContactDatabase();
		$db->open();
		$rs = $db->get($id);
		$db->close();
		$ets = new Contacts($rs);
		if($ets->count()>0)
			return $ets->items(0);
		else
			return null;
	}

	function setPostContact($contact)
	{
		$adr = $contact->GetAdresse();
		$etabContact = $contact->GetEtablissementsContacts();

		//V�rification du formulaire
		$_POST['nom_contact'] = $contact->GetNom();
		$_POST['prenom_contact'] = $contact->GetPrenom();
		$_POST['adr_id'] = $contact->GetAdresseId();
		$_POST['contact_id'] = $contact->GetContactId();
		$_POST['genre'] = $contact->getGenreId();
		$_POST['age'] = $contact->getAgeId();
		$_POST['statut'] = $contact->getStatutId();
		$_POST['tel1'] = $adr->GetTel1();
		$_POST['tel2'] = $adr->GetTel2();
		$_POST['mail1'] = $adr->GetEmail1();
		$_POST['mail2'] = $adr->GetEmail2();

		//ContactEtablissement
		$_POST["nbLignes"] = $etabContact->count();
		$_POST['etabIds'] = '';
		for($i=0; $i<$etabContact->count(); $i++)
		{
			if($etabContact->items($i)->GetEtablissementId() == null)
			{
				$_POST["global_contact"] = 1;
				$_POST['titre_global'] = $etabContact->items($i)->GetTitreContactId();
				$_POST['participation_global'] = $etabContact->items($i)->GetParticipationId();
				$_POST['specialite_global'] = $etabContact->items($i)->GetSpecialiteContactId();
				$_POST['origine_global_old'] = $etabContact->items($i)->GetOrigineContactId();
			}
			else
			{
				$_POST["etabName_".$i] = $etabContact->items($i)->GetEtablissement()->GetNom();
				$_POST["etabId_".$i] = $etabContact->items($i)->GetEtablissementId();
				$_POST['titre_'.$i] = $etabContact->items($i)->GetTitreContactId();
				$_POST['participation_'.$i] = $etabContact->items($i)->GetParticipationId();
				$_POST['specialite_'.$i] = $etabContact->items($i)->GetSpecialiteContactId();
				$_POST['origine_old'] = $etabContact->items($i)->GetOrigineContactId();
				$_POST['etabIds'] .= ($_POST['etabIds'] == '' ? '':';').$etabContact->items($i)->GetEtablissementId();
			}
		}

		$actionContact = $contact->GetActions();
		$_POST["nbLignesAction"] = $actionContact->count();
		$_POST['actionIds']='';
		for($i=0; $i<$actionContact->count(); $i++)
		{
			$_POST['actionName_'.$i] = $actionContact->items($i)->GetAction()->GetNom();
			$_POST['actionId_'.$i] = $actionContact->items($i)->GetAction()->GetActionId();
			$_POST['roleAction_'.$i] = $actionContact->items($i)->GetRoleContactActionId();
			$_POST['actionIds'] .= ($_POST['actionIds'] == '' ? '':';').$actionContact->items($i)->GetAction()->GetActionId();
		}

		$piContact = $contact->GetProjetsInnovants();
		$_POST["nbLignesBourse"] = $piContact->count();
		$_POST['bourseIds']='';
		for($i=0; $i<$piContact->count(); $i++)
		{
			$_POST['bourseName_'.$i] = $piContact->items($i)->GetAction()->GetNom();
			$_POST['bourseId_'.$i] = $piContact->items($i)->GetAction()->GetActionId();
			$_POST['roleBourse_'.$i] = $piContact->items($i)->GetRoleContactActionId();
			$_POST['bourseIds'] .= ($_POST['bourseIds'] == '' ? '':';').$piContact->items($i)->GetAction()->GetActionId();
		}
	}

	function SetCloseCallBack()
	{
		$closeCallBack = "none";
			
		if (isset($_REQUEST['close'])) $closeCallBack = $_REQUEST['close'];
		elseif (isset($_POST['CloseCallBack'])) $closeCallBack = $_POST['CloseCallBack'];

		return $closeCallBack;
	}
}

?>