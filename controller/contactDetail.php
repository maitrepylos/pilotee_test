<?php
    if(!$session->isLogged()) $session->redirect();

	REQUIRE_ONCE(SCRIPTPATH.'view/contactDetail_view_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/contact_database_model_class.php');
	
	$ctrl = new ContactDetailController($title, $shortcut, $menu, $login, $notices);
	$ctrl->Render();

	class ContactDetailController
	{
		private $view = null;
		private $title = null;
		private $notices = array();
		private $errors = array();
		private $id = 0; 
		
		public function __construct(&$title, &$shortcut, &$menu, &$login, &$notices)
		{
			$this->view = new ContactDetailView();			
			if(isset($_REQUEST['updateContact']))
			{
				$this->notices[]='Le contact � �t� modifi� avec succ�s !';
			}
			
			$this->title = &$title;
			$shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
			$this->view->Notices($this->notices);
			$menu = $this->view->createMenu();
			$login = $this->view->createLogin();
			$this->title = 'Contact : ';
		}
		
		public function Render()
		{
			if (isset($_REQUEST["id"]) && is_numeric($_REQUEST["id"]))
			{
				$this->id = $_REQUEST["id"];
				$this->RenderContact(); 
			}
			else
			{
				Mapping::RedirectTo('accueil');
			}
		}
		
		private function RenderContact()
		{
			$ets = $this->GetContact();
			if($ets->Count()>0)
			{
				$this->title .= $ets->items(0)->GetNom(). ' ' . $ets->items(0)->GetPrenom() ;
				$this->view->Render($ets->items(0));
			}
			else
			{
				$this->errors[]= 'Le contact n\'existe pas !';
				$this->view->Errors($this->errors);
			}
		}

		private function GetContact()
		{
			$db = new ContactDatabase();
			$db->open();
			$rs = $db->get($this->id);
			$db->close();
			
			return new Contacts($rs);
		}
	}

?>
	