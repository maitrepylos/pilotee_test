<?php
if(!$session->isLogged()) $session->redirect();

REQUIRE_ONCE(SCRIPTPATH.'view/entiteAdmin_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/entiteAdmin_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');

REQUIRE_ONCE(SCRIPTPATH.'domain/etablissement_searchresult_domain_class.php');

$ctrl = new EntiteAdminController($title, $shortcut, $menu, $login);
$ctrl->render();
?>

<?php
class EntiteAdminController
{
	private $view = null;
	private $title = null;
	private $user = null;
	private $notices = array();
	private $errors = array();
	private $id = null;

	public function __construct(&$title, &$shortcut, &$menu, &$login)
	{
		$this->user = Session::GetInstance()->GetCurrentUser();
		$this->view = new EntiteAdminView();
		$this->title = &$title;
		$shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$menu = $this->view->createMenu();
		$login = $this->view->createLogin();
		$this->title = 'Entit� administrative';
	}

	public function render()
	{
		if(isset($_REQUEST['action']))
		{
			
			if($_REQUEST['action'] == 'add')
			{
				
				$this->renderAdd();
			}
			
			elseif($_REQUEST['action'] == 'update')
			{
				$this->renderUpdate();
			}
			elseif($_REQUEST['action']=='delete')
			{
				$this->renderDelete();
			}
				
		}
		else
		{
			
		//	$_POST['swActif'] = 'on';
			$this->renderSearch();
		}
	}


	private function renderUpdate()
	{
		if(!isset($_POST['updateForm']))
		{

			if(isset($_REQUEST['updateId']) && is_numeric($_REQUEST['updateId']))
			{
				$this->id = $_REQUEST['updateId'];
				$entites = $this->getEntiteAdmin();
				if($entites->count()>0)
				{
					$this->setPostEntieAdmin($entites);
					$this->title .= ' : '.$entites->items(0)->getNom();
					$this->view->render(true);
				}
				else
				{
					$this->errors[]= 'L\' entit� administrative n\'existe pas !';
					$this->view->Errors($this->errors);					
				}
			}
			else
			{
				Mapping::RedirectTo('accueil');
			}
		}
		else
		{
			$this->insertUpdate();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->render(true);				
		}	
	}
	
	
	private function setPostEntieAdmin($entites)
	{
		$entite = $entites->items(0);
		$adr = $entite->getAdresse();
		$cp = $adr->GetCodepostal();  
		$_POST['idEntiteAdmin'] = $entite->getEntiteAdminId();
		$_POST["idAdresse"] = $entite->getAdresseId();
		$_POST['nom_entiteAdmin'] = $entite->getNom();
		if($entite->getSwActif()==1)
		{
			$_POST['swActif'] = 'on';
			$_POST['swInactif'] = null;
		}
		else
		{
			$_POST['swActif'] = null;
			$_POST['swInactif'] = 'on';
		}
		
		$_POST['rue'] = $adr->getRue();
		$_POST['cp'] = $cp->GetCodepostal();
		$_POST['localite'] = $adr->GetVille();
		$_POST['website'] = $entite->getWebsite();
		
		$_POST['fax'] = $adr->GetFax();
		$_POST['tel'] = $adr->GetTel1();
		$_POST['tel2'] = $adr->GetTel2();
		$_POST['mail'] = $adr->GetEmail1();
		$_POST['mail2'] = $adr->GetEmail2();
		 
	}
	
	private function getEntiteAdmin()
	{
		$db = new EntiteAdminDatabase();
		$db->open();
		$rs = $db->get($this->id, null);
		$db->close();
			
		return new EntiteAdmins($rs);
	}	

	private function renderDelete()
	{
		if(isset($_REQUEST['deleteId']) && is_numeric($_REQUEST['deleteId']))
		{
			try
			{
				$idEntite = $_REQUEST['deleteId'];
				
				$DB = new EntiteAdminDatabase();
				$DB->open();
				$entites = new EntiteAdmins($DB->get($idEntite));
				$DB->startTransaction();
					$DBADR = new AdresseDatabase();
						$DBADR->deleteAdresse($entites->items(0)->getAdresseId());				

				$DB->updateEtablissement($idEntite);
				$DB->delete($idEntite);
				$DB->commitTransaction();
				$DB->close();
				Mapping::RedirectTo('entiteAdmin&deleteEntite=0');

			}
			catch(Exception $e)
			{
				$DB->rollbackTransaction();
				$DB->close();
				Mapping::RedirectTo('entiteAdmin&deleteEntite=1');
			}
			
			
		}
		else
		{
			Mapping::RedirectTo('accueil');
		}
	}
	
	private function renderSearch()
	{
		if(isset($_REQUEST['deleteEntite']) && $_REQUEST['deleteEntite']=='0')
		{
			$this->notices[]='L\'entit� administrative a �t� supprim� avec succ�s!';
		}
		elseif(isset($_REQUEST['deleteEntite']) && $_REQUEST['deleteEntite']=='1')
		{
			$this->errors[]='Une erreur s\'est produite lors de la suppression de l\'entit� administrative !';
		}
		
		$this->view->Notices($this->notices);
		$this->view->Errors($this->errors);		
		$this->view->search();
		if(isset($_POST['searchForm']))
		{

			$nom = (isset($_POST['nom_entiteAdmin']))? $_POST['nom_entiteAdmin'] : null; 
			$db= new EntiteAdminDatabase();
			$db->open();
				$entites = new EtablissementSearchResults($db->search($nom, $this->GetSwActifCriteria()));
			$db->close();
			if($entites->Count()>0)
			{
				if($entites->Count()==1 && $_REQUEST['module']!='entiteAdminPopup')
					Mapping::RedirectTo('entiteAdminDetail&id='.$entites->items(0)->getId());
				$this->view->renderList($entites);
			}
			else
			{
				$this->view->NoResult();
			}
		}
	}

	
	private function renderAdd()
	{
		if(isset($_POST['addForm']))
		{
			$this->insertUpdate();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->render();
		}
		else
		{
			$this->initialize();
			$this->view->render();
		}
	}
	private function initialize()
	{
		$_POST['swActif'] = 'on';
	}
	
	private function GetSwActifCriteria()
	{
		$swActif = null;

		if ($this->user->isOperateur()) $swActif = 1;
		else
		{
			if ((isset($_POST['swActif'])) && (isset($_POST['swInactif']))) $swActif = null;
			elseif (isset($_POST['swActif'])) $swActif = 1;
			elseif (isset($_POST['swInactif'])) $swActif = 0;
		}
			
		return $swActif;
	}	
	
	private function insertUpdate()
	{
		//R�cup�ration des param�tres
		$id = (isset($_POST['idEntiteAdmin']))? $_POST['idEntiteAdmin'] : null;
		$idAdresse = (isset($_POST["idAdresse"]))? $_POST["idAdresse"] : null; 
		$nom = (isset($_POST['nom_entiteAdmin']))? $_POST['nom_entiteAdmin'] : null; 
		$rue = (isset($_POST['rue']))? $_POST['rue'] : null; 
		$localite = (isset($_POST['localite']))? $_POST['localite'] : null; 
		$cp = (isset($_POST['cp']))? $_POST['cp'] : null;
		$website = (isset($_POST['website']))? $_POST['website'] : null;
		$fax = (isset($_POST['fax']))? $_POST['fax'] : null;
		$tel = (isset($_POST['tel']))? $_POST['tel'] : null;
		$tel2 = (isset($_POST['tel2']))? $_POST['tel2'] : null;
		$mail = (isset($_POST['mail']))? $_POST['mail'] : null;
		$mail2 = (isset($_POST['mail2']))? $_POST['mail2'] : null;		
		
	
		$db= new EntiteAdminDatabase();
		try
		{
			$db->open();
			$db->startTransaction();
			$codePostals = Dictionnaries::GetCodePostals($cp);
			$provinceId = null;
			if($codePostals->items(0))
			{
				$provinceId = $codePostals->items(0)->GetProvinceId();
			}
			
			$adresse = new Adresse();
			$adresse->init($idAdresse, $provinceId, $rue, $cp, $localite, $tel, $tel2, $mail, $mail2,$fax,date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
			
			$DBADR = new AdresseDatabase();
				$DBADR->open();
				$DBADR->startTransaction();
				if($_REQUEST['action'] == 'update')
					$idAdresse = $DBADR->updateAdresse($adresse);
				else
					$idAdresse = $DBADR->insertAdresse($adresse);
				$DBADR->commitTransaction();
			$DBADR->close();			
			
			$entite  = new EntiteAdmin();
			$entite->init($id, $nom, $idAdresse, $website, $this->GetSwActifCriteria(),date('Y-m-d H:i:s'),$this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
			if($_REQUEST['action'] == 'update')
				$db->update($entite);
			else
				$db->insert($entite);
			$db->commitTransaction();
			$db->close();

			
			if($_REQUEST['action'] == 'update')
				Mapping::RedirectTo('entiteAdminDetail&id='.$id.'&updateEntite=0');
			
			$this->notices[]='L\'entit� administrative a �t� ajout� avec succ�s !';
		}
		catch(Exception $e)
		{

			if(isset($_POST['updateForm']))
			{
				Mapping::RedirectTo('entiteAdminDetail&id='.$id.'&updateEntite=1');
			}
			else			
				$this->error[]='Une erreur s\'est produite lors de l\'ajout de l\' entit� administrative  !';
			$db->rollbackTransaction();
			$db->close();
		}	
		

	}
	

}
?>

