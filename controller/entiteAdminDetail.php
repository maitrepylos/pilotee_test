<?php
	if(!$session->isLogged()) $session->redirect();
	
	REQUIRE_ONCE(SCRIPTPATH.'view/entiteAdminDetail_view_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/entiteAdmin_database_model_class.php');
	
	$ctrl = new EntiteAdminDetailController($title, $shortcut, $menu, $login);
	$ctrl->Render();
?>

<?php
	class EntiteAdminDetailController
	{
		private $view = null;
		private $title = null;
		private $entite = null;
		private $notices = array();
		private $errors = array();
		//updateEntite
		public function __construct(&$title, &$shortcut, &$menu, &$login)
		{
			$this->redirectIfNeeded();
			$this->view = new EntiteAdminDetailView();
			
			$this->title = &$title;
			$shortcut = $this->view->raccourcisPanel('entiteAdmin');
			$menu = $this->view->createMenu();
			$login = $this->view->createLogin();
			
			$this->title = 'Entit� administrative : ';
		}
		
		private function redirectIfNeeded()
		{
			$user = Session::GetInstance()->getCurrentUser();
			if (!$user->isAdmin()) Mapping::RedirectTo('accueil');
			if (!isset($_REQUEST["id"])) Mapping::RedirectTo('accueil');
			if (!is_numeric($_REQUEST["id"])) Mapping::RedirectTo('accueil');
			$this->id = $_REQUEST["id"];
		}
		
		public function Render()
		{
			$this->RenderEntiteAdmin(); 
		}
		
		private function RenderEntiteAdmin()
		{
			
			$this->entite = $this->GetEntiteAdmin();

			if ($this->entite->count() > 0)
			{
				if(isset($_REQUEST['updateEntite']) && $_REQUEST['updateEntite']=='0')
					$this->notices[] = 'La mise � jour de l\'entit� administrative a �t� effectu� avec succ�s !';
				else if(isset($_REQUEST['updateEntite']) && $_REQUEST['updateEntite']=='1')
					$this->errors[] = 'Une erreur s\'est produite lors de la mise � jour de l\'entit� administrative !';
				
				$this->title .= $this->entite->items(0)->getNom();
					
				$this->view->Notices($this->notices);
				$this->view->Errors($this->errors);
				$this->view->Render($this->entite->items(0));
				
			}
			else
			{
				$this->errors[]= 'L\' entit� administrative n\'existe pas !';
				$this->view->Errors($this->errors);
			}
			
		}

		private function GetEntiteAdmin()
		{
			if (isset($this->entite)) return $this->entite;
			
			$db = new EntiteAdminDatabase();
			$db->open();
			$rs = $db->get($this->id);
			$db->close();
			
			return new EntiteAdmins($rs);
		}

		
	}
?>

