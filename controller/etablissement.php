<?php
	if(!$session->isLogged()) $session->redirect();

	REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'domain/etablissement_searchresult_domain_class.php');
		
	REQUIRE_ONCE(SCRIPTPATH.'model/ase_database_model_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/etablissement_database_model_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');
	
	REQUIRE_ONCE(SCRIPTPATH.'view/etablissement_view_class.php');
	
	$view = new EtablissementView();

	if ($_REQUEST['module'] == 'etablissement')
	{
		$title = "Etablissements d'enseignement";
		$shortcut = $view->raccourcisPanel($_REQUEST['module']);
		$menu = $view->createMenu();
		$login = $view->createLogin();
	}

	$ec = new EtablissementController($view);
	$ec->Render();
?>

<?php
	class EtablissementController
	{
		private $view;
		private $user;
		private $id;
		private $title;
		private $notices=array();
		private $errors=array();
		
		public function __construct($view)
		{
			 $this->view = $view;
			 
			 $this->user = Session::GetInstance()->getCurrentUser();
			 
			 if (isset($_POST['SubmitAction']) && $_POST['SubmitAction'] == 'updateagent')
			 {
			 	$this->updateAgent();
			 	$_POST['SubmitAction'] = 'search';
			 }
		}
		
		private function updateAgent()
		{
			$agentId = $_POST['AgentId'];
			$ets = $_POST['EtsForAgent'];
	
			$db = new EtablissementDatabase();
			$db->open();
			$rs = $db->SetAgent($ets, $agentId);
			$db->close();
		}
		
		public function Render()
		{
			if(!isset($_REQUEST['action']))
			{
				if (!isset($_POST['SubmitAction']))
				{
					$this->Initialize();
					$this->view->Search($this->SetCloseCallBack());
				}
	
				if (isset($_POST['SubmitAction']))
				{
					if ($_POST['SubmitAction'] == 'search') $this->view->Search($this->SetCloseCallBack());
					
					$this->OnSubmit();
				}
			}
			else
			{
				if($_REQUEST['action'] == 'add')
				{
					$this->renderAdd();
				}
				elseif($_REQUEST['action'] == 'update')
				{
					$this->renderUpdate();
				}
			}
		}
		
		private function renderUpdate()
		{
			if(!isset($_POST['updateForm']))
			{
				if(isset($_REQUEST['updateId']) && is_numeric($_REQUEST['updateId']))
				{
					$this->id = $_REQUEST['updateId'];
					$etabs = $this->getEtablissement();
					if($etabs->count()>0)
					{
						$this->setPostEtablissment($etabs);
						$this->view->render(true);
					}
					else
					{
						$this->errors[]= 'L\' �tablissement n\'existe pas !';
						$this->view->Errors($this->errors);					
					}
				}
				else
				{
					Mapping::RedirectTo('accueil');
				}
				
			}
			else
			{
				$this->insertUpdate();
				$this->view->Notices($this->notices);
				$this->view->Errors($this->errors);
				$this->view->render(true);				
			}			
		}
		
		private function getEtablissement()
		{
			$db = new EtablissementDatabase();
			$db->open();
			$rs = $db->GetById($this->id);
			$db->close();
			
			return new Etablissements($rs);			
		}
		
		private function setPostEtablissment($etabs)
		{
			$etab = $etabs->items(0);
			$adr = $etab->GetAdresse();
			$cp = $adr->GetCodepostal();  
			$_POST['idEtablissement'] = $etab->getEtablissementId();
			$_POST["idAdresse"] = $etab->getAdresseId();
			
			$_POST['entiteId'] = $etab->GetEntiteAdminId();
			$_POST['entite'] = $etab->GetEntiteAdminLabel();
			
			$_POST['nom'] = $etab->getNom();
			if($etab->getSwActif()==1)
			{
				$_POST['swActif'] = 'on';
				$_POST['swInactif'] = null;
			}
			else
			{
				$_POST['swActif'] = null;
				$_POST['swInactif'] = 'on';
			}
			$_POST['niveau'] = $etab->GetNiveauEtablissementId();
			$_POST['reseau'] = $etab->GetReseauEtablissementId();
			$_POST['rue'] = $adr->getRue();
			$_POST['cp'] = isset($cp)?$cp->GetCodepostal():'';
			$_POST['localite'] = $adr->GetVille();
			$_POST['website'] = $etab->getWebsite();
			
			$_POST['fax'] = $adr->GetFax();
			$_POST['tel'] = $adr->GetTel1();
			$_POST['tel2'] = $adr->GetTel2();
			$_POST['mail'] = $adr->GetEmail1();
			$_POST['mail2'] = $adr->GetEmail2();			
		}
		
		private function renderAdd()
		{
			if(isset($_POST['addForm']))
			{
				$this->insertUpdate();
				$this->view->Notices($this->notices);
				$this->view->Errors($this->errors);
				$this->view->render();
			}
			else
			{
				$this->initialize();
				$this->view->render();
			}			
		}
		
		private function insertUpdate()
		{
			//R�cup�ration des param�tres
			$id = (isset($_POST['idEtablissement']))? $_POST['idEtablissement'] : null;
			$idAdresse = (isset($_POST["idAdresse"]))? $_POST["idAdresse"] : null; 
			$nom = (isset($_POST['nom']))? $_POST['nom'] : null; 
			$rue = (isset($_POST['rue']))? $_POST['rue'] : null; 
			$localite = (isset($_POST['localite']))? $_POST['localite'] : null; 
			$cp = (isset($_POST['cp']))? $_POST['cp'] : null;
			$website = (isset($_POST['website']))? $_POST['website'] : null;
			$fax = (isset($_POST['fax']))? $_POST['fax'] : null;
			$tel = (isset($_POST['tel']))? $_POST['tel'] : null;
			$tel2 = (isset($_POST['tel2']))? $_POST['tel2'] : null;
			$mail = (isset($_POST['mail']))? $_POST['mail'] : null;
			$mail2 = (isset($_POST['mail2']))? $_POST['mail2'] : null;	
			$niveau = (isset($_POST['niveau']))? $_POST['niveau'] : null;
			$reseau = (isset($_POST['reseau']))? $_POST['reseau'] : null;
			$entite = (isset($_POST['entite']))? $_POST['entite'] : null;
			$entiteId = (isset($_POST['entiteId']))? $_POST['entiteId'] : null;
			
			//$user = Session::GetInstance()->GetCurrentUser();
			$db= new EtablissementDatabase();
			try
			{
				$db->open();
				$db->startTransaction();
				$codePostals = Dictionnaries::GetCodePostals($cp);
				$provinceId = null;
				if($codePostals->items(0))
				{
					$provinceId = $codePostals->items(0)->GetProvinceId();
				}
				
				$adresse = new Adresse();
				$adresse->init($idAdresse, $provinceId, $rue, $cp, $localite, $tel, $tel2, $mail, $mail2,$fax,date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
				
				$DBADR = new AdresseDatabase();
					$DBADR->open();
					$DBADR->startTransaction();
					if($_REQUEST['action'] == 'update')
						$idAdresse = $DBADR->updateAdresse($adresse);
					else
						$idAdresse = $DBADR->insertAdresse($adresse);
					$DBADR->commitTransaction();
				$DBADR->close();			
				
				
				$etab  = new Etablissement();
				$etab->init($id, null, $nom, $idAdresse, $website, $niveau, $reseau, $entiteId, $this->GetSwActifCriteria(), date('Y-m-d H:i:s'),$this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
								
				if($_REQUEST['action'] == 'update')
					$db->update($etab);
				else
					$db->insert($etab);
				$db->commitTransaction();
				$db->close();
	
				
				if($_REQUEST['action'] == 'update')
					Mapping::RedirectTo('etablissementDetail&id='.$id.'&updateEtab=0');
				
				
				$this->notices[]='L\'�tablissement a �t� ajout� avec succ�s !';
			}
			catch(Exception $e)
			{
				
				if(isset($_POST['updateForm']))
				{
					Mapping::RedirectTo('etablissementDetail&id='.$id.'&updateEtab=1');
				}
				else							
					$this->error[]='Une erreur s\'est produite lors de l\'ajout de l\' �tablissement !';
					
				$db->rollbackTransaction();
				$db->close();
			}	
		
				
		}
		
		
		private function Initialize()
		{
			$_POST['swActif'] = 'on';
			$_POST['swOnlyMyPortFolio'] = 'on';
		}
		
		private function SetCloseCallBack()
		{
			$closeCallBack = "none";
			
			if (isset($_REQUEST['close'])) $closeCallBack = $_REQUEST['close'];
			elseif (isset($_POST['CloseCallBack'])) $closeCallBack = $_POST['CloseCallBack'];
		
			return $closeCallBack;
		}
		
		private function OnSubmit()
		{
			if ($_POST['SubmitAction'] == 'search') $this->Search();
		}
		
		private function Search()
		{
			$nom = ((isset($_POST['nom_etablissement'])) && (strlen($_POST['nom_etablissement']) > 0)) ? $_POST['nom_etablissement'] : null;  
			$swActif = $this->GetSwActifCriteria();
			$utilisateurId = $this->GetUtilisateurCriteria();
			$cp_1 = ((isset($_POST['code_postal_1']) && (strlen($_POST['code_postal_1']) > 0))) ? $_POST['code_postal_1'] : null; 
			$cp_2 = ((isset($_POST['code_postal_2']) && (strlen($_POST['code_postal_2']) > 0))) ? $_POST['code_postal_2'] : null;

			$arrondissementId = ((isset($_POST['arrondissement'])) && ($_POST['arrondissement'] != '-1')) ? $_POST['arrondissement'] : null;
			$provinceId = ((isset($_POST['province'])) && ($_POST['province']  != '-1')) ? $_POST['province'] : null;
			$reseauId = ((isset($_POST['reseau'])) && ($_POST['reseau']  != '-1')) ? $_POST['reseau'] : null;
			$niveauId = ((isset($_POST['niveau'])) && ($_POST['niveau']  != '-1')) ? $_POST['niveau'] : null;

			$ets = $this->PerformSearch($swActif, $utilisateurId, $nom, $cp_1, $cp_2, $arrondissementId, $provinceId, $reseauId, $niveauId);

			if ($_REQUEST['module'] == 'etablissement')
			{
				if ($ets->count() < 1) $this->view->NoResult();
				elseif ($ets->count() > 1) $this->view->RenderList($ets);
				else Mapping::RedirectTo('etablissementDetail', '&id=' . $ets->items(0)->getId());
			}
			else if ($_REQUEST['module'] == 'etablissementPopup')
			{
				if ($ets->count() < 1) $this->view->NoResult();
				else $this->view->RenderList($ets);
			}
		}
		
		private function GetSwActifCriteria()
		{
			$swActif = null;

			if ($this->user->isOperateur()) $swActif = 1;
			else
			{
				if ((isset($_POST['swActif'])) && (isset($_POST['swInactif']))) $swActif = null;
				elseif (isset($_POST['swActif'])) $swActif = 1;
				elseif (isset($_POST['swInactif'])) $swActif = 0;
			}
			
			return $swActif;
		}
		
		private function GetUtilisateurCriteria()
		{
			$utilisateurId = null;
			if (($this->user->isAdmin()) || ($this->user->isCoordinateur())  || ($this->user->isAgentCoordinateur()) )
			{
				if (isset($_POST['agent']))
				{
					if ($_POST['agent'] == -2) $utilisateurId = 0;
					elseif ($_POST['agent'] > 0) $utilisateurId = $_POST['agent'];
				}
			}
			elseif ($this->user->isAgent())
			{
				if (isset($_POST['swOnlyMyPortFolio'])) $utilisateurId = $this->user->getUtilisateurId();
			}
			
			return $utilisateurId;
		}
		
		function PerformSearch($swActif, $utilisateurId, $nom, $cp_1, $cp_2, $arrondissementId, $provinceId, $reseauId, $niveauId)
		{
			$db = new EtablissementDatabase();
			$db->open();
			$rs = $db->get($swActif, $utilisateurId, $nom, $cp_1, $cp_2, $provinceId, $arrondissementId, $niveauId, $reseauId);
			$db->close();
			
			return new EtablissementSearchResults($rs);
		}
	} 
?>
