<?php
	if(!$session->isLogged()) $session->redirect();

	/*
	REQUIRE_ONCE(SCRIPTPATH.'domain/etablissement_domain_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/etablissement_database_model_class.php');
	*/
	
	REQUIRE_ONCE(SCRIPTPATH.'view/etablissementDetail_view_class.php');
	
	
	//REQUIRE_ONCE(SCRIPTPATH.'domain/etablissement_searchresult_domain_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/etablissement_database_model_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/ficheSuivi_database_model_class.php');
	
	$ctrl = new EtablissementDetailController($title, $shortcut, $menu, $login);
	$ctrl->Render();
?>

<?php
	class EtablissementDetailController
	{
		private $view = null;
		private $title = null;
		private $notices = array();
		private $errors = null;
		private $id = 0; 
		
		public function __construct(&$title, &$shortcut, &$menu, &$login)
		{
			if (isset($_REQUEST["id"]))
			{
				$this->id = $_REQUEST["id"];
				
				if (!isset($this->id) || !is_numeric($this->id))
				{
					Mapping::RedirectTo('accueil');
					$this->id = null;
				}
			}

			$this->view = new EtablissementDetailView();
			
			if (isset($_POST['SubmitAction'])) $this->OnSubmit();
						
			$this->title = &$title;
			$shortcut = $this->view->raccourcisPanel('etablissementDetail');
			$menu = $this->view->createMenu();
			$login = $this->view->createLogin();
			$this->errors = array();
			$this->title = 'Etablissement : ';
			
			if(isset($_REQUEST['updatefiche']))
			{
				if($_REQUEST['updatefiche']=='0')
					$this->notices[] = 'La fiche de suivi a �t� mise � jour avec succ�s !';
				elseif($_REQUEST['updatefiche']=='1')
					$this->errors[] = 'Une erreur s\'est produite lors de la mise � jour de la fiche de suivi';
			}
			elseif(isset($_REQUEST['addFiche']))
			{
				$this->notices[]='La fiche de suivi a �t� ajout�e avec succ�s !';	
			}
			elseif(isset($_REQUEST['addMateriel']))
			{
				$this->notices[]='La remise de mat�riel a �t� ajout�e avec succ�s !';				
			}
			else if(isset($_REQUEST['updatemateriel']))
			{
				if($_REQUEST['updatemateriel']=='0')
					$this->notices[] = 'La fiche de remise de mat�riel a �t� mise � jour avec succ�s !';
				elseif($_REQUEST['updatemateriel']=='1')
					$this->errors[] = 'Une erreur s\'est produite lors de la mise � jour de la fiche de remise de mat�riel';
			}
			elseif(isset($_REQUEST['deleteFiche']))
			{
				if($_REQUEST['deleteFiche']=='0')
					$this->notices[] = 'La fiche de suivi a �t� supprim� avec succ�s !';
				elseif($_REQUEST['deleteFiche']=='1')
					$this->errors[] = 'Une erreur s\'est produite lors de la suppression de la fiche de suivi';
				
			}
			elseif(isset($_REQUEST['deletemateriel']))
			{
				if($_REQUEST['deletemateriel']=='0')
					$this->notices[] = 'La fiche de remise de mat�riel a �t� supprim� avec succ�s !';
				elseif($_REQUEST['deletemateriel']=='1')
					$this->errors[] = 'Une erreur s\'est produite lors de la suppression de la fiche de remise de mat�riel';
			}
				
			
		}
		
		public function OnSubmit()
		{
			if ($_POST['SubmitAction'] == 'UPDATEAGENT')
			{
				if (isset($this->id) && isset($_POST['AgentId']))
				{
					$agentId = $_POST['AgentId'];

					$db = new EtablissementDatabase();
					$db->open();
					$rs = $db->SetAgent($this->id, $agentId);
					$db->close();
				}
			}	
		}
		
		public function Render()
		{
			if (isset($this->id)) $this->RenderEtablissement(); 
		}
		
		private function RenderEtablissement()
		{
			$ets = $this->GetEtablissement();

			if ($ets->count() > 0)
			{
				$this->title .= $ets->items(0)->GetNom();
				$this->view->Notices($this->notices);
				$this->view->Errors($this->errors);
				$this->view->Render($ets->items(0));
			}
			else
			{
				$this->errors[]= 'L\'�tablissement n\'existe pas !';
				$this->view->Errors($this->errors);
			}
		}

		private function GetEtablissement()
		{
			$db = new EtablissementDatabase();
			$db->open();
			$rs = $db->GetById($this->id);
			$db->close();
			
			return new Etablissements($rs);
		}
	}
?>
