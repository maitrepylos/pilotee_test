<?php

$session = Session::GetInstance();

if(isset($_REQUEST['type']))
{
	if($_REQUEST['type']=='encodage')
	{
		$histo = $session->get('historique');
		$typeObjet = $session->get('typeObjet');
		$typeEncodage = $session->get('typeEncodage');
		$dateEncodage = $session->get('dateEncodage');
		REQUIRE_ONCE(SCRIPTPATH.'view/excel/rapport_encodage_view.php');
	}
	elseif($_REQUEST['type']=='action')
	{
		$actions = $session->get('actions');
		$postAction = $session->get('PostAction');
		REQUIRE_ONCE(SCRIPTPATH.'view/excel/rapport_action_view.php');
	}
	elseif($_REQUEST['type']=='bourse')
	{
		$actions = $session->get('bourses');
		$postAction = $session->get('PostBourse');
		REQUIRE_ONCE(SCRIPTPATH.'view/excel/rapport_bourse_view.php');
	}
	elseif($_REQUEST['type']=='etablissement')
	{
		$etablissements = $session->get('etablissementsRapport');
		$postEtab = $session->get('PostEtab');
		REQUIRE_ONCE(SCRIPTPATH.'view/excel/rapport_etablissement_view.php');
	}
	elseif($_REQUEST['type']=='journal')
	{
		$fiches = $session->get('journal');
		$postJ = $session->get('PostJournal');
		REQUIRE_ONCE(SCRIPTPATH.'view/excel/rapport_journal_view.php');
	}
	elseif($_REQUEST['type']=='contact')
	{

		$res = $session->get('contact');
		REQUIRE_ONCE(SCRIPTPATH.'view/excel/rapport_contact_view.php');

		$session->set('contact', $res);
	}
	elseif($_REQUEST['type']=='listing_etablissement')
	{

		$ets = $session->get('letab');
		$postEtab = $session->get('PostLEtab');
		REQUIRE_ONCE(SCRIPTPATH.'view/excel/listing_etablissements_view.php');

//		$session->set('contact', $res);
	}
	elseif($_REQUEST['type']=='listing_action')
	{

		$ets = $session->get('letab');
		$postEtab = $session->get('PostLEtab');
		REQUIRE_ONCE(SCRIPTPATH.'view/excel/listing_actions_view.php');

//		$session->set('contact', $res);
	}
	elseif($_REQUEST['type']=='listing_contact')
	{

		$ets = $session->get('letab');
		$postEtab = $session->get('PostLEtab');
		REQUIRE_ONCE(SCRIPTPATH.'view/excel/listing_contacts_view.php');

//		$session->set('contact', $res);
	}
	elseif($_REQUEST['type']=='listing_formation')
	{

		$ets = $session->get('letab');
		$postEtab = $session->get('PostLEtab');
		REQUIRE_ONCE(SCRIPTPATH.'view/excel/listing_formations_view.php');

//		$session->set('contact', $res);
	}
}
else
{
	Mapping::RedirectTo('report');
}
?>