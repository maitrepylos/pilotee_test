<?php
	if(!$session->isLogged()) $session->redirect();
	
	REQUIRE_ONCE(SCRIPTPATH.'view/ficheSuiviDetail_view_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/ficheSuivi_database_model_class.php');
	
	$ctrl = new FicheSuiviController($title, $shortcut, $menu, $login);
	$ctrl->Render();
?>

<?php
	class FicheSuiviController
	{
		private $view = null;
		private $title = null;
		private $fs = null;
		private $notices = array();
		private $errors = array();
		
		public function __construct(&$title, &$shortcut, &$menu, &$login)
		{
			$this->redirectIfNeeded();
			$this->view = new FicheSuiviDetailView();
			
			$this->title = &$title;
			$shortcut = $this->view->raccourcisPanel('ficheSuiviDetail');
			$menu = $this->view->createMenu();
			$login = $this->view->createLogin();
			
			$this->title = 'Fiche suivi n� ';
		}
		
		private function redirectIfNeeded()
		{
			$user = Session::GetInstance()->getCurrentUser();

			if ($user->isOperateur()) Mapping::RedirectTo('accueil');
			if (!isset($_REQUEST["id"])) Mapping::RedirectTo('accueil');
			if (!is_numeric($_REQUEST["id"])) Mapping::RedirectTo('accueil');
			
			$this->id = $_REQUEST["id"];
		}
		
		public function Render()
		{
			$this->RenderFicheSuivi(); 
		}
		
		private function RenderFicheSuivi()
		{
			$this->fs = $this->GetFicheSuivi();

			if ($this->fs->count() > 0)
			{
				$this->title .= $this->fs->items(0)->GetFicheSuiviId();
				if(isset($_REQUEST['updatefiche']) && $_REQUEST['updatefiche']=='0')
					$this->notices[] = 'La mise � jour de la fiche a �t� effectu� avec succ�s !';
				elseif(isset($_REQUEST['updatefiche']) && $_REQUEST['updatefiche']=='1')
					$this->errors[] = 'Une erreur s\'est produite lors de la mise � jour de la fiche de suivi !';

				$this->view->Notices($this->notices);
				$this->view->Errors($this->errors);
				$this->view->Render($this->fs->items(0));
			}
			else
			{
				$this->errors[]= 'La fiche de suivi n\'existe pas !';
				$this->view->Errors($this->errors);
			}
		}

		private function GetFicheSuivi()
		{
			if (isset($this->fs)) return $this->fs;
			
			$db = new FicheSuiviDatabase();
			$db->open();
			$rs = $db->get($this->id, null);
			$db->close();
			
			return new FicheSuivis($rs);
		}
	}
?>

