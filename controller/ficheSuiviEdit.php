<?php
if(!$session->isLogged()) $session->redirect();

REQUIRE_ONCE(SCRIPTPATH.'view/ficheSuiviEdit_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/ficheSuivi_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/ficheSuivi_contact_database_model_class.php');

$ctrl = new FicheSuiviEditController($title, $shortcut, $menu, $login);
$ctrl->render();
?>

<?php
class FicheSuiviEditController
{
	private $view = null;
	private $title = null;
	private $user = null;
	private $notices = array();
	private $errors = array();
	private $id = null;

	public function __construct(&$title, &$shortcut, &$menu, &$login)
	{
		$this->redirectIfNeeded();
		$this->user = Session::GetInstance()->GetCurrentUser();
		$this->view = new FicheSuiviEditView();
		$this->title = &$title;
		$shortcut = $this->view->raccourcisPanel();
		$menu = $this->view->createMenu();
		$login = $this->view->createLogin();
		$this->title = 'Fiche suivi n� ';
	}


	private function redirectIfNeeded()
	{
		$user = Session::GetInstance()->getCurrentUser();

		if ($user->isOperateur()) Mapping::RedirectTo('accueil');
	}
	
	private function SetCloseCallBack()
	{
		$closeCallBack = "none";
		
		if (isset($_REQUEST['close'])) $closeCallBack = $_REQUEST['close'];
		elseif (isset($_POST['CloseCallBack'])) $closeCallBack = $_POST['CloseCallBack'];
		
		return $closeCallBack;
	}
	
	public function render()
	{
		if(isset($_REQUEST['action']))
		{
			
			if($_REQUEST['action'] == 'add')
			{
				$this->renderAdd();
			}
			elseif($_REQUEST['action'] == 'update')
			{
				$this->renderUpdate();
			}
			elseif($_REQUEST['action']=='delete')
			{
				$this->renderDelete();
			}	
		}
	}

	private function renderUpdate()
	{
		if(isset($_POST['updateForm']))
		{
			$this->insertUpdate();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->render();
		}
		else
		{
			if(isset($_REQUEST['updateId']) && isset($_REQUEST['etabId']) && is_numeric($_REQUEST['etabId']) && is_numeric($_REQUEST['updateId']))
			{
				$this->id = $_REQUEST['updateId'];
				$fiche = $this->GetFicheSuivi();
				if($fiche->count()>0)
				{
					$this->title .= $fiche->items(0)->GetFicheSuiviId();
					$this->setPostFiche($fiche);
					$this->view->Notices($this->notices);
					$this->view->Errors($this->errors);
					$this->view->render(true);
				}
				else
				{
					$this->errors[]= 'La fiche de suivi n\'existe pas !';
					$this->view->Errors($this->errors);
				}
			}
			else
			{
				Mapping::RedirectTo('accueil');
			}
		}
	}

	private function setPostFiche($fiche)
	{
		$_POST['ficheSuiviId'] = $fiche->items(0)->GetFicheSuiviId(); 
		$_POST['comments'] = $fiche->items(0)->GetCommentaire();
		$_POST['dateRencontre'] = $fiche->items(0)->GetDateRencontre(true);
		$_POST['modeSuivi'] = $fiche->items(0)->GetModeSuiviId();
		$_POST['timingSuivi'] = $fiche->items(0)->GetTimingSuiviId();
		$_POST['intentionSuivi'] = $fiche->items(0)->GetIntentionSuiviId();
		//Contacts
		$contacts = $fiche->items(0)->GetContactList();
		$_POST['nbLignesContact'] = $contacts->count();
		$_POST['contactIds'] = '';
		for($i=0; $i<$contacts->count(); $i++)
		{
			$_POST['contactIds'] .= ($_POST["contactIds"] == '' ? '':';'). $contacts->items($i)->GetContactId();
			$_POST['contactName_'.$i] = $contacts->items($i)->GetNom(). ' ' .$contacts->items($i)->GetPrenom();
			$_POST['contactId_'.$i] = $contacts->items($i)->GetContactId();
		}
		$_POST['etablissementId'] = $fiche->items(0)->GetEtablissementId();
		$dbEtab = new EtablissementDatabase();
		$dbEtab->open();
		$rs = $dbEtab->GetById($fiche->items(0)->GetEtablissementId());
		$etab= new Etablissements($rs);
		$dbEtab->close();
		$_POST['etablissementNom'] = $etab->items(0)->GetNom();
		
		
	}
	
	private function renderDelete()
	{
		if(isset($_REQUEST['deleteId']) && is_numeric($_REQUEST['deleteId']))
		{
			try
			{
				$idFiche = $_REQUEST['deleteId'];
				$DB = new FicheSuiviDatabase();
				$DB->open();
				$DB->startTransaction();

				$DB->deleteFicheContact($idFiche);
				$DB->delete($idFiche);
				$DB->commitTransaction();
				$DB->close();
				Mapping::RedirectTo('etablissementDetail&id='.$_REQUEST['etab'].'&deleteFiche=0');

			}
			catch(Exception $e)
			{
				$DB->rollbackTransaction();
				$DB->close();
				Mapping::RedirectTo('etablissementDetail&id='.$_REQUEST['etab'].'&deleteFiche=1');
			}
			
			
		}
		else
		{
			Mapping::RedirectTo('accueil');
		}
	}
	
	private function renderAdd()
	{
		if(isset($_POST['addForm']))
		{
			$this->insertUpdate();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->render();
		}
		else
		{
			$this->initialize();
			$this->view->render();
		}
	}
	private function initialize()
	{
		$_POST['dateRencontre'] = date("d/m/Y");
	}
	
	private function insertUpdate()
	{
		//R�cup�ration des param�tres
		$etabId = $_REQUEST['etabId'];
		$date = (isset($_POST['dateRencontre']))? $_POST['dateRencontre'] : null; 
		$modeSuivi = (isset($_POST['modeSuivi']))?$_POST['modeSuivi']:null; 
		$timingSuivi = (isset($_POST['timingSuivi']))?$_POST['timingSuivi']:null;
		$intentionSuivi = (isset($_POST['intentionSuivi']))? $_POST['intentionSuivi']:null;
		$comments = (isset($_POST['comments']))?$_POST['comments']:null; 
		$nbContacts = !empty($_POST['nbLignesContact']) ?$_POST['nbLignesContact'] : null;
		$contacts = array();
		
		for($i=0; $i<$nbContacts; $i++)
		{
			if(isset($_POST['contactId_'.$i]))
				$contacts[] = array("id"=>$_POST['contactId_'.$i]);
		}
		$db= new FicheSuiviDatabase();
		try
		{
			$db->open();
			$db->startTransaction();
			$fiche = new FicheSuivi();
			$ficheId = (isset($_POST['ficheSuiviId']))?$_POST['ficheSuiviId']:null;
			
			$fiche->init($ficheId, $modeSuivi, $etabId, $intentionSuivi, $timingSuivi, $comments, $date, date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
			if(isset($_POST['updateForm']))
			{
				$db->deleteFicheContact($ficheId);
				$db->update($fiche);
			}
			else
				$ficheId = $db->insert($fiche);
			
			foreach($contacts as $c)
			{
				$ficheSuiviContact = new FicheSuiviContact();
				$ficheSuiviContact->init($c['id'], $ficheId, date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
				$db->insertFicheContact($ficheSuiviContact);
			}
			$db->commitTransaction();
			$db->close();
			
			if(isset($_POST['updateForm']))
			{
				Mapping::RedirectTo('etablissementDetail&id='.$etabId.'&updatefiche=0');
			}
			else $this->notices[]='La fiche de suivi a �t� ajout� avec succ�s !';
			
		}
		catch(Exception $e)
		{
			if(isset($_POST['updateForm']))
			{
				Mapping::RedirectTo('etablissementDetail&id='.$etabId.'&updatefiche=1');
			}
			else
				$this->error[]='Une erreur s\'est produite lors de l\'ajout de la fiche de suivi  !';
			$db->rollbackTransaction();
			$db->close();
		}	
			
			
			
			

			
			
	}
	
	private function GetFicheSuivi()
	{
		$db = new FicheSuiviDatabase();
		$db->open();
		$rs = $db->get($this->id, null);
		$db->close();
			
		return new FicheSuivis($rs);
	}
}
?>

