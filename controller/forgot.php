<?php
$errors = array();
$fieldserror = array();
$email ='';
if(isset($_REQUEST['back']))
{
	Mapping::RedirectTo('login');
}

if(isset($_REQUEST['commit']))
{
	if(!isset($_REQUEST['email']))
	{
		$errors[] = 'Veuillez indiquer une adresse de courriel';
		$fieldserror[] = 'email';
	}
	else
	{
		if(!preg_match('#' . REGEX_MAIL . '#i', $_REQUEST['email']))
		{
			$errors[] = 'Le format de l\'adresse de courriel est invalide';
			$fieldserror[] = 'email';
		}
	}

	if(!$errors)
	{
		$email = $_REQUEST['email'];
		REQUIRE_ONCE(SCRIPTPATH.'model/ase_database_model_class.php');
		$DB = new AseDatabase();
		$DB->open();
		$user = $DB->getUser($_REQUEST['email']);
		$DB->close();
			
		if(!$user)
		{
			$errors[] = 'Aucun utilisateur ne correspond � cette adresse de courriel';
			$fieldserror[] = 'email';
		}
		else
		{
			$password_sanitize	 = GeneratePassword();
			$DB->setPassword($user->getLogin(),$password_sanitize);
			
			$email = 'Vous avez demandez un rappel de mot de passe.
Vous trouverez ci-dessous le compte associ� � votre adresse de courriel : 
';

			$email.='
Identifiant  :    '.$user->getLogin().'
Mot de passe :    '.$password_sanitize.'
';

			REQUIRE_ONCE(SCRIPTPATH.'lib/mailsender.class.php');

			MailSender::initialize($_REQUEST['email'],'Rappel de mot de passe', $email, null, '', 'ASE');
			MailSender::send();

			Mapping::RedirectTo('login','&forgot=1');
		}
	}
}

REQUIRE_ONCE(SCRIPTPATH.'view/inscription_view_class.php');
$title = "Esprit d'entreprendre : Mot de passe oubli�";
$view = new InscriptionView();
//echo $view->Errors($errors);
$view->formulaireOubli($email,$errors);
echo $view->FieldsError($fieldserror);
?>
