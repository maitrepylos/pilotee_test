<?php
if(!$session->isLogged()) $session->redirect();
REQUIRE_ONCE(SCRIPTPATH.'view/formateur_view_class.php');

$view = new FormateurView();
$ec = new FormateurController($view);
$ec->Render();
class FormateurController
{
	private $view;
	private $user;
	
	public function __construct($view)
	{
		 $this->view = $view;
		 $this->user = Session::GetInstance()->getCurrentUser();
	}

	public function Render()
	{
		$formateurs = $this->GetListFormateur();
		$this->view->viewFormateurList($formateurs, $this->SetCloseCallBack());
	}
	
	private function GetListFormateur()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/formateur_database_model_class.php');
		$db = new FormateurDatabase();
		$db->open();
		$rs = $db->get();
		$db->close();
		return new Formateurs($rs);
	}
	
	private function SetCloseCallBack()
	{
		$closeCallBack = "none";
		
		if (isset($_REQUEST['close'])) $closeCallBack = $_REQUEST['close'];
		elseif (isset($_POST['CloseCallBack'])) $closeCallBack = $_POST['CloseCallBack'];
		
		return $closeCallBack;
	}	
}
?>