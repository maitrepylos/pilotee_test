<?php

if(!$session->isLogged()) $session->redirect();

REQUIRE_ONCE(SCRIPTPATH.'view/formationDetail_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/formation_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/formateur_database_model_class.php');

$ctrl = new FormationDetailController($title, $shortcut, $menu, $login, $notices);
$ctrl->Render();

class FormationDetailController
{
	private $view = null;
	private $title = null;
	private $notices = null;
	private $errors  = null;
	private $id = 0;

	public function __construct(&$title, &$shortcut, &$menu, &$login, &$notices)
	{
		//echo "<BR>DEBUG FFI <BR>";
		//echo "MODULE : " . $_REQUEST['module'];
		//echo "<BR>END DEBUG FFI <BR>";
		
		$this->view = new FormationDetailView();
		$this->notices = array();
		$this->errors = array();
		$this->title = &$title;
		$shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->view->Notices($this->notices);
		$menu = $this->view->createMenu();
		$login = $this->view->createLogin();
		$this->title = 'Formation ';
	}

	public function Render()
	{
		
		if (isset($_REQUEST["id"]) && is_numeric($_REQUEST["id"]))
		{
			if(isset($_REQUEST["error"]) && $_REQUEST["error"]=='0')
			{
				$this->notices[] = 'La formation a �t� mise � jour avec succ�s.';
			}
			elseif(isset($_REQUEST["error"]) && $_REQUEST["error"]=='1')
			{
				$this->errors[] = 'Une erreur s\'est produite lors de la mise � jour de la formation';
			}
			
			$this->id = $_REQUEST["id"];
			if(isset($this->id))
			{
				$this->RenderAction();
			}
			elseif (isset($_REQUEST["addAction"]) && $_REQUEST["addAction"]=='1')
			{
				$this->view->Errors($this->errors);	
			}
		}
		else
		{
			Mapping::RedirectTo('accueil');
		}
	}
	
	protected function _loadFormateurs()
	{
		REQUIRE_ONCE(SCRIPTPATH . 'model/formateur_database_model_class.php');
		REQUIRE_ONCE(SCRIPTPATH . 'domain/formateur_domain_class.php');
		$formateurDatabase = new FormateurDatabase();
		$formateurDatabase->open();
		$this->view->formateurs = new Formateurs($formateurDatabase->findAll());
	}
	
	protected function _loadFinalitePedagogique() {
		$this->view->pedagogList = Dictionnaries::getFinalitePedagogList();
	}
	
	protected function _loadFormationTypes(){
		
		$this->view->formationTypes = Dictionnaries::getFormationType();
	}

	protected function _loadDegreAndNiveau()
	{
		$this->view->degreNiveauList = Dictionnaries::getFiliereDegreNiveauList();
	}
	
	
	protected function _setCloseCallBack()
	{
		$closeCallBack = "none";

		if (isset($_REQUEST['close']))
		{
			$closeCallBack = $_REQUEST['close'];
		}
		elseif (isset($_POST['CloseCallBack']))
		{
			$closeCallBack = $_POST['CloseCallBack'];
		}

		return $closeCallBack;
	}
	
	private function getAnneeEnCours()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/annee_database_model_class.php');
		$DB_ANNEE = new AnneeDatabase();
		$annees = new Annees($DB_ANNEE->GetAnneeEnCoursId());
		return $annees;
	}
	
	private function initAction() {

		
		$this->_loadFinalitePedagogique();
		$this->_loadFormationTypes();
		$this->_loadDegreAndNiveau();
		// $this->_loadDataFormation();
		$this->view->isUpdate = false;
		$this->_loadFormateurs();

		/*foreach($formationTypes as $formationType)
		{
			$id = $formationType->getTypeFormationId();
			$this->view->formationTypesChecked[ $id ] = (!empty($_POST['formationType'][ $id ]));
		}
		*/
		


		$this->view->closeCallBack = $this->_setCloseCallBack();
	
		// FFI
		//if (! isset($this->view->anneeEnCours)) {
		//	$this->view->anneeEnCours = $this->getAnneeEnCours();
			
		//}
		//$_POST['anneesScolaire']= $this->getAnneeEnCours()->items(0)->getId();
		
	}
	
	private function _initFiliereDegreNiveau($formation) {
		// on cree un tableau associatif avec 
		// clef : id du filiereDegreeNiveau
		// value : nb d'apprenants
		// afin d'initialiser les text fields du form plus facilement
		
		$filieres = $formation->getFiliereDegreNiveau();
		$res = array();
		for ($i=0; $i<count($filieres); $i++) {
			$res[$filieres[$i]->GetFiliereDegreNiveauId()] = $filieres[$i]->GetNbApprenants();
		}
		
		return $res;		
	}
	
	private function _initActionFinalitePedagogs($formation) {
		// on cree un tableau associatif avec 
		// clef : id du finalitePedago
		// value : nb d'apprenants
		// afin d'initialiser les text fields du form plus facilement
		
		$peda = $formation->GetFinalitePedagog();
		$res = array();
		for ($i=0; $i<count($peda); $i++) {
			$res[$peda[$i]->GetFinalitePedagogId()] = $peda[$i]->GetNbApprenants();
		}
		
		return $res;		
	}
	

	private function RenderAction()
	{
		$currentsFormation = $this->GetAction();
		if($currentsFormation->count()>0)
		{
			$currentFormation = $currentsFormation->items(0);
			$this->initAction();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->filieresDegree = $this->_initFiliereDegreNiveau($currentFormation);
			$this->view->finalitePedago = $this->_initActionFinalitePedagogs($currentFormation);
			$this->view->Render($currentFormation);
		}
		else
		{
			$this->errors[]= 'L\' action n\'existe pas !';
			$this->view->Errors($this->errors);
		}
	}

	private function GetAction()
	{
		$db = new FormationDatabase();
		$db->open();
		$rs = $db->GetAll($this->id);
		$db->close();
		
		$formations = new Formations($rs);
		
		$db2 = new FormateurDatabase();
		$db2->open();
		
		foreach ($formations as $formation) {
			$res = $db2->getForAction($formation->getFormationId());
			$formateurs = new Formateurs($res);
			$formation->setFormateursAssocies($formateurs);			
		}
		
		return $formations;
	}
}

?>
