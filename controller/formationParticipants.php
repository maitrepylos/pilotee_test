<?php

    if(!$session->isLogged()) $session->redirect();

	REQUIRE_ONCE(SCRIPTPATH.'view/formationParticipants_view_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/formationParticipants_database_model_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'domain/formationParticipants_domain_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/age_database_model_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'domain/etablissement_searchresult_domain_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'domain/age_domain_class.php');
	
	$ctrl = new FormationParticipantsController();
	$ctrl->Render();

	class FormationParticipantsController
	{
		private $view = null;
		private $formation_id = null; 
		private $user = null;
		
		//?
		private $objet = null;
		private $errors = array();
		private $contactId=null, $ficheSuiviId=null, $remiseMaterielId=null, $actionId=null;
		
		public function __construct()
		{
			
		}
		
		public function Render()
		{
			/*if (isset($_REQUEST['action']) && $_REQUEST['action']=='update') {
				$this->RenderUpdate();
				// Mapping::RedirectTo('formationParticipants&formation_id='. $_REQUEST["formation_id"]);
			} else
			*/
			if (isset($_REQUEST['action']) && $_REQUEST['action']=='delete') {
				$this->RenderDelete();				
			/*} elseif (isset($_REQUEST['action']) && $_REQUEST['action']=='add') {
				$this->user = Session::GetInstance()->getCurrentUser();
				$this->view = new FormationParticipantsView();			
				$this->formation_id = $_REQUEST["formation_id"];
				$this->RenderAdd();*/				
			} elseif (isset($_REQUEST['action']) && $_REQUEST['action']=='getEtablissements') {
				$this->RenderEtablissements(); // plus utilis�			
			}elseif (isset($_REQUEST["formation_id"]) && is_numeric($_REQUEST["formation_id"]))
			{
				$this->user = Session::GetInstance()->getCurrentUser();
				$this->view = new FormationParticipantsView();
				$this->formation_id = $_REQUEST["formation_id"];
				$this->RenderParticipants(); 
			}
			else
			{
				Mapping::RedirectTo('accueil');
			}
		}
		
		/*private function RenderAdd() {
			$participant = new FormationParticipants();
			//$participant->id = $_POST['id'] ;
			$participant->nom = $_POST['nom'];
			$participant->prenom = $_POST['prenom'];
			$participant->telephone1 = $_POST['tel1'];
			$participant->telephone2 = $_POST['tel2'];
			$participant->email1 = $_POST['mail1'];
			$participant->email2 = $_POST['mail2'];
			$participant->genre = $_POST['genre'];
			$participant->age = $_POST['age'];
			$participant->etablissement_id = $_POST['ets'];
			$participant->formation_id = $_POST['formation_id'];
			
			$db = new FormationParticipantsDatabase();
			$db->open();
			$rs = $db->addParticipant($participant);
			$db->close();
						
			if($rs) {
				$participant->id = $rs;
				echo $participant->id;
			} //else { 
			//	$this->errors[]= 'Le participant n\'a pas pu �tre sauvegard� !';
			//	$this->view->Errors($this->errors);
			// }
			
		}
		*/
		
		/*
		 * 
		 public function RenderUpdate() {
			echo $_POST['value'];
			$participantid = $_POST['id'] ;
			$db = new FormationParticipantsDatabase();
			$db->open();
			$rs = $db->getParticipantForId($participantid);
			$participants = new FormationParticipantss($rs);
			if (count($participants) > 0 )
				$participant = $participants->items(0);
			//else {
			//	$this->errors[]= 'Pas de participant correspondant � l\'ID s�lectionn� !';
			//	$this->view->Errors($this->errors);
			//	return null;
			//}
			
			
			switch ($_POST['columnId']) {
				case 0:
					$participant->nom = $_POST['value'];    
					break;
				case 1:
					$participant->prenom = $_POST['value'];
					break;
				case 2:
					$participant->telephone1 = $_POST['value'];
					break;
				case 3:
					$participant->telephone2 = $_POST['value'];
					break;
				case 4:
					$participant->email1 = $_POST['value'];
					break;
				case 5:
					$participant->email2 = $_POST['value'];
					break;
				case 6:
					$participant->genre = $_POST['value'];
					break;
				case 7:
					$participant->age = $_POST['value'];
					break;
				case 8:
					$participant->etablissement_id = $_POST['value'];
					break;
			}
			
			$res = $db->updateParticipant($participant);
			
			$db->close();
			
			//if(! $res) {
			//	$this->errors[]= 'Le participant n\'a pas pu �tre modifi� !';
			//	$this->view->Errors($this->errors);
			//}
					
		}
		*/
		
		public function RenderDelete() {
			
			$id = $_POST['id'] ;
			$db = new FormationParticipantsDatabase();
			$db->open();
			$rs = $db->deleteParticipant($id);
			$db->close();
			
			/*if(! $rs) {
				$this->errors[]= 'Le participant n\'a pas pu �tre supprim� !';
				$this->view->Errors($this->errors);
			}*/
			
			
		}
		
		private function GetTranchesDage() {
			$db = new AgeDatabase();
			$db->open();
			$rs = $db->get();
			//if (!isset($userId)) $userId=-1;
			//$rs = $db->GetAgentEtablissement($userId);
			//($userId);
			$db->close();
			return new Ages($rs);
			
		}
		
		private function RenderEtablissements() {
			$ets = $this->GetEtablissements($this->GetUtilisateurId());	
			
			$res = array();
			if (count($ets)>0) {
					
					foreach ($ets as $et) {
						$res[$et->getId()] = $et->getNom() ;
					}
			} else {
				$res['-1'] = 'pas d\'�tablissement';
			}
			print json_encode($res);
		}
		
		private function RenderParticipants()
		{
			$elts = $this->GetParticipants($this->formation_id);
			
			$etablissements = $this->GetEtablissements($this->GetUtilisateurId());

			$tranchesDage = $this->GetTranchesDage();
			
			
				$this->view->participants = $elts;
				$this->view->etablissements= $etablissements;
				$this->view->tranchesdage = $tranchesDage;
				$this->view->Render();
			/*}
			else
			{
				$this->errors[]= 'La formation n\'existe pas !';
				$this->view->Errors($this->errors);
			}*/
		}
		
		private function GetParticipants($formationid)
		{
			$db = new FormationParticipantsDatabase();
			$db->open();
			$rs = $db->getParticipants($formationid);
			$db->close();
			return new FormationParticipantss($rs);
		}
		
		private function GetEtablissements($userId)
		{
			$db = new EtablissementDatabase();
			$db->open();
			$rs = $db->get(null, $userId, null, null, null, null, null, null, null);
			//if (!isset($userId)) $userId=-1;
			//$rs = $db->GetAgentEtablissement($userId);
			//($userId);
			$db->close();
			$ets = new EtablissementSearchResults($rs);
			/*if (count($ets) > 20) {
				$ets2= array();
				for ($i=0; $i < 20; $i++ )
					$ets2[] = $ets[$i];
				return $ets2;
			} else*/ 
			return $ets;
			// return new EtablissementSearchResults($rs);
		}
		
		private function GetUtilisateurId()
		{
			$utilisateurId = null;
			
			if (($this->user->isAdmin()) || ($this->user->isCoordinateur()) || $this->user->isAgentCoordinateur() )
			{
			}
			elseif ($this->user->isAgent() || $this->user->isFormateur())
			{
				$utilisateurId = $this->user->getUtilisateurId();
			}
			
			return $utilisateurId;
		}
		
	}

?>
	