<?php


REQUIRE_ONCE(SCRIPTPATH .'model/formationParticipants_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/formationParticipants_domain_class.php');

if (isset($_REQUEST['action']) && $_REQUEST['action']=='updateformationparticipants') {
	ProcessUpdate();
} elseif (isset($_REQUEST['action']) && $_REQUEST['action']=='addformationparticipants') {
	ProcessAdd();
}


function ProcessAdd() {
	$participant = new FormationParticipants();
	//$participant->id = $_POST['id'] ;
	$participant->nom = $_POST['nom'];
	$participant->prenom = $_POST['prenom'];
	$participant->telephone1 = $_POST['tel1'];
	$participant->telephone2 = $_POST['tel2'];
	$participant->email1 = $_POST['mail1'];
	$participant->email2 = $_POST['mail2'];
	$participant->genre = $_POST['genre'];
	$participant->age = $_POST['age'];
	$participant->etablissement_id = $_POST['ets'];
	$participant->formation_id = $_POST['formation_id'];
	
	$db = new FormationParticipantsDatabase();
	$db->open();
	$rs = $db->addParticipant($participant);
	$db->close();
				
	if($rs) {
		$participant->id = $rs;
		echo $participant->id;
	} /*else { 
		$this->errors[]= 'Le participant n\'a pas pu �tre sauvegard� !';
		$this->view->Errors($this->errors);
	}*/
	
}

function ProcessUpdate() {
	echo $_POST['value'];
	$participantid = $_POST['id'] ;
	$db = new FormationParticipantsDatabase();
	$db->open();
	$rs = $db->getParticipantForId($participantid);
	$participants = new FormationParticipantss($rs);
	if (count($participants) > 0 )
		$participant = $participants->items(0);
	/*else {
		$this->errors[]= 'Pas de participant correspondant � l\'ID s�lectionn� !';
		$this->view->Errors($this->errors);
		return null;
	}*/
		
	switch ($_POST['columnId']) {
		case 0:
			$participant->nom = $_POST['value'];    
			break;
		case 1:
			$participant->prenom = $_POST['value'];
			break;
		case 2:
			$participant->telephone1 = $_POST['value'];
			break;
		case 3:
			$participant->telephone2 = $_POST['value'];
			break;
		case 4:
			$participant->email1 = $_POST['value'];
			break;
		case 5:
			$participant->email2 = $_POST['value'];
			break;
		case 6:
			$participant->genre = $_POST['value'];
			break;
		case 7:
			$participant->age = $_POST['value'];
			break;
		case 8:
			$participant->etablissement_id = $_POST['value'];
			break;
	}
	
	$res = $db->updateParticipant($participant);
	
	$db->close();
	
	/*if(! $res) {
		$this->errors[]= 'Le participant n\'a pas pu �tre modifi� !';
		$this->view->Errors($this->errors);
	}*/
			
}
		

?>