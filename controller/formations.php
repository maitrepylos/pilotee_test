<?php
error_reporting(E_ALL ^ E_STRICT);

if(!$session->isLogged())
{
	$session->redirect(); 
}

REQUIRE_ONCE(SCRIPTPATH . 'lib/abstractController.php');
REQUIRE_ONCE(SCRIPTPATH . 'model/formation_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'model/action_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'view/formation_view_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'domain/dictionnaries_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'domain/formation_type_domain_class.php');

class FormationController extends AbstractController
{
	private $_formationId;
	protected $_defaultAction = 'search';
	
	public function render() //FFI
	{
		
		if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'addFormation')
		{
				$this->RenderAdd();
		} elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == 'updateFormation')
		{
				$this->RenderUpdate();
				/*echo "<br>UPDATE<BR>";	
				echo "REQUEST : <BR>";
				var_dump($_REQUEST);
				echo "POST : <BR>";
				var_dump($_POST);
				*/				
		} elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == 'deleteFormation')
		{
				// deleteId
				$this->RenderDelete();
				/*
				echo "<br>UPDATE<BR>";
				echo "REQUEST : <BR>";
				var_dump($_REQUEST);
				echo "POST : <BR>";
				var_dump($_POST);
				*/				
		}
		elseif (! isset($_REQUEST['action']) && isset($_POST['searchFormations']))
		{
				// echo "SEARCH";
				$this->RenderSearch();			
		} else {
			// echo "PARENT";
			parent::render();
		}
		
	}
	
	public function RenderSearch() {
		$this->_view->Notices($this->_notices);
		$this->_view->Errors($this->_errors);
		$this->_searchInit();
		$this->_view->setResults($this->Search());		
		$this->_view->setAction('Search');
		$this->_view->render();
			
	}
	
	private function Search() {
		
		/*echo "<br> FFI DEBUG <BR>";
		var_dump($_POST);
		echo "<br> END FFI DEBUG <BR>";
		*/
		
		
		// initialisation des variables du formulaire
		// pour plus de clarte
		$search_typeFormation = array();
		$search_anneeScolaire = null;
		$search_anneeAnte = null;
		$search_anneePost =  null;
		$search_intitule =  null;
		$search_actionValid = null;
		$search_actionEnCours = null;
		$search_niveauFormation = null;
		$search_niveauDegreFormation = null;
		$search_niveauDegreFilereFormation = null;
		$search_prestataire = null;
		$search_global = null;
		
		
		// decodage du formulaire
		if (!empty($_POST['formationType']))
		{
			/*if ($this->_user->isFormateur())
			{
				// les formateurs ne peuvent voir les formations de type ATE
				unset($_POST['formationType']['ATE']);
			}*/

			if (! empty($_POST['formationType']['ATE']) && ! $this->_user->isFormateur())
				// les formateurs ne peuvent voir les formations de type ATE
				$search_typeFormation[] = 'ATE';
			if (!empty($_POST['formationType']['STAC']))
				$search_typeFormation[] = 'STAC';
			if (!empty($_POST['formationType']['FOR']))
				$search_typeFormation[] = 'FOR';
			
		}
		
		$search_anneeScolaire = $_POST['anneesScolaire'];
		$search_anneeAnte = $_POST['annee_ante'];
		$search_anneePost = $_POST['annee_post'];
		$search_intitule = $_POST['intitule'];
				
		if (!empty($_POST['formationEnCours']))
		{
			$search_actionEnCours= $_POST['formationEnCours'];
		}
		if (!empty($_POST['formationValide']))
		{
			$search_actionValid = $_POST['formationValide'];
		}
		
		$search_niveauFormation = $_POST['niveauFormation'];
		$search_niveauDegreFormation = $_POST['niveauDegreFormation'];
		$search_niveauDegreFilereFormation = $_POST['niveauDegreFilereFormation'];
		
		if ($this->_user->isFormateur())
		{	
			$search_prestataire = $this->_user->getFormateurId();
		} else {
			$search_prestataire = $_POST['prestataire'];
		}
		
		$search_global = (!empty($_POST['actionGlob']) ? 1 : null);
		
		$db = new FormationDatabase();
		//($nomSpec, $anneeScolaire, $dateAnte, $datePost, $operateurId,
		// $typeFormation, $actionValide, $actionEnCours, $niveau, $degreNiveau, $degreNiveauFiliere)
		$results = $db->get($search_intitule, $search_anneeScolaire, $search_anneeAnte, $search_anneePost,
				$search_prestataire, $search_typeFormation, $search_actionValid, $search_actionEnCours,
				$search_niveauFormation, $search_niveauDegreFormation, $search_niveauDegreFilereFormation, $search_global);  
		
		$formations = new Formations($results);
		
		return $formations;
		
	}
	
	public function RenderDelete()
	{
		if (isset($_REQUEST['deleteId'])) 
		{
			//echo "OK FORM POSTED";
			//var_dump($_POST);
			$this->_deleteAction();
			
			//echo ('<br>debug FFI <br>');
			// var_dump($this->_view);
			
			$this->_view->Notices($this->_notices);
			$this->_view->Errors($this->_errors);
			//$this->actionId
			//$this->_view->AddAction(false, $this->actionId);
			//$this->_view->_addRender();
			//$this->_searchInit();
			$this->_view->setAction('edit');
			$this->_view->render();
		}
		else
		{
			echo "BIZARRE";
			var_dump($_POST);
			/*$this->InitializeAddForm();
			$this->view->AddAction();
			*/
		}
		
	}
	
	public function RenderUpdate()
	{
		if (isset($_POST['updateForm']) && isset($_POST['formation_id'])) //Si le formulaire action est post�
		{
			//echo "OK FORM POSTED";
			//var_dump($_POST);
			// TODO : modify for formation
			
			$this->insert_update($_POST['formation_id']);
			//echo ('<br>debug FFI <br>');
			// var_dump($this->_view);
			
			$this->_view->Notices($this->_notices);
			$this->_view->Errors($this->_errors);
			//$this->actionId
			//$this->_view->AddAction(false, $this->actionId);
			//$this->_view->_addRender();
			//$this->_searchInit();
			$this->_view->setAction('edit');
			$this->_view->addIdAction($_POST['formation_id']);
			$this->_view->render();
		}
		else
		{
			echo "BIZARRE";
			var_dump($_POST);
			/*$this->InitializeAddForm();
			$this->view->AddAction();
			*/
		}
		
	}
	
	public function RenderAdd()
	{
		if (isset($_POST['addForm']) )//Si le formulaire action est post�
		{
			//echo "OK FORM POSTED";
			//var_dump($_POST);
			// TODO : modify for formation
			
			//var_dump($this->_view);
			
			
			$this->insert_update();
			$this->_view->Notices($this->_notices);
			$this->_view->Errors($this->_errors);
			//$this->actionId
			/* Permet d'ajouter l'id de la formation
			 * Elle sera r�cup�r�e pour rediriger le visiteur sur la fiche pr�c�demment cr�e
			 * Elle est r�utilis�e dans la vue
			 */
			$this->_view->addIdAction($this->_formationId);
			//$this->_view->_addRender();
			$this->_view->setAction('addresult');
			$this->_view->render();
			
			
		}
		else
		{
			echo "BIZARRE";
			var_dump($_POST);
			/*$this->InitializeAddForm();
			$this->view->AddAction();
			*/
		}
		
	}
	
	private function insert_update($idFormation = null)
	{ // FFI
		// TODO : modify for formation
		
		/*
		 * array(36) { 
		 * ["formationType"]=> string(4) "STAC" 
		 * ["organisateur"]=> string(1) "1" 
		 * ["anneesScolaire"]=> string(7) "2011-12" 
		 * ["date"]=> string(10) "15/02/2012" 
		 * ["intitule"]=> string(0) "" 
		 * ["commentaire"]=> string(0) "" 
		 * ["website"]=> string(0) "" 
		 * ["nbGlobalApp"]=> string(1) "1" 
		 * ["nbGlobalEns"]=> string(0) "" 
		 * ["PRI1N"]=> string(1) "1" 
		 * ["PRI2N"]=> string(0) "" 
		 * ["PRI3N"]=> string(0) "" 
		 * ["SEC1C"]=> string(0) "" 
		 * ["SEC1D"]=> string(0) "" 
		 * ["SEC2G"]=> string(0) "" 
		 * ["SEC2Q"]=> string(0) ""
		 * ["SEC2A"]=> string(0) "" 
		 * ["SEC3G"]=> string(0) "" 
		 * ["SEC3Q"]=> string(0) "" 
		 * ["SEC3A"]=> string(0) "" 
		 * ["SEC4N"]=> string(0) "" 
		 * ["SUBAN"]=> string(0) "" 
		 * ["SUMAN"]=> string(0) "" 
		 * ["SUDON"]=> string(0) "" 
		 * ["ENSTN"]=> string(0) "" 
		 * ["PSOTN"]=> string(0) "" 
		 * ["INSTP"]=> string(0) "" 
		 * ["INSTM"]=> string(0) "" 
		 * ["AGINF"]=> string(0) "" 
		 * ["AGSUP"]=> string(0) "" 
		 * ["ECAP"]=> string(0) "" 
		 * ["ECAPE"]=> string(0) "" 
		 * ["addForm"]=> string(0) "" 
		 * ["selectedFormateurs"]=> string(0) "" 
		 * ["selectedFormateursName"]=> string(0) "" 
		 * ["formation_id"]=> string(0) "" }
		 * Statut = 'valider'
		 */
		
		// initiatlisation des variables

		
		$this->_formationId = $idFormation; 
		
		$dateAction = null;
		$statutActionId = 'ENCOU'; // FFI ?
		$commentaire = null;
		$web = null;
		$nbEnseignants = null; $nbApprenants = null;
		$anneeScolaireId = null;
		$typeActionId = null;
		$niveauDegreFiliere = array();
		$pedas = array();
		$intitule = null;
		$formateur = null;
		$global = null;
		
		// var_dump($_POST);
		
		// recuperation du formulaire
		
		$anneeScolaireId = $_POST['anneesScolaire'];
		$typeActionId = $_POST['formationType'];
	
		
		if($_REQUEST['statut'] == 'valider')
			$statutActionId = 'VALID';

		if(!empty($_POST['date']))
			$dateAction = $_POST['date'];
			
		if(!empty($_POST['commentaire']))
			$commentaire = $_POST['commentaire'];
			
		if(!empty($_POST['intitule']))
			$intitule = $_POST['intitule'];

		$web  = !empty($_POST['website']) ?$_POST['website'] : null;
		
		$nbEnseignants = $_POST['nbGlobalEns'];
		$nbApprenants = $_POST['nbGlobalApp'];
			
		$fdn = Dictionnaries::getFiliereDegreNiveauList();
		for($i=0; $i<$fdn->Count(); $i++)
		{
			if(!empty($_POST[$fdn->items($i)->GetFiliereDegreNiveauId()]))
				$niveauDegreFiliere[] = array("id"=>$fdn->items($i)->GetFiliereDegreNiveauId(), "nb"=>$_POST[$fdn->items($i)->GetFiliereDegreNiveauId()]);
		}
		
		$peda = Dictionnaries::getFinalitePedagogList();
		for($i=0; $i<$peda->Count(); $i++)
		{
			if(!empty($_POST[$peda->items($i)->GetFinalitePedagogId()]))
				$pedas[] = array("id"=>$peda->items($i)->GetFinalitePedagogId(), "nb"=>$_POST[$peda->items($i)->GetFinalitePedagogId()]);
		}
		
		/*if(!empty($_POST["selectedFormateurs"]))
		{
			$ops = explode(";", $_POST["selectedFormateurs"]);
			if (count($ops) > 0) $formateur = $ops[0];
		}*/
		
		if (!empty($_POST['organisateur'])) {
			$formateur = $_POST['organisateur'];
		}
		
		$nbEtabs = !empty($_POST['nbLignes']) ?$_POST['nbLignes'] : null;
		$nbContacts = !empty($_POST['nbLignesContact']) ?$_POST['nbLignesContact'] : null;
		$contactAction = array();
		
		for($i=0; $i<$nbContacts; $i++)
		{
			if(isset($_POST['roleActionContact_'.$i]))
				$contactAction[] = array("id"=>$_POST['contactId_'.$i],"role" => $_POST['roleActionContact_'.$i]);
		}		
		
		$global = isset($_POST['swGlobal']) ?$_POST['swGlobal'] : 0;
	
		//INSERT
		$dbAction = new ActionDatabase();
		try
		{
			$dbAction->open();
			$dbAction->startTransaction();

			$action = new Action();
			$action->init($this->_formationId, null, null, null, null,
							$_POST['formationType'], $_POST['anneesScolaire'], $statutActionId, null, $formateur,
							$dateAction, null, $intitule, $commentaire, $global, 1, $nbApprenants, $nbEnseignants,
							$web, null, null, null, null, null, null,
							date('Y-m-d H:i:s'), $this->_user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->_user->getUtilisateurId());
			
			if(! isset($_POST['updateForm']))
				$this->_formationId = $dbAction->insertAction($action);
			else
			{
				// TODO : adapt for formation
				$dbAction->deleteActionFormateur($action->GetActionId());
				$dbAction->deleteActionOperateur($action->GetActionId());
				$dbAction->deleteActionPeda($action->GetActionId());
				$dbAction->deleteActionSection($action->GetActionId());
				$dbAction->deleteActionFiliereDegreNiveau($action->GetActionId());
				$dbAction->deleteActionContact($action->GetActionId());
				$dbAction->deleteActionEtablissement($action->GetActionId());
				$this->_formationId = $dbAction->updateAction($action);
			}
			
					//ActionEtablissement
			if($_POST['nbLignes']!='0' && !empty($_POST['etabIds']))
			{
				$etabIds = explode(";", $_POST['etabIds']);
				foreach($etabIds as $etabId)
				{
					$actionEtab = new ActionEtablissement();
					$actionEtab->init($this->_formationId, $etabId, date('Y-m-d H:i:s'), $this->_user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->_user->getUtilisateurId());
					$dbAction->insertActionEtablissement($actionEtab);
				}
			}
			
			//ActionContact
			for($i=0; $i< count($contactAction); $i++)
			{
				$actionC = new ActionContact();
				$actionC->init($this->_formationId,$contactAction[$i]['id'], $contactAction[$i]['role'], date('Y-m-d H:i:s'), $this->_user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->_user->getUtilisateurId());
				$dbAction->insertActionContact($actionC);
			}
			
			
			//R�partition par niveau-degr�-fili�re
			for($i=0; $i< count($niveauDegreFiliere); $i++)
			{
				$afdn = new ActionFiliereDegreNiveau();
				$afdn->init($this->_formationId, $niveauDegreFiliere[$i]['id'], $niveauDegreFiliere[$i]['nb']);
				$dbAction->insertActionFiliereDegreNiveau($afdn);
			}
			for($i=0; $i< count($pedas); $i++)
			{
				$actionPeda = new ActionFinalitePedagog();
				$actionPeda->init($this->_formationId, $pedas[$i]['id'], $pedas[$i]['nb']);
				$dbAction->insertActionPeda($actionPeda);
			}

			if(!empty($_POST["selectedFormateurs"]))
			{
				$ops = explode(";", $_POST["selectedFormateurs"]);
				foreach($ops as $op)
				{
					// TODO : adapter � formation  : actionFormateur
					$actionOp = new ActionFormateur();
					$actionOp->init($this->_formationId, $op);
					$dbAction->insertActionFormateur($actionOp);
				}
			}
			
			$dbAction->commitTransaction();
			$dbAction->close();
			
			if(! isset($_POST['updateForm']))
				$this->_notices[] = 'La formation a �t� cr��e avec succ�s.';
			else
				$this->_notices[] = 'La formation a �t� mise � jour avec succ�s.';
			
			
		}
		catch(Exception $e)
		{
			// echo $e->getMessage();
			$dbAction->rollbackTransaction();
			
			$dbAction->close();
			
			if(!isset($_POST['updateForm']))
				$this->_errors[] = 'Une erreur s\'est produite lors de l\'ajout de la formation !';
			else
				// TODO : adapter � formation
				Mapping::RedirectTo('formationDetail&id='.$idAction.'&error=1');
		}
	}
	
	

	protected function _postConstruct()
	{
		if (!empty($_REQUEST['formationId']))
		{
			$this->_formationId = intval($_REQUEST['formationId']);
		}
	}
	
	private function getAnneeEnCours()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/annee_database_model_class.php');
		$DB_ANNEE = new AnneeDatabase();
		$annees = new Annees($DB_ANNEE->GetAnneeEnCoursId());
		return $annees;
	}
	
	protected function _searchAction()
	{
		// Recherche
		if (isset($_POST['searchActions']))
		{
			$this->_sendErrorsAndNotices();
		}

		// init datas
		$this->_searchInit();
		if(isset($this->notices)) $this->_view->Notices($this->notices);
	}

	protected function _addAction()
	{
		// don't forget it's also call by edit mode!		
		$this->_loadFinalitePedagogique();
		$this->_loadDegreAndNiveau();
		$this->_loadDataFormation();
		$this->_view->isUpdate = false;
		$this->_loadFormateurs();
		
		$formationTypes = Dictionnaries::getFormationType();
		$this->_view->formationTypes = $formationTypes;
		
		// FFI
		
		if (isset($_POST['anneesScolaire'])) {
			if (!empty($_POST['anneesScolaire']))
				$this->_view->anneeEnCours = $_POST['anneesScolaire'];	
		}
		
	}

	protected function _editAction()
	{
		$this->_addAction();
		$this->_view->isUpdate = true;

		// to use add view instead of edit
		$this->_callableAction = 'add';
	}

	protected function _deleteAction()
	{
		try
		{
			$formation = $this->_getFormation($_REQUEST['deleteId']);
			$DB = new ActionDatabase();
			$DB->open();
			$DB->startTransaction();
			$DB->deleteAction($formation->getFormationId());
			$DB->deleteActionFormateur($formation->getFormationId());
			$DB->deleteActionOperateur($formation->getFormationId());
			$DB->deleteActionPeda($formation->getFormationId());
			$DB->deleteActionSection($formation->getFormationId());
			$DB->deleteActionFiliereDegreNiveau($formation->getFormationId());
			$DB->deleteActionContact($formation->getFormationId());
			$DB->deleteActionEtablissement($formation->getFormationId()
			);
			/*
			$DB->deleteActionOperateur($formation->getFormationId());
			$DB->deleteActionPeda($formation->getFormationId());
			$DB->deleteActionSection($formation->getFormationId());
			$DB->deleteActionFiliereDegreNiveau($formation->getFormationId());
			$DB->deleteActionContact($formation->getFormationId());
			$DB->deleteActionEtablissement($formation->getFormationId());
			$DB->deleteAction($formation->getFormationId());
			*/
			$DB->commitTransaction();
			$this->_notices[] = 'La formation a �t� effac�e avec succ�s.';

		}
		catch(Exception $e)
		{
			$DB->rollbackTransaction();
			$DB->close();
		}
		$DB->close();
		// Mapping::RedirectTo('formations&delete=1');
	}

	//
	//
	//

	protected function _getFormation($id)
	{
		REQUIRE_ONCE(SCRIPTPATH . 'model/formation_database_model_class.php');
		$db = new FormationDatabase();
		$db->open();
		$rs = $db->getAll($id);
		$ets = new Formations($rs);
		return $ets->items(0);
	}

	protected function _loadFormateurs()
	{
		REQUIRE_ONCE(SCRIPTPATH . 'model/formateur_database_model_class.php');
		REQUIRE_ONCE(SCRIPTPATH . 'domain/formateur_domain_class.php');
		$formateurDatabase = new FormateurDatabase();
		$formateurDatabase->open();
		$this->_view->formateurs = new Formateurs($formateurDatabase->findAll());
	}

	protected function _loadDegreAndNiveau()
	{
		$this->_view->degreNiveauList = Dictionnaries::getFiliereDegreNiveauList();
	}

	protected function _loadFinalitePedagogique()
	{
		$this->_view->pedagogList = Dictionnaries::getFinalitePedagogList();
	}

	protected function _loadDataFormation()
	{
		// init data
		$data = array(
			'selectedFormateurs', 'selectedFormateursName', 'formation_id', 'formationType',
			'organisateur', 'anneesScolaire', 'date', 'intitule', 'commentaire', 'website',
			'assocFormateur', 'nbGlobalApp', 'nbGlobalEns', 'niveauDegreFormation', 'statutFormationId'
		);

		if (count($this->_view->pedagogList) > 0)
		{
			foreach($this->_view->pedagogList as $v)
			{
				$data[] = $v->GetFinalitePedagogId();
			}
		}

		if (count($this->_view->degreNiveauList) > 0)
		{
			foreach($this->_view->degreNiveauList as $v)
			{
				$data[] = $v->GetFiliereDegreNiveauId();
			}
		}

		$this->_view->data = array();
		foreach($data as $k)
		{
			$v = (!empty($_POST[ $k ]))?$_POST[ $k ]:'';
			if (is_array($v))
			{
				foreach ($v as $kk => $vv)
				{
					$v[ $kk ] = addslashes($vv);
				}
			}
			else
			{
				$v = addslashes($v);
			}
			$this->_view->data[ $k ] = (!empty($_POST[ $k ]))?$_POST[ $k ]:'';
		}
	}

	protected function _setCloseCallBack()
	{
		$closeCallBack = "none";

		if (isset($_REQUEST['close']))
		{
			$closeCallBack = $_REQUEST['close'];
		}
		elseif (isset($_POST['CloseCallBack']))
		{
			$closeCallBack = $_POST['CloseCallBack'];
		}

		return $closeCallBack;
	}

	/**
	 * Initialisation des donn�es utile pour effectuer la recherche
	 */
	protected function _searchInit()
	{
		$formationTypes = Dictionnaries::getFormationType();

		$this->_view->formationTypesChecked = array();
		$this->_view->formationTypes = $formationTypes;

		foreach($formationTypes as $formationType)
		{
			$id = $formationType->getTypeFormationId();
			$this->_view->formationTypesChecked[ $id ] = (!empty($_POST['formationType'][ $id ]));
		}

		$this->_loadFormateurs();

		$this->_view->closeCallBack = $this->_setCloseCallBack();
	
		
		// FFI

		if (isset($_POST['anneesScolaire'])) {
			if (!empty($_POST['anneesScolaire']))
				$this->_view->anneeEnCours = $_POST['anneesScolaire'];
		}
		else {
			if($this->getAnneeEnCours()->items(0)) $this->_view->anneeEnCours = $this->getAnneeEnCours()->items(0)->getId();
			else $this->notices[] = "Il n'y a pas d'ann�e d'encodage en base de donn�es pour la date actuelle";
		}	
		
	}

	protected function _fillDataIfSet(array &$data, $value = null, $fieldName)
	{
		if (!empty($value))
		{
			$data[ $fieldName ] = $value;
		}
	}
}

$view = new FormationView();
if ($_REQUEST['module'] == 'formations')
{
	$title = "Formations";
	$shortcut = $view->raccourcisPanel($_REQUEST['module']);
	$menu = $view->createMenu();
	$login = $view->createLogin();
}

$c = new FormationController($view);
$c->render();

# EOF
