<?php
	REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'view/google_maps_admin_view.php');
	
	if(!$session->isLogged()) $session->redirect();
	
	$ctrl = new GoogleMapsAdminController($title, $shortcut, $menu, $login);
	$ctrl->render();
?>

<?php
	class GoogleMapsAdminController
	{
		private $view = null;
		private $title = null;
		private $notices = array();
		private $errors = null;
		
		private $addressesList = null;
		
		public function __construct(&$title, &$shortcut, &$menu, &$login)
		{
			$this->redirectIfRequired();
			$this->onSubmit();
			
			$this->view = new GoogleMapsAdminView();
			
			$this->title = &$title;
			$shortcut = $this->view->raccourcisPanel();
			$menu = $this->view->createMenu();
			$login = $this->view->createLogin();
			
			$this->title = 'Administration Google Maps';
		}
		
		private function onSubmit()
		{
			if (isset($_POST['SubmitAction']))
			{
				if ($_POST['SubmitAction'] == 'start') $this->getAddressesList();
				if ($_POST['SubmitAction'] == 'update') $this->performUpdate();
			}
		}
		
		private function performUpdate()
		{
			if (isset($_POST['GeoValues']) && strlen($_POST['GeoValues']) > 0)
			{
				$user = Session::GetInstance()->getCurrentUser();
				
				$addressesArr = explode(';', $_POST['GeoValues']);

				$db = new AdresseDatabase();
				$db->open();
				
				for ($i = 0; $i < count($addressesArr); $i++)
				{
					$address = explode('/', $addressesArr[$i]);

					$db->setGeoLocalisation($address[0], $address[1], $address[2], $user->getUtilisateurId());
				}

				$db->close();
			}
		}
		
		private function getAddressesList()
		{
			$db = new AdresseDatabase();
			$db->open();
			
			$rs = null;
			
			if (isset($_POST['chkAll'])) $rs = $db->listForGM();
			else
			{
				$missing = null;
				
				if (isset($_POST['chkMissing'])) $missing = true;	
				if (isset($_POST['chkMissing'])) $rs = $db->listForGM($missing);
			}
			
			$db->close();
			
			if (isset($rs)) $this->addressesList = new Adresses($rs);
		}
		
		private function redirectIfRequired()
		{
			$user = Session::GetInstance()->getCurrentUser();
			
			if (!$user->isAdmin()) Mapping::RedirectTo('accueil');
		}
		
		public function render()
		{	
			$db = new AdresseDatabase();
			$db->open();
			$rsAll = $db->listForGMCount();
			$rsMissing = $db->listForGMCount(true);
			$db->close();
			
			$row = mysqli_fetch_assoc($rsAll);
			$countAll = $row['recordCount'];
			
			$row = mysqli_fetch_assoc($rsMissing);
			$countMissing = $row['recordCount'];

			$this->view->render($countAll, $countMissing, $this->addressesList);
		}
	}
?>
	