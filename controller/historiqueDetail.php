<?php

    if(!$session->isLogged()) $session->redirect();

	REQUIRE_ONCE(SCRIPTPATH.'view/historiqueDetail_view_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/historiqueEncodage_database_model_class.php');
	
	$ctrl = new HistoriqueDetailController();
	$ctrl->Render();

	class HistoriqueDetailController
	{
		private $view = null;
		private $id = null; 
		private $objet = null;
		private $errors = array();
		private $contactId=null, $ficheSuiviId=null, $remiseMaterielId=null, $actionId=null;
		
		public function __construct()
		{
			$this->view = new HistoriqueDetailView();
			
		}
		
		public function Render()
		{
			if (isset($_REQUEST["id"]) && is_numeric($_REQUEST["id"]) && isset($_REQUEST["objet"]))
			{
				$this->id = $_REQUEST["id"];
				$this->objet = $_REQUEST["objet"];
				$this->RenderHistorique(); 
			}
			else
			{
				Mapping::RedirectTo('accueil');
			}
		}
		
		private function RenderHistorique()
		{
			$ets = $this->GetHistorique();
			if($ets->count())
			{
				$this->view->Render($ets);
			}
			else
			{
				$this->errors[]= 'L\'historique n\'existe pas !';
				$this->view->Errors($this->errors);
			}
		}
		
		private function GetHistorique()
		{
			$this->setObjetId();
			$db = new HistoriqueEncodageDatabase();
			$db->open();
			$rs = $db->get(null,$this->contactId, $this->ficheSuiviId, $this->remiseMaterielId, $this->actionId);
			$db->close();
			return new HistoriqueEncodages($rs);
		}
		
		private function setObjetId()
		{
			switch($this->objet)
			{
				case 'contact' : $this->contactId = $this->id; break;
				case 'action' : $this->actionId = $this->id; break;
				case 'formation' : $this->actionId = $this->id; break;
				case 'fichesuivi' : $this->ficheSuiviId = $this->id; break;
				case 'remisemateriel' : $this->remiseMaterielId = $this->id; break; 
			}	
		}
	}

?>
	