<?php
if(!$session->isLogged()) $session->redirect();
$errors = array();
$fieldserror = array();

if(isset($_REQUEST['upload']))
{
	REQUIRE_ONCE(SCRIPTPATH.'lib/upload.commons.php');
	
	if(isUploadOk('annexe'))
	{
		if(isExtensionOk($_FILES['annexe']['name']))
		{
			$temp = $_FILES['annexe']['tmp_name'];
			$filename = $_FILES['annexe']['name'];
			$size = filesize($temp);
			$path = getUploadPath();
			if(moveFile($temp, $path, $filename))
			{
				//Fichier uploader correctement ==>TRT
				REQUIRE_ONCE(SCRIPTPATH.'lib/reader.php');		
				$excell = new Spreadsheet_Excel_Reader();
				$excell->read($path.$filename);
				 for ($i = 1; $i <= $excell->sheets[0]['numRows']; $i++) {
					 for ($j = 1; $j <= $excell->sheets[0]['numCols']; $j++) {
					 	if(isset($excell->sheets[0]['cells'][$i][$j]))
						 echo "".$excell->sheets[0]['cells'][$i][$j]."<br>";
					 }
				 echo "\n";
				}				
				unlink($path.$filename);
			}
		}
		else
		{
			$errors[] = 'Le fichier que vous voulez import� n\'est pas autoris�';
		}
	}
	else
	{
		$errors[] = 'Veuillez fournir un fichier';
	}
}
REQUIRE_ONCE(SCRIPTPATH.'view/operateur_view_class.php');

$view = new OperateurView();
$shortcut = $view->raccourcisPanel();
$title = "Esprit d'entreprendre : Importation";
$menu = $view->createMenu();
$login = $view->createLogin();
$view->uploader($errors);
echo $view->FieldsError($fieldserror);

?>