<?php
$errors = array();
$fieldserror = array();
$notices = array();
$email = '';
$password = '';
$notices[] = '';

//TRT Formulaire login
if (isset($_REQUEST['forgot']))
{
	$notices[] = 'Le mail de rappel vous a été envoyé';
}

if (isset($_REQUEST['logout']))
{
	$session->disconnect();
}
elseif(isset($_POST['login_commit']))
{
	if(empty($_REQUEST['email']))
	{
		$errors[] = 'Veuillez indiquer votre adresse de courriel';
		$fieldserror[] = 'email';
	}
	else
	{
		if(!preg_match('#' . REGEX_MAIL . '#', $_REQUEST['email']))
		{
			$errors[] = 'Le format de votre adresse de courriel est incorrect';
			$fieldserror[] = 'email';
		}
	}

	if(empty($_POST['password']))
	{
		$errors[] = 'Veuillez indiquer votre mot de passe';
		$fieldserror[] = 'password';
	}

	if(empty($errors))
	{
		$email = $_POST['email'];
		$password = $_POST['password'];

		REQUIRE_ONCE(SCRIPTPATH.'model/ase_database_model_class.php');

		$DB = new AseDatabase();
		$user = $DB->login($_REQUEST['email'], $_REQUEST['password']);

		if($user)
		{
			if($user->isLoggable())
			{
				$session->disconnect();
				$session->setCurrentUser($user);
				$session->redirect();
			}
			else
			{
				$errors[] = 'Vous ne pouvez pas vous logguer sur ce compte';
			}
		}
		else
		{
			$errors[] = 'Aucun compte ne correspond ? ces informations';
		}
	}
}

REQUIRE_ONCE(SCRIPTPATH.'view/inscription_view_class.php');
$view = new InscriptionView();
$title = "Esprit d'entreprendre : Identification";

$view->loginScreen($email, $password,$errors,$notices);
echo $view->FieldsError($fieldserror);
?>