<?php
	if(!$session->isLogged()) $session->redirect();
	
	REQUIRE_ONCE(SCRIPTPATH.'view/materielPedagDetail_view_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/remiseMaterielPedagEtab_database_model_class.php');
	
	$ctrl = new MaterielPedagController($title, $shortcut, $menu, $login);
	$ctrl->Render();
?>

<?php
	class MaterielPedagController
	{
		private $view = null;
		private $title = null;
		
		public function __construct(&$title, &$shortcut, &$menu, &$login)
		{
			$this->redirectIfNeeded();
			
			$this->view = new MaterielPedagView();
			
			$this->title = &$title;
			$shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
			$menu = $this->view->createMenu();
			$login = $this->view->createLogin();
			
			$this->title = 'Mat�riel p�dagogique - Fiche n� ';
		}
		
		private function redirectIfNeeded()
		{
			$user = Session::GetInstance()->getCurrentUser();

			if ($user->isOperateur()) Mapping::RedirectTo('accueil');
		}
		
		public function Render()
		{
			if (isset($_REQUEST["id"]))
			{
				$this->id = $_REQUEST["id"];
				if (isset($this->id) && (is_numeric($this->id))) $this->RenderMaterielPedag();
				else Mapping::RedirectTo('accueil'); 
			}
		}
		
		private function RenderMaterielPedag()
		{
			$mp = $this->GetMaterielPadeg();

			if ($mp->count() > 0)
			{
				$this->title .= $mp->items(0)->GetRemiseMaterielId();
				$this->view->Render($mp->items(0));
			}
			else
			{
				$this->errors[]= 'La remise de mat�riel n\'existe pas !';
				$this->view->Errors($this->errors);
				
			}
		}

		private function GetMaterielPadeg()
		{
			$db = new RemiseMaterielPedagEtabDatabase();
			$db->open();
			$rs = $db->get($this->id);
			$db->close();
			
			return new RemiseMaterielPedagEtabs($rs);
		}
	}
?>

