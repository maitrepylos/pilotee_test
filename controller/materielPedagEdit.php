<?php
if(!$session->isLogged()) $session->redirect();

REQUIRE_ONCE(SCRIPTPATH.'view/materielPedagEdit_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/remiseMaterielPedagEtab_database_model_class.php');


$ctrl = new MaterielPedagController($title, $shortcut, $menu, $login);
$ctrl->render();
?>

<?php
class MaterielPedagController
{
	private $view = null;
	private $title = null;
	private $user = null;
	private $notices = array();
	private $errors = array();
	private $id = null;

	public function __construct(&$title, &$shortcut, &$menu, &$login)
	{
		$this->redirectIfNeeded();
		$this->user = Session::GetInstance()->GetCurrentUser();
		$this->view = new MaterielPedagEditView();
		$this->title = &$title;
		$shortcut = $this->view->raccourcisPanel();
		$menu = $this->view->createMenu();
		$login = $this->view->createLogin();
		$this->title = 'Mat�riel p�dagogique - Fiche n�';
	}

	private function redirectIfNeeded()
	{
		$user = Session::GetInstance()->getCurrentUser();

		if ($user->isOperateur()) Mapping::RedirectTo('accueil');
	}
	
	private function SetCloseCallBack()
	{
		$closeCallBack = "none";
		
		if (isset($_REQUEST['close'])) $closeCallBack = $_REQUEST['close'];
		elseif (isset($_POST['CloseCallBack'])) $closeCallBack = $_POST['CloseCallBack'];
		
		return $closeCallBack;
	}
	
	public function render()
	{
		if(isset($_REQUEST['action']))
		{
			
			if($_REQUEST['action'] == 'add')
			{
				$this->renderAdd();
			}
			elseif($_REQUEST['action'] == 'update')
			{
				$this->renderUpdate();
			}
			elseif($_REQUEST['action']=='delete')
			{
				$this->renderDelete();
			}	
		}
	}

	private function renderUpdate()
	{
		if(isset($_POST['updateForm']))
		{
			$this->insertUpdate();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->render();
		}
		else
		{
			if(isset($_REQUEST['updateId']) && isset($_REQUEST['etabId']) && is_numeric($_REQUEST['etabId']) && is_numeric($_REQUEST['updateId']))
			{
				$this->id = $_REQUEST['updateId'];
			
				$materielPedag = $this->GetMaterielPedag();
				if($materielPedag->count()>0)
				{
					$this->title .= $materielPedag->items(0)->GetRemiseMaterielId();
					$this->setPostMateriel($materielPedag);
					$this->view->Notices($this->notices);
					$this->view->Errors($this->errors);
					$this->view->render(true);
				}
				else	
				{
					$this->errors[]= 'Le mat�riel p�dagogique n\'existe pas !';
					$this->view->Errors($this->errors);
				}
			}
			else
			{
				Mapping::RedirectTo('accueil');
			}
			
		}
	}

	private function setPostMateriel($materielPedag)
	{
		$_POST['materielPedagId'] = $materielPedag->items(0)->GetRemiseMaterielId();
		$_POST['dateRemise'] = $materielPedag->items(0)->GetDate(true);

		//Contacts
		
		$materiaux = $materielPedag->items(0)->getMateriaux();
		$_POST['nbLigneMat'] = $materiaux->count(); 
		
		for($i=0; $i<$materiaux->count(); $i++)
		{
			$_POST['materiel_'.$i] = $materiaux->items($i)->getMaterielId();
			$_POST['quantite_'.$i] = $materiaux->items($i)->getQuantite();
		}
		
		$contact = $materielPedag->items(0)->GetContact();
		
		if($contact)
		{
			$_POST['contactIds'] = $contact->GetContactId();
			$_POST['contactName_0'] = $contact->GetNom(). ' ' .$contact->GetPrenom();
			$_POST['contactId_0'] = $contact->GetContactId();
			$_POST['nbLignesContact'] = '1';
		}
		else
		{
			$_POST['contactIds']='';
			$_POST['contactName_0'] = '';
			$_POST['contactId_0'] = '';
			$_POST['nbLignesContact'] = '0';
		}
		
		$_POST['etablissementId'] = $materielPedag->items(0)->GetEtablissementId();
		$dbEtab = new EtablissementDatabase();
		$dbEtab->open();
		$rs = $dbEtab->GetById($materielPedag->items(0)->GetEtablissementId());
		$etab= new Etablissements($rs);
		$dbEtab->close();
		$_POST['etablissementNom'] = $etab->items(0)->GetNom();
		
	}
	
	private function renderDelete()
	{
		if(isset($_REQUEST['deleteId']) && is_numeric($_REQUEST['deleteId']))
		{
			try
			{
				$idMateriel = $_REQUEST['deleteId'];
				$DB = new RemiseMaterielPedagEtabDatabase();
				$DB->open();
				$DB->startTransaction();
				$DB->deleteMateriel($idMateriel);
				$DB->delete($idMateriel);
				$DB->commitTransaction();
				$DB->close();
				Mapping::RedirectTo('etablissementDetail&id='.$_REQUEST['etab'].'&deletemateriel=0');
			}
			catch(Exception $e)
			{
				$DB->rollbackTransaction();
				$DB->close();
				Mapping::RedirectTo('etablissementDetail&id='.$_REQUEST['etab'].'&deletemateriel=1');
			}
		}
		else
		{
			Mapping::RedirectTo('accueil');
		}
	}
	
	private function renderAdd()
	{
		if(isset($_POST['addForm']))
		{
			$this->insertUpdate();
			$this->view->Notices($this->notices);
			$this->view->Errors($this->errors);
			$this->view->render();
		}
		else
		{
			
			$this->initialize();
			$this->view->render();
		}
	}
	
	
	private function initialize()
	{
		$_POST['dateRemise'] = date("d/m/Y");
	}
	
	private function insertUpdate()
	{
		//R�cup�ration des param�tres
		$etabId = $_REQUEST['etabId'];
		$date = (isset($_POST['dateRemise']))? $_POST['dateRemise'] : null;
		//$materiel = (isset($_POST['materiel']))?$_POST['materiel']:null; 
		//$quantite = (isset($_POST['quantite']))?$_POST['quantite']:null; 
		$nbContacts = !empty($_POST['nbLignesContact']) ?$_POST['nbLignesContact'] : null;
		$contacts = array();
		for($i=0; $i<$nbContacts; $i++)
		{
			if(isset($_POST['contactId_'.$i]))
				$contacts[] = array("id"=>$_POST['contactId_'.$i]);
		}
		
		
		$nbMat = !empty($_POST['nbLigneMat']) ?$_POST['nbLigneMat'] : null;
		$materiaux = array();
		for($i=0; $i<$nbMat; $i++)
		{
			if(isset($_POST['materiel_'.$i]))
				$materiaux[] = array("materiel"=>$_POST['materiel_'.$i], 'quantite'=>$_POST['quantite_'.$i]);
		}

		
		$db= new RemiseMaterielPedagEtabDatabase();
		try
		{
			$db->open();
			$db->startTransaction();
			$materielPeda = new RemiseMaterielPedagEtab();
			$materielId = (isset($_POST['materielPedagId']))?$_POST['materielPedagId']:null;
			
			$materielPeda->init($materielId, $etabId, $contacts[0]['id'], $date, date('Y-m-d H:i:s'), $this->user->getUtilisateurId(), date('Y-m-d H:i:s'), $this->user->getUtilisateurId());
			if(isset($_POST['updateForm']))
			{
				$db->deleteMateriel($materielId);
				$db->update($materielPeda);
				for($i=0; $i<count($materiaux); $i++)
				{
					$remiseParMat = new RemiseParMateriel();
					$remiseParMat->init(null,$materielId,$materiaux[$i]['materiel'],$materiaux[$i]['quantite']);
					$db->insertMateriel($remiseParMat);
				}
			}
			else
			{
				$materielId = $db->insert($materielPeda);
				for($i=0; $i<count($materiaux); $i++)
				{
					$remiseParMat = new RemiseParMateriel();
					$remiseParMat->init(null,$materielId,$materiaux[$i]['materiel'],$materiaux[$i]['quantite']);
					$db->insertMateriel($remiseParMat);
				}
				
			}
			
			$db->commitTransaction();
			$db->close();
			if(isset($_POST['updateForm']))
			{
				Mapping::RedirectTo('etablissementDetail&id='.$etabId.'&updatemateriel=0');
			}
			else $this->notices[]='La remise de mat�riel a �t� ajout� avec succ�s !';
			
		}
		catch(Exception $e)
		{
			if(isset($_POST['updateForm']))
			{
				Mapping::RedirectTo('etablissementDetail&id='.$etabId.'&updatemateriel=1');
			}
			else
				$this->error[]='Une erreur s\'est produite lors de l\'ajout de la remise de mat�riel  !';
			$db->rollbackTransaction();
			$db->close();
		}	
		
	}
	
	private function GetMaterielPedag()
	{
		$db = new RemiseMaterielPedagEtabDatabase();
		$db->open();
		$rs = $db->get($this->id, null);
		$db->close();
			
		return new RemiseMaterielPedagEtabs($rs);
	}
}
?>

