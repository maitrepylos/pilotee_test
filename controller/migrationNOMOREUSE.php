<?php
/*
 * obsol�te � �t� utilis� pour mettre � jour l'ancienne base de donn�es vers la nouvelle
 */

REQUIRE_ONCE(SCRIPTPATH.'model/old_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/new_database_model_class.php');

//Table ee_etablissement
//***************************************************************

$session = Session::GetInstance();
$user = $session->getCurrentUser();

try
{
	
/*
$DB_OLD = new OldDatabase();
$DB_OLD->open();
	$results = $DB_OLD->GetEtablissements();
$DB_OLD->close();

$DB = new NewDatabase();
$DB->open();
$DB->startTransaction();

	while ($row = mysqli_fetch_assoc($results))
	{
	    $adresse = new Adresse();
		$provinceId = getIdProvince($row['province']);
		$adresse->init(null, $provinceId, $row['adresse_etablissement'], 
							$row['code_postal'], $row['ville_etablissement'],
							$row['tel_etablissement'], null, $row['email_etablissement'], null,
							$row['fax_etablissement'], date('Y-m-d H:i:s'), $user->getUtilisateurId(),
							date('Y-m-d H:i:s'), $user->getUtilisateurId());
			
		$entiteAdmin = $row['id_admin_etab'];							
		if(isset($entiteAdmin))
			$entiteAdmin = substr($entiteAdmin, 5, strlen($entiteAdmin));
					
		//insert Adresse
		$adresseId = $DB->insertAdresse($adresse);

		$etablissement = new Etablissement();

		$etablissement->init($row['id_etablissement'], null, $row['nom_etablissement'], $adresseId, 
							 $row['site_etablissement'], getIdNiveau($row['niveau']), getIdReseau($row['reseau']), $entiteAdmin, 1, date('Y-m-d H:i:s'), $user->getUtilisateurId(),
						 	 date('Y-m-d H:i:s'), $user->getUtilisateurId());

		//insert Etablissement
		$DB->insertEtablissement($etablissement);
	
	}
*/

//Table ee_personne (contact)
//***************************************************************
/*
$DB_OLD = new OldDatabase();
	$results = $DB_OLD->GetContacts();
	$res = $DB_OLD->GetLienContactEtablissement();
$DB_OLD->close();

$EtabContact = null;
while (list($idPers, $idEtab) = mysql_fetch_array($res))
{
  $EtabContact[$idPers][] = $idEtab;
}

$DB = new NewDatabase();
$DB->open();
$DB->startTransaction();
while ($row = mysqli_fetch_assoc($results))
{
	
	    $adresse = new Adresse();
		$adresse->init(null, null, null, 
							null, null,
							$row['tel1_personne'], $row['tel2_personne'], $row['email1_personne'], $row['email2_personne'],
							$row['fax_personne'], date('Y-m-d H:i:s'), $user->getUtilisateurId(),
							date('Y-m-d H:i:s'), $user->getUtilisateurId());
							
		//insert Adresse
		$adresseId = $DB->insertAdresse($adresse);
						 	 
		$contactId = $row['id_personne'];
		
		$contact = new Contact();
		$contact->init($row['id_personne'], $row['nom_personne'], $row['prenom_personne'], $adresseId, 
							 0, 1, date('Y-m-d H:i:s'), $user->getUtilisateurId(),
						 	 date('Y-m-d H:i:s'), $user->getUtilisateurId());
		$DB->insertContact($contact);				 	 
		//Recherhce de correspondance pour titre et sp�cialit�
		//******************************************************************************
		$specialite = null;
		if($row['specialitee_personne']!=null)
			$specialite = $DB->getCorrespondanceSpecialite($row['specialitee_personne']);
		$titre = null;
		if($row['titre_personne']!=null)
			$titre = $DB->getCorrespondanceTitre($row['titre_personne']);
		//******************************************************************************
		
		//insert Contact
		$EtabContactArray  = $EtabContact[$contactId];
		for ($i=0;$i<count($EtabContactArray);$i++)
		{
			$contactEtab =new ContactEtablissement();
			 
			$contactEtab->init(null, $EtabContactArray[$i], $contactId, "AGENT" , $specialite, $titre, $row["participation"],
			 					date('Y-m-d H:i:s'), $user->getUtilisateurId(),
								date('Y-m-d H:i:s'), $user->getUtilisateurId());
							
			$DB->insertContactEtablissement($contactEtab);
		}
}

*/
//Table ee_materiel_pedag
//**********************************************************************
/*
$DB_OLD = new OldDatabase();
$DB_OLD->open();
	$results = $DB_OLD->getMaterielPedagogique();	
$DB_OLD->close();
	
$DB = new NewDatabase();
$DB->open();
$DB->startTransaction();

	while ($row = mysqli_fetch_assoc($results))
	{	
		$dateArray = explode("/", $row['date_depot_materiel']);		
		$jour = $dateArray[0];
		if(strlen($dateArray[0])<2 && strlen($dateArray[0])>0)
			$jour  ="0".$dateArray[0];
			
		$mois = $dateArray[1];	
		if(strlen($dateArray[1])<2 && strlen($dateArray[1])>0)
			$mois  ="0".$dateArray[1];
		
		$annee = $dateArray[2];	
		if(strlen($dateArray[2])<4 && strlen($dateArray[2])>0)
			$annee  ="20".$dateArray[2];
			
		$date = $jour."/".$mois."/".$annee;
		
		$d = UnformatDate($date);
		
		$materiel = new RemiseMaterielPedagEtab();
		$materiel->init($row['id_materiel'], 'CAEE', $row['id_etablissement'], $row['id_personne'], $d,
						$row['quantite_materiel'],date('Y-m-d H:i:s'), $user->getUtilisateurId(),
						date('Y-m-d H:i:s'), $user->getUtilisateurId());

		$DB->insertMaterielPedagogique($materiel);						
	
	}
*/
//Table ee_evaluation_etab
//Table ee_est_documentee_par
/*
$DB_OLD = new OldDatabase();
$DB_OLD->open();
	$results = $DB_OLD->getFicheSuivi();
	$res = $DB_OLD->getLienFicheContact();	
$DB_OLD->close();

$SuiviContact = null;
while (list($idSuivi, $idPersonne) = mysql_fetch_array($res))
{
 $SuiviContact[$idSuivi][] = $idPersonne;
}

$DB = new NewDatabase();
$DB->open();
$DB->startTransaction();
while ($row = mysqli_fetch_assoc($results))
{
	$dateArray = explode("/", $row['date_entretien']);
	$jour = $dateArray[0];
	if(strlen($dateArray[0])<2 && strlen($dateArray[0])>0)
		$jour  ="0".$dateArray[0];
			
	$mois = $dateArray[1];	
	if(strlen($dateArray[1])<2 && strlen($dateArray[1])>0)
		$mois  ="0".$dateArray[1];
		
	$annee = $dateArray[2];	
	if(strlen($dateArray[2])<4 && strlen($dateArray[2])>0)
		$annee  ="20".$dateArray[2];
			
	$date = $jour."/".$mois."/".$annee;
	$d = UnformatDate($date);
    				
	$suivi = new FicheSuivi();
	$suivi->init($row['id_suivi_aee'],getIdModalite($row['modalite_contact']),$row['id_etablissement'],
				 getIdIntention($row['intentionalite']),getIdTiming($row['approche']), $row['evaluation_action'],$d,
				 date('Y-m-d H:i:s'), $user->getUtilisateurId(),date('Y-m-d H:i:s'), $user->getUtilisateurId());
				 
	$DB->insertFicheSuivi($suivi);
	$idSuivi = $row['id_suivi_aee'];
	//insert Contact
	$SuiviContactArray = $SuiviContact[$row['id_suivi_aee']];
	for ($i=0;$i<count($SuiviContactArray);$i++)
	{
		$ficheContact =new FicheSuiviContact();
		$ficheContact->init($SuiviContactArray[$i], $idSuivi,
		 					date('Y-m-d H:i:s'), $user->getUtilisateurId(),
							date('Y-m-d H:i:s'), $user->getUtilisateurId());
		$DB->insertFicheSuiviContact($ficheContact);
	}
}
*/


//dic_action_label
$DB_OLD = new OldDatabase();
$DB_OLD->open();
	$results = $DB_OLD->GetDistinctActionLabel();
$DB_OLD->close();

$DB = new NewDatabase();
	$DB->open();
$DB->startTransaction();
$ordre = 1;
while ($row = mysqli_fetch_assoc($results))
{
	$dicAction =  new ActionLabel();
	$id = strtoupper(substr($row['nom_action_label'],0,5));
	$operateurId = 1; //!!!!!!!!!!!!!!!!! Op�rateur bidon !!!!!!!!!!!!!!!!!
	$dicAction->init($id, $operateurId, $row['nom_action_label'],null, $ordre, 1);
	
	//$actionLabelId, $operateurId, $label, $anneeId, $ordre, $swActif 
	
	$DB->insertDicActionLabel($dicAction);
	$ordre++;
}
	
/*
//ee_est_titulaire_de ==>Lien entre agent et �tablissement
$DB_OLD = new OldDatabase();
$DB_OLD->open();
	$results = $DB_OLD->GetAgentEtab();
	$res = $DB_OLD->GetUtilisateurs();
$DB_OLD->close();

$utilisateurs = null;
while (list($idSession, $emailUser, $nom, $prenom) = mysql_fetch_array($res))
{
 $utilisateurs[strtolower($emailUser)][] = $nom;
 $utilisateurs[strtolower($emailUser)][] = $prenom; 
}


$DB = new NewDatabase();
	$DB->open();
$DB->startTransaction();

while ($row = mysqli_fetch_assoc($results))
{
	$utilisateurId = strtolower($row['email_utilisateur']);
	$etabId = $row['id_etablissement'];
	
	$idAgent = $DB->userExist($utilisateurId);
	
	if($idAgent==null)
		$idAgent = $DB->insertAgent($utilisateurId, $utilisateurs[$utilisateurId][0], $utilisateurs[$utilisateurId][1], null);
		
	$DB->updateAgentEtablissement($etabId, $idAgent);
}
*/

/*
$DB_OLD = new OldDatabase();
$DB_OLD->open();
	$results = $DB_OLD->GetActionLabel();
	$res = $DB_OLD->GETAnime2();
	$res2 = $DB_OLD->GetParticipe2();
$DB_OLD->close();
$DB = new NewDatabase();
	$DB->open();
$DB->startTransaction();

$anime2 = null;
while (list($idContact, $idAction) = mysql_fetch_array($res))
{
 $anime2[$idAction][] = $idContact;
}

$participe2 = null;
while (list($idEtab, $idAction) = mysql_fetch_array($res2))
{
 $participe2[$idAction][] = $idEtab;
}

while ($row = mysqli_fetch_assoc($results))
{
 	$idAction = $row['id_action_label'];
 	$anneeId = 'INCON';
 	if(isset($row['annee_action_label']) && $row['annee_action_label'] !='')
 		$anneeId = $row['annee_action_label'];
 	
 	
 	$labelId = 'INCON';
 	if(isset($row['nom_action_label']) && $row['nom_action_label']!='')
		$labelId = $DB->getIdActionLab($row['nom_action_label']);
	$nomSpec = $row['intitule_spec_action_label'];
	
	$typeActionId = 'LABEL';
	
	$nbParticipant = 0;
	$nbApprenant = 0;
	$commentaire = null;
	
	$pedagogique = 0; 
	if(isset($row['peda']) && $row['peda']!= '')
	{
		if($row['peda']=='OUI')
			$pedagogique = 1;
		elseif($row['peda']=='NON')
			$pedagogique = 0;
	}
	$statutActionId = 'VALID';
	
	$action = new Action(); 
	//29
	$action->init(
				  $idAction, 'OBTEN', $labelId, null, null, $typeActionId, $anneeId, $statutActionId,
				  null,null, null, null, $nomSpec, $commentaire, 0, $pedagogique, 0, 0, null, null, null, null, null,
				  null, null, date('Y-m-d H:i:s'), $user->getUtilisateurId(), date('Y-m-d H:i:s'), $user->getUtilisateurId()
				  );
	$DB->insertAction($action);
	
	$actionSection = new ActionSectionAction();
	$sectionId = getSection($row['section']);
	$actionSection->init($idAction, $sectionId, 0 );
	$DB->insertActionSection($actionSection);
	
	$actionContact = new ActionContact();
	$idPersonne = $row['id_personne'];
	
	$actionContact->init($idAction, $idPersonne, 'RESP', date('Y-m-d H:i:s'), $user->getUtilisateurId(), date('Y-m-d H:i:s'), $user->getUtilisateurId());
	$DB->insertActionContact($actionContact);
	
	$anime2Array = $anime2[$idAction];
	for ($i=0;$i<count($anime2Array);$i++)
	{
		$actionContact = new ActionContact();
		$idContact = $anime2Array[$i];
		echo $idContact."<br/>"; 
		if($idPersonne != $idContact)
		{
			$actionContact->init($idAction, $idContact, 'ANIM', date('Y-m-d H:i:s'), $user->getUtilisateurId(), date('Y-m-d H:i:s'), $user->getUtilisateurId());
			$DB->insertActionContact($actionContact);
		}
	}	
	
	$participe2Array = $participe2[$idAction];
	for ($i=0;$i<count($participe2Array);$i++)
	{
		$actionEtab = new ActionEtablissement();
		$idEtab = $participe2Array[$i];
 
		$actionEtab->init($idAction, $idEtab, date('Y-m-d H:i:s'), $user->getUtilisateurId(), date('Y-m-d H:i:s'), $user->getUtilisateurId());
		$DB->insertActionEtablissement($actionEtab);
	}	
}
/*
 * 
 * 
 * 
 * 
 */
/*
$DB_OLD = new OldDatabase();
$DB_OLD->open();
	$results = $DB_OLD->GetProjet();
	$res = $DB_OLD->GetProjetEtab();
	$res2 = $DB_OLD->GetProjetContact();
$DB_OLD->close();


$anime3 = null;
while (list($idAction, $idContact, $annee) = mysql_fetch_array($res2))
{
	$id = $idAction."||".$annee;
 $anime3[$id][] = $idContact;
}

$aDepose = null;
while (list($idAction, $idEtab, $annee ) = mysql_fetch_array($res))
{
 $id = $idAction."||".$annee;
 $aDepose[$id][] = $idEtab;
}

$DB = new NewDatabase();
$DB->open();
$DB->startTransaction();

while ($row = mysqli_fetch_assoc($results))
{
 	$idActionOld = $row['id_bourse'];
 	
 	$anneeId = 'INCON';
 	if(isset($row['annee_depot_bourse']) && $row['annee_depot_bourse'] !='')
 		$anneeId = $row['annee_depot_bourse'];
 		
 	$categorieBourse = $row['categorie_bourse'];
 	$intituleSpec = $row['intitule_spec_bourse'];
 	$sectionId = getSection($row['section']);
	$pedagogique = 0; 
	if(isset($row['peda']) && $row['peda']!= '')
	{
		if($row['peda']=='OUI')
			$pedagogique = 1;
		elseif($row['peda']=='NON')
			$pedagogique = 0;
	}
	
	$subvention = getSubvention($row['obtention_bourse']);
	$projetRealise = $row['projet_realise'];
	$collaboration = $row['collaboration_inter_etab'];
	$montantDem = $row['montant_demande'];
	$montantSubvention = $row['subvention'];
 	$id_personne = $row['id_personne'];
	$typeActionId = 'BOUPI';
	$nbParticipant = 0;
	$nbApprenant = 0;
	$commentaire = null;
	$statutActionId = 'VALID';
		
	$action = new Action(); 
	
	$action->init(
				  null, $subvention, null, null, null, $typeActionId, $anneeId, $statutActionId,
				  null,null, null, $intituleSpec, null, $commentaire, 0, $pedagogique, 0, 0, null, $montantDem, $montantSubvention, $categorieBourse, $projetRealise,
				  null, $collaboration, date('Y-m-d H:i:s'), $user->getUtilisateurId(), date('Y-m-d H:i:s'), $user->getUtilisateurId()
				  );
	$idActionNew = $DB->insertAction2($action);
	
	$actionSection = new ActionSectionAction();

	$actionSection->init($idActionNew, $sectionId, 0 );
	$DB->insertActionSection($actionSection);
	
	$actionContact = new ActionContact();
	
	
	$actionContact->init($idActionNew, $id_personne, 'RESP', date('Y-m-d H:i:s'), $user->getUtilisateurId(), date('Y-m-d H:i:s'), $user->getUtilisateurId());
	$DB->insertActionContact($actionContact);
	
	$anime3Array = $anime3[$idActionOld.'||'.$anneeId];
	for ($i=0;$i<count($anime3Array);$i++)
	{
		$actionContact = new ActionContact();
		$idContact = $anime3Array[$i];
		 
		if($id_personne != $idContact)
		{
			$actionContact->init($idActionNew, $idContact, 'ANIM', date('Y-m-d H:i:s'), $user->getUtilisateurId(), date('Y-m-d H:i:s'), $user->getUtilisateurId());
			$DB->insertActionContact($actionContact);
		}
	}	
	
	$aDeposeArray = $aDepose[$idActionOld.'||'.$anneeId];
	for ($i=0;$i<count($aDeposeArray);$i++)
	{
		$actionEtab = new ActionEtablissement();
		$idEtab = $aDeposeArray[$i];
 
		$actionEtab->init($idActionNew, $idEtab, date('Y-m-d H:i:s'), $user->getUtilisateurId(), date('Y-m-d H:i:s'), $user->getUtilisateurId());
		$DB->insertActionEtablissement($actionEtab);
	}
}
*/
	
//Entite administrative
/*
$DB_OLD = new OldDatabase();
$DB_OLD->open();
	$resEntiteAdmin = $DB_OLD->GetEntiteAdmin();
$DB_OLD->close();

$DB = new NewDatabase();
$DB->open();
$DB->startTransaction();
while ($row = mysqli_fetch_assoc($resEntiteAdmin))
{
	$entiteAdmin = $row['id_admin_etab'];
	echo 'entiteAdmin ==>'.$entiteAdmin; 							
	if($entiteAdmin!=null && $row['nom_admin_etab']!=null)
	{
		echo 'coucou';
		$adresse = new Adresse();
		$provinceId = getIdProvince($row['province']);
		$adresse->init(null, $provinceId, $row['adresse_etablissement'], 
								$row['code_postal'], $row['ville_etablissement'],
								$row['tel_etablissement'], null, $row['email_etablissement'], null,
								$row['fax_etablissement'], date('Y-m-d H:i:s'), $user->getUtilisateurId(),
								date('Y-m-d H:i:s'), $user->getUtilisateurId());
		//insert Adresse
		$adresseId = $DB->insertAdresse($adresse);

		$entite = new EntiteAdmin();
		$entiteAdmin = substr($entiteAdmin, 5, strlen($entiteAdmin));
 		$entite->init($entiteAdmin,$row['nom_admin_etab'], $adresseId, $row['email_etablissement'],1, date('Y-m-d H:i:s'), $user->getUtilisateurId(), date('Y-m-d H:i:s'), $user->getUtilisateurId());
		$DB->insertEntiteAdmin($entite);
	}
	
}
*/
$DB->commitTransaction();
$DB->close();
}
catch(Exception $e)
{
	echo $e;
	$DB->rollbackTransaction();
	$DB->close();			
}


//***************************************************************
	function getIdProvince($provinceLab)
	{
		$provinceId = 0;
 		switch($provinceLab)
 		{
 			case "BRABANT WALLON" :
 								$provinceId = "BWA";break;
 								
 			case "NAMUR" :
 								$provinceId = "NAM";break;
 								
 			case "HAINAUT" :
 								$provinceId = "HAI";break;
 								 					 	
 			case "LIEGE" :
 								$provinceId = "LIE";break;
 								
 			case "LUXEMBOURG" :
 								$provinceId = "LUX";break; 								
 								
 		}
 		return $provinceId;
	
	}
	
	function getSection($lab)
	{
		if($lab!='')
		{
			$section = 'INCON';
			switch($lab)
			{
			case 'ECO' : $section ='ECO'; break;
			case 'NONECO' : $section ='NECO'; break;
			case 'INDEF' : $section ='NECO'; break;
			}
		
			return $section;
		}
		else
			return 'NECO';
	}
	
	
	function getIdReseau($reseauLab)
	{
		$reseauId = 0;
 		switch($reseauLab)
 		{
 			case "COM FRAN" :
 								$reseauId = "COMFR";break;
 								
 			case "LIBRECONF" :
 								$reseauId = "LCONF";break;
 								
 			case "SUBVPROV" :
 								$reseauId = "SUBVP";break;
 								
 			case "SUBVCOM" :  								
 								$reseauId = "SUBVC";break;
 								 					 	
 			case "LIBRENONCONF" :
 								$reseauId = "LNCON";break;
 								
 			case "COMFRAN" :
 								$reseauId = "COMFR";break;
 								
 			default :  			$reseauId = "INCON";break;					
 								
 		}
 		return $reseauId;
	}
	
	function getIdNiveau($niveauLab)
	{
		
		$niveauId = 0;
 		switch(strtolower($niveauLab))
 		{
 			case "secondaire" :
 								$niveauId = "SECON";break;
 								
 			case "primaire" :
 								$niveauId = "PRIMA";break;
 								
 			case "superieur" :
 								$niveauId = "SUPER";break;
 								
 			case "suphte" :
 								$niveauId = "SUPER";break;
 								
 			case "supa" :
 								$niveauId = "SUPER";break;
 								
 			case "supuniv" :
 								$niveauId = "SUPER";break;
 			case 'sup' :
 								$niveauId = "SUPER";break;
 		}
 		return $niveauId;
	}
	
	function getIdModalite($modalite)
	{
		$modaliteId = null;
 		switch($modalite)
 		{
 			case "t�l + mail" :
 								$modaliteId = "TEL"; break;
 			case "t�l+mail" :
 								$modaliteId = "TEL"; break;
 			case "T�l�phone" :
 								$modaliteId = "TEL"; break;
 			case "t�l" :
 								$modaliteId = "TEL"; break; 								
 			case "Mail" :
 								$modaliteId = "MAIL"; break;
 			case "rdv" :
 								$modaliteId = "RDV"; break;
 			case "RDV bourse" :
 								$modaliteId = "RDV"; break;
 			case "Rencontre_fortuite" :
 								$modaliteId = "RENCF"; break;
 			case "Rencontre fortuite" :
 								$modaliteId = "RENCF"; break;
 			case "visite sur place" :
 								$modaliteId = "RENCF"; break;
 								 								
 			default:			$modaliteId = null; break; 								
 								
 		}
 		return $modaliteId;
	}
	
	
	function getSubvention($sub)
	{
		$subId  =null;
	 	switch($sub)
 		{
 			case "OUI" :
 								$subId = "OBTEN"; break;
 			case "NON" :
 								$subId = "REJET"; break;
 			
 			default:			$subId = 'EVALU'; break; 								
 								
 		}
		return $subId; 
	}
	
	function getIdIntention($intention)
	{
		$intentionId = null;
 		switch($intention)
 		{
 			case "Gener" :
 								$intentionId = "GEN"; break;
 			case "General" :
 								$intentionId = "GEN"; break;
 			case "Actio" :
 								$intentionId = "ACTIO"; break;
 			case "Action" :
 								$intentionId = "ACTIO"; break;
 			case "Bours" :
 								$intentionId = "BOURS"; break;
 			case "Bourse" :
 								$intentionId = "BOURS"; break;
 			case "Appro" :
 								$intentionId = "APPRO"; break;
 			case "Approche_opport" :
 								$intentionId = "APPRO"; break;
 								 								
 			default:			$intentionId = null; break; 								
 								
 		}
 		return $intentionId;
	}	
	
	function getIdTiming($timing)
	{
		$timingId = null;
 		switch($timing)
 		{
 			case "nouveau" :
 								$timingId = "APPR1"; break;
 			case "suivi" :
 								$timingId = "SUIV"; break;
 			default:			$timingId = null; break; 								
 								
 		}
 		return $timingId;
	}	
	
?>