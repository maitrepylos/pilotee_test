<?php
if(!$session->isLogged()) $session->redirect();
REQUIRE_ONCE(SCRIPTPATH.'view/operateur_view_class.php');

$view = new OperateurView();
$ec = new OperateurController($view);
$ec->Render();
class OperateurController
{
	private $view;
	private $user;
	
	public function __construct($view)
	{
		 $this->view = $view;
		 $this->user = Session::GetInstance()->getCurrentUser();
	}

	public function Render()
	{
		$ops = $this->GetListOperateur();
		$this->view->viewOperateurList($ops, $this->SetCloseCallBack());
	}
	
	private function GetListOperateur()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/operateur_database_model_class.php');
		$db = new OperateurDatabase();
		$db->open();
		$rs = $db->get();
		$db->close();
		return new Operateurs($rs);
	}
	
	private function SetCloseCallBack()
	{
		$closeCallBack = "none";
		
		if (isset($_REQUEST['close'])) $closeCallBack = $_REQUEST['close'];
		elseif (isset($_POST['CloseCallBack'])) $closeCallBack = $_POST['CloseCallBack'];
		
		return $closeCallBack;
	}	
}
?>