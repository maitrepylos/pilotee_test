<?php

    if(!$session->isLogged()) $session->redirect();

	REQUIRE_ONCE(SCRIPTPATH.'view/operateurDetail_view_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
	REQUIRE_ONCE(SCRIPTPATH.'model/operateur_database_model_class.php');
	
	$ctrl = new OperateurDetailController($title, $shortcut, $menu, $login, $notices);
	$ctrl->Render();

	class OperateurDetailController
	{
		private $view = null;
		private $title = null;
		private $notices = null;
		private $errors = null;
		private $id = 0; 
		
		public function __construct(&$title, &$shortcut, &$menu, &$login, &$notices)
		{
			$this->view = new OperateurDetailView();
			$this->notices = array();
			$this->errors = array();
			$this->title = &$title;
			$shortcut = $this->view->raccourcisPanel();
			$this->view->Notices($this->notices);
			$menu = $this->view->createMenu();
			$login = $this->view->createLogin();
			$this->title = 'Opérateur : ';
		}
		
		
		public function Render()
		{
			if (isset($_REQUEST["id"]))
			{
				$this->id = $_REQUEST["id"];
				if (isset($this->id) && (is_numeric($this->id)))
					 $this->RenderOperateur();
				else 
					Mapping::RedirectTo('accueil'); 
			}
		}
		
		private function RenderOperateur()
		{
			$ets = $this->GetOperateur();
			if($ets->count()>0)
			{
				$this->title .= $ets->items(0)->GetNom();
				$this->view->Render($ets->items(0));
			}
			else
			{
				$this->errors[]= 'L\'opérateur n\'existe pas !';
				$this->view->Errors($this->errors);
			}
		}
			
		public function GetOperateur() 
		{ 
			$db = new OperateurDatabase();
			$db->open();
			$rs = $db->get($this->id);
			$db->close();
			$op = new Operateurs($rs);
			return $op;
		}
	}

?>
	