<?php
	if(!$session->isLogged()) $session->redirect();

	REQUIRE_ONCE(SCRIPTPATH.'view/popup_google_maps_view_class.php');
	
	$ctrl = new PopupGoogleMapsController();
	$ctrl->Render();
?>

<?php
	class PopupGoogleMapsController
	{
		private $view = null;

		public function __construct()
		{
			$this->view = new PopupGoogleMapsView();
		}
		
		public function Render()
		{
			//$ids = preg_split("/[,]+/", $_REQUEST['ids']);
			
			$this->view->Render($this->GetEtablissements($_REQUEST['ids']));
		}
		
		private function GetEtablissements($ids)
		{
			$db = new EtablissementDatabase();
			$db->open();
			$rs = $db->GetByIds($ids);
			$db->close();
			
			return new Etablissements($rs);
		}
		
	}
?>
	