<?php

	$errors = array();
	$fieldserror = array();

	if(isset($_REQUEST['back']))
	{
		Mapping::RedirectTo('login');
	}
	
	if(isset($_REQUEST['register_commit']))
	{
		if(!isset($_REQUEST['email']))
		{
			$errors[] = 'Veuillez indiquer une adresse de courriel';
			$fieldserror[] = 'email';
		}
		else
		{
			if(!preg_match('#' . REGEX_MAIL . '#i', $_REQUEST['email']))
			{
				$errors[] = 'Le format de l\'adresse de courriel est invalide';
				$fieldserror[] = 'email';
			}
		}
	
		if(empty($_POST['nom']))
		{
			$errors[] = 'Veuillez indiquer votre nom';
			$fieldserror[] = 'nom';
		}
		if(empty($_POST['prenom']))
		{
			$errors[] = 'Veuillez indiquer votre prenom';
			$fieldserror[] = 'prenom';
		}
		$mdpSet = false;
		if(empty($_POST['mdp']))
		{
			$errors[] = 'Veuillez indiquer un mot de passe';
			$fieldserror[] = 'mdp';
			$mdpSet = false;
		}
		else $mdpSet = true;
		
		if(empty($_POST['confirmn_mdp']))
		{
			$mdpSet = false;
			$errors[] = 'Veuillez confirmer le mot de passe';
			$fieldserror[] = 'confirmn_mdp';
		}else $mdpSet = true;
		
		if($mdpSet)
		{
			if($_POST['confirmn_mdp'] != $_POST['mdp'])
			{
				$errors[] = 'Le mot de passe et la confirmation du mot de passe ne sont pas identiques';
				$fieldserror[] = 'confirmn_mdp';
				$fieldserror[] = 'mdp';
			}	
		}
		
	}

	REQUIRE_ONCE(SCRIPTPATH.'view/inscription_view_class.php');
	$title = "Esprit d'entreprendre : Inscription";
	$view = new InscriptionView();
	$view->registerBox($_POST["email"],$_POST["nom"],$_POST["prenom"],$_POST["mdp"],$_POST['confirmn_mdp'],$errors);
	echo $view->FieldsError($fieldserror);
?>