<?php
if(!$session->isLogged()) $session->redirect();

REQUIRE_ONCE(SCRIPTPATH.'view/report_activiteEncodage_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/report_action_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/report_bourse_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/report_journal_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/report_contact_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/report_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/report_etablissements_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/bourse_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/historiqueEncodage_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/ficheSuivi_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/contact_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/formation_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/listing_etablissements_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/listing_action_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/listing_contact_view_class.php');
REQUIRE_ONCE(SCRIPTPATH.'view/listing_formation_view_class.php');

$ctrl = new ReportingController($title, $shortcut, $menu, $login);
$ctrl->Render();

class ReportingController
{
	private $view = null;
	private $id = 0;
	private $user;

	private $title = null;
	private $shortcut = null;
	private $menu;
	private $login;

	public function __construct(&$title, &$shortcut, &$menu, &$login)
	{
		$this->user = Session::GetInstance()->GetCurrentUser();
			
		$this->title = &$title;
		$this->shortcut = &$shortcut;
		$this->menu = &$menu;
		$this->login = &$login;
	}

	public function Render()
	{
		if(isset($_REQUEST['type']))
		{
			switch ($_REQUEST['type'])
			{
				case 'activite' :
					$this->RenderReportActivite();
					break;
				case 'etablissement' :
					$this->RenderReportEtablissements();
					break;
				case 'action' :
					$this->RenderReportAction();
					break;
				case 'bourse' :
					$this->RenderReportBourse();
					break;
				case 'journal' :
					$this->RenderReportJournal();
					break;
				case 'contact' :
					$this->RenderReportContact();
					break;
				case 'listing_etablissement':
					$this->RenderListingEtablissements();
					break;
				case 'listing_action':
					$this->RenderListingAction();
					break;
				case 'listing_contact':					
					$this->RenderListingContact();
					break;
				case 'listing_formation':					
					$this->RenderListingFormation();
					break;
				default :
					$this->RenderDefaultView();
			}
		}
		else
			$this->RenderDefaultView();
	}

	private function RenderReportContact()
	{
		$this->view = new ReportContactView();
		$this->title = 'Rapport Qualification des contacts';
		$this->shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->menu = $this->view->createMenu();
		$this->login = $this->view->createLogin();

		$db = new ContactDatabase();
		$db->open();
		$res = $db->getRapport();
		$db->close();
		$session = Session::GetInstance();
		$session->set('contact', $res);
		$this->view->renderList($res);
			
	}


	private function RenderReportJournal()
	{
		$this->view = new ReportJournalView();
		$this->title = 'Rapport journal de classe';
		$this->shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->menu = $this->view->createMenu();
		$this->login = $this->view->createLogin();
			
		if(isset($_POST['report_journal']))
		{
			$this->view->Render();
			$this->searchJournal();
		}
		else
		{
			$this->Initialize();
			$this->view->Render();
		}
	}


	private function searchJournal()
	{
		$agent = ($this->user->isAgent())? $this->user->getUtilisateurId() : $_POST['agent'];
		$dateAnte = (!empty($_POST['annee_ante'])) ? $_POST['annee_ante']:null;
		$datePost= (!empty($_POST['annee_post'])) ? $_POST['annee_post']:null;
			
		$db = new FicheSuiviDatabase();
		$db->open();
		$fiches = new FicheSuivis($db->getRapport($agent, $dateAnte, $datePost));
		$db->close();
			
		$session = Session::GetInstance();
		$session->set('journal', $fiches);
		$session->set('PostJournal', $_POST);
			
		if(isset($fiches) && $fiches->count()>0)
			$this->view->renderList($fiches);
		else
			$this->view->renderNoResult();

	}

	private function RenderReportBourse()
	{
		$this->view = new ReportBoursesView();
		$this->title = 'Rapport sur les projets entrepreneuriaux';
		$this->shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->menu = $this->view->createMenu();
		$this->login = $this->view->createLogin();
			
		if(isset($_POST['report_bourse']))
		{
			$this->view->Render();
			$this->searchBourse();
		}
		else
		{
			$this->initializeReportBourse();
			$this->view->Render();
		}
	}

	private function searchBourse()
	{
		$projetInno = $_POST['projetInnovant'];
		$sub_obtenu = (isset($_POST['sub_obtenu']))?true : false;
		$sub_refus  = (isset($_POST['sub_refus']))?true : false;
		$sub_eval = (isset($_POST['sub_eval']))?true:false;
			
		$anneeId = (isset($_POST['anneesScolaire'])) ? $_POST['anneesScolaire'] : null;
		$entiteAdmin = (!empty($_POST['entiteAdmin'])) ? $_POST['entiteAdmin'] : null;
		$etablissement = (!empty($_POST['etablissement'])) ? $_POST['etablissement'] : null;
		$cp_1 = ((isset($_POST['code_postal_1']) && (strlen($_POST['code_postal_1']) > 0))) ? $_POST['code_postal_1'] : null;
		$cp_2 = ((isset($_POST['code_postal_2']) && (strlen($_POST['code_postal_2']) > 0))) ? $_POST['code_postal_2'] : null;
		//$global =(isset($_POST['global'])) ? $_POST['global'] : null;
		$global = null;
			
		$provincesArray = array();
		$arrondissementsArray = array();
		$reseauxArray = array();
		$niveauxArray = array();
			
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, 'chk_') === 0)
			{
				$k = str_replace('chk_', '', $key);
				if (strpos($k, 'pro_') === 0) array_push($provincesArray, str_replace('pro_', '', $k));
				elseif (strpos($k, 'niv_') === 0) array_push($niveauxArray, str_replace('niv_', '', $k));
				elseif (strpos($k, 'arr_') === 0) array_push($arrondissementsArray, str_replace('arr_', '', $k));
				elseif (strpos($k, 'res_') === 0) array_push($reseauxArray, str_replace('res_', '', $k));
			}
		}
			
		$db = new EtablissementDatabase();
		$db->open();
		$etablissements = new Etablissements($db->report(null, $anneeId, null, $entiteAdmin, $etablissement, $cp_1, $cp_2, $provincesArray, $arrondissementsArray, $reseauxArray, $niveauxArray));
		// $db->close();
			
		$db = new BourseDatabase();
		$db->open();
		$actions = new Actions($db->getRapport($projetInno, $anneeId, $global, $sub_obtenu, $sub_refus, $sub_eval, $etablissements));
		// $db->close();
			
		$session = Session::GetInstance();
		$session->set('bourses', $actions);
		$session->set('PostBourse', $_POST);
			
		if(isset($actions) && $actions->count()>0)
			$this->view->renderList($actions);
		else
			$this->view->renderNoResult();
			
	}

	private function initializeReportBourse()
	{
		$_POST['anneesScolaire']= $this->getAnneeEnCours()->items(0)->getId();
		$_POST['sub_obtenu']='on';
	}

	private function RenderReportAction()
	{
		$this->view = new ReportActionsView();
		$this->title = 'Rapport sur les actions';
		$this->shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->menu = $this->view->createMenu();
		$this->login = $this->view->createLogin();
			
		if(isset($_POST['report_action']))
		{
			// load operateurs
			$this->loadOperateurByYear(((isset($_POST['anneesScolaire'])) ? $_POST['anneesScolaire'] : null));
			$this->view->Render();
			$this->searchAction();
		}
		else
		{
			$this->initializeReportAction();
			$this->view->Render();
		}
	}

	private function initializeReportAction()
	{
		$_POST['anneesScolaire']= $this->getAnneeEnCours()->items(0)->getId();
		// $_POST["actionLab"]='on';
		// $_POST["actionNonLab"]='on';
		// $_POST["actionMicro"]='on';

		// load operateurs
		$this->loadOperateurByYear($_POST['anneesScolaire']);
	}

	private function RenderDefaultView()
	{
		$v = new ReportView();
			
		$this->title = 'Rapports';
		$this->shortcut = $v->raccourcisPanel($_REQUEST['module']);
		$this->menu = $v->createMenu();
		$this->login = $v->createLogin();
			
		$v->Render();
	}

	private function getAnneeEnCours()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/annee_database_model_class.php');
		$DB_ANNEE = new AnneeDatabase();
		$annees = new Annees($DB_ANNEE->GetAnneeEnCoursId());
		return $annees;
	}

	private function RenderReportActivite()
	{
		$this->view = new ReportActiviteEncodageView();
		$this->title = 'Rapport d\'activité d\'encodage';
		$this->shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->menu = $this->view->createMenu();
		$this->login = $this->view->createLogin();
			
			
		if(isset($_POST['searchEncodage']))
		{
			$agent=null; $operateur = null; $dateAnte = null; $datePost = null;

			if($this->user->isAgent())
			{
				$agent = $this->user->getUtilisateurId();
				$operateur = null;
			}
			elseif($this->user->isAdmin() || $this->user->isCoordinateur() || $this->user->isAgentCoordinateur())
			{
				$agent = $_POST['agent'];
				$operateur = $_POST['operateur'];
			}
			else
				$operateur = $this->user->getOperateurId();

			if(!empty($_POST['annee_ante'])) $dateAnte = $_POST['annee_ante'];
			if(!empty($_POST['annee_post'])) $datePost= $_POST['annee_post'];


			$typeObjet = array();
			if(isset($_POST['contactCheck'])) $typeObjet[] = $_POST['contactCheck'];
			if(isset($_POST['actionCheck'])) $typeObjet[] = $_POST['actionCheck'];
			if(isset($_POST['suivisCheck'])) $typeObjet[] = $_POST['suivisCheck'];
			if(isset($_POST['materielCheck'])) $typeObjet[] = $_POST['materielCheck'];
			if(count($typeObjet)<=0)
			{
				$typeObjet[] = 'contact';
				$typeObjet[] = 'action';
				$typeObjet[] = 'suivis';
				$typeObjet[] = 'materiel';
			}

			$typeEncodage = array();
			if(isset($_POST['ajoutCheck'])) $typeEncodage[] = $_POST['ajoutCheck'];
			if(isset($_POST['updateCheck']))$typeEncodage[] = $_POST['updateCheck'];
			if(isset($_POST['deleteCheck'])) $typeEncodage[] = $_POST['deleteCheck'];
			if(count($typeEncodage)<=0)
			{
				$typeEncodage[] = 'ajout';
				$typeEncodage[] = 'update';
				$typeEncodage[] = 'delete';
			}
			//Requete
			$DB = new HistoriqueEncodageDatabase();
			$DB->open();
			$histo = new HistoriqueEncodages($DB->getRapport($agent, $operateur, $dateAnte, $datePost, $typeObjet, $typeEncodage));
			$DB->close();
			$session = Session::GetInstance();
			$session->set('historique', $histo);
			$session->set('typeObjet', $typeObjet);
			$session->set('typeEncodage', $typeEncodage);
			$session->set('dateEncodage', 'Du '.$dateAnte.' au '.$datePost);
			$this->view->reportActivite();
			$this->view->ResultActivite($histo, $typeObjet, $typeEncodage);
		}
		else
		{
			$this->Initialize();
			$this->view->reportActivite();
		}
			
	}

	public function Initialize()
	{
		$date = new DateTime(null, new DateTimeZone('Europe/Brussels'));
		$_POST['annee_ante'] = $date->format('01/m/Y');
		$_POST['annee_post'] = $date->format('t/m/Y');
	}

	private function RenderReportEtablissements()
	{
		$this->view = new ReportEtablissementsView();
			
		$this->title = 'Rapport sur les établissements';
		$this->shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->menu = $this->view->createMenu();
		$this->login = $this->view->createLogin();

		$onSubmit = (isset($_REQUEST['type']) && isset($_POST['SubmitAction']) && ($_REQUEST['type'] == 'etablissement') && ($_POST['SubmitAction'] == 'search'));
		$ets = null;

		if ($onSubmit)
		{
			$ets = $this->searchEtablissement();
			$session = Session::GetInstance();
			//if ($session->exist('etab')) $session->reset('etab');
			$session->set('etablissementsRapport', $ets);
			$session->set('PostEtab', $_POST);
		}
		else $_POST['swActif'] = 'on';

		$this->view->Render();
			
		if ($onSubmit)
		{
			if ((isset($ets)) && ($ets->Count()) > 0)
				$this->view->renderList($ets);
			else
				$this->view->noResult();
		}
	}

	private function getSwActifCriteria()
	{
		$swActif = null;

		if ($this->user->isOperateur()) $swActif = 1;
		else
		{
			if ((isset($_POST['swActif'])) && (isset($_POST['swInactif']))) $swActif = null;
			elseif (isset($_POST['swActif'])) $swActif = 1;
			elseif (isset($_POST['swInactif'])) $swActif = 0;
		}
			
		return $swActif;
	}

	private function getUtilisateurCriteria()
	{
		$utilisateurId = null;
			
		if (isset($_POST['agent']) && $_POST['agent'] != -1)
		{
			if ($_POST['agent'] == -2) $utilisateurId = 0;
			elseif ($_POST['agent'] > 0) $utilisateurId = $_POST['agent'];
		}
			
		return $utilisateurId;
	}

	private function searchEtablissement()
	{
		$swActif = $this->getSwActifCriteria();
		$anneeId = (isset($_POST['anneesScolaire']) && $_POST['anneesScolaire'] > -1) ? $_POST['anneesScolaire'] : null;
		$utilisateurId = $this->getUtilisateurCriteria();
		$entiteAdminId = (isset($_POST['entite_administrative']) && strlen($_POST['entite_administrative']) > 0) ? $_POST['entite_administrative'] : null;
		$nom = ((isset($_POST['nom_etablissement'])) && (strlen($_POST['nom_etablissement']) > 0)) ? $_POST['nom_etablissement'] : null;
		$cp_1 = ((isset($_POST['code_postal_1']) && (strlen($_POST['code_postal_1']) > 0))) ? $_POST['code_postal_1'] : null;
		$cp_2 = ((isset($_POST['code_postal_2']) && (strlen($_POST['code_postal_2']) > 0))) ? $_POST['code_postal_2'] : null;
			
		$provincesArray = array();
		$arrondissementsArray = array();
		$reseauxArray = array();
		$niveauxArray = array();
			
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, 'chk_') === 0)
			{
				$k = str_replace('chk_', '', $key);
					
				if (strpos($k, 'pro_') === 0) array_push($provincesArray, str_replace('pro_', '', $k));
				elseif (strpos($k, 'niv_') === 0) array_push($niveauxArray, str_replace('niv_', '', $k));
				elseif (strpos($k, 'arr_') === 0) array_push($arrondissementsArray, str_replace('arr_', '', $k));
				elseif (strpos($k, 'res_') === 0) array_push($reseauxArray, str_replace('res_', '', $k));
			}
		}

		$db = new EtablissementDatabase();
		$db->open();
		$rs = $db->report($swActif, $anneeId, $utilisateurId, $entiteAdminId, $nom, $cp_1, $cp_2, $provincesArray, $arrondissementsArray, $reseauxArray, $niveauxArray);
		//$db->close();

		return new Etablissements($rs);
			
		//$v->renderList(null);
	}

	private function searchAction()
	{
		$nom = (!empty($_POST['nom'])) ? $_POST['nom'] : null;
		$anneeId = (isset($_POST['anneesScolaire'])) ? $_POST['anneesScolaire'] : null;
		$entiteAdmin = (!empty($_POST['entiteAdmin'])) ? $_POST['entiteAdmin'] : null;
		$etablissement = (!empty($_POST['etablissement'])) ? $_POST['etablissement'] : null;
		$cp_1 = ((isset($_POST['code_postal_1']) && (strlen($_POST['code_postal_1']) > 0))) ? $_POST['code_postal_1'] : null;
		$cp_2 = ((isset($_POST['code_postal_2']) && (strlen($_POST['code_postal_2']) > 0))) ? $_POST['code_postal_2'] : null;
		$global =(isset($_POST['global'])) ? $_POST['global'] : null;
			
		$actionNonLab = (isset($_POST['actionNonLab'])) ? true : false;
		$actionLab = (isset($_POST['actionLab'])) ? true : false;
		$actionMicro = (isset($_POST['actionMicro'])) ? true : false;
			
		//Categ
		$categorieArray = array();
		if(!$this->user->isOperateur())
		{
			$nbCateg = $_POST['nbCategorie'];
			for($i=0; $i<$nbCateg; $i++)
			{
				if(isset($_POST['categorie_'.$i]))
				{
					array_push($categorieArray, $_POST['categorie_'.$i]);
				}
			}
		}
			
		//operateur
		$operateurArray = array();
		if(!$this->user->isOperateur())
		{
			$nbOperateur = $_POST['nbOperateur'];
			for($i=0; $i<$nbOperateur; $i++)
			{
				if(isset($_POST['operateur_'.$i]))
				{
					array_push($operateurArray, $_POST['operateur_'.$i]);
				}
			}
		}
		else
		{
			$actionLab=true;
			array_push($operateurArray, $this->user->getOperateurId());
		}
			
		$provincesArray = array();
		$arrondissementsArray = array();
		$reseauxArray = array();
		$niveauxArray = array();
			
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, 'chk_') === 0)
			{
				$k = str_replace('chk_', '', $key);
				if (strpos($k, 'pro_') === 0) array_push($provincesArray, str_replace('pro_', '', $k));
				elseif (strpos($k, 'niv_') === 0) array_push($niveauxArray, str_replace('niv_', '', $k));
				elseif (strpos($k, 'arr_') === 0) array_push($arrondissementsArray, str_replace('arr_', '', $k));
				elseif (strpos($k, 'res_') === 0) array_push($reseauxArray, str_replace('res_', '', $k));
			}
		}
			
		$db = new EtablissementDatabase();
		$db->open();
		$etablissements = new Etablissements($db->report(null, $anneeId, null, $entiteAdmin, $etablissement, $cp_1, $cp_2, $provincesArray, $arrondissementsArray, $reseauxArray, $niveauxArray));
		// $db->close();
			
		$db = new ActionDatabase();
		// $db->open();
		$actions = new Actions($db->getRapport($nom, $anneeId, $global, $actionLab, $actionNonLab, $actionMicro, $categorieArray, $operateurArray, $etablissements));
		// $db->close();
			
		$session = Session::GetInstance();
		$session->set('actions', $actions);
		$session->set('PostAction', $_POST);
			
		if(isset($actions) && $actions->count()>0)
			$this->view->renderList($actions);
		else
			$this->view->renderNoResult();
	}

	private function loadOperateurByYear($anneeId = null)
	{
		// load operateurs
		require_once SCRIPTPATH . 'model/operateur_database_model_class.php';
		require_once SCRIPTPATH . 'domain/operateur_domain_class.php';
		$db = new OperateurDatabase();
		$db->open();
		$operateurs = null;
		if (!empty($anneeId))
		{
			$operateurs = $db->getOperateurInOneAnneeScolaire($anneeId);
		}
		else
		{
			$operateurs = $db->get();
		}
		$this->view->operateurs = new Operateurs($operateurs);
	}
	
	private function RenderListingEtablissements()
	{
		$this->view = new ListingEtablissementsView();
			
		$this->title = 'Listing sur les établissements';
		$this->shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->menu = $this->view->createMenu();
		$this->login = $this->view->createLogin();

		$onSubmit = (isset($_REQUEST['type']) && isset($_POST['SubmitAction']) && ($_REQUEST['type'] == 'listing_etablissement') && ($_POST['SubmitAction'] == 'search'));
		$ets = null;

		if ($onSubmit)
		{
			$ets = $this->searchListingEtablissement();
			$session = Session::GetInstance();
			$session->set('letab', $ets);
			$session->set('PostLEtab', $_POST);
		}
		else $_POST['swActif'] = 'on';

		$this->view->Render();
			
		if ($onSubmit)
		{
			if ((isset($ets)) && ($ets->Count()) > 0)
				$this->view->renderList($ets);
			else
				$this->view->noResult();
		}
	}
	
	private function searchListingEtablissement()
	{
		$activite = ((isset($_POST['activite']) && (strlen($_POST['activite']) > 0))) ? $_POST['activite'] : null;
			
		$utilisateurId = $this->getUtilisateurCriteria();
		$cp_1 = ((isset($_POST['code_postal_1']) && (strlen($_POST['code_postal_1']) > 0))) ? $_POST['code_postal_1'] : null;
		$cp_2 = ((isset($_POST['code_postal_2']) && (strlen($_POST['code_postal_2']) > 0))) ? $_POST['code_postal_2'] : null;
			

		$arrondissementsArray = array();
			
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, 'chk_') === 0)
			{
				$k = str_replace('chk_', '', $key);
					
				if (strpos($k, 'arr_') === 0) array_push($arrondissementsArray, str_replace('arr_', '', $k));
			}
		}
		
		$db = new EtablissementDatabase();
		$db->open();
		$rs = $db->listing($activite, $utilisateurId, $cp_1, $cp_2, $arrondissementsArray);
	
		return new Etablissements($rs);
	}
	
	private function RenderListingAction()
	{
		$this->view = new ListingActionsView();
		$this->title = 'Listing sur les activités';
		$this->shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->menu = $this->view->createMenu();
		$this->login = $this->view->createLogin();

		$onSubmit = (isset($_REQUEST['type']) && isset($_POST['SubmitAction']) && ($_REQUEST['type'] == 'listing_action') && ($_POST['SubmitAction'] == 'search'));
		
		$listActions = null;
		
		//FFI  if(isset($_POST['report_action']))
		if($onSubmit)
		{
			// load operateurs
			$this->loadOperateurByYear(((isset($_POST['anneesScolaire'])) ? $_POST['anneesScolaire'] : null));
			$this->view->Render();
			$listActions = $this->searchListingAction();
			
			if (isset($listActions) && ($listActions->Count() > 0)) {
				$session = Session::GetInstance();
				$session->set('letab', $listActions);
				$session->set('PostLEtab', $_POST);
				$this->view->renderList($listActions);
			} else $this->view->noResult();
		}
		else
		{
			$this->initializeReportAction();
			$this->view->Render();
		}		
		
	}
	
	private function searchListingAction()
	{
		$intitule = (!empty($_POST['intitule'])) ? $_POST['intitule'] : null;
		$utilisateurId = $this->getUtilisateurCriteria(); // -> agent !
		$anneeId = (isset($_POST['anneesScolaire'])) ? $_POST['anneesScolaire'] : null;
		$cp_1 = ((isset($_POST['code_postal_1']) && (strlen($_POST['code_postal_1']) > 0))) ? $_POST['code_postal_1'] : null;
		$cp_2 = ((isset($_POST['code_postal_2']) && (strlen($_POST['code_postal_2']) > 0))) ? $_POST['code_postal_2'] : null;
		$global =(isset($_POST['global'])) ? $_POST['global'] : null;
			
		$actionLab = (isset($_POST['actionLab'])) ? true : false;
		$actionNonLab = (isset($_POST['actionNonLab'])) ? true : false;
		$formations = (isset($_POST['formations'])) ? true : false;
		$stac = (isset($_POST['stac'])) ? true : false;
		$ateliers = (isset($_POST['ateliers'])) ? true : false;
		$projEntr = (isset($_POST['projEntr'])) ? true : false;
			
		$arrondissementsArray = array();
			
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, 'chk_') === 0)
			{
				$k = str_replace('chk_', '', $key);
				if (strpos($k, 'arr_') === 0) array_push($arrondissementsArray, str_replace('arr_', '', $k));
			}
		}
		
			
//		$db = new EtablissementDatabase();
//		$db->open();
//		$etablissements = new Etablissements($db->report(null, $anneeId, null, null, null, $cp_1, $cp_2, null, $arrondissementsArray, null, null));
//		// $db->close();
//			
//		$db = new ActionDatabase();
//		// $db->open();
//		$actions = new Actions($db->getRapport(null, $anneeId, $global, $actionLab, $projEntr, $form, null, null, null));
//		// $db->close();
//			
//		$session = Session::GetInstance();
//		$session->set('actions', $actions);
//		$session->set('PostAction', $_POST);
//			
//		if(isset($actions) && $actions->count()>0)
//			$this->view->renderList($actions);
//		else
//			$this->view->renderNoResult();
	
		$db = new ActionDatabase();
		$db->open();
		// $rs = $db->listing(null, $utilisateurId, null, $cp_1, $cp_2, $arrondissementsArray);
		
		//echo ('DEBUG : ' . $intitule . '<br>' .  $utilisateurId  . '<br>' .  $anneeId  . '<br>' .  $projEntr  . '<br>' .  $actionLab  . '<br>' .  $form  . '<br>' .  $cp_1  . '<br>' .  $cp_2  . '<br>'); // .  $arrondissementsArray);
		
		$rs = $db->listing($intitule, $utilisateurId, $anneeId, $actionLab, $actionNonLab, $formations, $stac, $ateliers, $projEntr, $cp_1, $cp_2, $arrondissementsArray);
		
		return new Actions($rs);
	}
	
	private function RenderListingFormation()
	{
		
		$this->view = new ListingFormationView();
			
		$this->title = 'Listing FSE';
		$this->shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->menu = $this->view->createMenu();
		$this->login = $this->view->createLogin();

		$onSubmit = (isset($_REQUEST['type']) && isset($_POST['SubmitFormation']) && ($_REQUEST['type'] == 'listing_formation') && ($_POST['SubmitFormation'] == 'search'));
		$ets = null;

		if ($onSubmit)
		{
			$ets = $this->searchListingFormation();			
		}
		else $_POST['swActif'] = 'on';

		$this->view->Render();
			
		if ($onSubmit)
		{
			if ((isset($ets)) && ($ets->Count()) > 0) {
				$session = Session::GetInstance();
				$session->set('letab', $ets);
				$session->set('PostLEtab', $_POST);
				$this->view->renderList($ets);
			}
			else
				$this->view->noResult();
		}
	}
	
	
	private function RenderListingContact()
	{
		$this->view = new ListingContactView();
			
		$this->title = 'Listing sur les contacts';
		$this->shortcut = $this->view->raccourcisPanel($_REQUEST['module']);
		$this->menu = $this->view->createMenu();
		$this->login = $this->view->createLogin();

		$onSubmit = (isset($_REQUEST['type']) && isset($_POST['SubmitContact']) && ($_REQUEST['type'] == 'listing_contact') && ($_POST['SubmitContact'] == 'search'));
		$ets = null;

		if ($onSubmit)
		{
			$ets = $this->searchListingContact();			
		}
		else $_POST['swActif'] = 'on';

		$this->view->Render();
			
		if ($onSubmit)
		{
			if ((isset($ets)) && ($ets->Count()) > 0) {
				$session = Session::GetInstance();
				$session->set('letab', $ets);
				$session->set('PostLEtab', $_POST);
				$this->view->renderList($ets);
			}
			else
				$this->view->noResult();
		}
	}
	
	private function searchListingContact()
	{
		
		/*
		 * // TODO : FFI : code input
		 * 
		 	
		 */
		
		$swActif = $this->getSwActifCriteria();
		$utilisateurId = $this->getUtilisateurCriteria(); // agent
		
		// $intitule = (!empty($_POST['intitule'])) ? $_POST['intitule'] : null;
		
		$cp_1 = ((isset($_POST['code_postal_1']) && (strlen($_POST['code_postal_1']) > 0))) ? $_POST['code_postal_1'] : null;
		$cp_2 = ((isset($_POST['code_postal_2']) && (strlen($_POST['code_postal_2']) > 0))) ? $_POST['code_postal_2'] : null;
		
		$titre = ( !empty($_POST['titre']) && $_POST['titre'] != '-1' ) ? $_POST['titre'] : null;
		$matiere = (!empty($_POST['matiere']) && $_POST['matiere'] != '-1' ) ? $_POST['matiere'] : null;
		
		$anneeAnte = (!empty($_POST['annee_ante'])) ? $_POST['annee_ante'] : null;
		$anneePost = (!empty($_POST['annee_post'])) ? $_POST['annee_post'] : null;
		
		$personneRes = null;
		if(isset($_POST['personneRes']))
			$personneRes = $_POST['personneRes'];

		$personneResNot = null;
		if(isset($_POST['personneResNot']))
			$personneResNot = $_POST['personneResNot'];
		
		//echo "request form : " . $intitule . '<br>' . $utilisateurId . '<br>' . $cp_1 . '<br>' . $cp_2 . '<br>' .$titre . '<br>' . $matiere . '<br>' . $anneeAnte . '<br>' . $anneePost . '<br>' . $personneRes . '<br>' . $personneResNot .'<br>';
		
			
		$db = new ContactDatabase();
		$db->open();
		$rs = $db->listing($utilisateurId, $titre, $matiere, $anneeAnte, $anneePost, $cp_1, $cp_2, $personneRes, $personneResNot); 
	
		
	
		//$db = new EtablissementDatabase();
		//$db->open();
		//$rs = $db->listing($swActif, $utilisateurId, $cp_1, $cp_2, $arrondissementsArray);

		return new Contacts($rs);
	}
	

	private function searchListingFormation()
	{
		
		/*
		 * // TODO : FFI : code input
		 * 
		 	
		 */
		
		$swActif = $this->getSwActifCriteria();
		$utilisateurId = $this->getUtilisateurCriteria(); // agent
		
		$intitule = (!empty($_POST['intitule'])) ? $_POST['intitule'] : null;
		
		$anneeAnte = (!empty($_POST['annee_ante'])) ? $_POST['annee_ante'] : null;
		$anneePost = (!empty($_POST['annee_post'])) ? $_POST['annee_post'] : null;
		
		// echo "request form : " . $intitule . '<br>' . $utilisateurId . '<br>' . $cp_1 . '<br>' . $cp_2 . '<br>' .$titre . '<br>' . $matiere . '<br>' . $anneeAnte . '<br>' . $anneePost . '<br>' . $personneRes . '<br>' . $personneResNot .'<br>';
		
		$db = new FormationDatabase();
		$db->open();
		$rs = $db->listing($intitule, $utilisateurId, $anneeAnte, $anneePost); 
	
		return new Formations($rs);
	}
}
