<?php
error_reporting(E_ALL & ~E_STRICT &  ~E_NOTICE);
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

REQUIRE_ONCE(SCRIPTPATH.'model/operateur_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/bourse_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/action_operateur_database_model_class.php');

// REQUIRE_ONCE(SCRIPTPATH.'domain/action_operateur_domain_class.php');

class ActionFields
{
    public static $actionId = 'actionId';
    public static $statutSubventionId = 'statutSubventionId';
    public static $actionLabelId = 'actionLabelId';
    public static $refChezOperateur = 'refChezOperateur';
    public static $refChezASE = 'refChezASE';    
    public static $typeActionId = 'typeActionId';
    public static $anneeId = 'anneeId';
    public static $statutActionId = 'statutActionId';
    public static $categActionNonLabelId = 'categActionNonLabelId';
    public static $operateurId = 'operateurId';
    public static $dateAction = 'dateAction';
    public static $nomGen = 'nomGen';
    public static $nomSpec = 'nomSpec';
    public static $commentaire = 'commentaire';
    public static $swGlobal = 'swGlobal';
    public static $swPedagogie = 'swPedagogie';
    public static $nbApprenants = 'nbApprenants';
    public static $nbEnseignants = 'nbEnseignants';
    public static $website = 'website';
    public static $montantDemande = 'montantDemande';
    public static $montantSubvention = 'montantSubvention';
    public static $categorieBourse = 'categorieBourse';
    public static $projetRealise = 'projetRealise';
    public static $groupeClasseConcernee = 'groupeClasseConcernee';
    public static $collaborationInterEtab = 'collaborationInterEtab';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class Action
{
    private $actionId;
    public function GetActionId() { return $this->actionId; }
    public function SetActionId($value) { $this->actionId = $value; }

    private $statutSubventionId;
    public function GetStatutSubventionId() { return $this->statutSubventionId; }
    public function SetStatutSubventionId($value) {$this->statutSubventionId = $value;}
	public function GetStatutSubvention() {
		
		return Dictionnaries::getStatutSubventionList($this->GetStatutSubventionId())->items(0)->getLabel(); 
#		return Dictionnaries::getStatutSubventionList($this->statutSubventionId())->items(0)->getLabel(); 
	}

    private $actionLabelId;
    public function GetActionLabelId() { return $this->actionLabelId; }
    public function SetActionLabelId($value) { $this->actionLabelId = $value; }

    private $refChezOperateur;
    public function GetRefChezOperateur() { return $this->refChezOperateur; }
    public function SetRefChezOperateur($value) { $this->refChezOperateur = $value; }

    private $refChezASE;
    public function GetRefChezASE() { return $this->refChezASE; }
    public function SetRefChezASE($value) { $this->refChezASE = $value; }

    private $typeActionId;
    public function GetTypeActionId() { return $this->typeActionId; }
    public function SetTypeActionId($value) { $this->typeActionId = $value; }

    private $anneeId;
    public function GetAnneeId() { return $this->anneeId; }
    public function SetAnneeId($value) { $this->anneeId = $value; }

    private $statutActionId;
    public function GetStatutActionId() { return $this->statutActionId; }
    public function SetStatutActionId($value) { $this->statutActionId = $value; }
	public function GetStatutAction() 
	{ 
		if($this->statutActionId == 'VALID')
			return 'Publi� ';
		else
			return 'Brouillon';
	}
	
	public function isValid() 
	{ 
		if($this->statutActionId == 'VALID')
			return true;
		else
			return false;
	}

	//Marucci Nicolas 06/08/2014 un utilisateur ne peut pas modifier une action si elle n'est pas dans son ann�e d'encodage
	public function checkAnneeEncodage(){

		REQUIRE_ONCE(SCRIPTPATH.'model/annee_database_model_class.php');
		$DB_ANNEE = new AnneeDatabase();
		$annees = new Annees($DB_ANNEE->get($this->anneeId));
		$annee  = $annees->items(0);
		if($annee){
			$now    = date('Y-m-d h:i:s');
			if($now >= $annee -> getDateDeb() && $now <= $annee -> getDateFin()) return true;
			else return false;
		}
		else return false;
	}
	
	

    private $categActionNonLabelId;
    public function GetCategActionNonLabelId() { return $this->categActionNonLabelId; }
    public function SetCategActionNonLabelId($value) { $this->categActionNonLabelId = $value; }
	public function GetCategActionNonLabel() 
	{
		if($this->GetCategActionNonLabelId()!=null)
			return Dictionnaries::getCategActionNonLabelList($this->GetCategActionNonLabelId())->items(0)->getLabel();
	}

    private $operateurId;
    public function GetOperateurId() { return $this->operateurId; }
    public function SetOperateurId($value) { $this->operateurId = $value; }
    
    private $operateur;
	public function GetOperateur() 
	{ 
		if (isset($this->operateur)) return $this->operateur->items(0);

		$db = new OperateurDatabase();
		$db->open();
		$rs = $db->get($this->GetOperateurId());
		$db->close();
		
		$this->operateur = new Operateurs($rs);
		
		return $this->operateur->items(0);
	}
	
	private $operateurs;
	public function GetOperateurs()
	{
		if (isset($this->operateurs)) return $this->operateurs;

		$db = new ActionOperateurDatabase();
		$db->open();
		$rs = $db->get($this->GetActionId(), null);
		$db->close();
		
		$this->operateurs = new ActionOperateurs($rs);
		
		return $this->operateurs;
	}
	
	private $formateurOperateur;
	public function GetFormateurOperateur() 
	{
		if (isset($this->formateurOperateur)) return $this->formateurOperateur;

		REQUIRE_ONCE(SCRIPTPATH . 'domain/formateur_domain_class.php');
		REQUIRE_ONCE(SCRIPTPATH . 'model/formateur_database_model_class.php');
		$db = new FormateurDatabase();
		$db->open();
		// $rs = $db->getForAction($this->GetActionId());
		$rs = $db->get($this->GetOperateurId());
		$this->formateurOperateur = new Formateurs($rs);
		$this->formateurOperateur = $this->formateurOperateur->items(0);
		
		return $this->formateurOperateur;
		
	}
    
    private $dateAction;
    public function GetDateAction($formated = null)
    {
    	if (!isset($formated)) return $this->dateAction;
    	else return FormatDate($this->dateAction);
    }
    public function SetDateAction($value) { $this->dateAction = $value; }

    private $nomGen;
    public function GetNomGen() { return $this->nomGen; }
    public function SetNomGen($value) { $this->nomGen = $value; }

    private $nomSpec;
    public function GetNomSpec() { return $this->nomSpec; }
    public function SetNomSpec($value) { $this->nomSpec = $value; }
    
    public function GetNom()
    {
    	if (isset($this->nomSpec) && (strlen($this->nomSpec) > 0)) return $this->GetNomSpec();
    	else return $this->GetNomGen();
    }

    private $commentaire;
    public function GetCommentaire() { return $this->commentaire; }
    public function SetCommentaire($value) { $this->commentaire = $value; }

    private $swGlobal;
    public function GetSwGlobal() { return $this->swGlobal; }
    public function SetSwGlobal($value) { $this->swGlobal = $value; }
	public function GetSwGlobalLabel() { return $this->GetSwGlobal() == 1 ? "Oui" : "Non"; }
	public function GetSwGlobalColor() { return $this->GetSwGlobal() == 1 ? "Green" : "Red"; }
	
    
    private $swPedagogie;
    public function GetSwPedagogie() { return $this->swPedagogie; }
    public function SetSwPedagogie($value) { $this->swPedagogie = $value; }
    
    public function GetPedagogieLabel()
    {
    	if($this->GetSwPedagogie()==1)
    		return 'Oui';
    	else
    		return 'Non';
    }

    private $nbApprenants;
    public function GetNbApprenants() { return $this->nbApprenants; }
    public function SetNbApprenants($value) { $this->nbApprenants = $value; }
    
    private $nbEnseignants;
    public function GetNbEnseignants() { return $this->nbEnseignants; }
    public function SetNbEnseignants($value) { $this->nbEnseignants = $value; }

    private $website;
    public function GetWebsite() { return $this->website; }
    public function SetWebsite($value) { $this->website = $value; }
	public function GetWebsiteAsAnchor() { return sprintf('<a href="%s" target="none">%s</a>', $this->GetWebsite(), $this->GetWebsite()); }

    private $montantDemande;
    public function GetMontantDemande() { return $this->montantDemande; }
    public function SetMontantDemande($value) { $this->montantDemande = $value; }

    private $montantSubvention;
    public function GetMontantSubvention() { return $this->montantSubvention; }
    public function SetMontantSubvention($value) { $this->montantSubvention = $value; }

    private $categorieBourse;
    public function GetCategorieBourse() { return $this->categorieBourse; }
    public function SetCategorieBourse($value) { $this->categorieBourse = $value; }

    private $projetRealise;
    public function GetProjetRealise() { return $this->projetRealise; }
    public function SetProjetRealise($value) { $this->projetRealise = $value; }

    private $groupeClasseConcernee;
    public function GetGroupeClasseConcernee() { return $this->groupeClasseConcernee; }
    public function SetGroupeClasseConcernee($value) { $this->groupeClasseConcernee = $value; }

    private $collaborationInterEtab;
    public function GetCollaborationInterEtab() { return $this->collaborationInterEtab; }
    public function SetCollaborationInterEtab($value) { $this->collaborationInterEtab = $value; }

    private $created;
    public function GetCreated() { return $this->created; }
    public function SetCreated($value) { $this->created = $value; }

    private $createdBy;
    public function GetCreatedBy() { return $this->createdBy; }
    public function SetCreatedBy($value) { $this->createdBy = $value; }
    
    public function GetCreatedByUser()
	{
		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get($this->GetCreatedBy(), null);
		$db->close();
		$users = new Utilisateurs($rs);
		return $users->items(0);
	}

    private $updated;
    public function GetUpdated() { return $this->updated; }
    public function SetUpdated($value) { $this->updated = $value; }
    
    public function GetDateUpdatedString($parenthese = true)
    {
    	if($this->GetUpdated()!=null)
    	{
    		if($parenthese)
	    	 	return '('.FormatDate($this->GetUpdated()).')';
    		else
    			return FormatDate($this->GetUpdated());
    	}
    	return '';
    }
    
    public function GetDateCreatedString($parenthese = true)
    {
    	if($this->GetCreated()!=null)
    	{
    		if($parenthese)
	    	 	return '('.FormatDate($this->GetCreated()).')';
    		else
    			return FormatDate($this->GetCreated());
    	}
    	return '';
    }    

    private $updatedBy;
    public function GetUpdatedBy() { return $this->updatedBy; }
    public function SetUpdatedBy($value) { $this->updatedBy = $value; }
    
    public function GetUpdatedByUser()
	{
			$db = new UtilisateurDatabase();
			$db->open();
			$rs = $db->get($this->GetUpdatedBy(), null);
			$db->close();
			$users = new Utilisateurs($rs);
			
			return $users->items(0);
	}

    public function GetActionEtablissment($utilisateurId = null)
    {
		$db = new ActionDatabase();
		$db->open();
		$rs = $db->getActionEtablissement($this->GetActionId(), $utilisateurId);
		$db->close();
		$etabs = new Etablissements($rs);
		return $etabs;
    }
    
    private $actionContacts;
    
    public function GetActionContact()
    {
    	//if (isset($this->actionContacts)) return $this->actionContacts;
    	 
		$db = new ActionDatabase();
		$db->open();
		$rs = $db->getActionContact($this->GetActionId());
		$db->close();
		
		$this->actionContacts = new ActionContacts($rs);
		
		return $this->actionContacts;
    }
    
    public function GetBourseAgent()
    {
		$db = new BourseDatabase();
		$db->open();
		$rs = $db->getBourseAgent($this->GetActionId());
		$db->close();
		$bourseAgents = new BourseAgents($rs);
		return $bourseAgents;
    }
    
    public function GetSections()
    {
		$db = new ActionDatabase();
		$db->open();
		$rs = $db->getActionSection($this->GetActionId());
		$db->close();
		$sections = new ActionSectionActions($rs);
		
		return $sections;
    }
    
    public function GetFinalitePedagog()
    {
		$db = new ActionDatabase();
		$db->open();
		$rs = $db->getActionFinalitePedagog($this->GetActionId());
		$db->close();
		$actionPeda = new ActionFinalitePedagogs($rs);
		
		return $actionPeda;
    }
    
    public function getFiliereDegreNiveau()
    {
		$db = new ActionDatabase();
		$db->open();
		$rs = $db->getActionFiliereDegreNiveau($this->GetActionId());
		$db->close();
		$fileres = new ActionFiliereDegreNiveaus($rs);
		
		return $fileres;
    }
    
    public function GetTypeActionLabel()
    {
    	$actionLab = null;
    	switch($this->GetTypeActionId())
    	{
    		case 'LABEL' : $actionLab =  'Labelis�es'; break;
    		case 'NONLA' : $actionLab = 'Non labellis�es'; break;
    		case 'AMICR' : $actionLab = 'Micro'; break;
    		case 'BOUPI' : $actionLab = 'Projets innovants'; break;
    		case 'FFOR'  : $actionLab = 'Formations'; break;
    		case 'FSTAC' : $actionLab = 'STAC (Stages d\'acculturation)'; break;
    		case 'FATE'  : $actionLab = 'Ateliers'; break;   
    	}
    	return $actionLab;
    }
    
    public function getActionLabel()
    {
    	$nomSpec = '';
		if($this->GetNomSpec()!=null)
			$nomSpec = '('.$this->GetNomSpec().')';
			
		return $this->GetNomGen(). ' '.$nomSpec; 
    }
    
    public function GetDateActionString($parenthese = true)
    {
    	if($this->GetDateAction()!=null)
    	{
    		if($parenthese)
	    	 	return '('.FormatDate($this->GetDateAction()).')';
    		else
    			return FormatDate($this->GetDateAction());
    	}
    	return '';
    }
    
    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetActionId($row[ActionFields::$actionId]);
        	$this->SetStatutSubventionId($row[ActionFields::$statutSubventionId]);
        	$this->SetActionLabelId($row[ActionFields::$actionLabelId]);
        	$this->SetRefChezOperateur($row[ActionFields::$refChezOperateur]);
        	$this->SetRefChezASE($row[ActionFields::$refChezASE]);
        	$this->SetTypeActionId($row[ActionFields::$typeActionId]);
        	$this->SetAnneeId($row[ActionFields::$anneeId]);
        	$this->SetStatutActionId($row[ActionFields::$statutActionId]);
        	$this->SetCategActionNonLabelId($row[ActionFields::$categActionNonLabelId]);
        	$this->SetOperateurId($row[ActionFields::$operateurId]);
        	$this->SetDateAction($row[ActionFields::$dateAction]);
        	$this->SetNomGen($row[ActionFields::$nomGen]);
        	$this->SetNomSpec($row[ActionFields::$nomSpec]);
	        $this->SetCommentaire($row[ActionFields::$commentaire]);
    	    $this->SetSwGlobal($row[ActionFields::$swGlobal]);
	        $this->SetSwPedagogie($row[ActionFields::$swPedagogie]);
    	    $this->SetNbApprenants($row[ActionFields::$nbApprenants]);
	       	$this->SetNbEnseignants($row[ActionFields::$nbEnseignants]);
    	    $this->SetWebsite($row[ActionFields::$website]);
	        $this->SetMontantDemande($row[ActionFields::$montantDemande]);
    	    $this->SetMontantSubvention($row[ActionFields::$montantSubvention]);
	        $this->SetCategorieBourse($row[ActionFields::$categorieBourse]);
    	    $this->SetProjetRealise($row[ActionFields::$projetRealise]);
	        $this->SetGroupeClasseConcernee($row[ActionFields::$groupeClasseConcernee]);
        	$this->SetCollaborationInterEtab($row[ActionFields::$collaborationInterEtab]);
        	$this->SetCreated($row[ActionFields::$created]);
        	$this->SetCreatedBy($row[ActionFields::$createdBy]);
        	$this->SetUpdated($row[ActionFields::$updated]);
        	$this->SetUpdatedBy($row[ActionFields::$updatedBy]);
        		
    	}
    }
    
    function init($actionId, $statutSubventionId, $actionLabelId, $refChezOperateur, 
     				$refChezASE, $typeActionId, $anneeId, $statutActionId, $categActionNonLabelId, $operateurId, 
     				$dateAction, $nomGen, $nomSpec, $commentaire, $swGlobal, $swPedagogie, $nbApprenants,
				    $nbEnseignants, $website, $montantDemande, $montantSubvention, $categorieBourse,
     				$projetRealise, $groupeClasseConcernee, $collaborationInterEtab, $created, $createdBy, 
     				$updated, $updatedBy)
    
    {
		$this->SetActionId($actionId);
        $this->SetStatutSubventionId($statutSubventionId);
        $this->SetActionLabelId($actionLabelId);
        $this->SetRefChezOperateur($refChezOperateur);
        $this->SetRefChezASE($refChezASE);
        $this->SetTypeActionId($typeActionId);
        $this->SetAnneeId($anneeId);
        $this->SetStatutActionId($statutActionId);
        $this->SetCategActionNonLabelId($categActionNonLabelId);
        $this->SetOperateurId($operateurId);
        $this->SetDateAction($dateAction);
        $this->SetNomGen($nomGen);
        $this->SetNomSpec($nomSpec);
	    $this->SetCommentaire($commentaire);
    	$this->SetSwGlobal($swGlobal);
	    $this->SetSwPedagogie($swPedagogie);
    	$this->SetNbApprenants($nbApprenants);
	    $this->SetNbEnseignants($nbEnseignants);
    	$this->SetWebsite($website);
	    $this->SetMontantDemande($montantDemande);
    	$this->SetMontantSubvention($montantSubvention);
	    $this->SetCategorieBourse($categorieBourse);
    	$this->SetProjetRealise($projetRealise);
	    $this->SetGroupeClasseConcernee($groupeClasseConcernee);
        $this->SetCollaborationInterEtab($collaborationInterEtab);
        $this->SetCreated($created);
        $this->SetCreatedBy($createdBy);
        $this->SetUpdated($updated);
        $this->SetUpdatedBy($updatedBy);    
    }
    	
}

class Actions extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        $i=0;
        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) {
            	// echo "<br>ROW : " . $i++ . '<br>';            	
            	array_push($this->elements, new Action($row));
            }
            
        $this->get();        
    }
    
 	private $nbApprenantsGlobal = 0;
    function getNbApprenantsGlobal()
    {
    	return $this->nbApprenantsGlobal;
    }
    
    private $nbEnseignantsGlobal = 0;
    function getNbEnseignantsGlobal()
    {	
    	return $this->nbEnseignantsGlobal;
    }

    private $arraySection = array();
    function getArraySection()
    {	
    	return $this->arraySection;
    }
    
    private $arrayFiliere = array();
    function getArrayFiliere()
    {	
    	return $this->arrayFiliere;
    }
    
    private $arrayFinalite = array();
    function getArrayFinalite()
    {	
    	return $this->arrayFinalite;
    }    
    
    private $montantGlobal;
    function getMontantGlobal()
    {
    	return $this->montantGlobal;
    }
    
    function get()
    {
    	$this->montantGlobal = 0;
    	
        for($i=0; $i<$this->count();$i++)
    	{
    		$this->nbEnseignantsGlobal += $this->items($i)->GetNbEnseignants();
    		$this->nbApprenantsGlobal += $this->items($i)->GetNbApprenants();
    		$this->montantGlobal+= $this->items($i)->GetMontantSubvention();
    		
    		$filieres = $this->items($i)->getFiliereDegreNiveau();
    		for($f=0; $f<$filieres->count(); $f++)
    		{
    			if(isset($this->arrayFiliere[$filieres->items($f)->GetFiliereDegreNiveauId()]))
    		 		$val = $filieres->items($f)->GetNbApprenants() + $this->arrayFiliere[$filieres->items($f)->GetFiliereDegreNiveauId()];
    		 	else		
    		 		$val = $filieres->items($f)->GetNbApprenants();
    		 		
	    		$this->arrayFiliere[$filieres->items($f)->GetFiliereDegreNiveauId()] = $val;
    		}
    		
    		
    		$sections = $this->items($i)->GetSections();
    		for($s=0; $s<$sections->count(); $s++)
    		{
    			if(isset($this->arraySection[$sections->items($s)->GetSectionActionId()]))
    		 		$val = $sections->items($s)->GetNbApprenants() + $this->arraySection[$sections->items($s)->GetSectionActionId()];
    		 	else		
    		 		$val = $sections->items($s)->GetNbApprenants();
	    		$this->arraySection[$sections->items($s)->GetSectionActionId()] = $val;  	
    		}
    		
    		$finalites =  $this->items($i)->GetFinalitePedagog();
    		for($fi=0; $fi<$finalites->count();$fi++)
    		{
    			if(isset($this->arrayFinalite[$finalites->items($fi)->GetFinalitePedagogId()]))
    		 		$val = $finalites->items($fi)->GetNbApprenants() + $this->arrayFinalite[$finalites->items($fi)->GetFinalitePedagogId()];
    		 	else		
    		 		$val = $finalites->items($fi)->GetNbApprenants();
	    		$this->arrayFinalite[$finalites->items($fi)->GetFinalitePedagogId()] = $val;  	
    			
    		}
    		
    	}
    	
     }
}
?>
