<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/contact_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');

class ActionContactFields
{
    public static $actionId = 'actionId';
    public static $contactId = 'contactId';
    public static $roleContactActionId = 'roleContactActionId';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class ActionContact
{
    private $actionId;
    public function GetActionId() { return $this->actionId; }
    public function GetAction()
    {
		$actions = new ActionDatabase();
		$actions->open();
		$rs = $actions->GetById($this->actionId);
		$actions->close();
		
		$obj = new Actions($rs);
		
		if ($obj->count() == 1) return $obj->items(0);
		else return null; 
    }
    public function SetActionId($value) { $this->actionId = $value; }

    private $contactId;
    public function GetContactId() { return $this->contactId; }
    public function SetContactId($value) { $this->contactId = $value; }
	public function GetContact() 
	{
		
		$db = new ContactDatabase();
		$db->open();
		$rs = $db->get($this->GetContactId());
		$db->close();
			
		$contacts = new Contacts($rs);
			
		return $contacts->items(0);
	}
    

    private $roleContactActionId;
    public function GetRoleContactActionId() { return $this->roleContactActionId; }
    public function SetRoleContactActionId($value) { $this->roleContactActionId = $value; }
	public function GetRoleContactAction() 
	{
	
	   $res = Dictionnaries::getRoleContactActionList($this->GetRoleContactActionId());
	      
         
	   if($res->count()>0)
	     return $res->items(0)->getLabel();
      else return ''; 
	/*
		if($this->GetRoleContactActionId()=='RESP')
			return 'Responsable';
		else
			return 'Animateur';
			
			*/
	}

    private $created;
    public function GetCreated() { return $this->created; }
    public function SetCreated($value) { $this->created = $value; }

    private $createdBy;
    public function GetCreatedBy() { return $this->createdBy; }
    public function SetCreatedBy($value) { $this->createdBy = $value; }

    private $updated;
    public function GetUpdated() { return $this->updated; }
    public function SetUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function GetUpdatedBy() { return $this->updatedBy; }
    public function SetUpdatedBy($value) { $this->updatedBy = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetActionId($row[ActionContactFields::$actionId]);
        	$this->SetContactId($row[ActionContactFields::$contactId]);
        	$this->SetRoleContactActionId($row[ActionContactFields::$roleContactActionId]);
        	$this->SetCreated($row[ActionContactFields::$created]);
        	$this->SetCreatedBy($row[ActionContactFields::$createdBy]);
        	$this->SetUpdated($row[ActionContactFields::$updated]);
        	$this->SetUpdatedBy($row[ActionContactFields::$updatedBy]);
    	}
    }
    
    function init($actionId, $contactId, $roleContactActionId, $created,
    				$createdBy, $updated, $updatedBy)
    {
       	$this->SetActionId($actionId);
       	$this->SetContactId($contactId);
      	$this->SetRoleContactActionId($roleContactActionId);
       	$this->SetCreated($created);
      	$this->SetCreatedBy($createdBy);
       	$this->SetUpdated($updated);
       	$this->SetUpdatedBy($updatedBy);
    }
    
}

class ActionContacts extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ActionContact($row));
    }
}
?>