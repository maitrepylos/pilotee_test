<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

REQUIRE_ONCE(SCRIPTPATH.'domain/action_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/etablissement_domain_class.php');


REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/etablissement_database_model_class.php');

class ActionEtablissementFields
{
    public static $actionId = 'actionId';
    public static $etablissementId = 'etablissementId';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class ActionEtablissement
{
    private $actionId;
    public function GetActionId() { return $this->actionId; }
    public function GetAction()
    {
		$actions = new ActionDatabase();
		$actions->open();
		$rs = $actions->GetById($this->actionId);
		$actions->close();
		
		$obj = new Actions($rs);
		
		if ($obj->count() == 1) return $obj->items(0);
		else return null; 
    }
    public function SetActionId($value) { $this->actionId = $value; }
    
    public function GetEtablissement() {
    	$etablissements = new EtablissementDatabase();
    	$etablissements->open();
    	$rs = $etablissements->GetById($this->etablissementId);
    	
    	$obj = new Etablissements($rs);
    	if ($obj->count() == 1) return $obj->items(0);
		else return null;
    	
    }

    private $etablissementId;
    public function GetEtablissementId() { return $this->etablissementId; }
    public function SetEtablissementId($value) { $this->etablissementId = $value; }

    private $created;
    public function GetCreated() { return $this->created; }
    public function SetCreated($value) { $this->created = $value; }

    private $createdBy;
    public function GetCreatedBy() { return $this->createdBy; }
    public function SetCreatedBy($value) { $this->createdBy = $value; }

    private $updated;
    public function GetUpdated() { return $this->updated; }
    public function SetUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function GetUpdatedBy() { return $this->updatedBy; }
    public function SetUpdatedBy($value) { $this->updatedBy = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetActionId($row[ActionEtablissementFields::$actionId]);
        	$this->SetEtablissementId($row[ActionEtablissementFields::$etablissementId]);
        	$this->SetCreated($row[ActionEtablissementFields::$created]);
        	$this->SetCreatedBy($row[ActionEtablissementFields::$createdBy]);
        	$this->SetUpdated($row[ActionEtablissementFields::$updated]);
        	$this->SetUpdatedBy($row[ActionEtablissementFields::$updatedBy]);
    	}
    }
    
    function init($actionId, $etablissementId, $created, $createdBy, $updated, $updatedBy)
    {
		$this->SetActionId($actionId);
        $this->SetEtablissementId($etablissementId);
        $this->SetCreated($created);
        $this->SetCreatedBy($createdBy);
        $this->SetUpdated($updated);
        $this->SetUpdatedBy($updatedBy);    
    }
}

class ActionEtablissements extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ActionEtablissement($row));
    }
}
?>