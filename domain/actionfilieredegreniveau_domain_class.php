<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class ActionFiliereDegreNiveauFields
{
    public static $actionId = 'actionId';
    public static $filiereDegreNiveauId = 'filiereDegreNiveauId';
    public static $nbApprenants = 'nbApprenants';
}

class ActionFiliereDegreNiveau
{
    private $actionId;
    public function GetActionId() { return $this->actionId; }
    public function SetActionId($value) { $this->actionId = $value; }

    private $filiereDegreNiveauId;
    public function GetFiliereDegreNiveauId() { return $this->filiereDegreNiveauId; }
    public function SetFiliereDegreNiveauId($value) { $this->filiereDegreNiveauId = $value; }
	public function GetActionFiliereDegreNiveau() { return Dictionnaries::getFiliereDegreNiveauList($this->GetFiliereDegreNiveauId())->items(0)->getLabel(); }

    private $nbApprenants;
    public function GetNbApprenants() { return $this->nbApprenants; }
    public function SetNbApprenants($value) { $this->nbApprenants = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetActionId($row[ActionFiliereDegreNiveauFields::$actionId]);
        	$this->SetFiliereDegreNiveauId($row[ActionFiliereDegreNiveauFields::$filiereDegreNiveauId]);
        	$this->SetNbApprenants($row[ActionFiliereDegreNiveauFields::$nbApprenants]);
    	}
    }
    
    function init($actionId, $filiereDegreNiveauId, $nbApprenants)
    {
       	$this->SetActionId($actionId);
       	$this->SetFiliereDegreNiveauId($filiereDegreNiveauId);
       	$this->SetNbApprenants($nbApprenants);
    }
}

class ActionFiliereDegreNiveaus extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ActionFiliereDegreNiveau($row));
    }
}
?>