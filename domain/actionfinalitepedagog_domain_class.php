<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class ActionFinalitePedagogFields
{
    public static $actionId = 'actionId';
    public static $finalitePedagogId = 'finalitePedagogId';
    public static $nbApprenants = 'nbApprenants';
}

class ActionFinalitePedagog
{
    private $actionId;
    public function GetActionId() { return $this->actionId; }
    public function SetActionId($value) { $this->actionId = $value; }

    private $finalitePedagogId;
    public function GetFinalitePedagogId() { return $this->finalitePedagogId; }
    public function SetFinalitePedagogId($value) { $this->finalitePedagogId = $value; }
	public function GetFinalitePedagog() { return Dictionnaries::getFinalitePedagogList($this->GetFinalitePedagogId())->items(0)->getLabel(); }
	
    private $nbApprenants;
    public function GetNbApprenants() { return $this->nbApprenants; }
    public function SetNbApprenants($value) { $this->nbApprenants = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetActionId($row[ActionFinalitePedagogFields::$actionId]);
        	$this->SetFinalitePedagogId($row[ActionFinalitePedagogFields::$finalitePedagogId]);
        	$this->SetNbApprenants($row[ActionFinalitePedagogFields::$nbApprenants]);
    	}
    }
    
    function init($actionId, $finalitePedagogId, $nbApprenants)
    {
       	$this->SetActionId($actionId);
       	$this->SetFinalitePedagogId($finalitePedagogId);
       	$this->SetNbApprenants($nbApprenants);
    }
}

class ActionFinalitePedagogs extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ActionFinalitePedagog($row));
    }
}
?>