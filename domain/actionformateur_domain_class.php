<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class ActionFormateurFields
{
    public static $actionId = 'actionId';
    public static $formateurId = 'formateurId';
}

class ActionFormateur
{
    private $actionId;
    public function GetActionId() { return $this->actionId; }
    public function SetActionId($value) { $this->actionId = $value; }

    private $formateurId;
    public function GetFormateurId() { return $this->formateurId; }
    public function SetFormateurId($value) { $this->formateurId = $value; }

    private $formateur;
	public function GetFormateur() 
	{ 
		if (isset($this->formateur)) return $this->formateur->items(0);

		$db = new FormateurDatabase();
		$db->open();
		$rs = $db->get($this->GetFormateurId());
		$db->close();
		
		$this->formateur = new Formateurs($rs);
		
		return $this->formateur->items(0);
	}
    
    
    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetActionId($row[ActionFormateurFields::$actionId]);
        	$this->SetFormateurId($row[ActionFormateurFields::$formateurId]);
    	}
    }
    
    function init($actionId, $formateurId)
    {
    	$this->SetActionId($actionId);
    	$this->SetFormateurId($formateurId);
    }
    
}

class ActionFormateurs extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0) while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ActionFormateur($row));
    }
    
    public function Exists($formateurId)
    {
    	for ($i = 0; $i < $this->count(); $i++)
    		if ($this->elements[$i]->GetFormateurId() == $formateurId) return true;
    		
    	return false;
    }
}
?>