<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class ActionHasOperateurFields
{
    public static $action_actionId = 'action_actionId';
    public static $operateur_operateurId = 'operateur_operateurId';
}

class Action_has_operateur
{
    private $action_actionId;
    public function GetAction_actionId() { return $this->action_actionId; }
    public function SetAction_actionId($value) { $this->action_actionId = $value; }

    private $operateur_operateurId;
    public function GetOperateur_operateurId() { return $this->operateur_operateurId; }
    public function SetOperateur_operateurId($value) { $this->operateur_operateurId = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetAction_actionId($row[ActionHasOperateurFields::$action_actionId]);
        	$this->SetOperateur_operateurId($row[ActionHasOperateurFields::$operateur_operateurId]);
    	}
    }
    
    function init($action_actionId, $operateur_operateurId)
    {
       	$this->SetAction_actionId($action_actionId);
       	$this->SetOperateur_operateurId($operateur_operateurId);
    }
}

class ActionHasOperateurs extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ActionHasOperateur($row));
    }
}
?>