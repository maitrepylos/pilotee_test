<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class ActionLabelFields
{
    public static $actionLabelId = 'actionLabelId';
    public static $operateurId = 'operateurId';
    public static $label = 'label';
    public static $anneeId = 'anneeId';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class ActionLabel
{
    private $actionLabelId;
    public function getActionLabelId() { return $this->actionLabelId; }
	public function getId() { return $this->actionLabelId; }
    public function setActionLabelId($value) { $this->actionLabelId = $value; }

    private $operateurId;
    public function getOperateurId() { return $this->operateurId; }
    public function setOperateurId($value) { $this->operateurId = $value; }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $anneeId;
    public function getAnneeId() { return $this->anneeId; }
    public function setAnneeId($value) { $this->anneeId = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->setActionLabelId($row[ActionLabelFields::$actionLabelId]);
        $this->setOperateurId($row[ActionLabelFields::$operateurId]);
        $this->setLabel($row[ActionLabelFields::$label]);
        $this->setAnneeId($row[ActionLabelFields::$anneeId]);
        $this->setOrdre($row[ActionLabelFields::$ordre]);
        $this->setSwActif($row[ActionLabelFields::$swActif]);
    }
    
    function init($actionLabelId, $operateurId, $label, $anneeId, $ordre, $swActif  )
    {
        $this->setActionLabelId($actionLabelId);
        $this->setOperateurId($operateurId);
        $this->setLabel($label);
        $this->setAnneeId($anneeId);
        $this->setOrdre($ordre);
        $this->setSwActif($swActif);
    }    
}

class ActionLabels extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ActionLabel($row));
    }
}
?>