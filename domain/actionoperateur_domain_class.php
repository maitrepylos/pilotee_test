<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class ActionOperateurFields
{
    public static $actionId = 'actionId';
    public static $operateurId = 'operateurId';
}

class ActionOperateur
{
    private $actionId;
    public function GetActionId() { return $this->actionId; }
    public function SetActionId($value) { $this->actionId = $value; }

    private $operateurId;
    public function GetOperateurId() { return $this->operateurId; }
    public function SetOperateurId($value) { $this->operateurId = $value; }

    private $operateur;
	public function GetOperateur() 
	{ 
		if (isset($this->operateur)) return $this->operateur->items(0);

		$db = new OperateurDatabase();
		$db->open();
		$rs = $db->get($this->GetOperateurId());
		$db->close();
		
		$this->operateur = new Operateurs($rs);
		
		return $this->operateur->items(0);
	}
    
    
    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetActionId($row[ActionOperateurFields::$actionId]);
        	$this->SetOperateurId($row[ActionOperateurFields::$operateurId]);
    	}
    }
    
    function init($actionId, $operateurId)
    {
    	$this->SetActionId($actionId);
    	$this->SetOperateurId($operateurId);
    }
    
}

class ActionOperateurs extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0) while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ActionOperateur($row));
    }
    
    public function Exists($operateurId)
    {
    	for ($i = 0; $i < $this->count(); $i++)
    		if ($this->elements[$i]->GetOperateurId() == $operateurId) return true;
    		
    	return false;
    }
}
?>