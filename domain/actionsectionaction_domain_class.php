<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

//REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class ActionSectionActionFields
{
    public static $actionId = 'actionId';
    public static $sectionActionId = 'sectionActionId';
    public static $nbApprenants = 'nbApprenants';
}

class ActionSectionAction
{
    private $actionId;
    public function GetActionId() { return $this->actionId; }
    public function SetActionId($value) { $this->actionId = $value; }

    private $sectionActionId;
    public function GetSectionActionId() { return $this->sectionActionId; }
    public function SetSectionActionId($value) { $this->sectionActionId = $value; }
	public function GetActionSection() { return Dictionnaries::getSectionActionList($this->GetSectionActionId())->items(0)->getLabel(); }
		
    
    private $nbApprenants;
    public function GetNbApprenants() { return $this->nbApprenants; }
    public function SetNbApprenants($value) { $this->nbApprenants = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetActionId($row[ActionSectionActionFields::$actionId]);
        	$this->SetSectionActionId($row[ActionSectionActionFields::$sectionActionId]);
        	$this->SetNbApprenants($row[ActionSectionActionFields::$nbApprenants]);
    	}
    }
    
    function init($actionId, $sectionActionId, $nbApprenants)
    {
       	$this->SetActionId($actionId);
       	$this->SetSectionActionId($sectionActionId);
       	$this->SetNbApprenants($nbApprenants);
    }
    
}

class ActionSectionActions extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ActionSectionAction($row));
    }
}
?>