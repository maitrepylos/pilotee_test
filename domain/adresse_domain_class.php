<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

//REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class AdresseFields
{
	public static $adresseId 	= "adresseId";
	public static $dic_provinceId = "dic_provinceId";
	public static $rue 			= "rue";
	public static $codepostal 	= "codepostal";
	public static $ville	    = "ville";
	public static $tel1 		= "tel_1";
	public static $tel2 		= "tel_2";		
	public static $email1 		= "email_1";
	public static $email2 		= "email_2";
	public static $fax 			= "fax";
	public static $lat			= "lat";
	public static $lng			= "lng";
	public static $created 		= "created";
	public static $createdBy 	= "createdBy";
	public static $updated 		= "updated";
	public static $updatedBy 	= "updatedBy";
}
	
class Adresse
{
	private $adresseId;
	public function GetAdresseId() { return $this->adresseId; }
	public function SetAdresseId($value) { $this->adresseId = $value; }
	
	private $provinceId;
	public function GetProvinceId() { return $this->provinceId; }
	public function GetProvince() { return Dictionnaries::getProvincesList($this->provinceId)->items(0); }
	public function SetProvinceId($value) { $this->provinceId = $value; }
	
	private $rue;
	public function GetRue() { return $this->rue; }
	public function SetRue($value) { $this->rue = $value; }
	
	private $codepostal;
	public function GetCodepostal() {if($this->codepostal!=null) return Dictionnaries::GetCodePostals($this->codepostal)->items(0);else return null; }
	public function SetCodepostal($value) { $this->codepostal = $value; }
	
	private $ville;
	public function GetVille() { return $this->ville; }
	public function SetVille($value) { $this->ville = $value; }
	
	private $tel1;
	public function GetTel1() { return $this->tel1; }
	public function SetTel1($value) { $this->tel1 = $value; }
	
	private $tel2;
	public function GetTel2() { return $this->tel2; }
	public function SetTel2($value) { $this->tel2 = $value; }
	
	private $email1;
	public function GetEmail1() { return $this->email1; }
	public function GetEmail1AsAnchor() 
	{
		$email = $this->GetEmail1();
		return ((isset($email)) && (strlen($email) > 0)) ? sprintf('<a href="mailto:%s">%s</a>', $email, $email) : null;
	}
	public function SetEmail1($value) { $this->email1 = $value; }
	
	private $email2;
	public function GetEmail2() { return $this->email2; }
	public function GetEmail2AsAnchor() 
	{
		$email = $this->GetEmail2();
		return ((isset($email)) && (strlen($email) > 0)) ? sprintf('<a href="mailto:%s">%s</a>', $email, $email) : null;
	}
	public function SetEmail2($value) { $this->email2 = $value; }
	
	private $fax;
	public function GetFax() { return $this->fax; }
	public function SetFax($value) { $this->fax = $value; }
	
	private $created;
	public function GetCreated() { return $this->created; }
	public function SetCreated($value) { $this->created = $value; }
	
	private $createdBy;
	public function GetCreatedBy() { return $this->createdBy; }
	public function SetCreatedBy($value) { $this->createdBy = $value; }
	
	private $updated;
	public function GetUpdated() { return $this->updated; }
	public function SetUpdated($value) { $this->updated = $value; }
	
	private $updatedBy;
	public function GetUpdatedBy() { return $this->updatedBy; }
	public function SetUpdatedBy($value) { $this->updatedBy = $value; }
	
	private $lat;
	public function getLat() { return $this->lat;}
	public function setLat($value) { $this->lat = $value; }

	private $lng;
	public function getLng() { return $this->lng;}
	public function setLng($value) { $this->lng = $value; }
	
	function __construct($row = null)
	{
		if(isset($row))
		{
			$this->SetAdresseId($row[AdresseFields::$adresseId]);
			$this->SetProvinceId($row[AdresseFields::$dic_provinceId]);
			$this->SetRue($row[AdresseFields::$rue]);
			$this->SetCodepostal($row[AdresseFields::$codepostal]);
			$this->SetVille($row[AdresseFields::$ville]);
			$this->SetTel1($row[AdresseFields::$tel1]);
			$this->SetTel2($row[AdresseFields::$tel2]);
			$this->SetEmail1($row[AdresseFields::$email1]);
			$this->SetEmail2($row[AdresseFields::$email2]);
			$this->SetFax($row[AdresseFields::$fax]);
			$this->SetCreated($row[AdresseFields::$created]);
			$this->SetCreatedBy($row[AdresseFields::$createdBy]);
			$this->SetUpdated($row[AdresseFields::$updated]);
			$this->SetUpdatedBy($row[AdresseFields::$updatedBy]);
			$this->setLat($row[AdresseFields::$lat]);
			$this->setLng($row[AdresseFields::$lng]);
		}
	}
	
    function init($adresseId, $provinceId, $rue, $cp, $ville, $tel1, $tel2, $mail1, 
    					$mail2, $fax, $created, $createdBy, $updated, $updatedBy)
    {
    	$this->SetAdresseId($adresseId);
    	$this->SetProvinceId($provinceId);
    	$this->SetRue($rue);
		$this->SetCodepostal($cp);
		$this->SetVille($ville);
		$this->SetTel1($tel1);
		$this->SetTel2($tel2);
		$this->SetEmail1($mail1);
		$this->SetEmail2($mail2);
		$this->SetFax($fax);
		$this->SetCreated($created);
		$this->SetCreatedBy($createdBy);
		$this->SetUpdated($updated);
		$this->SetUpdatedBy($updatedBy);
    }
}
	
class Adresses extends DomainBase
{
	function __construct($rs)
	{
		parent::__construct();
	
		if ($rs && mysqli_num_rows($rs) > 0)
		{
			while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Adresse($row));
		}
	}
}	
?>