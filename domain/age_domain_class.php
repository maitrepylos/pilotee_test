<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class AgeFields
{
    public static $ageId = 'ageId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class Age
{
    private $ageId;
    public function getAgeId() { return $this->ageId; }
    public function setAgeId($value) { $this->ageId = $value; }
	public function getId() { return $this->getAgeId(); }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->setAgeId($row[AgeFields::$ageId]);
        $this->setLabel($row[AgeFields::$label]);
        $this->setOrdre($row[AgeFields::$ordre]);
        $this->setSwActif($row[AgeFields::$swActif]);
    }
}

class Ages extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Age($row));
    }
}
?>