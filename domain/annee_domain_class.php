<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class AnneeFields
{
	public static $anneeId 		= "anneeId";
	public static $dateDebut	= "dateDebut";
	public static $dateFin 		= "dateFin";
	public static $ordre	    = "ordre";
	public static $swActif 		= "swActif";
}
	
class Annee
{
	private $id;
	public function setId($value) { $this->id = $value; }
	public function getId() { return $this->id; }

	private $date_debut;
	public function setDateDeb($value) { $this->date_debut = $value; }
	public function getDateDeb() { return $this->date_debut; }
	
	private $date_fin;
	public function setDateFin($value) { $this->date_fin = $value; }
	public function getDateFin() { return $this->date_fin; }
	
	private $ordre;
	public function setOrdre($value) { $this->ordre = $value; }
	public function getOrdre() { return $this->ordre; }

	private $swActif;
	public function setSwActif($value) { $this->swActif = $value; }
	public function getSwActif() { return $this->swActif; }
	
	public function getLabel() { return $this->getId(); }
	
	function __construct($row = array())
	{
		$this->setId($row[AnneeFields::$anneeId]);
		$this->setDateDeb($row[AnneeFields::$dateDebut]);
		$this->setDateFin($row[AnneeFields::$dateFin]);
		$this->setOrdre($row[AnneeFields::$ordre]);
		$this->setSwActif($row[AnneeFields::$swActif]);
	}
}
	
class Annees extends DomainBase
{
	function __construct($rs)
	{
		parent::__construct();
	
		if ($rs && mysqli_num_rows($rs) > 0)
		{
			while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Annee($row));
		}
	}
}
?>