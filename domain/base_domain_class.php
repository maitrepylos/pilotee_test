<?php

class DomainBase implements Countable, ArrayAccess, IteratorAggregate
{
	protected $elements;
	protected $_domaineTypeName;

	public function __construct($rs = null)
	{
		$this->elements = array();
		if (!empty($rs))
		{
			$this->_postConstruct($rs);
		}
	}

	public function items($idx)
	{
		if (isset($idx) && isset($this->elements[ $idx ]))
		{
			return $this->elements[ $idx ];
		}
		else
		{
			return null;
		}
	}

	public function count()
	{
		return count($this->elements);
	}

	public function offsetExists($offset)
	{
		return isset($this->elements[ $offset ]);
	}

	public function offsetGet($offset)
	{
		return $this->elements[ $offset ];
	}

	public function offsetSet($offset, $value)
	{
		$this->elements[ $offset ] = $value;
	}

	public function offsetUnset($offset)
	{
		unset($this->elements[ $offset ]);
	}
	
	public function getIterator()
	{
		return new ArrayIterator($this->elements);
	}

	protected function _getDomainTypeName()
	{
		if (empty($this->_domainTypeName))
		{
			$this->_domainTypeName = substr(get_class($this), 0, -1);
		}

		return $this->_domainTypeName;
	}

	protected function _postConstruct($rs)
	{
		if ($rs && mysqli_num_rows($rs) > 0)
		{
			$domaineTypeName = $this->_getDomainTypeName();
			while ($row = mysqli_fetch_assoc($rs))
			{
				array_push($this->elements, new $domaineTypeName($row));
			}
			mysqli_free_result($rs);
		}
	}
}
