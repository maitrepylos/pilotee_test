<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');

class BourseAgentFields
{
    public static $utilisateurId = 'utilisateurId';
    public static $actionId = 'actionId';
    public static $dateEvaluation = 'dateEvaluation';
    public static $syntheseAvisId = 'syntheseAvisId';
    public static $avisQualitatif = 'avisQualitatif';
    public static $eltImportant = 'eltImportant';
    public static $soutientFiancier = 'soutientFiancier';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class BourseAgent
{
    private $utilisateurId;
    public function getUtilisateurId() { return $this->utilisateurId; }
    public function GetUtilisateur()
    {
		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get($this->getUtilisateurId(), null);
		$db->close();
		$op = new Utilisateurs($rs);
		return $op->items(0);
    	
    }
    public function setUtilisateurId($value) { $this->utilisateurId = $value; }

    private $actionId;
    public function getActionId() { return $this->actionId; }
    public function setActionId($value) { $this->actionId = $value; }

    private $dateEvaluation;
    public function getDateEvaluation() { return $this->dateEvaluation; }
    public function setDateEvaluation($value) { $this->dateEvaluation = $value; }
    public function getDateEvaluationString($formated = null)
    {
    	if (!isset($formated)) return $this->getDateEvaluation();
    	else return FormatDate($this->getDateEvaluation());
    }
    

    private $syntheseAvisId;
    public function getSyntheseAvisId() { return $this->syntheseAvisId; }
    public function setSyntheseAvisId($value) { $this->syntheseAvisId = $value; }
    
	public function getSyntheseAvisLabel() {
			return Dictionnaries::getSyntheseAvisList($this->getSyntheseAvisId())->items(0)->getLabel();
	}

    private $avisQualitatif;
    public function getAvisQualitatif() { return $this->avisQualitatif; }
    public function setAvisQualitatif($value) { $this->avisQualitatif = $value; }

    private $eltImportant;
    public function getEltImportant() { return $this->eltImportant; }
    public function setEltImportant($value) { $this->eltImportant = $value; }

    private $soutientFiancier;
    public function getSoutientFiancier() { return $this->soutientFiancier; }
    public function setSoutientFiancier($value) { $this->soutientFiancier = $value; }

    private $created;
    public function getCreated() { return $this->created; }
    public function setCreated($value) { $this->created = $value; }

    private $createdBy;
    public function getCreatedBy() { return $this->createdBy; }
    public function setCreatedBy($value) { $this->createdBy = $value; }

    private $updated;
    public function getUpdated() { return $this->updated; }
    public function setUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function getUpdatedBy() { return $this->updatedBy; }
    public function setUpdatedBy($value) { $this->updatedBy = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{	
        	$this->setUtilisateurId($row[BourseAgentFields::$utilisateurId]);
        	$this->setActionId($row[BourseAgentFields::$actionId]);
        	$this->setDateEvaluation($row[BourseAgentFields::$dateEvaluation]);
        	$this->setSyntheseAvisId($row[BourseAgentFields::$syntheseAvisId]);
        	$this->setAvisQualitatif($row[BourseAgentFields::$avisQualitatif]);
        	$this->setEltImportant($row[BourseAgentFields::$eltImportant]);
        	$this->setSoutientFiancier($row[BourseAgentFields::$soutientFiancier]);
        	$this->setCreated($row[BourseAgentFields::$created]);
        	$this->setCreatedBy($row[BourseAgentFields::$createdBy]);
        	$this->setUpdated($row[BourseAgentFields::$updated]);
        	$this->setUpdatedBy($row[BourseAgentFields::$updatedBy]);
    	}
    }
    
    function init($utilisateurId, $actionId, $dateEvaluation, $syntheseAvisId, $avisQualitatif,
    				$eltImportant, $soutientFiancier, $created, $createdBy, $updated, $updatedBy)
    {
        $this->setUtilisateurId($utilisateurId);
        $this->setActionId($actionId);
        $this->setDateEvaluation($dateEvaluation);
        $this->setSyntheseAvisId($syntheseAvisId);
        $this->setAvisQualitatif($avisQualitatif);
        $this->setEltImportant($eltImportant);
        $this->setSoutientFiancier($soutientFiancier);
        $this->setCreated($created);
        $this->setCreatedBy($createdBy);
        $this->setUpdated($updated);
        $this->setUpdatedBy($updatedBy);
    }
}

class BourseAgents extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new BourseAgent($row));
    }
}
?>