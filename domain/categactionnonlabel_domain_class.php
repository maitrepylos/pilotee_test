<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class CategActionNonLabelFields
{
    public static $categActionNonLabelId = 'categActionNonLabelId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class CategActionNonLabel
{
    private $categActionNonLabelId;
    public function GetCategActionNonLabelId() { return $this->categActionNonLabelId; }
	public function GetId() { return $this->GetCategActionNonLabelId(); }
    public function SetCategActionNonLabelId($value) { $this->categActionNonLabelId = $value; }

    private $label;
    public function GetLabel() { return $this->label; }
    public function SetLabel($value) { $this->label = $value; }

    private $ordre;
    public function GetOrdre() { return $this->ordre; }
    public function SetOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function GetSwActif() { return $this->swActif; }
    public function SetSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->SetCategActionNonLabelId($row[CategActionNonLabelFields::$categActionNonLabelId]);
        $this->SetLabel($row[CategActionNonLabelFields::$label]);
        $this->SetOrdre($row[CategActionNonLabelFields::$ordre]);
        $this->SetSwActif($row[CategActionNonLabelFields::$swActif]);
    }
}

class CategActionNonLabels extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new CategActionNonLabel($row));
    }
}
?>