<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');
//REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class CodePostalFields
{
	public static $codepostal = "codepostal";
	public static $provinceId = "provinceId";
	public static $arrondissementId = "arrondissementId";
	public static $swActif = "swActif";
}

class CodePostal
{
	private $codepostal;
	public function GetCodepostal() { return $this->codepostal; }
	public function SetCodepostal($value) { $this->codepostal = $value; }
	
	private $provinceId;
	public function GetProvinceId() { return $this->provinceId; }
	public function GetProvince() { return Dictionnaries::getProvincesList($this->GetProvinceId())->items(0); }
	public function SetProvinceId($value) { $this->provinceId = $value; }
	
	private $arrondissementId;
	public function GetArrondissementId() { return $this->arrondissementId; }
	public function GetArrondissement()	{ return Dictionnaries::getArrondissementList($this->GetArrondissementId())->items(0); }
	public function SetArrondissementId($value) { $this->arrondissementId = $value; }
	
	private $swActif;
	public function GetSwActif() { return $this->swActif; }
	public function SetSwActif($value) { $this->swActif = $value; }
	
	function __construct($row = array())
	{
		$this->SetCodepostal($row[CodePostalFields::$codepostal]);
		$this->SetProvinceId($row[CodePostalFields::$provinceId]);
		$this->SetArrondissementId($row[CodePostalFields::$arrondissementId]);
		$this->SetSwActif($row[CodePostalFields::$swActif]);
	}
}

class CodePostals extends DomainBase
{
	function __construct($rs)
	{
		parent::__construct();
	
		if ($rs && mysqli_num_rows($rs) > 0)
		{
			while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new CodePostal($row));
		}
	}
}

?>