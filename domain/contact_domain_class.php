<?php

REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/contact_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/contact_etablissement_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/actioncontact_database_model_class.php');

class ContactFields
{
    public static $contactId = 'contactId';
    public static $nom = 'nom';
    public static $prenom = 'prenom';
    public static $adresseId = 'adresseId';
    public static $genreId = 'genreId';
    public static $ageId = 'ageId';
    public static $statutId = 'statutId';
    public static $contactGlobal = 'contactGlobal';
    public static $swActif = 'swActif';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class Contact
{
    private $contactId;
    public function GetContactId() { return $this->contactId; }
    public function SetContactId($value) { $this->contactId = $value; }

    private $nom;
    public function GetNom() { return $this->nom; }
    public function SetNom($value) { $this->nom = $value; }

    private $prenom;
    public function GetPrenom() { return $this->prenom; }
    public function SetPrenom($value) { $this->prenom = $value; }

    private $adresseId;
    public function GetAdresseId() { return $this->adresseId; }
    
    
    private $genreId;
    public function getGenreId() { return $this->genreId; }
    public function setGenreId($value) { $this->genreId = $value; }

    private $ageId;
    public function getAgeId() { return $this->ageId; }
    public function setAgeId($value) { $this->ageId = $value; }

    private $statutId;
    public function getStatutId() { return $this->statutId; }
    public function setStatutId($value) { $this->statutId = $value; }
    
    
    private $adresse;
    public function GetAdresse()
	{
		if (isset($this->adresse)) return $this->adresse->items(0);
		
		$db = new AdresseDatabase();
		$db->open();
		$rs = $db->get($this->GetAdresseId());
		$db->close();
			
		$this->adresse = new Adresses($rs);
			
		return $this->adresse->items(0);		
	}
	
    public function GetEtablissementsContacts()
	{
		$db = new ContactEtablissementDatabase();
		$db->open();
		
		$rs = $db->get(null, $this->GetContactId(), null);
		$db->close();
		$contactEtablissements = new ContactEtablissements($rs);
			
		return $contactEtablissements;
	}
	
	
	private $actions;
	public function GetActions($userId = null)
	{
		if (isset($this->actions)) return $this->actions;
		
		$db = new ActionContactDatabase();
		$db->open();
		$rs = $db->get($this->GetContactId(),null, $userId, null, BOURSES_PI, true);
		$db->close();
		
		$this->actions = new ActionContacts($rs);
				
		return $this->actions;
	}
	
	private $formations;
	public function GetFormations($userId = null)
	{
		if (isset($this->formations)) return $this->actions;
		
		$db = new ActionContactDatabase();
		$db->open();
		$rs = $db->getFormations($this->GetContactId(),null, $userId, null, true);
		
		$db->close();
		
		$this->formations = new ActionContacts($rs);
				
		return $this->formations;
	}
	
	
	private $pi;
	public function GetProjetsInnovants()
	{
		if (isset($this->pi)) return $this->pi;

		$db = new ActionContactDatabase();
		$db->open();
		
		$rs = $db->get($this->GetContactId(), null, null, BOURSES_PI);
		$db->close();
		
		$this->pi = new ActionContacts($rs);
		
		return $this->pi;
	}
	
	
    public function SetAdresseId($value) { $this->adresseId = $value; }
    
    private $contactGlobal;
    public function GetContactGlobal() { return $this->contactGlobal; }
    public function GetContactGlobalLabel() { return $this->GetContactGlobal() == 1 ? "Oui" : "Non"; }
	public function GetContactGlobalColor() { return $this->GetContactGlobal() == 1 ? "Green" : "Red"; }
    public function SetContactGlobal($value) { $this->contactGlobal = $value; }

    private $swActif;
    
    public function GetSwActif() { return $this->swActif; }
    public function GetSwActifLabel() { return $this->GetSwActif() == 1 ? "Actif" : "Inactif"; }
	public function GetSwActifColor() { return $this->GetSwActif() == 1 ? "Green" : "Red"; }
    
    public function SetSwActif($value) { $this->swActif = $value; }

    private $created;
    public function GetCreated() { return $this->created; }
    public function SetCreated($value) { $this->created = $value; }

    private $createdBy;
    public function GetCreatedBy() { return $this->createdBy; }
    public function SetCreatedBy($value) { $this->createdBy = $value; }

    private $updated;
    public function GetUpdated() { return $this->updated; }
    public function SetUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function GetUpdatedBy() { return $this->updatedBy; }
    public function SetUpdatedBy($value) { $this->updatedBy = $value; }
    
    private $dateContact;
    public function GetDateContact($formated = null)
    {
    	if (!isset($formated)) return $this->dateContact;
    	else return FormatDate($this->dateContact);
    }
    
    public function GetCreatedByUser()
	{
		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get($this->GetCreatedBy(), null);
		$db->close();
		$users = new Utilisateurs($rs);
		return $users->items(0);
	}
    
    public function GetDateCreatedString($parenthese = true)
    {
    	if($this->GetCreated()!=null)
    	{
    		if($parenthese)
	    	 	return '('.FormatDate($this->GetCreated()).')';
    		else
    			return FormatDate($this->GetCreated());
    	}
    	return '';
    }    
	
	
    public function GetUpdatedByUser()
	{
			$db = new UtilisateurDatabase();
			$db->open();
			$rs = $db->get($this->GetUpdatedBy(), null);
			$db->close();
			$users = new Utilisateurs($rs);
			
			return $users->items(0);
	}
    
    
    public function GetDateUpdateString($parenthese = true)
    {
    	if($this->GetUpdated()!=null)
    	{
    		if($parenthese)
	    	 	return '('.FormatDate($this->GetUpdated()).')';
    		else
    			return FormatDate($this->GetUpdated());
    	}
    	else
    		return '';
    }
    
    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetContactId($row[ContactFields::$contactId]);
        	$this->SetNom($row[ContactFields::$nom]);
        	$this->SetPrenom($row[ContactFields::$prenom]);
        	$this->SetAdresseId($row[ContactFields::$adresseId]);
        	$this->setGenreId($row[ContactFields::$genreId]);
        	$this->setAgeId($row[ContactFields::$ageId]);
        	$this->setStatutId($row[ContactFields::$statutId]);        	
        	$this->SetContactGlobal($row[ContactFields::$contactGlobal]);
	        $this->SetSwActif($row[ContactFields::$swActif]);
        	$this->SetCreated($row[ContactFields::$created]);
        	$this->SetCreatedBy($row[ContactFields::$createdBy]);
        	$this->SetUpdated($row[ContactFields::$updated]);
        	$this->SetUpdatedBy($row[ContactFields::$updatedBy]);
    	}
    }

    function init($contactId, $nom, $prenom, $adresseId, $genreId, $ageId, $statutId, $contactGlobal, 
    				$swActif=1, $created, $createdBy, $updated, $updatedBy)
    {
    	$this->SetContactId($contactId);
       	$this->SetNom($nom);
       	$this->SetPrenom($prenom);
       	$this->SetAdresseId($adresseId);
        $this->setGenreId($genreId);
        $this->setAgeId($ageId);
        $this->setStatutId($statutId);        	
       	$this->SetContactGlobal($contactGlobal);
	    $this->SetSwActif($swActif);
      	$this->SetCreated($created);
       	$this->SetCreatedBy($createdBy);
       	$this->SetUpdated($updated);
       	$this->SetUpdatedBy($updatedBy);
    }
}

class Contacts extends DomainBase
{
    function __construct($rs=null)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0){
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Contact($row));
        }
    }
}
?>