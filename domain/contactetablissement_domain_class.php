<?php
REQUIRE_ONCE(SCRIPTPATH.'model/etablissement_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/contact_database_model_class.php');

REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class ContactEtablissementFields
{
    public static $contactEtablissementId = 'contactEtablissementId';
    public static $etablissementId = 'etablissementId';
    public static $contactId = 'contactId';
    public static $origineContactId = 'origineContactId';
    public static $specialiteContactId = 'specialiteContactId';
    public static $titreContactId = 'titreContactId';
    public static $participationId = 'participationId';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class ContactEtablissement
{
    private $contactEtablissementId;
    public function GetContactEtablissementId() { return $this->contactEtablissementId; }
    public function SetContactEtablissementId($value) { $this->contactEtablissementId = $value; }

    private $etablissementId;
    public function GetEtablissementId() { return $this->etablissementId; }
    public function SetEtablissementId($value) { $this->etablissementId = $value; }

    private $contactId;
    public function GetContactId() { return $this->contactId; }
    public function SetContactId($value) { $this->contactId = $value; }
    
    private $contact;
    public function getContact()
    {
    	if (isset($this->contact)) return $this->contact->items(0);
    	
    	$db = new ContactDatabase();
		$db->open();
		$rs = $db->get($this->GetContactId());
		$db->close();
			
		$this->contact = new Contacts($rs);

		return $this->contact->items(0);
    }

    private $origineContactId;
    public function GetOrigineContactId() { return $this->origineContactId; }
    public function SetOrigineContactId($value) { $this->origineContactId = $value; }
    
    private $origine;
    public function getOrigine()
    {
    	if (isset($this->origine)) return $this->origine->items(0);
    	
    	$this->origine = Dictionnaries::getOrigineList($this->origineContactId);
    	
    	return $this->origine->items(0);
    }

    private $specialiteContactId;
    public function GetSpecialiteContactId() { return $this->specialiteContactId; }
    public function SetSpecialiteContactId($value) { $this->specialiteContactId = $value; }
    
    private $specialiteContact;
    public function getSpecialiteContact()
    {
    	if (isset($this->specialiteContact)) return $this->specialiteContact->items(0);
    	
    	if (! isset($this->specialiteContactId)) return "";
    	else $this->specialiteContact = Dictionnaries::getSpecialiteList($this->specialiteContactId);
    	
    	return $this->specialiteContact->items(0);
    }

    private $titreContactId;
    public function GetTitreContactId() { return $this->titreContactId; }
    public function SetTitreContactId($value) { $this->titreContactId = $value; }
    
    private $titreContact;
    public function getTitreContact()
    {
    	if (isset($this->titreContact)) return $this->titreContact->items(0);
    	
    	// FFI BUG CORRECTION : no titreContact => accompagnateur
    	// RESOLUTION : : no titreContact => ""
    	
    	if (! isset($this->titreContactId)) return "";
    	else $this->titreContact = Dictionnaries::getTitreContactList($this->titreContactId);
    	
    	return $this->titreContact->items(0);
    } 

    private $participationId;
    public function GetParticipationId() { return $this->participationId; }
    public function SetParticipationId($value) { $this->participationId = $value; }
    
    private $participation;
    public function getParticipation()
    {
    	if (isset($this->participation)) return $this->participation->items(0);
    	
    	$this->participation = Dictionnaries::getParticipationList($this->participationId);
    	
    	return $this->participation->items(0);
    }

    private $created;
    public function GetCreated() { return $this->created; }
    public function SetCreated($value) { $this->created = $value; }

    private $createdBy;
    public function GetCreatedBy() { return $this->createdBy; }
    public function SetCreatedBy($value) { $this->createdBy = $value; }

    private $updated;
    public function GetUpdated() { return $this->updated; }
    public function SetUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function GetUpdatedBy() { return $this->updatedBy; }
    public function SetUpdatedBy($value) { $this->updatedBy = $value; }

    private $etab;
    function GetEtablissement()
    {
    	if (isset($this->etab)) return $this->etab->items(0);
    	
    	$db = new EtablissementDatabase();
		$db->open();
		$rs = $db->GetById($this->GetEtablissementId());
		$db->close();
			
		$this->etab = new Etablissements($rs);
			
		return $this->etab->items(0);
    }
	
    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetContactEtablissementId($row[ContactEtablissementFields::$contactEtablissementId]);
        	$this->SetEtablissementId($row[ContactEtablissementFields::$etablissementId]);
        	$this->SetContactId($row[ContactEtablissementFields::$contactId]);
        	$this->SetOrigineContactId($row[ContactEtablissementFields::$origineContactId]);
	        $this->SetSpecialiteContactId($row[ContactEtablissementFields::$specialiteContactId]);
    	    $this->SetTitreContactId($row[ContactEtablissementFields::$titreContactId]);
	        $this->SetParticipationId($row[ContactEtablissementFields::$participationId]);
        	$this->SetCreated($row[ContactEtablissementFields::$created]);
        	$this->SetCreatedBy($row[ContactEtablissementFields::$createdBy]);
        	$this->SetUpdated($row[ContactEtablissementFields::$updated]);
        	$this->SetUpdatedBy($row[ContactEtablissementFields::$updatedBy]);
    	}
    }
    
    function init($contactEtablissementId, $etablissementId, $contactId, 
    				$origineContactId, $specialiteContactId, $titreContactId,
    				$participationId, $created, $createdBy, $updated, $updatedBy) 
    				
    {
        	$this->SetContactEtablissementId($contactEtablissementId);
        	$this->SetEtablissementId($etablissementId);
        	$this->SetContactId($contactId);
        	$this->SetOrigineContactId($origineContactId);
	        $this->SetSpecialiteContactId($specialiteContactId);
    	    $this->SetTitreContactId($titreContactId);
	        $this->SetParticipationId($participationId);
        	$this->SetCreated($created);
        	$this->SetCreatedBy($createdBy);
        	$this->SetUpdated($updated);
        	$this->SetUpdatedBy($updatedBy);
    }
}

class ContactEtablissements extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ContactEtablissement($row));
    }
    
    public function Exists($contactId)
    {
  		for ($i = 0; $i < $this->count(); $i++) if ($this->elements[$i]->GetContactId() == $contactId) return true;
  		
  		return false;
    }
}
?>