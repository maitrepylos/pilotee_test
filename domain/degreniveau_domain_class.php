<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class DegreNiveauFields
{
    public static $degreNiveauId = 'degreNiveauId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
    public static $niveauEtablissementId = 'niveauEtablissementId';
}

class DegreNiveau
{
    private $degreNiveauId;
    public function getDegreNiveauId() { return $this->degreNiveauId; }
    public function setDegreNiveauId($value) { $this->degreNiveauId = $value; }
	public function getId() { return $this->degreNiveauId; }
    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    private $niveauEtablissementId;
    public function getNiveauEtablissementId() { return $this->niveauEtablissementId; }
    public function setNiveauEtablissementId($value) { $this->niveauEtablissementId = $value; }

    function __construct($row = array())
    {
        $this->setDegreNiveauId($row[DegreNiveauFields::$degreNiveauId]);
        $this->setLabel($row[DegreNiveauFields::$label]);
        $this->setOrdre($row[DegreNiveauFields::$ordre]);
        $this->setSwActif($row[DegreNiveauFields::$swActif]);
        $this->setNiveauEtablissementId($row[DegreNiveauFields::$niveauEtablissementId]);
    }
}

class DegreNiveaux extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new DegreNiveau($row));
    }
}
?>