<?php

class Dictionnaries
{
	public static function getProvincesList($provinceId = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/province_database_model_class.php');
		$dbProvinces = new ProvinceDatabase();
		$dbProvinces->open();
		$rs = $dbProvinces->get($provinceId);
		$dbProvinces->close();
		
		return new Provinces($rs);
	}
	
	public static function getNiveauxList($niveauId = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/niveau_database_model_class.php');
		$dbNiveaux = new NiveauDatabase();
		$dbNiveaux->open();
		$rs = $dbNiveaux->get($niveauId);
		$dbNiveaux->close();
		
		return new Niveaux($rs);
	}
	
	public static function getTypeEncodageList($typeEncodageId = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/typeEncodage_database_model_class.php');
		$db = new TypeEncodageDatabase();
		$db->open();
		$rs = $db->get($typeEncodageId);
		$db->close();
		
		return new TypeEncodages($rs);
	}
	
	public static function getGenreList($genreId=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/genre_database_model_class.php');
		$dbGenre = new GenreDatabase();
		$dbGenre->open();
		$rs = $dbGenre->get($genreId);
		$dbGenre->close();
		
		return new Genres($rs);
	}
	
	public static function getStatutList($statutId=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/statut_database_model_class.php');
		$dbStatut = new StatutDatabase();
		$dbStatut->open();
		$rs = $dbStatut->get($statutId);
		$dbStatut->close();
		
		return new Statuts($rs);
	}	
	
	public static function getAgeList($ageId=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/age_database_model_class.php');
		$dbAge = new AgeDatabase();
		$dbAge->open();
		$rs = $dbAge->get($ageId);
		$dbAge->close();
		
		return new Ages($rs);
	}

	public static function getReseauxList($reseauId = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/reseau_database_model_class.php');
		$dbReseaux = new ReseauDatabase();
		$dbReseaux->open();
		$rs = $dbReseaux->get($reseauId);
		$dbReseaux->close();
		
		return new Reseaux($rs);
	}

	public static function getUtilisateursList($type, $userId = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
		$dbUsers = new UtilisateurDatabase();
		$dbUsers->open();
		$rs = $dbUsers->get($userId, $type);
		$dbUsers->close();
		
		return new Utilisateurs($rs);
	}
	
	public static function getArrondissementList($arrondissementId = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/arrondissement_database_model_class.php');
		$dbArrondissements = new ArrondissementDatabase();
		$dbArrondissements->open();
		$rs = $dbArrondissements->get($arrondissementId);
		$dbArrondissements->close();
		
		return new Arrondissements($rs);
	}
	
	public static function getAnneeList($anneeId = null, $current = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/annee_database_model_class.php');
		$dbAnnees = new AnneeDatabase();
		$dbAnnees->open();
		$rs = $dbAnnees->get($anneeId, $current);
		$dbAnnees->close();		
		
		return new Annees($rs);
	}
	
    public static function getAnneeEnCours()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/annee_database_model_class.php');
		
		$dbAnnees = new AnneeDatabase();
		$dbAnnees->open();
		$rs = $dbAnnees->GetAnneeEnCoursId();
		$dbAnnees->close();		
		
		return new Annees($rs);
	}

	/*
	public static function getCurrentAnneeId()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/annee_database_model_class.php');
		$dbAnnees = new AnneeDatabase();
		$dbAnnees->open();
		$rs = $dbAnnees->get(null, null);
		$dbAnnees->close();

		$annees = new Annees($rs); 
		
		return ((isset($annees)) && ($annees->count() > 0)) ? $annees->items($annees->count() - 1)->getId() : null; 
	}
	*/
	
	public static function getSectionActionList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
		$dbAction = new ActionDatabase();
		$dbAction->open();
		$rs = $dbAction->getSectionActionList($id);
		$dbAction->close();
	
		return new SectionActions($rs);
	}
	
	public static function getTypeActionList($operateur = false, $agent = false, $pi = false)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/typeaction_database_model_class.php');
		$dbTypesAction = new TypeActionDatabase();
		$dbTypesAction->open();
		$rs = $dbTypesAction->getListAddAction($operateur, $agent, $pi);
		$dbTypesAction->close();		
		
		return new TypeActions($rs);
	}
	
	public static function getCategorieActionNonLabList($id = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/categActionNonLabel_database_model_class.php');
		$db = new CategActionNonLabelDatabase();
		$db->open();
		$rs = $db->get($id);
		$db->close();		
		
		return new CategActionNonLabels($rs);
	}
	
	
	public static function getFiliereDegreNiveauList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
		$dbAction = new ActionDatabase();
		$dbAction->open();
		$rs = $dbAction->getFiliereDegreNiveauList($id);
		$dbAction->close();		
		
		return new FiliereDegreNiveaus($rs);
	}
	
	public static function getNiveauList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
		$dbAction = new ActionDatabase();
		$dbAction->open();
		$rs = $dbAction->getNiveauList($id);
		$dbAction->close();		
		
		return new Niveaux($rs);
	}
	
	public static function getNiveauDegreList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
		$dbAction = new ActionDatabase();
		$dbAction->open();
		$rs = $dbAction->getNiveauDegreList($id);
		$dbAction->close();		
		
		return new DegreNiveaux($rs);
	}
	
	public static function getCategActionNonLabelList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
		$dbAction = new ActionDatabase();
		$dbAction->open();
		$rs = $dbAction->getCategActionNonLabelList($id);
		$dbAction->close();		
		
		return new CategActionNonLabels($rs);
	}
	
	public static function getFinalitePedagogList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/action_database_model_class.php');
		$dbAction = new ActionDatabase();
		$dbAction->open();
		$rs = $dbAction->getFinalitePedagogList($id);
		$dbAction->close();		
		
		return new FinalitePedagogs($rs);
	}
	
	public static function getStatutSubventionList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/bourse_database_model_class.php');
		$dbBourse = new BourseDatabase();
		$dbBourse->open();
		$rs = $dbBourse->getStatutSubventionList($id);
		$dbBourse->close();
				
		return new StatutSubventions($rs);
	}
	
	
	public static function getOperateurList($id = null, $actif = false)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/operateur_database_model_class.php');
		$dbOp = new OperateurDatabase();
		$dbOp->open();
		$rs = $dbOp->get($id, $actif);
		$dbOp->close();
		
		return new Operateurs($rs);
	}
	
	public static function getTitreContactList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/titreContact_database_model_class.php');
		$dbC = new TitreContactDatabase();
		$dbC->open();
		$rs = $dbC->get($id);
		$dbC->close();

		return new TitreContacts($rs);
	}
	

	public static function getRoleContactActionListWithoutParticipant($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/roleContactAction_database_model_class.php');
		
		$dbC = new RoleContactActionDatabase();
		$dbC->open();
		$rs = $dbC->getWithoutParticipant($id);
		$dbC->close();

		return new RoleContactActions($rs);
	}
	
	public static function getRoleContactActionList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/roleContactAction_database_model_class.php');
		
		$dbC = new RoleContactActionDatabase();
		$dbC->open();
		$rs = $dbC->get($id);
		$dbC->close();

		return new RoleContactActions($rs);
	}
	
	public static function getSyntheseAvisList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/syntheseAvis_database_model_class.php');
		
		$dbC = new SyntheseAvisDatabase();
		$dbC->open();
		$rs = $dbC->get($id);
		$dbC->close();

		return new SyntheseAviss($rs);
	}
	
	public static function getParticipationList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/participation_database_model_class.php');
		$dbC = new ParticipationDatabase();
		$dbC->open();
		$rs = $dbC->get($id);
		$dbC->close();
		return new Participations($rs);
	}
	
	public static function getSpecialiteList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/specialiteContact_database_model_class.php');
		$dbC = new SpecialiteContactDatabase();
		$dbC->open();
		$rs = $dbC->get($id);
	
		$dbC->close();
		return new Specialites($rs);
	}

	public static function getOrigineList($id=null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/origineContact_database_model_class.php');
		
		$dbC = new OrigineContactDatabase();
		$dbC->open();
		$rs = $dbC->get($id);
		$dbC->close();

		return new OrigineContacts($rs);
	}
	
	
	public static function GetCodePostals($cp = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/codepostal_database_model_class.php');
		$db = new CodePostalDatabase();
		$db->open();
		$rs = $db->get($cp);
		$db->close();
		
		return new CodePostals($rs);
	}
	
	public static function GetTimingSuivis($ts = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/timingSuivi_database_model_class.php');
				
		$db = new TimingSuiviDatabase();
		$db->open();
		$rs = $db->get($ts);
		$db->close();
		
		return new TimingSuivis($rs);
	}
	
	public static function GetModeSuivis($ms = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/modeSuivi_database_model_class.php');
		$db = new ModeSuiviDatabase();
		$db->open();
		$rs = $db->get($ms);
		$db->close();
		
		return new ModeSuivis($rs);
	}
	
	public static function GetIntentionSuivis($is = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/intentionSuivi_database_model_class.php');
		$db = new IntentionSuiviDatabase();
		$db->open();
		$rs = $db->get($is);
		$db->close();
		
		return new IntentionSuivis($rs);
	}
	
	public static function GetActionLabelList($id = null, $operateurId = null, $anneeScolaireId = null )
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/action_label_database_model_class.php');
		
		$db = new ActionLabelDatabase();
		$db->open();
		$rs = $db->get($id, $operateurId, $anneeScolaireId);
		$db->close();
		
		return new ActionLabels($rs);
	}
	
	public static function GetMaterielPedag($mp = null)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/materielPedag_database_model_class.php');
		
		$db = new MaterielPedagDatabase();
		$db->open();
		$rs = $db->get($mp);
		$db->close();
			
		return new MaterielPedags($rs);
	}
	
	public static function getFormationType($id = null)
	{
		REQUIRE_ONCE(SCRIPTPATH . 'model/formation_type_database_model_class.php');
		REQUIRE_ONCE(SCRIPTPATH . 'domain/formation_type_domain_class.php');

		$db = new FormationTypeDatabase();
		$db->open();
		$rs = (empty($id)) ? $db->findAll() : $db->getById(intval($id));
		return new FormationTypes($rs);
	}
	/* Marucci 24/12/2013 Ajout de la fonction getUtilisateurType retourne une liste de type utilisateur */
	public static function getUtilisateurType($id = null)
	{
		REQUIRE_ONCE(SCRIPTPATH . 'domain/typeutilisateur_domain_class.php');
		REQUIRE_ONCE(SCRIPTPATH . 'model/typeUtilisateur_database_model_class.php');

		$db = new TypeUtilisateurDatabase();
		$db->open();

		$rs = (empty($id)) ? $db->findAll() : $db->getById(intval($id));
		return new TypeUtilisateurs($rs);
	}
}
