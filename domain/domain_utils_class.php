<?php

class DomainUtils
{
	const DOMAIN_NAME_POSTFIX = 's';
	const DOMAIN_FIELD_POSTFIX = 'Fields';

	public static function mapFromFieldsToDomainObject($data, $objectToMap, $objectField = null)
	{
		$objectField = (empty($objectField)) ? (get_class($objectToMap) . self::DOMAIN_FIELD_POSTFIX) : $objectField;

		if (!class_exists($objectField))
		{
			return false;
		}

		$fields = get_class_vars($objectField);
		foreach ($fields as $fieldName => $defValue)
		{
			$setMethod = 'set' . ucfirst($fieldName);
			$objectToMap->$setMethod($data[ $defValue ]);
		}
	}
}
