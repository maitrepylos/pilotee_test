<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/etablissement_database_model_class.php');
class EntiteAdminFields
{
    public static $entiteAdminId = 'entiteAdminId';
    public static $nom = 'nom';
    public static $adresseId = 'adresseId';
    public static $website = 'website';
    public static $swActif = 'swActif';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class EntiteAdmin
{
    private $entiteAdminId;
    public function getEntiteAdminId() { return $this->entiteAdminId; }
    public function setEntiteAdminId($value) { $this->entiteAdminId = $value; }

    private $nom;
    public function getNom() { return $this->nom; }
    public function setNom($value) { $this->nom = $value; }

    private $adresseId;
    public function getAdresseId() { return $this->adresseId; }
    private $adresse;
	public function getAdresse() 
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');
		if (isset($this->adresse)) return $this->adresse->items(0);
		$db = new AdresseDatabase();
		$db->open();
		$rs = $db->get($this->GetAdresseId());
		$db->close();
		$this->adresse = new Adresses($rs);
		return $this->adresse->items(0);
	}
    public function setAdresseId($value) { $this->adresseId = $value; }

    private $website;
    public function getWebsite() { return $this->website; }
    public function setWebsite($value) { $this->website = $value; }
	public function GetWebsiteAsAnchor() { return sprintf('<a href="%s" target="none">%s</a>', $this->getWebsite(), $this->getWebsite()); }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    private $created;
    public function getCreated() { return $this->created; }
    public function setCreated($value) { $this->created = $value; }

    private $createdBy;
    public function getCreatedBy() { return $this->createdBy; }
    public function setCreatedBy($value) { $this->createdBy = $value; }

    private $updated;
    public function getUpdated() { return $this->updated; }
    public function setUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function getUpdatedBy() { return $this->updatedBy; }
    public function setUpdatedBy($value) { $this->updatedBy = $value; }

    
    public function getEtablissements()
    {
    	$db = new EtablissementDatabase();
    	$db->open();
    		$etabs  = new Etablissements($db->getByEntiteAdmin($this->getEntiteAdminId()));
    	$db->close();
    	return $etabs;
    }
    
    public function GetCreatedByUser()
	{
		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get($this->getCreatedBy(), null);
		$db->close();
		$users = new Utilisateurs($rs);
		return $users->items(0);
	}
    
    public function GetDateCreatedString($parenthese = true)
    {
    	if($this->getCreated()!=null)
    	{
    		if($parenthese)
	    	 	return '('.FormatDate($this->getCreated()).')';
    		else
    			return FormatDate($this->getCreated());
    	}
    	return '';
    }    
	
	
    public function GetUpdatedByUser()
	{
			$db = new UtilisateurDatabase();
			$db->open();
			$rs = $db->get($this->getUpdatedBy(), null);
			$db->close();
			$users = new Utilisateurs($rs);
			
			return $users->items(0);
	}
    
    
    public function GetDateUpdateString($parenthese = true)
    {
    	if($this->GetUpdated()!=null)
    	{
    		if($parenthese)
	    	 	return '('.FormatDate($this->getUpdated()).')';
    		else
    			return FormatDate($this->getUpdated());
    	}
    	else
    		return '';
    }    
    
    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->setEntiteAdminId($row[EntiteAdminFields::$entiteAdminId]);
        	$this->setNom($row[EntiteAdminFields::$nom]);
        	$this->setAdresseId($row[EntiteAdminFields::$adresseId]);
        	$this->setWebsite($row[EntiteAdminFields::$website]);
        	$this->setSwActif($row[EntiteAdminFields::$swActif]);
        	$this->setCreated($row[EntiteAdminFields::$created]);
        	$this->setCreatedBy($row[EntiteAdminFields::$createdBy]);
        	$this->setUpdated($row[EntiteAdminFields::$updated]);
        	$this->setUpdatedBy($row[EntiteAdminFields::$updatedBy]);
    	}
    }
    
    function init($entiteAdminId, $nom, $adresseId, $website, $swActif, $created, $createdBy, $updated, $updatedBy)
    {
        $this->setEntiteAdminId($entiteAdminId);
        $this->setNom($nom);
        $this->setAdresseId($adresseId);
        $this->setWebsite($website);
        $this->setSwActif($swActif);
        $this->setCreated($created);
        $this->setCreatedBy($createdBy);
        $this->setUpdated($updated);
        $this->setUpdatedBy($updatedBy);    	
    }
    
}

class EntiteAdmins extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new EntiteAdmin($row));
    }
}
?>