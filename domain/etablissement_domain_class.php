<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

//REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/remiseMaterielPedagEtab_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/action_etablissement_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/contact_etablissement_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/entiteAdmin_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/ficheSuivi_database_model_class.php');

/*
REQUIRE_ONCE(SCRIPTPATH.'domain/utilisateur_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/adresse_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/actionetablissement_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/contactEtablissement_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/fichesuivi_domain_class.php');
*/

class EtablissementFields
{
	public static $etablissementId = 'etablissementId';
	public static $utilisateurId = 'utilisateurId';
	public static $nom = 'nom';
	public static $adresseId = 'adresseId';
	public static $website = 'website';
	public static $niveauEtablissementId = 'niveauEtablissementId';
	public static $reseauEtablissementId = 'reseauEtablissementId';
	public static $entiteAdminId = 'entiteAdminId';
	public static $swActif = 'swActif';
	public static $created = 'created';
	public static $createdBy = 'createdBy';
	public static $updated = 'updated';
	public static $updatedBy = 'updatedBy';
}

class Etablissement
{
	private $etablissementId;
	public function GetEtablissementId() { return $this->etablissementId; }
	public function SetEtablissementId($value) { $this->etablissementId = $value; }
	
	private $utilisateurId;
	public function GetUtilisateurId() { return $this->utilisateurId; }
	public function GetUtilisateur()
	{
		if (isset($this->utilisateurId))
		{
			$db = new UtilisateurDatabase();
			$db->open();
			$rs = $db->get($this->GetUtilisateurId(), null);
			$db->close();
			
			$users = new Utilisateurs($rs);
			
			return $users->items(0);
		}

		return null;
	}
	public function SetUtilisateurId($value) { $this->utilisateurId = $value; }
	
	private $nom;
	public function GetNom() { return $this->nom; }
	public function SetNom($value) { $this->nom = $value; }
	
	private $adresseId;
	public function GetAdresseId() { return $this->adresseId; }
	
	private $adresse;
	public function GetAdresse()
	{
		if (isset($this->adresse)) return $this->adresse->items(0);
		
		$db = new AdresseDatabase();
		$db->open();
		$rs = $db->get($this->GetAdresseId());
		$db->close();
			
		$this->adresse = new Adresses($rs);
			
		return $this->adresse->items(0);
	}
	public function SetAdresseId($value) { $this->adresseId = $value; }
	
	private $website;
	public function GetWebsite() { return $this->website; }
	public function GetWebsiteAsAnchor() { return sprintf('<a href="%s" target="none">%s</a>', $this->GetWebsite(), $this->GetWebsite()); }
	public function SetWebsite($value) { $this->website = $value; }
	
	private $niveauEtablissementId;
	public function GetNiveauEtablissementId() { return $this->niveauEtablissementId; }
	public function GetNiveauEtablissement() { return Dictionnaries::getNiveauxList($this->GetNiveauEtablissementId())->items(0)->getLabel(); }
	public function SetNiveauEtablissementId($value) { $this->niveauEtablissementId = $value; }
	
	private $reseauEtablissementId;
	public function GetReseauEtablissementId() { return $this->reseauEtablissementId; }
	
	public function GetReseauEtablissement() 
	{
		$var = Dictionnaries::getReseauxList($this->GetReseauEtablissementId())->items(0);
		if($var!=null)
			return $var->getLabel(); 
		else
			return '';
	}	
	
	public function SetReseauEtablissementId($value) { $this->reseauEtablissementId = $value; }
	
	private $entiteAdminId;
	public function GetEntiteAdminId() { return $this->entiteAdminId; }
	public function SetEntiteAdminId($value) { $this->entiteAdminId = $value; }
	
	private $entiteAdminLabel;
	public function GetEntiteAdminLabel() 
	{
		if($this->GetEntiteAdminId()!=null)
		{
			$db = new EntiteAdminDatabase();
			$db->open();
			$rs = $db->get($this->GetEntiteAdminId());
			$db->close();
			$entiteAdmins = new EntiteAdmins($rs);
			
			if (isset($entiteAdmins) && $entiteAdmins->Count() == 1)
				$this->entiteAdminLabel = $entiteAdmins->items(0)->getNom();
			else
				$this->entiteAdminLabel='';
		}
		else
			 $this->entiteAdminLabel='';
			 
		return $this->entiteAdminLabel;
	 }
	
	
	private $swActif;
	public function GetSwActif() { return $this->swActif; }
	public function SetSwActif($value) { $this->swActif = $value; }
	
	private $created;
	public function GetCreated() { return $this->created; }
	public function SetCreated($value) { $this->created = $value; }
	
	private $createdBy;
	public function GetCreatedById() { return $this->createdBy; }
	public function GetCreatedBy()
	{
		if (isset($this->createdBy))
		{
			$db = new UtilisateurDatabase();
			$db->open();
			$rs = $db->get($this->GetCreatedById(), null);
			$db->close();
			
			$users = new Utilisateurs($rs);
			
			return $users->count()> 0 ? $users->items(0) : null;
		}
		
		return null;
	} 
	public function SetCreatedBy($value) { $this->createdBy = $value; }
	
	private $updated;
	public function GetUpdated() { return $this->updated; }
	public function SetUpdated($value) { $this->updated = $value; }
	
	private $updatedBy;
	public function GetUpdatedById() { return $this->updatedBy; }
	public function GetUpdatedBy()
	{
		if (isset($this->updated))
		{
			$db = new UtilisateurDatabase();
			$db->open();
			$rs = $db->get($this->GetUpdatedById(), null);
			$db->close();
			
			$users = new Utilisateurs($rs);
			
			return $users->count()> 0 ? $users->items(0) : null;
		}
		
		return null;
	} 
	public function SetUpdatedBy($value) { $this->updatedBy = $value; }
	
	private $fiches;
	public function GetCahierDeSuivi()
	{
		if (isset($this->fiches)) return $this->fiches;
		
		$db = new FicheSuiviDatabase();
		$db->open();
		$rs = $db->get(null, $this->GetEtablissementId(), true);
		$db->close();
		
		$this->fiches = new FicheSuivis($rs);
		
		return $this->fiches;
	}
	
	private $materiel;
	public function GetMaterielPedag()
	{
		if (isset($this->materiel)) return $this->materiel;
		 
		$db = new RemiseMaterielPedagEtabDatabase();
		$db->open();
		$rs = $db->get(null, $this->GetEtablissementId(), true);
		$db->close();
		
		$this->materiel = new RemiseMaterielPedagEtabs($rs); 
		
		return $this->materiel;
	}
	
	private $pi;
	public function GetProjetsInnovants()
	{
		if (isset($this->pi)) return $this->pi;

		$db = new ActionEtablissementDatabase();
		$db->open();
		$rs = $db->get($this->GetEtablissementId(), null, BOURSES_PI, null, null, null, true);
		$db->close();
		
		$this->pi = new ActionEtablissements($rs);
		
		return $this->pi;
	}
	
	public function GetProjetsInnovantsObtenu()
	{
		//if (isset($this->pi)) return $this->pi;

		$db = new ActionEtablissementDatabase();
		$db->open();		
		$rs = $db->get($this->GetEtablissementId(), null, BOURSES_PI, null, null, 'OBTEN');
		$db->close();
		
		$pi = new ActionEtablissements($rs);
		
		return $pi;
	}	
	
	private $actions;
	public function GetActions($operateurId = null, $exclude = BOURSES_PI)
	{
		if (isset($this->actions)) return $this->actions;
		
		$db = new ActionEtablissementDatabase();
		$db->open();
		$rs = $db->get($this->GetEtablissementId(), $operateurId, null, $exclude, null, null, true);
		$db->close();
		
		$this->actions = new ActionEtablissements($rs);
				
		return $this->actions;
	}
	
	private $formations;
	public function GetFormations($operateurId = null)
	{
		if (isset($this->formations)) return $this->$formations;
		
		$db = new ActionEtablissementDatabase();
		$db->open();
		$rs = $db->getFormations($this->GetEtablissementId(), $operateurId, true);
		$db->close();
		
		$this->formations = new ActionEtablissements($rs);
				
		return $this->formations;
	}
	
	private $contacts;
	public function GetContacts()
	{
		if (isset($this->contacts)) return $this->contacts;

		$db = new ContactEtablissementDatabase();
		$db->open();
		$rs = $db->get(null, null, $this->GetEtablissementId(), true);
		$db->close();
		
		$this->contacts = new ContactEtablissements($rs);
		
		return $this->contacts;
	}
	
	public function getVisited($anneeId = null)
	{
		$visited = 0;
		$fiches = $this->GetCahierDeSuivi();
		
		if ($fiches->count() > 0)
		{
			//$visited = -1;
			
			$as = isset($anneeId) ? Dictionnaries::getAnneeList($anneeId, null) : Dictionnaries::getAnneeList(null, date('Y-m-d 00:00:00'));
			$start = $as->items(0)->getDateDeb();
			$end = $as->items(0)->getDateFin();

			for ($i = 0; $i < $fiches->count(); $i++)
			{
				if (($fiches->items($i)->GetDateRencontre() >= $start) && ($fiches->items($i)->GetDateRencontre() <= $end))
				{
					$visited = 1;
					break;
				}
				else
				{
					if ($fiches->items($i)->GetDateRencontre() < $start) $visited = -1;
				}
			}
		}
		
		if ($visited == 0)
		{
			$material = $this->GetMaterielPedag();
			
			if ($material->count() > 0)
			{
				//$visited = -1;
				
				// $as = Dictionnaries::getAnneeList($anneeId, date('Y-m-d 00:00:00'));
				$as = isset($anneeId) ? Dictionnaries::getAnneeList($anneeId, null) : Dictionnaries::getAnneeList(null, date('Y-m-d 00:00:00'));
				$start = $as->items(0)->getDateDeb();
				$end = $as->items(0)->getDateFin();
	
				for ($i = 0; $i < $material->count(); $i++)
				{
					if (($material->items($i)->GetDate() >= $start) && ($material->items($i)->GetDate() <= $end))
					{
						$visited = 1;
						break;
					}
					else
					{
						if ($material->items($i)->GetDate() < $start) $visited = -1;						
					}
				}
			}
		}
		
		return $visited;
	}

	public function getActivity($anneeId = null)
	{
		$activity = 0;
		$actions = $this->GetActions();
		
		if ($actions->count() > 0)
		{
			//$activity = -1;
			
			//$as = Dictionnaries::getAnneeList(null, date('Y-m-d 00:00:00'));
			$as = isset($anneeId) ? Dictionnaries::getAnneeList($anneeId, null) : Dictionnaries::getAnneeList(null, date('Y-m-d 00:00:00'));
			//pourquoi cette ligne était commentée? commencait par xb, empechait de voir l'acitvité pour l'année précédente
			$currentAnnee = $as->items(0)->getId();

			for ($i = 0; $i < $actions->count(); $i++)
			{
				if ($actions->items($i)->GetAction()->GetStatutActionId() == ACTION_VALID)
				{
					if ($actions->items($i)->GetAction()->GetAnneeId() == $currentAnnee)
					{
						$activity = 1;
						break;
					}
					else
					{
						if ($actions->items($i)->GetAction()->GetAnneeId() < $currentAnnee) $activity = -1;
					}
				}
			}
		}
		
		if ($activity == 0)
		{
			$projets = $this->GetProjetsInnovantsObtenu();
			
			if ($projets->count() > 0)
			{
				$as = isset($anneeId) ? Dictionnaries::getAnneeList($anneeId, null) : Dictionnaries::getAnneeList(null, date('Y-m-d 00:00:00'));
				$currentAnnee = $as->items(0)->getId();

				for ($i = 0; $i < $projets->count(); $i++)
				{
					if ($projets->items($i)->GetAction()->GetStatutActionId() == ACTION_VALID)
					{
						if ($projets->items($i)->GetAction()->GetAnneeId() == $currentAnnee)
						{
							$activity = 1;
							break;
						}
						else
						{
							if ($projets->items($i)->GetAction()->GetAnneeId() < $currentAnnee) $activity = -1;
						}
					}
				}
			}
		}
		
		return $activity;
	}
	
	function __construct($row = null)
	{
		if(isset($row))
		{
			$this->SetEtablissementId($row[EtablissementFields::$etablissementId]);
			$this->SetUtilisateurId($row[EtablissementFields::$utilisateurId]);
			$this->SetNom($row[EtablissementFields::$nom]);
			$this->SetAdresseId($row[EtablissementFields::$adresseId]);
			$this->SetWebsite($row[EtablissementFields::$website]);
			$this->SetNiveauEtablissementId($row[EtablissementFields::$niveauEtablissementId]);
			$this->SetReseauEtablissementId($row[EtablissementFields::$reseauEtablissementId]);
			$this->SetEntiteAdminId($row[EtablissementFields::$entiteAdminId]);
			$this->SetSwActif($row[EtablissementFields::$swActif]);
			$this->SetCreated($row[EtablissementFields::$created]);
			$this->SetCreatedBy($row[EtablissementFields::$createdBy]);
			$this->SetUpdated($row[EtablissementFields::$updated]);
			$this->SetUpdatedBy($row[EtablissementFields::$updatedBy]);
		}
	}
	
	function init($etablissementId, $utilisateurId, $nom, $adresseId, $website, 
					$niveauEtablissementId, $reseauEtablissementId, $entiteAdminId,
					$swActif, $created, $createdBy, $updated, $updatedBy)
	{
		$this->SetEtablissementId($etablissementId);
		$this->SetUtilisateurId($utilisateurId);
		$this->SetNom($nom);
		$this->SetAdresseId($adresseId);
		$this->SetWebsite($website);
		$this->SetNiveauEtablissementId($niveauEtablissementId);
		$this->SetReseauEtablissementId($reseauEtablissementId);
		$this->SetEntiteAdminId($entiteAdminId);
		$this->SetSwActif($swActif);
		$this->SetCreated($created);
		$this->SetCreatedBy($createdBy);
		$this->SetUpdated($updated);
		$this->SetUpdatedBy($updatedBy);
	}
	
}

class Etablissements extends DomainBase
{
	function __construct($rs = null)
	{
		parent::__construct();

		if ($rs && mysqli_num_rows($rs) > 0)
		{
			while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Etablissement($row));
		}
	}
}
?>
