<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class EtablissementSearchResultFields
{
	public static $agentPrenom = 'Agent_Prenom';
	public static $agentNom = 'Agent_Nom';
	public static $agentId = 'UtilisateurId';
	public static $id = 'Etablissement_Id';
	public static $nom = 'Etablissement_Nom';
	public static $niveau = 'Etablissement_Niveau';
	public static $reseau = 'Etablissement_Reseau';
	public static $codePostal = 'Etablissement_CodePostal';
	public static $ville = 'Etablissement_Ville';
	public static $province = 'Etablissement_Province';
	public static $arrondissement = 'Etablissement_Arrondissement';
}

class EtablissementSearchResult
{
	private $agentPrenom;
	public function getAgentPrenom() { return $this->agentPrenom; }
	public function setAgentPrenom($value) { $this->agentPrenom = $value; }
	
	private $agentNom;
	public function getAgentNom() { return $this->agentNom; }
	public function setAgentNom($value) { $this->agentNom = $value; }
	
	private $agentId;
	public function getAgentId() { return $this->agentId; }
	public function setAgentId($value) { $this->agentId = $value; }
	
	
	private $id;
	public function getId() { return $this->id; }
	public function setId($value) { $this->id = $value; }
	
	private $nom;
	public function getNom() { return $this->nom; }
	public function setNom($value) { $this->nom = $value; }

	private $niveau;
	public function getNiveau() { return $this->niveau; }
	public function setNiveau($value) { $this->niveau = $value; }

	private $reseau;
	public function getReseau() { return $this->reseau; }
	public function setReseau($value) { $this->reseau = $value; }

	private $codePostal;
	public function getCodePostal() { return $this->codePostal; }
	public function setCodePostal($value) { $this->codePostal = $value; }

	private $ville;
	public function getVille() { return $this->ville; }
	public function setVille($value) { $this->ville = $value; }

	private $province;
	public function getProvince() { return $this->province; }
	public function setProvince($value) { $this->province = $value; }

	private $arrondissement;
	public function getArrondissement() { return $this->arrondissement; }
	public function setArrondissement($value) { $this->arrondissement = $value; }

	function __construct($row = array())
	{
		if(isset($row[EtablissementSearchResultFields::$agentPrenom]))
			$this->setAgentPrenom($row[EtablissementSearchResultFields::$agentPrenom]);
		if(isset($row[EtablissementSearchResultFields::$agentNom]))
			$this->setAgentNom($row[EtablissementSearchResultFields::$agentNom]);
		if(isset($row[EtablissementSearchResultFields::$agentId]))	
			$this->setAgentId($row[EtablissementSearchResultFields::$agentId]);
		if(isset($row[EtablissementSearchResultFields::$id]))	
			$this->setId($row[EtablissementSearchResultFields::$id]);
		if(isset($row[EtablissementSearchResultFields::$nom]))			
			$this->setNom($row[EtablissementSearchResultFields::$nom]);
		if(isset($row[EtablissementSearchResultFields::$niveau]))	
			$this->setNiveau($row[EtablissementSearchResultFields::$niveau]);
		if(isset($row[EtablissementSearchResultFields::$reseau]))	
			$this->setReseau($row[EtablissementSearchResultFields::$reseau]);
		if(isset($row[EtablissementSearchResultFields::$codePostal]))	
			$this->setCodePostal($row[EtablissementSearchResultFields::$codePostal]);
		if(isset($row[EtablissementSearchResultFields::$ville]))	
			$this->setVille($row[EtablissementSearchResultFields::$ville]);
		if(isset($row[EtablissementSearchResultFields::$province]))	
			$this->setProvince($row[EtablissementSearchResultFields::$province]);
		if(isset($row[EtablissementSearchResultFields::$arrondissement]))	
			$this->setArrondissement($row[EtablissementSearchResultFields::$arrondissement]);
	}
}

class EtablissementSearchResults extends DomainBase
{
	function __construct($rs)
	{
		parent::__construct();
		
		if ($rs && mysqli_num_rows($rs) > 0)
		{
			while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new EtablissementSearchResult($row));
		}
	}
}
?>