<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

REQUIRE_ONCE(SCRIPTPATH.'model/ficheSuivi_contact_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/etablissement_database_model_class.php');

class FicheSuiviFields
{
    public static $ficheSuiviId = 'ficheSuiviId';
    public static $modeSuiviId = 'modeSuiviId';
    public static $etablissementId = 'etablissementId';
    public static $intentionSuiviId = 'intentionSuiviId';
    public static $timingSuiviId = 'timingSuiviId';
    public static $commentaire = 'commentaire';
    public static $dateRencontre = 'dateRencontre';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class FicheSuivi
{
    private $ficheSuiviId;
    public function GetFicheSuiviId() { return $this->ficheSuiviId; }
    public function SetFicheSuiviId($value) { $this->ficheSuiviId = $value; }

    private $modeSuiviId;
    public function GetModeSuiviId() { return $this->modeSuiviId; }
    public function GetModeSuivi() { return $this->modeSuiviId != null ? Dictionnaries::GetModeSuivis($this->GetModeSuiviId())->items(0) : null; }
    public function SetModeSuiviId($value) { $this->modeSuiviId = $value; }

    private $etablissementId;
    public function GetEtablissementId() { return $this->etablissementId; }
    public function SetEtablissementId($value) { $this->etablissementId = $value; }
    
    private $etablissement;
    public function GetEtablissement()
    {
    	if (isset($this->etablissement)) return $this->etablissement->items(0);
    	
		$db = new EtablissementDatabase();
		$db->open();
		$rs = $db->GetById($this->GetEtablissementId());
		$db->close();
			
		$this->etablissement = new Etablissements($rs);
    	
    	return $this->etablissement->items(0);
    } 

    private $intentionSuiviId;
    public function GetIntentionSuiviId() { return $this->intentionSuiviId; }
    public function GetIntentionSuivi() { return $this->intentionSuiviId != null ? Dictionnaries::GetIntentionSuivis($this->GetIntentionSuiviId())->items(0) : null; }
    public function SetIntentionSuiviId($value) { $this->intentionSuiviId = $value; }

    private $timingSuiviId;
    public function GetTimingSuiviId() { return $this->timingSuiviId; }
    public function GetTimingSuivi() { return $this->timingSuiviId != null ? Dictionnaries::GetTimingSuivis($this->GetTimingSuiviId())->items(0) : null; }
    public function SetTimingSuiviId($value) { $this->timingSuiviId = $value; }

    private $commentaire;
    public function GetCommentaire() { return $this->commentaire; }
    public function SetCommentaire($value) { $this->commentaire = $value; }

    private $dateRencontre;
    public function GetDateRencontre($formated = null)
    {
    	if (!isset($formated)) return $this->dateRencontre;
    	else return FormatDate($this->dateRencontre);
    }
    public function SetDateRencontre($value) { $this->dateRencontre = $value; }

    private $created;
    public function GetCreated() { return $this->created; }
    public function SetCreated($value) { $this->created = $value; }
    
    private $createdBy;
    public function GetCreatedBy() { return $this->createdBy; }
    public function SetCreatedBy($value) { $this->createdBy = $value; }
    
    private $creationString;
    public function getCreationString()
    {
    	if (isset($this->creationString)) return $this->creationString;

		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get($this->GetCreatedBy(), null);
		$db->close();
			
		$users = new Utilisateurs($rs);
    	$date = FormatDate($this->GetCreated());
    	
    	$this->creationString = sprintf('le %s par %s %s', $date, $users->items(0)->getPrenom(), $users->items(0)->getNom());
    	
    	return $this->creationString;
    }

    private $updated;
    public function GetUpdated() { return $this->updated; }
    public function SetUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function GetUpdatedBy() { return $this->updatedBy; }
    public function SetUpdatedBy($value) { $this->updatedBy = $value; }

    private $updatedString;
    public function getUpdatedString()
    {
    	if (isset($this->updatedString)) return $this->updatedString;

		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get($this->GetUpdatedBy(), null);
		$db->close();
			
		$users = new Utilisateurs($rs);
    	$date = FormatDate($this->GetUpdated());
    	
    	$this->updatedString = sprintf('le %s par %s %s', $date, $users->items(0)->getPrenom(), $users->items(0)->getNom());
    	
    	return $this->updatedString;
    }
    
    public function GetContactList()
    {
		$db = new FicheSuiviContactDatabase();
		$db->open();
		$rs = $db->GetFicheSuiviContacts($this->GetFicheSuiviId());
		$db->close();
		
		return new Contacts($rs);
    }
    
    public function delete()
    {
		$db = new FicheSuiviDatabase();
		$db->open();
		$result = $db->delete($this->GetFicheSuiviId());
		$db->close();
		
		$db = new FicheSuiviContactDatabase();
		$db->open();
		$result = $db->delete($this->GetFicheSuiviId());
		$db->close();
		return $result;
	}

    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetFicheSuiviId($row[FicheSuiviFields::$ficheSuiviId]);
        	$this->SetModeSuiviId($row[FicheSuiviFields::$modeSuiviId]);
        	$this->SetEtablissementId($row[FicheSuiviFields::$etablissementId]);
        	$this->SetIntentionSuiviId($row[FicheSuiviFields::$intentionSuiviId]);
        	$this->SetTimingSuiviId($row[FicheSuiviFields::$timingSuiviId]);
        	$this->SetCommentaire($row[FicheSuiviFields::$commentaire]);
        	$this->SetDateRencontre($row[FicheSuiviFields::$dateRencontre]);
        	$this->SetCreated($row[FicheSuiviFields::$created]);
        	$this->SetCreatedBy($row[FicheSuiviFields::$createdBy]);
        	$this->SetUpdated($row[FicheSuiviFields::$updated]);
        	$this->SetUpdatedBy($row[FicheSuiviFields::$updatedBy]);
    	}
    }
    
    function init($ficheSuiviId, $modeSuiviId, $etablissementId, $intentionSuiviId, $timingSuiviId, 
    				$commentaire, $dateRencontre, $created, $createdBy, $updated, $updatedBy) 
     
    {
        $this->SetFicheSuiviId($ficheSuiviId);
        $this->SetModeSuiviId($modeSuiviId);
        $this->SetEtablissementId($etablissementId);
        $this->SetIntentionSuiviId($intentionSuiviId);
        $this->SetTimingSuiviId($timingSuiviId);
        $this->SetCommentaire($commentaire);
        $this->SetDateRencontre($dateRencontre);
        $this->SetCreated($created);
        $this->SetCreatedBy($createdBy);
        $this->SetUpdated($updated);
        $this->SetUpdatedBy($updatedBy);
    
    }
    
}

class FicheSuivis extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new FicheSuivi($row));
    }
}
?>