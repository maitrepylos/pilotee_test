<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class FicheSuiviContactFields
{
    public static $contactId = 'contactId';
    public static $ficheSuiviId = 'ficheSuiviId';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class FicheSuiviContact
{
    private $contactId;
    public function GetContactId() { return $this->contactId; }
    public function SetContactId($value) { $this->contactId = $value; }

    private $ficheSuiviId;
    public function GetFicheSuiviId() { return $this->ficheSuiviId; }
    public function SetFicheSuiviId($value) { $this->ficheSuiviId = $value; }

    private $created;
    public function GetCreated() { return $this->created; }
    public function SetCreated($value) { $this->created = $value; }

    private $createdBy;
    public function GetCreatedBy() { return $this->createdBy; }
    public function SetCreatedBy($value) { $this->createdBy = $value; }

    private $updated;
    public function GetUpdated() { return $this->updated; }
    public function SetUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function GetUpdatedBy() { return $this->updatedBy; }
    public function SetUpdatedBy($value) { $this->updatedBy = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetContactId($row[FicheSuiviContactFields::$contactId]);
        	$this->SetFicheSuiviId($row[FicheSuiviContactFields::$ficheSuiviId]);
        	$this->SetCreated($row[FicheSuiviContactFields::$created]);
        	$this->SetCreatedBy($row[FicheSuiviContactFields::$createdBy]);
	        $this->SetUpdated($row[FicheSuiviContactFields::$updated]);
        	$this->SetUpdatedBy($row[FicheSuiviContactFields::$updatedBy]);
    	}
    }
    
    function init($contactId, $ficheSuiviId, $created, $createdBy, $updated, $updatedBy)
    {
        $this->SetContactId($contactId);
        $this->SetFicheSuiviId($ficheSuiviId);
        $this->SetCreated($created);
        $this->SetCreatedBy($createdBy);
        $this->SetUpdated($updated);
        $this->SetUpdatedBy($updatedBy);    
    }
    
    
}

class FicheSuiviContacts extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new FicheSuiviContact($row));
    }
}
?>