<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class FiliereDegreNiveauFields
{
    public static $filiereDegreNiveauId = 'filiereDegreNiveauId';
    public static $dic_filiere_filiereId = 'dic_filiere_filiereId';
    public static $degreNiveauId = 'degreNiveauId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class FiliereDegreNiveau
{
    private $filiereDegreNiveauId;
    public function GetFiliereDegreNiveauId() { return $this->filiereDegreNiveauId; }
    public function SetFiliereDegreNiveauId($value) { $this->filiereDegreNiveauId = $value; }
	public function GetId() { return $this->filiereDegreNiveauId; }

    private $dic_filiere_filiereId;
    public function GetFiliereId() { return $this->dic_filiere_filiereId; }
    public function SetFiliereId($value) { $this->dic_filiere_filiereId = $value; }

    private $degreNiveauId;
    public function GetDegreNiveauId() { return $this->degreNiveauId; }
    public function SetDegreNiveauId($value) { $this->degreNiveauId = $value; }

    private $label;
    public function GetLabel() { return $this->label; }
    public function SetLabel($value) { $this->label = $value; }

    private $ordre;
    public function GetOrdre() { return $this->ordre; }
    public function SetOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function GetSwActif() { return $this->swActif; }
    public function SetSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->SetFiliereDegreNiveauId($row[FiliereDegreNiveauFields::$filiereDegreNiveauId]);
        $this->SetFiliereId($row[FiliereDegreNiveauFields::$dic_filiere_filiereId]);
        $this->SetDegreNiveauId($row[FiliereDegreNiveauFields::$degreNiveauId]);
        $this->SetLabel($row[FiliereDegreNiveauFields::$label]);
        $this->SetOrdre($row[FiliereDegreNiveauFields::$ordre]);
        $this->SetSwActif($row[FiliereDegreNiveauFields::$swActif]);
    }
}

class FiliereDegreNiveaus extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new FiliereDegreNiveau($row));
    }
}
?>