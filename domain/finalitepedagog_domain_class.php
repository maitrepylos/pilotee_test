<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class FinalitePedagogFields
{
    public static $finalitePedagogId = 'finalitePedagogId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class FinalitePedagog
{
    private $finalitePedagogId;
    public function GetFinalitePedagogId() { return $this->finalitePedagogId; }
    public function SetFinalitePedagogId($value) { $this->finalitePedagogId = $value; }

    private $label;
    public function GetLabel() { return $this->label; }
    public function SetLabel($value) { $this->label = $value; }

    private $ordre;
    public function GetOrdre() { return $this->ordre; }
    public function SetOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function GetSwActif() { return $this->swActif; }
    public function SetSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->SetFinalitePedagogId($row[FinalitePedagogFields::$finalitePedagogId]);
        $this->SetLabel($row[FinalitePedagogFields::$label]);
        $this->SetOrdre($row[FinalitePedagogFields::$ordre]);
        $this->SetSwActif($row[FinalitePedagogFields::$swActif]);
    }
}

class FinalitePedagogs extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new FinalitePedagog($row));
    }
}
?>