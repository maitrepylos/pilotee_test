<?php

REQUIRE_ONCE(SCRIPTPATH . 'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'domain/domain_utils_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');


class FormateurFields
{
	public static $formateurId = 'formateurId';
	public static $nom = 'nom';
	public static $prenom = 'prenom';
	public static $agrementASE = 'agrementASE';
	public static $swActif = 'swActif';
	public static $adresseId = 'adresseId';
	public static $website = 'website';
	public static $created = 'created';
	public static $createdBy = 'createdBy';
	public static $updated = 'updated';
	public static $updatedBy = 'updatedBy';
}

class Formateur
{
	private $formateurId;
	private $nom;
	private $prenom;
	private $agrementASE;
	private $swActif;
	private $adresseId;
	private $website;
	private $created;
	private $createdBy;
	private $updated;
	private $updatedBy;

	public function getFormateurId()
	{
		return $this->formateurId;
	}

	public function setFormateurId($value)
	{
		$this->formateurId = $value;
	}

	public function setNom($value)
	{
		$this->nom = $value;
	}

	public function getNom()
	{
		return $this->nom;
	}

	public function setPrenom($value)
	{
		$this->prenom = $value;
	}

	public function getPrenom()
	{
		return $this->prenom;
	}
	public function setAgrementASE($value)
	{
		$this->agrementASE = $value;
	}

	public function getAgrementASE()
	{
		return $this->agrementASE;
	}
	public function setAdresseId($value)
	{
		$this->adresseId = $value;
	}

	//public function getAdresseId()
	//{
	//	return $this->adresseId;
	//} FFI
	public function GetAdresse()
	{
		if($this->GetAdresseId()!=null)
		{
			$db = new AdresseDatabase();
			$db->open();
			$rs = $db->get($this->GetAdresseId());
			$db->close();
			
			$adr = new Adresses($rs);
				
			return $adr->items(0);
		}		
	}
	
	public function setWebsite($value)
	{
		$this->website = $value;
	}

	public function getWebsite()
	{
		return $this->website;
	}

	public function setCreated($value)
	{
		$this->created = $value;
	}

	public function getCreated()
	{
		return $this->created;
	}
	public function setCreatedBy($value)
	{
		$this->createdBy = $value;
	}

	public function getCreatedBy()
	{
		return $this->createdBy;
	}

	public function setUpdated($value)
	{
		$this->updated = $value;
	}

	public function getUpdated()
	{
		return $this->updated;
	}

	public function setUpdatedBy($value)
	{
		$this->updatedBy = $value;
	}

	public function getUpdatedBy()
	{
		return $this->updatedBy;
	}

	public function getSwActif()
	{
		return $this->swActif;
	}

	public function setSwActif($value)
	{
		$this->swActif = $value;
	}
	
	/**
	 * Pour la cr�ation des listes de s�lection
	 */
	public function getId()
	{
		return $this->getFormateurId();
	}

	/**
	 * Pour la cr�ation des listes de s�lection
	 */
	public function getLabel()
	{
		return $this->getNom() . ' ' . $this->getPrenom();
	}

	//public function __construct($row = array())
	//{
	//	DomainUtils::mapFromFieldsToDomainObject($row, $this);
	//} FFI
	
	function __construct($row = null) //FFI
    {
        $this->setFormateurId($row[FormateurFields::$formateurId]);
        $this->SetNom($row[FormateurFields::$nom]);
        $this->SetPrenom($row[FormateurFields::$prenom]);
        $this->SetAgrementASE($row[FormateurFields::$agrementASE]);
        $this->SetSwActif($row[FormateurFields::$swActif]);
        $this->SetAdresseId($row[FormateurFields::$adresseId]);
        $this->SetWebsite($row[FormateurFields::$website]);
        $this->SetCreated($row[FormateurFields::$created]);
        $this->SetCreatedBy($row[FormateurFields::$createdBy]);
        $this->SetUpdated($row[FormateurFields::$updated]);
        $this->SetUpdatedBy($row[FormateurFields::$updatedBy]);
    }
    
    function init($formateurId, $nom, $prenom, $agrementASE, $swActif, $adresseId, $website,
    			 	$created, $createdBy, $updated, $updatedBy) //FFI
    {
        $this->SetOperateurId($operateurId);
        $this->SetNom($nom);
        $this->SetPrenom($prenom);
        $this->SetAgrementASE($agrementASE);
        $this->SetSwActif($swActif);
        $this->SetAdresseId($adresseId);
        $this->SetWebsite($website);
        $this->SetCreated($created);
        $this->SetCreatedBy($createdBy);
        $this->SetUpdated($updated);
        $this->SetUpdatedBy($updatedBy);
    }
	
}

class Formateurs extends DomainBase
{
	function __construct($rs) // FFI
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Formateur($row));
    }
}
