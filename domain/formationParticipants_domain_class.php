<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/etablissement_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/contact_etablissement_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/age_database_model_class.php');

class FormationParticipantsFields
{
    public static $participantId = 'participantId';
    public static $nom = 'nom';
    public static $prenom = 'prenom';
    public static $tel1 = 'tel1';
    public static $tel2 = 'tel2';
    public static $email1 = 'email1';
    public static $email2 = 'email2';
    public static $genreId = 'genreId';
    public static $ageId = 'ageId';
    public static $etablissementOrigineId = 'etablissementOrigineId';
    public static $formationId = 'formationId';
}

class FormationParticipants
{
    public $id;
	public $nom;
	public $prenom;
	public $telephone1;
	public $telephone2;
	public $email1;
	public $email2;
	public $genre;
	public $age;
	public $etablissement_id;
	public $formation_id;
	
	

    function __construct($row = null)
    {
    	if(isset($row))
    	{
	        $this->id = $row[FormationParticipantsFields::$participantId];
	        $this->nom = $row[FormationParticipantsFields::$nom];
	        $this->prenom = $row[FormationParticipantsFields::$prenom];
	        $this->telephone1 = $row[FormationParticipantsFields::$tel1];
	        $this->telephone2 = $row[FormationParticipantsFields::$tel2];
	        $this->age = $row[FormationParticipantsFields::$ageId];
	        $this->email1 = $row[FormationParticipantsFields::$email1];
	        $this->email2 = $row[FormationParticipantsFields::$email2];
	        $this->genre = $row[FormationParticipantsFields::$genreId];
	        $this->etablissement_id = $row[FormationParticipantsFields::$etablissementOrigineId];
	        $this->formation_id = $row[FormationParticipantsFields::$formationId];
    	}
    }
    
    function getGenreName() {
    	if ($this->genre == 1) return "M";
    	elseif ($this->genre == 2) return "F";
    	elseif ($this->genre == "M" || $this->genre == "F") return $this->genre;
    	else return "";
    	
    }
    
	function getEtablissementName() 
	{
		// $db = new database();
		
		// return $db->getEtablissementForId($this->etablissement_id);
		
		// TODO : a definir
		
		$db = new EtablissementDatabase();
		$db->open();
		$rs = $db->GetById($this->etablissement_id);
		$db->close();
		
		$ets = new Etablissements($rs);
		
		if ($ets->count() > 0) {
			return $ets->items(0);			
		} else return false;
		
		
		
	}

	function getAgeName() 
	{
		// $db = new database();
		
		// return $db->getEtablissementForId($this->etablissement_id);
		
		// TODO : a definir
		
		if ($this->age == null) return false;
		
		$db = new AgeDatabase();
		$db->open();
		$rs = $db->get($this->age);
		$db->close();
		
		$ets = new Ages($rs);
		
		if ($ets->count() > 0) {
			return $ets->items(0)->getLabel();;			
		} else return false;
		
		
		
	}
	
	public function GetSpecialite($actionId) {
		$db = new ContactEtablissementDatabase();
		$db->open();
		
		$rs = $db->getSpecialite($actionId, $this->id);
		$db->close();
		
		if ($rs && mysqli_num_rows($rs) > 0) {
			$row = mysqli_fetch_row($rs);
			return $row[0];			
		} else return null;
		
		
	}


    
}

class FormationParticipantss extends DomainBase
{
    function __construct($rs = null)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new FormationParticipants($row));
    }
    
    function addParticipant($participant) {
    	array_push($this->elements, $participant);
    	
    }
    
}
?>