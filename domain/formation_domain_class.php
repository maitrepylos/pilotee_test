<?php

REQUIRE_ONCE(SCRIPTPATH . 'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'domain/domain_utils_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'domain/formationParticipants_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'domain/contact_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'domain/actioncontact_domain_class.php');


REQUIRE_ONCE(SCRIPTPATH . 'model/formationParticipants_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'model/action_database_model_class.php');

class FormationFields
{
	public static $formationId = 'actionId';
	public static $statutSubventionId = 'statutSubventionId';
	public static $typeFormationId = 'typeActionId';
	public static $anneeId = 'anneeId';
	public static $statutFormationId = 'statutActionId';
	public static $formateurId = 'operateurId';
	public static $dateFormation = 'dateAction';
	public static $intitule = 'nomSpec'; //FFI ???
	public static $commentaire = 'commentaire';
	public static $nbApprenants = 'nbApprenants';
	public static $nbEnseignants = 'nbEnseignants';
	public static $website = 'website';
	public static $created = 'created';
	public static $createdBy = 'createdBy';
	public static $updated = 'updated';
	public static $updatedBy = 'updatedBy';
	public static $global = 'swGlobal';
	
}

class Formation
{
	protected $formationId;

	public function setFormationId($value)
	{
		$this->formationId = $value;
	}

	public function getFormationId()
	{
		return $this->formationId;
	}

	protected $statutSubventionId;

	public function setStatutSubventionId($value)
	{
		$this->statutSubventionId = $value;
	}

	public function getStatutSubventionId()
	{
		return $this->statutSubventionId;
	}

	protected $typeFormationId;

	public function settypeFormationId($value)
	{
		$this->typeFormationId = $value;
	}

	public function getTypeFormationId()
	{
		return $this->typeFormationId;
	}

	protected $anneeId;

	public function setAnneeId($value)
	{
		$this->anneeId = $value;
	}

	public function getAnneeId()
	{
		return $this->anneeId;
	}

	protected $statutFormationId;

	public function setStatutFormationId($value)
	{
		$this->statutFormationId = $value;
	}

	public function getStatutFormationId()
	{
		return $this->statutFormationId;
	}

	public function getStatutFormation()
	{
		if($this->statutFormationId == 'VALID')
			return 'Publi� ';
		else
			return 'Brouillon';
	}

	public function isValid()
	{
		if($this->statutFormationId == 'VALID')
			return true;
		else
			return false;
	}

//Marucci Nicolas 06/08/2014 un utilisateur ne peut pas modifier une action si elle n'est pas dans son ann�e d'encodage
	public function checkAnneeEncodage(){

		REQUIRE_ONCE(SCRIPTPATH.'model/annee_database_model_class.php');
		$DB_ANNEE = new AnneeDatabase();
		$annees = new Annees($DB_ANNEE->get($this->anneeId));
		$annee  = $annees->items(0);
		if($annee){
			$now    = date('Y-m-d h:i:s');
			if($now >= $annee -> getDateDeb() && $now <= $annee -> getDateFin()) return true;
			else return false;
		}
		else return false;
	}

	protected $formateurId;

	public function setFormateurId($value)
	{
		$this->formateurId = $value;
	}

	public function getFormateurId()
	{
		return $this->formateurId;
	}
	
	protected $formateursAssocies;
	
	public function getFormateursAssocies() {
		return $this->formateursAssocies;
	}
	
	public function setFormateursAssocies($formateurs) {
		$this->formateursAssocies = $formateurs;
	}

	protected $dateFormation;

	public function setDateFormation($value)
	{
		$this->dateFormation = $value;
	}

	public function getDateFormation()
	{
		return $this->dateFormation;
	}

	protected $intitule;

	public function setIntitule($value)
	{
		$this->intitule = $value;
	}

	public function getIntitule()
	{
		return $this->intitule;
	}

	protected $commentaire;

	public function setCommentaire($value)
	{
		$this->commentaire = $value;
	}

	public function getCommentaire()
	{
		return $this->commentaire;
	}

	protected $nbApprenants;


	public function setNbApprenants($value)
	{
		$this->nbApprenants = $value;
	}

	public function getNbApprenants()
	{
		return $this->nbApprenants;
	}

	protected $nbEnseignants;

	public function setNbEnseignants($value)
	{
		$this->nbEnseignants = $value;
	}

	public function getNbEnseignants()
	{
		return $this->nbEnseignants;
	}

	protected $website;

	public function setWebsite($value)
	{
		$this->website = $value;
	}

	public function getWebsite()
	{
		return $this->website;
	}

	protected $created;

	public function setCreated($value)
	{
		$this->created = $value;
	}

	public function getCreated()
	{
		return $this->created;
	}

	protected $createdBy;

	public function setCreatedBy($value)
	{
		$this->createdBy = $value;
	}

	public function getCreatedBy()
	{
		return $this->createdBy;
	}

	protected $updated;

	public function setUpdated($value)
	{
		$this->updated = $value;
	}

	public function getUpdated()
	{
		return $this->updated;
	}

	protected $updatedBy;

	public function setUpdatedBy($value)
	{
		$this->updatedBy = $value;
	}

	public function getUpdatedBy()
	{
		return $this->updatedBy;
	}
	
	protected $global;

	public function setGlobal($value)
	{
		$this->global = $value;
	}

	public function getGlobal()
	{
		return $this->global;
	}
	

	public function __construct($row = array())
	{
		// DomainUtils::mapFromFieldsToDomainObject($row, $this);
		if(isset($row))
    	{
        	$this->setFormationId($row[FormationFields::$formationId]);
        	$this->setStatutSubventionId($row[FormationFields::$statutSubventionId]);
        	$this->settypeFormationId($row[FormationFields::$typeFormationId]);
        	$this->setAnneeId($row[FormationFields::$anneeId]);
        	$this->setStatutFormationId($row[FormationFields::$statutFormationId]);
        	$this->setFormateurId($row[FormationFields::$formateurId]);
        	$this->setDateFormation($row[FormationFields::$dateFormation]);
        	$this->setIntitule($row[FormationFields::$intitule]); // FFI ???
	        $this->setCommentaire($row[FormationFields::$commentaire]);
    	    $this->setNbApprenants($row[FormationFields::$nbApprenants]);
	       	$this->setNbEnseignants($row[FormationFields::$nbEnseignants]);
    	    $this->setWebsite($row[FormationFields::$website]);
	        $this->setCreated($row[FormationFields::$created]);
        	$this->setCreatedBy($row[FormationFields::$createdBy]);
        	$this->setUpdated($row[FormationFields::$updated]);
        	$this->setUpdatedBy($row[FormationFields::$updatedBy]);
        	$this->setGlobal($row[FormationFields::$global]);
        		
    	}
	}

	public function getDateFormationString($parenthese = true)
	{
		if($this->getDateFormation()!=null)
		{
			if($parenthese)
			{
				return '('.FormatDate($this->getDateFormation()).')';
			}
			else
			{
				return FormatDate($this->getDateFormation());
			}
		}
		return '';
	}
	
 	public function GetFinalitePedagog()
    {
		$db = new ActionDatabase();
		$db->open();
		$rs = $db->getActionFinalitePedagog($this->getFormationId());
		$db->close();
		$actionPeda = new ActionFinalitePedagogs($rs);
		
		return $actionPeda;
    }
    
    public function getFiliereDegreNiveau()
    {
		$db = new ActionDatabase();
		$db->open();
		$rs = $db->getActionFiliereDegreNiveau($this->getFormationId());
		$db->close();
		$fileres = new ActionFiliereDegreNiveaus($rs);
		
		return $fileres;
    }
    
    

    private $formateur;
    
	public function getFormateur() 
	{ 
		if (empty($this->formateur))
		{
			REQUIRE_ONCE(SCRIPTPATH . 'domain/formateur_domain_class.php');
			REQUIRE_ONCE(SCRIPTPATH . 'model/formateur_database_model_class.php');
			$db = new FormateurDatabase();
			$db->open();
			$rs = $db->get($this->getFormateurId());
			$this->formateur = new Formateurs($rs);
			$this->formateur = $this->formateur->items(0);
		}
		
		return $this->formateur;
	}
	
	public function getParticipants() {
		// FFI : BIG BIG Change septembre 2012 ; participants deviennent des actions contacts
		/*$db = new FormationParticipantsDatabase();
		$db->open();
		$rs = $db->getParticipants($this->formationId);
		$db->close();
		
		$ets = new FormationParticipantss($rs);
		
		return $ets;*/
		
	
    	REQUIRE_ONCE(SCRIPTPATH . 'domain/actioncontact_domain_class.php');
    	$db = new ActionDatabase();
		$db->open();
		$rs = $db->getActionContact($this->formationId, "PARTI");
		$db->close();
		$actionContacts =  new ActionContacts($rs);
		
		$contacts = array();
		
		if (is_object($actionContacts)) {
		
			for ($i = 0; $i < $actionContacts->count(); $i++) {
				$contacts[] = $actionContacts->items($i)->GetContact();
			}
			
		}
		
		$participants = new FormationParticipantss();
		
		foreach ($contacts as $contact) {			
			$adresse = $contact->GetAdresse();
			$participant = new FormationParticipants();
			$participant->id = $contact->GetContactId();
			$participant->nom = $contact->GetNom();
			$participant->prenom = $contact->GetPrenom();
			$participant->telephone1 = ($adresse ? $adresse->GetTel1() : "");
			$participant->telephone2 = ($adresse ? $adresse->GetTel2() : "");
			$participant->email1 = ($adresse ? $adresse->GetEmail1() : "");
			$participant->email2 = ($adresse ? $adresse->GetEmail2() : "");
			$participant->genre = $contact->getGenreId();
			$participant->age = $contact->getAgeId();
			// $participant->etablissement_id = $contact->
			
			$participant->formation_id = $this->formationId;
			
			/*echo "<br>debug FFI<br>";
			echo var_dump($participant);
			echo "<br>end debug FFI<br>";
			*/
		
		
			
			$participants->addParticipant($participant);
			
		}
		
		// TODO : attention, on retourne d�sromais des Contacts et plus des Formations Participants...  !!!!!!!!!!!
    	return $participants;
			
		
	}
    
	private $actionEtablissements;
	
 	public function getActionEtablissements($utilisateurId = null)
    {
    	if (empty($this->actionEtablissements)) {
    		REQUIRE_ONCE(SCRIPTPATH . 'domain/actionetablissement_domain_class.php');
	    	$db = new ActionDatabase();
			$db->open();
			$rs = $db->getActionEtablissement($this->formationId, $utilisateurId);
			$db->close();
			$this->actionEtablissements = new Etablissements($rs);			
    	}
    	return $this->actionEtablissements;
    }
    
    private $actionContacts;
    
    public function getActionContacts()
    {
    	if (empty($this->actionContacts)) {
    		REQUIRE_ONCE(SCRIPTPATH . 'domain/actioncontact_domain_class.php');
    		$db = new ActionDatabase();
			$db->open();
			$rs = $db->getActionContact($this->formationId);
			$db->close();
			$this->actionContacts =  new ActionContacts($rs);
    	}
    	
    	return $this->actionContacts;
		
    }
}

class Formations extends DomainBase
{
	function __construct($rs = null)
	{
		parent::__construct();

		if ($rs && mysqli_num_rows($rs) > 0)
		{
			while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Formation($row));
		}
	}
}
