<?php

REQUIRE_ONCE(SCRIPTPATH . 'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH . 'domain/domain_utils_class.php');

class FormationTypeFields
{
	public static $typeFormationId = 'typeFormationId';
	public static $label = 'label';
	public static $ordre = 'ordre';
	public static $swActif = 'swActif';
}

class FormationType
{
	private $typeFormationId;
	private $ordre;
	private $swActif;
	private $label;
	
	public function getId() {
		return $this->getTypeFormationId();
	}
	
	public function getTypeFormationId()
	{
		return $this->typeFormationId;
	}
	
	public function setTypeFormationId($value)
	{
		$this->typeFormationId = $value;
	}

	public function getOrdre()
	{
		return $this->ordre;
	}
	
	public function setOrdre($value)
	{
		$this->ordre = $value;
	}

	public function getSwActif()
	{
		return $this->swActif;
	}

	public function setSwActif($value)
	{
		$this->swActif = $value;
	}

	public function getLabel()
	{
		return $this->label;
	}

	public function setLabel($value)
	{
		$this->label = $value;
	}

	public function __construct($row = array())
	{
		DomainUtils::mapFromFieldsToDomainObject($row, $this);
	}
}

class FormationTypes extends DomainBase
{
}
