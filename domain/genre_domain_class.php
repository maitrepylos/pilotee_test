<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class GenreFields
{
    public static $genreId = 'genreId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class Genre
{
    private $genreId;
    public function getGenreId() { return $this->genreId; }
    public function setGenreId($value) { $this->genreId = $value; }
	public function getId() { return $this->getGenreId(); }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->setGenreId($row[GenreFields::$genreId]);
        $this->setLabel($row[GenreFields::$label]);
        $this->setOrdre($row[GenreFields::$ordre]);
        $this->setSwActif($row[GenreFields::$swActif]);
    }
}

class Genres extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Genre($row));
    }
}
?>