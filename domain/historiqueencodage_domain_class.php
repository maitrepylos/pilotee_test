<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');

class HistoriqueEncodageFields
{
    public static $historiqueEncodageId = 'historiqueEncodageId';
    public static $typeEncodageId = 'typeEncodageId';
    public static $typeObjetEncodeId = 'typeObjetEncodeId';
    public static $utilisateurId = 'utilisateurId';
    public static $date = 'date';
    public static $contactId = 'contactId';
    public static $ficheSuiviId = 'ficheSuiviId';
    public static $remiseMaterielId = 'remiseMaterielId';
    public static $actionId = 'actionId';
}

class HistoriqueEncodage
{
    private $historiqueEncodageId;
    public function GetHistoriqueEncodageId() { return $this->historiqueEncodageId; }
    public function SetHistoriqueEncodageId($value) { $this->historiqueEncodageId = $value; }

    private $typeEncodageId;
    public function GetTypeEncodageId() { return $this->typeEncodageId; }
    public function SetTypeEncodageId($value) { $this->typeEncodageId = $value; }
	public function GetTypeEncodageLabel() 
	{ 
		return Dictionnaries::getTypeEncodageList($this->GetTypeEncodageId())->items(0)->getLabel(); 
	}

    private $typeObjetEncodeId;
    public function GetTypeObjetEncodeId() { return $this->typeObjetEncodeId; }
    public function SetTypeObjetEncodeId($value) { $this->typeObjetEncodeId = $value; }

    private $utilisateurId;
    public function GetUtilisateurId() { return $this->utilisateurId; }
    public function SetUtilisateurId($value) { $this->utilisateurId = $value; }

    private $user;
    public function GetUtilisateur()
	{
		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get($this->GetUtilisateurId(), null);
		$db->close();
		$users = new Utilisateurs($rs);
		$this->user = $users->items(0);
		return $this->user;
	}
    
    private $date;
    public function GetDate() { return $this->date; }
    public function SetDate($value) { $this->date = $value; }
    public function GetDateHistorique($formated = null)
    {
    	if (!isset($formated)) return $this->date;
    	else return FormatDate($this->date);
    }
    

    private $contactId;
    public function GetContactId() { return $this->contactId; }
    public function SetContactId($value) { $this->contactId = $value; }

    private $ficheSuiviId;
    public function GetFicheSuiviId() { return $this->ficheSuiviId; }
    public function SetFicheSuiviId($value) { $this->ficheSuiviId = $value; }

    private $remiseMaterielId;
    public function GetRemiseMaterielId() { return $this->remiseMaterielId; }
    public function SetRemiseMaterielId($value) { $this->remiseMaterielId = $value; }

    private $actionId;
    public function GetActionId() { return $this->actionId; }
    public function SetActionId($value) { $this->actionId = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{
	        $this->SetHistoriqueEncodageId($row[HistoriqueEncodageFields::$historiqueEncodageId]);
	        $this->SetTypeEncodageId($row[HistoriqueEncodageFields::$typeEncodageId]);
	        $this->SetTypeObjetEncodeId($row[HistoriqueEncodageFields::$typeObjetEncodeId]);
	        $this->SetUtilisateurId($row[HistoriqueEncodageFields::$utilisateurId]);
	        $this->SetDate($row[HistoriqueEncodageFields::$date]);
	        $this->SetContactId($row[HistoriqueEncodageFields::$contactId]);
	        $this->SetFicheSuiviId($row[HistoriqueEncodageFields::$ficheSuiviId]);
	        $this->SetRemiseMaterielId($row[HistoriqueEncodageFields::$remiseMaterielId]);
	        $this->SetActionId($row[HistoriqueEncodageFields::$actionId]);
    	}
    }
    
    function init(
    			$historiqueEncodageId, $typeEncodageId, $typeObjetEncodeId, $utilisateurId,
    			$date, $contactId, $ficheSuiviId, $remiseMaterielId, $actionId
    			)
    {
        $this->SetHistoriqueEncodageId($historiqueEncodageId);
        $this->SetTypeEncodageId($typeEncodageId);
        $this->SetTypeObjetEncodeId($typeObjetEncodeId);
        $this->SetUtilisateurId($utilisateurId);
        $this->SetDate($date);
        $this->SetContactId($contactId);
        $this->SetFicheSuiviId($ficheSuiviId);
        $this->SetRemiseMaterielId($remiseMaterielId);
        $this->SetActionId($actionId);
    }
}

class HistoriqueEncodages extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new HistoriqueEncodage($row));
    }
}
?>