<?php

interface IListable
{
	public function getListId();
	public function getListLabel();
}

?>