<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class IntentionSuiviFields
{
    public static $intentionSuiviId = 'intentionSuiviId';
    public static $Label = 'Label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class IntentionSuivi
{
    private $intentionSuiviId;
    public function GetIntentionSuiviId() { return $this->intentionSuiviId; }
    public function SetIntentionSuiviId($value) { $this->intentionSuiviId = $value; }
    public function GetId() { return $this->GetIntentionSuiviId(); }

    private $Label;
    public function GetLabel() { return $this->Label; }
    public function SetLabel($value) { $this->Label = $value; }

    private $ordre;
    public function GetOrdre() { return $this->ordre; }
    public function SetOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function GetSwActif() { return $this->swActif; }
    public function SetSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->SetIntentionSuiviId($row[IntentionSuiviFields::$intentionSuiviId]);
        $this->SetLabel($row[IntentionSuiviFields::$Label]);
        $this->SetOrdre($row[IntentionSuiviFields::$ordre]);
        $this->SetSwActif($row[IntentionSuiviFields::$swActif]);
    }
}

class IntentionSuivis extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new IntentionSuivi($row));
    }
}
?>