<?php

REQUIRE_ONCE(SCRIPTPATH.'domain/ilistable.domain.interface.php');

class ListElement implements IListable
{
	public $id;
	public $label;
	
	public function __construct($id, $label)
	{
		$this->id = $id;
		$this->label = $label;
	}
	
	public function getListId()
	{
		return $this->id;
	}
	
	public function getListLabel()
	{
		return $this->label;
	}
}

?>