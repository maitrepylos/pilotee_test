<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class MaterielPedagFields
{
    public static $materielId = 'materielId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class MaterielPedag
{
    private $materielId;
    public function GetMaterielId() { return $this->materielId; }
    public function SetMaterielId($value) { $this->materielId = $value; }
	public function GetId() { return $this->GetMaterielId(); }

    private $label;
    public function GetLabel() { return $this->label; }
    public function SetLabel($value) { $this->label = $value; }

    private $ordre;
    public function GetOrdre() { return $this->ordre; }
    public function SetOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function GetSwActif() { return $this->swActif; }
    public function SetSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->SetMaterielId($row[MaterielPedagFields::$materielId]);
        $this->SetLabel($row[MaterielPedagFields::$label]);
        $this->SetOrdre($row[MaterielPedagFields::$ordre]);
        $this->SetSwActif($row[MaterielPedagFields::$swActif]);
    }
}

class MaterielPedags extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new MaterielPedag($row));
    }
}
?>