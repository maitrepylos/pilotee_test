<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class ModeSuiviFields
{
    public static $modeSuiviId = 'modeSuiviId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class ModeSuivi
{
    private $modeSuiviId;
    public function GetModeSuiviId() { return $this->modeSuiviId; }
    public function SetModeSuiviId($value) { $this->modeSuiviId = $value; }
    public function GetId() { return $this->GetModeSuiviId(); }

    private $label;
    public function GetLabel() { return $this->label; }
    public function SetLabel($value) { $this->label = $value; }

    private $ordre;
    public function GetOrdre() { return $this->ordre; }
    public function SetOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function GetSwActif() { return $this->swActif; }
    public function SetSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->SetModeSuiviId($row[modeSuiviFields::$modeSuiviId]);
        $this->SetLabel($row[ModeSuiviFields::$label]);
        $this->SetOrdre($row[ModeSuiviFields::$ordre]);
        $this->SetSwActif($row[ModeSuiviFields::$swActif]);
    }
}

class ModeSuivis extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new ModeSuivi($row));
    }
}
?>