<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/adresse_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/operateur_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');

class OperateurFields
{
    public static $operateurId = 'operateurId';
    public static $nom = 'nom';
    public static $agrementASE = 'agrementASE';
    public static $swActif = 'swActif';
    public static $adresseId = 'adresseId';
    public static $website = 'website';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class Operateur
{
    private $operateurId;
    public function GetOperateurId() { return $this->operateurId; }
    public function SetOperateurId($value) { $this->operateurId = $value; }
	public function getId() { return $this->GetOperateurId(); }
	
	public function GetUtilisateur()
	{
		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->getWithOperateurId($this->GetOperateurId());
		$db->close();
		$op = new Utilisateurs($rs);
		return $op->items(0);
	}
	
    public function GetActions()
	{
			$db = new OperateurDatabase();
			$db->open();
			$rs = $db->getAction($this->GetOperateurId());
			$db->close();
			
			$actions = new Actions($rs);
				
			return $actions;
	}
    
    private $nom;
    public function GetNom() { return $this->nom; }
    public function SetNom($value) { $this->nom = $value; }
	public function getLabel() { return $this->GetNom(); }
    
    private $agrementASE;
    public function GetAgrementASE() { return $this->agrementASE; }
    public function SetAgrementASE($value) { $this->agrementASE = $value; }

    private $swActif;
    public function GetSwActif() { return $this->swActif; }
    public function GetSwActifLabel() { return $this->GetSwActif() == 1 ? "Actif" : "Inactif"; }
	public function GetSwActifColor() { return $this->GetSwActif() == 1 ? "Green" : "Red"; }
    public function SetSwActif($value) { $this->swActif = $value; }

    private $adresseId;
    public function GetAdresseId() { return $this->adresseId; }
    public function SetAdresseId($value) { $this->adresseId = $value; }
    
    public function GetAdresse()
	{
		if($this->GetAdresseId()!=null)
		{
			$db = new AdresseDatabase();
			$db->open();
			$rs = $db->get($this->GetAdresseId());
			$db->close();
			
			$adr = new Adresses($rs);
				
			return $adr->items(0);
		}		
	}
	
    private $website;
    public function GetWebsite() { return $this->website; }
    public function SetWebsite($value) { $this->website = $value; }
	
	public function GetWebsiteAsAnchor() { return sprintf('<a href="%s" target="none">%s</a>', $this->GetWebsite(), $this->GetWebsite()); }
    
    private $created;
    public function GetCreated() { return $this->created; }
    public function SetCreated($value) { $this->created = $value; }

    private $createdBy;
    public function GetCreatedBy() { return $this->createdBy; }
    public function SetCreatedBy($value) { $this->createdBy = $value; }

    private $updated;
    public function GetUpdated() { return $this->updated; }
    public function SetUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function GetUpdatedBy() { return $this->updatedBy; }
    public function SetUpdatedBy($value) { $this->updatedBy = $value; }

    function __construct($row = null)
    {
        $this->SetOperateurId($row[OperateurFields::$operateurId]);
        $this->SetNom($row[OperateurFields::$nom]);
        $this->SetAgrementASE($row[OperateurFields::$agrementASE]);
        $this->SetSwActif($row[OperateurFields::$swActif]);
        $this->SetAdresseId($row[OperateurFields::$adresseId]);
        $this->SetWebsite($row[OperateurFields::$website]);
        $this->SetCreated($row[OperateurFields::$created]);
        $this->SetCreatedBy($row[OperateurFields::$createdBy]);
        $this->SetUpdated($row[OperateurFields::$updated]);
        $this->SetUpdatedBy($row[OperateurFields::$updatedBy]);
    }
    
    function init($operateurId, $nom, $agrementASE, $swActif, $adresseId, $website,
    			 	$created, $createdBy, $updated, $updatedBy)
    {
        $this->SetOperateurId($operateurId);
        $this->SetNom($nom);
        $this->SetAgrementASE($agrementASE);
        $this->SetSwActif($swActif);
        $this->SetAdresseId($adresseId);
        $this->SetWebsite($website);
        $this->SetCreated($created);
        $this->SetCreatedBy($createdBy);
        $this->SetUpdated($updated);
        $this->SetUpdatedBy($updatedBy);
    }
}

class Operateurs extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Operateur($row));
    }
}
?>