<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class OperateurUtilisateurFields
{
    public static $operateurId = 'operateurId';
    public static $utilisateurId = 'utilisateurId';
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class OperateurUtilisateur
{
    private $operateurId;
    public function GetOperateurId() { return $this->operateurId; }
    public function SetOperateurId($value) { $this->operateurId = $value; }

    private $utilisateurId;
    public function GetUtilisateurId() { return $this->utilisateurId; }
    public function SetUtilisateurId($value) { $this->utilisateurId = $value; }

    private $created;
    public function GetCreated() { return $this->created; }
    public function SetCreated($value) { $this->created = $value; }

    private $createdBy;
    public function GetCreatedBy() { return $this->createdBy; }
    public function SetCreatedBy($value) { $this->createdBy = $value; }

    private $updated;
    public function GetUpdated() { return $this->updated; }
    public function SetUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function GetUpdatedBy() { return $this->updatedBy; }
    public function SetUpdatedBy($value) { $this->updatedBy = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->SetOperateurId($row[OperateurUtilisateurFields::$operateurId]);
        	$this->SetUtilisateurId($row[OperateurUtilisateurFields::$utilisateurId]);
        	$this->SetCreated($row[OperateurUtilisateurFields::$created]);
        	$this->SetCreatedBy($row[OperateurUtilisateurFields::$createdBy]);
        	$this->SetUpdated($row[OperateurUtilisateurFields::$updated]);
        	$this->SetUpdatedBy($row[OperateurUtilisateurFields::$updatedBy]);
    	}
    }
    
    function init($operateurId, $utilisateurId, $created, $createdBy, $updated, $updatedBy)
    {
       	$this->SetOperateurId($operateurId);
       	$this->SetUtilisateurId($utilisateurId);
       	$this->SetCreated($created);
       	$this->SetCreatedBy($createdBy);
       	$this->SetUpdated($updated);
       	$this->SetUpdatedBy($updatedBy);
    }
    
}

class OperateurUtilisateurs extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new OperateurUtilisateur($row));
    }
}
?>