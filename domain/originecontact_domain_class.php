<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class OrigineContactFields
{
    public static $origineContactId = 'origineContactId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class OrigineContact
{
    private $origineContactId;
    public function GetOrigineContactId() { return $this->origineContactId; }
    public function SetOrigineContactId($value) { $this->origineContactId = $value; }

    private $label;
    public function GetLabel() { return $this->label; }
    public function SetLabel($value) { $this->label = $value; }

    private $ordre;
    public function GetOrdre() { return $this->ordre; }
    public function SetOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function GetSwActif() { return $this->swActif; }
    public function SetSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->SetOrigineContactId($row[OrigineContactFields::$origineContactId]);
        $this->SetLabel($row[OrigineContactFields::$label]);
        $this->SetOrdre($row[OrigineContactFields::$ordre]);
        $this->SetSwActif($row[OrigineContactFields::$swActif]);
    }
}

class OrigineContacts extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new OrigineContact($row));
    }
}
?>