<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class ParticipationFields
{
	public static $participationId = "participationId";
	public static $label = "label";
	public static $ordre = "ordre";
	public static $swActif = "swActif";	
}

class Participation
{
	private $id;
	public function setId($value) { $this->id = $value; }
	public function getId() { return $this->id; }
	
	private $label;
	public function setLabel($value) { $this->label = $value; }
	public function getLabel() { return $this->label; }
	
	private $ordre;
	public function setOrdre($value) { $this->ordre = $value; }
	public function getOrdre() { return $this->ordre; }
	
	private $swActif;
	public function setSwActif($value) { $this->swActif = $value; }
	public function getSwActif() { return $this->swActif; }

	function __construct($row = array())
	{
		$this->setId($row[ParticipationFields::$participationId]);
		$this->setLabel($row[ParticipationFields::$label]);
		$this->setOrdre($row[ParticipationFields::$ordre]);
		$this->setSwActif($row[ParticipationFields::$swActif]);
	}
}

class Participations extends DomainBase
{
	function __construct($rs)
	{
		parent::__construct();
		
		if ($rs && mysqli_num_rows($rs) > 0)
		{
			while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Participation($row));
		}
	}
}
?>