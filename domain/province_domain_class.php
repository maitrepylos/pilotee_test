<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class ProvinceFields
{
	public static $dicProvinceId = "dic_provinceId";
	public static $label = "label";
	public static $ordre = "ordre";
	public static $swActif = "swActif";	
}

class Province
{
	private $id;
	public function setId($value) { $this->id = $value; }
	public function getId() { return $this->id; }
	
	private $label;
	public function setLabel($value) { $this->label = $value; }
	public function getLabel() { return $this->label; }
	
	private $ordre;
	public function setOrdre($value) { $this->ordre = $value; }
	public function getOrdre() { return $this->ordre; }
	
	private $swActif;
	public function setSwActif($value) { $this->swActif = $value; }
	public function getSwActif() { return $this->swActif; }

	function __construct($row = array())
	{
		$this->setId($row[ProvinceFields::$dicProvinceId]);
		$this->setLabel($row[ProvinceFields::$label]);
		$this->setOrdre($row[ProvinceFields::$ordre]);
		$this->setSwActif($row[ProvinceFields::$swActif]);
	}
}

class Provinces extends DomainBase
{
	function __construct($rs)
	{
		parent::__construct();
		
		if ($rs && mysqli_num_rows($rs) > 0)
		{
			while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Province($row));
		}
	}
}
?>