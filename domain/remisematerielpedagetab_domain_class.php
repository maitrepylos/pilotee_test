<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

REQUIRE_ONCE(SCRIPTPATH.'model/contact_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/contact_etablissement_database_model_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/remiseMaterielPedagEtab_database_model_class.php');

class RemiseMaterielPedagEtabFields
{
    public static $remiseMaterielId = 'remiseMaterielId';  
    public static $etablissementId = 'etablissementId';
    public static $contactId = 'contactId';
    public static $date = 'date';   
    public static $created = 'created';
    public static $createdBy = 'createdBy';
    public static $updated = 'updated';
    public static $updatedBy = 'updatedBy';
}

class RemiseMaterielPedagEtab
{
    private $remiseMaterielId;
    public function GetRemiseMaterielId() { return $this->remiseMaterielId; }
    public function SetRemiseMaterielId($value) { $this->remiseMaterielId = $value; }



    private $etablissementId;
    public function GetEtablissementId() { return $this->etablissementId; }
    public function SetEtablissementId($value) { $this->etablissementId = $value; }
    
    private $etablissement;
    public function GetEtablissement()
    {
    	if (isset($this->etablissement)) return $this->etablissement->items(0);
    	
		$db = new EtablissementDatabase();
		$db->open();
		$rs = $db->GetById($this->GetEtablissementId());
		$db->close();
			
		$this->etablissement = new Etablissements($rs);
    	
    	return $this->etablissement->items(0);
    } 

    private $contactId;
    public function GetContactId() { return $this->contactId; }
    
    private $contact;
	public function GetContact()
	{
		if (isset($this->contact)) return $this->contact->items(0);
		 
		$db = new ContactDatabase();
		$db->open();
		$rs = $db->get($this->GetContactId());
		$db->close();
			
		$this->contact = new Contacts($rs);
			
		return $this->contact->items(0);
	}
	
	private $contactEtablissement;
	public function getContactEtablissement()
	{
		if (isset($this->contactEtablissement)) return $this->contactEtablissement->items(0); 
		
		$db = new ContactEtablissementDatabase();
		$db->open();
		$rs = $db->get(null, $this->GetContactId(), $this->GetEtablissementId());
		$db->close();
		
		$this->contactEtablissement = new ContactEtablissements($rs);
		
		return $this->contactEtablissement->items(0);
	}
	
    public function SetContactId($value) { $this->contactId = $value; }

    private $date;
    public function GetDate($formated = null)
    {
    	if (!isset($formated)) return $this->date;
    	else return FormatDate($this->date);
    }
    public function SetDate($value) { $this->date = $value; }

    private $created;
    public function GetCreated() { return $this->created; }
    public function SetCreated($value) { $this->created = $value; }

    private $createdBy;
    public function GetCreatedBy() { return $this->createdBy; }
    public function SetCreatedBy($value) { $this->createdBy = $value; }
    
    private $creationString;
    public function getCreationString()
    {
    	if (isset($this->creationString)) return $this->creationString;

		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get($this->GetCreatedBy(), null);
		$db->close();
			
		$users = new Utilisateurs($rs);
    	$date = FormatDate($this->GetCreated());
    	
    	$this->creationString = sprintf('le %s par %s %s', $date, $users->items(0)->getPrenom(), $users->items(0)->getNom());
    	
    	return $this->creationString;
    }

    private $updated;
    public function GetUpdated() { return $this->updated; }
    public function SetUpdated($value) { $this->updated = $value; }

    private $updatedBy;
    public function GetUpdatedBy() { return $this->updatedBy; }
    public function SetUpdatedBy($value) { $this->updatedBy = $value; }

    private $updatedString;
    public function getUpdatedString()
    {
    	if (isset($this->updatedString)) return $this->updatedString;

		$db = new UtilisateurDatabase();
		$db->open();
		$rs = $db->get($this->GetUpdatedBy(), null);
		$db->close();
			
		$users = new Utilisateurs($rs);
    	$date = FormatDate($this->GetUpdated());
    	
    	$this->updatedString = sprintf('le %s par %s %s', $date, $users->items(0)->getPrenom(), $users->items(0)->getNom());
    	
    	return $this->updatedString;
    }
    
    function getMateriaux()
    {
    	if($this->GetRemiseMaterielId())
    	{
    		$db = new RemiseMaterielPedagEtabDatabase();
    		$db->open();
    		$remiseParMat = new RemiseParMateriels($db->getMateriaux($this->GetRemiseMaterielId()));
    		$db->close();
    		return $remiseParMat;
    	}
    	return null;
    }
    
    function __construct($row = null)
    {
    	if(isset($row))
    	{
	        $this->SetRemiseMaterielId($row[RemiseMaterielPedagEtabFields::$remiseMaterielId]);
	        $this->SetEtablissementId($row[RemiseMaterielPedagEtabFields::$etablissementId]);
    	    $this->SetContactId($row[RemiseMaterielPedagEtabFields::$contactId]);
	        $this->SetDate($row[RemiseMaterielPedagEtabFields::$date]);
	        $this->SetCreated($row[RemiseMaterielPedagEtabFields::$created]);
    	    $this->SetCreatedBy($row[RemiseMaterielPedagEtabFields::$createdBy]);
	        $this->SetUpdated($row[RemiseMaterielPedagEtabFields::$updated]);
    	    $this->SetUpdatedBy($row[RemiseMaterielPedagEtabFields::$updatedBy]);
    	}
    }
    
    function init($remiseMaterielId, $etablissementId, $contactId,
    				$date, $created, $createdBy, $updated, $updatedBy)
    {
	    $this->SetRemiseMaterielId($remiseMaterielId);
        //$this->SetMaterielId($materielId);
	    $this->SetEtablissementId($etablissementId);
    	$this->SetContactId($contactId);
	    $this->SetDate($date);
    	//$this->SetQuantite($quantite);
	    $this->SetCreated($created);
    	$this->SetCreatedBy($createdBy);
	    $this->SetUpdated($updated);
    	$this->SetUpdatedBy($updatedBy);
    }
    
}

class RemiseMaterielPedagEtabs extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new RemiseMaterielPedagEtab($row));
    }
}
?>