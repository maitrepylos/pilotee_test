<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class RemiseParMaterielFields
{
    public static $remiseParMaterielId = 'remiseParMaterielId';
    public static $remiseMaterielId = 'remiseMaterielId';
    public static $materielId = 'materielId';
    public static $quantite = 'quantite';
}

class RemiseParMateriel
{
    private $remiseParMaterielId;
    public function getRemiseParMaterielId() { return $this->remiseParMaterielId; }
    public function setRemiseParMaterielId($value) { $this->remiseParMaterielId = $value; }

    private $remiseMaterielId;
    public function getRemiseMaterielId() { return $this->remiseMaterielId; }
    public function setRemiseMaterielId($value) { $this->remiseMaterielId = $value; }

    private $materielId;
    public function getMaterielId() { return $this->materielId; }
    public function setMaterielId($value) { $this->materielId = $value; }

    private $quantite;
    public function getQuantite() { return $this->quantite; }
    public function setQuantite($value) { $this->quantite = $value; }

    function __construct($row = null)
    {
    	if(isset($row))
    	{
        	$this->setRemiseParMaterielId($row[RemiseParMaterielFields::$remiseParMaterielId]);
        	$this->setRemiseMaterielId($row[RemiseParMaterielFields::$remiseMaterielId]);
        	$this->setMaterielId($row[RemiseParMaterielFields::$materielId]);
        	$this->setQuantite($row[RemiseParMaterielFields::$quantite]);
    	}
    }
    
    function init($remiseParMaterielId, $remiseMaterielId, $materielId, $quantite)
    {
     	$this->setRemiseParMaterielId($remiseParMaterielId);
        $this->setRemiseMaterielId($remiseMaterielId);
        $this->setMaterielId($materielId);
        $this->setQuantite($quantite);
    }
}

class RemiseParMateriels extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new RemiseParMateriel($row));
    }
}
?>