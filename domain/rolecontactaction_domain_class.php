<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class RoleContactActionFields
{
    public static $roleContactActionId = 'roleContactActionId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class RoleContactAction
{
    private $roleContactActionId;
    public function getRoleContactActionId() { return $this->roleContactActionId; }
    public function setRoleContactActionId($value) { $this->roleContactActionId = $value; }
	public function getId() { return $this->getRoleContactActionId(); }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->setRoleContactActionId($row[RoleContactActionFields::$roleContactActionId]);
        $this->setLabel($row[RoleContactActionFields::$label]);
        $this->setOrdre($row[RoleContactActionFields::$ordre]);
        $this->setSwActif($row[RoleContactActionFields::$swActif]);
    }
}

class RoleContactActions extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new RoleContactAction($row));
    }
}
?>