<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class SectionActionFields
{
    public static $sectionActionId = 'sectionActionId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class SectionAction
{
    private $sectionActionId;
    public function getSectionActionId() { return $this->sectionActionId; }
    public function setSectionActionId($value) { $this->sectionActionId = $value; }
	public function getId() { return $this->sectionActionId; }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
    
        $this->setSectionActionId($row[SectionActionFields::$sectionActionId]);
        $this->setLabel($row[SectionActionFields::$label]);
        $this->setOrdre($row[SectionActionFields::$ordre]);
        $this->setSwActif($row[SectionActionFields::$swActif]);
    }
}

class SectionActions extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new SectionAction($row));
    }
}
?>