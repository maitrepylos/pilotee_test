<?php

REQUIRE_ONCE(SCRIPTPATH.'lib/sessionbase.domain.class.php');

class Session extends SessionBase
{	
	public function redirect()
	{
		if($this->isLogged())
		{
			if($this->isAdmin())
			{
				Mapping::RedirectTo('accueil');
			}
			elseif($this->isAgent())
			{
				Mapping::RedirectTo('accueil');
			}
			elseif($this->isCoordinateur())
			{
				Mapping::RedirectTo('accueil');
			}
			else//Opérateur
			{
				Mapping::RedirectTo('accueil');
			}
		}
		else
		{
			Mapping::RedirectTo('login');
		}
	}
	
	public function isUserConnected()
	{
		return ($this->get('user_connected') == true);
	}
	
	public function isAdmin()
	{
		return ($this->isUserConnected() && $this->get('user_roles') == USER_TYPE_ADMIN);
	}
	
	public function isAgentCoordinateur()
	{
		return ($this->isUserConnected() && $this->get('user_roles') == USER_TYPE_AGENT_COORDINATEUR);
	}
	
	
	public function isOperateur()
	{
		return ($this->isUserConnected() && $this->get('user_roles') == USER_TYPE_OPERATEUR);
	}
	
	public function isLogged()
	{
		return $this->exist('current_user');
	}
	
	public function isCoordinateur()
	{
		return ($this->isUserConnected() && $this->get('user_roles') == USER_TYPE_COORDINATEUR);
	}
	
	public function isAgent()
	{
		return ($this->isUserConnected() && $this->get('user_roles') == USER_TYPE_AGENT);
	}

	public function disconnect()
	{
		$this->resetCurrentUser();		
	}
	
	public function resetCurrentUser()
	{
		$this->reset('current_user');
	}
	
	public function setCurrentUser($user)
	{
		$this->set('current_user', $user);
	}

	public function getCurrentUser()
	{
		return $this->get('current_user');
	}
	
	public function getCurrentUserId()
	{
		$user = $this->getCurrentUser();
		return $user->getUtilisateurId();
	}
	
	public function getCurrentUserLogin()
	{
		$user = $this->getCurrentUser();
		return $user->getLogin();
	}
	
	public function getCurrentUserType()
	{
		$user = $this->getCurrentUser();
		return $user->getTypeUtilisateurId();
	}
	
	public function getCurrentUserState()
	{
		$user = $this->getCurrentUser();
		return $user->getSwActif();
	}
}

?>