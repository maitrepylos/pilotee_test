<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class StatutFields
{
    public static $statutId = 'statutId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class Statut
{
    private $statutId;
    public function getStatutId() { return $this->statutId; }
    public function setStatutId($value) { $this->statutId = $value; }
	public function getId() { return $this->getStatutId(); }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->setStatutId($row[StatutFields::$statutId]);
        $this->setLabel($row[StatutFields::$label]);
        $this->setOrdre($row[StatutFields::$ordre]);
        $this->setSwActif($row[StatutFields::$swActif]);
    }
}

class Statuts extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Statut($row));
    }
}
?>