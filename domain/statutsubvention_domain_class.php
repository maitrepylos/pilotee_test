<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class StatutSubventionFields
{
    public static $statutSubventionId = 'statutSubventionId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class StatutSubvention
{
    private $statutSubventionId;
    public function getStatutSubventionId() { return $this->statutSubventionId; }
	public function getId() { return $this->getStatutSubventionId(); }
    public function setStatutSubventionId($value) { $this->statutSubventionId = $value; }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->setStatutSubventionId($row[StatutSubventionFields::$statutSubventionId]);
        $this->setLabel($row[StatutSubventionFields::$label]);
        $this->setOrdre($row[StatutSubventionFields::$ordre]);
        $this->setSwActif($row[StatutSubventionFields::$swActif]);
    }
}

class StatutSubventions extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new StatutSubvention($row));
    }
}
?>