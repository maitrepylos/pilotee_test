<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class SyntheseAvisFields
{
    public static $syntheseAvisId = 'syntheseAvisId';
    public static $label = 'label';
    public static $swActif = 'swActif';
    public static $ordre = 'ordre';
}

class SyntheseAvis
{
    private $syntheseAvisId;
    public function getSyntheseAvisId() { return $this->syntheseAvisId; }
	public function getId() { return $this->getSyntheseAvisId(); }
    public function setSyntheseAvisId($value) { $this->syntheseAvisId = $value; }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    function __construct($row = array())
    {
        $this->setSyntheseAvisId($row[SyntheseAvisFields::$syntheseAvisId]);
        $this->setLabel($row[SyntheseAvisFields::$label]);
        $this->setSwActif($row[SyntheseAvisFields::$swActif]);
        $this->setOrdre($row[SyntheseAvisFields::$ordre]);
    }
}

class SyntheseAviss extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new SyntheseAvis($row));
    }
}
?>