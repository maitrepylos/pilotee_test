<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class TimingSuiviFields
{
    public static $timingSuiviId = 'timingSuiviId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class TimingSuivi
{
    private $timingSuiviId;
    public function GetTimingSuiviId() { return $this->timingSuiviId; }
    public function SetTimingSuiviId($value) { $this->timingSuiviId = $value; }
	public function GetId() { return $this->GetTimingSuiviId(); }
    private $label;
    public function GetLabel() { return $this->label; }
    public function SetLabel($value) { $this->label = $value; }

    private $ordre;
    public function GetOrdre() { return $this->ordre; }
    public function SetOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function GetSwActif() { return $this->swActif; }
    public function SetSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->SetTimingSuiviId($row[TimingSuiviFields::$timingSuiviId]);
        $this->SetLabel($row[TimingSuiviFields::$label]);
        $this->SetOrdre($row[TimingSuiviFields::$ordre]);
        $this->SetSwActif($row[TimingSuiviFields::$swActif]);
    }
}

class TimingSuivis extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new TimingSuivi($row));
    }
}
?>