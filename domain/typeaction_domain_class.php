<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class TypeActionFields
{
    public static $typeActionId = 'typeActionId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class TypeAction
{
    private $typeActionId;
    public function getTypeActionId() { return $this->typeActionId; }
	public function getId() { return $this->typeActionId; }
    public function setTypeActionId($value) { $this->typeActionId = $value; }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->setTypeActionId($row[TypeActionFields::$typeActionId]);
        $this->setLabel($row[TypeActionFields::$label]);
        $this->setOrdre($row[TypeActionFields::$ordre]);
        $this->setSwActif($row[TypeActionFields::$swActif]);
    }
}

class TypeActions extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new TypeAction($row));
    }
}
?>