<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class TypeEncodageFields
{
    public static $typeEncodageId = 'typeEncodageId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class TypeEncodage
{
    private $typeEncodageId;
    public function getTypeEncodageId() { return $this->typeEncodageId; }
    public function setTypeEncodageId($value) { $this->typeEncodageId = $value; }
	public function getId() { return $this->getTypeEncodageId(); }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->setTypeEncodageId($row[TypeEncodageFields::$typeEncodageId]);
        $this->setLabel($row[TypeEncodageFields::$label]);
        $this->setOrdre($row[TypeEncodageFields::$ordre]);
        $this->setSwActif($row[TypeEncodageFields::$swActif]);
    }
}

class TypeEncodages extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new TypeEncodage($row));
    }
}
?>