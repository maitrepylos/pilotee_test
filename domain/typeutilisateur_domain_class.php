<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class TypeUtilisateurFields
{
    public static $typeUtilisateurId = 'typeUtilisateurId';
    public static $label = 'label';
    public static $ordre = 'ordre';
    public static $swActif = 'swActif';
}

class TypeUtilisateur
{
    private $typeUtilisateurId;
    public function getTypeUtilisateurId() { return $this->typeUtilisateurId; }
    public function setTypeUtilisateurId($value) { $this->typeUtilisateurId = $value; }
	public function getId() { return $this->getTypeUtilisateurId(); }

    private $label;
    public function getLabel() { return $this->label; }
    public function setLabel($value) { $this->label = $value; }

    private $ordre;
    public function getOrdre() { return $this->ordre; }
    public function setOrdre($value) { $this->ordre = $value; }

    private $swActif;
    public function getSwActif() { return $this->swActif; }
    public function setSwActif($value) { $this->swActif = $value; }

    function __construct($row = array())
    {
        $this->setTypeUtilisateurId($row[TypeUtilisateurFields::$typeUtilisateurId]);
        $this->setLabel($row[TypeUtilisateurFields::$label]);
        $this->setOrdre($row[TypeUtilisateurFields::$ordre]);
        $this->setSwActif($row[TypeUtilisateurFields::$swActif]);
    }
}

class TypeUtilisateurs extends DomainBase
{
    function __construct($rs)
    {
        parent::__construct();

        if ($rs && mysqli_num_rows($rs) > 0)
            while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new TypeUtilisateur($row));
    }
}
?>