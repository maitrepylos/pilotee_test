<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/etablissement_database_model_class.php');

class UtilisateurFields
{
	public static $utilisateurId = 'utilisateurId';
	public static $typeUtilisateurId = 'typeUtilisateurId';
	public static $swActif = 'swActif';		
	public static $login = 'login';
	public static $motDePasse = 'motDePasse';
	public static $nom = 'nom';
	public static $prenom = 'prenom';
	public static $tel = 'tel';
	public static $created = 'created';
	public static $createdBy = 'createdBy';
	public static $updated = 'updated';
	public static $updatedBy = 'updatedBy';
	public static $email = 'email';
	
	public static $typeUtilisateurLabel = 'typeUtilisateurLabel';
}

class Utilisateur
{
	private $utilisateurId;
	public function getUtilisateurId() { return $this->utilisateurId; }
	public function setUtilisateurId($value) { $this->utilisateurId = $value; }
	
	public function getAgentEtablissement()
	{
		$db = new EtablissementDatabase();
		$db->open();
		$rs = $db->GetAgentEtablissement($this->getUtilisateurId());
		$db->close();
			
		$etabs = new Etablissements($rs);
			
		return $etabs;
	}
	
	private $typeUtilisateurId;
	public function getTypeUtilisateurId() { return $this->typeUtilisateurId; }
	public function setTypeUtilisateurId($value) { $this->typeUtilisateurId = $value; }
	
	private $swActif;
	public function getSwActif() { return $this->swActif; }
	public function setSwActif($value) { $this->swActif = $value; }
	
	private $login;
	public function getLogin() { return $this->login; }
	public function setLogin($value) { $this->login = $value; }
	
	public function GetEmailAsAnchor() 
	{
		$email = $this->getLogin();
		return ((isset($email)) && (strlen($email) > 0)) ? sprintf('<a href="mailto:%s">%s</a>', $email, $email) : null;
	}
	
	private $motDePasse;
	public function getMotDePasse() { return $this->motDePasse; }
	public function setMotDePasse($value) { $this->motDePasse = $value; }
	
	private $nom;
	/* Si c'est un op�rateur affiche le nom de l'op?rateur plutot que l'utilisateur ? */ 
	public function getNom()
	{
		if($this->getTypeUtilisateurId() == USER_TYPE_OPERATEUR)
		{
			REQUIRE_ONCE(SCRIPTPATH.'model/operateur_database_model_class.php');
			$db = new OperateurDatabase();
			$db->open();
			$op = new Operateurs($db->get($this->getOperateurId()));
			$db->close();
			return $op->items(0)->GetNom(); 
		}
		else
			return $this->nom; 
	}
	public function setNom($value) {$this->nom = $value; }
	
	/* Nom r�el a �t� implemnt�e pour ne pas casser la fonction existante getNom() inexploitable dans le cas de la gestion des utilisateur */
	private $nomReel;
	public function getNomReel()	{ return $this->nom; }
    public function setNomReel($value) { $this->nomReel = $value; }
	
	private $prenom;
	public function getPrenom() { return $this->prenom; }
	public function setPrenom($value) { $this->prenom = $value; }
	
	private $tel;
	public function getTel() { return $this->tel; }
	public function setTel($value) { $this->tel = $value; }
	
	private $created;
	public function getCreated() { return $this->created; }
	public function setCreated($value) { $this->created = $value; }
	
	private $createdBy;
	public function getCreatedBy() { return $this->createdBy; }
	public function setCreatedBy($value) { $this->createdBy = $value; }
	
	private $updated;
	public function getUpdated() { return $this->updated; }
	public function setUpdated($value) { $this->updated = $value; }
	
	private $updatedBy;
	public function getUpdatedBy() { return $this->updatedBy; }
	public function setUpdatedBy($value) { $this->updatedBy = $value; }
	
	private $email;
	public function getEmail() { return $this->email; }
	public function setEmail($value) { $this->email = $value;  }

	private $typeUtilisateurLabel;
	public function getTypeUtilisateurLabel() { return $this->typeUtilisateurLabel; }
	public function setTypeUtilisateurLabel($value) { $this->typeUtilisateurLabel = $value; }

	public function getId() { return $this->getUtilisateurId(); }
	public function getLabel() { return $this->getPrenom() . ' ' . $this->getNom(); }
	
	public function getOperateurId()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/operateur_database_model_class.php');
		if($this->getTypeUtilisateurId() == 'OPE')
		{
			$db = new OperateurDatabase();
			$db->open();
			$ops = new OperateurUtilisateurs($db->getOperateurIdWithUtilisateurId($this->getUtilisateurId()));
			$db->close();
			$return = ($ops->items(0)!=null)?$ops->items(0)->GetOperateurId():null;
			return $return;
		}
		return null;
	}
	public function getOperateurLabel()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/operateur_database_model_class.php');
		if($this->getTypeUtilisateurId() == 'OPE')
		{
			$db = new OperateurDatabase();
			$db->open();
			$ops = new Operateurs($db->get($this->getOperateurId()));
			$db->close();
			return $ops->items(0)->GetOperateurId();
		}
		return null;
	}

	public function getFormateurId()
	{
		if($this->isFormateur())
		{
			REQUIRE_ONCE(SCRIPTPATH.'model/formateur_database_model_class.php');	
			$db = new FormateurDatabase();
			$db->open();
			$result = $db->getFormateursIdWithUtilisateurId($this->getUtilisateurId(), true);
			$fors = mysqli_fetch_assoc($result);
			$db->close();
			return (!empty($fors))?$fors['formateurId']:null;
		}
		return null;
	}
	
	function __construct($row = array())
	{
		if(isset ($row[UtilisateurFields::$utilisateurId])) $this->setUtilisateurId($row[UtilisateurFields::$utilisateurId]);
		$this->setTypeUtilisateurId($row[UtilisateurFields::$typeUtilisateurId]);
		$this->setSwActif($row[UtilisateurFields::$swActif]);
		$this->setLogin($row[UtilisateurFields::$login]);
		if(isset ($row[UtilisateurFields::$motDePasse])) $this->setMotDePasse($row[UtilisateurFields::$motDePasse]);
		$this->setNom($row[UtilisateurFields::$nom]);
		$this->setNomReel($row[UtilisateurFields::$nom]);
		if($this->getTypeUtilisateurId() != USER_TYPE_OPERATEUR)
			$this->setPrenom($row[UtilisateurFields::$prenom]);
		$this->setTel($row[UtilisateurFields::$tel]);
		if(isset ($row[UtilisateurFields::$created])) $this->setCreated($row[UtilisateurFields::$created]);
		if(isset ($row[UtilisateurFields::$createdBy]))$this->setCreatedBy($row[UtilisateurFields::$createdBy]);
		if(isset ($row[UtilisateurFields::$updated]))$this->setUpdated($row[UtilisateurFields::$updated]);
		if(isset ($row[UtilisateurFields::$updatedBy]))$this->setUpdatedBy($row[UtilisateurFields::$updatedBy]);
		if(isset ($row[UtilisateurFields::$email])) $this->setEmail($row[UtilisateurFields::$email]);
		if(isset ($row[UtilisateurFields::$typeUtilisateurLabel])) $this->setTypeUtilisateurLabel($row[UtilisateurFields::$typeUtilisateurLabel]);
	}
		
	//Permet de savoir si l'utilisateur est actif
	public function isLoggable()
	{
		return ($this->getSwActif() == USER_STATE_ACTIF);
	}
	
	public function isAdmin()
	{
		return ($this->getTypeUtilisateurId() == USER_TYPE_ADMIN);
	}
	
	public function isAgentCoordinateur()
	{
		return ($this->getTypeUtilisateurId() == USER_TYPE_AGENT_COORDINATEUR);
	}

	public function isCoordinateur()
	{
		return ($this->getTypeUtilisateurId() == USER_TYPE_COORDINATEUR);
	}
	
	public function isOperateur()
	{
		return ($this->getTypeUtilisateurId() == USER_TYPE_OPERATEUR);
	}
	
	public function isAgent()
	{
		return ($this->getTypeUtilisateurId() == USER_TYPE_AGENT);
	}
	
	public function isFormateur()
	{
		return ($this->getTypeUtilisateurId() == USER_TYPE_FORMATEUR);
	}
}

class Utilisateurs extends DomainBase
{
	function __construct($rs)
	{
		parent::__construct();
		if ($rs && mysqli_num_rows($rs) > 0)
		{
			while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new Utilisateur($row));
		}
	}
}
?>