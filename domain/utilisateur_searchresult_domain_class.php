<?php
REQUIRE_ONCE(SCRIPTPATH.'domain/base_domain_class.php');

class UtilisateurSearchResultFields
{
	public static $utilisateurId = 'utilisateurId';
	public static $typeUtilisateurId = 'typeUtilisateurId';
	public static $swActif = 'swActif';		
	public static $login = 'login';
	public static $nom = 'nom';
	public static $prenom = 'prenom';
	public static $email = 'email';
	public static $typeUtilisateurLabel = 'typeUtilisateurLabel';
	public static $tel = 'tel';
}

class UtilisateurSearchResult
{
	private $utilisateurId;
	public function getUtilisateurId() { return $this->utilisateurId; }
	public function setUtilisateurId($value) { $this->utilisateurId = $value; }
	
	private $typeUtilisateurId;
	public function getTypeUtilisateurId() { return $this->typeUtilisateurId; }
	public function setTypeUtilisateurId($value) { $this->typeUtilisateurId = $value; }
	
	private $swActif;
	public function getSwActif() { return $this->swActif; }
	public function setSwActif($value) { $this->swActif = $value; }
	
	private $login;
	public function getLogin() { return $this->login; }
	public function setLogin($value) { $this->login = $value; }
	
	private $nom;
	public function getNom() { return $this->nom; }
	public function setNom($value) { $this->nom = $value; }
	
	private $prenom;
	public function getPrenom() {  return $this->prenom; }
	public function setPrenom($value) { $this->prenom = $value; }
	
	private $email;
	public function getEmail() { return $this->email; }
	public function setEmail($value) { $this->email = $value;  }
	
	private $typeUtilisateurLabel;
	public function getTypeUtilisateurLabel() { return $this->typeUtilisateurLabel; }
	public function setTypeUtilisateurLabel($value) { $this->typeUtilisateurLabel = $value; }
	
	private $tel;
	public function getTel() { return $this->tel; }
	public function setTel($value) { $this->tel = $value; }
	
	public function getOperateurName()
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/operateur_database_model_class.php');
		REQUIRE_ONCE(SCRIPTPATH.'domain/operateur_domain_class.php');
		REQUIRE_ONCE(SCRIPTPATH.'domain/operateurutilisateur_domain_class.php');
		if($this->getTypeUtilisateurId() == 'OPE')
		{
			$db = new OperateurDatabase();
			$db->open();
			$ops = new OperateurUtilisateurs($db->getOperateurIdWithUtilisateurId($this->getUtilisateurId()));
			if (count($ops) != 0) {
				$op  = new Operateurs($db->get(intval($ops->items(0)->GetOperateurId())));
				$db->close();
				return $op->items(0)->GetNom();
			}
			else {
				$db->close();
				return '';
			}
		}
		return null;
	}
	
	function __construct($row = array())
	{

		if(isset($row[UtilisateurFields::$utilisateurId]))
			$this->setUtilisateurId($row[UtilisateurFields::$utilisateurId]);
		
		if(isset($row[UtilisateurFields::$typeUtilisateurId]))
			$this->setTypeUtilisateurId($row[UtilisateurFields::$typeUtilisateurId]);
		
		if(isset($row[UtilisateurFields::$swActif]))
			$this->setSwActif($row[UtilisateurFields::$swActif]);
		
		if(isset($row[UtilisateurFields::$login]))
			$this->setLogin($row[UtilisateurFields::$login]);
		
		if(isset($row[UtilisateurFields::$prenom]))
			$this->setPrenom($row[UtilisateurFields::$prenom]);
		
		if(isset($row[UtilisateurFields::$nom]))
			$this->setNom($row[UtilisateurFields::$nom]);
		
		if(isset($row[UtilisateurFields::$email]))
			$this->setEmail($row[UtilisateurFields::$email]);
		
		if(isset($row[UtilisateurFields::$typeUtilisateurLabel]))
			$this->setTypeUtilisateurLabel($row[UtilisateurFields::$typeUtilisateurLabel]);
		
		if(isset($row[UtilisateurFields::$tel]))
			$this->setTel($row[UtilisateurFields::$tel]);
	}
}

class UtilisateurSearchResults extends DomainBase
{
	function __construct($rs)
	{
		parent::__construct();
		
		if ($rs && mysqli_num_rows($rs) > 0)
		{
			while ($row = mysqli_fetch_assoc($rs)) array_push($this->elements, new UtilisateurSearchResult($row));
		}
	}
}
?>