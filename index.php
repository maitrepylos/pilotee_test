<?php

/**
 * Front controller
 */



session_cache_limiter('private,must-revalidate');
$content = '';
define('INTYPO3', false);

if(!INTYPO3)
{
	define('SCRIPTPATH','./');
	define('FILEPATH','./');
}
else
{
	die('<u><b>INDEX.PHP :</b></u> La constante SCRIPTPATH doit �tre sett�e ! Remplacez {{EXTENSION_NAME}} par le r�pertoire de l\'extension et retirer ce die.');
	define('SCRIPTPATH','typo3conf/ext/{{EXTENSION_NAME}}/');
	define('FILEPATH','./fileadmin/');
}

REQUIRE_ONCE(SCRIPTPATH.'config/constants.php');
REQUIRE_ONCE(SCRIPTPATH.'_init.php');
if (isset($_REQUEST['action']) && ( $_REQUEST['action']=='updateformationparticipants' || $_REQUEST['action']=='addformationparticipants')) {
	REQUIRE_ONCE(SCRIPTPATH.'controller/formationParticipantsProcess.php');
} else {

	ob_start();
	REQUIRE_ONCE(Mapping::Execute(MODULE));
	$content = ENV::get().ob_get_contents();
	ob_end_clean();
	
	if(!INTYPO3)
	{
		//tenir compte si ajax
		if(!empty(Mapping::$layout) && Mapping::$layout!='ajax_callback' )
		{
			REQUIRE_ONCE(SCRIPTPATH.'view/layout/'.Mapping::$layout.'.php'); exit;
		}
		else
		{
			echo $content;
		}
		flush();
	}
}
