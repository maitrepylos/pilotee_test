<?php

require_once(SCRIPTPATH . 'lib/abstractView.php');

/**
 * Overwritte possibilities
 * 	constructor
 * 		protected function _postConstruct(array $args = null)
 * 	defautl action
 * 		$this->_defaultAction = 'newMethodCall';
 * 
 * Call
 * 	$x = new X
 * 	$x->render(); // will call action and view render
 * 
 * Use
 * 	following action request the _XAction method will be call.
 * 	Example :
 * 		// $action = $_REQUEST['action']
 * 		$action = 'index';
 * 		// will call
 * 		$this->_indexAction();
 */
abstract class AbstractController
{
	/**
	 * @var AbstractView
	 */
	protected $_view;
	protected $_user;
	protected $_notices;
	protected $_errors;
	protected $_fieldserror;
	protected $_session;
	protected $_defaultAction = 'index';
	protected $_callableAction = 'index';
	protected $_callableActionMethod = 'indexAction';

	public function __construct(AbstractView $view)
	{
		//echo ('<br>debug FFI <br>');
		//var_dump($view);
		//echo ('<br>END debug FFI <br>');
		$this->_view = $view;
		$this->_session = Session::GetInstance();
		$this->_user = $this->_session->getCurrentUser();
		$this->_errors = array();

		if($this->_session->exist('erreurs'))
		{
			$this->_errors[] = $this->_session->get('erreurs');
			$this->_session->reset('erreurs');
		}

		$this->_notices = array();
		if($this->_session->exist('notices'))
		{
			$this->_notices[] = $this->session->get('notices');
			$this->_session->reset('notices');
		}
		$this->_fieldserror = array();

		$this->_dispatch();

		$this->_postConstruct(func_get_args());
	}

	public function render()
	{
		call_user_func(array($this, $this->_callableActionMethod));
		if (!empty($this->_view))
		{
			$this->_view->setAction($this->_getActionView());
			$this->_view->render();
		}
	}

	protected function _postConstruct(array $args = null)
	{
		// to overwrite
	}

	protected function _dispatch()
	{
		if (empty($_REQUEST['action']))
		{
			$this->_dispatchAction($this->_defaultAction);
		}
		else
		{
			$this->_dispatchAction(str_replace('.', '', addslashes(htmlspecialchars($_REQUEST['action']))));
		}
	}

	protected function _dispatchAction($action)
	{
		$actionMethod = $this->_buildActionMethodName($action);
		if (method_exists($this, $actionMethod))
		{
			$this->_callableAction = $action;
			$this->_callableActionMethod = $actionMethod;
		}
		elseif ($action == $this->_defaultAction)
		{
			throw new RuntimeException('Can\'t dispatch : ' . $action);
		}
		else
		{
			$this->_dispatchAction($this->_defaultAction);
		}
	}

	protected function _buildActionMethodName($action)
	{
		return '_' . $action . 'Action';
	}

	protected function _getActionView()
	{
		return $this->_callableAction;
	}

	protected function _sendErrorsAndNotices()
	{
		$this->_view->Notices($this->_notices);
		$this->_view->Errors($this->_errors);
	}
}

# EOF
