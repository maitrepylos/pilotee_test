<?php

REQUIRE_ONCE(SCRIPTPATH . 'lib/database.model.class.php');

abstract class AbstractDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function getById($id)
	{
		$sql = 'SELECT * FROM ' . $this->_getTableName() . ' WHERE ' . sprintf(' ' . $this->_getIdName() . ' = %s', $id);
		return $this->lquery($sql, DB_ECHO);
	}

	public function findAll()
	{
		$sql = 'SELECT * FROM ' . $this->_getTableName();
		return $this->lquery($sql, DB_ECHO);
	}
	
	abstract protected function _getTableName();
	
	abstract protected function _getIdName();
}

# EOF
