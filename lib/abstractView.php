<?php

REQUIRE_ONCE(SCRIPTPATH . 'lib/base.view.class.php');

/**
 * Render
 * 	on render() call the followinf method will be called _[ $this->getAction() ]Render()
 * Overwritte
 * 	_postRender() or _preRender()
 */
abstract class AbstractView extends BaseView
{
	/**
	 * @var string
	 */
	private $_action;
	private $_idAction;

	/**
	 * @var Utilisateur
	 */
	protected $_user;

	public function render()
	{
		$this->_preRender();

		$actionMethod = $this->_buildActionMethodName($this->getAction());
		if (method_exists($this, $actionMethod))
		{
			call_user_func(array($this, $actionMethod));
		}
		else
		{
			throw new RuntimeException('Can\'t render : ' . $this->getAction());
		}

		$this->_postRender();
	}

	public function setAction($action)
	{
		$this->_action = addslashes(htmlspecialchars($action));
	}

	public function getAction()
	{
		return $this->_action ;
	}
	
	public function addIdAction($idAction) {
		$this->_idAction = $idAction;
	}
	
	public function getIdAction() {
		return $this->_idAction;
	}

	protected function _postRender()
	{
		// to overwrite
	}

	protected function _preRender()
	{
		// to overwrite
	}

	protected function _buildActionMethodName($action)
	{
		return '_' . $action . 'Render';
	}

	/**
	 * @return Utilisateur
	 */
	protected function _getUser()
	{
		// it's awfull to see this in view but ...
		if (empty($this->_user))
		{ 
			$this->_user = Session::GetInstance()->getCurrentUser();
		}

		return $this->_user;
	}
}

# EOF
