<?php

REQUIRE_ONCE(SCRIPTPATH.'lib/excelwriter.class.php');

class ArrayExcelConfig
{
	const STRING = 1;
	const DATETIME = 2;
	const NUMERIC = 3;
	const DATE = 4;
	const TIME = 5;

	public $columns;
	
	function setColumn($header, $type, $width)
	{
		$this->columns[] = array('header' => $header, 'type' => $type, 'width' => $width);
	}
}

class ArrayExcelProxy extends ExcelWriter
{
	function loadArrayWorksheet($id, $name, $config, $array)
	{
		$this->setStyle('
  <Style ss:ID="header">
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="value">
   <Alignment ss:Vertical="Bottom" ss:ShrinkToFit="1"/>
  </Style>
  <Style ss:ID="datetime_value">
   <Alignment ss:Vertical="Bottom" ss:ShrinkToFit="1"/>
   <NumberFormat ss:Format="m/d/yy\ h:mm;@"/>
  </Style>
  <Style ss:ID="date_value">
   <Alignment ss:Vertical="Bottom" ss:ShrinkToFit="1"/>
   <NumberFormat ss:Format="m/d/yy"/>
  </Style>
  <Style ss:ID="time_value">
   <Alignment ss:Vertical="Bottom" ss:ShrinkToFit="1"/>
   <NumberFormat ss:Format="h:mm;@"/>
  </Style>');
		
		$w = $this->initWorksheet($id, $name);
		
		$filter = '_arrayWSFilter'.$id;
		
		$w->newRow(40, 'header');
		$columns = '';
		
		foreach($config->columns as $c)
		{
			$w->setString($c['header'], null, $filter);
			$columns.= '
   <Column ss:AutoFitWidth="0" ss:Width="'.($c['width'] + 0).'"/>';
		}
		
		$w->setColumns($columns);
		
		
		
		
		
		
		foreach($array as $row)
		{
			$w->newRow(null,'value');
			$nbCell = 0;
			foreach($row as $cell)
			{
				switch($config->columns[$nbCell]['type'])
				{
					case ArrayExcelConfig::NUMERIC:
						$w->setNumber($cell, null, $filter);
						break;
					case ArrayExcelConfig::DATETIME:
						$w->setDateTime($cell, 'datetime_value', $filter);
						break;
					case ArrayExcelConfig::DATE:
						$w->setDateTime($cell, 'date_value', $filter);
						break;
					case ArrayExcelConfig::TIME:
						$w->setDateTime($cell, 'time_value', $filter);
						break;
					default:
						$w->setString($cell, null, $filter);
						break;
				}
				
				$nbCell++;
			}
		}
		
		$range = 'R1C1:R'.Count($array).'C'.$w->maxColumn;
		$w->setNamedRange($filter,'=\'Filtre\'!'.$range, true);
		$w->autoFilter = $range;
		
		
	}
}

?>