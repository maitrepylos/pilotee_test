<?php
	/**
	 * Fonction d'autoload pour les classes domains
	 *
	 * @param String Nom de la classe
	 */
	function __autoload($Class)
	{
		$path = SCRIPTPATH.'domain/'.strtolower($Class).'_domain_class.php';
		
		if(file_exists($path))
		{
			REQUIRE_ONCE($path);
		}
		else
		{
			$path = SCRIPTPATH.'domain/'.strtolower(substr($Class,0, -1)).'_domain_class.php';
			
			if(file_exists($path))
			{
				REQUIRE_ONCE($path);	
			}
			else
			{
				$path = SCRIPTPATH.'domain/'.strtolower(substr($Class,0, -6)).'_domain_class.php';
				if(file_exists($path))
				{
					REQUIRE_ONCE($path);	
				}
			}
		}
	}
?>