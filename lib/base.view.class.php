<?php

class BaseView
{
	protected $section = null;

	public function __construct()
	{
		if(I18NENABLE && defined('LANGUAGE'))
		{
			if(I18NUSECOMMON) SP::load('common',LANGUAGE);

			if ($this->section != null)
			{
				SP::load($this->section,LANGUAGE);
			}
		}
	}

	/**
	 * Faire un appel � la fonction JavaScript permettant d'highlighter les champs erron�s
	 *
	 * @param Array Tableau contenant les ID des champs erron�s
	 * @return String Appel au javascript
	 */
	public function FieldsError($fieldserror = null)
	{
		if(!$fieldserror) $fieldserror = ENV::$fieldserror;

		if (!empty($fieldserror))
		{
			$body = '
			<script type="text/javascript">
			highlightFields(
			new Array(';
			foreach($fieldserror as $field)
			{
				$body.='
				\''.$field.'\',';
			}
			$body = substr($body,0,-1);
			$body.='
			)
			);
			</script>
			';
			echo $body;
		}
	}

	public function FieldsExemples($fieldsexemple)
	{
		if (!empty($fieldsexemple))
		{
			$body='
			<script type="text/javascript">
			var form = new CForm({
			fields : [';
			foreach($fieldsexemple as $key => $exemple)
			{
				$body.= '{ id: \''.$key.'\', format : \''.$exemple.'\' },';
			}
			$body = substr($body, 0, -1);
			$body.='
			]
		});
		</script>';
			echo $body;
		}
		return null;
	}
	
	public function raccourcisPanel($module = null)
	{
		$user = Session::GetInstance()->getCurrentUser();
		$raccourcis = '<br/>';


		switch($module)
		{
			case 'contact' :
			case 'contactDetail' :
				$raccourcis .= $this->_raccourcisPanelEntry('Cr�er contact', 'ShowFormAddContact(); return false;');
				if($module == 'contactDetail')
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Historique', 'ShowHistoriqueDetail(); return false;');
				}
				break;

			case 'action' :
			case 'actionDetail' :
				if($user->isOperateur())
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Encoder donn�es actions labellis�es', 'ShowFormAddAction(); return false;');
				}
				else
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Encoder donn�es autres actions', 'ShowFormAddAction(); return false;');
				}
					
				$raccourcis .= $this->_raccourcisPanelEntry('Cr�er contact', 'ShowFormAddContact(); return false;');

				if($module == 'actionDetail')
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Historique', 'ShowHistoriqueDetail(); return false;');
				}

				break;

			case 'formations' :
			case 'formationDetail' :
				
				if($user->isAdmin() || $user->isCoordinateur() || $user->isAgent() || $user->isFormateur() || $user->isAgentCoordinateur() )
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Ajouter une formation', 'ShowFormAddFormation(); return false;');
				}

				if($module == 'formationDetail')
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Historique', 'ShowHistoriqueDetail(); return false;');
					// $raccourcis .= $this->_raccourcisPanelEntry('Participants', 'ShowParticipantsFormation(); return false;');
				}
				
				$raccourcis .= $this->_raccourcisPanelEntry('Cr�er contact', 'ShowFormAddContactFormation(); return false;');
				
				break;
			case 'bourse' :
			case 'bourseDetail' :
				if($user->isAdmin() || $user->isCoordinateur() || $user->isAgentCoordinateur() )
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Cr�er projet entrepreneurial', 'ShowFormAddBourse(); return false;');
					$raccourcis .= $this->_raccourcisPanelEntry('Cr�er contact', 'ShowFormAddContact(); return false;');
				}

				if($module == 'bourseDetail')
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Historique', 'ShowHistoriqueDetail(); return false;');
				}

				break;
			case 'etablissement' :
			case 'etablissementDetail' :
				if ($user->isAdmin())
				{
					$raccourcis .= $this->_raccourcisPanelEntry('G�olocalisation', null, $this->BuildURL('google_maps_admin'));
					if($module == 'etablissement')
					{
						$raccourcis .= $this->_raccourcisPanelEntry('Cr�er �tablissement', 'ShowFormAddEtab(); return false;');
						$raccourcis .= $this->_raccourcisPanelEntry('Entit� administrative', null, $this->BuildURL('entiteAdmin'));
					}
				}

				if($user->isAdmin() || $user->isCoordinateur() || $user->isAgentCoordinateur() )
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Attribuer un agent', 'attribuerAgent(); return false;');
				}
				
				$raccourcis .= $this->_raccourcisPanelEntry('Cr�er contact', 'ShowFormAddContact(); return false;');
				
				if($user->isOperateur())
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Encoder donn�es actions labellis�es', 'ShowFormAddAction(); return false;');
				}
				else
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Encoder donn�es autres actions', 'ShowFormAddAction(); return false;');
				}
				
				if($user->isAdmin() || $user->isCoordinateur() || $user->isAgent() || $user->isFormateur() || $user->isAgentCoordinateur() )
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Ajouter une formation', 'ShowFormAddFormation(); return false;');
				}

				if($user->isAdmin() || $user->isCoordinateur() || $user->isAgentCoordinateur() )
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Cr�er projet entrepreneurial', 'ShowFormAddBourse(); return false;');
				}
					
				if($module == 'etablissementDetail')
				{
					if($user->isAdmin() || $user->isCoordinateur() || $user->isAgent() || $user->isAgentCoordinateur() )
					{
						$raccourcis .= $this->_raccourcisPanelEntry('Cr�er fiche suivi', 'ShowFormAddFicheSuivi(); return false;');
						$raccourcis .= $this->_raccourcisPanelEntry('Cr�er remise de mat�riel', 'ShowFormAddRemise(); return false;');
					}
				}
				
				$raccourcis .= $this->_raccourcisPanelEntry('Visualiser sur la carte', 'showGoogleMap(); return false;');
					
				break;

			case 'ficheSuiviDetail' :
				/*
					if ($user->isAdmin() || $user->isCoordinateur() || $user->isAgent())
					{
					$raccourcis .= '<div class="shortcut_item"><a class="shortcut_item" onclick="ShowFormAddContact();return false" href="javascript:void(0);">Cr?er contact</a><br/></div>';
					}
					*/
					
				$raccourcis .= $this->_raccourcisPanelEntry('Historique', 'ShowHistoriqueDetail(); return false;');
				break;

			case 'materielPedagDetail' :
				$raccourcis .= $this->_raccourcisPanelEntry('Historique', 'ShowHistoriqueDetail(); return false;');
				break;
			case 'report' :
				$raccourcis .= $this->_raccourcisPanelEntry('Rapport d\'activit� d\'encodage', null, $this->BuildURL('report') . '&type=activite');

				if(!$user->isOperateur())
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Rapport journal de classe', null, $this->BuildURL('report') . '&type=journal');
				}
				
				$raccourcis .= $this->_raccourcisPanelEntry('Rapport sur les �tablissements', null, $this->BuildURL('report') . '&type=etablissement');
				$raccourcis .= $this->_raccourcisPanelEntry('Rapport sur les actions', null, $this->BuildURL('report') . '&type=action');

				if(!$user->isOperateur())
				{
					$raccourcis .= $this->_raccourcisPanelEntry('Rapport sur les projets entrepreneuriaux', null, $this->BuildURL('report') . '&type=bourse');
				}
				
				$raccourcis .= $this->_raccourcisPanelEntry('Rapport sur les qualifications des contacts', null, $this->BuildURL('report') . '&type=contact');
				$raccourcis .= $this->_raccourcisPanelEntry('Listing - Contacts', null, $this->BuildURL('report') . '&type=listing_contact');
				$raccourcis .= $this->_raccourcisPanelEntry('Listing - Activit�s', null, $this->BuildURL('report') . '&type=listing_action');
				$raccourcis .= $this->_raccourcisPanelEntry('Listing - Etablissements', null, $this->BuildURL('report') . '&type=listing_etablissement');
				$raccourcis .= $this->_raccourcisPanelEntry('Listing FSE', null, $this->BuildURL('report') . '&type=listing_formation');
				break;
			case 'entiteAdmin' :
				$raccourcis .= $this->_raccourcisPanelEntry('Cr�er entit� admin.', 'ShowFormAddEntiteAdmin(); return false;');
				$raccourcis .= $this->_raccourcisPanelEntry('Rechercher entit� admin', null, $this->BuildURL('entiteAdmin'));

				break;
			// Marucci 24/12/2013 Ajout du lien utilisateurs depuis le panel d'aministration
			case 'administration' : 
				$raccourcis .= $this->_raccourcisPanelEntry('Utilisateurs', null, $this -> BuildURL('administration') . '&action=user_search');
				$raccourcis .= $this->_raccourcisPanelEntry('Ajouter un utilisateur', null, $this -> BuildURL('administration') . '&action=user_add');
		}
		return $raccourcis;
	}

	public function createMenu()
	{
		$user = Session::GetInstance()->getCurrentUser();

		$body  = '<ul>';
		$body .= $this->_createMenuEntry('Accueil', 'accueil', 'Accueil');
		$body .= $this->_createMenuEntry('Etablissement', 'etablissement', 'Etablissements d\'enseignement');
		if(!$user->isOperateur())
		{
			$body .= $this->_createMenuEntry('Contact', 'contact', 'Contacts');
		}
		
		$body .= $this->_createMenuEntry('Action', 'action', 'Actions');
		$body .= $this->_createMenuEntry('Formations', 'formations', 'Formations');
		if(!$user->isOperateur())
		{
			$body .= $this->_createMenuEntry('Bourse', 'bourse', 'Projets entrepreneuriaux');
		}
		$body .= $this->_createMenuEntry('Report', 'report', 'Rapports');
		/* Auteur Marucci 23/12 Cr?ation onglet Administration */
		if($user->isAdmin()){
			$body .= $this -> _createMenuEntry('Administration', 'administration', 'Administration');	
		}
		$body .='</ul>';

		return $body;
	}

	public function createLogin()
	{
		$session = Session::GetInstance();
		$user = $session->getCurrentUser();

		$login = $user->getTypeUtilisateurLabel() . ' ';
		$login .= $user->getLabel();

		$str = ToHTML($login) . '&nbsp;[<a href="'.SCRIPTPATH.'index.php?module=login&logout=1">D�connexion</a>]';

		return $str;
	}

	/*
	 public function FieldsResetButton($label,$fields)
	 {
	if (!empty($fields))
	{
	$script = 'return resetFields(new Array(';
				
			if (is_array($fields))
			{
			foreach($fields as $field)
			{
			$script.='\''.$field.'\',';
			}
			}
			else
	{
			$script.='\''.$fields.'\',';
			}
				
			$script.= '\'hack\'));';
	}

	echo '<input type="reset" name="reset" onclick="'.$script.'" value="'.$label.'">';
	}
	*/

	/**
	 * Afficher les erreurs
	 *
	 * @param Array Tableau contenant les erreurs � afficher
	 * @return String Erreurs
	 */
	public function Errors($errors = null)
	{
		if(!$errors) $errors = ENV::$errors;

		if (!empty($errors)) {
			$body = '
			<div class="errors" id="errors">
			<ul>';
			foreach($errors as $error)
			{
				$body.='
				<li>'.(I18NENABLE ? SP::get($error) : $error).'</li>';
			}

			$body.= '
			</ul>
			</div>';
			echo $body;
		}
	}

	/**
	 * Afficher les notices
	 *
	 * @param Array Tableau contenant les notices � afficher
	 * @return String Notices
	 */
	public function Notices($notices = null)
	{
		if(!$notices) $notices = ENV::$notices;

		if (!empty($notices)) {
			$body = '
			<div class="notices" id="notices">
			<ul>
			';
			foreach($notices as $notice)
			{
				$body.='		<li>'.(I18NENABLE ? SP::get($notice) : $notice).'</li>
				';
			}

			$body.= '	</ul>
			</div>
			';
			echo $body;
		}
	}

	protected function BuildURL($module = null, $forHtml = true)
	{
		$amp = ($forHtml ? '&amp;' : '&');
		//$data = Mapping::getModuleData($application, $module);

		if(INTYPO3)	$url = SITEPATH.'?id='.TYPO3PAGE.'&L='.TYPO3LANG.'&module='.urlencode($module);
		else		$url = SITEPATH.'?module='.urlencode($module);
		return $url;
	}

	/**
	 * Cr�e une entr�e pour le menu
	 * @param string $id composante de l'id de l'entr�e
	 * @param string $module module du lien
	 * @param string $label libell�
	 */
	protected function _createMenuEntry($id, $module, $label)
	{
		return '<li onMouseOver="javascript:onMouseOver(\'' . $id . '\');" onMouseOut="javascript:onMouseOut(\'' . $id . '\');"><a id="a' . $id . '" href="' .
			SCRIPTPATH . 'index.php?module=' . $module . '"><span id="spn' . $id . '" class="expand">' . $label . '</span></a></li>';
	}

	/**
	 * Cr�e une entr�e pour le panel de gauche
	 * @param string $label libell� du lien
	 * @param string $jsFct fonction js sur le lien, optionel
	 * @param string $liens IRI, optionel
	 */
	protected function _raccourcisPanelEntry($label, $jsFct = null, $liens = null)
	{
		if (empty($liens))
		{
			$liens = 'javascript:void(0);';
		}

		if (!empty($jsFct))
		{
			$jsFct = ' onclick="' . $jsFct . '"';
		}

		return '<div class="shortcut_item"><a class="shortcut_item" href="' . $liens . '"' . $jsFct . '>' . $label . '</a><br /></div>';
	}

	/**
	 * Obtenir une liste d�roulante
	 *
	 * @param String Nom (et id) de l'�l�ment HTML
	 * @param Array Tableau contenant les pays
	 * @param String Id du pays s�lectionn�
	 * @param bool Indique s'il faut ajouter une option vide
	 * @return String Element HTML
	 */
	protected function SelectList($name, $options, $sel = null, $defaultsel = null, $addEmptyOption = false, $onchange = null)
	{
		$liste = '
		<select name="'.$name.'" id="'.$name.'" class="input_inno"'.(!empty($onchange) ? ' onchange="'.$onchange.'"' : '').'>';
		if ($sel == null && !$addEmptyOption) $sel = $defaultsel;
		if ($addEmptyOption) $liste.= '
		<option value=""> --- </option>';

		foreach($options as $option)
		{
			$liste.= '
			<option value="'.ToHTML($option->getListId()).'"'.($sel == ToHTML($option->getListId()) ? ' selected="selected"' : '').'>'.ToHTML($option->getListLabel()).'</option>';
		}

		$liste.= '
		</select>
		';

		return $liste;
	}

	/**
	 * Obtenir une checkbox
	 *
	 * @param String Nom de l'�l�ment HTML
	 * @param String Valeur attribu�e au checkbox
	 * @param bool Indique si la checkbox doit �tre coch�e
	 * @param String Id de l'�l�ment HTML
	 * @return String Element HTML
	 */
	protected function Checkbox($name, $value, $checked = false, $id = null, $disabled = false, $onclick = null)
	{
		echo '<input class="notauto" type="checkbox" name="'.$name.'" value="'.ToHTML($value).'"'.($id != null ? ' id="'.$id.'"' : '').($checked ? ' checked="checked"' : '').($disabled ? ' disabled="disabled"' : '').($onclick ? ' onclick="'.$onclick.'"' : '').' />';
	}

	protected function CheckBoxList($name, $options, $sel = array(), $defaultsel = array())
	{
		if ($sel === null) $sel = $defaultsel;

		for ($x = 0; $x < count($options) ; $x++)
		{
			$liste.= $this->Checkbox($name.'_'.$options[$x]->getListId(), $options[$x]->getListId(), (array_search($options[$x]->getListId(), $sel) !== false), $name.'_'.$options[$x]->getListId()).'<label for="'.$name.'_'.$options[$x]->getListId().'">'.ToHTML($options[$x]->getListLabel()).'</label><br />';
		}

		return $liste;
	}

	protected function RadioList($name, $options, $sel = null, $defaultsel = null, $addEmptyOption = false)
	{
		if ($sel == null && !$addEmptyOption) $sel = $defaultsel;

		if ($addEmptyOption) $liste.= '
		<input class="notauto" type="radio" name="'.$name.'" id="'.$name.'_0" value="" /> <label class="field_label" for="'.$name.'_0">---</label>';

		for ($x = 0; $x < count($options) ; $x++)
		{
			$liste.= '
			<input class="notauto" type="radio" name="'.$name.'" id="'.$name.'_'.($x + 1).'" value="'.ToHTML($options[$x]->getListId()).'"'.($sel === ToHTML($options[$x]->getListId()) ? ' checked="checked"' : '').' /> <label for="'.$name.'_'.($x + 1).'" class="field_label">'.ToHTML($options[$x]->getListLabel()).'</label>';
		}

		return $liste;
	}
	
	/** MArucci Nicolas 26/12/2013
	 * Import une liste de resources JS
	 * @param $array la liste des resources JS ? importer
	 * @param $directory le dossier des resources cibles
	 * @return le html d'import du JS
	 */
	public function importScripts($array = array(), $directory = "") {
		$output = "";
		foreach ($array as $resource_name) {
			$output .= '<script type="text/javascript" src="scripts' . $directory . "/" . $resource_name . '" ></script>'."\n";
		}
		return $output;
	}
	
	//* Marucci 26/12/2013 add paginator fonction */
	protected function renderPaginator($begin=0,$offset=15,$nbResults=0,$colgroup){
		$nbPage = ceil($nbResults/$offset);
		$currentPage = 0;
		if($nbResults!=0)
			$currentPage = floor($begin/$offset)+1;
		?>
			<tfoot class="paginator">
				<tr>
					<td colspan="<?php echo $colgroup; ?>">
					<table>
						<tbody>
							<tr>
								<td><?php if($begin>0){
								?>
								<input type="button" class="button first" value="&#139;&#139;"/>
								<input type="button" class="button previous" value="&#139;" />
								<?php } 
								 if($nbPage>0){
								?>
								<input type="hidden" class="inputPaginator" value="<?php echo $currentPage; ?>" data-max="<?php echo $nbPage; ?>"/>
								<?php echo $currentPage; ?>&#160;/&#160;<?php echo $nbPage; ?>
								<?php }
								 if((intval($begin)+intval($offset))<$nbResults){ ?>
									<input type="button" class="button next" value="&#155;"/>
									<input type="button" class="button last" value="&#155;&#155;"/>
						<?php } ?></td>
							</tr>
						</tbody>
					</table></td>
				</tr>
			</tfoot>
<?php }
}
?>
