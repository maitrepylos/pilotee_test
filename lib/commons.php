<?php

/**
 * Fonction permettant de s'assurer que seul certaine balise html soit reprise dans un string
 *
 * @param string String � corriger
 * @return string String corrig�
 */
function sanitize_html($string)
{
	return strip_tags($string,'<p><a><b><strong><em><i><u><font><span><ul><ol><li><br>');
}

function sanitize_js($value)
{
	return preg_replace("/\r?\n/", "\\n", addslashes($value));
}

if(!function_exists('date_parse'))
{
	/**
	 * Impl�mentation (incompl�te) de la fonction date_parse de php
	 *
	 * @param string Date sous forme de string YYYY-MM-DD
	 * @return array Tableau reprenant l'ann�e, le mois et le jour de la date
	 */
	function date_parse($str)
	{
		$strX = explode(' ',$str);
		$strX = explode('-',$strX[0]);
		$array = array('year' => $strX[0], 'month' => $strX[1], 'day' => $strX[2]);
		
		return $array;
	}
}

/**
 * G�n�rer un mot de passe en fonction de param�tres donn�s
 *
 * @param Integer Longueur du mot de passe � g�n�rer
 * @param Integer Nombre de num�ro � g�n�rer (en plus des caract�res � utiliser imp�rativement)
 * @param Integer Nombre de caract�res sp�ciaux � g�n�rer (en plus des caract�res � utiliser imp�rativement)
 * @param String Caract�res � exclure du mot de passe (le caract�re NULL est ajout� automatiquement)
 * @param String Caract�res � utiliser imp�rativement (chaque caract�re sera utilis� une fois)
 * @return String Mot de passe (ou FALSE si les crit�res sont incoh�rents)
 */
function GeneratePassword($length = 8,$nbGeneratedNumbers = 3,$nbGeneratedSpecialChars = 0, $excludedChars = "^~?`?l", $mustUseChars = "")
{
	$excludedChars.="\0";
	$nbGeneratedAlphabetic = $length - ($nbGeneratedNumbers + $nbGeneratedSpecialChars + strlen($mustUseChars));
	
	//if ($nbAlphanumeric < 0) return false;
	
	$chars = array();
	
	for	($x = 0;$x < strlen($mustUseChars);$x++) {
		if (strpos($excludedChars, $mustUseChars[$x])) return false;
		$chars[] = $mustUseChars[$x];
	}

	for ($x = 0; $x<$nbGeneratedNumbers; $x++) {						
		do {
			$char = chr(rand(48,57));
		} while (strpos($excludedChars, $char));
		$chars[] = $char;		
	}
	
	for ($x = 0; $x<$nbGeneratedSpecialChars; $x++) {
		do {
			if (rand(0,1) == 1) {
				$char = chr(rand(35,47));
			} else {
				$char = chr(rand(58,64));	
			}
		} while (strpos($excludedChars, $char));
		$chars[] = $char;
	}

	for ($x = 0; $x<$nbGeneratedAlphabetic; $x++) {
		do {
			if (rand(0,1) == 1) {
				$char = chr(rand(65,90));
			} else {
				$char = chr(rand(97,122));
			}
		} while (strpos($excludedChars, $char));
		$chars[] = $char;
	}

	$pass = "";	
	
	for ($x = 0; $x < $length; $x++) {
		do {		
			$idx = rand(0,$length-1);
		} while ($chars[$idx] == "\0");
		
		$pass.=$chars[$idx];
		$chars[$idx] = "\0";
		
	}
	
	return $pass;
}

/**
 * Transforme un string pour qu'il soit affichable dans le html
 *
 * @param String Texte
 * @return String Texte affichable dans un document HTML
 */
function ToHTML($value, $allowLinebreak = true) {
	$value = htmlentities($value,ENT_QUOTES, 'ISO-8859-1');
	if ($allowLinebreak)
	{
		return nl2br($value);
	}
	else
	{
		return $value;
	}
}

/**
 * Permet d'obtenir le r�sultat pr�format� d'un print_r sur l'objet pass� en param�tre
 *
 * @param mixed Objet
 * @return String Affichage HTML dans une balise pre
 */
function Pre($objet) {
	return '<pre style="text-align:left">'.htmlentities(print_r($objet,true)).'</pre>';
}


/**
 * Passer une date du format YYYY-MM-DD ... au format DD/MM/YYYY
 *
 * @param String Date au format YYYY-MM-DD ...
 * @return String Date au format DD/MM/YYYY
 */
function FormatDate($date) {
	if ($date == '0000-00-00 00:00:00' || $date == '0000-00-00') return null;
	
	$formatted_date = $date;
	$dateA1 = explode(' ',$date);
	
	if (count($dateA1) > 0) {
		$dateA2 = explode ('-',$dateA1[0]);
		if (count($dateA2) > 2 && checkdate($dateA2[1],$dateA2[2],$dateA2[0])) {
			$formatted_date = sprintf('%02d/%02d/%04d',$dateA2[2],$dateA2[1],$dateA2[0]);
		}
	}
	
	return $formatted_date;
}

/**
 * Passer une date du format DD/MM/YYYY au format YYYY-MM-DD 00:00:00
 *
 * @param String Date au format DD/MM/YYYY
 * @return String Date au format YYYY-MM-DD 00:00:00
 */
function UnformatDate($date) {
	$unformatted_date = null;
	
	$date = trim($date);
	$dateA1 = explode('/',$date);
		
	if (count($dateA1) > 2 && checkdate($dateA1[1],$dateA1[0],$dateA1[2])) {
		$unformatted_date = sprintf('%04d-%02d-%02d 00:00:00',$dateA1[2],$dateA1[1],$dateA1[0]);
	}
	
	return $unformatted_date;
}

/**
 * Obtenir la date courante au format YYYY-MM-DD
 *
 * @return String Date courante au format YYYY-MM-DD
 */
function NowYMD() {
	return date('Y-m-d');
}

function NowDMY()
{
	return date('d/m/Y');
}

/**
 * 
 *
 * @param unknown_type $date
 * @return unknown
 */
function UnformattedDateToTS($date)
{
	$dateA1 = explode(' ',$date);
	
	if (count($dateA1) > 0) {
		$dateA2 = explode ('-',$dateA1[0]);
		if (count($dateA2) > 2 && checkdate($dateA2[1],$dateA2[2],$dateA2[0])) {
			return mktime(0,0,0,$dateA2[1],$dateA2[2],$dateA2[0]);
		}
	}
	
	return null;
}

/**
 * Obtenir l'age d'une personne
 *
 * @param String Date de naissance
 * @param String Date de r�f�rence pour le calcul (par d�faut now)
 * @return int Age
 */
function GetAge($date, $reference = null)
{
	if ($reference == null) $reference = date('d/m/Y');

	$reference = explode('/',$reference);
	$date = explode('/',trim($date));
	
	$age = $reference[2] - $date[2];

	if ($reference[1] < $date[1]) {
		$age--;
	}
	elseif ($reference[1] == $date[1] && $reference[0] < $date[0]) {
		$age--;
	}
	
	return $age;
}


/**
 * Calculer un modulo (l'op�rateur % n'accepte qu'un entier en dividende)
 *
 * @param Float Dividende
 * @param Float Diviseur
 * @return Modulo
 */
function Modulo($q, $d) {
	return ($q - $d * floor($q / $d));
}

function trace()
{
	try
	{
		throw new Exception();
	}
	catch(Exception $e)
	{
		echo Pre($e->getTraceAsString());
	}
}

function insertGA()
{
	return;
	?>
	<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
		try {
		var pageTracker = _gat._getTracker("UA-3751362-2");
		pageTracker._trackPageview();
		} catch(err) {}
	</script>
	<?php
}

function guessFileType($filename)
{
	$ext = strtolower(substr($filename,(strrpos($filename,'.') + 1)));
	
	$a = array(
		'pdf' => 'pdf',
		'doc' => 'doc',
		'docx' =>'doc',
		'docm' =>'doc',
		'xls' => 'xls',
		'xlm' => 'xls',
		'xlsx' =>'xls',
		'png' => 'img',
		'jpg' => 'img',
		'gif' => 'img',
		'jpeg' =>'img',
		'bmp' => 'img',
		'sql' => 'exec',
		'php' => 'exec',
		'exe' => 'exec',
		'com' => 'exec',
		'pl'  => 'exec',
		'cgi' => 'exec',
		'txt' => 'txt',
		'pps' => 'pps',
		'ppt' => 'pps'
	);
	
	if(isset($a[$ext])) return array($ext, $a[$ext]);
	else return array($ext, '');
}

function getUploadPath()
{
	return UPLOAD_DIR;

}

/*V�rifie si  l'extension du fichier est autoris�e*/
function isExtensionOk($fichier) {
	
    $extensions = unserialize(EXTENSION);
    $ext = strtolower(substr($fichier,(strrpos($fichier,'.') + 1)));
    
    if (!in_array($ext, $extensions))
        return FALSE;
    else 
        return TRUE;
}

function createInputForm($name,$value,$size=null,$mode=false)
{
	$readOnly = '';
	if($mode)
		$readOnly = 'readonly="readonly"';
	
	$input = '<input type="text" name="'.$name.'" id="'.$name.'" '.$readOnly.' value = "'.ToHTML($value).'"';
	if(isset($size))
		$input .= ' size = "'.$size.'"';
		
	$input .=' />';
	echo $input;
}

//Cr�ation d'un select sur base d'un objet du domaine
function createSelect($name,$lValue, $valSelected)
{
	$input = '<select name="'.$name.'" id="'.$name.'">';
	
	if($valSelected=='-1')
		$input.='<option value="-1"></option>';
	else
		$input.='<option selected="selected" value="-1"></option>';
	
		for($i=0;$i<$lValue->count(); $i++)
		{
			$select = '';
			if($lValue->items($i)->getId() == $valSelected)
				$select = 'selected="selected"';
			
			$input.='<option '.$select.' value="'.$lValue->items($i)->getId().'">'.$lValue->items($i)->getLabel().'</option>';
		}
	
	$input .='</select>';
	echo $input;
}

//Cr�ation d'un select sur base d'un array
function createSelectArray($name,$array, $valSelected)
{
	$input = '<select name="'.$name.'" id="'.$name.'">';

	if($valSelected=='-1')
		$input.='<option value="-1"></option>';
	else
		$input.='<option selected="selected" value="-1"></option>';
			
	for($i=0;$i<count($array); $i++)
	{
		$select = '';
		if($i == $valSelected)
			$select = 'selected="selected"';
			
		$input.='<option '.$select.' value="'.$i.'">'.$array[$i].'</option>';
	}
	
	$input .='</select>';
	echo $input;
}

function createSelectOptions($values, $selectedValue = null)
{
	$input = "";

	for ($i = 0; $i < $values->count(); $i++)
	{
		$select = "";
		
		if ($selectedValue != null && $values->items($i)->getId() == $selectedValue)
			$select = "selected='selected'";
			
		$input .= "<option " . $select . " value='" . $values->items($i)->getId() . "'>" . ToHTML($values->items($i)->getLabel()) . "</option>";
	}
	
	// echo '<hr>' . $input . '<hr>';
	
	return $input;
}

?>
