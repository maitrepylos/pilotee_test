<?php

/**
 * @package Libs
 * @copyright Cediti
 * @subpackage Model
 * @version 1.1
 */

/**
 * Classe repr�sentant un argument de proc�dure stock�e
 *
 */
class StoredProcedureArgument
{
	/**
	 * Nom du param�tre
	 *
	 * @var String
	 */
	private $name;
	
	/**
	 * Type du param�tre
	 *
	 * @var String
	 */
	private $type;
	
	/**
	 * Valeur du param�tre
	 *
	 * @var Mixed
	 */
	private $value;
	
	/**
	 * Instancier un nouveau param�tre de proc�dure stock�e
	 *
	 * @param String Nom du param�tre
	 * @param String Type du param�tre (IN, OUT ou INOUT)
	 * @param Mixed Valeur du param�tre
	 */
	function __construct($name, $type = 'OUT', $value = null)
	{
		$this->name = 'var_'.$name;
		$this->type = $type;
		
		if ($type == 'OUT')
		{
			$value = null;
		}
		
		$this->value = $value;
	}
	
	/**
	 * Obtenir le nom du param�tre
	 *
	 * @return String Nom
	 */
	public function getName()
	{
		return $this->name; 
	}
	
	/**
	 * Obtenir le type du param�tre
	 *
	 * @return String Type
	 */
	public function getType()
	{
		return $this->type; 
	}
	
	/**
	 * Obtenir la valeur du param�tre
	 *
	 * @return Mixed Valeur
	 */
	public function getValue()
	{
		return $this->value; 
	}
	
	/**
	 * D�finir la valeur du param�tre
	 *
	 * @param Mixed Valeur
	 */
	public function setValue($val)
	{
		$this->value = $val; 
	}
}


/**
 * Classe database g�n�rique pour la manipulation de la base de donn�es
 */
class Database
{
	/**
	 * D�fini si une connection est ouverte
	 */
	private function isOpen()
	{
		return (!empty(Database::$connections[$this->host.'/'.$this->database]));
	}
	
	private function getConnection()
	{
		return Database::$connections[$this->host.'/'.$this->database];
	}
	
	static $connections = array();
	
	/**
	 * Nom de l'h�te (serveur de la base de donn�e)
	 *
	 * @var String
	 */
	private $host;
	
	/**
	 * Login pour la connection � la base de donn�es
	 *
	 * @var String
	 */
	private $login;
	
	/**
	 * Password pour la connection � la base de donn�es
	 *
	 * @var String
	 */
	private $password;
	
	/**
	 * Nom de la base de donn�es
	 *
	 * @var String
	 */
	private $database;
	
	/**
	 * D�fini si une transaction est en cours
	 *
	 * @var Bool
	 */
	private $openTransaction = false;
	
	static public $requestNumber = 0;
	
	/**
	 * Faire appel � une proc�dure stock�e
	 *
	 * @param String Nom de la procedure
	 * @param Array Tableau d'arguments (StoredProcedureArgument)
	 * @param Bool Indique si les requ�tes SQL et les �ventuelles erreurs mysql doivent �tre affich�es � l'�cran
	 * @return Array Tableau contenant la valeur des variables de type OUT et INOUT
	 */
	protected function callStoredProcedure($storedProcName,$storedProcArg = array(),$echo = false) 
	{	
		$call = 'CALL '.$storedProcName.'(';
		$select = 'SELECT ';
		foreach($storedProcArg as $key => $value)
		{
			if ($value->getType() != 'OUT')
			{
				$val = $value->getValue();
				$val = (
							is_string($val) ? 
								'\''.$val.'\''
							:
								(is_bool($val) ?
									($val ?
										'TRUE'
									:
										'FALSE'
									)
								:
									$val
							)
						);
				$sql = 'SET @'.$value->getName().' = '.$val;
				$this->lquery($sql,$echo);
			}
			
			if ($value->getType() != 'IN')
			{
				$select.='@'.$value->getName().',';
			}
			
			$call.='@'.$value->getName().',';
		}
		$call=substr($call,0,-1).');';		
		$this->lquery($call,$echo);
		
		$select=substr($select,0,-1).';';
		$res_var = $this->lquery($select,$echo);
		
		if ($res_var && $row = mysqli_fetch_assoc($res_var))
		{
			return $row;
		}
	}
	
	/**
	 * D�buter une transaction
	 *
	 */
	public function startTransaction()
	{
		$this->lquery("START TRANSACTION;");
		$this->openTransaction = true;
	}
	
	/**
	 * Accepter les changements effectu�s par une transaction
	 *
	 */
	public function commitTransaction()
	{
		$this->lquery('COMMIT');
		$this->openTransaction = false;
	}
	
	/**
	 * Annuler les changements effectu�s par une transaction
	 *
	 */
	public function rollbackTransaction()
	{
		$this->lquery('ROLLBACK');
		$this->openTransaction = false;
	}
	
	/**
	 * D�finir les informations de connexion
	 *
	 * @param String Nom de l'h�te
	 * @param String Login pour la connection � la base de donn�es
	 * @param String Password pour la connection � la base de donn�es
	 * @param String Nom de la base de donn�es
	 */
	protected function setConnectionData($host, $login, $password, $database) {
		$this->host = $host;
		$this->login = $login;
		$this->password = $password;
		$this->database = $database;
		$this->open();
	}
	
	/**
	 * Tente d'ouvrir une connexion sur la base de donn�es
	 *
	 */
	public function open() {
		if (!$this->isOpen()) {
			$connect = @mysqli_connect($this->host,$this->login,$this->password,$this->database);
			if (!$connect)
				throw new DatabaseException(DBEXCEPTION_SERVER);				
			Database::$connections[$this->host.'/'.$this->database] = $connect;
		}
	}
	
	/**
	 * Ferme la connexion � la base de donn�es si elle existe
	 *
	 */
	public function close($force = false)
	{
		if ($this->openTransaction !== true && $force)
		{
			if ($this->isOpen())
			{
				//if (mysql_close())
				if (mysqli_close(Database::$connections[$this->host.'/'.$this->database]))
				{
					unset(Database::$connections[$this->host.'/'.$this->database]);
				}
			}
		 }
	}
	
	public static function closeAll()
	{
		foreach(Database::$connections as $connection)
		{
			mysqli_close($connection);
		}
	}

	/**
	 * Ex�cuter une requ�te SQL de "bas niveau" (execute only if connection already open and do not close connection)
	 *
	 * @param String Requ�te SQL
	 * @param Bool Indique si la requ�te SQL et les �ventuelles erreurs mysql doivent �tre affich�es � l'�cran
	 * @return Resource R�sultat de la requ�te
	 */
	protected function lquery($query_string, $echo = false) {
		if ($this->isOpen()) {
			$result = @mysqli_query($this->getConnection(), $query_string);
			ENV::$requestNumber++;
			
			if ($echo || !$result) ENV::logSql($query_string);
			if (!$result) {
				echo $query_string;
				ENV::logError('^ '.mysqli_errno($this->getConnection()).' > '.mysqli_error($this->getConnection()));
				throw new DatabaseException(DBEXCEPTION_QUERY);
			}
			
			return $result;
		}
	}
	
	/**
	 * Ex�cuter une requ�te SQL de "haut niveau" "intelligente", comme hquery sauf que ne ferme pas les connections ouvertes
	 *
	 * @param String Requ�te SQL
	 * @param Bool Indique si la requ�te SQL et les �ventuelles erreurs mysql doivent �tre affich�es � l'�cran
	 * @return Resource R�sultat de la requ�te
	 */
	protected function iquery($query_string, $echo = DB_ECHO)
	{
		$open = $this->isOpen();

		if(!$open) $this->open();
		
		$result = $this->lquery($query_string, $echo);

		if(!$open) $this->close(true);
		
		return $result;
	}
	
	/**
	 * S'assurer qu'une valeur soit de type num�rique, la convertir en type num�rique le cas �ch�ant
	 *
	 * @param mixed Valeur
	 * @return Numeric Valeur num�rique
	 */
	protected function param_num($value) {
		$value = trim($value);
		$value = str_replace(',','.',$value);

		$valueAr = explode('.',$value);

		$count = count($valueAr);
		if ($count == 1) {
			$value = $valueAr[0];
		} else {
			$value = "";
			foreach($valueAr as $key=>$val) {
				if ($key == ($count - 1)) $value.='.';
				$value.=$val;
			}
		}

		$value = $value + 0;		
		return $value;
	}
	
	/**
	 * S'assurer qu'une valeur soit de type mon�taire (num�rique � deux d�cimales), la convertir en type mon�taire le cas �ch�ant
	 *
	 * @param mixed Valeur
	 * @return Numeric Valeur mon�taire
	 */
	protected function param_currency($value) {
		return round($this->param_num($value),2);
	}
	
	/**
	 * S'assurer qu'une valeur soit un entier, la convertir en entier le cas �ch�ant
	 *
	 * @param mixed Valeur
	 * @return Integer Entier
	 */
	protected function param_int($value) {
		return floor($this->param_num($value));
	}
	
	/**
	 *
	 * @param mixed Valeur
	 * @return String Valeur escap�e
	 */
	protected function param_string($value) {
		//return $str = strip_tags(addslashes($value));
		return mysqli_real_escape_string($this->getConnection(), $value);
	}
	
	protected function param_string2($value)
	{
		return mysqli_real_escape_string($this->getConnection(), $value);
	}
	
	/**
	 * Obtenir la repr�sentation en string du bool�en r�sultant de la comparaison de la valeur � true pour la base de donn�es
	 *
	 * @param mixed Valeur
	 * @return String Repr�sentation en string du bool�en r�sultant de la comparaison de la valeur � true
	 */
	protected function param_bool($value) {
		return ( $value == true ? 'TRUE' : 'FALSE' );
	}
	
	/**
	 * Passer une date du format DD/MM/YYYY au format YYYY-MM-DD 00:00:00 pour la base de donn�es
	 *
	 * @param mixed Valeur
	 * @return String Date
	 */
	protected function param_date($value) {
		return UnformatDate($value);		
	}
	
	/**
	 * Obtenir l'id du dernier enregistrement ins�r�
	 *
	 * @return mixed Identifiant du dernier enregistrement ins�r�
	 */
	public function getLastInsertId()
	{
		return mysqli_insert_id($this->getConnection());
	}
	
	/**
	 * Obtenir le code d'erreur de la derni�re erreur mysql survenue
	 *
	 * @return Int Code d'erreur
	 */
	public function getErrorNumber()
	{
		return mysqli_errno($this->getConnection());
	}

	public function free($result)
	{
		return mysqli_free_result($result);
	}
	
	/* Auteur Marucci 24/12/13
	 * Execute une requete et renvoie un tableau associatif
	 * @param $id de la colonne a utiliser comme cle du tableau
	 */
	public function qA($sql, $id = null) {
		$result = $this -> iquery($sql);

		$return = array();
		if ($result) {
			if ($id) {
				while ($row = mysqli_fetch_assoc($result)) {
					$return[$row[$id]] = $row;
				}
			}
			else {
				while ($row = mysqli_fetch_assoc($result)) {
					$return[] = $row;
				}
			}
		}

		return $return;
	}
}

/**
 * Classe d'exception pour la database
 */
class DatabaseException extends Exception {
	
	/**
	 * Constructeur
	 *
	 * @param Integer Code d'erreur
	 * @param String Message d'erreur
	 */
	function __construct($code = DBEXCEPTION_SERVER,$message = NULL) {
		
		$message;
		
		switch ($code) {
			case DBEXCEPTION_SERVER : 
				$message = 'La connexion au serveur de base de donn�es a �chou�e.';
				break;			
			case DBEXCEPTION_DATABASE : 
				$message = 'La base de donn�e indiqu�e n\'a pas �t� trouv�e.';
				break;
			case DBEXCEPTION_QUERY :
				$message = 'L\'ex�cution de la requ�te a �chou�e.';
				break;
			case DBEXCEPTION_OPENTRANSACTION :
				$message = 'Une transaction est toujours ouverte.';
				break;
		}
		
		parent::__construct($message, $code);		
	}
	
	/**
	 * Cast de la classe en string
	 *
	 * @return String Cast de la classe en string 
	 */
	public function __toString() {
		return "<pre>\r\n".__CLASS__ . " {\r\n\t[Code : {$this->getCode()}]\r\n\t[Message : {$this->getMessage()}]\r\n\t[Position : Line {$this->getLine()} of {$this->getFile()}]\r\n}\r\n</pre>";
	}	
	
}

/**
 * Code d'erreur pour une tentative de connexion au serveur de base de donn�es �chou�e
  */
define('DBEXCEPTION_SERVER',0);

/**
 * Code d'erreur pour l'absence de base de donn�es portant le nom indiqu�
 */
define('DBEXCEPTION_DATABASE',1);

/**
 * Code d'erreur pour l'ex�cution d'une requ�te �chouant
 */
define('DBEXCEPTION_QUERY',2);

/**
 * Code d'erreur pour la fermeture de la connection lorsqu'une transaction est en cours
 */
define('DBEXCEPTION_OPENTRANSACTION',3);

// To close automatically connection
register_shutdown_function(array('Database', 'closeAll'));
