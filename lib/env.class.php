<?php

abstract class LogGeneric
{
	public $type;
	public $time;
	public $memory;
	
	public function __construct($type)
	{
		$this->type = $type;
		$this->time = ENV::getTimeElapsed();
		$this->memory = ENV::getMemoryUsage();
	}
	
	abstract function get();
	
	protected function getStats()
	{
		return '<div class="LogStats" id="Log'.$this->id.'_stats">
		'.$this->memory.' | '.$this->time.'
	</div>';
	}
	
	protected function getJs()
	{
		return '<script type="text/javascript">
		LoggerSH(\''.$this->id.'\');
	</script>';
	}
}

class LogSql extends LogGeneric
{
	public $sql;
	
	public function __construct($sql)
	{
		parent::__construct('sql');
		$this->sql = $sql;
	}
	
	public function get()
	{
		if(strlen($this->sql) > 120) $title = substr($this->sql,0,120).' [...]';
		else $title = $this->sql;
				
		$key = array('as', 'select','from','insert','into','where', 'update', 'delete', 'truncate', 'is', 'not', 'null', 'left', 'right', 'join');
		$message = htmlentities(trim($this->sql));
		
		foreach($key as $k)
		{
			$message = str_ireplace($k.' ', ' <span class="LogKeyword">'.strtoupper($k).'</span> ', $message);
			$message = str_ireplace(' '.$k, ' <span class="LogKeyword">'.strtoupper($k).'</span> ', $message);		}
		
		return '
<div class="Log LogSql" id="Log'.$this->id.'">
	<div class="LogTitle" id="Log'.$this->id.'_title">
		'.htmlentities($title).'
	</div>
	<div class="LogContent" id="Log'.$this->id.'_content">
		'.nl2br($message).'
	</div>
	'.$this->getStats().'
</div>
'.$this->getJs();
	}
}

class LogError extends LogGeneric
{
	public $error;
	
	public function __construct($error)
	{
		parent::__construct('error');
		$this->error = $error;
	}
	
	public function get()
	{
		return '
<div class="Log LogError" id="Log'.$this->id.'">
	<div class="LogTitle" id="Log'.$this->id.'_title">
		'.htmlentities($this->error).'
	</div>
</div>
'.$this->getJs();
	}
}

class LogException extends LogGeneric
{
	public $exception;
	
	public function __construct($exception)
	{
		parent::__construct('exception');
		$this->exception = $exception;
	}
	
	protected function getFile()
	{
		$filename = $this->exception->getFile();
		
		$filename = substr($filename,strrpos($filename, '/') + 1);
		$filename = substr($filename,strrpos($filename, '\\') + 1);
		
		return $filename;
	}
	
	public function get()
	{
		$title = '['.($this->exception->getCode() + 0).'] '.$this->exception->getMessage().' ('.$this->getFile().' #'.$this->exception->getLine().')';
		$message = $this->exception->getTraceAsString();
		
		return '
<div class="Log LogException" id="Log'.$this->id.'">
	<div class="LogTitle" id="Log'.$this->id.'_title">
		'.htmlentities($title).'
	</div>
	<div class="LogContent" id="Log'.$this->id.'_content">
		'.nl2br(htmlentities($message)).'
	</div>
	'.$this->getStats().'
</div>
'.$this->getJs();
	}
}

class LogObject extends LogGeneric
{
	public $object;
	
	public function __construct($object)
	{
		parent::__construct('object');
		ob_start();
			var_dump($object);
			$this->object = ob_get_contents();
		ob_end_clean();
	}
	
	public function get()
	{
		$title = 'objet';
		
		return '
<div class="Log LogObject" id="Log'.$this->id.'">
	<div class="LogContent" id="Log'.$this->id.'_content">
		<pre style="margin:0; padding:0;">'.htmlentities($this->object).'</pre>
	</div>
	'.$this->getStats().'
</div>
'.$this->getJs();
	}
}

class LogMessage extends LogGeneric
{
	public $message;
	
	public function __construct($message)
	{
		parent::__construct('message');
		$this->message = $message;
	}
	
	public function get()
	{
		if(strlen($this->message) > 120) $title = substr($this->message,0,120).' [...]';
		else $title = $this->message;
		
		$message = $this->message;
		
		return '
<div class="Log LogMessage" id="Log'.$this->id.'">
	<div class="LogTitle" id="Log'.$this->id.'_title">
		'.htmlentities($title).'
	</div>
	<div class="LogContent" id="Log'.$this->id.'_content">
		'.nl2br(htmlentities($message)).'
	</div>
	'.$this->getStats().'
</div>
'.$this->getJs();
	}
}

class ENV
{
	static public $collection = array();
	static public $time;
	static public $active = false;
	static public $requestNumber = 0;
	
	static public $errors = array();
	static public $notices = array();
	static public $fieldserror = array();
	
	static public function addError($error, $field = null)
	{
		if($error != null) ENV::$errors[] = $error;
		if($field != null) ENV::$fieldserror[] = $field;
	}
	
	static public function addNotice($notice)
	{
		if($notice != null) ENV::$notices[] = $notice;
	}
	
	static public function initialize($active)
	{
		ENV::$active = $active;
		ENV::$time = microtime(true);
	}

	static public function getRequestsNumber()
	{
		return ENV::$requestNumber;
	}
	
	static public function getTimeElapsed()
	{
		return round((microtime(true) - ENV::$time) * 1000, 2).' ms';
	}
	
	static public function getMemoryUsage()
	{
		return (function_exists('memory_get_usage') ? round(memory_get_usage() / (1024 * 1024),2).' mo' : 'Pas de memory_get_usage');
	}
	
	static protected function addLog($l)
	{
		$l->id = Count(ENV::$collection);
		ENV::$collection[] = $l;
	}
	
	static protected function logMessage($message)
	{
		$l = new LogMessage($message);
		ENV::addLog($l);
	}
	
	static protected function logObject($object)
	{
		$l = new LogObject($object);
		ENV::addLog($l);
	}
	
	static protected function logException($exception)
	{
		$l = new LogException($exception);
		ENV::addLog($l);
	}
	
	static public function logError($error)
	{
		if(!ENV::$active) return;
		$l = new LogError($error);
		ENV::addLog($l);
	}
	
	static public function logSql($sql)
	{
		if(!ENV::$active) return;
		$l = new LogSql($sql);
		ENV::addLog($l);
	}
	
	static public function log($v)
	{
		if(is_string($v)) ENV::logMessage($v);
		elseif(is_object($v) && is_a($v, 'Exception')) ENV::logException($v);
		else ENV::logObject($v);
	}
	
	static public function get()
	{
		if(!ENV::$active) return;
		
		$string = '
<style>
	body div.Logger
	{
		font-size:12px;
		font-family:Arial;
		text-align:left;
		color:#FFFFFF;
		background-color:#FFFFFF;
		padding:1px;
	}
	
	div.Logger div.Log
	{
		border-left:5px solid #EEEEEE;
		background-color:#444444;
		margin:3px;
	}
	
	div.Logger div.Log div.LogContent
	{
		padding:2px;
	}
	
	div.Logger div.Log div.LogStats
	{
		padding-bottom:2px;
		padding-right:2px;
		text-align:right;
	}
	
	div.Logger div.Log div.LogTitle
	{
		padding:2px;
		font-weight:bold;
		background-color:#000000;
		letter-spacing:1px;
	}
	
	div.Logger div.LogException
	{
		border-color:#FF4444;
	}
	
	div.Logger div.LogSQL
	{
		border-color:#BBBBFF;
	}
	
	div.Logger div.LogObject
	{
		border-color:#F6F600;
	}
	
	div.Logger div.LogMessage
	{
		border-color:#44FF44;
	}
	
	div.Logger div.LogError
	{
		border-color:#000000;
	}
	
	div.Logger div.LogError div.LogTitle
	{
		background-color:#BB0000;
	}
	
	div.Logger div.Log div.LogContent span.LogKeyword
	{
		color:#DDDDFF;
	}
</style>
<script type="text/javascript">
	function LoggerSH(id)
	{
		var id = \'Log\' + id;
		
		var title = document.getElementById(id + \'_title\');
		var content = document.getElementById(id + \'_content\');
		var stats = document.getElementById(id + \'_stats\');
		
		if(title && !title.onclick)
		{
			if(stats)   stats.style.display = \'none\';
			if(content) content.style.display = \'none\';
			
			if(stats && content)
			{
				title.onclick = function() {
					if(content.style.display == \'none\')
					{
						content.style.display = \'block\';
						stats.style.display = \'block\';
					}
					else
					{
						content.style.display = \'none\';
						stats.style.display = \'none\';
					}
				}
			}
		}
	}
</script>
		
<div class="Logger">';
		foreach(ENV::$collection as $l)
		{
			$string.= $l->get();
		}
		$string.= '</div>';
		
		return $string;
	}
}

?>