<?php

class ExcelCell
{
	protected $type;
	protected $value;
	protected $formula;
	protected $style;
	protected $name;
	protected $mergeCell;
	
	function __construct($type, $value, $class = null, $name = null)
	{
		$this->type = $type;
		$this->value = $value;
		
		if(!empty($class)) $this->setStyle($class);
		if(!empty($name)) $this->setName($name);
	}
	
	function setMergeAcross($nbCell)
	{
		$this->mergeCell = ($nbCell + 0);
	}
	
	function setStyle($style)
	{
		$this->style = $style;
	}
	
	function setFormula($formula)
	{
		$this->formula = $formula;
	}
	
	function setName($name)
	{
		$this->name = $name;
	}
	
	function output()
	{
		return '
    <Cell'.(

		$this->mergeCell ? ' ss:MergeAcross="'.$this->mergeCell.'"' : '' 
		
	).(
		
		$this->style ? ' ss:StyleID="'.ExcelWriter::t($this->style).'"' : ''
		
	).(
	
		$this->formula ? ' ss:Formula="'.ExcelWriter::t($this->formula).'"' : ''
		
	).'><Data ss:Type="'.ExcelWriter::t($this->type).'">'.ExcelWriter::t($this->value).'</Data>'.(
	
		$this->name ? '<NamedCell ss:Name="'.$this->name.'" />' : ''
	
	).'</Cell>';
	}
}

class ExcelRow
{
	protected $height;
	protected $style;
	protected $cells = array();
	
	public function __construct($height = null, $style = null)
	{
		$this->height = $height;
		$this->style = $style;
	}
	
	public function output()
	{
   		$string ='
   <Row ss:AutoFitHeight="0"'.(
   		
   		$this->height ? ' ss:Height="'.$this->height.'"' : ''
   		
   	).(
   		
   		$this->style ? ' ss:StyleID="'.$this->style.'"' : '').'>';
		
		foreach($this->cells as $cell)
		{
			$string.= $cell->output();
		}
		
		$string.='
   </Row>';
		
		return $string;
	}
	
	public function addCell($cell)
	{
		$this->cells[] = $cell;
	}
}

class ExcelWorksheet
{
	public $name;
	public $data;
	public $row;
	public $maxColumn;
	
	public $conditionalFormatting;
	public $columns;
	
	public $autoFilter;
	
	public $names;
	
	public $pageMarginTop;
	public $pageMarginBottom;
	public $pageMarginLeft;
	public $pageMarginRight;
	
	public $headerMargin;
	public $footerMargin;
	
	public $defaultRowHeight;
	
	function __construct($name)
	{
		$this->name = substr($name,0,31);
		$this->names = array();
		$this->rows = array();
		$this->maxColumn = 0;
		$this->row = -1;
		$this->col = 1;
		$this->autoFilter = false;
		
		$this->setPageMargins(0.75,0.7,0.75,0.7);
		$this->setFooterHeaderMargins(0.3,0.3);
		$this->setDefaultRowHeight(15);
	}
	
	function output()
	{
		$string = '
 <Worksheet ss:Name="'.ExcelWriter::t($this->name).'">';
		
		if($this->names)
		{
			$string.= '
  <Names>';
			foreach($this->names as $n)
			{
				$string.= $n->output();
			}
			
			$string.= '
  </Names>';
		}

		$string.= '
  <Table ss:ExpandedColumnCount="'.($this->maxColumn).'" ss:ExpandedRowCount="'.(Count($this->rows)).'" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="'.$this->defaultRowHeight.'">'.($this->columns ? $this->columns : '');
	
		foreach($this->rows as $row)
		{
			$string.= $row->output();
		}
		
		$string.='   
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="'.$this->headerMargin.'"/>
    <Footer x:Margin="'.$this->footerMargin.'"/>
    <PageMargins x:Bottom="'.$this->pageMarginBottom.'" x:Left="'.$this->pageMarginLeft.'" x:Right="'.$this->pageMarginRight.'" x:Top="'.$this->pageMarginTop.'"/>
   </PageSetup>
   <Selected/>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>'.
($this->conditionalFormatting ?
	'
  <ConditionalFormatting xmlns="urn:schemas-microsoft-com:office:excel">'.$this->conditionalFormatting.'
  </ConditionalFormatting>' : '').
($this->autoFilter ? '
  <AutoFilter x:Range="'.$this->autoFilter.'"
   xmlns="urn:schemas-microsoft-com:office:excel">
  </AutoFilter>' : '').'
 </Worksheet>';
		
		return $string;
	}
	
	function newRow($height = null, $style = null)
	{
		//echo 'new row<br />';
		$this->row++;
		$this->rows[] = new ExcelRow($height, $style);
		$this->col = 1;
	}
	
	function setDefaultRowHeight($height)
	{
		$this->defaultRowHeight = $height;
	}
	
	function setPageMargins($top, $right, $bottom, $left)
	{
		$this->pageMarginBottom = $bottom;
		$this->pageMarginLeft = $left;
		$this->pageMarginRight = $right;
		$this->pageMarginTop = $top;
	}
	
	function setFooterHeaderMargins($header, $footer)
	{
		$this->headerMargin = $header;
		$this->footerMargin = $footer;
	}
	
	function setConditionalFormatting($string)
	{
		$this->conditionalFormatting = $string;
	}
	
	function setColumns($string)
	{
		$this->columns = $string;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $name
	 * @param unknown_type $reference
	 * @param unknown_type $hidden
	 * @return ExcelNamedRange
	 */
	function setNamedRange($name, $reference, $hidden = true)
	{
		$namedRange = new ExcelNamedRange($name, $reference, $hidden);
		$this->names[$name] = $namedRange;
		
		return $namedRange;
	}
	
	function setAutofilter($range)
	{
		$this->autoFilter = $range;
	}
	
	function setValue($type, $value, $class = null, $name = null)
	{
		$cell = new ExcelCell($type, $value, $class, $name);
		//echo 'set val on '.$this->row.'<br />';
		$this->rows[$this->row]->addCell($cell);
		
		if($this->col > $this->maxColumn) $this->maxColumn = $this->col;
		
		$this->col++;
		
		return $cell;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $x
	 * @param unknown_type $y
	 * @param unknown_type $time
	 * @return ExcelCell
	 */
	function setDateTime($time, $class = null, $name = null)
	{
		$time = date(ExcelWriter::DateFormat, ($time + 0));
		return $this->setValue('DateTime', $time, $class, $name);
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $x
	 * @param unknown_type $y
	 * @param unknown_type $value
	 * @return ExcelCell
	 */
	function setString($value, $class = null, $name = null)
	{
		return $this->setValue('String', $value, $class, $name);
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $x
	 * @param unknown_type $y
	 * @param unknown_type $value
	 * @return ExcelCell
	 */
	function setNumber($value, $class = null, $name = null)
	{
		return $this->setValue('Number', ($value + 0), $class, $name);
	}
}

class ExcelWriter
{
	public $author;
	protected $worksheets = array();
	protected $style;
	protected $defaultStyle;
	
	public $lastAuthor;
	public $date;
		
	public $version = '12.00';
	
	const DateFormatH = 'Y-m-d\TH:i:s\Z';
	const DateFormat = 'Y-m-d\TH:i:s.000';
	
	static function t($v)
	{
		return ($v);
	}
	
	function setStyle($string)
	{
		$this->style = $string;
	}
	
	function setDefaultStyle($style)
	{
		$this->defaultStyle = $style;
	}
	
	function __construct($author)
	{
		$this->author = $author;
		$this->date = date(ExcelWriter::DateFormatH);
		$this->setDefaultStyle('
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat/>
   <Protection/>');
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $id
	 * @param unknown_type $name
	 * @return ExcelWorksheet
	 */
	function initWorksheet($id, $name)
	{
		$worksheet = new ExcelWorksheet($name);
		$this->worksheets[$id] = $worksheet;
		return $worksheet;
	}
	
	function outputAsXml()
	{
		ob_end_clean();
		
		$content = $this->output();
		
		header('Cache-control: private, must-revalidate');
		header('Content-Type: application/xml');
		header('Content-Length: '.strlen($content));
		
		echo $content;
		exit;
	}
	
	function outputAsFile($filename)
	{
		ob_end_clean();
		
		$content = $this->output();
		
		header('Cache-control: private, must-revalidate');
		header('Content-Type: application/octet-stream');
		//header('Content-Type: application/excel');
		
		//header('Content-Type: application/ms-excel');
		header('Content-Length: '.strlen($content));
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		
		echo $content;
		
		exit;
	}
	function output()
	{
		$string ='<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>'.ExcelWriter::t($this->author).'</Author>
  <LastAuthor>'.ExcelWriter::t($this->author).'</LastAuthor>
  <Created>'.ExcelWriter::t($this->date).'</Created>
  <LastSaved>'.ExcelWriter::t($this->date).'</LastSaved>
  <Company>'.ExcelWriter::t($this->author).'</Company>
  <Version>'.ExcelWriter::t($this->version).'</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>8670</WindowHeight>
  <WindowWidth>17175</WindowWidth>
  <WindowTopX>240</WindowTopX>
  <WindowTopY>60</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">'.$this->defaultStyle.'
  </Style>'.($this->style ? $this->style : '').'
 </Styles>';
		
		foreach($this->worksheets as $w) 
		{
			$string.=$w->output();
		}

		$string.='
</Workbook>';
		
		return utf8_encode($string);
	}
}

class ExcelNamedRange
{
	public $name;
	public $reference;
	public $hidden;
	
	public function __construct($name, $reference, $hidden = true)
	{
		$this->name = $name;
		$this->reference = $reference;
		$this->hidden = $hidden;
	}
	
	public function output()
	{
		$string = '
   <NamedRange ss:Name="'.$this->name.'"
    ss:RefersTo="'.$this->reference.'" ss:Hidden="'.($this->hidden ? '1' : '0').'"/>';
		
		return $string;
	}
}

?>