<?php

class EximporterFormat
{
	function encode($values, $options)
	{
		throw(new Exception('Encode non support� pour ce format'));
	}
	
	function decode($string, $options)
	{
		throw(new Exception('Decode non support� pour ce format'));
	}
}

class Eximporter
{
	const CSV = 1;
	const XML = 2;
	const JSON = 3;
	const TABBED_FILE = 4;
	const EXCEL_XML_SPREADSHEET = 5;
	const FLAT_FILE = 6;
	const XHTML = 7;
	
	/**
	 * Obtenir objet de transformation
	 *
	 * @param string $type
	 * @return EximporterFormat
	 */
	public function getFormatObject($type)
	{
		$obj = null;
		
		switch($type)
		{
			case Eximporter::CSV:
				REQUIRE_ONCE(SCRIPTPATH.'lib/eximporter/csv.php');
				$obj = new CSVEximporter();
				break;
			case Eximporter::XML:
				REQUIRE_ONCE(SCRIPTPATH.'lib/eximporter/xml.php');
				$obj = new XMLEximporter();
				break;
			case Eximporter::JSON:
				REQUIRE_ONCE(SCRIPTPATH.'lib/eximporter/json.php');
				$obj = new JSONEximporter();
				break;
			case Eximporter::TABBED_FILE:
				REQUIRE_ONCE(SCRIPTPATH.'lib/eximporter/tab.php');
				$obj = new TABEximporter();
				break;
			case Eximporter::EXCEL_XML_SPREADSHEET:
				REQUIRE_ONCE(SCRIPTPATH.'lib/eximporter/excel.php');
				$obj = new EXCELEximporter();
				break;
			case Eximporter::FLAT_FILE:
				REQUIRE_ONCE(SCRIPTPATH.'lib/eximporter/flat.php');
				$obj = new FLATEximporter();
				break;
			case Eximporter::XHTML:
				REQUIRE_ONCE(SCRIPTPATH.'lib/eximporter/xhtml.php');
				$obj = new XHTMLEximporter();
				break;
			default:
				throw(new Exception('Format non support�'));
		}
		
		return $obj;
	}
	
	public function process($string, $decode_type, $decode_options, $encode_type = null, $encode_options = null)
	{
		$values = $this->decode($string, $decode_type, $decode_options);
		
		if(!$encode_type) return $values;
		
		return $this->encode($values, $encode_type, $encode_options);
	}
	
	protected function decode($string, $type, $options)
	{
		$obj = $this->getFormatObject($type);
		
		if($obj)
		{
			return $obj->decode($string, $options);
		}
	}
	
	protected function encode($values, $type, $options)
	{
		$obj = $this->getFormatObject($type);
		
		if($obj)
		{
			return $obj->encode($values, $options);
		}
	}
}

?>