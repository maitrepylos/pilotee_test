<?php

class CSVEximporter
{
	function decode($string, $options)
	{
		$string = trim($string);
		$temp = tmpfile();
		fwrite($temp, $string);
		fseek($temp, 0);
		
		$separator = !isset($options['separator']) ? ';' : $options['separator'];
		$enclosure = !isset($options['enclosure']) ? '"' : $options['enclosure'];
		$escape = !isset($options['escape']) ? '\\' : $options['escape']; //ignor�
		$nbLineToIgnore = $options['ignored_lines'] + 0;
		$mappingField= $options['mapping'];
		
		$result = array();
		
		if($temp)
		{
			$line = 0;
			while (($data = fgetcsv($temp, 0, $separator, $enclosure)) !== FALSE)
			{
				$row = array();
			    $num = count($data);
			    
			    if($line >= $nbLineToIgnore)
			    {
			    	$row = array();
			    	$empty = true;
			    	
				    for ($c=0; $c < $num; $c++)
				    {
				    	$l = (isset($mappingField[$c]) ? $mappingField[$c] : $c);
				    	$row[$l] = $data[$c];
				        $empty = $empty && empty($data[$c]);
				    }
			    	
			    	if(!$empty) $result[] = $row;
			    }
			    
			    $line++;
			}
			
			fclose($temp);
		}
		
		return $result;
	}
	
	function encode($values, $options)
	{
		$separator = !isset($options['separator']) ? ';' : $options['separator'];
		$enclosure = !isset($options['enclosure']) ? '"' : $options['enclosure'];
		
		$temp = tmpfile();
		
		foreach($values as $line)
		{
			fputcsv($temp, $line, $separator, $enclosure);
		}
		
		fseek($temp, 0);
		
		$string = '';
		
		while (!feof($temp))
		{
  			$string.=fread($temp, 8192);
		}
		
		return $string;
	}
}

?>