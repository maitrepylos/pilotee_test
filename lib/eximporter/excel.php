<?php

class EXCELEximporter extends EximporterFormat
{
	function encode($values, $options)
	{
		REQUIRE_ONCE(SCRIPTPATH.'/lib/arrayexcelproxy.class.php');
		
		$e = new ArrayExcelProxy('auteur');
		$conf = new ArrayExcelConfig();
		
		$auteur = $options['auteur'];
		$sheet = $options['sheet'];
		
		if(isset($options['columns']))
		{
			foreach($options['columns'] as $c)
			{
				$conf->setColumn($c['header'], $c['type'], $c['width']);
			}
		}
		
		$e->loadArrayWorksheet(1,$sheet,$conf,$values);
		
		return $e->output();
	}
}

?>