<?php

class FLATEximporter extends EximporterFormat
{
	function encode($values, $options)
	{
		$filling_char = !isset($options['filling']) ? ' ' : $options['filling'];
		$linebreak = !isset($options['linebreak']) ? "\r\n" : $options['linebreak'];
		$string = '';
		
		foreach($values as $lines)
		{
			$nbV = 0;
			foreach($lines as $value)
			{
				$filling = !isset($options['columns'][$nbV]['filling']) ? $filling_char : $options['columns'][$nbV]['filling'];
				$size = !isset($options['columns'][$nbV]['size']) ? 20 : $options['columns'][$nbV]['size'];
				$alignRight = (!isset($options['columns'][$nbV]['align']) || $options['columns'][$nbV]['align'] != 'left');
	
				if(strlen($value) >= $size)
				{
					if($alignRight)
					{
						$vString = substr($value, strlen($value) - $size, $size);
					}
					else
					{
						$vString = substr($value, 0, $size);
					}
				}
				else
				{
					$padding = '';
					$diff = $size - strlen($value);
					for($x = 0; $x < $diff; $x++)
					{
						$padding.= $filling; 
					}
					if($alignRight)
					{
						$vString = $padding.$value;
					}
					else
					{
						$vString = $value.$padding;
					}
				}
				$string.= $vString;
				$nbV++;
			}
			
			$string.=$linebreak;
		}
		
		return $string;
	}
	
	function decode($string, $options)
	{
		$nbLineToIgnore = $options['ignored_lines'] + 0;
		$filling_char = !isset($options['filling']) ? ' ' : $options['filling'];
		$linebreak = !isset($options['linebreak']) ? "\r\n" : $options['linebreak'];
		
		$string = trim($string);
		
		$array = explode($linebreak, $string);
		$values = array();
		
		for($x = $nbLineToIgnore; $x < count($array); $x++)
		{
			$offset = 0;
			$line = array();
			foreach($options['columns'] as $c)
			{
				$filling = !isset($c['filling']) ? $filling_char : $c['filling'];
				$size = !isset($c['size']) ? 20 : $c['size'];
				$align = !isset($c['align']) ? 'mixed' : $c['align'] != 'left';
				
				$value = substr($array[$x], $offset, $size);
				$offset+=$size;
				
				switch($align)
				{
					case 'left':
						$value = ltrim($value, $filling);
						break;
					case 'right':
						$value = rtrim($value, $filling);
						break;
					default:
						$value = trim($value, $filling);
						break;
				}
				
				$line[] = $value;
			}
			
			$values[] = $line;
		}
		
		return $values;
	}
}

?>