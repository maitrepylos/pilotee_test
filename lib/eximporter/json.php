<?php

class JSONEximporter extends EximporterFormat
{
	function decode($string, $options)
	{
		return json_decode($string, true);
	}
	
	function encode($values, $options)
	{
		$utf8 = $options['utf8'];
		if($utf8) $encodedArray = array_map(utf8_encode, $rawArray);
		return json_encode($values);
	}
}

?>