<?php

class TABEximporter extends EximporterFormat
{
	function decode($string, $options)
	{
		$string = trim($string, "\r\n");
		$l = strlen($string);
		
		$separator = !isset($options['separator']) ? "\t" : $options['separator'];
		$nbLineToIgnore = $options['ignored_lines'] + 0;
		$mappingField= $options['mapping'];
		
		$result = array();
		$line = array();
		$field = '';
		for($x = 0; $x < $l; $x++)
		{
			$char = $string[$x]; 
			if($char == "\t")
			{
				$line[] = $field;
				$field = '';	
			}
			elseif($char == "\r" || $char == "\n")
			{
				$line[] = $field;
				$field = '';
				
				if($nbLineToIgnore > 0) $nbLineToIgnore--;
					else $result[] = $line;
				
				$line = array();
				
				$nChar = $string[$x + 1];
				if($nChar == "\r" || $nChar = "\n")
				{
					$x++;
				}
			}
			else
			{
				$field.=$char;
			}
		}
		
		$line[] = $field;
		$field = '';
		
		$result[] = $line;
		$line = array();
		
		return $result;
	}
	
	function encode($values, $options)
	{
		$separator = $options['separator']; if(empty($separator)) $separator = "\t";
		$linebreak = $options['linebreak']; if(empty($linebreak)) $linebreak = "\r\n";
		
		$string = '';
		
		foreach($values as $line)
		{
			foreach($line as $value)
			{
				$string.= $value.$separator;
			}
			
			$string = substr($string,0,-(strln($separator))).$linebreak;
		}
		
		$string = substr($string,0,-(strln($linebreak)));
		
		return $string;
	}
}


?>