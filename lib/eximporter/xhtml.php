<?php

class XHTMLEximporter extends EximporterFormat
{
	function encode($values, $options)
	{
		$string = '
<table'.($options['class'] ? ' class="'.htmlentities($options['class']).'"' : '').'>
	<thead>
		<tr>';
		
		foreach($options['columns'] as $k => $c)
		{
			$string.='
			<th'.($c['class'] ? ' class="'.htmlentities($c['class']).'_header"' : '').'>'.htmlentities($c['header']).'</th>';
		}
		
		$string.='
		</tr>
	</thead>
	<tbody>';
		foreach($values as $line)
		{
			$string.= '
		<tr>';
			
			foreach($options['columns'] as $k => $c)
			{
				$string.= '
			<td'.($c['class'] ? ' class="'.htmlentities($c['class']).'"' : '').'>'.htmlentities($line[$k]).'</td>';
			}
			
			$string.= '
		</tr>';
		}
		
		$string.='
	</tbody>
</table>';
		
		return $string;
	}
}

$e = new XHTMLEximporter();

$xhtml_options = array(
	'class' => 'tableau',
	'columns' => array(
		array( 'class' => 'champs_1', 'header' => 'Champs 1' ),
		array( 'header' => 'Champs 2' ),
		array( 'header' => 'Champs 3' )
	)
);

$values = array(
	array(90, 100, 200),
	array('H�bergement', 'Mutualis�', 'D�di�'),
	array('test')
);

$r = $e->encode($values, $xhtml_options);

echo '<pre>'.htmlentities(print_r($r, true)).'</pre>';

?>