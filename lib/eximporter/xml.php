<?php

class XMLEximporter extends EximporterFormat
{
	function decode($string, $options)
	{
		$string = trim($string);
		$utf8 = $options['utf8'];
		$tagName = $options['tagname'];
		$getElements = $options['elements'];
		$getAttributes = $options['attributes'];
		
		$doc = new DOMDocument('1.0', 'utf-8');
		
		$result = array();
		
		if(@$doc->loadXml($string))
		{
			$root = $doc->firstChild;
			$elements = $root->getElementsByTagName($tagName);
			
			for($x = 0; $x < $elements->length; $x++)
			{
				$result[$x] = array();
				$element = $elements->item($x);
				
				if($getElements && $element->hasChildNodes())
				{
					foreach($element->childNodes as $child)
					{
						if ($child != null && $child->nodeType == XML_ELEMENT_NODE)
						{
							$value = $child->nodeValue;
							if($utf8) $value = utf8_decode($value);
							
							$result[$x][$child->tagName] = $value;
						}
					}
				}
				
				/*
				if($getAttributes && $element->hasAttributes())
				{
					foreach($element->childNodes as $child)
					{
						if ($child != null && $child->nodeType == XML_ELEMENT_NODE)
						{
							$value = $child->nodeValue;
							if($utf8) $value = utf8_decode($value);
							
							$result[$x][$child->tagName] = $value;
						}
					}
				}
				*/
			}
		}
		else
		{
			
		}
		
		return $result;
	}
	
	function encode($values, $options)
	{
		$doc = new DOMDocument('1.0', 'utf-8');
		
		$utf8 = $options['utf8'];
		$cdata = $options['cdata'];
		$root = $options['root']; if(empty($root)) $root = 'root';
		$tagName = $options['tagname']; if(empty($tagName)) $tagName = 'element';
		$setAttributes = (isset($options['attributes']) && $options['attributes']);
		
		$doc = new DOMDocument('1.0', 'utf-8');
		
		$root = $doc->appendChild(new DOMElement($root));
		
		
		foreach($values as $v)
		{
			$el = $root->appendChild(new DOMElement($tagName));
			
			if(!$setAttributes)
			{
				foreach($v as $k => $c)
				{
					if(is_numeric($k)) $k = 'element'.$k;
					$elc = $el->appendChild(new DOMElement($k));
					
					if($utf8) $c = utf8_encode($c);
					if($cdata)
					{
						$txt = $doc->createCDATASection($c);
						$elc->appendChild($txt);
					}
					else
					{
						$elc->nodeValue = $c;
					}
				}
			}
			else
			{
				foreach($v as $k => $c)
				{
					if(is_numeric($k)) $k = 'attribute'.$k;
					if($utf8) $c = utf8_encode($c);
					$el->setAttribute($k, $c);
				}
			}
		}
		
		return $doc->saveXML();
	}
}

?>