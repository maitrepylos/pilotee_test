<?php

require_once(SCRIPTPATH."lib/pdf/phptopdf.domain.class.php");

abstract class FormPdf extends phpToPDF
{
	public $appearances;
	protected $softBreak = false;
	
	public function iAddPage($orientation = '')
	{
		if($this->y != $this->tMargin) $this->AddPage($orientation);
	}
	
	public function __construct($title, $subject, $author)
	{
		parent::__construct('P','mm','A4'); //portrait millimètre A4
		
		$this->appearances = array(
			'checkbox' => array(
				'width' => 2,
				'height' => 2,
				'drawcolor' => array(0,0,0),
				'fillcolor' => array(10,20,140),
				'emptycolor' => array(240,240,240)
			),
			'field' => array(
				'font' => 'Arial',
				'style' => '',
				'size' => '8',
				'color' => array(10,20,140),
				'bgcolor' => array(250,250,250),
				'drawcolor' => array(180,180,180),
				'border' => true
			),
			'label' => array(
				'align' => 'R'
			),
			'b' => array(
				'style' => 'B'
			),
			'u' => array(
				'style' => 'U'
			),
			'ub' => array(
				'style' => 'UB'
			),
			'tab_header' => array(
				'T_COLOR' => array(0,0,0),
				'T_SIZE' => 9,
				'T_FONT' => 'Arial',
				'T_ALIGN' => 'C',
				'V_ALIGN' => 'M',
				'T_TYPE' => 'B',
				'LN_SIZE' => 4.5,
				'BG_COLOR' => array(200,200,200),
				'BRD_COLOR' => array(120,120,120),
				'BRD_SIZE' => 0.3,
				'BRD_TYPE' => 1,
				'BRD_TYPE_NEW_PAGE' => '',
			),
			'tab_content' => array(
				'T_COLOR' => array(0,0,0),
				'T_SIZE' => 8,
				'T_FONT' => 'Arial',
				'T_ALIGN' => 'L',
				'V_ALIGN' => 'M',
				'T_TYPE' => '',
				'LN_SIZE' => 5,
				'BG_COLOR' => array(255,255,255),
				'BRD_COLOR' => array(120,120,120),
				'BRD_SIZE' => 0.1,
				'BRD_TYPE' => '1',
				'BRD_TYPE_NEW_PAGE' => '',
			)
		);
		
		$this->SetDisplayMode("real");
		$this->SetTopMargin(21.5);
		$this->SetLeftMargin(25);
		$this->SetRightMargin(25);
		$this->SetTextColor('0,0,0');
		$this->SetAuthor($author);
		$this->SetCreator($author);
		$this->SetTitle($title);
		$this->SetSubject($subject);
		$this->startPageNums();
		$this->_numPageNum = 0;
	}
	
	abstract protected function finalize();
	
	protected function addAppearance($id, $array)
	{
		$this->appearances[$id] = $array;
	}
	
	protected function getAppearance($type)
	{
		if($type == 'checkbox')
		{
			$a = $this->appearances[$type];
		}
		else
		{
			$a = array(
				'align' => '',
				'font' => 'Arial',
				'style' => '',
				'size' => '8',
				'color' => '0,0,0',
				'bgcolor' => array(255,255,255),
				'drawcolor' => '0,0,0',
				'border' => false
			);
			
			if(!empty($type) && isset($this->appearances[$type]))
			{
				$f = $this->appearances[$type];
				$a = $f + $a;
			}
		}
		return $a;
	}
	
	protected function checkbox($checked)
	{
		$a = $this->getAppearance('checkbox');
		
		$this->y++;
		$this->SetDrawColor($a['drawcolor'][0],$a['drawcolor'][1],$a['drawcolor'][2]);
		
		if($checked)
		{
			$this->SetFillColor($a['fillcolor'][0],$a['fillcolor'][1],$a['fillcolor'][2]);
		}
		else
		{
			$this->SetFillColor($a['emptycolor'][0],$a['emptycolor'][1],$a['emptycolor'][2]);
		}
		
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(100);
		$this->Cell($a['width'],$a['height'],'',1,false,'',1);
		$this->x++;
		$this->y--;
	}
	
	protected function textarea($w, $h, $texte, $allowEndingBreak = true)
	{
		$x = $this->x;
		$y = $this->y;
		$page = $this->page;
		$a = $this->getAppearance('field');
		$this->SetTextColor($a['color'][0], $a['color'][1], $a['color'][2]);
		$this->SetFillColor($a['bgcolor'][0], $a['bgcolor'][1], $a['bgcolor'][2]);
		$this->SetDrawColor($a['drawcolor'][0], $a['drawcolor'][1], $a['drawcolor'][2]);
		$this->SetFont($a['font'], $a['style'], $a['size']);
		$this->MultiCell($w,4.5,$texte);
		$newPage = $this->page;
		$newX = $this->x;
		$newY = $this->y;

		if($page == $newPage && $h >= $newY - $y)
		{
			$this->x = $x;
			$this->y = $y;
			$this->MultiCell($w,$h,'', $a['border'], 'J', 1);
			$newX = $this->x;
			$newY = $this->y;
			$this->x = $x;
			$this->y = $y;
			$this->MultiCell($w,4.5,$texte);
			$this->x = $newX;
			$this->y = $newY + 1;
		}
		else
		{
			$this->softBreak = true;
			$tX = $x;
			$tY = $y;
			for($p = $page; $p <= $newPage; $p++)
			{
				$this->page = $p;
				
				$this->SetTextColor($a['color'][0], $a['color'][1], $a['color'][2]);
				$this->SetFillColor($a['bgcolor'][0], $a['bgcolor'][1], $a['bgcolor'][2]);
				$this->SetDrawColor($a['drawcolor'][0], $a['drawcolor'][1], $a['drawcolor'][2]);
				$this->x = $tX;
				$this->y = $tY;
				$h = $this->h - ($this->y + $this->bMargin);
				$this->MultiCell($w,$h,'', $a['border'], 'J', 1);
				$tX = $tX;
				$tY = $this->tMargin;
			}

			$this->page = $page;
			$this->x = $x;
			$this->y = $y;
			$this->FontFamily = null; //gros hack pour contourner bug font
			$this->SetFont($a['font'], $a['style'], $a['size']);
			$this->MultiCell($w,4.5,$texte,0,'J',0,true);
			$this->softBreak = false;
			
			if($allowEndingBreak) $this->AddPage();
		}
	}
	
	protected function wCadre($num, $title, $level = 0)
	{
		$fill = true;
		if($level == 1)
		{
			$this->SetTextColor(0, 0, 0);
			$this->SetFillColor(220, 220, 220);
			$this->SetFont('Arial', 'B', 9);
			$height = 6;
			$paddingTop = 5;
			$paddingBottom = 2;
		}
		elseif($level == 2)
		{
			$this->SetTextColor(0, 0, 0);
			$fill = false;
			$this->SetFont('Arial', 'B', 8.5);
			$height = 5;
			$this->y+=2;
			$paddingBottom = 2;
			$this->SetDrawColor(150);
			$x = $this->x;
			$this->Cell(161,$height,'',1);
			$this->x = $x;
		}
		elseif($level == 3)
		{
			$this->SetTextColor(0, 0, 0);
			$fill = false;
			$this->SetFont('Arial', 'B', 8.5);
			$height = 5;
			$paddingTop = 2;
			$paddingBottom = 2;
		}
		else
		{
			$this->SetTextColor($this->color2[0],$this->color2[1],$this->color2[2]);
			$this->SetFillColor($this->color1[0],$this->color1[1],$this->color1[2]);
			$this->SetFont('Arial', 'B', 10);
			$height = 6;
			$paddingTop = 1;
			$paddingBottom = 2;
		}
		
		$this->y+=$paddingTop;
		$this->Cell(20, $height, $num, false, false, 'R', $fill);
		$this->Cell(4, $height, '', false, false, '', $fill);
		$this->MultiCell(137, $height, $title, false, '', $fill);
		$this->y+=$paddingBottom;
	}
	
	protected function w($w,$h = 4.5,$texte, $simple = true, $appearance = '', $newLine = false)
	{
		if(!$h) $h = 4.5;
		$a = $this->getAppearance($appearance);
		$this->SetTextColor($a['color'][0], $a['color'][1], $a['color'][2]);
		$this->SetFillColor($a['bgcolor'][0], $a['bgcolor'][1], $a['bgcolor'][2]);
		$this->SetDrawColor($a['drawcolor'][0], $a['drawcolor'][1], $a['drawcolor'][2]);
		$this->SetFont($a['font'], $a['style'], $a['size']);
		if($simple)
		{
			$this->Cell($w, $h, $texte, $a['border'], false, $a['align'], 1);
			$this->x++;
		}
		else 
		{
			$this->MultiCell($w, $h, $texte, $a['border'], $a['align'], 1);
			$this->y++;
		}
	}
	
	protected function br()
	{
		$this->Ln();
		$this->y++;
	}
	
	protected function tab($contenuHeader, $contenuTableau)
	{
		$proprietesTableau = array(
			'TB_ALIGN' => 'L'
		);
		
		$proprieteHeader = $this->getAppearance('tab_header');
		
		$proprieteContenu = $this->getAppearance('tab_content');
		
		$this->drawTableau($this, $proprietesTableau, $proprieteHeader, $contenuHeader, $proprieteContenu, $contenuTableau);
	}

	public function toString()
	{
		return $this->Output('','S');
	}
	
	public function generate()
	{
		$this->_generate();
		$this->finalize();
	}
	
	public function toBrowser($filename)
	{
		$content = $this->toString();
		header('Cache-control: private, must-revalidate'); 
		header('Content-Type: application/octet-stream'); 
		header('Content-Length: '.strlen($content)); 
		header('Content-Disposition: attachment; filename="'.($filename).'"'); 
		
		echo $content; exit;
	}
	
	abstract protected function _generate();
}

?>