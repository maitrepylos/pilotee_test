<?php

/**
 * @copyright Cediti
 * @package Libs
  */

/**
 * Classe g�n�rique d'acc�s � un serveur FTP
 * @version 1.0
 */
class FTPHelper
{
	static public $self;
	
	private $host;
	private $login;
	private $password;
	private $passiveMode;
	private $port;
	private $timeout;
	private $useSSL;
	private $mode;
	private $root;
	
	private $connection;
	
	/**
	 * Obtenir la derni�re instance de la classe
	 *
	 * @return FTPHelper Instance de la classe (ou null)
	 */
	static function getLastInstance()
	{
		return FTPHelper::$self;
	}
	
	/**
	 * Instancier un objet d'acc�s au FTP
	 *
	 * @param string Host
	 * @param string Identifiant
	 * @param string Mot de passe
	 * @param bool Mode passif
	 * @param int Port (defaut 21)
	 * @param int Timeout en seconde (defaut 90)
	 * @param bool Connection SSL (defaut false)
	 * @param bool Mode de transfert binaire (defaut false)
	 */
	public function __construct($host, $login, $password, $root, $passiveMode, $port = 21, $timeout = 90, $useSSL = false, $useBinaryMode = true)
	{
		$this->root = $root;
		$this->passiveMode = ($passiveMode === true ? true : false);
		$this->host = $host;
		$this->login = $login;
		$this->password = $password;
		$this->passiveMode = $passiveMode;
		$this->port = $port;
		$this->timeout = $timeout;
		$this->useSSL = ($useSSL === true ? true : false);
		$this->mode = ($useBinaryMode === true ? FTP_BINARY : FTP_ASCII);
		
		FTPHelper::$self = $this;
	}
	
	/**
	 * Etablir la connection au FTP
	 *
	 */
	public function connect()
	{
		if(!isset($this->connection) || $this->connection == null)
		{
			if($this->useSSL)
			{
				$this->connection = @ftp_ssl_connect($this->host, $this->port, $this->timeout);
			}
			else
			{
				$this->connection = @ftp_connect($this->host, $this->port, $this->timeout);
			}
			
			if($this->connection != null)
			{
				ftp_login($this->connection, $this->login, $this->password);
				
				ftp_pasv($this->connection, $this->passiveMode);
				
				$this->chdir();
			}
		}
		
		return $this->connection;
	}
	
	/**
	 * Fermer la connection au FTP
	 *
	 */
	public function close()
	{
		if(isset($this->connection) && $this->connection != null)
		{
			ftp_close($this->connection);
			unset($this->connection);
		}
	}
	
	/**
	 * Cr�er un fichier sur le serveur FTP en en sp�cifiant le contenu en string
	 *
	 * @param string Contenu
	 * @param string Chemin du fichier sur le serveur
	 * @return bool Succ�s
	 */
	public function putString($string, $remote_file)
	{
		//cr�e fichier temporaire
		$tmp = tmpfile();
	    fwrite($tmp, $string);
	    rewind($tmp);
	    
	    $result = ftp_fput($this->connection, $remote_file, $tmp, $this->mode); //upload fichier temp
	    fclose($tmp); //d�truit fichier temp
	    
	    return $result;
	}
	
	/**
	 * Copier un fichier local sur le serveur FTP
	 *
	 * @param string Chemin du fichier local
	 * @param string Chemin du fichier sur le serveur
	 * @return bool Succ�s
	 */
	public function putFile($local_file, $remote_file)
	{
		$result = ftp_put($this->connection, $remote_file, $local_file, $this->mode);
		
		return $result;
	}
	
	/**
	 * Copier un fichier du serveur FTP en local
	 *
	 * @param string Chemin du fichier sur le serveur
	 * @param string Chemin du fichier local
	 * @return bool Succ�s
	 */
	public function getFile($remote_file, $local_file)
	{
		$result = ftp_get($this->connection, $local_file, $remote_file, $this->mode);
		
		return $result;
	}
	
	/**
	 * Obtenir le contenu d'un fichier du serveur FTP sous forme de string
	 *
	 * @param string Chemin du fichier sur le serveur
	 * @return string Contenu du fichier (false en cas d'�chec)
	 */
	public function getString($remote_file)
	{
		//cr�e un fichier temporaire
		$tmp = tmpfile();
		
		$result = ftp_fget($this->connection, $tmp, $remote_file, $this->mode);
		
		if($result)
		{
			fseek($tmp,0);
			$content = stream_get_contents($tmp);
			fclose($tmp);
			return $content;
		}		
		else
		{
			fclose($tmp);
			return false;
		}
	}
	
	/**
	 * Changer le r�pertoire courant sur le serveur FTP
	 *
	 * @param string R�pertoire
	 * @return bool Succ�s
	 */
	public function chdir($dir = null)
	{
		if ($dir == null) $dir = $this->root;
		return ftp_chdir($this->connection,$dir);
	}
	
	/**
	 * Obtenir le r�pertoire courant
	 *
	 * @return string R�pertoire courant
	 */
	public function getCurrentDir()
	{
		return ftp_pwd($this->connection);
	}
	
	
	/**
	 * Supprimer un fichier du serveur FTP
	 *
	 * @param string Fichier sur le serveur
	 * @return bool Succ�s
	 */
	public function deleteFile($remote_file)
	{
		return ftp_delete($this->connection, $remote_file);
	}
	
	/**
	 * Renommer un fichier sur le serveur FTP
	 *
	 * @param string Fichier sur le serveur
	 * @param string Nouveau nom
	 * @return bool Succ�s
	 */
	public function renameFile($remote_file, $new_name)
	{
		return ftp_rename($this->connection, $remote_file, $new_name);
	}
	
	/**
	 * Obtenir le listing des objets contenus dans le dossier, attention le r�sultat peut varier selon les serveurs.
	 *
	 * @return array listing
	 */
	public function listContent()
	{
		$result = ftp_nlist($this->connection,ftp_pwd($this->connection));
		$result = (is_array($result) ? $result : array()); //pour certain serveur la fonction renvoit false plut�t qu'un array vide
		$files = array();
		foreach($result as $key => $f)
		{
			//supprime les �ventuels r�pertoires parasite dans le nom de fichier
			$f = explode('/', $f);
			$f = $f[(Count($f) - 1)];
			
			if($f != '.' && $f != '..')
			{
				$files[] = $f;
			}
		}
		
		return $files;
	}
}

?>