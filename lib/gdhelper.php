<?php

class GDHelper
{
	static function ResizeToFixed($imageSrc, $fixedWidth, $fixedHeight, $entire = false)
	{
		$width = imagesx($imageSrc);
		$height = imagesy($imageSrc);
		
		if(!$entire)
		{
			$rel = ($width > $height ? $height : $width);
		}
		else
		{
			$rel = ($width < $height ? $height : $width);
		}
		
		$inter = imagecreatetruecolor($rel, $rel);
		
		imagecopy($inter, $imageSrc, 0, 0, floor(($width / $rel) / 2), floor(($height / $rel) / 2), $rel, $rel);
		$thumb = imagecreatetruecolor($fixedWidth, $fixedHeight);
		
		imagecopyresampled($thumb,$inter,0,0,0,0,$fixedWidth,$fixedHeight,$rel,$rel);
		unset($inter);
		return $thumb;
	}
}

?>