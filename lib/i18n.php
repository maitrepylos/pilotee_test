<?php
/**
 * String Provider
 */
class SP
{
	/**
	 * Array associatif contenant les cha�nes de caract�res
	 *
	 * @var Array
	 */
	protected static $arr;
	
	/**
	 * Array d�finissant les sections d�j� charg�es
	 *
	 * @var Array
	 */
	protected static $loadedSection;

	/**
	 * Charger les traductions d'une section
	 *
	 * @param String Nom de la section pour laquelle il faut r�cup�rer les cha�nes de caract�res
	 * @param String Langue (co) pour laquelle il faut r�cup�rer les cha�nes de caract�res
	 * @param String Chemin d'acc�s de base
	 */
	public static function load($section, $lang, $forMail = false, $basepath = I18NBASEPATH)
	{
		$section = strtolower($section);
		$lang = strtolower($lang);
		
		if (empty(SP::$loadedSection[$section]) || SP::$loadedSection[$section] != $lang)
		{
			$node = new DOMDocument('1.0','utf-8');
			if (@$node->load($basepath.$section.'.xml'))
			{
				$messages = $node->getElementsByTagName('message');

				foreach ($messages as $message)
				{
					$id = $message->getAttribute('id');
					$trads = $message->getElementsByTagName('traduction');
					
					foreach ($trads as $key => $trad)
					{
						$tradLang = $trad->getAttribute('lang');
						if ($lang == '*' || $tradLang == $lang)
						{
							SP::set($section, ($lang == '*' ? $id.'_'.$tradLang : $id), ($forMail ? utf8_decode($trad->nodeValue) : nl2br(utf8_decode($trad->nodeValue))));
							continue;
						}
					}
				}

				SP::$loadedSection[$section] = $lang;
			}
			else
			{
				SP::log('I/O','Translation file for section "'.$section.'" couldn\'t be loaded');
			}
		}
	}

	function unload($section)
	{
		unset(SP::$loadedSection[$section]);
	}
	
	/**
	 * D�finir une cha�ne de caract�res
	 *
	 * @param String Clef
	 * @param String Cha�ne de caract�res
	 */
	public static function set($section, $id, $value)
	{
		SP::$arr[$id] = $value;
	}

	/**
	 * Obtenir une cha�ne de caract�res
	 *
	 * @param String Clef
	 * @return String Cha�ne de caract�res
	 */
	public static function get($id, $lang = null)
	{
		if($lang)
		{
			if(!empty(SP::$arr[$id.'_'.$lang]))
			{
				return SP::$arr[$id.'_'.$lang];
			}
		}
		
		if(empty(SP::$arr[$id]))
		{
			if(I18NDEBUG)
			{
				SP::log('MissingTranslation','Missing translation for "'.$id.'"');
			}
			return 'Missing translation for &lt;'.$id.'&gt;';
		}
		else
		{
			return SP::$arr[$id];
		}
	}

	/**
	 * Afficher le contenu du tableau associatif des cha�nes de caract�res
	 *
	 */
	public static function print_raw()
	{
		return '
<pre><h2>********* LoadedSections *********</h2>
'.print_r(SP::$loadedSection,true).'

<h2>********* Strings *********</h2>
'.print_r(SP::$arr,true).'
</pre>';
	}

	/**
	 * Enregistrer un log
	 *
	 * @param String Type
	 * @param String Message
	 */
	private static function log($type,$message)
	{
		REQUIRE_ONCE(SCRIPTPATH.'model/log_database.model.class.php');
		$DB = new LogDatabase();

		$DB->insertLog('I18N',$type,$message);
	}
	
	public static function checkForErrors($sections, $lang, $basepath = I18NBASEPATH)
	{	
		foreach($sections as $section)
		{
			$node = new DOMDocument('1.0','utf-8');
			if (@$node->load($basepath.$section.'.xml'))
			{
				$messages = $node->getElementsByTagName('message');

				foreach ($messages as $message)
				{
					$id = $message->getAttribute('id');
					if(isset($array[$id][$section])) echo '<span style="font-weight:bold; color:red">Doublons &gt;&gt; Section : '.$section.' | String : '.$id.'</span><br>';
					elseif(isset($array[$id]['common'])) echo '<span style="font-weight:bold; color:blue">Doublons (en common) &gt;&gt; Section : '.$section.' | String : '.$id.'</span><br>';
					elseif(isset($array[$id])) echo '<span style="color:#888888">Utilis� dans plusieures sections &gt;&gt; String : '.$id.' | Sections : '.print_r($array[$id], true).' | Sections en chargement : '.$section.'</span><br>';
					else $array[$id][$section] = true;
					continue;
				}
			}
			else
			{
				echo '<span style="font-weight:bold; color:red; background:black;">Translation file for section "'.$section.'" couldn\'t be loaded</span><br>';
			}
		}
	}
	
	public static function getIncompletes($sections, $basepath = I18NBASEPATH, $showAll = false)
	{
		foreach($sections as $section)
		{
			echo '<strong style="font-size:2em; color:#777777;">'.$section.'</strong><br>';
			$node = new DOMDocument('1.0','utf-8');
			if (@$node->load($basepath.$section.'.xml'))
			{
				$messages = $node->getElementsByTagName('message');

				foreach ($messages as $message)
				{
					$id = $message->getAttribute('id');
					$trads = $message->getElementsByTagName('traduction');
					$string = '&nbsp;<strong style="color:#000000;">'.$id.'</strong><br>';
					$empty = false;
					
					foreach($trads as $trad)
					{
						$lang = strtoupper($trad->getAttribute('lang'));
						if (true) //afin de retirer l'anglais
						{
							$content = utf8_decode($trad->nodeValue);
							
							if(empty($content))
							{
								//$content = '_________________________';
								$empty = true;
								$style = 'font-weight:normal; color:blue;'; //font-weight:bold;
							}
							else
							{
								$style = 'font-weight:normal; color:#888888; color:#000000;';
							}
							
							$string.= '&nbsp;&nbsp;&nbsp;<span style="'.$style.'">'.$lang.' : '.htmlentities($content).'</span></span><br>';
						}
					}
					
					if($empty || $showAll) echo $string;
				}
			}
			else
			{
				echo '<span style="font-weight:bold; color:red; background:black;">Translation file for section "'.$section.'" couldn\'t be loaded</span><br>';
			}
		}
	}
}
?>