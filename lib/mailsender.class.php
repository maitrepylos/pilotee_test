<?php

/**
 * @package Libs
 * @subpackage Commons
 * @copyright Cediti 
 * @version 0.5
 * @origin WebEAD
 * @used_in WebEAD
 */

/**
 * Classe d'envoi de mail
 * 
 */
class MailSender
{
	/**
	 * Initialiser un mail
	 *
	 * @param string Destinataire
	 * @param string Objet du mail
	 * @param string Corps du mail en simple texte
	 * @param string Corps du mail en html (defaut : version html de plainbody)
	 * @param string Adresse de l'envoyeur (ignor� sur les plateformes autres que windows)
	 * @param string Nom de l'envoyeur
	 */
	static public function initialize($to, $subject, $plainBody, $htmlBody, $fromaddress, $fromname = null)
	{
		MailSender::$to = $to;
		MailSender::$plainBody = $plainBody;
		MailSender::$htmlBody = $htmlBody;
		MailSender::$subject = $subject;
		MailSender::$fromaddress = $fromaddress;
		MailSender::$fromname = $fromname;
		MailSender::$files = array();
		MailSender::$isInitialized = true;
	}
	
	static protected $isInitialized = false;
	static protected $to;
	static protected $plainBody;
	static protected $htmlBody;
	static protected $subject;
	static protected $fromaddress;
	static protected $fromname;
	static protected $files;
	
	/**
	 * Ajouter un fichier en attachement en en sp�cifiant le contenu ou le chemin sur le serveur
	 *
	 * @param string Nom / chemin du fichier
	 * @param string Contenu du fichier (si vide, assume qu'il s'agit d'un fichier sur le serveur)
	 * @param string Type mime du fichier (defaut : application/octet-stream)
	 */
	static public function addFile($filename, $content = null, $type = null)
	{
		if(empty($content))
		{
			try
			{
				$content = file_get_contents($filename);
			}
			catch(Exception $e)
			{
				$content = 'Le fichier n\'a pas pu �tre r�cup�r�.';
			}
			
			$x = explode('/',$filename);
			$filename = $x[(Count($x) - 1)]; 
		}
		
		if(empty($type)) $type = 'application/octet-stream';
		
		$file = array(
			'name' => $filename,
			'content' => $content,
			'type' => $type
		);
		
		MailSender::$files[] = $file;
	}
	
	/**
	 * Envoyer le mail
	 *
	 * @param bool Indique s'il faut afficher le mail en raw pour debug
	 * @return bool R�ussite de l'op�ration
	 */
	static public function send($debug = false)
	{
		if(MailSender::$isInitialized)
		{
			$mime_boundary=md5(time());
			$mime_boundary2=md5(time()+1);
			$to = MailSender::$to;
			$subject = MailSender::$subject;
			$fromaddress = MailSender::$fromaddress;
			$fromname = MailSender::$fromname;
			$plainBody = MailSender::$plainBody;
			$htmlBody = MailSender::$htmlBody;
			if(empty($htmlBody)) $htmlBody = '<p>'.nl2br(htmlentities($plainBody, ENT_SUBSTITUTE,'ISO-8859-15')).'</p>';
/*if($to == 'xavier.bogaert@cediti.be') 
{
echo '<pre>';echo 'plainBody='.$plainBody;echo '</pre>';
echo '<pre>';echo 'htmlBody='.$htmlBody;echo '</pre>';
echo '<pre>';echo 'htmlBodytest='.htmlentities($plainBody, ENT_SUBSTITUTE, 'ISO-8859-15');echo '</pre>';
echo 'zz';exit;
}*/
			
			$files = MailSender::$files;
			
			// headers normaux
			$headers =  "From: ".(!empty($fromname) ? $fromname.' ' : '')."<".$fromaddress.">\n";
			$headers .= "Reply-To: ".$fromname." <".$fromaddress.">\n";
			$headers .= "Return-Path: ".$fromname." <".$fromaddress.">\n";
			$headers .= "Message-ID: <".time()."-".$fromaddress.">\n";
			$headers .= "X-Mailer: PHP v".phpversion()."\n";
			
			// boundary
			$headers .= 'MIME-Version: 1.0'."\n";
			$headers .= "Content-Type: multipart/mixed; boundary=".$mime_boundary."\n\n";
			//premi�re partie de l'email
			
			$msg =	"This is a multi-part message in MIME format.\n\n";
			$msg .= "--".$mime_boundary."\n";
			
			$msg .= "Content-Type: multipart/alternative; boundary=".$mime_boundary2."\n\n";
			
			if(!empty($plainBody))
			{
				$msg .= "Content-Type: text/plain; charset=iso-8859-1"."\n";
				$msg .= "Content-Transfer-Encoding: 8bit"."\n\n";
				$msg .= $plainBody."\n\n";  
			}
					
			$msg .= "--".$mime_boundary2."\n";
			$msg .= "Content-Type: text/html; charset=iso-8859-1"."\n";
			$msg .= "Content-Transfer-Encoding: 8bit"."\n\n";
			$msg .= $htmlBody."\n\n";  
			
			
			//ajoute un fichier en attachement si un fichier a �t� donn�
	
			$msg .= "--".$mime_boundary2."--\n\n";
			
			if(Count($files) > 0)
			{
				foreach($files as $file)
				{
					if ($file != null)
					{
						$msg .= "--".$mime_boundary."\n";
						$msg .= "Content-Type: ".$file['type']."; name=".$file['name']."\n";
						$msg .= "Content-Transfer-Encoding: base64"."\n";
						$msg .= "Content-Disposition: attachment; filename=".$file['name']."\n\n"; //
						$msg .= chunk_split(base64_encode($file['content']))."\n\n";
					}
				}
			}
			
			// fin de l'email
			$msg .= "--".$mime_boundary."--"."\n\n";
				
			if($debug)
			{
				echo nl2br(htmlentities($headers));
				echo nl2br(htmlentities($msg));
			}
			
			// envoi du mail
			ini_set('sendmail_from',$fromaddress);  // tente de modifier temporairement l'adresse d'envoi parametr�e dans la config
			$mail_sent = @mail($to, $subject, $msg, $headers);
			ini_restore('sendmail_from');
		}
		else
		{
			$mail_sent = false;
		}
		
		return $mail_sent;
	}
	
	/**
	 * Envoyer un mail (ancienne fonction)
	 *
	 * @deprecated deprecated
	 * @param String Adresse du destinataire
	 * @param String Corps du mail
	 * @param String Sujet du mail
	 * @param String Adresse de l'envoyeur (ignor� sur les plateforme autre que windows !)
	 * @param String Nom de l'envoyeur
	 * @param String Contenu du fichier
	 * @return Bool Indique si le mail a �t� envoy�.
	 */
	function send_old($to, $body, $subject, $fromaddress, $fromname = null, $fileName = NULL, $file = NULL)
	{ 
		$mime_boundary=md5(time());
		// headers normaux
		$headers = "";
		$headers .= "From: ".(empty($fromname) ? $fromname.' ' : '')."<".$fromaddress.">\n";
		$headers .= "Reply-To: ".$fromname." <".$fromaddress.">\n";
		$headers .= "Return-Path: ".$fromname." <".$fromaddress.">\n";
		$headers .= "Message-ID: <".time()."-".$fromaddress.">\n";
		$headers .= "X-Mailer: PHP v".phpversion()."\n";
	
		// boundary
		$headers .= 'MIME-Version: 1.0'."\n";
		$headers .= "Content-Type: multipart/mixed; boundary=".$mime_boundary."\n\n";
	
		//premi�re partie de l'email
		$msg = "--".$mime_boundary."\n";
	  
		$msg .= "Content-Type: text/plain; charset=ISO-8859-1"."\n";
		$msg .= "Content-Transfer-Encoding: 8bit"."\n\n";
		$msg .= $body."\n\n";  
	
		//ajoute un fichier en attachement si un fichier a �t� donn�
		if ($file != null) {
			$msg .= "--".$mime_boundary."\n";
			$msg .= "Content-Type: application/pdf; name=".$fileName."\n";
			$msg .= "Content-Transfer-Encoding: base64"."\n";
			$msg .= "Content-Disposition: attachment; filename=".$fileName."\n\n"; //
			$msg .= chunk_split(base64_encode($file))."\n\n";
		}
	
		// fin de l'email
		$msg .= "--".$mime_boundary."--"."\n\n";
		
		// envoi du mail
		ini_set('sendmail_from',$fromaddress);	// tente de modifier temporairement l'adresse d'envoi parametr�e dans la config
		$mail_sent = @mail($to, $subject, $msg, $headers);
		ini_restore('sendmail_from');
		
		return $mail_sent;
	}
}

?>
