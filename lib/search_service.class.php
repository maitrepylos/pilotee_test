<?php
/* Create By Marucci 24/12/2013 library for search ! */ 
REQUIRE_ONCE (SCRIPTPATH . 'lib/database.model.class.php');

abstract class SearchService extends Database {
	protected $begin;
	protected $nbResultsPerPage;
	protected $nbResults;
	protected $results;
	protected $where;
	protected $orderby;

	public function __construct() {
		$this -> setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
		$this -> begin = 0;
		$this -> nbResultsPerPage = 15;
		$this -> where = '';
		$this -> orderby = '';
	}

	public function performSearch($columnId) {
		REQUIRE_ONCE ('model/' . $this -> getTable() . '_database_model_class.php');
		$this -> constructWhereClause();
		$this -> results = array();
		$sql = "SELECT ".$columnId." as id FROM " . $this -> getTable() . " WHERE 1=1 " . $this -> where . $this -> orderby . " LIMIT " . $this -> begin . "," . $this -> nbResultsPerPage;
		$class = $this -> getModelClass();
		$temp = $this -> qA($sql);	
		foreach ($temp as $l) {
			$obj = new $class();
			$obj -> get($l['id'],null);
			$this -> results[] = $obj;
			$obj = $this -> postSearchAlterObject($obj);
		}

		//calcul du nb total de r�sultats
		$nb_results = $this -> lquery("SELECT count(*) as nb FROM " . $this -> getTable() . " WHERE 1=1 " . $this -> where);
		$this -> nbResults = $nb_results['nb'];
		return $this -> results;

	}

	abstract protected function constructWhereClause();
	abstract protected function getModelClass();
	abstract protected function getTable();

	//Permet de modifier un objet instanci� apr�s recherche
	public function postSearchAlterObject($result) {
		return $result;
	}

	public function getNbResults() {
		return $this -> nbResults;
	}

	public function getResults() {
		return $this -> results;
	}

	public function setBeginIndex($index) {
		$this -> begin = $this -> param_int($index);
	}

	public function setResultsPerPage($nbResultsPerPage) {
		$this -> nbResultsPerPage = $this -> param_int($nbResultsPerPage);
	}

}
