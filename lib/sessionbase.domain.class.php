<?php
/**
 * @package Libs
 * @copyright Cediti
 * @subpackage Commons
 * @version 1.0
 */

/**
 * Session
 *  
 */
class SessionBase {

	static $self;
	static $prefix = 'php_';
	
	/**
	 * Constructeur, lance la session
	 *
	 */
	function __construct() {
		session_start();
		SessionBase::$self = $this;
	}
	
	/**
	 * Enter description here...
	 *
	 * @return SessionBase
	 */
	static function GetInstance()
	{
		return SessionBase::$self;
	}
	
	/**
	 * Entrer/Modifier une valeur en session
	 *
	 * @param String Valeur en session � modifier
	 * @param mixed Valeur
	 */
	function set($key, $value) {
		$_SESSION[SessionBase::$prefix.$key] = $value;
	}
	
	/**
	 * V�rifier l'existance d'une valeur en session
	 *
	 * @param String Nom de la valeur en session
	 * @return mixed Valeur
	 */
	function exist($key) {
		$value = $this->get($key);
		return !empty($value);
	}
	
	/**
	 * Supprimer une valeur en session
	 *
	 * @param String Nom de la valeur en session
	 */
	function reset($key) {
		$_SESSION[SessionBase::$prefix.$key] = null;
		unset($_SESSION[SessionBase::$prefix.$key]);
	}
	
	/**
	 * Obtenir une valeur en session
	 *
	 * @param String Nom de la valeur en session � obtenir
	 * @return mixed Valeur
	 */
	function get($key) {
		if (isset($_SESSION[SessionBase::$prefix.$key]))
			return $_SESSION[SessionBase::$prefix.$key];
		else
			return '';
	}
	
	/**
	 * Obtenir une valeur en session ou dans un cookie
	 *
	 * @param String Nom de la valeur en session � obtenir
	 * @param Bool Indique si la valeur se trouve dans un cookie
	 * @return mixed Valeur
	 */
	function getRaw($key, $in_cookie = false) {
		return ($in_cookie ? $_COOKIE[$key] : $_SESSION[$key]);
	}
	
	/**
	 * Supprimer la session
	 *
	 */
	function kill() {
		session_destroy();
		if (isset($_COOKIE[session_name()])) {
		    setcookie(session_name(), '', time()-42000, '/');
		}
		unset($_SESSION);
	}	
}

?>