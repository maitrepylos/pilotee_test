<?php


/**
 * Mettre en majuscule un string, caract�res accentu�s compris
 *
 * @param String $string
 * @return String
 */
function str2upper($string)
{
	return strtr(
		$string,
		'abcdefghijklmnopqrstuvwxyz���������������������',
		'ABCDEFGHIJKLMNOPQRSTUVWXYZC��������������������'
	);
}

/**
 * Mettre en minuscule un string, caract�res accentu�s compris
 *
 * @param String $string
 * @return String
 */
function str2lower($string)
{
	return strtr(
		$string,
		'ABCDEFGHIJKLMNOPQRSTUVWXYZ��������������������',
		'abcdefghijklmnopqrstuvwxyz��������������������'
	);
}

/**
 * Comparer deux string en ignorant la casse, et en rempla�ant les caract�res accentu�s par leur version simple
 *
 * @param String $string1
 * @param String $string2
 * @return Bool
 */
function strcompare($string1, $string2)
{
	$string1 = strneutral($string1);
	$string2 = strneutral($string2);
	
	return ($string1 == $string2);
}

function strneutral($string)
{
	return strtr(
		trim($string),
		'abcdefghijklmnopqrstuvwxyz����������������������������������������� -\'�`',
		'ABCDEFGHIJKLMNOPQRSTUVWXYZCEEEEAAAAUUUUOOOOIIIIEEEEAAAAUUUUOOOOIIII_____'
	);
}

function strneutral2($string)
{
	return strtr(
		trim($string),
		'������������������������������������������������������',
		'nceeeeaaaaaauuuuoooooiiiiyyncEEEEAAAAAAUUUUOOOOOOIIIIY'
	);
}

function makeSizeStr($str, $size = 0, $zero = " ", $type = "s", $alignRight = false)
{
	if (!$alignRight) $align = '-';
	else $align = '';
	
	return sprintf('%'.$align.$zero.$size.$type, substr($str, 0, $size));
}


?>