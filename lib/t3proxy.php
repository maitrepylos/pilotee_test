<?php

/**
 * @package Libs
 * @subpackage Commons
 * @copyright Cediti
 * @version 0.1
 */


/**
 * Classe proxy pour manipuler Typo3
 * 
 */
class T3Proxy
{

	/**
	 * Ajouter un fichier CSS, celui-ci doit se trouver dans l'extension et FILE doit �tre pr�sent dans headerData
	 *
	 * @param string $file
	 */
	static function addStylesheet($file)
	{
		if(!isset($GLOBALS['TSFE']->pSetup['includeCSS.'])) $GLOBALS['TSFE']->pSetup['includeCSS.'] = Array();
		for($x = 1; isset($GLOBALS['TSFE']->pSetup['includeCSS.']['file'.$x.'0']); $x++) {}
		$GLOBALS['TSFE']->pSetup['includeCSS.']['file'.$x.'0'] = SCRIPTPATH.$file;
		$GLOBALS['TSFE']->pSetup['includeCSS.']['file'.$x.'0.']['media'] = 'all';
	}
	
	/**
	 * Ajouter un fichier Javascript, celui-ci doit se trouver dans l'extension et FILE doit �tre pr�sent dans headerData
	 *
	 * @param string $file
	 */
	static function addJavascript($file)
	{
		if(!isset($GLOBALS['TSFE']->pSetup['includeJS.'])) $GLOBALS['TSFE']->pSetup['includeJS.'] = Array();
		for($x = 1; isset($GLOBALS['TSFE']->pSetup['includeJS.']['file'.$x.'0']); $x++) {}
		$GLOBALS['TSFE']->pSetup['includeJS.']['file'.$x.'0'] = SCRIPTPATH.$file;
	}
	
	/**
	 * Obtenir une constante d�finie dans page.10.mesconstantes
	 *
	 * @param string Label
	 * @return mixed Value
	 */
	static function getCustomConstant($label)
	{
		return $GLOBALS['TSFE']->tmpl->setup["page."]["10."]["mesconstantes."][$label];
	}
	
	/**
	 * Red�finir le titre de la page
	 *
	 * @param string Titre d�sir�
	 */
	static function setPageTitle($title)
	{
		$GLOBALS['TSFE']->page['title'] = $title;
	}
	
	/**
	 * Se connecter en frontend.
	 *
	 * @param string Identifiant
	 * @param string Mot de passe
	 * @param string pId du sysfolder contenant les fe_users
	 */
	static function login($username, $password, $pId)
	{
		$_POST['logintype'] = 'login';
		$_POST['pass'] = $password;
		$_POST['user'] = $username;
		
		$GLOBALS['TSFE']->fe_user->checkPid_value = $pId;
		
		$GLOBALS['TSFE']->fe_user->start();
	}
	
	/**
	 * Se d�connecter du frontend
	 *
	 */
	static function logout()
	{
		$_POST['logintype'] = 'logout';
		$GLOBALS['TSFE']->fe_user->start();
	}
	
	/**
	 * Exp�rimental ! Plut�t contre performant.
	 *
	 */
	static function sendMail($fromMail, $fromName, $to, $subject, $messageTxt, $messageHtml = null, $attachments = array(), $debug = false)
	{
		require_once(PATH_t3lib.'class.t3lib_htmlmail.php');
		$cls=t3lib_div::makeInstanceClassName('t3lib_htmlmail');
		
		$t3mail = t3lib_div::makeInstance('t3lib_htmlmail');
		$t3mail->start();
		$t3mail->subject = $subject;
		
		//sender & receiver data
		$t3mail->from_email = $fromMail;
		$t3mail->from_name = str_replace (',' , ' ', $fromName);
		$t3mail->replyto_email = $t3mail->from_email;
		$t3mail->replyto_name = $t3mail->from_name;
		$t3mail->organisation = '';
			
		if(empty($messageHtml))
		{
			$messageHtml = nl2br(htmlentities($messageTxt));
		}
		
		$t3mail->addPlain($messageTxt);
		$t3mail->setHTML($t3mail->encodeMsg($messageHtml));
		
		foreach($attachments as $f)
		{
			$t3mail->addAttachment($f);
		}
		
		$t3mail->setHeaders();
		$t3mail->setContent();
		$t3mail->setRecipient(explode(',', $to));
		
		if($debug) echo $t3mail->preview();
		return ($t3mail->sendTheMail() ? true : false);
	}
	
	/**
	 * Obtenir la langue courante
	 *
	 * @return String Langue courante
	 */
	static function getLang()
	{
		return $GLOBALS['TSFE']->lang;
	}
}

?>