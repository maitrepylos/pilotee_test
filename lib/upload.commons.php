<?php

	function isUploadOk($name)
	{
		return (isset($_FILES[$name]) && $_FILES[$name]['error'] == 0);
	}

	
	function isDirOk($dir, $create = true)
	{
		if(is_dir($dir))
		{
			return true;
		}
		else
		{
			if(create)
			{
				$dirX = explode('/',trim($dir,'/'));
				$dChain = '';
				foreach($dirX as $d)
				{
					$dChain.= $d.'/';
					if(!is_dir($dChain)) //cr�e la partie du chemin si n'existe pas
					{
						@mkdir($dChain);
					}
					
					if(!is_dir($dChain)) return false; //une partie du chemin n'a pu �tre cr��
				}
				
				return (is_dir($dir));
			}
			else
			{
				return false;
			}
		}
	}
	
	function moveFile($orig, $destPath, $destFilename)
	{
		$old = umask(0002);
		$fichier = $orig;
		$filename = $destFilename;
		$dir = $destPath;
		
		if(isDirOk($dir))
		{
			$filepath = $dir.$filename;
			if(is_file($filepath))
			{
				$dot = strrpos($filepath, '.');
				$filepathP1 = substr($filepath, 0, $dot);
				$filepathP2 = substr($filepath, $dot);
				
				$x = 1;
				$filepath = $filepathP1.' ('.$x.')'.$filepathP2;
				
				while(is_file($filepath))
				{
					$x++;
					$filepath = $filepathP1.' ('.$x.')'.$filepathP2;
				}
			}
			move_uploaded_file($fichier, $filepath);
			$result = true;
		}
		else
		{
			$result = false;
		}
		
		umask($old);
		
		return $result;
	}
?>