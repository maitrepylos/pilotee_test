<?php

function getOrigine($user, $pi=false)
{
	$origine = 'INCON';
	if(!$pi)
	{
		switch($user->getTypeUtilisateurId())
		{
			case USER_TYPE_ADMIN :			
										$origine='AGENT';
										break;
			case USER_TYPE_AGENT :     
										$origine='AGENT';
										break;
			case USER_TYPE_OPERATEUR :
										$origine='OPERA';
										break;
			case USER_TYPE_COORDINATEUR :
										$origine='AGENT';
										break;
			case USER_TYPE_AGENT_COORDINATEUR :
										$origine='AGENT';
										break;
		}
	}
	else
		$origine = 'PROJI';
	return $origine;
}

function php2js( $elt, $js_array_name) 
{
 $script_js = "var $js_array_name = new Array();\n";
 for($i=0; $i<$elt->count(); $i++) 
 {
 	$script_js.= "{$js_array_name}[{$i}] = new Array();\n";
 	$script_js.= "{$js_array_name}[{$i}]['actionLabelId'] = \"".$elt->items($i)->getActionLabelId()."\";\n";
 	$script_js.= "{$js_array_name}[{$i}]['operateurId'] = ".$elt->items($i)->getOperateurId().";\n";
 	$script_js.= "{$js_array_name}[{$i}]['label'] = \"".addslashes($elt->items($i)->getLabel())."\";\n";
 	$script_js.= "{$js_array_name}[{$i}]['anneeId'] = \"".$elt->items($i)->getAnneeId()."\";\n";
 	$script_js.= "{$js_array_name}[{$i}]['ordre'] = ".$elt->items($i)->getOrdre().";\n";
 	$script_js.= "{$js_array_name}[{$i}]['swActif'] = ".$elt->items($i)->getSwActif().";\n";
 }
 return $script_js;
}

?>