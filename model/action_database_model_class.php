<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ActionDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($nomGen, $actionLab, $actionNonLab, $actionMicro, $anneeScolaire, $dateAnte, $datePost, $etablissements, $operateurId,
						$actionValide, $actionEnCours, $refAse, $refOp, $niveau, $degreNiveau, $degreNiveauFiliere, $section, $pedaAvec, $pedaSans, $global)
	{
		$user = Session::GetInstance()->getCurrentUser();
		$nomGen = $this->param_string($nomGen);
		$anneeScolaire = $this->param_string($anneeScolaire);
		$refAse = $this->param_string($refAse);
		$refOp = $this->param_string($refOp);
		if(isset($dateAnte)) $dateAnte = $this->param_date($dateAnte);
		if(isset($datePost)) $datePost = $this->param_date($datePost);
	
		$sql = 'select distinct a.*'; 
		$sql .= ' from';
		$sql .= ' action a';
		if(!empty($etablissements))
			$sql .= ' left join action_etablissement ae on ae.actionId = a.actionId';

		if($degreNiveau!='-1' || $niveau!='-1' || $degreNiveauFiliere!='-1')
		{
			$sql .= ' left join action_filiereDegreNiveau fdn on fdn.actionId = a.actionId';
			$sql .= ' left join dic_filiereDegreNiveau dfdn on fdn.filiereDegreNiveauId  = dfdn.filiereDegreNiveauId';
		}
			
		if($section!='-1')
			$sql .= ' left join actionSectionAction sa on sa.actionId = a.actionId';

		if($user->isOperateur())
			$sql .= ' left join action_operateur op on op.actionId = a.actionId';	
			
		$sql .= ' where 1 and a.typeActionId != \''. BOURSES_PI .'\'';
		
		if(!empty($nomGen)) $sql .= ' and ucase(a.nomGen) like \'%' . strtoupper($nomGen) . '%\'';
		
		if(!empty($refAse)) $sql .= ' and ucase(a.refChezASE) like \'%' . strtoupper($refAse) . '%\'';
		
		if(!empty($refOp)) $sql .= ' and ucase(a.refChezOperateur) like \'%' . strtoupper($refOp) . '%\'';
		
		if(isset($pedaAvec) || isset($pedaSans))
		{
			if(isset($pedaAvec) && isset($pedaSans))
			{}	
			else
			{
			 if(isset($pedaAvec))
			 {
			 	$sql .= ' and a.swPedagogie = 1';
			 }
			 elseif(isset($pedaSans))
			 	$sql .= ' and a.swPedagogie = 0';
			}
		}
		
		if(isset($actionValide) || isset($actionEnCours))
		{
			if(isset($actionValide) && isset($actionEnCours)){}				
			else
			{
			 if(isset($actionValide))
			 {
			 	$sql .= ' and a.statutActionId = \''.$actionValide.'\'';
			 }
			 elseif(isset($actionEnCours))
			 	$sql .= ' and a.statutActionId = \''.$actionEnCours.'\'';
			}
		}
		
		if($actionLab || $actionNonLab || $actionMicro)
		{
			$sql .=' and (';
			$first = true;
			if($actionLab)
			{
				$or = 'or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= '('.$or.' a.typeActionId = \''.ACTION_LABELLISEES.'\'';
				
				if($operateurId!='-1')
				{
					$sql .= ' and (a.operateurId = '.$operateurId;
					if($user->isOperateur())
						$sql.=' OR op.operateurId ='.$operateurId;
					$sql.=')';
				}
				$sql.=')';
			}
			
			if($actionNonLab)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.typeActionId = \''.ACTION_NON_LABELLISEES.'\'';
			}
			
			if($actionMicro)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.typeActionId = \''.ACTION_MICRO.'\'';
			}
			
			$sql .=')';
		}
		
		if($anneeScolaire!=-1)
		{
			if($anneeScolaire!=-1)
				$sql.=' and anneeId =\''.$anneeScolaire.'\'';
		}
		
		if(!empty($dateAnte) && !empty($datePost))
		{
			if($dateAnte == $datePost)
			{
				$sql.=' and STR_TO_DATE(dateAction, \'%Y-%m-%d\') =\''.$dateAnte.'\'';
			}
			else
			{
				$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') >=\''.$dateAnte.'\' and STR_TO_DATE(dateAction, \'%Y-%m-%d\') <=\''.$datePost.'\')';
			}		
		}
		elseif(!empty($dateAnte) || !empty($datePost))
		{
		 	if(!empty($dateAnte))
		 		$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') >=\''.$dateAnte.'\')';
		 		
		 	if(!empty($datePost))
		 		$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') <=\''.$datePost.'\')';
		}
		
		$i=0;
		if(!empty($etablissements))
		{
			$etablissements = explode(";",$etablissements);
			$sql.='AND (';
			
			foreach ($etablissements as $et)
			{
				if($i==0)
					$sql.=' ae.etablissementId ='.$et.'';
				else
					$sql.=' OR ae.etablissementId ='.$et.'';
				$i++;
			}
			
			if(isset($global) && $i>0)
				$sql.=' or (a.swGlobal=1)';
			
			$sql.=')';
		}
		
		if(isset($global) && $i==0)
				$sql.=' and a.swGlobal=1';
		
		if($niveau!='-1')
		{
				$niv = '';
			if($niveau == 'PRIMA')
			 	$niv = 'PRI';
			 elseif($niveau == 'SECON')
			 	$niv='SEC';
			 elseif($niveau == 'SUPER')
			 	$niv='SU';
			 elseif($niveau == 'ENSEI')
			 	$niv='ENS';	
			 elseif($niveau == 'PROMO')
			 	$niv='PSO';
			 
			$sql .= ' and fdn.filiereDegreNiveauId like \''.$niv.'%\'';
		}
			
		if($degreNiveau!='-1')
			$sql .= ' and dfdn.degreNiveauId = \''.$degreNiveau.'\'';
			
		if($degreNiveauFiliere!='-1')
			$sql .= ' and dfdn.filiereDegreNiveauId = \''.$degreNiveauFiliere.'\'';
		
			
		if($section!='-1')
			$sql .= ' and sa.sectionActionId = \''.$section.'\'';
				
		$sql .= ';';
		return $this->lquery($sql, DB_ECHO);
	}

	public function getRapport($nom, $anneeId, $global, $actionLab, $actionNonLab, $actionMicro, $categorieArray, $operateurArray, $etablissements)
	{
		$user = Session::GetInstance()->getCurrentUser();
		$sql = 'select distinct a.* ';
		$sql .= 'from  action a';
		if(isset($etablissements) && $etablissements->count()>0)
			$sql .= ' left join action_etablissement ae on ae.actionId = a.actionId';
			
		if($user->isOperateur())
			$sql .= ' left join action_operateur op on op.actionId = a.actionId';	
		$sql.=' where 1 ';
		
		if(!empty($nom)) $sql .= ' and ucase(a.nomGen) like \'%' . strtoupper($nom) . '%\'';
		$sql.=' and anneeId =\''.$anneeId.'\'';
		$sql .= ' and a.statutActionId = \'VALID\'';
		if($actionLab || $actionNonLab || $actionMicro)
		{
			$sql .=' and (';
			$first = true;
			if($actionLab)
			{
				$or = 'or';
				if($first)
				{
					$or = ''; 
					$first = false;
				}
				$sql .= '('. $or.' a.typeActionId = \''.ACTION_LABELLISEES.'\'';
				
				if(isset($operateurArray) && count($operateurArray)>0)
				{
					$sql .= ' and a.operateurId in(';
					for($i=0; $i<count($operateurArray); $i++)
					{
						$sql.= '\''.$operateurArray[$i].'\', ';				
					}
					$sql= substr($sql, 0, strlen($sql)-2) .')' ;
					
					if($user->isOperateur())
						$sql.=' OR op.operateurId ='.$operateurArray[0];
					
				}				
				$sql.=')';
			}
			
			if($actionNonLab)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				
				$sql .= $or.' ( a.typeActionId = \''.ACTION_NON_LABELLISEES.'\'  ';
				if(isset($categorieArray) && count($categorieArray)>0)
				{
					$sql.= 'and a.categActionNonLabelId in ('; 
					for($i=0; $i<count($categorieArray); $i++)
					{
						$sql.= '\''.$categorieArray[$i].'\', ';				
					}
					$sql= substr($sql, 0, strlen($sql)-2) .')' ;
				}
				$sql.=')';
			}
			
			if($actionMicro)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.typeActionId = \''.ACTION_MICRO.'\'';
			}
			
			$sql .=')';
		}
		
		$i=0;
		if(isset($etablissements) && $etablissements->count()>0)
		{
			$sql.='AND (';
			
			for($i=0; $i<$etablissements->count();$i++)
			{
				if($i==0)
					$sql.=' ae.etablissementId ='.$etablissements->items($i)->getEtablissementId().'';
				else
					$sql.=' OR ae.etablissementId ='.$etablissements->items($i)->getEtablissementId().'';
				
			}
			
			if($i>0)
				$sql.=' or (a.swGlobal=1)';
			
			$sql.=')';
		}
		
		if(isset($global) && $i==0)
			$sql.=' and a.swGlobal=1';
		
		//	echo $sql;
		return $this->lquery($sql, DB_ECHO);
		
	}
	
	public function GetById($id)
	{
		$sql = 'select * from action where 1';
		$sql .= sprintf(' and actionId = %s', $id);
		$sql .= ';';
		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function getActionEtablissement($idAction, $utilisateurId = null)
	{
		$sql = 'select e.* from action_etablissement ae left join etablissement e on ae.etablissementId = e.etablissementId where 1';
		$sql .= sprintf(' and ae.actionId = %s', $idAction);
		if (isset($utilisateurId))$sql .= sprintf(' and e.utilisateurId = %s', $utilisateurId); 
		$sql .= ';';
		
		// echo '<br>' . $sql . '<br>';
		
		return $this->lquery($sql, DB_ECHO);	
	}

	public function getActionContact($idAction, $txtRole = null)
	{
		$sql = 'select * from action_contact where 1';
		$sql .= sprintf(' and actionId = %s', $idAction);
		if ($txtRole) $sql .= sprintf(' and roleContactActionId = \'%s\'', $txtRole);
		$sql .= ' order by roleContactActionId DESC';
		$sql .= ';';
		
		/*echo "<br>debug FFI<br>";
		echo $sql;
		echo "<br>end debug FFI<br>";
		*/		
		
		return $this->lquery($sql, DB_ECHO);	
	}
	
	public function getActionSection($idAction)
	{
		$sql = 'select asa.*, d.ordre from actionSectionAction asa, dic_sectionAction d where 1';
		$sql.= ' and asa.sectionActionId = d.sectionActionId';
		if(isset($idAction))
		$sql .= sprintf(' and actionId = %s', $idAction);
		$sql.= ' order by d.ordre';
		$sql .= ';';
		
		return $this->lquery($sql, DB_ECHO);		
	}
	
	public function getSectionActionList($id = null)
	{
		$sql = 'select * from dic_sectionAction  where swActif = 1 ';
		if(isset($id))
			$sql.= ' and sectionActionId = \''.$id.'\'';
		$sql.=' order by ordre';
		
		return $this->lquery($sql, DB_ECHO);		
	}
	
	public function getActionFiliereDegreNiveau($idAction)
	{
		$sql = 'select af.*, d.ordre from action_filiereDegreNiveau af, dic_filiereDegreNiveau d where 1';
		$sql .=' and af.filiereDegreNiveauId  = d.filiereDegreNiveauId ';
		if(isset($idAction))
		$sql .= sprintf(' and actionId = %s', $idAction);
		$sql.= ' order by d.ordre';
		$sql .= ';';
		
		return $this->lquery($sql, DB_ECHO);		
	}
	
	public function getFinalitePedagogList($id=null)
	{
		$sql = 'select * from dic_finalitePedagog  where 1';
		if(isset($id))
			$sql.= ' and finalitePedagogId = \''.$id.'\'';
		
		return $this->lquery($sql, DB_ECHO);	
	}
	
	public function getActionFinalitePedagog($idAction)
	{
		$sql = 'select afp.*,d.ordre from action_finalitePedagog afp, dic_finalitePedagog d  where 1';
		$sql.= ' and afp.finalitePedagogId = d.finalitePedagogId';
		if(isset($idAction))
			$sql .= sprintf(' and actionId = %s', $idAction);
		$sql.= ' order by d.ordre';
		$sql .= ';';
				
		return $this->lquery($sql, DB_ECHO);		
	}
	
	public function getFiliereDegreNiveauList($id= null)
	{
		$sql = 'select * from dic_filiereDegreNiveau  where swActif = 1';
		if(isset($id))
			$sql.= ' and filiereDegreNiveauId = \''.$id.'\'';
		$sql.=' order by ordre';
		
		return $this->lquery($sql, DB_ECHO);		
	}
	
	public function getNiveauList($id= null)
	{
		$sql = 'select * from dic_niveauEtablissement  where swActif = 1';
		if(isset($id))
			$sql.= ' and niveauEtablissementId = \''.$id.'\'';
		$sql.=' order by ordre';
		
		return $this->lquery($sql, DB_ECHO);		
	}
	
	public function getNiveauDegreList($id= null)
	{
		$sql = 'select * from dic_degreNiveau  where swActif = 1';
		if(isset($id))
			$sql.= ' and degreNiveauId = \''.$id.'\'';
		$sql.=' order by ordre';
		
		return $this->lquery($sql, DB_ECHO);		
	}
	
	public function getCategActionNonLabelList($id=null)
	{
		$sql = 'select * from dic_categActionNonLabel  where swActif = 1';
		if(isset($id))
			$sql.= ' and categActionNonLabelId = \''.$id.'\'';
			
		$sql.=' order by ordre';
		
		return $this->lquery($sql, DB_ECHO);	
	}
	
	public function updateAction($action)
	{
		$sql = 'UPDATE action set statutSubventionId =';
		
		if($action->GetStatutSubventionId()!=null) $sql.= "'".$this->param_string($action->GetStatutSubventionId())."', ";
		else $sql.= "null, ";
		
		$sql.='actionLabelId=';
		if($action->GetActionLabelId()!=null) $sql.= "'".$this->param_string($action->GetActionLabelId())."', ";
		else $sql.= "null, ";
		
		$sql.='refChezOperateur=';		
		if($action->GetRefChezOperateur()!=null) $sql.= "'".$this->param_string($action->GetRefChezOperateur())."', ";
		else $sql.= "null, ";
		
		$sql.='refChezASE=';
		if($action->GetRefChezASE()!=null) $sql.= "'".$this->param_string($action->GetRefChezASE())."', ";
		else $sql.= "null, ";
		
		$sql.='typeActionId=';
		$sql.= "'".$this->param_string($action->GetTypeActionId())."', ";
		
		$sql.='anneeId=';
		$sql.= "'".$this->param_string($action->GetAnneeId())."', ";
		
		$sql.='statutActionId=';
		$sql.= "'".$this->param_string($action->GetStatutActionId())."', ";
		
		$sql.='categActionNonLabelId=';
		if($action->GetCategActionNonLabelId()!=null) $sql.= "'".$this->param_string($action->GetCategActionNonLabelId())."', ";
		else $sql.= "null, ";
		
		$sql.='operateurId=';
		if($action->GetOperateurId()!=null) $sql.= "'".$this->param_string($action->GetOperateurId())."', ";
		else $sql.= "null, ";
		
		$sql.='dateAction=';
		if($action->GetDateAction()!=null) $sql.= "'".$this->param_date($action->GetDateAction())."', ";
		else $sql.= "null, ";
		
		$sql.='nomGen=';
		if($action->GetNomGen()!=null) $sql.= "'".$this->param_string($action->GetNomGen())."', ";
		else $sql.= "null, ";
		
		$sql.='nomSpec=';
		if($action->GetNomSpec()!=null) $sql.= "'".$this->param_string($action->GetNomSpec())."', ";
		else $sql.= "null, ";

		$sql.='commentaire=';
		if($action->GetCommentaire()!=null) $sql.= "'".$this->param_string($action->GetCommentaire())."', ";
		else $sql.= "null, ";
		
		$sql.='swGlobal=';
		$sql.= "".$action->GetSwGlobal().", ";
		
		$sql.='swPedagogie=';
		$sql.= "".$action->GetSwPedagogie().", ";
		
		$sql.='nbApprenants=';
		$sql.= "".$action->GetNbApprenants().", ";
		
		$sql.='nbEnseignants=';
		$sql.= "".$action->GetNbEnseignants().", ";
		
		$sql.='website=';
		if($action->GetWebsite()!=null) $sql.= "'".$this->param_string($action->GetWebsite())."', ";
		else $sql.= "null, ";
		
		$sql.='montantDemande=';
		if($action->GetMontantDemande()!=null) $sql.= "'".$action->GetMontantDemande()."', ";
		else $sql.= "null, ";
		
		$sql.='montantSubvention=';
		if($action->GetMontantSubvention()!=null) $sql.= "'".$action->GetMontantSubvention()."', ";
		else $sql.= "null, ";
		
		$sql.='categorieBourse=';
		if($action->GetCategorieBourse()!=null) $sql.= "'".$this->param_string($action->GetCategorieBourse())."', ";
		else $sql.= "null, ";
		
		$sql.='projetRealise=';
		if($action->GetProjetRealise()!=null) $sql.= "'".$this->param_string($action->GetProjetRealise())."', ";
		else $sql.= "null, ";
		
		$sql.='groupeClasseConcernee=';
		if($action->GetGroupeClasseConcernee()!=null) $sql.= "".$action->GetGroupeClasseConcernee().", ";
		else $sql.= "null, ";
		
		$sql.='collaborationInterEtab=';
		if($action->GetCollaborationInterEtab()!=null) $sql.= "'".$this->param_string($action->GetCollaborationInterEtab())."', ";
		else $sql.= "null, ";
		
		$sql.='updated=';
		$sql.= "'".$action->GetUpdated()."', ";
		
		$sql.='updatedBy=';
		$sql.= $action->GetUpdatedBy()." ";
		
		$sql.='where actionId = '.$action->GetActionId();

		if($this->lquery($sql, DB_ECHO))
		{
			return $action->GetActionId();
		}	
	}
	
	public function insertAction($action)
	{
		$sql = 'INSERT INTO action (statutSubventionId, actionLabelId, refChezOperateur, '. 
				'refChezASE, typeActionId, anneeId, statutActionId, categActionNonLabelId, operateurId, '.
				'dateAction, nomGen, nomSpec, commentaire, swGlobal, swPedagogie, nbApprenants, nbEnseignants, '.
				'website, montantDemande, montantSubvention, categorieBourse, projetRealise, groupeClasseConcernee, '.
				'collaborationInterEtab, created, createdBy, updated, updatedBy) VALUES (';
		
			if($action->GetStatutSubventionId()!=null) $sql.= "'".$this->param_string($action->GetStatutSubventionId())."', ";
			else $sql.= "null, ";
						
			if($action->GetActionLabelId()!=null) $sql.= "'".$this->param_string($action->GetActionLabelId())."', ";
			else $sql.= "null, ";

			if($action->GetRefChezOperateur()!=null) $sql.= "'".$this->param_string($action->GetRefChezOperateur())."', ";
			else $sql.= "null, ";

			if($action->GetRefChezASE()!=null) $sql.= "'".$this->param_string($action->GetRefChezASE())."', ";
			else $sql.= "null, ";

			$sql.= "'".$this->param_string($action->GetTypeActionId())."', ";
			$sql.= "'".$this->param_string($action->GetAnneeId())."', ";
			$sql.= "'".$this->param_string($action->GetStatutActionId())."', ";			

			if($action->GetCategActionNonLabelId()!=null) $sql.= "'".$this->param_string($action->GetCategActionNonLabelId())."', ";
			else $sql.= "null, ";
			
			if($action->GetOperateurId()!=null) $sql.= "'".$this->param_string($action->GetOperateurId())."', ";
			else $sql.= "null, ";
			
			if($action->GetDateAction()!=null) $sql.= "'".$this->param_date($action->GetDateAction())."', ";
			else $sql.= "null, ";

			if($action->GetNomGen()!=null) $sql.= "'".$this->param_string($action->GetNomGen())."', ";
			else $sql.= "null, ";

			if($action->GetNomSpec()!=null) $sql.= "'".$this->param_string($action->GetNomSpec())."', ";
			else $sql.= "null, ";        	
			
			if($action->GetCommentaire()!=null) $sql.= "'".$this->param_string($action->GetCommentaire())."', ";
			else $sql.= "null, ";

			$sql.= "".$action->GetSwGlobal().", ";
			$sql.= "".$action->GetSwPedagogie().", ";
			$sql.= "".$action->GetNbApprenants().", ";
			$sql.= "".$action->GetNbEnseignants().", ";
			
			if($action->GetWebsite()!=null) $sql.= "'".$this->param_string($action->GetWebsite())."', ";
			else $sql.= "null, ";
			
			if($action->GetMontantDemande()!=null) $sql.= "'".$action->GetMontantDemande()."', ";
			else $sql.= "null, ";
			
			if($action->GetMontantSubvention()!=null) $sql.= "'".$action->GetMontantSubvention()."', ";
			else $sql.= "null, ";
			
			if($action->GetCategorieBourse()!=null) $sql.= "'".$this->param_string($action->GetCategorieBourse())."', ";
			else $sql.= "null, ";
			
			if($action->GetProjetRealise()!=null) $sql.= "'".$this->param_string($action->GetProjetRealise())."', ";
			else $sql.= "null, ";
			
			if($action->GetGroupeClasseConcernee()!=null) $sql.= "".$action->GetGroupeClasseConcernee().", ";
			else $sql.= "null, ";
			
			if($action->GetCollaborationInterEtab()!=null) $sql.= "'".$this->param_string($action->GetCollaborationInterEtab())."', ";
			else $sql.= "null, ";
			
	        $sql.= "'".$action->GetCreated()."', ";
			$sql.= $action->GetCreatedBy().", ";
			$sql.= "'".$action->GetUpdated()."', ";
			$sql.= $action->GetUpdatedBy()." ";
			$sql.=')';
			//echo $sql;
			

			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}	
	}
	
	public function deleteAction($actionId)
	{
		$sql = 'delete from action where actionId = '.$actionId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}

	public function insertActionEtablissement($actionEtab)
	{
		$sql = 'INSERT INTO action_etablissement(actionId, etablissementId, created, createdBy, updated, updatedBy) VALUES(';
		
		$sql.= "".$actionEtab->GetActionId().", ";
		$sql.= "".$actionEtab->GetEtablissementId().", ";
		$sql.= "'".$actionEtab->GetCreated()."', ";
		$sql.= $actionEtab->GetCreatedBy().", ";
		$sql.= "'".$actionEtab->GetUpdated()."', ";
		$sql.= $actionEtab->GetUpdatedBy()." ";
		$sql.=')';

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
		
	}
	
	public function deleteActionEtablissement($actionId)
	{
		$sql = 'delete from action_etablissement where actionId = '.$actionId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}

	public function insertActionContact($actionContact)
	{
		$sql='INSERT INTO action_contact(actionId, contactId, roleContactActionId, created, createdBy, updated, updatedBy) VALUES(';
		$sql.= "".$actionContact->GetActionId().", ";
		$sql.= "".$actionContact->GetContactId().", ";
		$sql.= "'".$actionContact->GetRoleContactActionId()."', ";
	    $sql.= "'".$actionContact->GetCreated()."', ";
		$sql.= $actionContact->GetCreatedBy().", ";
		$sql.= "'".$actionContact->GetUpdated()."', ";
		$sql.= $actionContact->GetUpdatedBy()." ";
		$sql.=')';

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
	}
	
	public function deleteActionContact($actionId)
	{
		$sql = 'delete from action_contact where actionId = '.$actionId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}
	
	public function insertActionFiliereDegreNiveau($afdn)
	{
		$sql='INSERT INTO action_filiereDegreNiveau(actionId, filiereDegreNiveauId, nbApprenants) VALUES(';
		$sql.= "".$afdn->GetActionId().", ";
		$sql.= "'".$afdn->GetFiliereDegreNiveauId()."', ";
		$sql.= "".$afdn->GetNbApprenants()." ";
		$sql.=')';

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
	}
	
	public function deleteActionFiliereDegreNiveau($actionId)
	{
		$sql = 'delete from action_filiereDegreNiveau where actionId = '.$actionId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}
	
	public function insertActionSection($actionSection)
	{
		$sql = 'INSERT INTO actionSectionAction(actionId, sectionActionId, nbApprenants) VALUES(';
		
		$sql.= "".$actionSection->GetActionId().", ";
		$sql.= "'".$actionSection->GetSectionActionId()."', ";
		$sql.= "".$actionSection->GetNbApprenants()." ";
		$sql.=')';

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
	}
	
	public function deleteActionSection($actionId)
	{
		$sql = 'delete from actionSectionAction where actionId = '.$actionId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}
	
	public function insertActionPeda($actionPeda)
	{
		$sql = 'INSERT INTO action_finalitePedagog(actionId, finalitePedagogId, nbApprenants) VALUES(';
		
		$sql.= "".$actionPeda->GetActionId().", ";
		$sql.= "'".$actionPeda->GetFinalitePedagogId()."', ";
		$sql.= "".$actionPeda->GetNbApprenants()." ";
		$sql.=')';

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
	}
	
	public function deleteActionPeda($actionId)
	{
		$sql = 'delete from action_finalitePedagog where actionId = '.$actionId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}
	
	public function insertActionOperateur($actionOp)
	{
		$sql = 'INSERT INTO action_operateur(actionId, operateurId) VALUES(';
		$sql.= "".$actionOp->GetActionId().", ";
		$sql.= "".$actionOp->GetOperateurId()." ";
		$sql.=')';

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
	}

	public function deleteActionOperateur($actionId)
	{
		$sql = 'delete from action_operateur where actionId = '.$actionId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}
	
	// FFI
	public function insertActionFormateur($actionOp)
	{
		$sql = 'INSERT INTO action_formateur(actionId, formateurId) VALUES(';
		$sql.= "".$actionOp->GetActionId().", ";
		$sql.= "".$actionOp->GetFormateurId()." ";
		$sql.=')';

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
	}
	
	public function deleteActionFormateur($actionId)
	{
		$sql = 'delete from action_formateur where actionId = '.$actionId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}
	
	
	
	/**
	 * @author Fr�d�ric Fil�e
	 * Listing pour les actions (point C.2)
	 */
	public function listing($activite, $utilisateurId,  $anneeId, $actionLab, $actionNonLab, $formations, $stac, $ateliers, $projEntr, $cp_1, $cp_2, $arrondissementsArray)
	{
		$user = Session::GetInstance()->getCurrentUser();
		$sql = 'select distinct a.*'; 
		$sql .= ' from';
		$sql .= ' action a';
		$sql .= ' left join action_etablissement ae on ae.actionId = a.actionId';
		$sql .= ' left join etablissement e on ae.etablissementId = e.etablissementId';
		
		if($user->isOperateur())
			$sql .= ' left join action_operateur op on op.actionId = a.actionId';	
		
		$sql .= ' left join adresse adr on adr.adresseId = e.adresseId';
		$sql .= ' left join dic_codepostal cp on cp.codepostal = adr.codepostal';
		$sql .= ' left join dic_province dp on dp.dic_provinceId = cp.provinceId';
		$sql .= ' left join dic_reseauEtablissement dr on dr.reseauEtablissementId = e.reseauEtablissementId';
		$sql .= ' left join dic_niveauEtablissement dn on dn.niveauEtablissementId = e.niveauEtablissementId';
		$sql .= ' left join entiteAdmin ea on ea.entiteAdminId = e.entiteAdminId';
			
		$sql .= ' where 1 '; 
		// FFI : ...and a.typeActionId != \''. BOURSES_PI .'\'';
		
		if(!empty($activite)) $sql .= ' and ( ucase(a.nomGen) like \'%' . $this->param_string(strtoupper($activite)) . '%\' or ucase(a.nomSpec) like \'%' . $this->param_string(strtoupper($activite)) . '%\')';
		
		if(!empty($utilisateurId)) $sql .= ' and e.utilisateurId = ' . $utilisateurId ; 
		
		if($anneeId!=-1)
		{
			$sql.=' and anneeId =\''.$anneeId .'\'';
		}
		
		//if(isset($global) && $i==0)
		//		$sql.=' and a.swGlobal=1';
		
		if($actionLab ||$actionNonLab || $projEntr || $formations || $stac || $ateliers)
		{
			$sql .=' and (';
			$first = true;
			if($actionLab)
			{
				$or = 'or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .=  $or.' a.typeActionId = \''.ACTION_LABELLISEES.'\'';
				
			}
			
			if($actionNonLab)
			{
				$or = 'or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.typeActionId = \''.ACTION_NON_LABELLISEES.'\'';
				
			}
			
			
			if($projEntr)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.typeActionId = \''. BOURSES_PI.'\'';
			}
			
			
			if($formations)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.typeActionId = \''. 'FOR' .'\'';
			}
			
			
			if($stac)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.typeActionId = \'' . 'STAC' . '\''; 
				
			}
			
			if($ateliers)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.typeActionId = \'' . 'ATE' . '\'';
				
			}
			
			
			$sql .=')';
		}
		
		
		if (isset($cp_1)) $sql .= ' and cast(cp.codepostal as signed) >= cast(\'' . $cp_1 .'\' as signed)';
		if (isset($cp_2)) $sql .= ' and cast(cp.codepostal as signed) <= cast(\'' . $cp_2 .'\' as signed)';

		if (isset($arrondissementsArray) && (count($arrondissementsArray) > 0))
		{
			$arrondissements = null;
				
			for ($i = 0; $i < count($arrondissementsArray); $i++)
				$arrondissements .= (isset($arrondissements) ? ',' : '') . '\'' . $arrondissementsArray[$i] . '\'';
				
			$sql .= ' and cp.arrondissementId in (' . $arrondissements . ')';
		}
		
		$sql .= ' order by a.nomGen';
		
		//echo "<br>";
		//echo $sql;
		//echo "<br>";
		
		return $this->lquery($sql, DB_ECHO);
	}
	
	
	
	
	
	
}
?>