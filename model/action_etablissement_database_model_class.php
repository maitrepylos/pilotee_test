<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ActionEtablissementDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($etablissementId = null, $userId = null, $typeActionId = null, $exclude = null, $ordered = null, $subvention = null, $orderDate = false)
	{
		$sql = 'select ae.* from action_etablissement ae';

		$sql .= ' inner join action a on a.actionId = ae.actionId';
		if (isset($userId)) $sql .= ' inner join operateur_utilisateur ou on ou.utilisateurId = ' . $userId;
		if (isset($ordered)) $sql .= ' inner join dic_typeAction dta on a.typeActionId = dta.typeActionId';
		
		$sql .= ' where 1';
		
		if (isset($etablissementId)) $sql .= ' and ae.etablissementId = ' . $etablissementId;
		if (isset($userId)) $sql .= ' and (a.operateurId = ou.operateurId or exists (select 1 from action_operateur ao where ao.actionId = a.actionId))';
		if (isset($typeActionId)) $sql .= ' and a.typeActionId = \'' . $typeActionId . '\'';
		
		//19 Ao�t 2014 Marucci les formations ne doivent pas �tre pris en compte
		$sql .= ' and a.typeActionId <> \'' . 'FOR' . '\'';
		
		if (isset($exclude)) $sql .= ' and a.typeActionId <> \'' . $exclude . '\'';
		if(isset($subvention)) $sql .= ' and a.statutSubventionId = \'' . $subvention . '\'';
		if (isset($ordered)) $sql .= ' order by dta.ordre';
		if ($orderDate) $sql .= (isset($ordered)?',':' ORDER BY') . ' a.dateAction DESC';
		
		$sql .= ';';
		
		// FFI DEBUG 
		// echo 'ETABLISSEMENT : ' . $etablissementId . ' : ' . $sql . '<br>';
		
		
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}
	
	public function getFormations($etablissementId = null, $userId = null, $orderDate = false)
	{
		$sql = 'select ae.* from action_etablissement ae';

		$sql .= ' inner join action a on a.actionId = ae.actionId';
		if (isset($userId)) $sql .= ' inner join operateur_utilisateur ou on ou.utilisateurId = ' . $userId;
		
		$sql .= ' where 1';
		
		if (isset($etablissementId)) $sql .= ' and ae.etablissementId = ' . $etablissementId;
		if (isset($userId)) $sql .= ' and (a.operateurId = ou.operateurId or exists (select 1 from action_operateur ao where ao.actionId = a.actionId))';
		$sql .= ' and (a.typeActionId = \'FOR\' or a.typeActionId = \'STAC\' or a.typeActionId = \'ATE\') ';
		$sql .= ' order by a.typeActionId';
		if ($orderDate) $sql .= ', a.dateAction DESC';
		
		$sql .= ';';
		
		// FFI DEBUG 
		//echo 'ETABLISSEMENT : ' . $etablissementId . ' : ' . $sql . '<br>';
		
		
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}

	
}
?>