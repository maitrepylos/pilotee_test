<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ActionLabelDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($actionLabelId = null, $operateurId = null, $anneeId = null)
    {
        $sql = 'select * from dic_actionLabel where 1';
        if (isset($actionLabelId)) $sql .= ' and actionLabelId = \'' . $actionLabelId . '\'';
        if (isset($operateurId)) $sql .= ' and operateurId = '.$operateurId;
        if(isset($anneeId)) $sql .=' and anneeId = \''.$anneeId.'\'';
        $sql .= ';';
        return $this->lquery($sql, DB_ECHO);
    }
}
?>