<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ActionOperateurDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($actionId = null, $operateurId = null)
    {
        $sql = 'select * from action_operateur where 1';

        if (isset($actionId)) $sql .= ' and actionId = ' . $actionId;
        if (isset($operateurId)) $sql .= ' and operateurId = \'' . $operateurId . '\'';

        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>