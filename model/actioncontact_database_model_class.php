<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ActionContactDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    // ($this->GetContactId(),null, $userId, null, BOURSES_PI, true);
    public function get($contactId = null, $actionId = null, $userId = null, $typeActionId = null, $exclude = null, $ordered = null)
    {
        $sql = 'select ac.* from action_contact ac';
        
        if (isset($typeActionId) || isset($ordered) || isset($operateurId)) $sql .= ' inner join action a on a.actionId = ac.actionId';
		if (isset($ordered)) $sql .= ' inner join dic_typeAction dta on a.typeActionId = dta.typeActionId';
		if (isset($userId)) $sql .= ' inner join operateur_utilisateur ou on ou.utilisateurId = ' . $userId;
		
        $sql .= ' where 1';
        
        if (isset($actionId)) $sql .= ' and ac.actionId = ' . $actionId;
        if (isset($contactId)) $sql .= ' and ac.contactId = ' . $contactId;
		if (isset($operateurId)) $sql .= ' and (a.operateurId = ' . $operateurId . ' or exists (select 1 from action_operateur ao where ao.actionId = a.actionId))';
		if (isset($typeActionId)) $sql .= ' and a.typeActionId = \'' . $typeActionId . '\'';
		if (isset($exclude)) $sql .= ' and a.typeActionId <> \'' . $exclude . '\'';
		if (isset($ordered)) $sql .= ' order by dta.ordre';
        $sql .= ';';
      	// echo $sql;
        return $this->lquery($sql, DB_ECHO);
    }
    
	public function getFormations($contactId = null, $actionId = null, $userId = null, $ordered = null)
    {
        $sql = 'select ac.* from action_contact ac';
        
        //if (isset($typeActionId) || isset($ordered) || isset($operateurId)) 
        $sql .= ' inner join action a on a.actionId = ac.actionId';
		// if (isset($ordered)) $sql .= ' inner join dic_typeAction dta on a.typeActionId = dta.typeActionId';
		if (isset($userId)) $sql .= ' inner join operateur_utilisateur ou on ou.utilisateurId = ' . $userId;
		
        $sql .= ' where 1';
        
        if (isset($actionId)) $sql .= ' and ac.actionId = ' . $actionId;
        if (isset($contactId)) $sql .= ' and ac.contactId = ' . $contactId;
		if (isset($operateurId)) $sql .= ' and (a.operateurId = ' . $operateurId . ' or exists (select 1 from action_operateur ao where ao.actionId = a.actionId))';
		$sql .= ' and (a.typeActionId = \'FOR\' or a.typeActionId = \'STAC\' or a.typeActionId = \'ATE\') ';
		if (isset($ordered)) $sql .= ' order by a.typeActionId';
        $sql .= ';';
      	
        // echo $sql;
        
        return $this->lquery($sql, DB_ECHO);
    }
    
}
?>