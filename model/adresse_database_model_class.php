<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class AdresseDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($adresseId)
	{
		$sql = 'select * from adresse where 1';
		
		if (isset($adresseId))
		{
			$sql .= ' and adresseId = ' . $adresseId;
		}
		
		$rs = $this->lquery($sql, DB_ECHO);

		return $rs;
	}
	
	public function listForGM($missing = null)
	{
		$sql = 'select * from adresse where (1';
		
		
		if (isset($missing)) $sql .= ' and (lat is null or lng is null)';
		
		$sql.=' and(codepostal is not null)';
		$sql .= ')';
		
		$rs = $this->lquery($sql, DB_ECHO);

		return $rs;
	}

	public function listForGMCount($missing = null, $unknown = null)
	{
		$sql = 'select count(*) as recordCount from adresse where 1';
		
		if (isset($missing)) $sql .= ' and lat is null or lng is null';
		if (isset($unknown)) $sql .= ' and lat = 999 or lng = 999';

		$rs = $this->lquery($sql, DB_ECHO);

		return $rs;
	}
	
	public function insertAdresse($adresse)
	{
	
		$sql = 'INSERT INTO adresse '.
			   '(dic_provinceId, rue, codepostal, ville, tel_1, tel_2, email_1, email_2, fax,'.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';
				 	 
					if($adresse->GetProvinceId()!=null)	$sql.= "'".$adresse->GetProvinceId()."', ";
					else $sql.= "null, ";
					
					if($adresse->GetRue()!=null) $sql.= "'".$this->param_string($adresse->GetRue())."', ";
					else $sql.= "null, ";
					
					if($adresse->GetCodePostal()!=null) 
						$sql.= "'".$adresse->GetCodePostal()->GetCodePostal()."', ";
					else 
						$sql.= "null, ";

					if($adresse->GetVille()!=null) $sql.= "'".$this->param_string($adresse->GetVille())."', ";
					else $sql.= "null, ";
						
					if($adresse->GetTel1()!=null) $sql.= "'".$this->param_string($adresse->GetTel1())."', ";
					else $sql.= "null, ";
					
					if($adresse->GetTel2()!=null) $sql.= "'".$this->param_string($adresse->GetTel2())."', ";
					else $sql.= "null, ";
					
					if($adresse->GetEmail1()!=null)	$sql.= "'".$this->param_string($adresse->GetEmail1())."', ";
					else $sql.= "null, ";
						
					if($adresse->GetEmail2()!=null)	$sql.= "'".$this->param_string($adresse->GetEmail2())."', ";
					else $sql.= "null, ";
					
					if($adresse->GetFax()!=null) $sql.= "'".$this->param_string($adresse->GetFax())."', ";
					else $sql.= "null, ";
					
					$sql.= "'".$adresse->GetCreated()."', ";
					$sql.= $adresse->GetCreatedBy().", ";
					$sql.= "'".$adresse->GetUpdated()."', ";
					$sql.= $adresse->GetUpdatedBy()." ";
				$sql.=')';
				
				if($this->lquery($sql, DB_ECHO))
				{
					return $this->getLastInsertId();
				}
	}
	
			    
	public function updateAdresse($adresse)
	{
			$sql = 'UPDATE adresse SET dic_provinceId = ';
			
			if($adresse->GetProvinceId()!=null)	$sql.= "'" .$adresse->GetProvinceId()."', "; 
			else $sql.= 'null, ';

			$sql.="rue = ";
			if($adresse->GetRue()!=null) $sql.= "'".$this->param_string($adresse->GetRue())."', ";
			else $sql.= "null, ";
					
			$sql.="codepostal = ";
			if($adresse->GetCodePostal()!=null) 
				$sql.= "'".$adresse->GetCodePostal()->GetCodePostal()."', ";
			else 
				$sql.= "null, ";

			$sql.="ville = ";
			if($adresse->GetVille()!=null) $sql.= "'".$this->param_string($adresse->GetVille())."', ";
			else $sql.= "null, ";
						
			$sql.="tel_1 = ";
			if($adresse->GetTel1()!=null) $sql.= "'".$this->param_string($adresse->GetTel1())."', ";
			else $sql.= "null, ";
					
			$sql.="tel_2 = ";
			if($adresse->GetTel2()!=null) $sql.= "'".$this->param_string($adresse->GetTel2())."', ";
			else $sql.= "null, ";
					
			$sql.="email_1 = ";
			if($adresse->GetEmail1()!=null)	$sql.= "'".$this->param_string($adresse->GetEmail1())."', ";
			else $sql.= "null, ";
						
			$sql.="email_2 = ";
			if($adresse->GetEmail2()!=null)	$sql.= "'".$this->param_string($adresse->GetEmail2())."', ";
			else $sql.= "null, ";
					
			$sql.="fax = ";
			if($adresse->GetFax()!=null) $sql.= "'".$this->param_string($adresse->GetFax())."', ";
			else $sql.= "null, ";
					
			$sql.= "updated ='".$adresse->GetUpdated()."', ";
			$sql.= "updatedBy =" .$adresse->GetUpdatedBy()." ";
					
			$sql.=" WHERE adresseId = ".$adresse->GetAdresseId();
				
			if($this->lquery($sql, DB_ECHO))
			{
				return $adresse->GetAdresseId();
			}
	}
	
	public function deleteAdresse($idAdresse)
	{
	
		if($idAdresse)
		{
			$sql='delete from adresse where adresseId = '.$idAdresse;
			
			if($this->lquery($sql, DB_ECHO))
				return true;
		}
		return true;
	}
	
	public function setGeoLocalisation($id, $lat, $lng, $updatedBy)
	{
		$sql = sprintf('update adresse set lat = %s, lng = %s, updatedBy = %s, updated = now() where adresseId = %s', $lat, $lng, $updatedBy, $id);
		
		$this->lquery($sql, DB_ECHO);
	}
}
?>