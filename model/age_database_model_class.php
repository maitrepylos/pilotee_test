<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class AgeDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($ageId = null)
    {
        $sql = 'select * from dic_age where 1';

        if (isset($ageId)) $sql .= ' and ageId = \'' . $ageId . '\'';

        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>