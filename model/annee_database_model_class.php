<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class AnneeDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($anneeId = null, $date = null)
	{

		$sql = 'select * from dic_annee where swActif = 1';
		
		if (isset($anneeId) && (strlen($anneeId) > 0))
		{
			$anneeId = $this->param_string($anneeId);			
			
			$sql .= ' and anneeId = \'' . $anneeId . '\'';
		}

		if (isset($date) && (strlen($date) > 0))
		{
			$sql .= ' and \'' . $date . '\' between dateDebut and dateFin';
		}
		
		$sql .= ' order by ordre;';
		
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}
	
	public function GetAnneeEnCoursId()
	{
		
		date_default_timezone_set('Europe/Paris');
		$now = date('Y-m-d H:i:s');
		$sql = 'select * from dic_annee where swActif = 1';
		$sql .= ' and (STR_TO_DATE(dateDebut, \'%Y-%m-%d\') <=\''.$now.'\' and STR_TO_DATE(dateFin, \'%Y-%m-%d\') >=\''.$now.'\')';
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}
}
?>