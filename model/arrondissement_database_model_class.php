<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ArrondissementDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($arrondissementId)
	{
		$sql = 'select * from dic_arrondissement where swActif = 1';
		
		if (isset($arrondissementId) && (strlen($arrondissementId) > 0))
		{
			$arrondissementId = $this->param_string($arrondissementId);			
			
			$sql .= ' and dic_arrondissementId = \'' . $arrondissementId . '\'';
		}
		
		$sql .= ' order by ordre;';
		
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}
}
?>