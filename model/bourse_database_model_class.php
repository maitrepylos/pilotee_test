<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class BourseDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}
	
	public function get($nomGen, $subventionObt, $subventionRefus, $subventionEval, $anneeScolaire, $dateAnte, $datePost, $etablissements,
						$actionValide, $actionEnCours, $refAse, $niveau, $degreNiveau, $degreNiveauFiliere, $section, $pedaAvec, $pedaSans,
						$aCommenter, $commenter, $global)
	{
		$user = Session::GetInstance()->getCurrentUser();
		$nomGen = $this->param_string($nomGen);
		$refAse = $this->param_string($refAse);
		$anneeScolaire = $this->param_string($anneeScolaire);

		if(isset($dateAnte)) $dateAnte = $this->param_date($dateAnte);
		if(isset($datePost)) $datePost = $this->param_date($datePost);
	
		$sql = 'select a.*'; 
		$sql .= ' from';
		$sql .= ' action a';
		
		if(!empty($etablissements))
			$sql .= ' left join action_etablissement ae on ae.actionId = a.actionId';

		if($degreNiveau!='-1' || $niveau!='-1' || $degreNiveauFiliere!='-1')
		{
			$sql .= ' left join action_filiereDegreNiveau fdn on fdn.actionId = a.actionId';
			$sql .= ' left join dic_filiereDegreNiveau dfdn on fdn.filiereDegreNiveauId  = dfdn.filiereDegreNiveauId';
		}
			
		if($section!='-1')
			$sql .= ' left join actionSectionAction sa on sa.actionId = a.actionId';
		
		if (isset($aCommenter) && isset($commenter)){}
		elseif(isset($aCommenter) || isset($commenter)){
			$sql .= ' left join bourse_agent ba on ba.actionId = a.actionId';
		}

		$sql .= ' where a.typeActionId = \''.BOURSES_PI.'\'';
		
		if(!empty($nomGen)) $sql .= ' and ucase(a.nomGen) like \'%' . strtoupper($nomGen) . '%\'';
		if(!empty($refAse)) $sql .= ' and ucase(a.refChezASE) like \'%' . strtoupper($refAse) . '%\'';
		
		if($subventionObt || $subventionRefus || $subventionEval)
		{
			$sql .=' and (';
			$first = true;
			if($subventionObt)
			{
				$or = 'or';
				if($first)
				{
					$or = '';
					$first = false;
				}
					
				$sql .= $or.' a.statutSubventionId = \''.SUBVENTION_OBTENU.'\'';
			}
			
			if($subventionRefus)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.statutSubventionId = \''.SUBVENTION_REJET.'\'';
			}
			
			if($subventionEval)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.statutSubventionId = \''.SUBVENTION_EVALUATION.'\'';
			}
			$sql .=')';
		}

		if(isset($actionValide) || isset($actionEnCours))
		{
			if(isset($actionValide) && isset($actionEnCours)){}
			else
			{
			 if(isset($actionValide))
			 {
			 	$sql .= ' and a.statutActionId = \''.$actionValide.'\'';
			 }
			 elseif(isset($actionEnCours))
			 	$sql .= ' and a.statutActionId = \''.$actionEnCours.'\'';
			}
		}		
		
		if($anneeScolaire!=-1)
		{
			if($anneeScolaire!=-1)
				$sql.=' and anneeId =\''.$anneeScolaire.'\'';
		}
		
		if(!empty($dateAnte) && !empty($datePost))
		{
			if($dateAnte == $datePost)
			{
				$sql.=' and STR_TO_DATE(dateAction, \'%Y-%m-%d\') =\''.$dateAnte.'\'';
			}
			else
			{
				$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') >=\''.$dateAnte.'\' and STR_TO_DATE(dateAction, \'%Y-%m-%d\') <=\''.$datePost.'\')';
			}		
		}
		elseif(!empty($dateAnte) || !empty($datePost))
		{
		 	if(!empty($dateAnte))
		 		$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') >=\''.$dateAnte.'\')';
		 		
		 	if(!empty($datePost))
		 		$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') <=\''.$datePost.'\')';
		}
		$i=0;
		if(!empty($etablissements))
		{
			$etablissements = explode(";",$etablissements);
			$sql.='AND (';
			
			foreach ($etablissements as $et)
			{
				if($i==0)
					$sql.=' ae.etablissementId ='.$et.'';
				else
					$sql.=' OR ae.etablissementId ='.$et.'';
				$i++;
			}
			
			if(isset($global) && $i>0)
				$sql.=' or (a.swGlobal=1)';
			
			$sql.=')';
		}
		
		if(isset($global) && $i==0)
			$sql.=' and a.swGlobal=1';
		
		if($niveau!='-1')
		{
			$niv = '';
			if($niveau == 'PRIMA')$niv = 'PRI';
			elseif($niveau == 'SECON')$niv='SEC';
			elseif($niveau == 'SUPER')$niv='SU';
			elseif($niveau == 'ENSEI')$niv='ENS';	
			elseif($niveau == 'PROMO')$niv='PSO';
			 
			$sql .= ' and fdn.filiereDegreNiveauId like \''.$niv.'%\'';
		}
			
		if($degreNiveau!='-1')
			$sql .= ' and dfdn.degreNiveauId = \''.$degreNiveau.'\'';
			
		if($degreNiveauFiliere!='-1')
			$sql .= ' and dfdn.filiereDegreNiveauId = \''.$degreNiveauFiliere.'\'';
		
		if($section!='-1')
			$sql .= ' and sa.sectionActionId = \''.$section.'\'';
		
		if(isset($pedaAvec) || isset($pedaSans))
		{
			if(isset($pedaAvec) && isset($pedaSans)){}
			else
			{
			 if(isset($pedaAvec))
			 {
			 	$sql .= ' and a.swPedagogie = 1';
			 }
			 elseif(isset($pedaSans))
			 	$sql .= ' and a.swPedagogie = 0';
			}
		}

		if(isset($aCommenter) && isset($commenter)){}
		else if(isset($aCommenter) || isset($commenter))
		{
			if (isset($aCommenter))
			{
				$sql .= 'and a.actionId not in (select actionId from `bourse_agent` where utilisateurId ='. $user->getUtilisateurId().')';
			}
			elseif (isset($commenter))
			{
				$sql .= ' and ba.utilisateurId = '.$user->getUtilisateurId();
			}
			$sql .= ' and a.statutSubventionId = \''.SUBVENTION_EVALUATION.'\'';
		}
		$sql .= ';';
		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function getRapport($projetInno, $anneeId, $global, $subventionObt, $subventionRefus, $subventionEval, $etablissements)
	{
		$user = Session::GetInstance()->getCurrentUser();
		$nomGen = $this->param_string($projetInno);
	
		$sql = 'select * from action a'; 
		if(isset($etablissements) && $etablissements->count()>0)
			$sql .= ' left join action_etablissement ae on ae.actionId = a.actionId ';

		$sql .= ' where a.typeActionId = \''.BOURSES_PI.'\'';
		
		$sql .= ' and a.statutActionId = \'VALID\'';
		$sql.=' and anneeId =\''.$anneeId.'\'';
		
		if(!empty($nomGen)) $sql .= ' and ucase(a.nomGen) like \'%' . strtoupper($nomGen) . '%\'';
		
		if($subventionObt || $subventionRefus || $subventionEval)
		{
			$sql .=' and (';
			$first = true;
			if($subventionObt)
			{
				$or = 'or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.statutSubventionId = \''.SUBVENTION_OBTENU.'\'';
			}
			
			if($subventionRefus)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.statutSubventionId = \''.SUBVENTION_REJET.'\'';
			}
			
			if($subventionEval)
			{
				$or = ' or';
				if($first)
				{
					$or = '';
					$first = false;
				}
				$sql .= $or.' a.statutSubventionId = \''.SUBVENTION_EVALUATION.'\'';
			}
			$sql .=')';
		}

		$i=0;
		if(isset($etablissements) && $etablissements->count()>0)
		{
			$sql.=' AND (';
			for($i=0; $i<$etablissements->count();$i++)
			{
				if($i==0)
					$sql.=' ae.etablissementId ='.$etablissements->items($i)->getEtablissementId().'';
				else
					$sql.=' OR ae.etablissementId ='.$etablissements->items($i)->getEtablissementId().'';
			}
			
			if(isset($global) && $i>0)
				$sql.=' OR (a.swGlobal=1) ';
			
			$sql.=')';
		}		
		
		
		if(isset($global) && $i==0)
			$sql.=' and a.swGlobal=1';
		
		$sql .= ';';
		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetById($id)
	{
		$sql = 'select * from action where 1';
		$sql .= sprintf(' and actionId = %s', $id);
		$sql .= ';';
		
		return $this->lquery($sql, DB_ECHO);
	}

	public function getStatutSubventionList($id=null)
	{
		$sql = 'select * from dic_statutSubvention  where swActif = 1 ';
		if(isset($id))
			$sql.= ' and statutSubventionId = \''.$id.'\'';
		$sql.=' order by ordre';
		return $this->lquery($sql, DB_ECHO);		
	}
	
	public function getBourseAgent($idAction=null, $idUser = null)
	{
		$sql = 'select * from bourse_agent  where 1 ';
		if(isset($idAction))
			$sql.= ' and actionId = \''.$idAction.'\'';

		if(isset($idUser))
			$sql.= ' and utilisateurId = \''.$idUser.'\'';
			
		$sql.=' order by dateEvaluation';
		
		return $this->lquery($sql, DB_ECHO);		
	}
	
	public function insertBourseAgent($bourseAgent)
	{
		$sql = 'INSERT INTO bourse_agent '.
			   '(utilisateurId, actionId, dateEvaluation, syntheseAvisId, avisQualitatif, eltImportant, soutientFiancier, '.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';
		
		
		$sql.= "".$bourseAgent->GetUtilisateurId().", ";
		$sql.= "".$bourseAgent->GetActionId().", ";
		$sql.= "'".$bourseAgent->GetDateEvaluation()."', ";
		$sql.= "".$bourseAgent->GetSyntheseAvisId().", ";
		$sql.= "'".$this->param_string($bourseAgent->GetAvisQualitatif())."', ";
		$sql.= "'".$this->param_string($bourseAgent->GetEltImportant())."', ";
		$sql.= "".$bourseAgent->GetSoutientFiancier().", ";
		$sql.= "'".$bourseAgent->GetCreated()."', ";
		$sql.= $bourseAgent->GetCreatedBy().", ";
		$sql.= "'".$bourseAgent->GetUpdated()."', ";
		$sql.= $bourseAgent->GetUpdatedBy()." ";
		$sql.=')';
		
		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}
	
	
	
}
?>