<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class CategActionNonLabelDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($categActionNonLabelId = null)
    {
        $sql = 'select * from dic_categActionNonLabel where 1';

        if (isset($categActionNonLabelId)) $sql .= ' and categActionNonLabelId = \'' . $categActionNonLabelId . '\'';

        $sql .= ' AND swActif = 1';        
        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>