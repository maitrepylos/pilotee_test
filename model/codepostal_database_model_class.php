<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class CodePostalDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($codePostal)
	{
		$sql = 'select * from dic_codepostal where swActif = 1';
		
		if (isset($codePostal) && (strlen($codePostal) > 0))
		{
			$codePostal = $this->param_string($codePostal);			
			
			$sql .= ' and codePostal = \'' . $codePostal . '\'';
		}
		
		$sql .= ';';
		
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}
	
	public function getArrondissements($idProvince)
	{
		$sql = 'select distinct ar.* from dic_codepostal cp left join dic_arrondissement ar on cp.arrondissementId = ar.dic_arrondissementId where cp.swActif = 1';
		
		if($idProvince!=null)
		{
			if(is_array($idProvince))
			{
				$sql.= ' and cp.provinceId in(';
				for($i=0; $i<count($idProvince); $i++)
				{
					$sql.= '\''.$idProvince[$i].'\', ';  
				}
				$sql = substr($sql, 0, strlen($sql)-2).')';
			}
			else
			{
				$sql.=' and cp.provinceId = \''.$idProvince.'\'';
			}
		}
		
		$rs = $this->lquery($sql, DB_ECHO);
		return $rs;
	}
}
?>