<?php

REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ContactDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}
	/**
	 * Enter description here...
	 *
	 * @param String $nom
	 * @param String $prenom
	 * @param String $etablissement
	 * @return Array

	 */

	public function SearchContact($nom, $prenom, $email, $etablissement, $swActif, $swInactif, $global)
	{
		$i=0;
		$nom = $this->param_string($nom);
		$prenom = $this->param_string($prenom);
		$email = $this->param_string($email);
		$etablissement = $this->param_string($etablissement);

		$sql = '
SELECT distinct c.*  
FROM contact c
LEFT JOIN adresse ad ON c.adresseId = ad.adresseId
LEFT JOIN contact_etablissement ce ON c.contactId = ce.contactId

LEFT JOIN dic_province p ON ad.dic_provinceId = p.dic_provinceId

WHERE 1 ';
		if(!empty($nom))
		$sql.='
AND c.nom like \'%'.$nom.'%\'';
		if(!empty($prenom))
		$sql.='
AND c.prenom like \'%'.$prenom.'%\'';
		if(!empty($email))
		$sql.='
AND (ad.email_1 like \'%'.$email.'%\' OR ad.email_2 like \'%'.$email.'%\')';		

		if(!empty($etablissement))
		{
			$etablissement = explode(";",$etablissement);
			$sql.='AND (';
			
			foreach ($etablissement as $et)
			{
				if($i==0)
					$sql.=' ce.etablissementId ='.$et.'';
				else
					$sql.=' OR ce.etablissementId ='.$et.'';
				$i++;
			}
			
			if(isset($global) && $i>0)
				$sql.=' or (ce.etablissementId is null and c.contactGlobal=1)';
			
			$sql.=')';
		}

		if(isset($swActif) && isset($swInactif))
		{

		}
		else
		{
			if(isset($swActif) && $swActif=='1')
			$sql.=' AND c.swActif = 1';

			if(isset($swInactif) && $swInactif=='0')
			$sql.=' AND c.swActif = 0';
		}

			if(isset($global) && $i==0)
				$sql.=' AND ce.etablissementId is null and c.contactGlobal=1';
				
		$sql.=' order by c.nom';
		
		$result = $this->lquery($sql, DB_ECHO);
		if($result && mysqli_num_rows($result) > 0)
		{
			return $result;
		}

		return null;
	}

	public function get($id=null)
	{
		$sql = 'select * from contact where 1';
		if(isset($id))
		$sql .=' AND contactId='.$id;
		
		$result = $this->lquery($sql, DB_ECHO);
		if($result)
		{
			return $result;
		}
		return null;
	}

	public function exist($nom, $prenom)
	{
		$nom = $this->param_string($nom);
		$prenom = $this->param_string($prenom);

		$sql = 'SELECT contactId, nom, prenom FROM contact where swActif = 1';

		$result = $this->lquery($sql, DB_ECHO);


		$ArrayDoublon = array();
		while ($row = mysqli_fetch_assoc($result))
		{
				
			$percentNom = 0;
			similar_text(strtolower($row['nom']), strtolower($nom), $percentNom);

			$percentPrenom = 0;
			similar_text(strtolower($row['prenom']), strtolower($prenom), $percentPrenom);

				
			if($percentPrenom>90 && $percentNom>90)
			$ArrayDoublon[] = array("id"=>$row['contactId']);
		}

		return $ArrayDoublon;
	}
	
	public function getLastContact()
	{
		$sql = 'select contactId from contact order by contactId  desc  limit 1';
		$result = $this->lquery($sql, DB_ECHO);
		$row = mysqli_fetch_assoc($result);
		return $row['contactId'];
	}

	public function insertContact($contact)
	{
		$sql = 'INSERT INTO contact '.
			   '(nom, prenom, adresseId, genreId, ageId, statutId, contactGlobal, swActif,'.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';

		//$sql.= "".$contact->GetContactId().", ";
			
		if($contact->GetNom()!=null) $sql.= "'".$this->param_string(strtoupper($contact->GetNom()))."', ";
		else $sql.= "null, ";

		if($contact->GetPrenom()!=null) $sql.= "'".$this->param_string($contact->GetPrenom())."', ";
		else $sql.= "null, ";

		if($contact->GetAdresseId()!=null) $sql.= "".$contact->GetAdresseId().", ";
		else $sql.= "null, ";
		
		if($contact->getGenreId()!=null) $sql.= "'".$contact->getGenreId()."', ";
		else $sql.= "null, ";

		if($contact->getAgeId()!=null) $sql.= "'".$contact->getAgeId()."', ";
		else $sql.= "null, ";
		
		if($contact->getStatutId()!=null) $sql.= "'".$contact->getStatutId()."', ";
		else $sql.= "null, ";
			
		$sql.= "".$contact->GetContactGlobal().", ";
			
			
		if($contact->GetSwActif()!=null)	$sql.= "".$contact->GetSwActif().", ";
		else $sql.= "null, ";

		$sql.= "'".$contact->GetCreated()."', ";
		$sql.= $contact->GetCreatedBy().", ";
		$sql.= "'".$contact->GetUpdated()."', ";
		$sql.= $contact->GetUpdatedBy()." ";
		$sql.=')';

		if($this->lquery($sql, DB_ECHO))
		{
			return $this->getLastInsertId();
		}
	}
	

	
	public function updateContact($contact)
	{
		$sql = 'UPDATE contact SET nom = ';

		if($contact->GetNom()!=null) $sql.= "'".$this->param_string(strtoupper($contact->GetNom()))."', ";
		else $sql.= "null, ";

		$sql.="prenom = ";
		if($contact->GetPrenom()!=null) $sql.= "'".$this->param_string($contact->GetPrenom())."', ";
		else $sql.= "null, ";
		
		$sql.="genreId = ";
		if($contact->getGenreId()!=null) $sql.= "'".$this->param_string($contact->getGenreId())."', ";
		else $sql.= "null, ";
		
		$sql.="ageId = ";
		if($contact->getAgeId()!=null) $sql.= "'".$this->param_string($contact->getAgeId())."', ";
		else $sql.= "null, ";
		
		$sql.="statutId = ";
		if($contact->getStatutId()!=null) $sql.= "'".$this->param_string($contact->getStatutId())."', ";
		else $sql.= "null, ";		

		$sql.="adresseId = ";
		if($contact->GetAdresseId()!=null) $sql.= "".$contact->GetAdresseId().", ";
		else $sql.= "null, ";
			
		$sql .= "contactGlobal = ";
		$sql.= "".$contact->GetContactGlobal().", ";

		$sql .= "swActif = ";
		if($contact->GetSwActif()!=null)	$sql.= "".$contact->GetSwActif().", ";
		else $sql.= "null, ";

		$sql.= "updated = '" . $contact->GetUpdated()."', ";
		$sql.= "updatedBy = " . $contact->GetUpdatedBy()." ";
			
		$sql.="WHERE contactId = ".$contact->GetContactId();


		if($this->lquery($sql, DB_ECHO))
		{
			return $contact->GetContactId();
		}
	}

	public function insertContactEtablissement($contactEtab,$origine=true)
	{
		$sql = 'INSERT INTO contact_etablissement '.
			   '(etablissementId, contactId, origineContactId, specialiteContactId, titreContactId, participationId,'.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';

		if($contactEtab->GetEtablissementId()!=null) $sql.= "".$contactEtab->GetEtablissementId().", ";
		else $sql.= "null, ";
			
		$sql.= "".$contactEtab->GetContactId().", ";
		$sql.= "'".$this->param_string($contactEtab->GetOrigineContactId())."', ";

		if($contactEtab->GetSpecialiteContactId()!=null) $sql.= "'".$this->param_string($contactEtab->GetSpecialiteContactId())."', ";
		else $sql.= "null, ";

		if($contactEtab->GetTitreContactId()!=null) $sql.= "'".$this->param_string($contactEtab->GetTitreContactId())."', ";
		else $sql.= "null, ";
			
		if($contactEtab->GetParticipationId()!=null) $sql.= "'".$this->param_string($contactEtab->GetParticipationId())."', ";
		else $sql.= "null, ";
			
		$sql.= "'".$contactEtab->GetCreated()."', ";
		$sql.= $contactEtab->GetCreatedBy().", ";
		$sql.= "'".$contactEtab->GetUpdated()."', ";
		$sql.= $contactEtab->GetUpdatedBy()." ";
		$sql.=')';

		if($this->lquery($sql, DB_ECHO))
		{
			return $this->getLastInsertId();
		}
	}

	public function insertContactAction($contactAction)
	{
		$sql = 'INSERT INTO action_contact '.
			   '(actionId, contactId, roleContactActionId,'.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';

			
		$sql.= "".$contactAction->GetActionId().", ";
		$sql.= "".$contactAction->GetContactId().", ";
		$sql.= "'".$this->param_string($contactAction->GetRoleContactActionId())."', ";
		$sql.= "'".$contactAction->GetCreated()."', ";
		$sql.= $contactAction->GetCreatedBy().", ";
		$sql.= "'".$contactAction->GetUpdated()."', ";
		$sql.= $contactAction->GetUpdatedBy()." ";
		$sql.=')';
		if($this->lquery($sql, DB_ECHO))
		{
			return $this->getLastInsertId();
		}
	}
	
	
	public function deleteContactEtablissement($contactId)
	{
		$sql = 'delete from contact_etablissement where contactId = '.$contactId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}
	
	public function deleteContactAction($contactId)
	{
		$sql = 'delete from action_contact where contactId = '.$contactId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}

	public function deleteContactFicheSuivi($contactId)
	{
		$sql = 'delete from ficheSuivi_contact where contactId = '.$contactId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}

	public function deleteRemiseMaterielPeda($contactId)
	{
		$sql = 'delete from remiseMaterielPedagEtab where contactId = '.$contactId;

		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}
	}	
	
	

	public function deleteContact($idcontact)
	{
		$sql ='delete from contact where contactId = '.$idcontact;

		if($this->lquery($sql, DB_ECHO))
		return true;
	}
	
	public function getRapport()
	{
		$sql = 'select distinct c.*, adr.dic_provinceId, p.label as province from contact c
				left join contact_etablissement ce on c.contactId = ce.contactId
				left join etablissement e on ce.etablissementId = e.etablissementId
				left join adresse adr on e.adresseId = adr.adresseId
				left join dic_province p on adr.dic_provinceId = p.dic_provinceId
				where adr.dic_provinceId is not null
				order by p.ordre';
		
		$result = $this->lquery($sql, DB_ECHO);
		$array['contact'] = array();
	
		while($row = mysqli_fetch_assoc($result))
		{
			$array['contact'][] = $row;
	
		}
		return $array;
	}
	
	public function listing($utilisateurId, $titre, $matiere, $dateAnte, $datePost, $cp_1, $cp_2, $personnesRes, $personnesResNot) 
	
	// $anneeId, $projEntr, $actionLab, $form, $cp_1, $cp_2, $arrondissementsArray)
	{
		$dateAnte = $this->param_date($dateAnte);
	    $datePost = $this->param_date($datePost);
	    
		
		$user = Session::GetInstance()->getCurrentUser();
		$sql = 'SELECT distinct c.*  
				FROM contact c
				INNER JOIN contact_etablissement ce ON c.contactId = ce.contactId
				INNER join etablissement e on ce.etablissementId = e.etablissementId
				LEFT join adresse adr on e.adresseId = adr.adresseId
				LEFT join dic_codepostal cp on cp.codepostal = adr.codepostal								
				LEFT join action_contact ac on c.contactId = ac.contactId
				LEFT join action a on ac.actionId = a.actionId ';

		// LEFT JOIN dic_specialitecontact spct on ce.specialiteContactId  = spct.specialiteContactId
		// LEFT JOIN dic_titrecontact tct on ce.titreContactId = tct.titreContactId
		
		// TODO : FFI : voir les parametres $personnesRes et $personnesResNot
		// => question d'analyse : omment voir une personne resource ????		
		
		
		if($user->isOperateur())
				$sql .= ' left join action_operateur op on op.actionId = a.actionId';	
		$sql .= 'WHERE 1 ';
				
		if (isset($utilisateurId))
			{
				if ($utilisateurId > 0) $sql .= ' and e.utilisateurId = ' .$utilisateurId;
				else $sql .= ' and e.utilisateurId is null';
			}
	
		if (isset($cp_1)) $sql .= ' and cast(cp.codepostal as signed) >= cast(\'' . $cp_1 .'\' as signed)';
		if (isset($cp_2)) $sql .= ' and cast(cp.codepostal as signed) <= cast(\'' . $cp_2 .'\' as signed)';
		
		// if(!empty($titre)) $sql .= ' and ucase(tct.label) like \'%' . strtoupper($titre) . '%\'';
		if(!empty($titre)) $sql .= ' and ce.titreContactId = \'' . $titre . '\'';
		
		// if(!empty($matiere)) $sql .= ' and ucase(spct.label) like \'%' . strtoupper($matiere) . '%\'';
		if(!empty($matiere)) $sql .= ' and ce.specialiteContactId = \'' . $matiere . '\'';
		
		if(!empty($dateAnte) && !empty($datePost))
		{
			if($dateAnte == $datePost)
			{
				$sql.=' and STR_TO_DATE(a.dateAction, \'%Y-%m-%d\') =\''.$dateAnte.'\'';
			}
			else
			{
				$sql.=' and (STR_TO_DATE(a.dateAction, \'%Y-%m-%d\') >=\''.$dateAnte.'\' and STR_TO_DATE(a.dateAction, \'%Y-%m-%d\') <=\''.$datePost.'\')';
			}		
		}
		elseif(!empty($dateAnte) || !empty($datePost))
		{
		 	if(!empty($dateAnte))
		 		$sql.=' and (STR_TO_DATE(a.dateAction, \'%Y-%m-%d\') >=\''.$dateAnte.'\')';
		 		
		 	if(!empty($datePost))
		 		$sql.=' and (STR_TO_DATE(a.dateAction, \'%Y-%m-%d\') <=\''.$datePost.'\')';
		}
		
		if (isset($personnesRes) && isset($personnesResNot)) {
				$sql .= '';
		}
		else { 
			
			if (isset($personnesRes)) {
				$sql .= " and ce.participationId = 2";			
			}
			
			if (isset($personnesResNot)) {
				$sql .= " and (ce.participationId = 1  or ce.participationId IS NULL)";			
			}
		}
		
		$sql .= ' order by c.nom';
		
		// FFI DEBUG
		//echo "<br>";
		//echo $sql;
		//echo "<br>";
		
		return $this->lquery($sql, DB_ECHO);
	}
	
	
}

?>