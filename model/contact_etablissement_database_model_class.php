<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ContactEtablissementDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($contactEtablissementId = null, $contactId= null, $etablissementId = null, $orderByContactName = false)
	{
		$sql = 'select contact_etablissement.* from contact_etablissement';
		if ($orderByContactName)
		{
			$sql .= ' LEFT JOIN contact c ON c.contactId = contact_etablissement.contactId ';
		}
		$sql .= ' where 1 ';

		if (isset($contactEtablissementId)) $sql .= ' and contactEtablissementId = ' . $contactEtablissementId;
		if (isset($contactId)) $sql .= ' and contactId = ' . $contactId;
		if (isset($etablissementId)) $sql .= ' and etablissementId = ' . $etablissementId;
		if ($orderByContactName)
		{
			$sql .= ' ORDER BY c.nom, c.prenom';
		}

		$sql .= ';';

		return $this->lquery($sql, DB_ECHO);
	}
	
	public function getSpecialite($actionId, $contactId) {
		$sql = ' select ds.label ' ;
		$sql .= ' from contact_etablissement ce ' ;
		$sql .= ' left join action_contact ac on (ac.contactId = ce.contactId) ' ;
		$sql .= ' left join action_etablissement ae on (ae.etablissementId = ce.etablissementId) ' ;
		$sql .= ' left join action a on (a.actionId = ae.actionId and a.actionId = ac.actionId) ' ;
		$sql .= ' left join contact c on (c.contactId = ac.contactId) ' ;
		$sql .= ' inner join dic_specialiteContact ds on (ds.specialiteContactId = ce.specialiteContactId) ' ;
		$sql .= ' where a.actionId = '. $actionId . ' and ac.contactId = ' . $contactId;
		$sql .= ';';
		return $this->lquery($sql, DB_ECHO);
	}
}
