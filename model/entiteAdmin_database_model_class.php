<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class EntiteAdminDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($entiteAdminId = null, $nom = null)
    {
        $sql = 'select * from entiteAdmin where 1';

        if (isset($entiteAdminId)) $sql .= ' and entiteAdminId = ' . $entiteAdminId;
        if (isset($nom)) $sql .= ' and nom like \'%' . $this->param_string($nom).'%\'';
        $sql .= ';';
        return $this->lquery($sql, DB_ECHO);
    }
    
    public function insert($entite)
    {
    	
		$sql = 'INSERT INTO entiteAdmin '.
			   '(nom, adresseId, website, swActif, created, createdBy, updated, updatedBy) VALUES ( ';
				 	 
					if($entite->getNom()!=null)	$sql.= "'".$this->param_string($entite->getNom())."', ";
					else $sql.= "null, ";

					if($entite->getAdresseId()!=null)	$sql.= $this->param_int($entite->getAdresseId()).", ";
					else $sql.= "null, ";					

					if($entite->getWebsite()!=null)	$sql.= "'".$this->param_string($entite->getWebsite())."', ";
					else $sql.= "null, ";

					$sql.= "".$this->param_int($entite->getSwActif()).", ";
										
					
					$sql.= "'".$entite->GetCreated()."', ";
					$sql.= $entite->GetCreatedBy().", ";
					$sql.= "'".$entite->GetUpdated()."', ";
					$sql.= $entite->GetUpdatedBy()." ";
				$sql.=')';
				
				if($this->lquery($sql, DB_ECHO))
				{
					return $this->getLastInsertId();
				} 
				   	
    }
    
    public function update($entite)
    {
		$sql = 'update entiteAdmin SET ';
		
		if($entite->getNom()!=null)	$sql.= "nom = '".$this->param_string($entite->getNom())."', ";
		else $sql.= "nom = null, ";
		
		if($entite->getAdresseId()!=null)	$sql.='adresseId ='. $this->param_int($entite->getAdresseId()).", ";
		else $sql.= "adresseId=null, ";

		if($entite->getWebsite()!=null)	$sql.= "website='".$this->param_string($entite->getWebsite())."', ";
		else $sql.= "website=null, ";

		$sql.= "swActif=".$this->param_int($entite->getSwActif()).", ";
		
		
		$sql .= ' updated = now()';
		$sql .= sprintf(', updatedBy = %s', $entite->GetUpdatedBy());
		$sql .= ' where entiteAdminId = ' . $entite->getEntiteAdminId();

		return $this->lquery($sql, DB_ECHO);
    }
    
    public function updateEtablissement($id)
    {
    	$sql = 'UPDATE etablissement set entiteAdminId=null where entiteAdminId='.$id;
    	return $this->lquery($sql, DB_ECHO);
    }
    
    public function delete($id)
    {
    	$sql = 'DELETE FROM entiteAdmin where entiteAdminId ='.$id;
    	return $this->lquery($sql, DB_ECHO);
    }
    
    public function search($nom = null, $actif=1)
    {
		$nom = $this->param_string($nom);
		
		$sql = 'select'; 
		$sql .= ' e.entiteAdminId as Etablissement_Id';
		$sql .= ',e.nom as Etablissement_Nom';
		$sql .= ',a.codepostal as Etablissement_CodePostal';
		$sql .= ',a.ville as Etablissement_Ville';
		$sql .= ',p.label as Etablissement_Province';
		$sql .= ',ar.label as Etablissement_Arrondissement';
		$sql .= ' from';
		$sql .= ' entiteAdmin e';
		$sql .= ' left join adresse a on a.adresseId = e.adresseId';
		$sql .= ' left join dic_province p on p.dic_provinceId = a.dic_provinceId';
		$sql .= ' left join dic_codepostal cp on cp.codepostal = a.codepostal';
		$sql .= ' left join dic_arrondissement ar on ar.dic_arrondissementId = cp.arrondissementId';
		$sql .= ' where 1';
		
		if (isset($nom)) $sql .= ' and e.nom like \'%' . $this->param_string($nom).'%\'';
		if(isset($actif))
			$sql .= ' and e.swActif = '.$actif;
		
		
		return $this->lquery($sql, DB_ECHO);    	
    }
    
    
    
}
?>