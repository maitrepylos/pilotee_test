<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class EtablissementDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($swActif, $utilisateurId, $nom, $cp_1, $cp_2, $provinceId, $arrondissementId, $niveau, $reseau)
	{
		$nom = $this->param_string($nom);

		$sql = 'select';
		$sql .= ' u.prenom as Agent_Prenom';
		$sql .= ',u.nom as Agent_Nom';
		$sql .= ',e.etablissementId as Etablissement_Id';
		$sql .= ',e.utilisateurId as UtilisateurId';
		$sql .= ',e.nom as Etablissement_Nom';
		$sql .= ',ne.label as Etablissement_Niveau';
		$sql .= ',re.label as Etablissement_Reseau';
		$sql .= ',a.codepostal as Etablissement_CodePostal';
		$sql .= ',a.ville as Etablissement_Ville';
		$sql .= ',p.label as Etablissement_Province';
		$sql .= ',ar.label as Etablissement_Arrondissement';
		$sql .= ' from';
		$sql .= ' etablissement e';
		$sql .= ' left join adresse a on a.adresseId = e.adresseId';
		$sql .= ' left join utilisateur u on u.utilisateurId = e.utilisateurId';
		$sql .= ' left join dic_niveauEtablissement ne on ne.niveauEtablissementId = e.niveauEtablissementId';
		$sql .= ' left join dic_reseauEtablissement re on re.reseauEtablissementId = e.reseauEtablissementId';
		$sql .= ' left join dic_province p on p.dic_provinceId = a.dic_provinceId';
		$sql .= ' left join dic_codepostal cp on cp.codepostal = a.codepostal';
		$sql .= ' left join dic_arrondissement ar on ar.dic_arrondissementId = cp.arrondissementId';
		$sql .= ' where 1';

		if (isset($swActif)) $sql .= ' and e.swActif = ' .$swActif;

		if (isset($utilisateurId))
		{
			if ($utilisateurId > 0) $sql .= ' and e.utilisateurId = ' .$utilisateurId;
			else $sql .= ' and e.utilisateurId is null';
		}

		//if (isset($nom)) $sql .= sprintf(' and ucase(e.nom) like concat(\'\%\', ucase(\'%s\'), \'\%\'', $nom);
		if (isset($nom)) $sql .= ' and ucase(e.nom) like \'%' . strtoupper($nom) . '%\'';

		if ((isset($cp_1)) && (isset($cp_2)))
		{
			$sql .= sprintf(' and cast(a.codepostal as signed) >= cast(\'%s\' as signed)', $cp_1);
			$sql .= sprintf(' and cast(a.codepostal as signed) <= cast(\'%s\' as signed)', $cp_2);
		}
		elseif (isset($cp_1)) $sql .= sprintf(' and cast(a.codepostal as signed) = cast(\'%s\' as signed)', $cp_1);
		elseif (isset($cp_2)) $sql .= sprintf(' and cast(a.codepostal as signed) = cast(\'%s\' as signed)', $cp_2);

		if (isset($provinceId)) $sql .= sprintf(' and a.dic_provinceId = \'%s\'', $provinceId);
		if (isset($arrondissementId)) $sql .= sprintf(' and cp.arrondissementId = \'%s\'', $arrondissementId);
		if (isset($niveau)) $sql .= sprintf(' and e.niveauEtablissementId = \'%s\'', $niveau);
		if (isset($reseau)) $sql .= sprintf(' and e.reseauEtablissementId = \'%s\'', $reseau);

		$sql .= ';';
		//echo $sql;

		return $this->lquery($sql, DB_ECHO);
	}

	public function getByEntiteAdmin($idEntiteAdmin)
	{
		$sql = 'select * from etablissement where 1';
		$sql .= sprintf(' and  entiteAdminId = %s', $idEntiteAdmin);
		$sql .= ';';

		return $this->lquery($sql, DB_ECHO);
	}

	public function GetById($id)
	{
		$sql = 'select * from etablissement where 1';
		if ($id != '') $sql .= sprintf(' and etablissementId = %s', $id);
		else $sql .= ' and etablissementId = -1';
		$sql .= ';';

		return $this->lquery($sql, DB_ECHO);
	}

	public function GetByIds($ids)
	{
		$sql = 'select * from etablissement where 1';
		$sql .= sprintf(' and etablissementId in (%s)', $ids);
		$sql .= ';';

		return $this->lquery($sql, DB_ECHO);
	}

	public function GetAgentEtablissement($idAgent)
	{
		$sql = 'select distinct * from etablissement where 1';
		$sql .= sprintf(' and utilisateurId = %s', $idAgent);
		$sql .= ';';

		return $this->lquery($sql, DB_ECHO);
	}

	public function SetAgent($etsIds, $agentId)
	{
		$sql = sprintf('update etablissement set utilisateurId = %s where etablissementId in (%s);', ($agentId == -2 ? 'null' : $agentId), $etsIds);

		return $this->lquery($sql, DB_ECHO);
	}

	public function report($swActif, $anneeId, $utilisateurId, $entiteAdminId, $nom, $cp1, $cp2, $provincesArray, $arrondissementsArray, $reseauxArray, $niveauxArray)
	{
		$sql = 'select distinct e.* from etablissement e';
		//$sql .= ' left join action_etablissement ae on ae.etablissementId = e.etablissementId';
		//$sql .= ' left join action act on act.actionId = ae.actionId';
		$sql .= ' left join adresse adr on adr.adresseId = e.adresseId';
		$sql .= ' left join dic_codepostal cp on cp.codepostal = adr.codepostal';
		$sql .= ' left join dic_province dp on dp.dic_provinceId = cp.provinceId';
		$sql .= ' left join dic_reseauEtablissement dr on dr.reseauEtablissementId = e.reseauEtablissementId';
		$sql .= ' left join dic_niveauEtablissement dn on dn.niveauEtablissementId = e.niveauEtablissementId';
		$sql .= ' left join entiteAdmin ea on ea.entiteAdminId = e.entiteAdminId';
		$sql .= ' where 1';

		if (isset($swActif)) $sql .= ' and e.sWActif = ' . $swActif;
		//if (isset($anneeId)) $sql .= ' and act.anneeId = \'' . $anneeId . '\'';

		if (isset($utilisateurId))
		{
			if ($utilisateurId > 0) $sql .= ' and e.utilisateurId = ' .$utilisateurId;
			else $sql .= ' and e.utilisateurId is null';
		}

		if (isset($entiteAdminId)) $sql .= ' and ea.nom like concat(\'%\', \'' . $this->param_string($entiteAdminId) . '\', \'%\')';
		if (isset($nom)) $sql .= ' and e.nom like concat(\'%\', \'' . $this->param_string($nom) . '\', \'%\')';
		if (isset($cp1)) $sql .= ' and cast(cp.codepostal as signed) >= cast(\'' . $cp1 .'\' as signed)';
		if (isset($cp2)) $sql .= ' and cast(cp.codepostal as signed) <= cast(\'' . $cp2 .'\' as signed)';

		if (isset($provincesArray) && (count($provincesArray) > 0))
		{
			$provinces = null;
				
			for ($i = 0; $i < count($provincesArray); $i++)
				$provinces .= (isset($provinces) ? ',' : '') . '\'' . $provincesArray[$i] . '\'';
				
			$sql .= ' and cp.provinceId in (' . $provinces . ')';
		}

		if (isset($arrondissementsArray) && (count($arrondissementsArray) > 0))
		{
			$arrondissements = null;
				
			for ($i = 0; $i < count($arrondissementsArray); $i++)
				$arrondissements .= (isset($arrondissements) ? ',' : '') . '\'' . $arrondissementsArray[$i] . '\'';
				
			$sql .= ' and cp.arrondissementId in (' . $arrondissements . ')';
		}

		if (isset($reseauxArray) && (count($reseauxArray) > 0))
		{
			$reseaux = null;
				
			for ($i = 0; $i < count($reseauxArray); $i++)
				$reseaux .= (isset($reseaux) ? ',' : '') . '\'' . $reseauxArray[$i] . '\'';
				
			$sql .= ' and dr.reseauEtablissementId in (' . $reseaux . ')';
		}

		if (isset($niveauxArray) && (count($niveauxArray) > 0))
		{
			$niveaux = null;
				
			for ($i = 0; $i < count($niveauxArray); $i++)
				$niveaux .= (isset($niveaux) ? ',' : '') . '\'' . $niveauxArray[$i] . '\'';
				
			$sql .= ' and dn.niveauEtablissementId in (' . $niveaux . ')';
		}

		return $this->lquery($sql, DB_ECHO);
	}
	
	/**
	 * @author Anne-Lise Lambin
	 * Listing pour les établissements
	 */
	public function listing($activite, $utilisateurId, $cp_1, $cp_2, $arrondissementsArray)
	{
		$sql = 'select distinct e.* from etablissement e';
		// FFI $sql .= ' left join action_etablissement ae on ae.etablissementId = e.etablissementId';
		$sql .= ' inner join action_etablissement ae on ae.etablissementId = e.etablissementId';
		// FFI $sql .= ' left join action act on act.actionId = ae.actionId';
		$sql .= ' inner join action act on act.actionId = ae.actionId';
		$sql .= ' left join adresse adr on adr.adresseId = e.adresseId';
		$sql .= ' left join dic_codepostal cp on cp.codepostal = adr.codepostal';
		$sql .= ' left join dic_province dp on dp.dic_provinceId = cp.provinceId';
		$sql .= ' left join dic_reseauEtablissement dr on dr.reseauEtablissementId = e.reseauEtablissementId';
		$sql .= ' left join dic_niveauEtablissement dn on dn.niveauEtablissementId = e.niveauEtablissementId';
		$sql .= ' left join entiteAdmin ea on ea.entiteAdminId = e.entiteAdminId';
		$sql .= ' where 1';
		
		if(isset($activite))
		{
			$sql .= ' and act.nomGen = "'.$activite.'"';
		}
		
		if (isset($utilisateurId))
		{
			if ($utilisateurId > 0) $sql .= ' and e.utilisateurId = ' .$utilisateurId;
			else $sql .= ' and e.utilisateurId is null';
		}

		if (isset($cp_1)) $sql .= ' and cast(cp.codepostal as signed) >= cast(\'' . $cp_1 .'\' as signed)';
		if (isset($cp_2)) $sql .= ' and cast(cp.codepostal as signed) <= cast(\'' . $cp_2 .'\' as signed)';

		if (isset($arrondissementsArray) && (count($arrondissementsArray) > 0))
		{
			$arrondissements = null;
				
			for ($i = 0; $i < count($arrondissementsArray); $i++)
				$arrondissements .= (isset($arrondissements) ? ',' : '') . '\'' . $arrondissementsArray[$i] . '\'';
				
			$sql .= ' and cp.arrondissementId in (' . $arrondissements . ')';
		}
		
		$sql .= ' order by e.nom';
		
		return $this->lquery($sql, DB_ECHO);
	}

	public function insert($etab)
	{
		$sql = 'INSERT INTO etablissement '.
				'(utilisateurId, nom, adresseId, website, niveauEtablissementId, reseauEtablissementId, entiteAdminId, swActif,'.
				' created, createdBy, updated, updatedBy) VALUES ( ';
			
			
		if($etab->GetUtilisateurId()!=null) $sql.= "".$etab->GetUtilisateurId().", ";
		else $sql.= "null, ";
			
		if($etab->GetNom()!=null) $sql.= "'".$this->param_string($etab->GetNom())."', ";
		else $sql.= "null, ";

		if($etab->GetAdresseId()!=null) $sql.= "".$etab->GetAdresseId().", ";
		else $sql.= "null, ";

		if($etab->GetWebsite()!=null) $sql.= "'".$this->param_string($etab->GetWebsite())."', ";
		else $sql.= "null, ";
			
		if($etab->GetNiveauEtablissementId()!=null) $sql.= "'".$etab->GetNiveauEtablissementId()."', ";
		else $sql.= "null, ";
			
		if($etab->GetReseauEtablissementId()!=null)	$sql.= "'".$etab->GetReseauEtablissementId()."', ";
		else $sql.= "null, ";
			
		if($etab->GetEntiteAdminId()!=null)	$sql.= "".$etab->GetEntiteAdminId().", ";
		else $sql.= "null, ";
			
		if($etab->GetSwActif()!=null)	$sql.= "'".$etab->GetSwActif()."', ";
		else $sql.= "null, ";
			
		$sql.= "'".$etab->GetCreated()."', ";
		$sql.= "1, ";
		$sql.= "'".$etab->GetUpdated()."', ";
		$sql.= "1 ";
		$sql.=')';
			
		if($this->lquery($sql, DB_ECHO))
		{
			return $etab->GetEtablissementId();
		}
	}

	public function update($etab)
	{
		$sql = 'UPDATE etablissement SET ';
			
		if($etab->GetUtilisateurId()!=null) $sql.= "utilisateurId=".$etab->GetUtilisateurId().", ";
		else $sql.= "utilisateurId=null, ";
			
		if($etab->GetNom()!=null) $sql.= "nom='".$this->param_string($etab->GetNom())."', ";
		else $sql.= "nom=null, ";

		if($etab->GetAdresseId()!=null) $sql.= "adresseId=".$etab->GetAdresseId().", ";
		else $sql.= "adresseId=null, ";

		if($etab->GetWebsite()!=null) $sql.= "website='".$this->param_string($etab->GetWebsite())."', ";
		else $sql.= "website=null, ";
			
		if($etab->GetNiveauEtablissementId()!=null) $sql.= "niveauEtablissementId='".$etab->GetNiveauEtablissementId()."', ";
		else $sql.= "niveauEtablissementId=null, ";
			
		if($etab->GetReseauEtablissementId()!=null)	$sql.= "reseauEtablissementId='".$etab->GetReseauEtablissementId()."', ";
		else $sql.= "reseauEtablissementId=null, ";
			
		if($etab->GetEntiteAdminId()!=null)	$sql.= "entiteAdminId=".$etab->GetEntiteAdminId().", ";
		else $sql.= "entiteAdminId=null, ";
			
		if($etab->GetSwActif()!=null)	$sql.= "swActif='".$etab->GetSwActif()."', ";
		else $sql.= "swActif=null, ";

		$sql .= ' updated = now()';
		$sql .= sprintf(', updatedBy = %s', $etab->GetUpdatedById());
		$sql .= ' where etablissementId = ' . $etab->GetEtablissementId();

		if($this->lquery($sql, DB_ECHO))
		{
			return $etab->GetEtablissementId();
		}
	}

}
?>