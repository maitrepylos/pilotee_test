<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class FicheSuiviContactDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function GetFicheSuiviContacts($ficheSuiviId = null)
	{
		$sql = 'select c.* from ficheSuivi_contact fsc inner join contact c on fsc.contactId = c.contactId where 1';

		if (isset($ficheSuiviId)) $sql .= ' and fsc.ficheSuiviId = ' . $ficheSuiviId;

		$sql .= ';';

		return $this->lquery($sql, DB_ECHO);
	}
	
	public function delete($ficheSuiviId=null, $contactId=null)
	{
		$sql = 'delete from ficheSuivi_contact where true';
		
		if (isset($ficheSuiviId)) $sql .= sprintf(' and ficheSuiviId = %s', $ficheSuiviId);
		if (isset($contactId))
		{
			if (is_numeric($contactId)) $sql .= sprintf(' and contactId = %s', $contactId);
			else $sql .= sprintf(' and contactId in %s', $contactId);
		}
		
		$sql .= ';';
		
		return $this->lquery($sql, DB_ECHO);
	}
	
}
?>