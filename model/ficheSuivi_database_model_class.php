<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class FicheSuiviDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($ficheSuiviId = null, $etablissementId = null, $orderDESC = false)
	{
		$sql = 'select * from ficheSuivi where 1';
		
		if (isset($ficheSuiviId)) $sql .= ' and ficheSuiviId = ' . $ficheSuiviId;
		if (isset($etablissementId)) $sql .= ' and etablissementId = ' . $etablissementId;
		
		$sql .= ' order by dateRencontre ' . ($orderDESC?'DESC' : 'ASC') . ';';
		
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}
	
	public function getRapport($agent, $dateAnte, $datePost)
	{
		$dateAnte = $this->param_date($dateAnte);
	    $datePost = $this->param_date($datePost);
	    
		$sql = 'select * from ficheSuivi fs ';
		$sql.='LEFT JOIN etablissement e on fs.etablissementId = e.etablissementId ';
		$sql.= 'where 1';
		$sql.= ' and ((fs.createdBy = '.$agent. ' OR fs.updatedBy = '.$agent.') OR (e.utilisateurId='.$agent.'))';
		$sql .= ' AND fs.dateRencontre >=\''.$dateAnte.'\' AND fs.dateRencontre<=\''.$datePost.'\' '; 
		$sql .= ' order by fs.dateRencontre;';
		//echo $sql;

		$rs = $this->lquery($sql, DB_ECHO);
		return $rs;
	}
	
	public function update($fiche)
	{
		$sql = 'update ficheSuivi';
		
		$commentaire = $this->param_string($fiche->GetCommentaire());

		if ($fiche->GetModeSuiviId()!=null) $sql .= sprintf(' set modeSuiviId = \'%s\'', $fiche->GetModeSuiviId()); else $sql .= ' set modeSuiviId = null';	
		if ($fiche->GetIntentionSuiviId()!=null) $sql .= sprintf(', intentionSuiviId = \'%s\'', $fiche->GetIntentionSuiviId()); else $sql .= ', intentionSuiviId = null';	
		if ($fiche->GetTimingSuiviId()!=null) $sql .= sprintf(', timingSuiviId = \'%s\'', $fiche->GetTimingSuiviId()); else $sql .= ', timingSuiviId = null';	
		if ($commentaire!=null) $sql .= sprintf(', commentaire = \'%s\'', $commentaire); else $sql .= ', commentaire = null';
		if ($fiche->GetDateRencontre()!=null) $sql .= sprintf(', dateRencontre = \'%s\'', $this->param_date($fiche->GetDateRencontre())); else $sql .= ', dateRencontre = null';
		
		$sql .= ', updated = now()';
		$sql .= sprintf(', updatedBy = %s', $fiche->GetUpdatedBy());
		$sql .= ' where ficheSuiviId = ' . $fiche->GetFicheSuiviId();

		return $this->lquery($sql, DB_ECHO);
	}
	
	
	public function deleteFicheContact($ficheSuiviId=null, $contactId=null)
	{
		$sql = 'delete from ficheSuivi_contact where true';
		
		if (isset($ficheSuiviId)) $sql .= sprintf(' and ficheSuiviId = %s', $ficheSuiviId);
		if (isset($contactId))
		{
			if (is_numeric($contactId)) $sql .= sprintf(' and contactId = %s', $contactId);
			else $sql .= sprintf(' and contactId in %s', $contactId);
		}
		
		$sql .= ';';
		
		return $this->lquery($sql, DB_ECHO);
	}
	
	
	public function insert($fiche)
	{
		$sql = 'INSERT INTO ficheSuivi (modeSuiviId, etablissementId, intentionSuiviId, timingSuiviId, commentaire, dateRencontre,'.
										' created, createdBy, updated, updatedBy) VALUES (';
		
			if($fiche->GetModeSuiviId()!=null) $sql.= "'".$this->param_string($fiche->GetModeSuiviId())."', ";
			else $sql.= "null, ";

			$sql.= "".$fiche->GetEtablissementId().", ";
			
			if($fiche->GetIntentionSuiviId()!=null) $sql.= "'".$this->param_string($fiche->GetIntentionSuiviId())."', ";
			else $sql.= "null, ";
			
			if($fiche->GetTimingSuiviId()!=null) $sql.= "'".$this->param_string($fiche->GetTimingSuiviId())."', ";
			else $sql.= "null, ";
			
			if($fiche->GetCommentaire()!=null) $sql.= "'".$this->param_string($fiche->GetCommentaire())."', ";
			else $sql.= "null, ";
			
			if($fiche->GetDateRencontre()!=null) $sql.= "'".$this->param_date($fiche->GetDateRencontre())."', ";
			else $sql.= "null, ";

	        $sql.= "'".$fiche->GetCreated()."', ";
			$sql.= $fiche->GetCreatedBy().", ";
			$sql.= "'".$fiche->GetUpdated()."', ";
			$sql.= $fiche->GetUpdatedBy()." ";
			$sql.=')';

			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}	
	}
	
	public function insertFicheContact($ficheC)
	{
		$sql = 'INSERT INTO ficheSuivi_contact (contactId, ficheSuiviId,'.
												' created, createdBy, updated, updatedBy) VALUES (';

			$sql.= "".$ficheC->GetContactId().", ";
			$sql.= "".$ficheC->GetFicheSuiviId().", ";
	        $sql.= "'".$ficheC->GetCreated()."', ";
			$sql.= $ficheC->GetCreatedBy().", ";
			$sql.= "'".$ficheC->GetUpdated()."', ";
			$sql.= $ficheC->GetUpdatedBy()." ";
			$sql.=')';
			
			if($this->lquery($sql, DB_ECHO))
				return true;
	}
	
	public function delete($ficheSuiviId)
	{
		return $this->lquery(sprintf('delete from ficheSuivi where ficheSuiviId = %s;', $ficheSuiviId), DB_ECHO);
		
	}
}
?>