<?php

REQUIRE_ONCE(SCRIPTPATH . 'lib/abstractDatabase.php');

class FormateurDatabase extends AbstractDatabase
{
	/**
	 * R�cup�re les ids des formateur qui li� � cet utilisateur
	 * @param int $utilisateurId
	 * @param bool $limit si true alors on ne renvoi que un et un seul r�sultat
	 */
	
	public function get($formateurId = null, $actif = false)
	{
		$sql = 'select * from formateur where 1';
		if (isset($formateurId) && (strlen($formateurId) > 0))
		{
			$formateurId = $this->param_string($formateurId);
			$sql .= ' and formateurId = ' . $formateurId;
		}

		if($actif)
			$sql .= ' and swActif = 1';

		//echo $sql;


		$sql .= ' order by swActif DESC, nom ASC;';

		$rs = $this->lquery($sql, DB_ECHO);
		return $rs;
	}
	
	public function getForAction($actionid) {
		$sql  =  'select * from formateur f';
		$sql .= ' left join action_formateur af on f.formateurId = af.formateurId';
		$sql .= ' where af.actionId = ' . $actionid;
		
		return $this->lquery($sql, DB_ECHO);
	
	}
	
	public function getFormateursIdWithUtilisateurId($utilisateurId = -1, $limit = false)
	{
		$sql = 'SELECT formateurId from formateur_utilisateur WHERE 1';
		
		if ($utilisateurId != "-1") $sql .=  ' and utilisateurId = ' . intval($utilisateurId); // FFI
		
		if ($limit)
		{
			$sql .= ' LIMIT 1';
		}
		
		// echo $sql;

		return $this->lquery($sql, DB_ECHO);
	}

	protected function _getTableName()
	{
		return 'formateur';
	}
	
	protected function _getIdName()
	{
		return 'formateurId';
	}
}

# EOF
