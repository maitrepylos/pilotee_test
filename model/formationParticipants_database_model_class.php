<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class FormationParticipantsDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }
    
	function addParticipant($elt) {
		$sql =  'insert into formation_participant (' .
				'nom, prenom, formationId, tel1, tel2, email1, email2, genreId, ageId, etablissementOrigineId) VALUES (' .
				'\'' . $elt->nom . '\', \'' . $elt->prenom  . '\', \'' . $elt->formation_id  . '\', \'' . $elt->telephone1  . '\', \'' .  $elt->telephone2 . '\', \'' . $elt->email1 . '\', \'' . $elt->email2 . '\', \'' . $elt->genre . '\', \'' . $elt->age . '\', \'' . $elt->etablissement_id  . '\')' 				
				;
		
		if($this->lquery($sql))
		{
			$elt->id = $this->getLastInsertId();
			return $elt->id ;
		}
		else	
			return false;
		
	}
	
	function deleteParticipant($id) {
		$sql =  'delete from formation_participant where participantId = ' . $id;
		
		return $this->lquery($sql, DB_ECHO);
			
	}
	
	function updateParticipant($elt) {
		$sql = 'update formation_participant set '
					. 'nom = ' . "'" . $elt->nom . "', "
					. 'prenom = ' . "'" . $elt->prenom . "', "
					. 'tel1 = ' . "'" . $elt->telephone1 . "', "
					. 'tel2 = ' . "'" . $elt->telephone2 . "', "
					. 'email1 = ' . "'" . $elt->email1 . "', "
					. 'email2 = ' . "'" . $elt->email2 . "', "
					. 'genreId = ' . "'" . $elt->genre . "', "
					. 'ageId = ' . "'" . $elt->age . "', "
					. 'etablissementOrigineId = ' . "'" . $elt->etablissement_id . "' "
					. 'where participantId = ' . $elt->id;
		
		// echo '<br>' . $sql . '<br>';
		
		return $this->lquery($sql, DB_ECHO);
		
	}
	
	function getParticipants($formationID) {
	
		$sql = 'select * from formation_participant where formationId = ' . $formationID;
		//echo '<br> ' . $sql . '<br>';
		
		return $this->lquery($sql, DB_ECHO);
	
	
	}
	
	function getParticipantForId($participantID) {
	
		$sql = 'select * from formation_participant where participantId = ' . $participantID;
		//echo '<br> ' . $sql . '<br>';
		
		return $this->lquery($sql);
	
	
	}

    
}
?>