<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class FormationDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}
	
	public function getAll($id = -1){
		
		$user = Session::GetInstance()->getCurrentUser();
		
		$sql =  'select distinct a.*';
		$sql .= ' from ';
		$sql .= ' action a';
		//if($user->isFormateur())
		//	$sql .= ' left join action_formateur of on of.actionId = a.actionId';
				
		$sql .= ' where 1 ';
		
		if ($id != -1) $sql .= ' and a.actionID = ' . $id;
		
		$sql .= ' and (a.typeActionId = \''. 'FOR' .'\'' . ' OR ' . ' a.typeActionId = \''. 'STAC' .'\'' . ' OR ' . ' a.typeActionId = \''. 'ATE' .'\'' . ')';
		 
		//if($operateurId!='-1')
		//{
		//	$sql .= ' and (a.operateurId = '.$operateurId;
		//	if($user->isOperateur())
		//		$sql.=' OR op.operateurId ='.$operateurId;
		//	$sql.=')';
		//}
		//$sql.=')';
		
		$sql .= ';';
		//echo "<br>DEBUG FFI<br>";
		//echo $sql;
		//echo "<br>DEBUG FFI<br>";
		return $this->lquery($sql, DB_ECHO);
		
	}

	public function get($nomSpec, $anneeScolaire, $dateAnte, $datePost, $formateur_utilisateurId,
						$typeFormation, $actionValide, $actionEnCours, $niveau, $degreNiveau, $degreNiveauFiliere, $global = null)
	{
		$user = Session::GetInstance()->getCurrentUser();
		$nomSpec = $this->param_string($nomSpec);
		$anneeScolaire = $this->param_string($anneeScolaire);
		if(isset($dateAnte)) $dateAnte = $this->param_date($dateAnte);
		if(isset($datePost)) $datePost = $this->param_date($datePost);
			
		$sql = 'select distinct a.*'; 
		$sql .= ' from';
		$sql .= ' action a';
		
		if($degreNiveau!='-1' || $niveau!='-1' || $degreNiveauFiliere!='-1')
		{
			$sql .= ' left join action_filiereDegreNiveau fdn on fdn.actionId = a.actionId';
			$sql .= ' left join dic_filiereDegreNiveau dfdn on fdn.filiereDegreNiveauId  = dfdn.filiereDegreNiveauId';
		}
			
		$sql .= ' where 1 '; //and a.typeActionId != \''. BOURSES_PI .'\'';
		
		if (count($typeFormation) == 0)
			$sql .= ' and (a.typeActionId = \''. 'FOR' .'\'' . ' OR ' . ' a.typeActionId = \''. 'STAC' .'\'' . ' OR ' . ' a.typeActionId = \''. 'ATE' .'\'' . ')';
		else {
			for ($i=0 ; $i < count($typeFormation); $i++) {
				if ($i == 0) $sql .= ' and (a.typeActionId = \'' . $typeFormation[$i] . '\'';
				else $sql .= ' OR a.typeActionId = \''. $typeFormation[$i] .'\'';
				if ($i == (count($typeFormation)-1) ) $sql .= ')';				
			}			
		}
				
		if(!empty($nomSpec)) $sql .= ' and ucase(a.nomSpec) like \'%' . strtoupper($nomSpec) . '%\'';
		
		if(isset($actionValide) || isset($actionEnCours))
		{
			if(isset($actionValide) && isset($actionEnCours)){}				
			else
			{
			 if(isset($actionValide))
			 {
			 	$sql .= ' and a.statutActionId = \''.$actionValide.'\'';
			 }
			 elseif(isset($actionEnCours))
			 	$sql .= ' and a.statutActionId = \''.$actionEnCours.'\'';
			}
		}
		
		
		if($anneeScolaire!=-1)
		{
			$sql.=' and anneeId =\''.$anneeScolaire.'\'';
		}
		
		if($formateur_utilisateurId!=-1)
		{
			$sql.=' and operateurId =\''. $formateur_utilisateurId  .'\'';
		}
		
		if(!empty($dateAnte) && !empty($datePost))
		{
			if($dateAnte == $datePost)
			{
				$sql.=' and STR_TO_DATE(dateAction, \'%Y-%m-%d\') =\''.$dateAnte.'\'';
			}
			else
			{
				$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') >=\''.$dateAnte.'\' and STR_TO_DATE(dateAction, \'%Y-%m-%d\') <=\''.$datePost.'\')';
			}		
		}
		elseif(!empty($dateAnte) || !empty($datePost))
		{
		 	if(!empty($dateAnte))
		 		$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') >=\''.$dateAnte.'\')';
		 		
		 	if(!empty($datePost))
		 		$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') <=\''.$datePost.'\')';
		}
		
		
		if(isset($global)) //  && $i==0)
				$sql.=' and a.swGlobal=1';
		
		if($niveau!='-1')
		{
				$niv = '';
			if($niveau == 'PRIMA')
			 	$niv = 'PRI';
			 elseif($niveau == 'SECON')
			 	$niv='SEC';
			 elseif($niveau == 'SUPER')
			 	$niv='SU';
			 elseif($niveau == 'ENSEI')
			 	$niv='ENS';	
			 elseif($niveau == 'PROMO')
			 	$niv='PSO';
			 
			$sql .= ' and fdn.filiereDegreNiveauId like \''.$niv.'%\'';
		}
			
		if($degreNiveau!='-1')
			$sql .= ' and dfdn.degreNiveauId = \''.$degreNiveau.'\'';
			
		if($degreNiveauFiliere!='-1')
			$sql .= ' and dfdn.filiereDegreNiveauId = \''.$degreNiveauFiliere.'\'';
		
		$sql .= ';';
		
		// echo "<br>debug FFI<br>";
		// echo $sql;
		// echo "<br>end debug FFI<br>";
		
		
		return $this->lquery($sql, DB_ECHO);
	}
	

	public function listing($nomSpec, $operateurId, $anneeAnt, $anneePost)
						
	//listing($intitule, $utilisateurId, $anneeAnt, $anneePost, $cp_1, $cp_2); 
	
	{
		$user = Session::GetInstance()->getCurrentUser();
		$nomSpec = $this->param_string($nomSpec);
		if(isset($anneeAnt)) $dateAnte = $this->param_date($anneeAnt);
		if(isset($anneePost)) $datePost = $this->param_date($anneePost);
			
		$sql = 'select distinct a.*'; 
		$sql .= ' from';
		$sql .= ' action a';
			
		$sql .= ' where 1 '; //and a.typeActionId != \''. BOURSES_PI .'\'';
		
		$sql .= ' and (a.typeActionId = \''. 'FOR' .'\'' . ' OR ' . ' a.typeActionId = \''. 'ATE' .'\'' . ')';
		// ' a.typeActionId = \''. 'STAC' .'\'' . ' OR ' .
		
				
		if(!empty($nomSpec)) $sql .= ' and ucase(a.nomSpec) like \'%' . strtoupper($nomSpec) . '%\'';
		
		/* FFI : on prend aussi les formations "brouillon" ???
		 * la, j'ai laiss� le plus ouvert possible, donc aussi les "brouillon"
		 * 
		if(isset($actionValide) || isset($actionEnCours))
		{
			if(isset($actionValide) && isset($actionEnCours)){}				
			else
			{
			 if(isset($actionValide))
			 {
			 	$sql .= ' and a.statutActionId = \''.$actionValide.'\'';
			 }
			 elseif(isset($actionEnCours))
			 	$sql .= ' and a.statutActionId = \''.$actionEnCours.'\'';
			}
		}*/ 
		
		
		if(!empty($dateAnte) && !empty($datePost))
		{
			if($dateAnte == $datePost)
			{
				$sql.=' and STR_TO_DATE(dateAction, \'%Y-%m-%d\') =\''.$dateAnte.'\'';
			}
			else
			{
				$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') >=\''.$dateAnte.'\' and STR_TO_DATE(dateAction, \'%Y-%m-%d\') <=\''.$datePost.'\')';
			}		
		}
		elseif(!empty($dateAnte) || !empty($datePost))
		{
		 	if(!empty($dateAnte))
		 		$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') >=\''.$dateAnte.'\')';
		 		
		 	if(!empty($datePost))
		 		$sql.=' and (STR_TO_DATE(dateAction, \'%Y-%m-%d\') <=\''.$datePost.'\')';
		}
		
		$sql .= ';';
		
		/*echo "<br>debug FFI<br>";
		echo $sql;
		echo "<br>end debug FFI<br>";
		*/
		
		
		return $this->lquery($sql, DB_ECHO);
	}
	
}
?>