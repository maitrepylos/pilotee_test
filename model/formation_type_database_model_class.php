<?php

REQUIRE_ONCE(SCRIPTPATH . 'lib/abstractDatabase.php');

class FormationTypeDatabase extends AbstractDatabase
{
	protected function _getTableName()
	{
		return 'dic_typeFormation';
	}
	
	protected function _getIdName()
	{
		return 'typeFormationId';
	}
}

# EOF
