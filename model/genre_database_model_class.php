<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class GenreDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($genreId = null)
    {
        $sql = 'select * from dic_genre where 1';

        if (isset($genreId)) $sql .= ' and genreId = \'' . $genreId . '\'';

        $sql .= ';';
        return $this->lquery($sql, DB_ECHO);
    }
}
?>