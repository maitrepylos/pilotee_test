<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class HistoriqueEncodageDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($historiqueEncodageId = null, $contactId=null, $ficheSuiviId=null, $remiseMaterielId=null, $actionId=null)
    {
        $sql = 'select * from historiqueEncodage where 1';

        if (isset($historiqueEncodageId)) $sql .= ' and historiqueEncodageId = ' . $historiqueEncodageId;
        
        if (isset($contactId)) $sql .= ' and contactId = ' . $contactId;
        if (isset($ficheSuiviId)) $sql .= ' and ficheSuiviId = ' . $ficheSuiviId;
        if (isset($remiseMaterielId)) $sql .= ' and remiseMaterielId = ' . $remiseMaterielId;
        if (isset($actionId)) $sql .= ' and actionId = ' . $actionId;
		$sql.=' order by date';
        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
    
    public function insert($historique)
    {
    
    		$sql = 'INSERT INTO historiqueEncodage(typeEncodageId, typeObjetEncodeId,'.
    	    	   'utilisateurId, date, contactId, ficheSuiviId, remiseMaterielId, actionId) VALUES(';
    	       
			$sql.= "'".$this->param_string($historique->GetTypeEncodageId())."', ";
			$sql.= "'".$this->param_string($historique->GetTypeObjetEncodeId())."', ";
			$sql.= "".$historique->GetUtilisateurId().", ";
			$sql.= "'".$historique->GetDate()."', ";
			
			
			if($historique->GetContactId()!=null) 
				$sql.= "".$historique->GetContactId().", ";
			else 
				$sql.= "null, ";
			
			if($historique->GetFicheSuiviId()!=null) 
				$sql.= "".$historique->GetFicheSuiviId().", ";
			else 
				$sql.= "null, ";

			if($historique->GetRemiseMaterielId()!=null) 
				$sql.= "".$historique->GetRemiseMaterielId().", ";
			else 
				$sql.= "null, ";

			if($historique->GetActionId()!=null) 
				$sql.= "".$historique->GetActionId()." ";
			else 	
				$sql.= "null ";
			
    		$sql.=')';
    		
    		if($this->lquery($sql, DB_ECHO))
			{
				return true;
			}
			else	
				return false;
    	
    }
    
    public function getRapport($agent=null, $operateur = null, $dateAnte = null, $datePost = null, $typeObjet = null, $typeEncodage = null)
    {
    	$dateAnte = $this->param_date($dateAnte);
	    $datePost = $this->param_date($datePost);
    	
	    $sql = 'select he.* from historiqueEncodage he';
		if(isset($operateur) && $operateur !='-1')
		{
			$sql.= ' left join operateur_utilisateur ou on ou.operateurId = '.$operateur;	
		}
	    
	    $sql.=' where 1 ';
	    $sql .= 'AND date >=\''.$dateAnte.'\' AND date<=\''.$datePost.'\' ';
	    
	    if(isset($typeObjet) && count($typeObjet)>0)
	    {
	    	$sql .= 'AND typeObjetEncodeId in(';	
	    	foreach($typeObjet as $obj)
	    	{
	    		$type = '';
	    		
	    		switch($obj)
	    		{
	    			case 'contact' : $type = 'CONTACT'; break;
	    			case 'action' : $type = 'ACTION'; break;
	    			case 'suivis' : $type = 'FSUIVI'; break;
	    			case 'materiel' : $type = 'MATPEDA'; break; 
	    		}
	    		
				$sql .= '\''.$type.'\',';
	    	}
	    	$sql = substr($sql, 0, strlen($sql)-1); 
	    	$sql .= ') ';
	    }
	    
    	if(isset($typeEncodage) && count($typeEncodage)>0)
	    {
	    	$sql .= 'AND typeEncodageId in(';	
	    	foreach($typeEncodage as $obj)
	    	{
	    		$type = '';
	    		
	    		switch($obj)
	    		{
	    			case 'ajout' : $type = 'AJOUT'; break;
	    			case 'update' : $type = 'MAJ'; break;
	    			case 'delete' : $type = 'SUP'; break;
	    		}
	    		
				$sql .= '\''.$type.'\',';
	    	}
	    	$sql = substr($sql, 0, strlen($sql)-1); 
	    	$sql .= ') ';
	    }
	    
	    
	    
	    if(isset($agent) && isset($operateur))
	    {
	    	if($agent!='-1')
	    		$sql .= 'AND he.utilisateurId ='.$agent;
	    		
	    	if($operateur!='-1')
	    	{
	    		if($agent!='-1')
	    			$sql .= ' OR he.utilisateurId =ou.utilisateurId';
	    		else
	    			$sql .= ' AND he.utilisateurId =ou.utilisateurId';
	    	}
	    }
	    else
	    {
	    	if(isset($agent))
	    		$sql .= ' AND he.utilisateurId ='.$agent;
	    	else
	    		$sql .= ' AND he.utilisateurId =ou.utilisateurId';
	    }
	    
	    $sql .= ' order by he.utilisateurId';
	   
 		return $this->lquery($sql, DB_ECHO);
    }
    
}
?>