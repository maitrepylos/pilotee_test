<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class IntentionSuiviDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($intentionSuiviId = null)
    {
        $sql = 'select * from dic_intentionSuivi where 1';

        if (isset($intentionSuiviId)) $sql .= ' and intentionSuiviId = \'' . $intentionSuiviId . '\'';

        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>