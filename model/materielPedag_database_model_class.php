<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class MaterielPedagDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($materielId = null)
    {
        $sql = 'select * from dic_materielPedag where 1';

        if (isset($materielId)) $sql .= ' and materielId = \'' . $materielId . '\'';

        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>