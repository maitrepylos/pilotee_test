<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ModeSuiviDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($modeSuiviId = null)
    {
        $sql = 'select * from dic_modeSuivi where 1';

        if (isset($modeSuiviId)) $sql .= ' and modeSuiviId = \'' . $modeSuiviId . '\'';

        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>