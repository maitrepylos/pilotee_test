<?php

REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class NewDatabase extends Database
{

	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function getRemiseMat()
	{
		$sql= 'select * from remiseMaterielPedagEtab';
		
		return $this->lquery($sql, DB_ECHO); 
	}
	
	public function insertMateriel($remMaterielId, $materielId, $quantite)
	{
		$sql = 'insert into remiseParMateriel (remiseMaterielId, materielId, quantite) VALUE ( ';
		$sql.= $remMaterielId.', \''.$materielId.'\', '.$quantite.')';
		$this->lquery($sql, DB_ECHO);
	}
	
	
	public function getContactEtablissement()
	{
		$sql = 'SELECT * from contact_etablissement;';	
		return $this->lquery($sql, DB_ECHO); 
	}
	
	public function getContactAction()
	{
		$sql = 'SELECT * from action_contact;';	
		return $this->lquery($sql, DB_ECHO); 
	}	
	
	
	public function getFicheSuiviContact()
	{
		$sql = 'SELECT * FROM ficheSuivi_contact';
		return $this->lquery($sql, DB_ECHO); 
	}
	
	public function getMaterielContact()
	{
		$sql = "SELECT * FROM remiseMaterielPedagEtab";
		return $this->lquery($sql, DB_ECHO); 
	}
	
	public function getContact($id)
	{
		$sql = 'SELECT * FROM contact where contactId = '.$id;
		return $this->lquery($sql, DB_ECHO);		
	}

	public function deleteContactEtab($in)
	{
		$sql = 'delete from contact_etablissement where contactId in '.$in;
		echo $sql.'<br/>';
		return $this->lquery($sql, DB_ECHO); 
	}
	
	public function deleteContactAction($in)
	{
		$sql = 'delete from action_contact where contactId in '.$in;
		echo $sql.'<br/>';
		return $this->lquery($sql, DB_ECHO); 
		
	}
	
	public function deleteRemiseMateriel($in)
	{
		$sql = 'delete from remiseMaterielPedagEtab where contactId in '.$in;
		echo $sql.'<br/>';
		return $this->lquery($sql, DB_ECHO); 
				
	}
	
	public function deleteFicheSuiviContact($in)
	{
		$sql = 'delete from ficheSuivi_contact where contactId in '.$in;
		echo $sql.'<br/>';
		return $this->lquery($sql, DB_ECHO); 
		
	}	
	
	public function insertAdresse($adresse)
	{
	
		$sql = 'INSERT INTO adresse '.
			   '(dic_provinceId, rue, codepostal, ville, tel_1, tel_2, email_1, email_2, fax,'.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';
				 	 
					if($adresse->GetProvinceId()!=null)	$sql.= "'".$adresse->GetProvinceId()."', ";
					else $sql.= "null, ";
					
					if($adresse->GetRue()!=null) $sql.= "'".$this->param_string($adresse->GetRue())."', ";
					else $sql.= "null, ";
					
					if($adresse->GetCodePostal()!=null) 
						$sql.= "'".$adresse->GetCodePostal()->GetCodePostal()."', ";
					else 
						$sql.= "null, ";

					if($adresse->GetVille()!=null) $sql.= "'".$this->param_string($adresse->GetVille())."', ";
					else $sql.= "null, ";
						
					if($adresse->GetTel1()!=null) $sql.= "'".$this->param_string($adresse->GetTel1())."', ";
					else $sql.= "null, ";
					
					if($adresse->GetTel2()!=null) $sql.= "'".$this->param_string($adresse->GetTel2())."', ";
					else $sql.= "null, ";
					
					if($adresse->GetEmail1()!=null)	$sql.= "'".$this->param_string($adresse->GetEmail1())."', ";
					else $sql.= "null, ";
						
					if($adresse->GetEmail2()!=null)	$sql.= "'".$this->param_string($adresse->GetEmail2())."', ";
					else $sql.= "null, ";
					
					if($adresse->GetFax()!=null) $sql.= "'".$this->param_string($adresse->GetFax())."', ";
					else $sql.= "null, ";
					
					$sql.= "'".$adresse->GetCreated()."', ";
					$sql.= "1, ";
					$sql.= "'".$adresse->GetUpdated()."', ";
					$sql.= "1 ";
				$sql.=')';
				echo "insertAdresse : ".$sql."<br/>";
				
				if($this->lquery($sql, DB_ECHO))
				{	
					return $this->getLastInsertId();
				}
	}
	
	
	public function  insertEntiteAdmin($entiteAdmin)
	{
		$sql = 'INSERT INTO entiteAdmin '.
			   '(entiteAdminId, nom, adresseId, website, swActif,  '.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';
		
			if($entiteAdmin->getEntiteAdminId()!=null) $sql.= "".$entiteAdmin->getEntiteAdminId().", ";
			else $sql.= "null, ";
			
			if($entiteAdmin->getNom()!=null) $sql.= "'".$this->param_string($entiteAdmin->getNom())."', ";
			else $sql.= "null, ";
			
			if($entiteAdmin->getAdresseId()!=null) $sql.= "".$entiteAdmin->getAdresseId().", ";
			else $sql.= "null, ";
			
			if($entiteAdmin->getWebsite()!=null) $sql.= "'".$this->param_string($entiteAdmin->getWebsite())."', ";
			else $sql.= "null, ";
			
			if($entiteAdmin->getSwActif()!=null) $sql.= "".$entiteAdmin->getSwActif().", ";
			else $sql.= "null, ";
			
		
			$sql.= "'".$entiteAdmin->getCreated()."', ";
			$sql.= "1, ";
			$sql.= "'".$entiteAdmin->getUpdated()."', ";
			$sql.= "1 ";
			$sql.=')';		
			
			echo "insertEntiteAdmin : ".$sql."<br/>";
				
			if($this->lquery($sql, DB_ECHO))
			{	
				return $this->getLastInsertId();
			}			
	}
	
	public function insertEtablissement($etab)
	{
			$sql = 'INSERT INTO etablissement '.
			   '(etablissementId, utilisateurId, nom, adresseId, website, niveauEtablissementId, reseauEtablissementId, entiteAdminId, swActif,'.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';
			$sql.= "".$etab->GetEtablissementId().", ";
					
			if($etab->GetUtilisateurId()!=null) $sql.= "".$etab->GetUtilisateurId().", ";
			else $sql.= "null, ";
			
			if($etab->GetNom()!=null) $sql.= "'".$this->param_string($etab->GetNom())."', ";
			else $sql.= "null, ";
		
			if($etab->GetAdresseId()!=null) $sql.= "".$etab->GetAdresseId().", ";
			else $sql.= "null, ";
						
			if($etab->GetWebsite()!=null) $sql.= "'".$this->param_string($etab->GetWebsite())."', ";
			else $sql.= "null, ";
					
			if($etab->GetNiveauEtablissementId()!=null) $sql.= "'".$etab->GetNiveauEtablissementId()."', ";
			else $sql.= "null, ";
					
			if($etab->GetReseauEtablissementId()!=null)	$sql.= "'".$etab->GetReseauEtablissementId()."', ";
			else $sql.= "null, ";
					
			if($etab->GetEntiteAdminId()!=null)	$sql.= "".$etab->GetEntiteAdminId().", ";
			else $sql.= "null, ";
					
			if($etab->GetSwActif()!=null)	$sql.= "'".$etab->GetSwActif()."', ";
			else $sql.= "null, ";
					
			$sql.= "'".$etab->GetCreated()."', ";
			$sql.= "1, ";
			$sql.= "'".$etab->GetUpdated()."', ";
			$sql.= "1 ";
			$sql.=')';
			echo 'insert etablissement : '.$sql;
			if($this->lquery($sql, DB_ECHO))
			{
				return $etab->GetEtablissementId();
			}
		}
		
	public function insertContact($contact)
	{
		$sql = 'INSERT INTO contact '.
			   '(contactId, nom, prenom, adresseId, contactGlobal, swActif,'.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';

					$sql.= "".$contact->GetContactId().", ";
					
					if($contact->GetNom()!=null) $sql.= "'".$this->param_string($contact->GetNom())."', ";
					else $sql.= "null, ";

					if($contact->GetPrenom()!=null) $sql.= "'".$this->param_string($contact->GetPrenom())."', ";
					else $sql.= "null, ";
						
					if($contact->GetAdresseId()!=null) $sql.= "".$contact->GetAdresseId().", ";
					else $sql.= "null, ";
					
					$sql.= "".$contact->GetContactGlobal().", ";
					
					
					if($contact->GetSwActif()!=null)	$sql.= "".$contact->GetSwActif().", ";
					else $sql.= "null, ";
						
					$sql.= "'".$contact->GetCreated()."', ";
					$sql.= $contact->GetCreatedBy().", ";
					$sql.= "'".$contact->GetUpdated()."', ";
					$sql.= $contact->GetUpdatedBy()." ";
				$sql.=')';
				echo $sql."<br/>";
				if($this->lquery($sql, DB_ECHO))
				{
					return $contact->GetContactId();
				}
	}
	
	public function insertContactEtablissement($contactEtab)
	{
			$sql = 'INSERT INTO contact_etablissement '.
			   '(etablissementId, contactId, origineContactId, specialiteContactId, titreContactId, participationId,'.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';

					if($contactEtab->GetEtablissementId()!=null) $sql.= "".$contactEtab->GetEtablissementId().", ";
					else $sql.= "null, ";
					
					$sql.= "".$contactEtab->GetContactId().", ";
					
					
					$sql.= "'".$this->param_string($contactEtab->GetOrigineContactId())."', ";

					if($contactEtab->GetSpecialiteContactId()!=null) $sql.= "'".$this->param_string($contactEtab->GetSpecialiteContactId())."', ";
					else $sql.= "null, ";
						
					if($contactEtab->GetTitreContactId()!=null) $sql.= "'".$this->param_string($contactEtab->GetTitreContactId())."', ";
					else $sql.= "null, ";
					
					if($contactEtab->GetParticipationId()!=null) $sql.= "'".$this->param_string($contactEtab->GetParticipationId())."', ";
					else $sql.= "null, ";
					
					$sql.= "'".$contactEtab->GetCreated()."', ";
					$sql.= $contactEtab->GetCreatedBy().", ";
					$sql.= "'".$contactEtab->GetUpdated()."', ";
					$sql.= $contactEtab->GetUpdatedBy()." ";
				$sql.=')';
				echo $sql."<br/>";
				if($this->lquery($sql, DB_ECHO))
				{
					return $this->getLastInsertId();
				}
	}
	
	public function getCorrespondanceSpecialite($specialite)
	{
		
		$specialite = $this->param_string($specialite);
		$sql='SELECT specialiteContactId from dic_specialiteContact  WHERE label like \'%'.$specialite.'%\'';
		$result = $this->lquery($sql, DB_ECHO);
		
		if($result && mysqli_num_rows($result) == 1)
		{
			$row = mysqli_fetch_assoc($result);
			return $row["specialiteContactId"];
		}
		else
		{
			$sql='SELECT specialiteContactId from dic_specialiteContact WHERE specialiteContactId like \'%'.$specialite.'%\'';
			
			$r = $this->lquery($sql, DB_ECHO);
			if($r && mysqli_num_rows($r) == 1)
			{
				$ro = mysqli_fetch_assoc($r);
				return $ro["specialiteContactId"];
			}
			else
				return null;
		}
	}
	
	public function getCorrespondanceTitre($titre)
	{
		$titre = $this->param_string($titre);
		$sql='SELECT titreContactId from dic_titreContact WHERE label like \'%'.$titre.'%\'';
		
		$result = $this->lquery($sql, DB_ECHO);
		
		if($result && mysqli_num_rows($result) == 1)
		{
			$row = mysqli_fetch_assoc($result);
			return $row["titreContactId"];
		}
		else
		{
		
			$sql='SELECT titreContactId from dic_titreContact WHERE titreContactId like \'%'.$titre.'%\'';
			$r = $this->lquery($sql, DB_ECHO);
			
			if($r && mysqli_num_rows($r) == 1)
			{
				$ro = mysqli_fetch_assoc($r);
				return $ro["titreContactId"];
			}
			else
				return null;
		}		
	}
	
	public function insertMaterielPedagogique($materiel)
	{
			$sql = 'INSERT INTO remiseMaterielPedagEtab '.
			   '(remiseMaterielId, materielId, etablissementId, contactId, date, quantite,'.
			   ' created, createdBy, updated, updatedBy) VALUES ( ';

					
					$sql.= "".$materiel->GetRemiseMaterielId().", ";
					
					$sql.= "'".$this->param_string($materiel->GetMaterielId())."', ";
					
					$sql.= "".$materiel->GetEtablissementId().", ";
					
					$sql.= "".$materiel->GetContactId().", ";
					
					if($materiel->GetDate()!=null) $sql.= "'".$materiel->GetDate()."', ";
					else $sql.= "null, ";
					
					$sql.= "".$materiel->GetQuantite().", ";
					
					$sql.= "'".$materiel->GetCreated()."', ";
					
					$sql.= $materiel->GetCreatedBy().", ";
					
					$sql.= "'".$materiel->GetUpdated()."', ";
					
					$sql.= $materiel->GetUpdatedBy()." ";
					
				$sql.=')';
				echo $sql;
				if($this->lquery($sql, DB_ECHO))
				{
	
					return $this->getLastInsertId();
				}
	}
	
	function insertFicheSuivi($suivi)
	{
			$sql = 'INSERT INTO ficheSuivi '.
				  '(ficheSuiviId, modeSuiviId, etablissementId, intentionSuiviId, timingSuiviId, commentaire, dateRencontre, '.
			   	  ' created, createdBy, updated, updatedBy) VALUES ( ';
			
			$sql.= "".$suivi->GetFicheSuiviId().", ";
					
			if($suivi->GetModeSuiviId()!=null) $sql.= "'".$this->param_string($suivi->GetModeSuiviId())."', ";
			else $sql.= "null, ";
			
			$sql.= "".$suivi->GetEtablissementId().", ";
			
			if($suivi->GetIntentionSuiviId()!=null) $sql.= "'".$this->param_string($suivi->GetIntentionSuiviId())."', ";
			else $sql.= "null, ";
					
			if($suivi->GetTimingSuiviId()!=null) $sql.= "'".$this->param_string($suivi->GetTimingSuiviId())."', ";
			else $sql.= "null, ";
						
			if($suivi->GetCommentaire()!=null) $sql.= "'".$this->param_string($suivi->GetCommentaire())."', ";
			else $sql.= "null, ";
					
			if($suivi->GetDateRencontre()!=null) $sql.= "'".$suivi->GetDateRencontre()."', ";
			else $sql.= "null, ";
					
			$sql.= "'".$suivi->GetCreated()."', ";
			$sql.= $suivi->GetCreatedBy().", ";
			$sql.= "'".$suivi->GetUpdated()."', ";
			$sql.= $suivi->GetUpdatedBy()." ";
			$sql.=')';
			echo $sql;
			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}
	}

	function insertFicheSuiviContact($ficheContact)
	{
			$sql = 'INSERT INTO ficheSuivi_contact '.
				  '(contactId, ficheSuiviId,'.
			   	  ' created, createdBy, updated, updatedBy) VALUES ( ';
			
			$sql.= "".$ficheContact->GetContactId().", ";
			$sql.= "".$ficheContact->GetFicheSuiviId().", ";
					
					
			$sql.= "'".$ficheContact->GetCreated()."', ";
			$sql.= $ficheContact->GetCreatedBy().", ";
			$sql.= "'".$ficheContact->GetUpdated()."', ";
			$sql.= $ficheContact->GetUpdatedBy()." ";
			$sql.=')';
			echo $sql;
			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}	
	}
	
	function insertDicActionLabel($actionLabel)
	{
			$sql = 'INSERT INTO dic_actionLabel '.
				  '(actionLabelId, operateurId, label,'.
			   	  ' anneeId, ordre, swActif) VALUES ( ';
			
			$sql.= "'".$actionLabel->GetActionLabelId()."', ";
			$sql.= "".$actionLabel->GetOperateurId().", ";
			$sql.= "'".$actionLabel->GetLabel()."', ";
			$sql.= "null, ";
			$sql.= "".$actionLabel->GetOrdre().", ";
			$sql.= "".$actionLabel->GetSwActif()." ";
			
			$sql.=')';
			echo $sql;
			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}	
	}
	
	public function getIdActionLab($label)
	{
		 $sql = 'select actionLabelId from dic_actionLabel where label=\''.$label.'\'';
		 
 		 $results = $this->lquery($sql, DB_ECHO);
		 $row = mysqli_fetch_assoc($results);
		 return $row['actionLabelId']; 
	}
	
	public function insertActionLabelise($action)
	{
		$sql = 'INSERT INTO dic_actionLabel '.
				  '(actionLabelId, operateurId, label,'.
			   	  ' dateDebut, dateFin, ordre, swActif) VALUES ( ';
			
			$sql.= "'".$actionLabel->GetActionLabelId()."', ";
			$sql.= "".$actionLabel->GetOperateurId().", ";
			$sql.= "'".$actionLabel->GetLabel()."', ";

			if($actionLabel->GetDateDebut()!=null) $sql.= "'".$actionLabel->GetDateDebut()."', ";
			else $sql.= "null, ";
			
			if($actionLabel->GetDateFin()!=null) $sql.= "'".$actionLabel->GetDateFin()."', ";
			else $sql.= "null, ";
			
			$sql.= "".$actionLabel->GetOrdre().", ";
			$sql.= "".$actionLabel->GetSwActif()." ";
			
			$sql.=')';
			
			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}	
	}
	
	public function userExist($email)
	{
		$sql ='select * from utilisateur where login=\''.$email.'\'';
		$result = $this->lquery($sql, DB_ECHO);
		if($result && mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_assoc($result);
		 	return $row['utilisateurId']; 
		}
		else 
			return null;
		
		
	}
	
	public function insertAgent($login, $nom, $prenom, $tel  )
	{
		$sql = 'INSERT INTO utilisateur '.
				  '(typeUtilisateurId, swActif, login, motDePasse, nom, prenom, tel, '.
			   	  ' created, createdBy, updated, updatedBy) VALUES ( ';
			
			$sql.= "'AGE', ";
			$sql.= "1, ";
			$sql.= "'".$login."', ";
			$sql.= "'biznet', ";

			if($nom!=null) $sql.= "'".$nom."', ";
			else $sql.= "null, ";
			
			if($prenom!=null) $sql.= "'".$prenom."', ";
			else $sql.= "null, ";

			if($tel!=null) $sql.= "'".$tel."', ";
			else $sql.= "null, ";
			
			$sql.= "'".date('Y-m-d H:i:s')."', ";
			$sql.= "1, ";
			$sql.= "'".date('Y-m-d H:i:s')."', ";
			$sql.= "1 ";
			$sql.=')';			
			echo $sql."<br/>";	
			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}	
	}
	
	public function insertAction($action)
	{
		$sql = 'INSERT INTO action (actionId, statutSubventionId, actionLabelId, refChezOperateur, '. 
				'refChezASE, typeActionId, anneeId, statutActionId, categActionNonLabelId, operateurId, '.
				'dateAction, nomGen, nomSpec, commentaire, swGlobal, swPedagogie, nbApprenants, nbEnseignants, '.
				'website, montantDemande, montantSubvention, categorieBourse, projetRealise, groupeClasseConcernee, '.
				'collaborationInterEtab, created, createdBy, updated, updatedBy) VALUES (';
		
		
			$sql.= "".$action->GetActionId().", ";
			
			if($action->GetStatutSubventionId()!=null) $sql.= "'".$this->param_string($action->GetStatutSubventionId())."', ";
			else $sql.= "null, ";
						
			if($action->GetActionLabelId()!=null) $sql.= "'".$this->param_string($action->GetActionLabelId())."', ";
			else $sql.= "null, ";

			if($action->GetRefChezOperateur()!=null) $sql.= "'".$this->param_string($action->GetRefChezOperateur())."', ";
			else $sql.= "null, ";

			if($action->GetRefChezASE()!=null) $sql.= "'".$this->param_string($action->GetRefChezASE())."', ";
			else $sql.= "null, ";

			$sql.= "'".$this->param_string($action->GetTypeActionId())."', ";
			$sql.= "'".$this->param_string($action->GetAnneeId())."', ";
			$sql.= "'".$this->param_string($action->GetStatutActionId())."', ";			

			if($action->GetCategActionNonLabelId()!=null) $sql.= "'".$this->param_string($action->GetCategActionNonLabelId())."', ";
			else $sql.= "null, ";
			
			if($action->GetOperateurId()!=null) $sql.= "'".$this->param_string($action->GetOperateurId())."', ";
			else $sql.= "null, ";
			
			if($action->GetDateAction()!=null) $sql.= "'".$action->GetDateAction()."', ";
			else $sql.= "null, ";

			if($action->GetNomGen()!=null) $sql.= "'".$this->param_string($action->GetNomGen())."', ";
			else $sql.= "null, ";

			if($action->GetNomSpec()!=null) $sql.= "'".$this->param_string($action->GetNomSpec())."', ";
			else $sql.= "null, ";        	
			
			if($action->GetCommentaire()!=null) $sql.= "'".$this->param_string($action->GetCommentaire())."', ";
			else $sql.= "null, ";

			$sql.= "".$action->GetSwGlobal().", ";
			$sql.= "".$action->GetSwPedagogie().", ";
			$sql.= "".$action->GetNbApprenants().", ";
			$sql.= "".$action->GetNbEnseignants().", ";
			
			if($action->GetWebsite()!=null) $sql.= "'".$this->param_string($action->GetWebsite())."', ";
			else $sql.= "null, ";
			
			if($action->GetMontantDemande()!=null) $sql.= "'".$action->GetMontantDemande()."', ";
			else $sql.= "null, ";
			
			if($action->GetMontantSubvention()!=null) $sql.= "'".$action->GetMontantSubvention()."', ";
			else $sql.= "null, ";
			
			if($action->GetCategorieBourse()!=null) $sql.= "'".$this->param_string($action->GetCategorieBourse())."', ";
			else $sql.= "null, ";
			
			if($action->GetProjetRealise()!=null) $sql.= "'".$this->param_string($action->GetProjetRealise())."', ";
			else $sql.= "null, ";
			
			if($action->GetGroupeClasseConcernee()!=null) $sql.= "".$action->GetGroupeClasseConcernee().", ";
			else $sql.= "null, ";
			
			if($action->GetCollaborationInterEtab()!=null) $sql.= "'".$this->param_string($action->GetCollaborationInterEtab())."', ";
			else $sql.= "null, ";
			
	        $sql.= "'".$action->GetCreated()."', ";
			$sql.= $action->GetCreatedBy().", ";
			$sql.= "'".$action->GetUpdated()."', ";
			$sql.= $action->GetUpdatedBy()." ";
			
		$sql.=')';
		echo "InsertAction ==> ".$sql . "<br/><br/>";
			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}	
	}
	
	public function insertAction2($action)
	{
		$sql = 'INSERT INTO action (statutSubventionId, actionLabelId, refChezOperateur, '. 
				'refChezASE, typeActionId, anneeId, statutActionId, categActionNonLabelId, operateurId, '.
				'dateAction, nomGen, nomSpec, commentaire, swGlobal, swPedagogie, nbApprenants, nbEnseignants, '.
				'website, montantDemande, montantSubvention, categorieBourse, projetRealise, groupeClasseConcernee, '.
				'collaborationInterEtab, created, createdBy, updated, updatedBy) VALUES (';
		
		
			if($action->GetStatutSubventionId()!=null) $sql.= "'".$this->param_string($action->GetStatutSubventionId())."', ";
			else $sql.= "null, ";
						
			if($action->GetActionLabelId()!=null) $sql.= "'".$this->param_string($action->GetActionLabelId())."', ";
			else $sql.= "null, ";

			if($action->GetRefChezOperateur()!=null) $sql.= "'".$this->param_string($action->GetRefChezOperateur())."', ";
			else $sql.= "null, ";

			if($action->GetRefChezASE()!=null) $sql.= "'".$this->param_string($action->GetRefChezASE())."', ";
			else $sql.= "null, ";

			$sql.= "'".$this->param_string($action->GetTypeActionId())."', ";
			$sql.= "'".$this->param_string($action->GetAnneeId())."', ";
			$sql.= "'".$this->param_string($action->GetStatutActionId())."', ";			

			if($action->GetCategActionNonLabelId()!=null) $sql.= "'".$this->param_string($action->GetCategActionNonLabelId())."', ";
			else $sql.= "null, ";
			
			if($action->GetOperateurId()!=null) $sql.= "'".$this->param_string($action->GetOperateurId())."', ";
			else $sql.= "null, ";
			
			if($action->GetDateAction()!=null) $sql.= "'".$action->GetDateAction()."', ";
			else $sql.= "null, ";

			if($action->GetNomGen()!=null) $sql.= "'".$this->param_string($action->GetNomGen())."', ";
			else $sql.= "null, ";

			if($action->GetNomSpec()!=null) $sql.= "'".$this->param_string($action->GetNomSpec())."', ";
			else $sql.= "null, ";        	
			
			if($action->GetCommentaire()!=null) $sql.= "'".$this->param_string($action->GetCommentaire())."', ";
			else $sql.= "null, ";

			$sql.= "".$action->GetSwGlobal().", ";
			$sql.= "".$action->GetSwPedagogie().", ";
			$sql.= "".$action->GetNbApprenants().", ";
			$sql.= "".$action->GetNbEnseignants().", ";
			
			if($action->GetWebsite()!=null) $sql.= "'".$this->param_string($action->GetWebsite())."', ";
			else $sql.= "null, ";
			
			if($action->GetMontantDemande()!=null) $sql.= "'".$action->GetMontantDemande()."', ";
			else $sql.= "null, ";
			
			if($action->GetMontantSubvention()!=null) $sql.= "'".$action->GetMontantSubvention()."', ";
			else $sql.= "null, ";
			
			if($action->GetCategorieBourse()!=null) $sql.= "'".$this->param_string($action->GetCategorieBourse())."', ";
			else $sql.= "null, ";
			
			if($action->GetProjetRealise()!=null) $sql.= "'".$this->param_string($action->GetProjetRealise())."', ";
			else $sql.= "null, ";
			
			if($action->GetGroupeClasseConcernee()!=null) $sql.= "".$action->GetGroupeClasseConcernee().", ";
			else $sql.= "null, ";
			
			if($action->GetCollaborationInterEtab()!=null) $sql.= "'".$this->param_string($action->GetCollaborationInterEtab())."', ";
			else $sql.= "null, ";
			
	        $sql.= "'".$action->GetCreated()."', ";
			$sql.= $action->GetCreatedBy().", ";
			$sql.= "'".$action->GetUpdated()."', ";
			$sql.= $action->GetUpdatedBy()." ";
			
		$sql.=')';
		echo "InsertAction ==> ".$sql . "<br/><br/>";
			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}
	}
	
	
	public function insertActionContact($actionContact)
	{
		$sql='INSERT INTO action_contact(actionId, contactId, roleContactActionId, created, createdBy, updated, updatedBy) VALUES(';
		
		$sql.= "".$actionContact->GetActionId().", ";
		$sql.= "".$actionContact->GetContactId().", ";
		$sql.= "'".$actionContact->GetRoleContactActionId()."', ";
	    $sql.= "'".$actionContact->GetCreated()."', ";
		$sql.= $actionContact->GetCreatedBy().", ";
		$sql.= "'".$actionContact->GetUpdated()."', ";
		$sql.= $actionContact->GetUpdatedBy()." ";
		$sql.=')';
		echo $sql."<br/>";
		
		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
		
	}
	
	public function updateAgentEtablissement($idEtab, $idAgent)
	{
		$sql ='update etablissement set utilisateurId ='.$idAgent.' where etablissementId='.$idEtab;
		
		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
	}
	
	public function insertActionSection($actionSection)
	{
		$sql = 'INSERT INTO actionSectionAction(actionId, sectionActionId, nbApprenants) VALUES(';
		
		$sql.= "".$actionSection->GetActionId().", ";
		$sql.= "'".$actionSection->GetSectionActionId()."', ";
		$sql.= "".$actionSection->GetNbApprenants()." ";
		$sql.=')';
		echo $sql."<br/>";
		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
	}
	
	public function insertActionEtablissement($actionEtab)
	{
		$sql = 'INSERT INTO action_etablissement(actionId, etablissementId, created, createdBy, updated, updatedBy) VALUES(';
		
		$sql.= "".$actionEtab->GetActionId().", ";
		$sql.= "".$actionEtab->GetEtablissementId().", ";
		$sql.= "'".$actionEtab->GetCreated()."', ";
		$sql.= $actionEtab->GetCreatedBy().", ";
		$sql.= "'".$actionEtab->GetUpdated()."', ";
		$sql.= $actionEtab->GetUpdatedBy()." ";
		$sql.=')';
		echo $sql."<br/>";
		if($this->lquery($sql, DB_ECHO))
		{
			return true;
		}	
	}
	
	
}

?>