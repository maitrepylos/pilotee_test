<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class NiveauDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($niveauId)
	{
		$sql = 'select * from dic_niveauEtablissement where swActif = 1';
		
		if (isset($niveauId) && (strlen($niveauId) > 0))
		{
			$niveauId = $this->param_string($niveauId);			
			
			$sql .= ' and niveauEtablissementId = \'' . $niveauId . '\'';
		}
		
		$sql .= ' order by ordre;';
		
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}
}
?>