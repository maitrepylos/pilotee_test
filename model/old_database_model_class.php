<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class OldDatabase extends Database
{
	public function __construct()
	{
		//$this->setConnectionData('aurelys.intranet.cediti.be', 'webpublish', 'bdhh2lh', 'ASE_EspritEntreprendre_old');
		$this->setConnectionData('localhost', 'root', '', 'ASE_EspritEntreprendre_old');
	}
	
	public function GetEtablissements()
	{
		$sql = 'select * from ee_etablissement where 1;';
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetEntiteAdmin()
	{
		$sql = 'select * from ee_entite_admin where 1;';
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetContacts()
	{
		$sql = 'select * from ee_personne where 1;';
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetLienContactEtablissement()
	{
		$sql = 'select * from ee_fait_parti_de1;';
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function getMaterielPedagogique()
	{
		$sql = 'select * from ee_materiel_pedag;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function getFicheSuivi()
	{
		$sql = 'select * from ee_evaluation_etab;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function getLienFicheContact()
	{
		$sql = 'select * from ee_est_documentee_par;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetDistinctActionLabel()
	{
		$sql = 'select distinct nom_action_label from ee_action_label where nom_action_label !=\'\';';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetActionLabel()
	{
		$sql = 'select * from ee_action_label;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetAgentEtab()
	{
		$sql = 'select * from ee_est_titulaire_de;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetUtilisateurs()
	{
		$sql = 'select * from ee_sessionweb;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GETAnime2()
	{
		$sql = 'select * from ee_anime2;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetParticipe2()
	{
		$sql = 'select * from ee_participe2;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetProjet()
	{
		$sql = 'select * from ee_projet;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetProjetEtab()
	{
		$sql = 'select * from ee_a_depose;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	public function GetProjetContact()
	{
		$sql = 'select * from ee_anime3;';		
		return $this->lquery($sql, DB_ECHO);
	}
	
	
}
?>