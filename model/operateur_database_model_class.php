<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class OperateurDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($operateurId = null, $actif = false)
	{
		$sql = 'select * from operateur where 1';
		if (isset($operateurId) && (strlen($operateurId) > 0))
		{
			$operateurId = $this->param_string($operateurId);
			$sql .= ' and operateurId = ' . $operateurId;
		}

		if($actif)
			$sql .= ' and swActif = 1';

		

		$sql .= ' order by swActif DESC, nom ASC';

		$rs = $this->lquery($sql, DB_ECHO);
		return $rs;
	}

	public function getOperateurIdWithUtilisateurId($utilisateurId)
	{
		$sql = 'select * from operateur_utilisateur';
		$sql .= ' where utilisateurId = ' . $utilisateurId;
		
		$rs = $this->lquery($sql, DB_ECHO);

		return $rs;
	}

	public function getAction($operateurId)
	{
		$sql = 'select * from action';
		$sql .= ' where operateurId = ' . $operateurId;
		$sql .= ' and typeActionId = \'LABEL\'' ;
		$rs = $this->lquery($sql, DB_ECHO);
		return $rs;
	}

	public function getOperateurInOneAnneeScolaire($anneeScolaireId)
	{
		
		// $sql  = 'SELECT DISTINCT o.* FROM action_operateur ao LEFT JOIN `action` a ON (ao.actionId = a.actionId) LEFT JOIN operateur o ON ';
		// $sql .= ' (o.operateurId = ao.operateurId OR a.operateurId = o.operateurId) WHERE a.anneeId = \'';
		
		
		// FFI BUG CORRECTION AVRIL 2012
		$sql =	'select distinct o.* from operateur o left join action a on (o.operateurId =  a.operateurId) LEFT JOIN action_operateur ao ON '; 
		$sql .= ' (o.operateurId = ao.operateurId) WHERE a.anneeId = \''; 
		
		$sql .= $this->param_string2($anneeScolaireId) . '\'';
		$sql .= ' ORDER BY o.nom ASC';
		$rs = $this->lquery($sql, DB_ECHO);
		return $rs;
	}
}
