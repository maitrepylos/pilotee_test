<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class OrigineContactDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($origineContactId = null)
    {
        $sql = 'select * from dic_origineContact where 1';

        if (isset($origineContactId)) $sql .= ' and origineContactId = \'' . $origineContactId . '\'';

        $sql .= ' and SwActif = 1 order by ordre;';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>