<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ParticipationDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($participationId = null)
    {
        $sql = 'select * from dic_participation where 1';

        if (isset($participationId)) $sql .= ' and participationId = \'' . $participationId . '\'';

        $sql .= ' and SwActif = 1 order by ordre;';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>