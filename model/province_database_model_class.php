<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ProvinceDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($provinceId)
	{
		$sql = 'select * from dic_province where swActif = 1';
		
		if (isset($provinceId) && (strlen($provinceId) > 0))
		{
			$provinceId = $this->param_string($provinceId);			
			
			$sql .= ' and dic_provinceId = \'' . $provinceId . '\'';
		}
		
		$sql .= ' order by ordre;';
		
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}
}
?>