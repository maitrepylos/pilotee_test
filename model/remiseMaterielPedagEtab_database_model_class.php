<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class RemiseMaterielPedagEtabDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($remiseMaterielId = null, $etablissementId = null, $orderDateDESC = false)
    {
        $sql = 'select * from remiseMaterielPedagEtab where 1';

        if (isset($remiseMaterielId)) $sql .= ' and remiseMaterielId = ' . $remiseMaterielId;
		if (isset($etablissementId)) $sql .= ' and etablissementId = ' . $etablissementId;
        $sql.=' order by date' . ($orderDateDESC ? ' ASC ':' DESC ');
        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
    
    public function getMateriaux($id)
    {
    	$sql = 'select * from remiseParMateriel where remiseMaterielId ='.$id;
    	return $this->lquery($sql, DB_ECHO);
    }
    
    public function insert($materielPeda)
    {
		$sql = 'INSERT INTO remiseMaterielPedagEtab (etablissementId, contactId, date,'.
										' created, createdBy, updated, updatedBy) VALUES (';
		
			//$sql.= "'".$materielPeda->GetMaterielId()."', ";
			$sql.= "".$materielPeda->GetEtablissementId().", ";
			$sql.= "".$materielPeda->GetContactId().", ";
			if($materielPeda->GetDate()!=null) $sql.= "'".$this->param_date($materielPeda->GetDate())."', ";
			else $sql.= "null, ";
			
			//$sql.= "".$materielPeda->GetQuantite().", ";

	        $sql.= "'".$materielPeda->GetCreated()."', ";
			$sql.= $materielPeda->GetCreatedBy().", ";
			$sql.= "'".$materielPeda->GetUpdated()."', ";
			$sql.= $materielPeda->GetUpdatedBy()." ";
			$sql.=')';

			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}	
    }
    
    public function insertMateriel($remiseParMat)
    {
    		$sql = 'INSERT INTO remiseParMateriel (remiseMaterielId, materielId, quantite) VALUES (';
										
		
			$sql.= "".$remiseParMat->getRemiseMaterielId().", ";
			$sql.= "'".$remiseParMat->getMaterielId()."', ";
			$sql.= "".$remiseParMat->GetQuantite()." ";
			$sql.=')';

			if($this->lquery($sql, DB_ECHO))
			{
				return $this->getLastInsertId();
			}    	
    }
    
    public function deleteMateriel($id)
    {
    	$sql='delete from remiseParMateriel where remiseMaterielId = '.$id;
    	$this->lquery($sql, DB_ECHO);
    }
    
    public function update($materielPeda)
    {
		$sql = "UPDATE remiseMaterielPedagEtab ";
		//$sql.="set materielId ='".$materielPeda->GetMaterielId()."', ";
		$sql.="set etablissementId =".$materielPeda->GetEtablissementId().", ";
		$sql.=" contactId =".$materielPeda->GetContactId().", ";
		$sql.=" date =";
		if($materielPeda->GetDate()!=null)
			$sql.= "'".$this->param_date($materielPeda->GetDate())."', ";
		else
			$sql.= "null, ";
		
		//$sql.=" quantite =".$materielPeda->GetQuantite().", ";
		$sql .= " updated = now()";
		$sql .= sprintf(", updatedBy = %s", $materielPeda->GetUpdatedBy());
		
		$sql .= " where remiseMaterielId = " . $materielPeda->GetRemiseMaterielId();

		echo $sql;
		return $this->lquery($sql, DB_ECHO);
		
    	
    }
    
    public function delete($remiseMaterielId)
    {
    	return $this->lquery(sprintf('delete from remiseMaterielPedagEtab where remiseMaterielId = %s;', $remiseMaterielId), DB_ECHO);
    }
    
    
    
}
?>