<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class ReseauDatabase extends Database
{
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}

	public function get($reseauId)
	{
		$sql = 'select * from dic_reseauEtablissement where swActif = 1';
		
		if (isset($reseauId) && (strlen($reseauId) > 0))
		{
			$reseauId = $this->param_string($reseauId);			
			
			$sql .= ' and reseauEtablissementId = \'' . $reseauId . '\'';
		}
		
		$sql .= ' order by ordre;';
		
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}
}
?>