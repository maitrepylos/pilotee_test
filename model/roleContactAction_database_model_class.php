<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class RoleContactActionDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($roleContactActionId = null)
    {
        $sql = 'select * from dic_roleContactAction where 1';
        if (isset($roleContactActionId)) $sql .= ' and roleContactActionId = \'' . $roleContactActionId . '\'';
        $sql .= ';';
        return $this->lquery($sql, DB_ECHO);
    }
    
	public function getWithoutParticipant($roleContactActionId = null)
    {
        $sql = 'select * from dic_roleContactAction where 1';
        $sql .= ' and label != \'Participant\'';
        if (isset($roleContactActionId)) $sql .= ' and roleContactActionId = \'' . $roleContactActionId . '\'';        
        $sql .= ';';
        return $this->lquery($sql, DB_ECHO);
    }
}
?>