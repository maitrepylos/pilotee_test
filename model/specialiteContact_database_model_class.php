<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class SpecialiteContactDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($specialiteContactId = null)
    {
        $sql = 'select * from dic_specialiteContact where 1';

        if (isset($specialiteContactId)) $sql .= ' and specialiteContactId = \'' . $specialiteContactId . '\'';

        $sql .= ' and SwActif = 1 order by ordre;';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>