<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class StatutDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($statutId = null)
    {
        $sql = 'select * from dic_statut where 1';

        if (isset($statutId)) $sql .= ' and statutId = \'' . $statutId . '\'';

        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>