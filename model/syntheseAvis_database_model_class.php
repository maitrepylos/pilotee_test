<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class SyntheseAvisDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($syntheseAvisId = null)
    {
        $sql = 'select * from dic_SyntheseAvis where 1';

        if (isset($syntheseAvisId)) $sql .= ' and syntheseAvisId = ' . $syntheseAvisId;

        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>