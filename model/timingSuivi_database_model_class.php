<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class TimingSuiviDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($timingSuiviId = null)
    {
        $sql = 'select * from dic_timingSuivi where 1';

        if (isset($timingSuiviId)) $sql .= ' and timingSuiviId = \'' . $timingSuiviId . '\'';

        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>