<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class TitreContactDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($titreContactId = null)
    {
        $sql = 'select * from dic_titreContact where 1';

        if (isset($titreContactId)) $sql .= ' and titreContactId = \'' . $titreContactId . '\'';

        $sql .= ' and SwActif = 1 order by ordre;';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>