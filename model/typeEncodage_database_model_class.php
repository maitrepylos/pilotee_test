<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class TypeEncodageDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($typeEncodageId = null)
    {
        $sql = 'select * from dic_typeEncodage where 1';

        if (isset($typeEncodageId)) $sql .= ' and typeEncodageId = \'' . $typeEncodageId . '\'';

        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
}
?>