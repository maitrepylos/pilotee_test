<?php
/* Created by Marucci 24/12/2013 */
REQUIRE_ONCE(SCRIPTPATH.'lib/abstractDatabase.php');

class TypeUtilisateurDatabase extends AbstractDatabase
{
    protected function _getTableName()
	{
		return 'dic_typeUtilisateur';
	}
	
	protected function _getIdName()
	{
		return 'typeUtilisateurId';
	}
}
?>