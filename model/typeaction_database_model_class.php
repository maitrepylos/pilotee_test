<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class TypeActionDatabase extends Database
{
    public function __construct()
    {
        $this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
    }

    public function get($typeActionId = null)
    {
        $sql = 'select * from dic_typeAction where 1';
        	if (isset($typeActionId)) $sql .= ' and typeActionId = \'' . $typeActionId . '\'';
        $sql .= ';';

        return $this->lquery($sql, DB_ECHO);
    }
    public function getListAddAction($operateur = false, $agent = false, $pi = false)
    {
        $sql = 'select * from dic_typeAction where 1';
        	if ($operateur) $sql .= ' and typeActionId = \'' . ACTION_LABELLISEES . '\'';
        	if ($agent) $sql .= ' and typeActionId = \'' . ACTION_MICRO . '\' or typeActionId = \''.ACTION_NON_LABELLISEES.'\'';
        	if ($pi) $sql .= ' and typeActionId = \'' . BOURSES_PI . '\'';
        	else $sql .= ' and typeActionId != \'' . BOURSES_PI . '\'';
        	
        	$sql.= ' order by ordre';
        	
        $sql .= ';';
        
        echo $sql;

        return $this->lquery($sql, DB_ECHO);
    }
}
?>