<?php
/* Create By Marucci 24/12/2013 Model for search users ! */ 
REQUIRE_ONCE (SCRIPTPATH . 'lib/search_service.class.php');

class UserSearch extends SearchService {

	private $nom;
	private $prenom;
	private $email;
	private $login;
	private $typeUtilisateur;
	private $operateur;
	private $slc;

	public function __construct() {
		parent::__construct();
	}

	protected function constructWhereClause() {
		$where = '';

		if ($this -> nom) {
			$where .= " AND upper(utilisateur.nom) like upper('%" . $this -> nom . "%')";
		}
		
		if ($this -> prenom) {
			$where .= " AND upper(utilisateur.prenom) LIKE upper('%" . $this -> prenom . "%')";
		}

		if ($this -> email) {
			$where .= " AND lower(utilisateur.email) LIKE lower('%" . $this -> email . "%')";
		}
		
		if ($this -> login) {
			$where .= " AND upper(utilisateur.login) LIKE upper('%" . $this -> login . "%')";
		}
		
		if ($this -> typeUtilisateur) {
			$where .= " AND upper(utilisateur.fk_typeUtilisateur)  LIKE upper('%" . $this -> typeUtilisateur. "%')";
		}
		
		if ($this -> operateur) {
			$where .= " AND utilisateur.fk_operateur = " . $this -> operateur;
		}
		
		if ($this -> slc) {
			$where .= " AND upper(utilisateur.fk_slc)  LIKE upper('%" . $this -> slc. "%')";
		}

		$this -> orderby = ' ORDER BY utilisateur.login';
		$this -> where = $where;
	}

	protected function getTable() {
		return 'utilisateur';
	}

	protected function getModelClass() {
		return 'UtilisateurDatabase';
	}

	public function setNom($nom) {
		$this -> nom = $this -> param_string2(strtoupper($this -> param_string($nom)));
	}
	
	public function setPrenom($prenom) {
		$this -> prenom = $this -> param_string2(strtoupper($this -> param_string($prenom)));
	}

	public function setEmail($email) {
		$this -> email = $this -> param_string2(strtolower($this -> param_string($email)));
	}
	
	public function setLogin($login) {
		$this -> login = $this -> param_string2(strtoupper($this -> param_string($login)));
	}
	
	public function setTypeUtilisateur($typeUtilisateur) {
		$this -> typeUtilisateur = $this -> param_string2(strtoupper($this -> param_string($typeUtilisateur)));
	}
	
	public function setSlc($slc) {
		$this -> slc = $this -> param_string2(strtoupper($this -> param_string($slc)));
	}
	
	public function setOperateur($operateur) {
		$this -> operateur = $this -> param_int($operateur);
	}
	

	

}
