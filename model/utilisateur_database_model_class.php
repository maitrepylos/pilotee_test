<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/database.model.class.php');

class UtilisateurDatabase extends Database
{
	public $count;
	public function __construct()
	{
		$this->setConnectionData(DB_HOST, DB_LOGIN, DB_PWD, DB_NAME);
	}
	public function get($userId, $userType = null, $nom = null, $prenom = null, $email = null, $login = null, $typeUtilisateur = null, $begin = null, $offset = null)
	{
		$sql_count = 'select count(*) from utilisateur u where true';
		$sql = 'select u.* , ut.label as typeUtilisateurLabel from utilisateur u left join dic_typeUtilisateur ut on u.typeUtilisateurId = ut.typeUtilisateurId where true';
		if (isset($userId) && ($userId > -1)) {
			$sql .= ' and u.utilisateurId = ' . $userId;
			$sql_count .= ' and u.utilisateurId = ' . $userId;
		}
		if (isset($userType) && (strlen($userType) > 0)){
			if($userType == USER_TYPE_AGENT || $userType == USER_TYPE_COORDINATEUR) $sql .= ' and u.typeUtilisateurId = \'' . $userType . '\' or u.typeUtilisateurId = \'' . 'AGC' . '\' ';
			else $sql .= ' and u.typeUtilisateurId = \'' . $userType . '\'';
			if($userType == USER_TYPE_AGENT || $userType == USER_TYPE_COORDINATEUR) $sql_count .= ' and u.typeUtilisateurId = \'' . $userType . '\' or u.typeUtilisateurId = \'' . 'AGC' . '\' ';
			else $sql_count .= ' and u.typeUtilisateurId = \'' . $userType . '\'';
		} 
		if (isset($nom) && (strlen($nom) > 0)){
			$sql .= ' and upper(u.nom) like \'%' . strtoupper($nom) . '%\'';
			$sql_count .= ' and upper(u.nom) like \'%' . strtoupper($nom) . '%\'';
		} 
		if (isset($prenom) && (strlen($prenom) > 0)){
			$sql .= ' and upper(u.prenom) like \'%' . strtoupper($prenom) . '%\'';
			$sql_count .= ' and upper(u.prenom) like \'%' . strtoupper($prenom) . '%\'';
		} 
		if (isset($email) && (strlen($email) > 0)){
			$sql .= ' and upper(u.email) like \'%' . strtoupper($email) . '%\'';
			$sql_count .= ' and upper(u.email) like \'%' . strtoupper($email) . '%\'';
		} 
		if (isset($login) && (strlen($login) > 0)){
			$sql .= ' and upper(u.login) like \'%' . strtoupper($login) . '%\'';
			$sql_count .= ' and upper(u.login) like \'%' . strtoupper($login) . '%\'';
		}
		if (isset($typeUtilisateur) && ($typeUtilisateur > -1)){
			$sql .= ' and upper(u.typeUtilisateurId) like \'%' . strtoupper($typeUtilisateur) . '%\'';
			$sql_count .= ' and upper(u.typeUtilisateurId) like \'%' . strtoupper($typeUtilisateur) . '%\'';
		} 
		
		$sql .= " ORDER BY login";
		
		if (isset($begin) && ($begin > -1)){
			$sql .= ' limit '.$begin.', '. $offset;
		}
		$rs = $this->lquery($sql_count, DB_ECHO);   
		$count = mysqli_fetch_row($rs);
		$this->count = $count[0];
		
		$rs = $this->lquery($sql, DB_ECHO);
		
		return $rs;
	}
	public function isUniqueLogin($login,$id = 0){
		$sql = 'SELECT utilisateurId FROM utilisateur WHERE login = "' . $login . '"';
		$rs = $this->lquery($sql, DB_ECHO);
		$row = mysqli_fetch_assoc($rs);
		if($row == false) return true; 
		else if((isset($row["utilisateurId"])) && $row["utilisateurId"] == $id) return true;
		else return false;
	}
	
	public function createUtilisateur(Utilisateur $utilisateur, $operateurID)
	{
		//On cr?? le nouvel id
		$sql = 'SELECT MAX(utilisateurId) + 1 as idUtilisateur FROM utilisateur';
		$rs = $this->lquery($sql, DB_ECHO);
		$row = mysqli_fetch_assoc($rs);
		//on ins?re un nouvel utilisateur
		$sql = 'INSERT INTO utilisateur (utilisateurId, typeUtilisateurId,swActif,login,nom,prenom,tel,email) 
		VALUES ('.$row['idUtilisateur'].',"'.$utilisateur->getTypeUtilisateurId().'",1 ,"'.$utilisateur->getLogin().'", "'.$utilisateur->getNomReel().
		'","'.$utilisateur->getPrenom().'","'.$utilisateur->getTel().'","'.$utilisateur->getEmail().'")';
		
		$this->lquery($sql, DB_ECHO);
		
		if($operateurID != -1)
		{
			$sql = 'INSERT INTO operateur_utilisateur (utilisateurId, operateurId)
			VALUES ('.$row['idUtilisateur'].','.$operateurID.')';
			
			$this->lquery($sql, DB_ECHO);
		}
		
		//On retourne l'id du nouvel utilisateur
		return $row['idUtilisateur'];
	}
	
	public function updateUtilisateur(Utilisateur $utilisateur, $operateurID)
	{
			$sql = 'UPDATE utilisateur SET typeUtilisateurId = ';
			
			if($utilisateur->getTypeUtilisateurId()!=null)	$sql.= "'" .$utilisateur->getTypeUtilisateurId()."', "; 
			else $sql.= 'null, ';

			$sql.="swActif = ";
			if($utilisateur->getSwActif()!=null) $sql.= " 1, ";
			else $sql.= "0, ";
					
			$sql.="login = ";
			if($utilisateur->getLogin()!=null) 
				$sql.= "'".$utilisateur->getLogin()."', ";
			else 
				$sql.= "null, ";

			$sql.="nom = ";
			if($utilisateur->getNomReel()!=null) $sql.= "'".$this->param_string($utilisateur->getNomReel())."', ";
			else $sql.= "null, ";
						
			$sql.="prenom = ";
			if($utilisateur->getPrenom()!=null) $sql.= "'".$this->param_string($utilisateur->getPrenom())."', ";
			else $sql.= "null, ";
					
			$sql.="tel = ";
			if($utilisateur->getTel()!=null) $sql.= "'".$this->param_string($utilisateur->getTel())."', ";
			else $sql.= "null, ";
					
			$sql.="email = ";
			if($utilisateur->getEmail()!=null)	$sql.= "'".$this->param_string($utilisateur->getEmail())."' ";
			else $sql.= "null ";
					
			$sql.=" WHERE utilisateurId = ".$utilisateur->getUtilisateurId();
				
				
			if($this->lquery($sql, DB_ECHO))
			{
        // Marucci 30/01 Tenir en compte la table operateur_utilisateur
        $sql = 'DELETE FROM operateur_utilisateur WHERE utilisateurId = ' . $utilisateur->getUtilisateurId();
        $this->lquery($sql, DB_ECHO);
        if($operateurID!=-1&&!empty($operateurID)) {
          $sql = 'INSERT INTO `operateur_utilisateur` (`operateurId`, `utilisateurId`) VALUES ('.$operateurID.', '.$utilisateur->getUtilisateurId().')';
          $this->lquery($sql, DB_ECHO);
        }
        return $utilisateur->getUtilisateurId();
			}
	}
	
	public function getWithOperateurId($opId)
	{
		$sql = 'select u.*, ut.label as typeUtilisateurLabel from operateur op  inner join operateur_utilisateur ou on ou.operateurId = op.operateurId inner join utilisateur u on ou.utilisateurId = u.utilisateurId inner join dic_typeUtilisateur ut on u.typeUtilisateurId = ut.typeUtilisateurId where op.operateurId = '.$opId;
		$rs = $this->lquery($sql, DB_ECHO);
		return $rs;
	}
	
	public function getCountUtilisateur(){
		return $this -> lquery("SELECT count(*) as nb FROM " . $this -> getTable() . " WHERE 1=1 " . $this -> where);
	}
	
	public function getCount(){
		return $this->count;
	}
	
	
}
?>