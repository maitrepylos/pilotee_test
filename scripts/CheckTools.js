/*<![CDATA[*/
function SetInputError(obj){obj.style.backgroundColor = '#FFB6C1';}
function SetInputValid(obj){obj.style.backgroundColor = '';}

var regEx_Integer = new RegExp('^[0-9]+$', ''); // Integer pattern
var regEx_Float = new RegExp('^[0-9]+([.,][0-9]+){0,1}$', ''); // Float pattern
var regEx_Email = new RegExp('^[\\w\\-]+(\\.[\\w\\-]+)*@[\\w\\-]+(\\.[\\w\\-]+)*\\.[\\w\\-]{2,}$', 'i'); // EMail pattern : xxx[.xxx]@xxx[.xxx.xxx].[xxx]
var regEx_Phone = new RegExp('^[+]?[0-9 ]*[(]?[0-9 ]+[)]?[0-9 .\\-/]+$', '');

function replace(str, fromString, toString) 
{
	str = str.toString();
	var val = str;
	var sub = "";
	var pos = str.indexOf(fromString);
	if(pos > -1)
	{
		val = str.replace(fromString, toString);
		var i = pos + toString.length - fromString.length;
		sub = val.substring(0, i + 1);
		sub += replace(val.substring(i + 1, val.length), fromString, toString);
		val = sub;
	};
	return(val);
};

function JSBackSlash(string)
{
	var tmp = replace(string, '\\', '\\\\');
	tmp = replace(tmp, '\'', '\\\'');
	return tmp;
};

function IsInteger(value){return regEx_Integer.test(value);};
function verifyInteger(obj)
{
	if(obj.value != '')
	{
		if(!(IsInteger(obj.value)))
		{
			obj.focus();
			obj.select();
			return false;
		};
		return true;
	};
	return false;
};

function IsFloat(value){return regEx_Float.test(value);};
function verifyFloat(obj)
{
	if(obj.value != '')
	{
		if(!(IsFloat(obj.value)))
		{
			obj.focus();
			obj.select();
			return false;
		};
		return true;
	};
	return false;
};

function IsValidDate(year, month, day) 
{
	if (year < 1000) return false;
	try{
		var aDate = new Date(year, month - 1, day);
		return ((aDate.getDate() == day) && (aDate.getMonth() == (month - 1)) && (aDate.getFullYear() == year));
	}catch(e){return false;};
};
function IsDate(value) 
{
  	var date = value.split('/');
  	if(date.length != 3){return false;};
  	var day = parseInt (date[0], 10);
  	var month = parseInt (date[1], 10);
  	var year = parseInt (date[2], 10);
  	if(isNaN(day)||isNaN(month)||isNaN(year)){return false;};
  	return IsValidDate(year, month, day);
};
function verifyDate(obj)
{
	if(obj.value != '')
	{
		if(!(IsDate(obj.value)))
		{
			obj.focus();
			obj.select();
			return false;
		};
		return true;
	};
	return false;
};

function IsEmail(value){return regEx_Email.test(value);};
function verifyMail(obj)
{
	if(obj.value != '')
	{
		if(!(IsEmail(obj.value)))
		{
			obj.focus();
			obj.select();
			return false;
		};
		return true;
	};
	return false;
};

function IsPhone(value){return regEx_Phone.test(value);};
function verifyPhone(obj)
{
	if(obj.value != '')
	{
		if(!(IsPhone(obj.value)))
		{
			obj.focus();
			obj.select();
			return false;
		};
		return true;
	};
	return false;
};

function IsHour(value)
{
  	var hour = value.split(':');
  	if(hour.length != 2){return false;};
  	var h = parseInt (hour[0], 10);
  	var m = parseInt (hour[1], 10);
  	if(isNaN(h)||isNaN(m)){return false;};
  	return (h >= 0 && h < 24 && m >= 0 && m < 60);
};
function verifyHour(obj)
{
	if(obj.value != '')
	{
		if(!(IsHour(obj.value)))
		{
			obj.focus();
			obj.select();
			return false;
		};
		return true;
	};
	return false;
};

function IsEmpty(obj)
{
	return(obj.value == "");
};

/* Check Functions */
function CheckValue(value, operation, lowerBound, higherBound, isNumeric)
{
	var result = true;
	
	if(lowerBound != null && higherBound != null)
	{
		result = ((value >= lowerBound) && (value <= higherBound));
	}
	else if(operation && isNumeric)
	{
		if(lowerBound != null)
		{
			try{result = eval(value + ' ' + operation + ' ' + lowerBound);}catch(e){result = false;};
		};
		
		if(higherBound != null)
		{
			try{result = result && eval(value + ' ' + operation + ' ' + higherBound);}catch(e){result = false;};
		};
	}
	else if(operation)
	{
		if(lowerBound != null)
		{
			try{result = eval('\'' + JSBackSlash(value) + '\' ' + operation + ' \'' + JSBackSlash(lowerBound) + '\'');}catch(e){result = false;};
		};
		
		if(higherBound != null)
		{
			try{result = result && eval('\'' + JSBackSlash(value) + '\' ' + operation + ' \'' + JSBackSlash(higherBound) + '\'');}catch(e){result = false;};
		};
	}
	return result;
};
function CheckText(obj, operation, lowerBound, higherBound)
{
	var result = true;
	return CheckValue(obj.value, operation, lowerBound, higherBound, false);
};
function CheckInteger(obj, operation, lowerBound, higherBound)
{
	var result = IsInteger(obj.value);
	if(!result){return false;}

	return CheckValue(obj.value, operation, lowerBound, higherBound, true);
};
function CheckFloat(obj, operation, lowerBound, higherBound, nbrOfDecimalDigits)
{
	var result = IsFloat(obj.value);
	if(!result){return false;}

	result = CheckValue(obj.value, operation, lowerBound, higherBound, true);
	if (!result){return false;}
	
	if (nbrOfDecimalDigits)
		result = CheckNumberOfDecimal(obj, nbrOfDecimalDigits);
		
	return result;
};
function CheckNumberOfDecimal(val, digitsAfterDecimal)
{
	var result = IsFloat(val.value);
	if(!result){return false;}
	
	if (val.value.indexOf(".") > -1)
		result = val.value.length - (val.value.indexOf(".") + 1) <= digitsAfterDecimal;
	
	if (val.value.indexOf(",") > -1)
		result = val.value.length - (val.value.indexOf(",") + 1) <= digitsAfterDecimal;
	
	return result;
};

function CheckDate_Format(value, format)
{
	var result = "";
	if(IsDate(value))
	{
		var date = value.split("/");
		
		result = replace(format, 'yyyy', date[2]);
		var month = date[1].length < 2 ? "0" + date[1] : date[1];
		result = replace(result, 'mm', month);
		var day = date[0].length < 2 ? "0" + date[0] : date[0];
		result = replace(result, 'dd', day);
	};
	return result;
};
function CheckDate(obj, operation, lowerBound, higherBound)
{
	var result = IsDate(obj.value);
	if(!result){return false;};
	
	var lb, hb;
	if(lowerBound){lb = CheckDate_Format(lowerBound, 'yyyymmdd');};
	if(higherBound){hb = CheckDate_Format(higherBound, 'yyyymmdd');};
	
	return CheckValue(CheckDate_Format(obj.value, 'yyyymmdd'), operation, lb, hb);
};
function CheckHour_Format(value, format)
{
	var result = "";
	if(IsHour(value))
	{
  		var hour = value.split(':');
		var h = hour[0].length < 2 ? "0" + hour[0] : hour[0];
		result = replace(format, 'hh', h);
		var m = hour[1].length < 2 ? "0" + hour[1] : hour[1];
		result = replace(result, 'mm', m);		
	};
	return result;
};
function CheckHour(obj, operation, lowerBound, higherBound)
{
	var result = IsHour(obj.value);
	if(!result){return false;};
	
	var lb, hb;
	if(lowerBound){lb = CheckHour_Format(lowerBound, 'hhmm');};
	if(higherBound){hb = CheckHour_Format(higherBound, 'hhmm');};
	
	return CheckValue(CheckHour_Format(obj.value, 'hhmm'), operation, lb, hb);
};
function CheckMail(obj, operation, lowerBound, higherBound)
{
	var result = IsEmail(obj.value);
	return CheckValue(obj.value, operation, lowerBound, higherBound);
};
function CheckPhone(obj, operation, lowerBound, higherBound)
{
	var result = IsPhone(obj.value);
	return CheckValue(obj.value, operation, lowerBound, higherBound);
};
function CheckCombo(obj, operation, lowerBound, higherBound)
{
	var value = obj.options[obj.selectedIndex].value;
	return CheckValue(value, operation, lowerBound, higherBound);
};

function CheckFunction()
{
	this.ObjArray = new Array();
	
	this.Add = CheckFunction_Add;
	this.IsValid = CheckFunction_IsValid;
	
};

/*
CheckFunction_Add : Add an input object to check
Parameters :
- obj : the input object to check
- type : the type of the value, this parameter can be :
		- TEXT : for character entry
		- INT or INTEGER : for numeric entry
		- EMAIL : for email entry
		- PHONE : for phone number entry
		- DATE : for date entry (format : dd/mm/yyyy)
		- HOUR : for hour entry (format : hh:mm)
		- FLOAT : for decimal number (format : 9999.9)
		- COMBO : for drop down list
- mandatory : indicate if then entry can be blank
		- true : entry must be filled
		- false : entry can be blank
- operation : a compare operator supported by javascript (==, >, >=, <=, <).
		Parameter can be null
- lowerBound : the minimal value to use with the operator.
		Parameter can be null
- higherBound : the maximal value to use with the operator.
		Parameter can be null
- nbrOfDecimals : if parameter type is FLOAT --> check maximum number of decimal
		Parameter can be null
*/
function CheckFunction_Add(obj, type, mandatory, errorMessage, operation, lowerBound, higherBound, nbrOfDecimals)
{
	var idx = this.ObjArray.length;

	if(obj)
	{
		this.ObjArray[idx] = new Array();
		this.ObjArray[idx][0] = obj;
		this.ObjArray[idx][1] = type;
		this.ObjArray[idx][2] = mandatory;
		if(operation != null){operation = operation.toString();};
		this.ObjArray[idx][3] = (operation == '') ? null : operation;
		if(lowerBound != null){lowerBound = lowerBound.toString();};
		this.ObjArray[idx][4] = (lowerBound == '') ? null : lowerBound;
		if(higherBound != null){higherBound = higherBound.toString();};
		this.ObjArray[idx][5] = (higherBound == '') ? null : higherBound;
		this.ObjArray[idx][6] = errorMessage;
		this.ObjArray[idx][7] = nbrOfDecimals;
	};
};

function CheckFunction_IsValid() 
{
	var msg = '';
	var result = true;

	for(var idx = 0; idx < this.ObjArray.length; idx++)
	{	
		try
		{
			var tmp = CheckFunction_CheckField(this.ObjArray[idx][0], this.ObjArray[idx][1], this.ObjArray[idx][2], this.ObjArray[idx][3], this.ObjArray[idx][4], this.ObjArray[idx][5], this.ObjArray[idx][7]);
			if(result) result = tmp;
			if(!tmp && this.ObjArray[idx][6] != null && this.ObjArray[idx][6] != '')
			{
				if(msg != '') msg += String.fromCharCode(13, 10);
				msg += this.ObjArray[idx][6];
			}
		}
		catch(e){};
	};
	if(msg != '') alert(msg);
	return result;
};

function CheckFunction_CheckField(obj, type, mandatory, operation, lowerBound, higherBound, nbrOfDecimalDigits)
{
	var result = true;

	var emptyField = IsEmpty(obj);
	result = mandatory ? !emptyField : true;
	
	if(result && !emptyField)
	{
		switch(type.toUpperCase())
		{
			case 'TEXT' :	
				result = CheckText(obj, operation, lowerBound, higherBound);
				break;
			case 'EMAIL' :
				result = IsEmail(obj.value);
				break;
			case 'DATE' :
				result = CheckDate(obj, operation, lowerBound, higherBound);
				break;
			case 'HOUR' :
				result = CheckHour(obj, operation, lowerBound, higherBound);
				break;
			case 'INTEGER', 'INT':
				result = CheckInteger(obj, operation, lowerBound, higherBound);
				break;
			case 'FLOAT':
				result = CheckFloat(obj, operation, lowerBound, higherBound, nbrOfDecimalDigits);
				break;
			case 'PHONE':
				result = IsPhone(obj.value);
				break;
			case 'COMBO':
				result = CheckCombo(obj, operation, lowerBound, higherBound);
				break;
			break;
		};
	}

	if(!result)
		SetInputError(obj);
	else
		SetInputValid(obj);

	return result;
};
/*]]>*/

