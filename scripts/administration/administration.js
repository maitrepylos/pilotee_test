(function(window, $) {
		var searchUserHandler = function(event) {
			 var data = {};
			data["begin"] = 0;
	        data["offset"] = 15;
	        if ($(event.target).hasClass('first')) {
	            data["begin"] = 0;
	        } else if ($(event.target).hasClass('previous')) {
	            data["begin"] = (parseInt($('.inputPaginator', $('.paginator')).val(), 10) - 2) * data["offset"];
	        } else if ($(event.target).hasClass('next')) {
	            data["begin"] = (parseInt($('.inputPaginator', $('.paginator')).val(), 10)) * data["offset"];
	        } else if ($(event.target).hasClass('last')) {
	            data["begin"] = (parseInt($('.inputPaginator', $('.paginator')).attr("data-max"), 10) - 1) * data["offset"];
	        }
	        
			data["nom"] = jQuery('#nom').val();
			data["prenom"] = jQuery('#prenom').val();
			data["email"] = jQuery('#email').val();
			data["login"] = jQuery('#login').val();
			data["typeUtilisateur"] = jQuery('#typeUtilisateur').val();
			data["slcField"] = jQuery('#slcField').val();
			data["operatorField"] = jQuery('#operatorField').val();
			 $.ajax("index.php", {
            "data" : {
            	"module" : "administration_ajax",
                "action" : "user_search_populate",
                "data" : JSON.stringify(data)
            },
            "contentType": "application/x-www-form-urlencoded;charset=ISO-8859-1",
            "success" : function(data) {
                $('#form_table_users').remove();
				$('#SearchUsers').after(data);
                $(":input.first, :input.previous, :input.next, :input.last", $('#table_users .paginator')).bind('click', function(event) {
                    searchUserHandler(event)
                });
                $("html,body").animate({
                    "scrollTop" : $('#submitSearchUsers').offset.top
                }, "slow");
            }
       	 	});
		}
		
	$(function() {
		//test si il s'agit bien d'une action sauvegarde de user
		if(document.URL.split('?')[1].split('&')[1]=="action=saveuser")
		  //new Notification('success', "L'utilisateur a été sauvegardé", 4000).show();

		$('#submitSearchUsers').bind('click', function(event) {
	            searchUserHandler(event);
	        });
        $('#back').bind('click', function(event) {
            window.location.href='./index.php?module=administration&action=user_search';
        });
        $('#delete_user').bind('click', function(event) {
            if (confirm("Êtes-Vous sûr de vouloir supprimer cet utilisateur ? ")) {
       			 deleteUserHandler(event);
    		}
        });
	     $(":input.first, :input.previous, :input.next, :input.last", $('#table_users .paginator')).bind('click', function(event) {
                    searchUserHandler(event);
                });
	});
})(window, jQuery);
