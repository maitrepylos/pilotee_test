function CForm(data)
{
	if (data['fields'] != null && data['fields'].length > 0)
	{
		this._formats = new Array();
		var id = new Array();
		for ( var x = 0; x < data['fields'].length; x++)
		{
			id.push(data['fields'][x]['id']);
			this._formats[data['fields'][x]['id']] = {
				format : data['fields'][x]['id']['format']
			};

			this.setFormat(data['fields'][x]['id'], data['fields'][x]['format']);
		}
	}
}

CForm.prototype._formats;

CForm.prototype.setFormat = function(id, exemple)
{
	var el = null;

	if ((typeof (id)).toLowerCase() == 'object')
	{
		el = id;
		id = el.id;
	}
	else
	{
		var el = document.getElementById(id);
	}

	if (el != null)
	{
		var inputExemple = el.cloneNode(false);
		inputExemple.name = inputExemple.name + '_exemple';
		inputExemple.id = inputExemple.id + '_exemple';
		inputExemple.className = (inputExemple.className != null && inputExemple.className != '' ? inputExemple.className
				+ ' js_exemple'
				: 'js_exemple');
		inputExemple.value = exemple;

		inputExemple.onfocus = function()
		{
			inputExemple.style.display = 'none';
			el.style.display = 'inline';
			el.focus();
		};

		el.onblur = function()
		{
			if (el.value == null || el.value == '')
			{
				inputExemple.style.display = 'inline';
				el.style.display = 'none';
			}
		};

		el.parentNode.appendChild(inputExemple);

		if (el.value == null || el.value == '')
		{
			inputExemple.style.display = 'inline';
			el.style.display = 'none';
		}
		else
		{
			inputExemple.style.display = 'none';
			el.style.display = 'inline';
		}

		return true;
	}
};

function highlightFields(fields)
{
	var x = 0;
	for (x = 0; x < fields.length; x++)
	{
		var elem = document.getElementById(fields[x]);
		if (elem != null)
		{
			if (elem.className != '')
			{
				elem.className = elem.className + ' field_highlight';
			}
			else
			{
				elem.className = 'field_highlight';
			}
		}
	}
}

function hideShortcutPanel(id)
{

	var obj = document.getElementById(id);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		document.getElementById("shortcut_items").style.display = '';
		document.getElementById("open_close").title = "Fermer le panneau de raccourcis";
		document.getElementById("open_close").src = "./styles/img/hide.gif";
	}
	else
	{
		obj.style.display = 'none';
		document.getElementById("shortcut_items").style.display = 'none';
		document.getElementById("open_close").src = "./styles/img/show.gif";
		document.getElementById("open_close").title = "Ouvrir le panneau de raccourcis";
	}
}

function clearForm(formId)
{
	var form, elements, i, elm;
	form = document.getElementById ? document.getElementById(formId) : document.forms[formId];

	if (document.getElementsByTagName)
	{
		elements = form.getElementsByTagName('input');
		for (i = 0, elm; elm = elements.item(i++);)
		{
			if (elm.getAttribute('type') == "text")
			{
				elm.value = '';
			}
			else if (elm.getAttribute('type') == "checkbox")
			{
				if ((elm.getAttribute('name') == 'action_lab' && elm.checked)
						|| (elm.getAttribute('name') == 'evaluation' && elm.checked)
						|| (elm.getAttribute('name') == 'swActif' && elm.checked))

					elm.checked = true;
				else
					elm.checked = false;
			}
		}
		elements = form.getElementsByTagName('textarea');
		for (i = 0, elm; elm = elements.item(i++);)
		{
			elm.value = '';
		}
		elements = form.getElementsByTagName('select');

		for (i = 0, elm; elm = elements.item(i++);)
		{
			elm.value = '-1';
		}
	}
	if (formId == 'SearchContact')
	{
		document.getElementById('etablissement').length = 0;
		document.getElementById('selectedEts').value = '';
		document.getElementById('selectedName').value = '';
		document.getElementById('ActifContact').checked = true;
	}
	else if (formId == 'formContact')
	{
		var divGlobal = document.getElementById("divEtablissementsContact");
		var table = document.getElementById("tableEtablissementsContact");
		if (table)
			divGlobal.removeChild(table);
		document.getElementById('nbLignes').value = 0;
		document.getElementById('ignoreDoublon').value = 0;
		document.getElementById('selectDoublon').value = 0;
		document.getElementById('etabIds').value = '';
		// action
		divGlobal = document.getElementById('divActionContact');
		table = document.getElementById('tableActionsContact');
		if (table)
			divGlobal.removeChild(table);

		document.getElementById('nbLignesAction').value = 0;
		document.getElementById('actionIds').value = '';
		divGlobal = document.getElementById('divBourseContact');
		table = document.getElementById('tableBoursesContact');
		if (table)
			divGlobal.removeChild(table);
		document.getElementById('nbLignesBourse').value = 0;
		document.getElementById('bourseIds').value = '';
	}
	else if (formId == 'SearchAction' || formId == 'SearchBourse')
	{
		document.getElementById('etablissement').length = 0;
		document.getElementById('selectedEts').value = '';
		document.getElementById('selectedName').value = '';
		document.getElementById('anneesScolaire').selectedIndex= document.getElementById('anneesScolaire').length - 1;
		if (formId == 'SearchBourse'){
			document.getElementsByName('bourseValide')[0].checked = true;
			document.getElementsByName('sub_eval')[0].checked = true;
		}
		else {
			document.getElementsByName('actionLab')[0].checked = true;
			document.getElementsByName('actionNonLab')[0].checked = true;
			document.getElementsByName('actionMicro')[0].checked = true;
			document.getElementsByName('actionValide')[0].checked = true;
			document.getElementById('niveauAction').selectedIndex = 0;
			document.getElementById('tr_operateur').checked = true;
		}
	}
	else if (formId == 'formAction')
	{
		if (document.getElementById('selectedOps'))
			document.getElementById('selectedOps').value = '';

		if (document.getElementById('selectedOpsName'))
			document.getElementById('selectedOpsName').value = '';

		if (document.getElementById('assocOp'))
			document.getElementById('assocOp').length = 0;

		document.getElementById('nbLignesContact').value = 0;
		document.getElementById('contactIds').value = '';
		divGlobal = document.getElementById('divContactsAction');
		table = document.getElementById('tableContactsAction');
		if (table)
			divGlobal.removeChild(table);

		document.getElementById('nbLignes').value = 0;
		document.getElementById('etabIds').value = '';

		divGlobal = document.getElementById('divEtablissementsAction');
		table = document.getElementById('tableEtablissementsAction');
		if (table)
			divGlobal.removeChild(table);
	}
	else if (formId == 'ReportAction')
	{
		// TODO : FFI : clean the list actions
		var tr_categorie = document.getElementById('tr_categorie');
		var tr_operateur = document.getElementById('tr_operateur');
		document.getElementById('actionLab').checked = false;
		document.getElementById('projEntr').checked = false;
		document.getElementById('form').checked = false;
		
		// actionNonLab
		// actionMicro
		tr_operateur.style.display = '';
		tr_categorie.style.display = '';
	}
	else if (formId == 'formFormation')
	{
		if (document.getElementById('assocFormateur'))
			document.getElementById('assocFormateur').length = 0;
	}
}

function sure(lib)
{
	return confirm(lib);
}

function submitForm(formId)
{
	var form;
	form = document.getElementById ? document.getElementById(formId) : document.forms[formId];
	form.submit();
}

function EmptyList(ctrlId, ids, names)
{
	document.getElementById(ctrlId).length = 0;
	document.getElementById(ids).value = "";
	document.getElementById(names).value = "";
}

function show_hide(id)
{
	var elt = document.getElementById(id);
	if (elt.style.display == 'none')
		elt.style.display = '';
	else
		elt.style.display = 'none';
}

function removeRow(id, nbLigneId)
{
	elt = document.getElementById(id);
	var p = elt.parentNode;
	p.removeChild(elt);

	if (p.rows.length == 1)
	{
		var table = p.parentNode;
		table.parentNode.removeChild(p.parentNode);
		document.getElementById(nbLigneId).value = 0;
	}
}

function removeRowGlobal(id, checkboxId)
{

	elt = document.getElementById(id);
	var p = elt.parentNode;
	p.removeChild(elt);
	checkbox = document.getElementById(checkboxId);
	checkbox.checked = false;

	if (p.rows == null || p.rows.length == 1)
	{
		var table = p.parentNode;
		table.parentNode.removeChild(p.parentNode)
	}
}

function select_search(value, select)
{
	for ( var x = 0; x < select.options.length; x++)
	{
		if (select.options[x].value == value)
			return x;

	}
	return -1;
}

function getEcartDate(dateFin, dateDeb)
{
	var one_day = 1000 * 60 * 60 * 24;
	return Math.ceil((dateFin.getTime() - dateDeb.getTime()) / (one_day));
}

function updateSelect(idP, idA)
{
	selectP = document.getElementById(idP);
	selectA = document.getElementById(idA);
	jx.load('./index.php?module=ajax&val=' + selectP.value, function(data)
	{
		selectA.length = 0;
		selectA.options[selectA.length] = new Option('Tous', '-1');
		var tabElt = data.split("/#-#/");
		for (i = 0; i < tabElt.length; i++)
		{
			tabOp = tabElt[i].split('||');
			id = tabOp[0];
			value = tabOp[1];
			selectA.options[selectA.length] = new Option(value, id);
		}
	}, 'text');
}

function updateDivCheckBox()
{
	var chks = document.getElementsByTagName("input");
	var ids = '';
	for ( var i = 0; i < chks.length; i++)
	{
		if (chks[i].name.match("^chk_pro_"))
		{
			if (chks[i].checked)
			{
				ids += chks[i].name.replace("chk_pro_", "") + "||";
			}
		}
	}

	jx.load('./index.php?module=ajax&report=ok&val=' + ids, function(data)
	{
		div = document.getElementById('divArr');
		div.innerHTML = '';

		var tabElt = data.split("/#-#/");
		for (i = 0; i < tabElt.length; i++)
		{
			tabOp = tabElt[i].split('||');
			id = tabOp[0];
			value = tabOp[1];
			div.innerHTML += '<div style="vertical-align:bottom;"><input type="checkbox" name="chk_arr_' + id
					+ '"/>&nbsp;' + value + '</div>';
		}
	}, 'text');

}

function intval(mixed_var, base)
{
	var type = typeof (mixed_var);

	if (type === 'boolean')
	{
		return (mixed_var) ? 1 : 0;
	}
	else if (type === 'string')
	{
		tmp = parseInt(mixed_var, base || 10);
		return (isNaN(tmp) || !isFinite(tmp)) ? 0 : tmp;
	}
	else if (type === 'number' && isFinite(mixed_var))
	{
		return Math.floor(mixed_var);
	}
	else
	{
		return 0;
	}
}

// Validation du formulaire creer contact
// function submitFormContact(idForm)
// {
// res1 = true;
// var nom = document.getElementById("nom_contact");
// var prenom = document.getElementById("prenom_contact");
// var email1 = document.getElementById("mail1");
// var email2 = document.getElementById("mail2");
// var tel1 = document.getElementById("tel1");
// var tel2 = document.getElementById("tel2");
//				
// var chk = new CheckFunction();
// chk.Add(nom, 'TEXT', true); chk.Add(prenom, 'TEXT', true);
// chk.Add(email1, 'EMAIL', false); chk.Add(email2, 'EMAIL', false);
// chk.Add(tel1, 'PHONE', false); chk.Add(tel2, 'PHONE', false);
//
// //ContactAction
// var nbLigneAction = document.getElementById('nbLignesAction');
// for(i=0; i<nbLigneAction.value; i++)
// {
// var role = document.getElementById('roleAction_'+i);
// if(role)
// {
// if(role.value == '-1')
// {
// SetInputError(role);
// res1 = false;
// }
// }
// }
// //ContactBourse
// var nbLigneBourse = document.getElementById('nbLignesBourse');
// for(i=0; i<nbLigneBourse.value; i++)
// {
// var role = document.getElementById('roleBourse_'+i);
// if(role)
// {
// if(role.value=='-1')
// {
// SetInputError(role);
// res1 = false;
// }
// }
// }
//
// var res = chk.IsValid();
// if (res && res1)
// {
// submitForm(idForm);
// }
// else return false;
// }

function getTypeActionLab()
{
	var anneeScolaire = document.getElementById("anneesScolaire").value;
	var organisateur = document.getElementById("organisateur").value;
	var selectType = document.getElementById("actionTypeLab");
	selectType.length = 0;
	selectType.options[selectType.length] = new Option('', '-1');

	for (i = 0; i < arr.length; i++)
	{
		if (anneeScolaire != '-1' && organisateur != '-1')
		{
			if (arr[i]['anneeId'] == anneeScolaire && arr[i]['operateurId'] == organisateur)
				selectType.options[selectType.length] = new Option(arr[i]['label'], arr[i]['actionLabelId']);
		}
		else
		{
			if (anneeScolaire != '-1')
			{
				if (arr[i]['anneeId'] == anneeScolaire)
					selectType.options[selectType.length] = new Option(arr[i]['label'], arr[i]['actionLabelId']);
			}
		}
	}
}

function viewFinalitePeda(action)
{
	display = 'none';
	if (action == 'oui')
		display = '';

	nbFinalitePeda = document.getElementById('nbFinalitePeda').value;
	for (i = 0; i < nbFinalitePeda; i++)
		document.getElementById('tr_finalite_' + i).style.display = display;
}

function checkTypeAction(selectBox)
{
	if (selectBox.value == 'LABEL')
	{
		document.getElementById("tr_categorie").style.display = 'none';
		document.getElementById("tr_organisateur").style.display = '';
		document.getElementById("tr_typeActionLab").style.display = '';
		document.getElementById("tr_nomGen").style.display = 'none';
		// document.getElementById("tr_refOperateur").style.display = '';
		document.getElementById("tr_assOp").style.display = '';
	}
	else if (selectBox.value == 'NONLA')
	{
		document.getElementById("tr_categorie").style.display = '';
		document.getElementById("tr_organisateur").style.display = 'none';
		document.getElementById("tr_typeActionLab").style.display = 'none';
		document.getElementById("tr_nomGen").style.display = '';
		// document.getElementById("tr_refOperateur").style.display = 'none';
		document.getElementById("tr_assOp").style.display = 'none';
	}
	else if (selectBox.value == 'AMICR')
	{
		document.getElementById("tr_categorie").style.display = 'none';
		document.getElementById("tr_organisateur").style.display = 'none';
		document.getElementById("tr_typeActionLab").style.display = 'none';
		document.getElementById("tr_nomGen").style.display = '';
		// document.getElementById("tr_refOperateur").style.display = 'none';
		document.getElementById("tr_assOp").style.display = 'none';
	}
}

function Func1()
{

}

function Func1Delay()
{
	setTimeout("Func1()", 5000);
}

function reloadFrameBlank(idFrame)
{
	//pose probl�me quand on quitte une popup (ajout contact par ex et qu'on la reouvre)
	frm = document.getElementById(idFrame);
	if (frm)
		frm.src = 'about:blank';
}

function gup(name)
{
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if (results == null)
		return "";
	else
		return results[1];
}

// Declaring valid date character, minimum year and maximum year
var dtCh = "/";
var minYear = 1900;
var maxYear = 2100;

function isInteger(s)
{
	var i;
	for (i = 0; i < s.length; i++)
	{
		// Check that current character is number.
		var c = s.charAt(i);
		if (((c < "0") || (c > "9")))
			return false;
	}
	// All characters are numbers.
	return true;
}

function stripCharsInBag(s, bag)
{
	var i;
	var returnString = "";
	// Search through string's characters one by one.
	// If character is not in bag, append to returnString.
	for (i = 0; i < s.length; i++)
	{
		var c = s.charAt(i);
		if (bag.indexOf(c) == -1)
			returnString += c;
	}
	return returnString;
}

function daysInFebruary(year)
{
	// February has 29 days in any year evenly divisible by four,
	// EXCEPT for centurial years which are not also divisible by 400.
	return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
}
function DaysArray(n)
{
	for ( var i = 1; i <= n; i++)
	{
		this[i] = 31;
		if (i == 4 || i == 6 || i == 9 || i == 11)
		{
			this[i] = 30;
		}
		if (i == 2)
		{
			this[i] = 29;
		}
	}
	return this;
}

function isDate(dtStr)
{
	var daysInMonth = DaysArray(12);
	var pos1 = dtStr.indexOf(dtCh);
	var pos2 = dtStr.indexOf(dtCh, pos1 + 1);
	var strDay = dtStr.substring(0, pos1);
	var strMonth = dtStr.substring(pos1 + 1, pos2);
	var strYear = dtStr.substring(pos2 + 1);
	strYr = strYear;
	if (strDay.charAt(0) == "0" && strDay.length > 1)
		strDay = strDay.substring(1);
	if (strMonth.charAt(0) == "0" && strMonth.length > 1)
		strMonth = strMonth.substring(1);
	for ( var i = 1; i <= 3; i++)
	{
		if (strYr.charAt(0) == "0" && strYr.length > 1)
			strYr = strYr.substring(1);
	}
	month = parseInt(strMonth);
	day = parseInt(strDay);
	year = parseInt(strYr);
	if (pos1 == -1 || pos2 == -1)
	{
		// alert("The date format should be : mm/dd/yyyy");
		return false;
	}
	if (strMonth.length < 1 || month < 1 || month > 12)
	{
		// alert("Please enter a valid month");
		return false;
	}
	if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year))
			|| day > daysInMonth[month])
	{
		// alert("Please enter a valid day");
		return false;
	}
	if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear)
	{
		// alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear);
		return false;
	}
	if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false)
	{
		// alert("Please enter a valid date");
		return false;
	}
	return true;
}
/* Add Notification jquery Marucci 26/12/2013  */
window.notifications = {
    "id" : 0
};

window.computePositions = function(notif) {
    var height = notif.$notif.outerHeight() + 25;
    $.each(window.notifications, function(index, notification) {
        if (!isNaN(index) && index != notif.id) {
            if (notification.$notif.is(':visible')) {
                notification.$notif.animate({
                    "bottom" : "-=" + height + "px"
                });
            }
        }
    });
}

window.Notification = function(type, content, timeout, element) {
    var prox = this, timeout = timeout || 0;
    this.getPosition = function() {
        var length = window.notifications.length, inc = 25;
        $.each(window.notifications, function(index, notification) {
            if (!isNaN(index)) {
                if (notification.$notif.is(':visible')) {
                    inc += notification.$notif.outerHeight() + 25;
                }
            }
        });
        return {
            "bottom" : inc,
            "right" : 25
        };
    };

    this.show = function() {
        this.$notif.fadeIn("fast");
        if (prox.timeout != 0) {
            window.setTimeout(function() {
                prox.hide();
            }, prox.timeout);
        }
        return this;
    }

    this.hide = function() {
        window.computePositions(this);
        this.$notif.fadeOut("fast", function() {
            prox.destroy();
        });
        return this;
    }

    this.destroy = function() {
        this.$notif.empty().remove();
        this.$notif.unbind('mouseenter.notification,mouseleave.notification');
        delete window.notifications[this.id];
        return this;
    }

    this.init = function(type, content, timeout, element) {
        this.id = notifications.id;
        this.timeout = timeout;

        var $notif = $('<div></div>', {
            "id" : "notification-" + this.id,
            "class" : "notification shadow"
        }).appendTo('body').css(this.getPosition()).addClass(type);
        $notif.text(content);
        this.$notif = $notif;

        if (element && $notif) {
            $notif.bind({
                'mouseenter.notification' : function(event) {
                    $(element).addClass('notificationFocused');
                },
                'mouseleave.notification' : function(event) {
                    $(element).removeClass('notificationFocused');
                },
                'click' : function(event) {
                    $("html,body").animate({
                        "scrollTop" : $(element).offset().top - 25
                    }, "slow");
                }
            }).addClass('hovered');
        }

        notifications.id = notifications.id + 1;
        notifications[this.id] = this;
        return this;
    }
    return this.init(type, content, timeout, element);
}

