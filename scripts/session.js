(function(window, $) {
//Récursivité pour la temporisation de la session
	//Call ajax pour récupérer la durée d'une session
	// On va boucler toutes les secondes pour voir si la session va arriver à expiration
	var timer = function()
	{	
		 $.ajax({
			 //Ajout de la page en brut car sinon ajout de style et il ne trouve pas le nombre
            "url": "./controller/session_timelife.php",
            "success" : function(data) {
 				var expired_date = parseInt($.now()) + parseInt(data)*1000;
 				var date = new Date(expired_date);
 				var interval = setInterval(function(){
 					//Marge de 30 secondes
 					if(expired_date-30000<$.now()) {
 						if (confirm('Voulez-vous prolongez la session en cours ?')){
 							$.ajax({
           					 	"url": "./index.php?module=session_restart",
           					 	"success" : function(data){
           					 	
           					 	}
           					});
           					timer();
 						}
 						clearInterval(interval);	
 					}
 				},1000);
            }
       	 	});
       }
       timer();
})(window, jQuery)