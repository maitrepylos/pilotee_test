<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class ActionDetailView extends BaseView
{
	private $user = null;
	
	public function Render($action)
	{
		$this->user = Session::GetInstance()->getCurrentUser();
?>
	<script>
	function ShowFormAddAction()
	{
		var frm = document.getElementById("frmFormAddAction");
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=actionPopup&action=add'; ?>';		
		Popup.showModal('formAddAction');
	}
	
	function ShowFormAddContact()
	{
		var ets = new Array();
		var id = document.getElementById("chk_action_id").value;
		var nom = document.getElementById("chk_action_name").value;
		ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
		
		var frm = document.getElementById("frmFormAddContact");		
		Popup.showModal('formAddContact');
		frmContent = frm.contentWindow.document || frm.contentDocument;
		if(ets.length>0)
		{
			SetActionContact(ets, frmContent);
		}
		else
		{
			frmContent.getElementById('divActionContact').innerHTML = '';
		}
	
	}
	function SetActionContact(objectsArray, frmContent)
	{
	  if (objectsArray.length > 0)		  
	  {
	  	var nbLignes = 0;
		var divGlobal = frmContent.getElementById('divActionContact');			
		var tab = frmContent.getElementById('tableActionsContact');
		if(tab==null)
		{
				tab = divGlobal.appendChild(frmContent.createElement('table'));
				tab.setAttribute('id','tableActionsContact');
				tab.setAttribute('style','width:80%');
				frmContent.getElementById("nbLignesAction").value = '0';
				frmContent.getElementById('actionIds').value = '';
				
				newRow = tab.insertRow(-1);
				newRow.setAttribute('id','headAction_-1');
				newCell = newRow.insertCell(0);
				newCell.innerHTML = "<center>Action</center>";
				newCell = newRow.insertCell(1);
				newCell.innerHTML = "<center>R�le</center>";
				newCell = newRow.insertCell(2);
				newCell.innerHTML = "";
			}
			else
			{
				nbLignes = frmContent.getElementById("nbLignesAction").value;
			}	

			autoid = nbLignes;
		
	 		for(j=0;j<objectsArray.length;j++)
			{
				var actionSelect = frmContent.getElementById("actionIds");
				var reg=new RegExp("[;]+", "g");
				tableActionSelect = actionSelect.value.split(reg);
				var isPresent = false;					
				for(var x=0; x<tableActionSelect.length; x++)
				{
					if(tableActionSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
			
				if(!isPresent)
				{
					actionSelect.value += (actionSelect.value == "" ? "" : ";") + objectsArray[j].id;
					newRow = tab.insertRow(-1);
 					newRow.setAttribute('id','action_'+nbLignes);

				 	for(i=0;i<5;i++)
			 		{ 		 		 
		 				newCell = newRow.insertCell(i);
		 			 	var tmp = "";
 						switch(i)
						{
							case 0 :
						    	tmp = '<input type="text" name="actionName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />';
						    	tmp += "<input name='actionId_"+nbLignes+"' id='actionId_"+nbLignes+"' type='hidden' value='"+objectsArray[j].id+"' />";
								newCell.innerHTML = tmp;
								break;
							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>";
								newCell.innerHTML = "<select name='roleAction_"+nbLignes+"' style='width:175px;'>"+
												"<option value='-1'></option>"+tmp+"</select>";
								break;
							case 2 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"action_"+nbLignes+"\", \"nbLignesAction\")'/>";
								break; 							
						}
					
						with(this.newCell.style)
						{	
				 			width = '100px';
				 			textAlign = 'center';
				 			padding = '5px';
						} 
						autoid++;
				 	}
				 	nbLignes++;
				 }
			  }
	 		frmContent.getElementById("nbLignesAction").value = nbLignes;
	   }
	}

	function updateAction(actionId)
	{
		parent.document.location.href = '<?php echo $this->BuildURL('action') .'&action=update&updateId='; ?>' + actionId;	
	}
	
	function deleteAction(actionId)
	{
		if (sure("Supprimer l'action ?") == true)	
			parent.document.location.href = '<?php echo $this->BuildURL('action') .'&action=delete&deleteId='; ?>' + actionId; 
	}
	
	function ShowHistoriqueDetail()
	{
		var frm = document.getElementById("frmHistoriqueDetail");
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=historiquePopup&id='.$action->GetActionId().'&objet=action';?>';			
		Popup.showModal('HistoriqueDetail');
	}
	</script>
	
	<div id="HistoriqueDetail" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Historique : <?php echo $action->GetNom(); ?></td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('HistoriqueDetail');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmHistoriqueDetail" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>	
	
	<div id="formAddContact" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er un contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddContact');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddContact" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div id="formAddAction" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er une action</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddAction');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd">
				<iframe id="frmFormAddAction" frameborder=0 src="" style="width: 1024px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	
	<div class="boxGen">
		<div class="boxPan" style="padding:5px;">
			<input type="hidden" id="chk_action_id" value="<?php echo $action->GetActionId();?>">
			<input type="hidden" id="chk_action_name" value="<?php echo $action->GetNom();?>">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Ann�e scolaire :</td>
						<td class="detail"><?php echo ToHTML($action->GetAnneeId());?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Type d'action :</td>
						<td class="detail"><?php echo ToHTML($action->GetTypeActionLabel());?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Cat�gorie(Non lab.) :</td>
						<td class="detail"><?php echo ToHTML($action->GetCategActionNonLabel());?></td>
					</tr>
					
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Global :</td>
						<td class="detail" style="color:<?php echo $action->GetSwGlobalColor(); ?>"><?php echo $action->GetSwGlobalLabel(); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Nombre d'apprenants :</td>
						<td class="detail"><?php echo ToHTML($action->GetNbApprenants()); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Nombre d'enseignants :</td>
						<td class="detail"><?php echo ToHTML($action->GetNbEnseignants()); ?></td>
					</tr>
					
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Niveau/degr�/fili�re :</td>
						<td class="detail">
						<?php
							$filieres = $action->getFiliereDegreNiveau();
							for($i=0; $i<$filieres->Count(); $i++)
								echo $filieres->items($i)->GetActionFiliereDegreNiveau().' '.'('.$filieres->items($i)->GetNbApprenants().') <br/>';
						?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">P�dagogique :</td>
						<td class="detail">
						<?php
							$peda = $action->GetFinalitePedagog();
							for($i=0; $i<$peda->Count(); $i++)
								echo $peda->items($i)->GetFinalitePedagog().' '.'('.$peda->items($i)->GetNbApprenants().') <br/>';
						?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Section :</td>
						<td class="detail">
						<?php
							$sections = $action->GetSections();
								for($i=0; $i<$sections->Count(); $i++)
									echo $sections->items($i)->GetActionSection().' '.'('.$sections->items($i)->GetNbApprenants().') <br/>';
						?>
						</td>
					</tr>
  				 </table>
				</td>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Date :</td>
						<td class="detail"><?php echo $action->GetDateActionString(false); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Action :</td>
						<td class="detail"><?php echo ToHTML($action->getActionLabel());?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Encodage :</td>
						<?php
							echo'<td class="detail">'.ToHTML($action->GetStatutAction()).'</td>';
						?>
					</tr>
					
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Op�rateur :</td>
						<?php
								if($action->GetOperateurId()!=null)
								{	
									$operateur = $action->GetOperateur();
									echo'<td class="detail"><a href=\''.$this->BuildURL('operateurDetail').'&id='.$operateur->GetOperateurId().'\'>'. $operateur->GetNom().'</a></td>';
								}
								else 
									echo'<td class="detail"></td>';
						?>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr style="display:none">
						<td class="label">R�f. chez l'op�rateur :</td>
						<td class="detail"><?php echo ToHTML($action->GetRefChezOperateur()); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">R�f. chez l'ASE :</td>
						<td class="detail"><?php echo ToHTML($action->GetRefChezASE()); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Site internet :</td>
						<td class="detail"><?php echo $action->GetWebsiteAsAnchor();?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Commentaire :</td>
						<td class="detail"><?php echo ToHTML($action->GetCommentaire());?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Cr�ation :</td>
						<td class="detail">
						<?php
							$user  = $action->GetCreatedByUser();
							echo 'le '.$action->GetDateCreatedString(false);

							if (!empty($user))
							{
								if($user->getTypeUtilisateurId() != 'AGE')
									echo ' par ' . $user->GetNom().' '.$user->GetPrenom();
								else
									echo ' par <a href=\''.$this->BuildURL('agentDetail').'&id='.$user->getUtilisateurId().'\'>'.$user->GetNom().' '.$user->GetPrenom().'</a>';
							}
						?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Modification :</td>
						<td class="detail">
						<?php
							$user  = $action->GetUpdatedByUser();
							echo 'le '.$action->GetDateUpdatedString(false);
							if (!empty($user))
							{
								if($user->getTypeUtilisateurId() !='AGE')	
									echo ' par '.$user->GetNom().' '.$user->GetPrenom();
								else
									echo ' par <a href=\''.$this->BuildURL('agentDetail').'&id='.$user->getUtilisateurId().'\'>'.$user->GetNom().' '.$user->GetPrenom().'</a>';
							}
						?>
						</td>
					</tr>					
  				  </table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<script>
						function ActionDetailViewClass() 
						{
							this.current = 'All';
							this.tabs = new Array('All', 'Contact2', 'Etablissement2');
						}

						ActionDetailViewClass.prototype.onMouseOver = function(liID)
						{
							document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
							document.getElementById("spn" + liID).style.color = "#FFFFFF";
							document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
						}

						ActionDetailViewClass.prototype.onMouseOut = function(liID)
						{
							if (this.current != liID)
							{
								document.getElementById("spn" + liID).style.backgroundPosition = "100% 0%";
								document.getElementById("spn" + liID).style.color = "#737373";
								document.getElementById("a" + liID).style.backgroundPosition = "0% 0%";
							}
						}
						
						ActionDetailViewClass.prototype.onLIClick = function(liID)
						{
							this.current = liID;
							
							for (var i = 0; i < this.tabs.length; i++)
							{
								document.getElementById("spn" + this.tabs[i]).style.backgroundPosition = "100% 0%";
								document.getElementById("spn" + this.tabs[i]) .style.color = "#737373";
								document.getElementById("a" + this.tabs[i]).style.backgroundPosition = "0% 0%";
							}

							document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
							document.getElementById("spn" + liID).style.color = "#FFFFFF";
							document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
							
							var contact = document.getElementById('Contact');
							var etablissement = document.getElementById('Etablissement');
							
							if (liID == 'All')
							{
								if (contact) contact.style.display = '';
								if (etablissement) etablissement.style.display = '';
							}
							else if (liID == 'Contact2')
							{
								if (contact) contact.style.display = '';
								if (etablissement) etablissement.style.display = 'none';
							}
							else if (liID == 'Etablissement2')
							{
								if (etablissement) etablissement.style.display = '';
								if (contact) contact.style.display = 'none';
							}
						}
						window.edvc = new ActionDetailViewClass();
						
						function ShowEtablissementInfo(id)
						{
							window.location = '<?php echo $this->BuildURL('etablissementDetail') . '&id='; ?>' + id;
						}
				</script>
					<div class="header" style="border-bottom:3px solid #EAC3C3;">
						<ul>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('All');" OnMouseOut="javascript:window.edvc.onMouseOut('All');"><a id="aAll" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('All');"><span id="spnAll" class="expand">Tout</span></a></li>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('Etablissement2');" OnMouseOut="javascript:window.edvc.onMouseOut('Etablissement2');"><a id="aEtablissement2" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Etablissement2');"><span id="spnEtablissement2" class="expand">Etablissements d'enseignement</span></a></li>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('Contact2');" OnMouseOut="javascript:window.edvc.onMouseOut('Contact2');"><a id="aContact2" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Contact2');"><span id="spnContact2" class="expand">Contacts</span></a></li>
						</ul>
					</div>
					<div style="margin-top:5px;">
						<div id="Etablissement">
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Etablissements d'enseignement</span></div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="resultDetailPanel">
										<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
										<colgroup>
											<col width="*" />
										</colgroup>
										<?php
											echo '<tr><td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Nom</td></tr>';
											
											$EtabActionArray = array();
											$etabs = $action->GetActionEtablissment();
											for($i=0; $i<$etabs->Count(); $i++)
											{
												array_push($EtabActionArray, $etabs->items($i)->GetEtablissementId());
												echo '<tr class="trResult">';
												echo '<td class="resultLine resultLineRedirection" onclick="ShowEtablissementInfo(\''.$etabs->items($i)->GetEtablissementId().'\');">'.$etabs->items($i)->GetNom().'</td>';
												echo '</tr>';												
											}
										?>
									</table>
								</td>
							</tr>
							</table>
							</div>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>
						<div id="Contact">	
						<script language="javascript" type="text/javascript">
							function ShowContactInfo(contactId)
							{
								parent.document.location.href = '<?php echo $this->BuildURL('contactDetail') . '&id='; ?>' + contactId; 
								if(parent.panel_doublon)parent.panel_doublon.hide();
							}
	
							function ShowEtablissementInfo(id)
							{
								window.location = '<?php echo $this->BuildURL('etablissementDetail') . '&id='; ?>' + id;
							}
						</script>
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Contacts</span></div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="resultDetailPanel">
										<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
										<colgroup>
											<col width="100" />
											<col width="150" />
											<col width="*" />
										</colgroup>
										<?php
											$userLog = Session::GetInstance()->getCurrentUser();
																				
											echo '<tr>
													<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">R�le</td>
													<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Nom - Pr�nom</td>
													<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">�tablissements (Nom - Titre - Sp�cialit�';
											
											if(!$userLog->isOperateur())
												echo ' - Participation';
												
											   echo ')</td>
												  </tr>';
											   
											$actionContact = $action->GetActionContact();
											for($i=0; $i < $actionContact->Count(); $i++)
											{
												$contact = $actionContact->items($i)->GetContact();
												$contactEtablissement = $contact->GetEtablissementsContacts();
												
							  					$class='class="trResult"';
												echo'<tr '.$class.' colspan="2">';
												echo'<td class="resultLine">'.$actionContact->items($i)->GetRoleContactAction().'</td>';
												echo'<td class="resultLine resultLineRedirection" onclick="ShowContactInfo(\''.$contact->GetContactId().'\');">'. $contact->GetNom().' '.$contact->GetPrenom().'</td>';
												echo '<td class="resultLine" >';
												for($x=0; $x < $contactEtablissement->Count(); $x++)
												{
													$find = false;
													foreach ($EtabActionArray as $et)
													{
														if($et==$contactEtablissement->items($x)->GetEtablissementId())
														{
															$find=true;
															break;
														}
													}
													
													if($find)
													{
														echo '<span class="resultLineRedirection" onclick="javascript:ShowEtablissementInfo(\'' . $contactEtablissement->items($x)->GetEtablissementId() . '\');">'.$contactEtablissement->items($x)->GetEtablissement()->GetNom().'</span>';
														echo ' - ';
														
														$titre = Dictionnaries::getTitreContactList($contactEtablissement->items($x)->GetTitreContactId());
														if(isset($titre) && $titre->count()>0)
															echo $titre->items(0)->GetLabel();
														echo ' - ';
														$specialite = Dictionnaries::getSpecialiteList($contactEtablissement->items($x)->GetSpecialiteContactId());
														if(isset($specialite) && $specialite->count()>0)
															echo $specialite->items(0)->GetLabel();
														if(!$userLog->isOperateur())
														{
															echo ' - ';													
															$participation = Dictionnaries::getParticipationList($contactEtablissement->items($x)->GetParticipationId());
															if(isset($participation) && $participation->count()>0)
																echo $participation->items(0)->GetLabel();
														}
															echo '<br/>';
													}
												}		
												echo'</td>
												</tr>';
										}
										?>
									</table>
								</td>
							</tr>
							</table>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>	
					</div>
				  </div>
				</td>
			</tr>
		  </table>
		</div>

		<script language="javascript" type="text/javascript">
			document.getElementById("spnAll").style.backgroundPosition = "100% -250px";
			document.getElementById("spnAll").style.color = "#FFFFFF";
			document.getElementById("aAll").style.backgroundPosition = "0% -250px";
		</script>
		
		<?php if(($this->user->isAdmin() || 
					$this->user->isAgentCoordinateur() ||
					$this->user->isCoordinateur())||
					((!$action->isValid()) && $action->checkAnneeEncodage() && (!$this->user->isOperateur() || ($this->user->isOperateur() && $action->GetCreatedBy() == $this->user->getUtilisateurId())))){?>
		<div class="boxBtn">
			<table class="buttonTable">
				<tr>
					<td onclick="updateAction('<?php echo $action->GetActionId(); ?>')">Modifier</td>
					<td class="separator"></td>
					<td onclick="deleteAction('<?php echo $action->GetActionId(); ?>')">Supprimer</td>
					<td class="separator"></td>
				</tr>
			</table>
		</div>
		<?php }?>
	</div>
<?php
	}
}
?>
