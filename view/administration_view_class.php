<?php
header('Content-Type: text/html; charset=ISO-8859-1');
/* Create By Marucci 24/12/2013 Happy christmaes ! */ 
REQUIRE_ONCE (SCRIPTPATH . 'lib/util.php');
REQUIRE_ONCE (SCRIPTPATH . 'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/utilisateur_domain_class.php');


class AdmninistrationView extends BaseView {
	public function viewUsersSearch($users, $begin = 0, $offset = 15, $nbResults = 0){
		$this->viewUsersSearchFilter();
		$this->viewUsersSearchTable($users, $begin, $offset, $nbResults);
	}
	
	public function viewUsersSearchTable($users, $begin = 0, $offset = 15, $nbResults = 0){
		echo '<form action="" method="POST" id="form_table_users">';
		echo '<table class="table grid panel result" id="table_users">';
		echo '	<thead>
					<tr>
						<td class="resultHeader">Id</td>
						<td class="resultHeader">Login</td>
						<td class="resultHeader">Type</td>
						<td class="resultHeader">Email</td>
						<td class="resultHeader">Nom</td>
						<td class="resultHeader">Pr�nom</td>
						<td class="resultHeader">T�l�phone</td>
						<td class="resultHeader">swActif</td>
						<td class="resultHeader">Op�rateur</td>
					</tr>
				</thead><tbody>';
		foreach ($users as $user) {
			echo '<tr class="trResult">';
			echo '<td class="resultLine">'.$user->getUtilisateurId().'</td>';
			echo '<td class="resultLine"><a href="index.php?module=administration&amp;action=view&amp;id='.$user->getUtilisateurId().'">'.$user->getLogin().'</a></td>';
			echo '<td class="resultLine">'.$user->getTypeUtilisateurLabel().'</td>';
			echo '<td class="resultLine">'.$user->getEmail().'</td>';
			echo '<td class="resultLine">'.$user->getNom().'</td>';
			echo '<td class="resultLine">'.$user->getPrenom().'</td>';
			echo '<td class="resultLine">'.$user->getTel().'</td>';
			echo '<td class="resultLine">'.$user->getSwActif().'</td>';
			echo '<td class="resultLine">'.$user->getOperateurName().'</td>';
			echo '</tr>';
		}
		echo '</tbody>';
		$this -> renderPaginator($begin, $offset, $nbResults, 13); 
		echo '</table>';
		echo '</form>';
	}

public function viewAddUser($errors = "",$request =""){
		$typesUtilisateurs = Dictionnaries::getUtilisateurType();
		$operatorslists = Dictionnaries::getOperateurList();
		
		$request = $request=="user_add" ? "addsaveuser" : "saveuser"; 
		
		$nom 				= isset($_REQUEST['nom'])?$_REQUEST['nom']:NULL;
		$prenom				= isset($_REQUEST['prenom'])?$_REQUEST['prenom']:NULL;
		$tel				= isset($_REQUEST['tel'])?$_REQUEST['tel']:NULL;
		$email				= isset($_REQUEST['email'])?$_REQUEST['email']:NULL;
		$login				= isset($_REQUEST['login'])?$_REQUEST['login']:NULL;
		$typeUtilisateur	= isset($_REQUEST['typeUtilisateur'])?$_REQUEST['typeUtilisateur']:NULL;
		$operatorslist		= isset($_REQUEST['operatorslist'])?$_REQUEST['operatorslist']:NULL;

		echo $this->Errors($errors);
		echo '<form action="index.php?module=administration&action=addsaveuser" method="POST">';
		echo '<table class="grid large" style="margin-left:0;" id="searchFormUserWrapper"><tbody>';
		echo '<tbody>';
		echo '<tr>';
		echo '<td class="title">Nom</td>';
		echo '<td><input type="text" name="nom" id="nom" value="'.$nom.'" /></td>';
		echo '<td class="title">Pr�nom</td>';
		echo '<td><input type="text" name="prenom" id="prenom" value="'.$prenom.'"  /></td>';
		echo '<td class="title">T�l�phone</td>';
		echo '<td><input type="text" name="tel" id="tel" value="'.$tel.'" /></td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td class="title">Email</td>';
		echo '<td><input type="text" name="email"  id="email" value="'.$email.'"  size="30" style="display: inline;"/><span style="color: red;">*</span></td>';
		echo '<td class="title">Login</td>';
		echo '<td><input type="text" name="login" id="login" size="30" value="'.$login.'" style="display: inline;"/><span style="color: red;">*</span></td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td class="title">Type</td><td>';
		echo createSelect('typeUtilisateur', $typesUtilisateurs, $typeUtilisateur, 'label').'<span style="color: red;">*</span>'; 
		echo '</td>';
		echo '<td class="title">Op�rateur</td><td>';
		echo createSelect('operatorslist', $operatorslists, $operatorslist, 'label'); 
		echo '</td>';
		echo '</tr>';
		echo '</tbody></table>';
		echo '<div><input type="button" value="Retour" onClick="history.go(-1);return true;" class="button"/>';
		echo '<input type="submit" id="submit_button" name="submit_button" value="Enregistrer" class="button"/></div>';
		echo '</form>';
	}

	public function viewOneUser($user, $errors = ""){
		$typesUtilisateurs = Dictionnaries::getUtilisateurType();
		$operatorslists = Dictionnaries::getOperateurList();
		$checked = $user->getSwActif()==1?'checked':null;
		
		$nom 				= isset($_REQUEST['nom'])?$_REQUEST['nom']:$user->getNomReel();
		$prenom				= isset($_REQUEST['prenom'])?$_REQUEST['prenom']:$user->getPrenom();
		$tel				= isset($_REQUEST['tel'])?$_REQUEST['tel']:$user->getTel();
		$email				= isset($_REQUEST['email'])?$_REQUEST['email']:$user->getEmail();
		$login				= isset($_REQUEST['login'])?$_REQUEST['login']:$user->getLogin();
		$typeUtilisateur	= isset($_REQUEST['typeUtilisateur'])?$_REQUEST['typeUtilisateur']:$user->getTypeUtilisateurId();
		$operatorslist		= isset($_REQUEST['operatorslist'])?$_REQUEST['operatorslist']:$user->getOperateurId();
		
		echo $this->Errors($errors);
		echo '<form action="index.php?module=administration&action=saveuser&id='.$user->getUtilisateurId().'" method="POST">';
		echo '<table class="grid large" style="margin-left:0;" id="searchFormUserWrapper"><tbody>';
		echo '<tbody>';
		echo '<tr>';
		echo '<td class="title">Nom</td>';
		echo '<td><input type="hidden" name="id" id="id" value="'.$user->getUtilisateurId().'" /><input type="text" name="nom" id="nom" value="'.$nom.'" /></td>';
		echo '<td class="title">Pr�nom</td>';
		echo '<td><input type="text" name="prenom" id="prenom" value="'.$prenom.'" /></td>';
		echo '<td class="title">T�l�phone</td>';
		echo '<td><input type="text" name="tel" id="tel" value="'.$tel.'" /></td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td class="title">Email</td>';
		echo '<td><input type="text" name="email"  id="email" size="40" value="'.$email.'" /></td>';
		echo '<td class="title">Login</td>';
		echo '<td><input type="text" name="login" id="login" size="40" value="'.$login.'" /></td>';
		echo '<td class="title">Actif</td>';
		echo '<td><input type="checkbox" name="actif" id="actif" '.$checked.' /></td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td class="title">Type</td><td>';
		echo createSelect('typeUtilisateur', $typesUtilisateurs, $typeUtilisateur, 'label'); 
		echo '</td>';
		echo '<td class="title">Op�rateur</td><td>';
		echo createSelect('operatorslist', $operatorslists, $operatorslist, 'label'); 
		echo '</td>';
		echo '</tr>';
		echo '</tbody></table>';
		echo '<div><input type="button" value="Retour" onClick="history.go(-1);return true;" class="button"/>';
		//echo '<input type="button" id="delete_user" name="delete_user" value="Supprimer" class="' . $this -> renderClass("button") . '"/>';
		echo '<input type="submit" id="submit_button" name="submit_button" value="Enregistrer" class="button"/></div>';
		echo '</form>';
	}

	public function viewUsersSearchFilter(){
		$typesUtilisateurs = Dictionnaries::getUtilisateurType();
		$operatorslist = Dictionnaries::getOperateurList();
		
		?>
			<form id="SearchUsers" action="" method="POST">
				<table class="table grid panel" style="margin-left:0;" id="searchFormUserWrapper">
					<tbody>
						<tr>
	                        <td class="title">Nom</td>
	                        <td><input type="text" id="nom" name="nom" value="<?php echo isset($_REQUEST['nom'])?$_REQUEST['nom']:null; ?>" /></td>
							<td class="title">Pr�nom</td>
							<td><input type="text" id="prenom" name ="prenom" value="<?php echo isset($_REQUEST['prenom'])?$_REQUEST['prenom']:null; ?>" /></td>
						</tr>
						<tr>
	                        <td class="title">Email</td>
	                        <td><input type="text" id="email" name="email" value="<?php echo isset($_REQUEST['email'])?$_REQUEST['email']:null; ?>" /></td>
							<td class="title">Login</td>
							<td><input type="text" id="login" name="login" value="<?php echo isset($_REQUEST['login'])?$_REQUEST['login']:null; ?>" /></td>
						</tr>
						<tr>
							<td class="title">Types utilisateurs</td>
							<td><?php echo createSelect('typeUtilisateur', $typesUtilisateurs, isset($_REQUEST['typeUtilisateur'])?$_REQUEST['typeUtilisateur']:null, 'label'); ?></td>
						</tr>
					</tbody>
				</table>
				<table class="buttonTable">
					<tr>
						<td onclick="submitForm('SearchUsers')">Rechercher</td>
						<td onclick="clearForm('SearchUsers')">Effacer</td>
					</tr>
				</table>
			</form>
	<?php
	}

}
?>
