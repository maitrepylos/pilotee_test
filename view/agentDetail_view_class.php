<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class AgentDetailView extends BaseView
{
	private $user = null;
	
	public function Render($agent)
	{
		$this->user = Session::GetInstance()->getCurrentUser();
		$etabs = $agent->getAgentEtablissement();
?>
	<div class="boxGen">
	<script language="javascript" type="text/javascript">
		function ShowEtablissementInfo(id)
		{
			window.location = '<?php echo $this->BuildURL('etablissementDetail') . '&id='; ?>' + id;
		}
	</script>
		<div class="boxPan" style="padding:5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Nom :</td>
						<td class="detail"><?php echo $agent->GetNom();?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Téléphone(s) :</td>
						<td class="detail">
							<?php
								echo $agent->getTel();
							?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Nb d'établissements attribués :</td>
						<td class="detail">
							<?php
								echo $etabs->Count();
							?>
						</td>
					</tr>
					
  				 </table>
				</td>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Prénom :</td>
						<td class="detail"><?php echo $agent->GetPrenom(); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Courriel :</td>
						<td class="detail">
							<?php
								echo $agent->GetEmailAsAnchor();
							?>
						</td>
					</tr>			
  				  </table>
				</td>
			</tr>
			<?php if($etabs->Count()>0){ ?>
			<tr>
				<td colspan="2">
					<div style="margin-top:15px; width:100%;">
						<div id="Global" style="width:100%;">
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Établissements</span></div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="resultDetailPanel">
											<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
												<colgroup>
													<col width="300" />
													<col width="150" />
													<col width="150" />
													<col width="150" />
													<col width="*" />
												</colgroup>
										<?php
										
											echo '<tr>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Nom</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Niveau</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">CP</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Ville</td>';
											echo '</tr>';
											
												for ($i = 0; $i < $etabs->Count(); $i++)
												{
													$adr = $etabs->items($i)->GetAdresse();
													echo '<tr><td colspan="9" style="font-size:1px;height:2px;"></td></tr>';
													echo '<tr class="trResult">';
														echo sprintf('<td onclick="javascript:ShowEtablissementInfo(\'' . $etabs->items($i)->GetEtablissementId() . '\');" class="resultLine resultLineRedirection">%s</td>', $etabs->items($i)->getNom());
														echo sprintf('<td class="resultLine">%s</td>', $etabs->items($i)->GetNiveauEtablissement());
														echo sprintf('<td class="resultLine">%s</td>', ($adr->GetCodepostal()!=null?$adr->GetCodepostal()->GetCodepostal():''));
														echo sprintf('<td class="resultLine">%s</td>', $adr->GetVille());
													echo '</tr>';
												} 
										?>
									</table>
								</td>
							</tr>
							</table>
							</div>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>	
					</div>
				</td>
			</tr>
			<?php } ?>
		  </table>
		</div>
	</div>
<?php
	}
}
?>
