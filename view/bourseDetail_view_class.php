<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class BourseDetailView extends BaseView
{
	private $user = null;
	
	public function Render($action)
	{
	 $userLog = Session::GetInstance()->getCurrentUser();
?>
	<script language="javascript" type="text/javascript">
	function ShowFormAddContact()
	{
		var ets = new Array();
		var id = document.getElementById("chk_bourse_id").value;
		var nom = document.getElementById("chk_bourse_name").value;
		ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
		
		var frm = document.getElementById("frmFormAddContact");		
		Popup.showModal('formAddContact');
		frmContent = frm.contentWindow.document || frm.contentDocument;
		if(ets.length>0)
		{
			SetBourseContact(ets, frmContent);
		}
		else
		{
			frmContent.getElementById('divBourseContact').innerHTML = '';
		}
	
	}
	function SetBourseContact(objectsArray, frmContent)
	{
		  if (objectsArray.length > 0)		  
		  {
		  	var nbLignes = 0;
			var divGlobal = frmContent.getElementById('divBourseContact');			
			var tab = frmContent.getElementById('tableBoursesContact');
			
			
			if(tab==null)
			{
 				tab = divGlobal.appendChild(frmContent.createElement('table'));
 				tab.setAttribute('id','tableBoursesContact');
 				tab.setAttribute('style','width:80%');
 				frmContent.getElementById('bourseIds').value = '';
 				frmContent.getElementById("nbLignesBourse").value = '0';
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute('id','headBourse_-1');
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>Projet entrepreneurial</center>";
 				newCell = newRow.insertCell(1);
 				newCell.innerHTML = "<center>R�le</center>";
 				newCell = newRow.insertCell(2);
 				newCell.innerHTML = "";
 			}
 			else
 			{
 				nbLignes = frmContent.getElementById("nbLignesBourse").value;
 			}	

			autoid = nbLignes;
			
		 	for(j=0;j<objectsArray.length;j++)
 			{
				var actionSelect = frmContent.getElementById("bourseIds");
				var reg=new RegExp("[;]+", "g");
				tableActionSelect = actionSelect.value.split(reg);
				var isPresent = false;					
				for(var x=0; x<tableActionSelect.length; x++)
				{
					if(tableActionSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				
				if(!isPresent)
				{
					actionSelect.value += (actionSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);
	 				newRow.setAttribute('id','bourse_'+nbLignes);

					 for(i=0;i<5;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
							case 0 :
								
							    tmp = '<input type="text" name="bourseName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />';
							    tmp += "<input name='bourseId_"+nbLignes+"' id='bourseId_"+nbLignes+"' type='hidden' value='"+objectsArray[j].id+"' />";
								newCell.innerHTML = tmp;
 								break;
 						
							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>";
								newCell.innerHTML = "<select name='roleBourse_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
 							case 2 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"bourse_"+nbLignes+"\",\"nbLignesBourse\")'/>";
 								break; 							
 								
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }
 			  }
		 	frmContent.getElementById("nbLignesBourse").value = nbLignes;
 			  
		   }		
	}
	function updateAction(actionId)
	{
		parent.document.location.href = '<?php echo $this->BuildURL('bourse') .'&action=update&updateId='; ?>' + actionId;	
	}
	
	function deleteAction(actionId)
	{
		if (sure("Supprimer l'action ?") == true)
			parent.document.location.href = '<?php echo $this->BuildURL('bourse') .'&action=delete&deleteId='; ?>' + actionId;
	}

	function ShowFormAddBourse()
	{
		var frm = document.getElementById("frmFormAddBourse");
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=boursePopup&action=add'; ?>';		
		Popup.showModal('formAddBourse');
	}
	
	function ShowHistoriqueDetail()
	{
		var frm = document.getElementById("frmHistoriqueDetail");
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=historiquePopup&id='.$action->GetActionId().'&objet=action';?>';			
		Popup.showModal('HistoriqueDetail');
	}
	</script>
	
	<div id="HistoriqueDetail" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Historique : <?php echo $action->GetNom(); ?></td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('HistoriqueDetail');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmHistoriqueDetail" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	
	<div id="formAddContact" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er un contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddContact');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddContact" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div id="formAddBourse" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er un projet entrepreneurial</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddBourse');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd">
				<iframe id="frmFormAddBourse" frameborder=0 src="" style="width: 1024px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	<div class="boxGen">
		<input type="hidden" id="chk_bourse_id" value="<?php echo $action->GetActionId();?>">
		<input type="hidden" id="chk_bourse_name" value="<?php echo $action->GetNom();?>">
		
		<div class="boxPan" style="padding:5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Ann�e scolaire :</td>
						<td class="detail"><?php echo ToHTML($action->GetAnneeId());?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Action :</td>
						<td class="detail"><?php echo ToHTML($action->getActionLabel());?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr style="display:none">
						<td class="label">Global :</td>
						<td class="detail" style="color:<?php echo $action->GetSwGlobalColor(); ?>"><?php echo $action->GetSwGlobalLabel(); ?></td>
					</tr>

					<tr><td colspan="2" class="separator"></td></tr>					
					<tr>
						<td class="label">Cat�gorie de bourse :</td>
						<td class="detail"> <?php echo ToHTML($action->GetCategorieBourse()); ?></td>
					</tr>	
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Montant demand� :</td>
						<td class="detail" ><?php echo $action->GetMontantDemande();?></td>
					</tr>
					
					<?php if($action->GetStatutSubventionId()==SUBVENTION_OBTENU){
					 if($action->GetNbApprenants()!=0) 
						$montant  = round($action->GetMontantSubvention()/$action->GetNbApprenants(), 2);
					 else
					 	$montant = 0;
					?>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Montant par apprenant :</td>
						<td class="detail" ><?php echo $montant ?></td>
					</tr>
					<?php } ?>					 								 
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Groupe de classes concern�s :</td>
						<td class="detail" ><?php echo $action->GetGroupeClasseConcernee(); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					
					<tr>
						<td class="label">Nombre d'apprenants :</td>
						<td class="detail"><?php echo $action->GetNbApprenants(); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Nombre d'enseignants :</td>
						<td class="detail"><?php echo $action->GetNbEnseignants(); ?></td>
					</tr>
					
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Niveau/degr�/fili�re :</td>
						<td class="detail">
						<?php
							$filieres = $action->getFiliereDegreNiveau();
							for($i=0; $i<$filieres->Count(); $i++)
								echo $filieres->items($i)->GetActionFiliereDegreNiveau().' '.'('.$filieres->items($i)->GetNbApprenants().') <br/>';
						?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">P�dagogique :</td>
						<td class="detail">
						<?php
							$peda = $action->GetFinalitePedagog();
							for($i=0; $i<$peda->Count(); $i++)
								echo $peda->items($i)->GetFinalitePedagog().' '.'('.$peda->items($i)->GetNbApprenants().') <br/>';
						?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Section :</td>
						<td class="detail">
						<?php
							$sections = $action->GetSections();
								for($i=0; $i<$sections->Count(); $i++)
									echo $sections->items($i)->GetActionSection().' '.'('.$sections->items($i)->GetNbApprenants().') <br/>';
						?>
						</td>
					</tr>
  				 </table>
				</td>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Date :</td>
						<td class="detail"><?php echo $action->GetDateActionString(false); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					
					<tr>
						<td class="label">Encodage :</td>
						<?php
							echo'<td class="detail">'.ToHTML($action->GetStatutAction()).'</td>';
						?>
					</tr>
					
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Subvention :</td>
						<td class="detail">
						<?php
						 echo $action->GetStatutSubvention();
						?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">R�f�rence chez l'ASE :</td>
						<td class="detail"><?php echo ToHTML($action->GetRefChezASE()); ?></td>
					</tr>
					<?php if($action->GetStatutSubventionId()==SUBVENTION_OBTENU){ ?>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Montant subventionn� :</td>
						<td class="detail"><?php echo $action->GetMontantSubvention(); ?></td>
					</tr>		
					<?php } ?>			
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Projet r�alis� :</td>
						<td class="detail"><?php echo ToHTML($action->GetProjetRealise()); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Collaboration inter �tablissement :</td>
						<td class="detail"><?php echo ToHTML($action->GetCollaborationInterEtab()); ?></td>
					</tr>					 
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Site internet :</td>
						<td class="detail"><?php echo $action->GetWebsiteAsAnchor();?></td>
					</tr>
					
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Commentaire :</td>
						<td class="detail"><?php echo ToHTML($action->GetCommentaire());?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Cr�ation :</td>
						<td class="detail">
						<?php
							$user  = $action->GetCreatedByUser();
							
							if($user->getTypeUtilisateurId() != 'AGE')	
								echo 'le '.$action->GetDateCreatedString(false).' par '. $user->GetNom().' '.$user->GetPrenom();
							else
								echo 'le '.$action->GetDateUpdatedString(false).' par <a href=\''.$this->BuildURL('agentDetail').'&id='.$user->getUtilisateurId().'\'>'.$user->GetNom().' '.$user->GetPrenom().'</a>';

						?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Modification :</td>
						<td class="detail">
						<?php
							$user  = $action->GetUpdatedByUser();
							if($user->getTypeUtilisateurId() !='AGE')	
								echo 'le '.$action->GetDateUpdatedString(false).' par '. $user->GetNom().' '.$user->GetPrenom();
							else
								echo 'le '.$action->GetDateUpdatedString(false).' par <a href=\''.$this->BuildURL('agentDetail').'&id='.$user->getUtilisateurId().'\'>'.$user->GetNom().' '.$user->GetPrenom().'</a>';
						?>
						</td>
					</tr>					
  				  </table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				
				<script>
					
					function ActionDetailViewClass() 
					{
						this.current = 'All2';
						this.tabs = new Array('All2', 'Etablissement2', 'Contact2', 'Commentaire2');
					}

					ActionDetailViewClass.prototype.onMouseOver = function(liID)
					{
						document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
						document.getElementById("spn" + liID).style.color = "#FFFFFF";
						document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
					}

					ActionDetailViewClass.prototype.onMouseOut = function(liID)
					{
						if (this.current != liID)
						{
							document.getElementById("spn" + liID).style.backgroundPosition = "100% 0%";
							document.getElementById("spn" + liID).style.color = "#737373";
							document.getElementById("a" + liID).style.backgroundPosition = "0% 0%";
						}
					}
				
					ActionDetailViewClass.prototype.onLIClick = function(liID)
					{
						
						this.current = liID;
						
						for (var i = 0; i < this.tabs.length; i++)
						{
							document.getElementById("spn" + this.tabs[i]).style.backgroundPosition = "100% 0%";
							document.getElementById("spn" + this.tabs[i]) .style.color = "#737373";
							document.getElementById("a" + this.tabs[i]).style.backgroundPosition = "0% 0%";
						}
					
						document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
						document.getElementById("spn" + liID).style.color = "#FFFFFF";
						document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
					
						var contact = document.getElementById('Contact2');
						var etablissement = document.getElementById('Etablissement2');
						var commentaire = document.getElementById('Commentaire2');
						
					
						if (liID == 'All2')
						{
							if (contact) contact.style.display = '';
							if (etablissement) etablissement.style.display = '';
							if(commentaire) commentaire.style.display = '';
						}
						else if (liID == 'Contact2')
						{
							if (contact) contact.style.display = '';
							if (etablissement) etablissement.style.display = 'none';
							if(commentaire) commentaire.style.display = 'none';
						}
						else if (liID == 'Etablissement2')
						{
							if (etablissement) etablissement.style.display = '';
							if (contact) contact.style.display = 'none';
							if(commentaire) commentaire.style.display = 'none';
						}
						else if (liID == 'Commentaire2')
						{
							if (etablissement) etablissement.style.display = 'none';
							if (contact) contact.style.display = 'none';
							if(commentaire) commentaire.style.display = '';
						}
					}
					window.edvc = new ActionDetailViewClass();
				
						function ShowEtablissementInfo(id)
						{
							window.location = '<?php echo $this->BuildURL('etablissementDetail') . '&id='; ?>' + id;
						}
						
						function CommenterBourse(bourseId, AgentId)
						{
							parent.document.location.href = '<?php echo $this->BuildURL('commenterBourse') .'&bourseId='; ?>' + bourseId + '&agentId='+AgentId; 
						}
				</script>
					<div class="header" style="border-bottom:3px solid #EAC3C3;">
						<ul>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('All2');" OnMouseOut="javascript:window.edvc.onMouseOut('All2');"><a id="aAll2" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('All2');"><span id="spnAll2" class="expand">Tout</span></a></li>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('Etablissement2');" OnMouseOut="javascript:window.edvc.onMouseOut('Etablissement2');"><a id="aEtablissement2" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Etablissement2');"><span id="spnEtablissement2" class="expand">Etablissements d'enseignement</span></a></li>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('Contact2');" OnMouseOut="javascript:window.edvc.onMouseOut('Contact2');"><a id="aContact2" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Contact2');"><span id="spnContact2" class="expand">Contacts</span></a></li>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('Commentaire2');" OnMouseOut="javascript:window.edvc.onMouseOut('Commentaire2');"><a id="aCommentaire2" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Commentaire2');"><span id="spnCommentaire2" class="expand">Commentaires</span></a></li>
						</ul>
					</div>
					
					<div style="margin-top:5px;">
						<div id="Etablissement2">
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Etablissements d'enseignement</span></div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="resultDetailPanel">
										<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
										<colgroup>
											<col width="*" />
										</colgroup>
										<?php
											echo '<tr><td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Nom</td></tr>';
											$EtabActionArray = array();
											$etabs = $action->GetActionEtablissment();
											for($i=0; $i<$etabs->Count(); $i++)
											{
												array_push($EtabActionArray, $etabs->items($i)->GetEtablissementId());
												echo '<tr class="trResult">';
												echo '<td class="resultLine resultLineRedirection" onclick="ShowEtablissementInfo(\''.$etabs->items($i)->GetEtablissementId().'\');">'.$etabs->items($i)->GetNom().'</td>';
												echo '</tr>';												
											}
										?>
									</table>
								</td>
							</tr>
							</table>
							</div>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>
						<div id="Contact2">	
						<script language="javascript" type="text/javascript">
							function ShowContactInfo(contactId)
							{
								parent.document.location.href = '<?php echo $this->BuildURL('contactDetail') . '&id='; ?>' + contactId; 
								parent.panel_doublon.hide();
							}
	
							function ShowEtablissementInfo(id)
							{
								window.location = '<?php echo $this->BuildURL('etablissementDetail') . '&id='; ?>' + id;
							}
						</script>
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Contacts</span></div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="resultDetailPanel">
										<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
										<colgroup>
											<col width="100" />
											<col width="150" />
											<col width="*" />
										</colgroup>
										<?php
											$userLog = Session::GetInstance()->getCurrentUser();
																				
											echo '<tr>
													<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">R�le</td>
													<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Nom - Pr�nom</td>
													<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">�tablissements (Nom - Titre - Sp�cialit�';
											
											if(!$userLog->isOperateur())
												echo ' - Participation';
												
											   echo ')</td>
												  </tr>';
											$actionContact = $action->GetActionContact();
											
											for($i=0; $i < $actionContact->Count(); $i++)
											{
												$contact = $actionContact->items($i)->GetContact();
												
												if($contact)
												{
													$contactEtablissement = $contact->GetEtablissementsContacts();
												
							  						$class='class="trResult"';
													echo'<tr '.$class.' colspan="2">';
													echo'<td class="resultLine">'.$actionContact->items($i)->GetRoleContactAction().'</td>';
													echo'<td class="resultLine resultLineRedirection" onclick="ShowContactInfo(\''.$contact->GetContactId().'\');">'. $contact->GetNom().' '.$contact->GetPrenom().'</td>';
													echo '<td class="resultLine" >';
													for($x=0; $x < $contactEtablissement->Count(); $x++)
													{
														$find = false;
														foreach ($EtabActionArray as $et)
														{
															if($et==$contactEtablissement->items($x)->GetEtablissementId())
															{
																$find=true;
																break;
															}
														}
													
														if($find)
														{
															echo '<span class="resultLineRedirection" onclick="javascript:ShowEtablissementInfo(\'' . $contactEtablissement->items($x)->GetEtablissementId() . '\');">'.$contactEtablissement->items($x)->GetEtablissement()->GetNom().'</span>';
															echo ' - ';
														
															$titre = Dictionnaries::getTitreContactList($contactEtablissement->items($x)->GetTitreContactId());
															if($titre->count()>0)
																echo $titre->items(0)->GetLabel();
															else
																echo '/';
															echo ' - ';
															$specialite = Dictionnaries::getSpecialiteList($contactEtablissement->items($x)->GetSpecialiteContactId());
															if($specialite->count()>0)
															echo $specialite->items(0)->GetLabel();
															else echo '/';
															if(!$userLog->isOperateur())
															{
																echo ' - ';													
																$participation = Dictionnaries::getParticipationList($contactEtablissement->items($x)->GetParticipationId());
																if($participation->count()>0)
																echo $participation->items(0)->GetLabel();
																else
																echo '/';
															}
															echo '<br/>';
														}
													
													}		
													echo'</td>
													</tr>';
										  	}
										}
										?>
									</table>
								</td>
							</tr>
							</table>
							</div>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>	
						<div id="Commentaire2">
						<script>
						function ShowAgentInfo(id)
						{
							window.location = '<?php echo $this->BuildURL('agentDetail') . '&id='; ?>' + id;
						}
						</script>
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Commentaires</span></div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="resultDetailPanel">
										<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
										<colgroup>
											<col width="150" />
											<col width="200" />
											<col width="*" />
										</colgroup>
										<?php
										/*
											echo '<tr><td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Agent</td>';
											echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Date d\'�valuation</td>';
											echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Commentaire</td></tr>';
										*/
											$bourseAgt = $action->GetBourseAgent();
											$commentaireOk = true;
											for($i=0; $i<$bourseAgt->Count(); $i++)
											{
												$agt = $bourseAgt->items($i)->GetUtilisateur();
												if($userLog->getUtilisateurId()==$agt->getUtilisateurId())
													$commentaireOk = false;
												
												echo '<tr class="trResult">';
												echo '<td style="width:100%">';
												echo '<table><tr><td class="resultLine resultLineRedirection" onclick="ShowAgentInfo(\''.$agt->getUtilisateurId().'\');" align="left">'.$agt->GetNom().' '.$agt->GetPrenom().'</td>';
												echo '<tr><td align="left" style="padding-left:25px">Date :</td><td> '. $bourseAgt->items($i)->GetDateEvaluationString(true).'</td></tr>';
												echo '<tr><td align="left" style="padding-left:25px">Synth�se de l\'avis :</td><td> '. ToHTML($bourseAgt->items($i)->getSyntheseAvisLabel()).'</td></tr>';
												echo '<tr><td align="left" style="padding-left:25px">Avis qualitatif :</td><td> '. ToHTML($bourseAgt->items($i)->getAvisQualitatif()).'</td></tr>';
												echo '<tr><td align="left" style="padding-left:25px">El�ments importants :</td><td> '. ToHTML($bourseAgt->items($i)->getEltImportant()).'</td></tr>';
												echo '<tr><td align="left" style="padding-left:25px">Soutien financier :</td><td> '. $bourseAgt->items($i)->getSoutientFiancier().'</td></tr>';
												echo '</tr></table></td>';
												
												echo '</tr>';												
											}
										?>
									</table>
								</td>
							</tr>
							</table>
							</div>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>						
					</div>
				</td>
			</tr>
		  </table>
		</div>
		<script language="javascript" type="text/javascript">
			document.getElementById("spnAll2").style.backgroundPosition = "100% -250px";
			document.getElementById("spnAll2").style.color = "#FFFFFF";
			document.getElementById("aAll2").style.backgroundPosition = "0% -250px";
		</script>							
		<div class="boxBtn">
			<table class="buttonTable">
				<tr>
					<?php if($userLog->isAdmin() || $userLog->isCoordinateur() || $userLog->isAgentCoordinateur()){ ?>
					<td onclick="updateAction('<?php echo $action->GetActionId(); ?>')">Modifier</td>
					<td class="separator"></td>
					<td onclick="deleteAction('<?php echo $action->GetActionId(); ?>')">Supprimer</td>
					<td class="separator"></td>
					<?php }?>
					<?php if(($userLog->isAgent() || $userLog->isAgentCoordinateur()) && $commentaireOk && $action->GetStatutSubventionId() == SUBVENTION_EVALUATION && $action->GetStatutActionId()== 'VALID'){ ?>
					<td onclick="CommenterBourse('<?php echo $action->GetActionId();?>', '<?php echo $userLog->getUtilisateurId();?>')">Commenter</td>
					<td class="separator"></td>
					<?php }?>
				</tr>
			</table>
		</div>
	</div>
<?php
	}
}
?>
