<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');
class BourseView extends BaseView
{
	public function Search($operateur = false, $closeCallBack)
	{
	?>
	<script type="text/javascript">
		function ShowEtablissement(closeCallBack)
		{
			var frm = document.getElementById("frmEtablissements");
			url = '<?php echo SCRIPTPATH . 'index.php?module=etablissementPopup'; ?>';
			frm.src = url + closeCallBack;
			Popup.showModal('etablissements');
		}
		
		function ShowFormAddContact()
		{
			var ets = new Array();
			var chks = document.getElementsByTagName("input");
			for (var i = 0; i < chks.length; i++)
			{
				if (chks[i].name.match("^chk_bourse_"))
				{
					if (chks[i].checked)
					{
						var id = chks[i].name.replace("chk_bourse_", "");
						var nom = document.getElementById("chk_bourse_name_" + id).innerHTML;
						ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
					}
				}
			}
			var frm = document.getElementById("frmFormAddContact");
			Popup.showModal('formAddContact');
			frmContent = frm.contentWindow.document || frm.contentDocument;
			if(ets.length>0)
			{
				SetBourseContact(ets, frmContent);
			}
			else
			{
				frmContent.getElementById('divBourseContact').innerHTML = '';
			}
		}

		function SetBourseContact(objectsArray, frmContent)
		{
			  if (objectsArray.length > 0)		  
			  {
			  	var nbLignes = 0;
				var divGlobal = frmContent.getElementById('divBourseContact');			
				var tab = frmContent.getElementById('tableBoursesContact');
				
				
				if(tab==null)
				{
	 				tab = divGlobal.appendChild(frmContent.createElement('table'));
	 				tab.setAttribute('id','tableBoursesContact');
	 				tab.setAttribute('style','width:80%');
	 				frmContent.getElementById('bourseIds').value = '';
	 				frmContent.getElementById("nbLignesBourse").value = '0';
		 			//Premi�re ligne du tableau
	 				newRow = tab.insertRow(-1);
	 				newRow.setAttribute('id','headBourse_-1');
	 				newCell = newRow.insertCell(0);
	 				newCell.innerHTML = "<center>Projet entrepreneurial</center>";
	 				newCell = newRow.insertCell(1);
	 				newCell.innerHTML = "<center>R�le</center>";
	 				newCell = newRow.insertCell(2);
	 				newCell.innerHTML = "";
	 			}
	 			else
	 			{
	 				nbLignes = frmContent.getElementById("nbLignesBourse").value;
	 			}	

				autoid = nbLignes;
				
			 	for(j=0;j<objectsArray.length;j++)
	 			{
					var actionSelect = frmContent.getElementById("bourseIds");
					var reg=new RegExp("[;]+", "g");
					tableActionSelect = actionSelect.value.split(reg);
					var isPresent = false;					
					for(var x=0; x<tableActionSelect.length; x++)
					{
						if(tableActionSelect[x] == objectsArray[j].id)
						{
							isPresent = true;
							break;
						}
					}
					
					if(!isPresent)
					{
						actionSelect.value += (actionSelect.value == "" ? "" : ";") + objectsArray[j].id;
	 					newRow = tab.insertRow(-1);
		 				newRow.setAttribute('id','bourse_'+nbLignes);

						 for(i=0;i<5;i++)
	 			 		 { 		 		 
	 		 			 	newCell = newRow.insertCell(i);
	 		 			 	var tmp = "";
		 					switch(i)
	 						{
								case 0 :									
								    tmp = '<input type="text" name="bourseName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />';
								    tmp += "<input name='bourseId_"+nbLignes+"' id='bourseId_"+nbLignes+"' type='hidden' value='"+objectsArray[j].id+"' />";
									newCell.innerHTML = tmp;
	 								break;
	 						
								case 1 :
								    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>";
									newCell.innerHTML = "<select name='roleBourse_"+nbLignes+"' id='roleBourse_"+nbLignes+"' style='width:175px;'>"+
														"<option value='-1'></option>"+tmp+"</select>";
	 								break;
	 							case 2 :
									newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"bourse_"+nbLignes+"\",\"nbLignesBourse\")'/>";
	 								break; 				
	 						}
	 					
	 						with(this.newCell.style)
							{	
						 		width = '100px';
						 		textAlign = 'center';
						 		padding = '5px';
							} 
	 						autoid++;
	 				 	}
	 				 	nbLignes++;
	 				 }
	 			  }
			 	frmContent.getElementById("nbLignesBourse").value = nbLignes;
	 			  
			   }		
		}
		
		function SetEtablissements(objectsArray, close) 
		{
			var ctrl = document.getElementById("etablissement");
			if (objectsArray.length > 0)
			{
				var selectedEts = document.getElementById("selectedEts");
				var selectedName = document.getElementById("selectedName");
				
				for (var i=0; i<objectsArray.length; i++)
				{
					key = select_search(objectsArray[i].id, ctrl);
					if(key==-1)
					{
						selectedEts.value += (selectedEts.value == "" ? "" : ";") + objectsArray[i].id;
						selectedName.value += (selectedName.value == "" ? "" : ";") + objectsArray[i].text; 
						ctrl.options[ctrl.length] = new Option(unescape(objectsArray[i].text), objectsArray[i].id);
					}
				}
			}
			if (close)
			{
				 document.getElementById("frmEtablissements").src = "about:blank";
				 Popup.hide('etablissements');
			} 
		}

		function ShowFormAddBourse()
		{
			var frm = document.getElementById("frmFormAddBourse");
			frm.src = '<?php echo SCRIPTPATH . 'index.php?module=boursePopup&action=add'; ?>';		
			Popup.showModal('formAddBourse');
		}

		function submitF(formId)
		{
			var date1 = document.getElementById('annee_ante');
			var date2 = document.getElementById('annee_post');
			//var chk = new CheckFunction();
			//chk.Add(dateRencontre, 'DATE', true);
			res1 = true;
			if(date1.value!='')
			{
				if (isDate(date1.value)==false)
				{
					SetInputError(date1);
					res1=false;
				}
				else
				{
					SetInputValid(date1);
				}
			}
			if(date2.value!='')
			{
				if (isDate(date2.value)==false)
				{
					SetInputError(date2);
					res1=false;
				}
				else
				{
					SetInputValid(date2);
				}
			}
			
		    
			if(res1)
			{
				var form = document.getElementById ? document.getElementById(formId): document.forms[formId];
				form.submit();
			}
			else return false;
		}	

		
	</script>
	<div id="formAddContact" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er un contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddContact');reloadFrameBlank('frmFormAddContact');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddContact" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div id="formAddBourse" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er un projet entrepreneurial</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddBourse');reloadFrameBlank('frmFormAddBourse')"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd">
				<iframe id="frmFormAddBourse" frameborder=0 src="" style="width: 1024px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	<div class="boxGen">
		<br />
		<form id="SearchBourse"  action="<?php echo $this->BuildURL($_REQUEST['module']) ?>" method="post">
			<div class="boxPan" style="padding:5px;">	
			
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<colgroup>
						<col width="175"></col>
						<col width="10"></col>
						<col width="150"></col>
						<col width="30"></col>
						<col width="200"></col>
						<col width="200"></col>
						<col width="*"></col>
					</colgroup>
					<tr>
						<td height="25">Projet entrepreneurial</td>
						<td>:</td>
						<td><input size="40" maxlength="100" class ="notauto" type="text" name="projetInnovant" id = "projetInnovant" value="<?php if(isset($_POST["projetInnovant"])) echo ToHTML($_POST["projetInnovant"]) ?>"/></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Subvention</td>
						<td>:</td>
						<td height="25" colspan="5">
						 	<input type="checkbox" <?php if (isset($_POST["sub_obtenu"])) echo 'checked="checked"';?> name="sub_obtenu" class="notauto" value="1"> Obtenu &nbsp;
					 		<input type="checkbox" <?php if (isset($_POST["sub_refus"])) echo 'checked="checked"';?> name="sub_refus" class="notauto" value="2"> Refus�&nbsp;
					 		<input type="checkbox" <?php if (isset($_POST["sub_eval"])) echo 'checked="checked"';?> name="sub_eval" class="notauto" value="3"> En analyse 
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Encodage</td>
						<td>:</td>
						<td height="25" colspan="5">
					 		<input type="checkbox" <?php if (isset($_POST['bourseEnCours'])) echo 'checked="checked"';?> name="bourseEnCours" class="notauto" value="ENCOU"> Brouillon&nbsp;
					 		<input type="checkbox" <?php if (isset($_POST['bourseValide'])) echo 'checked="checked"';?> name="bourseValide" class="notauto" value="VALID"> Publi� 
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>R�f�rence chez AS-E</td>
						<td>:</td>
						<td height="25">
							<input size="40" maxlength="100" class ="notauto" type="text" name="refAse" id = "refAse" value="<?php if(isset($_POST["refAse"])) echo ToHTML($_POST["refAse"]) ?>"/>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td height="25">Ann�e scolaire</td>
						<td>:</td>
						<td>
							<select id="anneesScolaire" name="anneesScolaire" style="width:230px;">
							<?php
								echo '<option value="-1"></option>';
								echo createSelectOptions(Dictionnaries::getAnneeList(), $_POST['anneesScolaire']);
							?>
							</select>
						</td>
						<td></td>
						<td colspan="4">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							(Entre <input size="20" maxlength="10" class ="notauto" type="text" name="annee_ante" id = "annee_ante" value="<?php if(isset($_POST["annee_ante"])) echo ToHTML($_POST["annee_ante"]) ?>"/>
							<img src='./styles/img/calendar3.gif' id='date_ante_pic'  class="expand" title='Calendrier' />
				            <script type='text/javascript'>
 									Calendar.setup({
        								inputField     :    'annee_ante',     
        								ifFormat       :    '%d/%m/%Y',      
        								button         :    'date_ante_pic',  
        								align          :    'Tr'            
									});
							</script>								

							et <input size="20" maxlength="10" class ="notauto" type="text" name="annee_post" id = "annee_post" value="<?php if(isset($_POST["annee_post"])) echo ToHTML($_POST["annee_post"]) ?>"/> 
							<img src='./styles/img/calendar3.gif' id='date_post_pic'  class="expand" title='Calendrier' />)
				            <script type='text/javascript'>
   									Calendar.setup({
        								inputField     :    'annee_post',     
        								ifFormat       :    '%d/%m/%Y',      
        								button         :    'date_post_pic',  
        								align          :    'Tr'            
									});
							</script>
							<img src="./styles/img/eraser.gif" onclick="document.getElementById('annee_ante').value='';document.getElementById('annee_post').value='';" class="expand" title = "vider"/>
						</td>
					</tr>
					<tr>
						<td height="25">�tablissement</td>
						<td>:</td>
						<td>
							<input type="hidden" id="selectedEts" name="selectedEts" value="<?php if(isset($_POST["selectedEts"])) echo ToHTML($_POST["selectedEts"]) ?>"></input>
							<input type="hidden" id="selectedName" name="selectedName" value="<?php if(isset($_POST["selectedName"])) echo ToHTML($_POST["selectedName"]) ?>"></input>
							<select disabled id="etablissement" size="3" style="width:230px;">
							<?php
								if((isset($_POST["selectedEts"]) && isset($_POST["selectedName"]))
									&& (!empty($_POST["selectedEts"]) && !empty($_POST["selectedName"])))
								{
									$ids = explode(";",$_POST["selectedEts"]);
									$names = explode(";",$_POST["selectedName"]);
									for ($i=0; $i<count($ids); $i++)
										echo '<option value="'.$ids[$i].'">'.urldecode($names[$i]).'</option>';											
								}
							?>
							</select> 
						</td>
						<td style="padding-left:10px;">
							<img src="./styles/img/etablissement.png" id="searchEtablissements" onclick="ShowEtablissement('&close=SetEtablissements');return false" class='expand'/>
							<br /><br />
							<img src="./styles/img/eraser.gif" onclick="javascript:EmptyList('etablissement','selectedEts','selectedName')" class='expand' title = "vider"/>
						</td>
						<td style='display:none;'  height="25" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Global : <input type="checkbox" id="actionGlob" <?php if (isset($_POST["actionGlob"])) echo 'checked="checked"'; ?> name="actionGlob" class="notauto" value='1'></td>
					</tr>
					<tr>
						<td>Niveau</td>
						<td>:</td>
						<td height="25">								
							<select id="niveauAction" name="niveauAction" style="width:230px;">
							<?php
								echo '<option value="-1"></option>';
								if (isset($_POST['niveauAction'])) {
									echo createSelectOptions(Dictionnaries::getNiveauList(), $_POST['niveauAction']);
								}
								else {
									echo createSelectOptions(Dictionnaries::getNiveauList());
								}
							?>
							</select>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Niveau/Degr�</td>
						<td>:</td>
						<td height="25">								
							<select id="niveauDegreAction" name="niveauDegreAction" style="width:230px;">
							<?php
								echo '<option value="-1"></option>';
								if (isset($_POST['niveauDegreAction'])) {
									echo createSelectOptions(Dictionnaries::getNiveauDegreList(), $_POST['niveauDegreAction']);
								}
								else {
									echo createSelectOptions(Dictionnaries::getNiveauDegreList());
								}
							?>
							</select>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Niveau/Degr�/Fili�re</td>
						<td>:</td>
						<td height="25">								
							<select id="niveauDegreFilereAction" name="niveauDegreFilereAction" style="width:230px;">
							<?php
								echo '<option value="-1"></option>';
								if (isset($_POST['niveauDegreFilereAction'])) {
									echo createSelectOptions(Dictionnaries::getFiliereDegreNiveauList(), $_POST['niveauDegreFilereAction']);
								}
								else {
									echo createSelectOptions(Dictionnaries::getFiliereDegreNiveauList());
								}
							?>
							</select>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Section</td>
						<td>:</td>
						<td height="25">
							<select id="actionSection" name="actionSection" style="width:230px;">
							<?php
								echo '<option value="-1"></option>';
								if (isset($_POST['actionSection'])) {
									echo createSelectOptions(Dictionnaries::getSectionActionList(), $_POST['actionSection']);
								}
								else {
									echo createSelectOptions(Dictionnaries::getSectionActionList());
								}
							?>
							</select>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>P�dagogie</td>
						<td>:</td>
						<td height="25">
					 		<input type="checkbox" <?php if (isset($_POST['pedaAvec'])) echo 'checked="checked"';?> name="pedaAvec" class="notauto" value="1"> Avec&nbsp;
					 		<input type="checkbox" <?php if (isset($_POST['pedaSans'])) echo 'checked="checked"';?> name="pedaSans" class="notauto" value="0"> Sans 
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<?php  
					$user = Session::GetInstance()->getCurrentUser();
					if($user->isAgent() || $user->isAgentCoordinateur())
					{
					?>
					<tr>
						<td>Commentaire</td>
						<td>:</td>
						<td height="25">
					 		<input type="checkbox" <?php if (isset($_POST['aCommenter'])) echo 'checked="checked"';?> name="aCommenter" class="notauto" value="1"> � commenter&nbsp;
					 		<input type="checkbox" <?php if (isset($_POST['commente'])) echo 'checked="checked"';?> name="commente" class="notauto" value="2"> Comment�
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<?php } ?>	
				</table>
			</div>
			<input type="hidden" id="CloseCallBack" name="CloseCallBack" value="<?php echo $closeCallBack; ?>"></input>
			<script language="javascript" type="text/javascript">
				function BourseViewClass() {}
				BourseViewClass.prototype.Close = function(action)
				{
					var ets = new Array();
					var chks = document.getElementsByTagName("input");
				
					if (action == 'select')
					{
						for (var i = 0; i < chks.length; i++)
						{
							if (chks[i].name.match("^chk_bourse_"))
							{
								if (chks[i].checked)
								{
									var id = chks[i].name.replace("chk_bourse_", "");
									var nom = document.getElementById("chk_bourse_name_" + id).innerHTML;								
									ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
								}
							}
						}	
					}
					<?php echo 'parent.' . $closeCallBack . "(ets, true);"; ?>
				}
				window.bvc = new BourseViewClass();
			</script>			
			<div class="boxBtn">
				<input type="hidden" name="searchBourses"/>
				<table class="buttonTable">
					<tr>
						<td onclick="submitF('SearchBourse')" >Rechercher</td>
						<td class="separator"></td>
						<td onclick="clearForm('SearchBourse')">Effacer</td>
						<?php if ($_REQUEST['module'] == 'boursePopup') { ?>
						<td class="separator"></td>
						<td onclick="javascript:window.bvc.Close('select');">Selectionner</td>
						<td class="separator"></td>
						<td onclick="javascript:window.bvc.Close('cancel');">Annuler</td>
						<?php }?>
					</tr>
				</table>
			</div>
		</form>
	</div>
	<div id="etablissements" class="popup" style="width:944px">
		<div class="popupPanel" style="width:944px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:934px">
					<tr>
						<td width="95%">Recherche sur �tablissements</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('etablissements');reloadFrameBlank('frmEtablissements');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:944px"><iframe id="frmEtablissements" frameborder=0 src="" style="width:944px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>

	<?php  
	}
	
	public function BoursesList($elt=null)
	{

	$user = Session::GetInstance()->getCurrentUser();
	?>
	<script language="javascript" type="text/javascript">
		function ShowBourseInfo(actionId)
		{
			window.location = '<?php echo $this->BuildURL('bourseDetail') . '&id='; ?>' + actionId;
		}
		function ShowAgentInfo(id)
		{
			
			window.location = '<?php echo $this->BuildURL('agentDetail') . '&id='; ?>' + id;
		}
		function ShowEtablissementInfo(id)
		{
			window.location = '<?php echo $this->BuildURL('etablissementDetail') . '&id='; ?>' + id;
		}
		function selectAll(selected)
		{
			var liste = document.getElementsByTagName("input");
			var re = new RegExp("^chk_bourse_");

			for (var i = 0; i < liste.length; i++)
			{
				if (re.exec(liste[i].name)) liste[i].checked = selected;  
			} 
		}		
	</script>
	
	<div class="boxGen" style="border:0px;">
		<br />
			<div class="boxPan" style="padding: 5px;">
				<span style="padding-left: 5px;" class="title">Liste des projets entrepreneuriaux :&nbsp; <a class="resultLineRedirection" href="javascript:void(0);" onclick="selectAll(true);">tout s�lectionner</a> - <a class="resultLineRedirection" href="javascript:void(0);" onclick="selectAll(false);">tout d�selectionner</a></span>	
				<table  width = "98%;" cellspacing="0"  cellpadding="2" border="0" style="margin-bottom: 4px; margin:10px;" class="result">
					
					  <tr>
					  	 <td class="resultHeader" width="2%"></td>
						 <td class="resultHeader" width="10%">R�f. ASE</td>
						 <td class="resultHeader" width="20%">�tablissement</td>
						 <td class="resultHeader" width="20%">Projets entrepreneuriaux</td>
						 <td class="resultHeader" width="10%">Subvention</td>
						 <?php if ($_REQUEST['module'] == 'bourse') { ?>
						 <td class="resultHeader" width="15%">Nb d'apprenants/<br/>enseignants</td>
						 <?php } ?>
						 <td class="resultHeader" width="13%">Ann�e scolaire<br/> (Date)</td>
						 <?php if ($_REQUEST['module'] == 'bourse') { 
						  			//if($user->isAdmin()|| $user->isCoordinateur()) {?>
						 <td class="resultHeader" width="13%">Agents</td>
						 <?php } //}?>
						 <td class="resultHeader" width="*">Encodage</td>
					   </tr>
					
						<tbody>
						<tr><td>&nbsp;</td></tr>
						<?php 
						if(isset($elt) && $elt->count()>0)
						{	
							for($i=0;$i<$elt->count(); $i++)
							{
							  	$class='class="trResult"';
								echo'<tr '.$class.'>';
					  				echo sprintf('<td><input type="checkbox" name="chk_bourse_%s"></td>', $elt->items($i)->getActionId());
									echo '<td class="resultLine" valign="top">'. $elt->items($i)->GetRefChezASE().'</td>';
									echo '<td class="resultLine" valign="top">';
								
									$etabs = $elt->items($i)->GetActionEtablissment();
									for($x=0; $x<$etabs->Count(); $x++)
									{
										if ($_REQUEST['module'] == 'bourse') 
											echo '<span class="resultLineRedirection" onclick="ShowEtablissementInfo(\''.$etabs->items($x)->GetEtablissementId().'\');">'.$etabs->items($x)->GetNom().'</span><br/>';
										else
											echo '<span>'.$etabs->items($x)->GetNom().'</span><br/>';
									}
									echo '</td>';
																		
									if ($_REQUEST['module'] == 'bourse') 
										echo '<td id="chk_bourse_name_'.$elt->items($i)->getActionId().'" class="resultLine resultLineRedirection" valign="top" onclick="ShowBourseInfo(\''.$elt->items($i)->GetActionId().'\');">'. ToHTML($elt->items($i)->getActionLabel()).'</td>';
									else
										echo sprintf('<td id="chk_bourse_name_%s">%s</td>', $elt->items($i)->getActionId(), ToHTML($elt->items($i)->getActionLabel()));
										


									echo '<td class="resultLine" valign="top">'.$elt->items($i)->GetStatutSubvention().'</td>';
									if ($_REQUEST['module'] == 'bourse') { 																		
										echo '<td class="resultLine" valign="top" align="center">'.$elt->items($i)->GetNbApprenants().'/'.$elt->items($i)->GetNbEnseignants().'</td>';
									}
									echo '<td class="resultLine" valign="top">'. $elt->items($i)->GetAnneeId().' <br/>'.$elt->items($i)->GetDateActionString().'</td>';
									
									if ($_REQUEST['module'] == 'bourse') { 
										//if($user->isAdmin()|| $user->isCoordinateur()) {
											$bourseAgents = $elt->items($i)->GetBourseAgent();
										
								 			echo '<td class="resultLine" valign="top">';
								 			for($x=0;$x<$bourseAgents->count(); $x++)
											{
												$agt = $bourseAgents->items($x)->GetUtilisateur();
												echo '<span class="resultLineRedirection" onclick="javascript:ShowAgentInfo(\'' . $agt->getUtilisateurId() . '\');">'.$agt->getNom().' '.$agt->getPrenom().'</span><br/>'; 
											}	
								 			echo'</td>';
										//}
									}
									echo '<td class="resultLine" valign="top">'. $elt->items($i)->GetStatutAction().'</td>';
									echo '</tr>';
								}					
						}
						else
						{ 		
							echo'<tr><td colspan="5"><i>Il n\'y a aucun r�sultat pour votre requ�te ...</i></td></tr>';
						} 
						?>				
					</tbody>							
				</table>
			</div>
		</div>
		<div class="ligneHeaderPan"></div>
	<?php
	}

	public function AddBourse($boUpdate = false, $idB = null)
{
	
?>
	<script>
	function submitFormAction(formId, action, operation)
	{
		var errorMsg = "";
		var res1 = true;
		var chk = new CheckFunction();
		var nomGen = document.getElementById("nomGeneral");
		chk.Add(nomGen, 'TEXT', true);
		
		nbGlobalApp = document.getElementById('nbGlobalApp');
		chk.Add(nbGlobalApp, 'INT', true);
		
		nbGlobalEns = document.getElementById('nbGlobalEns');
		chk.Add(nbGlobalEns, 'INT', true);

		montanDem = document.getElementById('montantDemande');
		chk.Add(montanDem, 'FLOAT', true);
		
		categBourse = document.getElementById('categBourse');
		chk.Add(categBourse, 'TEXT', true);

		projetReal = document.getElementById('projetReal');
		chk.Add(projetReal, 'TEXT', true);

		nbClasseC = document.getElementById('nbClasseC');
		chk.Add(nbClasseC, 'INT', true);

		collabInter = document.getElementById('collabInter');
		chk.Add(collabInter, 'TEXT', true);
		
		montanSub = document.getElementById('montanSub');
		subvention = document.getElementById('subvention');
		
		if(montanSub && (subvention && subvention.value == 'OBTEN'))
			chk.Add(montanSub, 'FLOAT', true);
		
		var nbApprenantsFiliere = 0;
		var nbApprenantsPeda = 0;
		var nbApprenantsSection = 0; 
		//Verif fili�re
		<?php 
		$fdn = Dictionnaries::getFiliereDegreNiveauList();
		$niveauDegreFiliere = array();
		for($i=0; $i<$fdn->Count(); $i++)
		{
		?>	
			var val = null;
			val = document.getElementById('<?php echo $fdn->items($i)->GetFiliereDegreNiveauId() ?>');
			chk.Add(val, 'INT', false);	
			nbApprenantsFiliere += intval(val.value);
		<?php
		}
		
		//V�rif section
		$section = Dictionnaries::getSectionActionList();
		for($i=0; $i<$section->Count(); $i++)
		{
		?>
			var val = null;
			val = document.getElementById('<?php echo $section->items($i)->getSectionActionId() ?>');
			chk.Add(val, 'INT', false);	
			nbApprenantsSection += intval(val.value);
				
		<?php 			 
		}		
		?>
		//P�dagogie
		if(document.getElementById('swPeda_oui').checked)
		{
			<?php 
			$peda = Dictionnaries::getFinalitePedagogList();
			for($i=0; $i<$peda->Count(); $i++)
			{
			?>
				var val = null;
				val = document.getElementById('<?php echo $peda->items($i)->GetFinalitePedagogId() ?>');
				chk.Add(val, 'INT', false);	
				nbApprenantsPeda  += intval(val.value);
			<?php 		
			}
			?>		
		}

		var global = document.getElementById("swGlobal");
		var nbEtab = document.getElementById("nbLignes");
		var nbContact = document.getElementById("nbLignesContact");
		
		if(!global.checked && nbEtab.value <=0)
		{
			res1=false;
			errorMsg += 'Veuillez associer l\'action � un/des �tablissement(s)\n';
		}
		else if(nbContact.value<=0)
		{
			res1= false;
			errorMsg += 'Veuillez associer l\'action � un/des contact(s)\n';
		}
		else
		{
			for(i=0; i<nbContact.value; i++)
			{
				role = document.getElementById('roleActionContact_'+i);
				if(role)
				{
					if(role.value == '-1')
					{
						res1 = false;
						SetInputError(role);
					}
					else
					{
						SetInputValid(role);
					}
				}
			}
		}

		var date = document.getElementById('date');
		if (isDate(date.value)==false)
		{
			SetInputError(date);
			res1=false;
		}
		else
		{
			SetInputValid(date);
		}

		
		var res = chk.IsValid();
		if (res && res1)
		{
				if(action=='valider')
				{
					boMsg = false;
					message=''; 
					if(intval(nbApprenantsFiliere) != intval(nbGlobalApp.value) && nbApprenantsFiliere!=0)
					{
						message+='Le nombre global d\'apprenants participant � l\'action pour la r�partition par niveau-degr�-fili�re est diff�rent de '+nbGlobalApp.value+'\n';
						boMsg=true;
					}
					if(intval(nbApprenantsPeda) != intval(nbGlobalApp.value) && document.getElementById('swPeda_oui').checked && nbApprenantsPeda!=0)
					{
						message+='Le nombre global d\'apprenants participant � l\'action pour la r�partition par finalit� p�dagogique est diff�rent de '+nbGlobalApp.value+'\n';
						boMsg=true;
					}
					
					if(intval(nbApprenantsSection) != intval(nbGlobalApp.value) && nbApprenantsSection!=0)
					{
						message+='Le nombre global d\'apprenants participant � l\'action pour la r�partition par section est diff�rent de '+nbGlobalApp.value+'\n';
						boMsg=true;
					}

					if(boMsg)
					{
						alert(message);
						location.href='#top';
						return false;
					}

				}
			
			var form;
			form = document.getElementById ? document.getElementById(formId): document.forms[formId];
			var act = "<?php echo $this->BuildURL($_REQUEST['module']) ?>";
			if(operation=='add')
				act += "&action=add";
			else
				act += "&action=update";

			if(action=='valider')
				act += "&statut=valider";
			else
				act += "&statut=enCours";
			
			form.setAttribute("action", act);
			form.submit();
		} 
		else 
		{
			if(errorMsg.length>0)
				alert(errorMsg);
			
			location.href='#top';
			return false;
		}
	}
	
	function ShowListOperateur(closeCallBack)
	{
		var frm = document.getElementById("frmOperateurs");
		url = '<?php echo SCRIPTPATH . 'index.php?module=operateurPopup'; ?>';
		frm.src =url + closeCallBack;
		Popup.showModal('operateurList');
	}
	function ShowEtablissement(closeCallBack)
	{
		url ='<?php echo SCRIPTPATH . 'index.php?module=etablissementPopup'; ?>';
		var frm = document.getElementById("frmEtablissements");
		frm.src = url + closeCallBack;
		Popup.showModal('etablissements');
	}
	function ShowContactList(closeCallBack)
	{
		nbLignesEtab = document.getElementById("nbLignes").value;
		//swGlobal = document.getElementById("swGlobal");
		
		if(nbLignesEtab>0)
		{
			var urlEtab = "";
			if(nbLignesEtab>0)
			{
				var etabSelect = document.getElementById("etabIds");
				urlEtab += "&etabs="+etabSelect.value;
			}
			/*
			if(swGlobal.checked)
			{
				urlEtab += "&global=1";
			}
            */
			url = '<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=contactList'; ?>';
			var frm = document.getElementById("frmContacts");
			frm.src = url+closeCallBack+urlEtab;
			Popup.showModal('contacts');					
	
		}
		else
			alert('Veuillez associer l\'action � un �tablissement avant de l\'associer � un contact'); 
	}
	function SetContactsAction(objectsArray, close)
	{
		if (objectsArray.length > 0)		  
		{
			var nbLignes = 0;
			var divGlobal = document.getElementById("divContactsAction");
			var tab = document.getElementById('tableContactsAction');
			if(tab==null)
			{
				document.getElementById('contactIds').value = '';
 				tab = divGlobal.appendChild(document.createElement("table"));
 				tab.setAttribute("id","tableContactsAction");
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id","tr_head_contact");
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>Contact</center>";
 				newCell = newRow.insertCell(1);
 				newCell.innerHTML = "<center>R�le</center>";
 				
 			}
 			else
 				nbLignes = document.getElementById("nbLignesContact").value;
				
			autoid = nbLignes;			
			for(j=0;j<objectsArray.length;j++)
			{
				var etabSelect = document.getElementById("contactIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);	 				
	 				newRow.setAttribute("id","id_contact_tr_"+nbLignes);
	 				
					 for(i=0;i<3;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
 							case 0 :
 								tmp = '<input type="text" name="contactName_'+nbLignes+'" style="width:300px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 									  '<input name="contactId_'+nbLignes+'" id="contactId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 								newCell.innerHTML = tmp;
 								break;
 							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>";
								newCell.innerHTML = "<select name='roleActionContact_"+nbLignes+"' id='roleActionContact_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
	 							
 							case 2 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"id_contact_tr_"+nbLignes+"\", \"nbLignesContact\")'/>";
 								break; 							
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }				

			}
			 document.getElementById("nbLignesContact").value = nbLignes;
		}
		
		if(close)
		{
			document.getElementById("frmContacts").src = "about:blank";
			Popup.hide('contacts');
		}
	}
	function SetEtablissementsAction(objectsArray, close)
	{
		if (objectsArray.length > 0)		  
		{
			var nbLignes = 0;
			var divGlobal = document.getElementById("divEtablissementsAction");
			var tab = document.getElementById('tableEtablissementsAction');
			if(tab==null)
			{
				document.getElementById('etabIds').value = '';
 				tab = divGlobal.appendChild(document.createElement("table"));
 				tab.setAttribute("id","tableEtablissementsAction");
			
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id","tr_head_etab");
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>�tablissement</center>";
 			}
 			else
 			{
 				nbLignes = document.getElementById("nbLignes").value;
 			}
			autoid = nbLignes;			
		 	for(j=0;j<objectsArray.length;j++)
 			{
				var etabSelect = document.getElementById("etabIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);
	 				newRow.setAttribute("id",nbLignes);

					 for(i=0;i<2;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
 							case 0 :
 								tmp = '<input type="text" name="etabName_'+nbLignes+'" style="width:300px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 									  '<input name="etabId_'+nbLignes+'" id="etabId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 								newCell.innerHTML = tmp;
 								break;
 							case 1 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\""+nbLignes+"\", \"nbLignes\")'/>";
 								break; 							
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }
 			  }
 			  document.getElementById("nbLignes").value = nbLignes;
 			}
		if(close)
		{
			document.getElementById("frmEtablissements").src = "about:blank";
			Popup.hide('etablissements');
		}
	}
	function viewFinalitePeda(action)
	{
		display = 'none';
		if(action == 'oui') display = '';
		
		nbFinalitePeda = document.getElementById('nbFinalitePeda').value;
		for(i=0; i<nbFinalitePeda; i++)
			document.getElementById('tr_finalite_'+i).style.display=display;
	}
	function FormActionViewClass() {}
	FormActionViewClass.prototype.Close = function()
	{
		parent.Popup.hide("formAddBourse");
	}
	window.fbvc = new FormActionViewClass();	
			
	</script>
	<div id="top"></div>
	<div id="operateurList" class="popup" style="width:984px">
		<div class="popupPanel" style="width:984px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Liste des op�rateurs</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('operateurList');reloadFrameBlank('frmOperateurs');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:984px">
				<iframe id="frmOperateurs" frameborder=0 src="" style="width: 984px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	<div id="etablissements" class="popup" style="width:984px">
		<div class="popupPanel" style="width:984px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0"  style="width:974px">
					<tr>
						<td width="95%">Recherche sur �tablissements</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('etablissements');reloadFrameBlank('frmEtablissements');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"  style="width:984px">
				<iframe id="frmEtablissements" frameborder=0 src="" style="width: 984px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	<div id="contacts" class="popup" style="width:984px">
		<div class="popupPanel" style="width:984px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Liste des contacts</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('contacts'); reloadFrameBlank('frmContacts');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:984px">
				<iframe id="frmContacts" frameborder=0 src="" style="width: 984px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	<div class="boxGen">
		<form id="formAction" action="" method="post">
		<input type="hidden" value="<?php if(isset($_POST["action_id"])) echo ToHTML($_POST["action_id"]) ?>" name="action_id" />
			<div class="boxPan" style="padding: 5px;">
				<table>
					<tr>
						<td>Ann�e scolaire</td>
						<td>:</td>
						<td>
							<select id="anneesScolaire" name="anneesScolaire" style="width: 230px;" onchange="getTypeActionLab();">
							<?php
							echo createSelectOptions(Dictionnaries::getAnneeList(), $_POST['anneesScolaire']);
							?>
							</select>
						</td>
						<td>
							(<input style="width: 100px" type="text" maxlength="10" value="<?php if(isset($_POST['date'])) echo $_POST['date']; ?>" name="date" id="date" />
					 		<img src='./styles/img/calendar3.gif' id='date_pic' class="expand" title='Calendrier' />)
			 		 		<img src="./styles/img/eraser.gif" onclick="document.getElementById('date').value='';" class="expand" title="vider" />
					 		<script type='text/javascript'>
									Calendar.setup({
        								inputField     :    'date',     
        								ifFormat       :    '%d/%m/%Y',      
        								button         :    'date_pic',  
        								align          :    'Tr'            
									});
							</script>
						</td>
					</tr>
					<tr id="tr_nomGen">
						<td>Nom g�n�ral</td>
						<td>:</td>
						<td>
							<input type="text" name="nomGeneral" id="nomGeneral" value="<?php if(isset($_POST['nomGeneral'])) echo $_POST['nomGeneral']; ?>"></input>
						</td>
					</tr>
					<tr>
						<td>Nom sp�cifique</td>
						<td>:</td>
						<td>	
							<input type="text" name="nomSpecifique" id="nomSpecifique" value="<?php if(isset($_POST['nomSpecifique'])) echo $_POST['nomSpecifique']; ?>"></input>
						</td>
					</tr>
					<tr>
						<td valign="top">Commentaire</td>
						<td valign="top">:</td>
						<td colspan="2">
							<textarea name="commentaire" id="commentaire" rows="5" cols="50"><?php if(isset($_POST['commentaire'])) echo $_POST['commentaire']; ?></textarea>
						</td>
					</tr>
					<tr>
						<td>Site internet</td>
						<td>:</td>
						<td>
							<input type="text" name="web" id="web" value="<?php if(isset($_POST['web'])) echo $_POST['web']; ?>"></input>
						</td>
					</tr>
					<tr>
						<td>R�f�rence ASE</td>
						<td>:</td>
						<td><input type="text" name="refASE" id="refASE" value="<?php if(isset($_POST['refASE'])) echo $_POST['refASE']; ?>"></input></td>
					</tr>
					<?php if($boUpdate){?>
					<tr>
						<td>Subvention</td>
						<td>:</td>
						<td>
							<select id="subvention" name="subvention" style="width: 230px;">
							<?php
							echo createSelectOptions(Dictionnaries::getStatutSubventionList(), $_POST['subvention']);
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Montant subventionn�</td>
						<td>:</td>
						<td>
							<input type="text" name="montanSub" id="montanSub" value="<?php if(isset($_POST['montanSub'])) echo $_POST['montanSub']; ?>"></input>
						</td>
					</tr>					
					<?php }?>
					<tr>
						<td>Montant demand� </td>
						<td>:</td>
						<td><input type="text" name="montantDemande" id="montantDemande" value="<?php if(isset($_POST['montantDemande'])) echo $_POST['montantDemande']; ?>"></input></td>
					</tr>
					<tr>
						<td valign="top">Cat�gorie de bourse</td>
						<td valign="top">:</td>
						<td colspan="2">
							<textarea name="categBourse" id="categBourse" rows="5" cols="50"><?php if(isset($_POST['categBourse'])) echo $_POST['categBourse']; ?></textarea>
						</td>
					</tr>
					<tr>
						<td valign="top">Projet r�alis�</td>
						<td valign="top">:</td>
						<td colspan="2">
							<textarea name="projetReal" id="projetReal" rows="5" cols="50"><?php if(isset($_POST['projetReal'])) echo $_POST['projetReal']; ?></textarea>
						</td>
					</tr>
					<tr>
						<td>Nombre de classes concern�es</td>
						<td>:</td>
						<td><input type="text" name="nbClasseC" id="nbClasseC" value="<?php if(isset($_POST['nbClasseC'])) echo $_POST['nbClasseC']; ?>"></input></td>
					</tr>					
					<tr>
						<td valign="top">Collaboration inter-�tablissements</td>
						<td valign="top">:</td>
						<td colspan="2">
							<textarea name="collabInter" id="collabInter" rows="5" cols="50"><?php if(isset($_POST['collabInter'])) echo $_POST['collabInter']; ?></textarea>
						</td>
					</tr>
					<tr style='display:none'>
						<td>Global</td>
						<td>:</td>
						<td>
							<input type="checkbox" name="swGlobal" <?php if(isset($_POST['swGlobal'])) echo 'checked="checked"'; ?> id="swGlobal" value="1">
						</td>
					</tr>
				</table><br /><br />
				<div>
					<u>Associer � un/des �tablissement(s)</u> &nbsp;&nbsp; 
					<img src="./styles/img/etablissement.png" id="searchEtablissements" onclick="ShowEtablissement('&close=SetEtablissementsAction');return false" class='expand' />
					<input type="hidden" id="etabIds" name="etabIds" value="<?php if(isset($_POST['nbLignes'])) echo $_POST['etabIds']; ?>">
					<?php $nbL=0; if(isset($_POST['nbLignes'])) $nbL = $_POST['nbLignes'];?>
					<input name='nbLignes' id='nbLignes' type='hidden' value='<?php echo $nbL ?>' />
				</div><br /> <br />
				<div id="divEtablissementsAction">
				<?php 
				if((isset($_POST['nbLignes']) && $_POST['nbLignes']>0))
				{
					$this->createTableEtablissementAction();
				}
				?>
				</div><br />
				<!-- Contact -->
				<div>
					<u>Associer � un/des contact(s)</u> &nbsp;&nbsp; 
					<img src="./styles/img/add_contact.gif" id="searchContacts" onclick="ShowContactList('&close=SetContactsAction');return false" class='expand' /> 
					<input type="hidden" id="contactIds" name="contactIds" value="<?php echo !isset($_POST['contactIds']) ? "" : $_POST['contactIds']; ?>">
					<?php $nbL=0; if(isset($_POST['nbLignesContact'])) $nbL = $_POST['nbLignesContact'];?>
					<input name='nbLignesContact' id='nbLignesContact' type='hidden' value='<?php echo $nbL ?>' />
				</div><br /><br/>
				<div id="divContactsAction">
				<?php 
					if((isset($_POST['nbLignesContact']) && $_POST['nbLignesContact']>0))
						$this->createTableContactAction();
				?>
				</div>
			
			<div>
				<hr>
			</div>
			<table>
				<tr>
					<td><i>Nombre global d'apprenants participant � l'action</i></td>
					<td>:</td>
					<td>
						<input type="text" value="<?php if(isset($_POST['nbGlobalApp'])) echo $_POST['nbGlobalApp']; ?>" name="nbGlobalApp" id="nbGlobalApp" size="5"></input>
					</td>
				</tr>
				<tr>	
					<td><i>Nombre global d'enseignants encadrant l'action</i></td>
					<td>:</td>
					<td>
						<input type="text" value="<?php if(isset($_POST['nbGlobalEns'])) echo $_POST['nbGlobalEns']; ?>" name="nbGlobalEns" id="nbGlobalEns" size="5"></input>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">
						<u>Veuillez d�tailler la r�partition du nombre d'apprenants participant selon <br />les cl�s de r�partition ci-dessous.</u>
					</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>		
				<tr>
					<td colspan="2"><b>R�partition par niveau-degr�-fili�re</b></td>
					<td align="center">Nb Appr</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<?php
					$fdn = Dictionnaries::getFiliereDegreNiveauList();
					for($i=0; $i<$fdn->Count(); $i++)
					{
						$value = '';
						if(isset($_POST[$fdn->items($i)->GetFiliereDegreNiveauId()]))
							$value = $_POST[$fdn->items($i)->GetFiliereDegreNiveauId()];
						echo '<tr>';
						echo '<td>'.$fdn->items($i)->GetLabel().'</td>';
						echo '<td>:</td>';
						echo '<td><input type="text" value="'.$value.'" name="'.$fdn->items($i)->GetFiliereDegreNiveauId().'" id="'.$fdn->items($i)->GetFiliereDegreNiveauId().'" size="5"></input></td>';
						echo '</tr>';
					}
				?>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2"><b>R�partition par section</b></td>
					<td align="center">Nb Appr</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<?php
				$section = Dictionnaries::getSectionActionList();
				for($i=0; $i<$section->Count(); $i++)
				{
					$value = '';
					if(isset($_POST[$section->items($i)->getSectionActionId()]))
						$value = $_POST[$section->items($i)->getSectionActionId()];
					echo '<tr>';
					echo '<td>'.$section->items($i)->GetLabel().'</td>';
					echo '<td>:</td>';
					echo '<td><input type="text" value="'.$value.'" name="'.$section->items($i)->getSectionActionId().'" id="'.$section->items($i)->getSectionActionId().'" size="5"></input></td>';
					echo '</tr>';
				}
				?>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>	
					<td><b>L'action poursuit-elle une finalit� p�dagogique</b></td>
					<td>:</td>
					<td>
						<input type="radio" onclick="viewFinalitePeda('oui');"
						<?php if(isset($_POST['swPeda'])&& $_POST['swPeda'] == '1') echo 'checked="checked"';?>name="swPeda" id="swPeda_oui" value="1" />Oui / <input type="radio" name="swPeda" id="swPeda_non" value="0" onclick="viewFinalitePeda('non');"
						<?php if(isset($_POST['swPeda'])&& $_POST['swPeda'] == '0') echo 'checked="checked"';?> />Non
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<?php
				$peda = Dictionnaries::getFinalitePedagogList();
				for($i=0; $i<$peda->Count(); $i++)
				{
					$style= 'style="display:none;"';
					if(isset($_POST['swPeda'])&& $_POST['swPeda'] == '1')
						$style='';
					$value = '';
					if(isset($_POST[$peda->items($i)->GetFinalitePedagogId()]))
						$value = $_POST[$peda->items($i)->GetFinalitePedagogId()];
					echo '<tr id="tr_finalite_'.$i.'" '.$style.'>';
					echo '<td>'.$peda->items($i)->GetLabel().'</td>';
					echo '<td>:</td>';
					echo '<td><input type="text" value="'.$value.'" name="'.$peda->items($i)->GetFinalitePedagogId().'" id="'.$peda->items($i)->GetFinalitePedagogId().'" size="5"></input></td>';
					echo '</tr>';
				}
				?>
			</table>
			<input type="hidden" value="<?php echo $peda->Count(); ?>" id="nbFinalitePeda" />
			<?php if($boUpdate){ $operation = 'update';?>
				<input type="hidden" name="updateForm" />
			<?php }else{$operation = 'add';?>
				<input type="hidden" name="addForm" />
			<?php }?>
	</div>			
	<div class="boxBtn">
		<table class="buttonTable">
			<tr>
			<?php if(($boUpdate && $_POST['statutActionId']!='VALID') || !$boUpdate){?>
				<td style="width:180px;" onclick="submitFormAction('formAction', 'en_cours', '<?php echo $operation;?>')">Sauvegarder en Brouillon</td>
				<td class="separator"></td>
				<?php }?>
				<td style="width:180px;" onclick="submitFormAction('formAction', 'valider', '<?php echo $operation;?>')">Sauvegarder en Publi�</td>
				<td class="separator"></td>
				<td onclick="clearForm('formAction')">Effacer</td>
				<?php if(!$boUpdate){?>
				<td class="separator"></td>
				<td onclick="window.fbvc.Close();">Fermer</td>
				<?php }?>
			</tr>
		</table>
	</div>
	</form>
</div>
<?php 
	if(isset($idB))
	{
?>
	<script>
		url = "<?php echo SCRIPTPATH . 'index.php?module=bourseDetail&id='.$idB; ?>";
		parent.window.location.href = url;
		window.fbvc.Close();
	</script>		
<?php 		
	}
}

public function createTableEtablissementAction()
{
	$idTable = 'tableEtablissementsAction';
	$idHeadTr = 'tr_head_etab';
	$headLabel='�tablissement';
	$nbLignes = $_POST['nbLignes'];
?>
		<table id="<?php echo $idTable; ?>" style="width: 20%;">
			<tbody>
				<tr id="<?php echo $idHeadTr; ?>">
					<td>
						<center><?php echo ToHTML($headLabel); ?></center>
					</td>
					<td></td>
				</tr>
				<?php
				for($i=0; $i<$nbLignes; $i++)
				{
					if(isset($_POST['etabName_'.$i]))
					{
						echo '<tr id="'.$i.'"><td style="padding: 5px; width: 100px; text-align: center;">';
						echo '<input type="text" readonly="readonly" value="'.$_POST['etabName_'.$i].'" style="width: 300px;" name="etabName_'.$i.'"/>';
						echo '<input type="hidden" value="'.$_POST['etabId_'.$i].'" id="etabId_'.$i.'" name="etabId_'.$i.'"/>';
						echo '</td><td style="padding: 5px; width: 100px; text-align: center;">';
						echo '<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRow(\''.$i.'\', \'nbLignes\')"/>';
						echo '</td>';
						echo'</tr>';
					}
				}
				?>
			</tbody>
		</table>
<?php
}
	
public function createTableContactAction()
{
	$idTable = 'tableContactsAction';
	$idHeadTr = 'tr_head_contact';
	$headLabel='Contact';
	$nbLignes = $_POST['nbLignesContact'];
	?>
		<table id="<?php echo $idTable; ?>" style="width: 20%;">
			<tbody>
				<tr id="<?php echo $idHeadTr; ?>">
					<td>
						<center><?php echo ToHTML($headLabel); ?></center>
					</td>
					<td>
						<center>R�le</center>
					</td>
				</tr>
				<?php
				for($i=0; $i<$nbLignes; $i++)
				{
					if(isset($_POST['contactName_'.$i]))
					{
						echo '<tr id="id_contact_tr_'.$i.'"><td style="padding: 5px; width: 100px; text-align: center;">';
						echo '<input type="text" readonly="readonly" value="'.$_POST['contactName_'.$i].'" style="width: 300px;" name="contactName_'.$i.'"/>';
						echo '<input type="hidden" value="'.$_POST['contactId_'.$i].'" id="contactId_'.$i.'" name="contactId_'.$i.'"/>';
						echo '</td><td style="padding: 5px; width: 100px; text-align: center;">';
						echo '<select name="roleActionContact_'.$i.'" id="roleActionContact_'.$i.'" style="width:175px;">';
						echo '<option value="-1"></option>';
						echo createSelectOptions(Dictionnaries::getRoleContactActionList(), $_POST['roleActionContact_'.$i]);
						echo '</select>';
						echo '</td><td style="padding: 5px; width: 100px; text-align: center;">';
						echo '<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRow(\'id_contact_tr_'.$i.'\', \'nbLignesContact\')"/>';
						echo '</td>';
						echo'</tr>';
					}
				}
				?>
			</tbody>
		</table>
	<?php
	}	
}
?>