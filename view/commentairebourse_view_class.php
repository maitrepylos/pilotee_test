<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class CommentaireView extends BaseView
{
	private $user = null;
	
	public function addCommentaire($idBourse, $idAgent)
	{
		$this->user = Session::GetInstance()->getCurrentUser();
		?>
		<script>
		function valideCom (idForm)
		{ 
			var res1 = true;
			var syntheseAvis = document.getElementById("syntheseAvis");
			var avisQualitatif = document.getElementById("avisQualitatif");
			var eltImportant = document.getElementById("eltImportant");
			var soutientFiancier = document.getElementById("soutientFiancier");
			var chk = new CheckFunction();
			if(syntheseAvis.value=='-1')
			{
				SetInputError(syntheseAvis);
				res1 = false;
			}
			else
				SetInputValid(syntheseAvis);

			chk.Add(avisQualitatif, 'TEXT', true);
			chk.Add(eltImportant, 'TEXT', true);
			chk.Add(soutientFiancier, 'FLOAT', true);
			var res = chk.IsValid();
			if (res && res1)
			{
				submitForm(idForm);	
			}
			else
				return false;			
			
			
		}
		
		</script>
		<div class="boxPan" style="padding:5px;">
			<form id="addCommentaire" action="<?php echo $this->BuildURL('commenterBourse').'&bourseId='.$idBourse.'&agentId='.$idAgent?>" method="post">
				<table>
					<tr><td>Synth�se de l'avis : </td></tr>
					<tr>
						<td>
							<select id="syntheseAvis" name="syntheseAvis" class="notauto" style="width:40%">
								<option value="-1"></option>
								<?php 
									echo createSelectOptions(Dictionnaries::getSyntheseAvisList(), $_POST['syntheseAvis']);
								?>
							</select>
						</td>					
					</tr>

					<tr><td>Avis qualitatif : </td></tr>
					<tr><td><textarea name="avisQualitatif" id="avisQualitatif" rows="5" cols="90"></textarea> </td></tr>
					
					<tr><td>El�ments importants : </td></tr>
					<tr><td><textarea name="eltImportant" id="eltImportant" rows="5" cols="90"></textarea> </td></tr>					
					
					<tr><td>Soutien financier : </td></tr>
					<tr><td><input type="text" value="" name="soutientFiancier" id="soutientFiancier" size="25" class="notauto"></input>  </td></tr>
				</table>
				<input type="hidden" name="addCommentaire"></input>
				<div class="boxBtn">
					<table class="buttonTable">
						<tr>
							<td onclick = "valideCom('addCommentaire')">Valider</td>
							<td class="separator"></td>
							<td onclick="clearForm('addCommentaire')">Effacer</td>
							<td class="separator"></td>
						</tr>
					</table>
				</div>
			</form>
		</div>
<?php
	}
}
?>
