<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class ContactDetailView extends BaseView
{
	private $user = null;
	
	public function Render($contact)
	{
		$this->user = Session::GetInstance()->getCurrentUser();	
		$adr = $contact->GetAdresse();	
?>
	<script language="javascript" type="text/javascript">

		function ShowFormAddContact()
		{
			var frm = document.getElementById("frmFormAddContact");
			frm.src = '<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add';?>';			
			Popup.showModal('formAddContact');
		}

		function ShowHistoriqueDetail()
		{
			var frm = document.getElementById("frmHistoriqueDetail");
			frm.src = '<?php echo SCRIPTPATH . 'index.php?module=historiquePopup&id='.$contact->GetContactId().'&objet=contact';?>';			
			Popup.showModal('HistoriqueDetail');
		}

		function updateContact(contactId)
		{
			document.location.href = '<?php echo $this->BuildURL('contact') .'&action=update&updateId='; ?>' + contactId; 
		}
		
		function deleteContact(contactId)
		{
			if (sure("Supprimer le contact ?") == true)	
				parent.document.location.href = '<?php echo $this->BuildURL('contact') .'&action=delete&deleteId='; ?>' + contactId; 
		}
		
		function backToContact()
		{
			parent.document.location.href = '<?php echo $this->BuildURL('contact'); ?>'; 
		}

	</script>
	<div id="formAddContact" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er un contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddContact');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddContact" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div id="HistoriqueDetail" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Historique : <?php echo $contact->GetNom(). ' ' . $contact->GetPrenom() ?></td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('HistoriqueDetail');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmHistoriqueDetail" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>	
	<div class="boxGen">
		<div class="boxPan" style="padding:5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Nom - Pr�nom :</td>
						<td class="detail"><?php echo $contact->GetNom(). ' ' . $contact->GetPrenom() ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Autre acteur :</td>
						<td class="detail" style="color:<?php echo $contact->GetContactGlobalColor(); ?>"><?php echo $contact->GetContactGlobalLabel(); ?></td>
					</tr>					
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">T�l�phone(s) :</td>
						<td class="detail">
							<?php
								$tel1 = $adr->GetTel1();
								$tel2 = $adr->GetTel2();

								$tel = null;
								
								if (isset($tel1) && isset($tel2)) $tel = sprintf('%s / %s', $tel1, $tel2);
								elseif (isset($tel1)) $tel = $tel1;
								elseif (isset($tel2)) $tel = $tel2;
								
								echo $tel;
							?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Genre :</td>
						<td class="detail">
						<?php 
							$genreLab = '';
							if($contact->getGenreId())
							{
								$genres =  Dictionnaries::getGenreList($contact->getGenreId());
								$genreLab = $genres->items(0)->getLabel();
							}
							echo $genreLab;
						?>
						</td>
					</tr>					
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Cr�ation :</td>
						<td class="detail">
						<?php
							$user  = $contact->GetCreatedByUser();
							
							if($user->getTypeUtilisateurId() != 'AGE')
							 		
								echo 'le '.$contact->GetDateCreatedString(false). ' par '. $user->GetNom().' '.$user->GetPrenom();
							else
								echo 'le '.$contact->GetDateCreatedString(false).' par <a href=\''.$this->BuildURL('agentDetail').'&id='.$user->getUtilisateurId().'\'>'.$user->GetNom().' '.$user->GetPrenom().'</a>';

						?>
						</td>
					</tr>
  				 </table>
				</td>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Identification :</td>
						<td class="detail"><?php echo $contact->GetContactId(); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Statut :</td>
						<td class="detail" style="color:<?php echo $contact->GetSwActifColor(); ?>"><?php echo $contact->GetSwActifLabel(); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Courriel(s) :</td>
						<td class="detail">
							<?php
							
								$email1 = $adr->GetEmail1AsAnchor();
								$email2 = $adr->GetEmail2AsAnchor();
								$email = null;
								
								if (isset($email1) && isset($email2)) $email = sprintf('%s / %s', $email1, $email2);
								elseif (isset($email1)) $email = $email1;
								elseif (isset($email2)) $email = $email2;
								echo $email;
							?>
						</td>
					 </tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Tranche d'�ge :</td>
						<td class="detail">
						<?php 
							$ageLab = '';
							if($contact->getAgeId())
							{
								$ages =  Dictionnaries::getAgeList($contact->getAgeId());
								$ageLab = $ages->items(0)->getLabel();
							}
							echo $ageLab;
						?>							
						</td>
					</tr>					 
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Modification :</td>
						<td class="detail">
						<?php
							$user  = $contact->GetUpdatedByUser();
							if($user->getTypeUtilisateurId() !='AGE')	
								echo 'le '.$contact->GetDateUpdateString(false).' par '.$user->GetNom().' '.$user->GetPrenom();
							else
								echo 'le '.$contact->GetDateUpdateString(false) .' par <a href=\''.$this->BuildURL('agentDetail').'&id='.$user->getUtilisateurId().'\'>'.$user->GetNom().' '.$user->GetPrenom().'</a>';
						?>
						</td>
					</tr>					
					 			
  				  </table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<script language="javascript" type="text/javascript">
						function ContactDetailViewClass() 
						{
							this.current = 'All';
							this.tabs = new Array('All', 'Global', 'Etablissement2', 'Actions', 'Formations2', 'ProjetsInnovants');
						}

						ContactDetailViewClass.prototype.onMouseOver = function(liID)
						{
							
							document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
							document.getElementById("spn" + liID).style.color = "#FFFFFF";
							document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
						}

						ContactDetailViewClass.prototype.onMouseOut = function(liID)
						{
							if (this.current != liID)
							{
								document.getElementById("spn" + liID).style.backgroundPosition = "100% 0%";
								document.getElementById("spn" + liID).style.color = "#737373";
								document.getElementById("a" + liID).style.backgroundPosition = "0% 0%";
							}
						}

						ContactDetailViewClass.prototype.onLIClick = function(liID)
						{
							this.current = liID;
							
							for (var i = 0; i < this.tabs.length; i++)
							{
								if(document.getElementById("spn" + this.tabs[i]))
								{
									document.getElementById("spn" + this.tabs[i]).style.backgroundPosition = "100% 0%";
									document.getElementById("spn" + this.tabs[i]) .style.color = "#737373";
									document.getElementById("a" + this.tabs[i]).style.backgroundPosition = "0% 0%";
								}
							}

							document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
							document.getElementById("spn" + liID).style.color = "#FFFFFF";
							document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
							
							var global = document.getElementById('Global');
							var etablissement = document.getElementById('Etablissement');
							var actions = document.getElementById('Actions');
							var formations2 = document.getElementById('Formations2');							
							var projetsInnovants = document.getElementById('ProjetsInnovants');
							
							if (liID == 'All')
							{
								if (etablissement) etablissement.style.display = '';
								if (global) global.style.display = '';
								if (projetsInnovants) projetsInnovants.style.display = '';
								if (actions) actions.style.display = '';
								if (formations2) formations2.style.display = '';
							}
							else if (liID == 'Global')
							{
								if (etablissement) etablissement.style.display = 'none';
								if (global) global.style.display = '';
								if (projetsInnovants) projetsInnovants.style.display = 'none';
								if (actions) actions.style.display = 'none';
								if (formations2) formations2.style.display = 'none';
							}
							else if (liID == 'Etablissement2')
							{
								if (etablissement) etablissement.style.display = '';
								if (global) global.style.display = 'none';
								if (projetsInnovants) projetsInnovants.style.display = 'none';
								if (actions) actions.style.display = 'none';
								if (formations2) formations2.style.display = 'none';
							}
							else if (liID == 'Actions')
							{

								if (actions) actions.style.display = '';
								if (etablissement) etablissement.style.display = 'none';
								if (global) global.style.display = 'none';
								if (projetsInnovants) projetsInnovants.style.display = 'none';								
								if (formations2) formations2.style.display = 'none';
							}
							else if (liID == 'ProjetsInnovants')
							{
								if (actions) actions.style.display = 'none';
								if (projetsInnovants) projetsInnovants.style.display = '';
								if (etablissement) etablissement.style.display = 'none';
								if (global) global.style.display = 'none';
								if (formations2) formations2.style.display = 'none';
							}
							else if (liID == 'Formations2')
							{
								if (actions) actions.style.display = 'none';
								if (projetsInnovants) projetsInnovants.style.display = 'none';
								if (etablissement) etablissement.style.display = 'none';
								if (global) global.style.display = 'none';
								if (formations2) formations2.style.display = '';
							}
							
						}
						window.edvc = new ContactDetailViewClass();
					</script>
					<?php 
					$etC = $contact->GetEtablissementsContacts();
				
					?>

					<div class="header" style="border-bottom:3px solid #EAC3C3;">
						<ul>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('All');" OnMouseOut="javascript:window.edvc.onMouseOut('All');"><a id="aAll" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('All');"><span id="spnAll" class="expand">Tout</span></a></li>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('Etablissement2');" OnMouseOut="javascript:window.edvc.onMouseOut('Etablissement2');"><a id="aEtablissement2" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Etablissement2');"><span id="spnEtablissement2" class="expand">Etablissements d'enseignement</span></a></li>
							<?php if($contact->GetContactGlobal()){?>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('Global');" OnMouseOut="javascript:window.edvc.onMouseOut('Global');"><a id="aGlobal" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Global');"><span id="spnGlobal" class="expand">Autre acteur</span></a></li>
							<?php }?>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('Actions');" OnMouseOut="javascript:window.edvc.onMouseOut('Actions');"><a id="aActions" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Actions');"><span id="spnActions" class="expand">Actions</span></a></li>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('Formations2');" OnMouseOut="javascript:window.edvc.onMouseOut('Formations2');"><a id="aFormations2" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Formations2');"><span id="spnFormations2" class="expand">Formations</span></a></li>
							<?php if (!$this->user->isOperateur()) { ?>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('ProjetsInnovants');" OnMouseOut="javascript:window.edvc.onMouseOut('ProjetsInnovants');"><a id="aProjetsInnovants" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('ProjetsInnovants');"><span id="spnProjetsInnovants" class="expand">Projets entrepreneuriaux</span></a></li>
							<?php } ?>
						</ul>
					
					</div>
					<div style="margin-top:5px; width:100%;">
						<div id="Etablissement">
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Etablissements d'enseignement</span></div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="resultDetailPanel">
										<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
										<colgroup>
											<col width="300" />
											<?php if(!$this->user->isOperateur()){ ?>
											<col width="150" />
											<?php } ?>
											<col width="150" />
											<col width="150" />
											<col width="*" />
										</colgroup>
										<?php																				
									
											echo '<tr>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Nom</td>';
												if(!$this->user->isOperateur())
													echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Origine</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Sp�cialit�</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Titre</td>';
												if(!$this->user->isOperateur())
													echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Personne ressource</td>';
											echo '</tr>';
											//echo '<tr><td>&nbsp;</td></tr>';
											
											for($i=0;$i<$etC->count(); $i++)
											{
												if($etC->items($i)->GetEtablissementId()!=null)
												{
												$etab = true;												
												echo '<tr class="trResult">';
													echo '<td  style="padding-top:5px;padding-bottom:5px;"><a href="'.$this->BuildURL('etablissementDetail') .'&id='.$etC->items($i)->GetEtablissementId().'">'.$etC->items($i)->GetEtablissement()->GetNom().'</a></td>';
													if(!$this->user->isOperateur())
													{
														$ori = '';
														if($etC->items($i)->GetOrigineContactId()!=null && $etC->items($i)->GetOrigineContactId()!=-1)
														{
															$origine = Dictionnaries::getOrigineList($etC->items($i)->GetOrigineContactId());
															if (count($origine) > 0) {
																$ori = $origine->items(0)->GetLabel();
															}
														}
														
														echo '<td  style="padding-top:5px;padding-bottom:5px;">'.$ori.'</td>';
													}
													$specia = '';
													if($etC->items($i)->GetSpecialiteContactId()!=null && $etC->items($i)->GetSpecialiteContactId()!=-1)
													{
														$specialite = Dictionnaries::getSpecialiteList($etC->items($i)->GetSpecialiteContactId());
														if (count($specialite) > 0) {
																$specia = $specialite->items(0)->GetLabel();
														}
													}
													echo '<td  style="padding-top:5px;padding-bottom:5px;">'.$specia.'</td>';
													
													$tit = '';
													if($etC->items($i)->GetTitreContactId() != null && $etC->items($i)->GetTitreContactId()!=-1)
													{
														$titre = Dictionnaries::getTitreContactList($etC->items($i)->GetTitreContactId());
														if (count($titre) > 0) {
																$tit = $titre->items(0)->GetLabel(); 
														}
													}
													
													echo '<td  style="padding-top:5px;padding-bottom:5px;">'.$tit.'</td>';
													
													if(!$this->user->isOperateur())
													{
														$part='';
														if($etC->items($i)->GetParticipationId() !=null && $etC->items($i)->GetParticipationId() != -1)
														{
															$participation = Dictionnaries::getParticipationList($etC->items($i)->GetParticipationId());
															if (count($participation) > 0) {
																$part = $participation->items(0)->GetLabel(); 
															}
														}
														echo '<td style="padding-top:5px;padding-bottom:5px;">'.$part.'</td>';
													}
												echo '</tr>';	
												}											
											}
										
										?>
								  </table>
								</td>
						 	  </tr>
							</table>
							</div>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>	
						<?php if($contact->GetContactGlobal()){?>
						<div id="Global" style="width:100%;">
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Autre acteur</span></div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="resultDetailPanel">
										<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
										<colgroup>
											<col width="300" />
											<?php if(!$this->user->isOperateur()){ ?>
											<col width="150" />
											<?php } ?>
											<col width="150" />
											<col width="150" />
											<col width="*" />
										</colgroup>
										<?php
										
											echo '<tr>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Nom</td>';
												if(!$this->user->isOperateur())
													echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Origine</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Sp�cialit�</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Titre</td>';
												if(!$this->user->isOperateur())
													echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Personne ressource</td>';
											echo '</tr>';

											for($i=0;$i<$etC->count(); $i++)
											{
												if($etC->items($i)->GetEtablissementId()==null)
												{												
												echo '<tr class="trResult">';
													echo '<td  style="padding-top:5px;padding-bottom:5px;">Autre acteur</td>';
													$origine = Dictionnaries::getOrigineList($etC->items($i)->GetOrigineContactId());
													echo '<td  style="padding-top:5px;padding-bottom:5px;">'.$origine->items(0)->GetLabel().'</td>';
													if(!$this->user->isOperateur())
													{
														$spec = '';
														
														if($etC->items($i)->GetSpecialiteContactId()!=null && $etC->items($i)->GetSpecialiteContactId()!=-1)
														{
															$specialite = Dictionnaries::getSpecialiteList($etC->items($i)->GetSpecialiteContactId());
															$spec = $specialite->items(0)->GetLabel();
														}
														echo '<td  style="padding-top:5px;padding-bottom:5px;">'.$spec.'</td>';
													}
													
													$tit = '';
													if($etC->items($i)->GetTitreContactId()!=null && $etC->items($i)->GetTitreContactId()!=-1)
													{
														$titre = Dictionnaries::getTitreContactList($etC->items($i)->GetTitreContactId());
														$tit = $titre->items(0)->GetLabel();
													}
													echo '<td  style="padding-top:5px;padding-bottom:5px;">'.$tit.'</td>';
													if(!$this->user->isOperateur())
													{
														$part = '';
														if($etC->items($i)->GetParticipationId()!=null && $etC->items($i)->GetParticipationId()!=-1)
														{
															$participation = Dictionnaries::getParticipationList($etC->items($i)->GetParticipationId());
															$part = $participation->items(0)->GetLabel();
														}
														
														echo '<td style="padding-top:5px;padding-bottom:5px;">'.$part.'</td>';
													}
												echo '</tr>';	
												}
												
											}
										
										?>
									</table>
								</td>
							</tr>
							</table>
							</div>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>	
						<?php } ?>
						<div id="Actions">
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Actions</span></div>
							<div class="resultDetailPanel">
								<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
								<colgroup>
								<col width="125" />
								<col width="125" />
								<col width="125" />
								<col width="125" />
								<col width="*" />
								</colgroup>
								
								<?php
									$acts = $this->user->isOperateur() ? $contact->GetActions($this->user->getUtilisateurId()) : $contact->GetActions();
									
									if (isset($acts))
									{
										echo '<tr>';
										echo '<td class="resultHeader">Ann�e scolaire</td>';
										echo '<td class="resultHeader">Date d\'action</td>';
										echo '<td class="resultHeader">Action</td>';
										echo '<td class="resultHeader">Op�rateur</td>';	
										echo '</tr>';
										
										for ($i = 0; $i < $acts->count(); $i++)
										{
											echo '<tr class="trResult">';
											echo sprintf('<td>%s</td>', $acts->items($i)->GetAction()->GetAnneeId());
											echo sprintf('<td>%s</td>', $acts->items($i)->GetAction()->GetDateAction(true));
											echo sprintf('<td><a class="resultLineRedirection" href="%s">%s</a></td>', $this->BuildURL('actionDetail') . '&id=' . $acts->items($i)->GetAction()->GetActionId(), ToHTML($acts->items($i)->GetAction()->GetNom()));
											echo sprintf('<td><a class="resultLineRedirection" href="%s">%s</a></td>', $this->BuildURL('operateurDetail') . '&id=' . $acts->items($i)->GetAction()->GetOperateur()->GetOperateurId(), $acts->items($i)->GetAction()->GetOperateur()->GetNom());
											echo '</tr>';
										}
									}
								?>
								</table>
							</div>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>
						
						<div id="Formations2">
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Formations</span></div>
							<div class="resultDetailPanel">
								<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
								<colgroup>
								<col width="125" />
								<col width="125" />
								<col width="125" />
								<col width="125" />
								<col width="*" />
								</colgroup>
								
								<?php // xxx
									$acts = $this->user->isOperateur() ? $contact->GetFormations($this->user->getUtilisateurId()) : $contact->GetFormations();
									
									if (isset($acts))
									{
										echo '<tr>';
										echo '<td class="resultHeader">Ann�e scolaire</td>';
										echo '<td class="resultHeader">Date de la formation</td>';
										echo '<td class="resultHeader">Formation</td>';
										echo '<td class="resultHeader">Op�rateur</td>';	
										echo '</tr>';
										
										for ($i = 0; $i < $acts->count(); $i++)
										{

											echo '<tr class="trResult">';
											echo sprintf('<td>%s</td>', $acts->items($i)->GetAction()->GetAnneeId());
											echo sprintf('<td>%s</td>', $acts->items($i)->GetAction()->GetDateAction(true));
											echo sprintf('<td><a class="resultLineRedirection" href="%s">%s</a></td>', $this->BuildURL('formationDetail') . '&id=' . $acts->items($i)->GetAction()->GetActionId(), ToHTML($acts->items($i)->GetAction()->GetNom()));
											echo sprintf('<td><a class="resultLineRedirection" href="%s">%s</a></td>', $this->BuildURL('operateurDetail') . '&id=' . $acts->items($i)->GetAction()->GetOperateur()->GetOperateurId(), $acts->items($i)->GetAction()->GetFormateurOperateur()->GetNom());
											echo '</tr>';
										}
									}
								?>
								</table>
							</div>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>
						
						<?php if (!$this->user->isOperateur()) { ?>
						<div id="ProjetsInnovants">
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Projets entrepreneuriaux</span></div>
							<div class="resultDetailPanel">
								<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
								<colgroup>
								<col width="125" />
								<col width="125" />
								<col width="*" />
								</colgroup>
								
								<?php
									$pi = $contact->GetProjetsInnovants();
									
									if (isset($pi))
									{
										echo '<tr>';
										echo '<td class="resultHeader">Ann�e scolaire</td>';
										echo '<td class="resultHeader">Date du projet</td>';
										echo '<td class="resultHeader">Projet</td>';
										echo '</tr>';
										
										for ($i = 0; $i < $pi->count(); $i++)
										{
											echo '<tr class="trResult">';
											echo sprintf('<td>%s</td>', $pi->items($i)->GetAction()->GetAnneeId());
											echo sprintf('<td>%s</td>', $pi->items($i)->GetAction()->GetDateAction(true));
											echo sprintf('<td><a class="resultLineRedirection" href="%s">%s</a></td>', $this->BuildURL('bourseDetail') . '&id=' . $pi->items($i)->GetAction()->GetActionId(), ToHTML($pi->items($i)->GetAction()->GetNom()));
											echo '</tr>';
										}
									}
								?>
								
								</table>
							</div>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>			
							<?php } ?>
					</div>
				</td>
			</tr>
		  </table>
		</div>
		
		<script language="javascript" type="text/javascript">
			document.getElementById("spnAll").style.backgroundPosition = "100% -250px";
			document.getElementById("spnAll").style.color = "#FFFFFF";
			document.getElementById("aAll").style.backgroundPosition = "0% -250px";
		</script>
		<?php if(!$this->user->isOperateur() || ($this->user->isOperateur() && $contact->GetCreatedBy() == $this->user->getUtilisateurId())){ ?>
		<div class="boxBtn">
			<table class="buttonTable">
				<tr>
				<?php if($_REQUEST['module']!='contactDetailPopup'){?>
					<td onclick="updateContact('<?php echo $contact->GetContactId();?>')">Modifier</td>
					<td class="separator"></td>
					<td onclick="deleteContact('<?php echo $contact->GetContactId();?>')">Supprimer</td>
				<?php }else{?>
				<td onclick="parent.Popup.hide('contactDetail');">Fermer</td>
				<?php }?>	
				</tr>
			</table>
		</div>
		<?php } ?>
	</div>
<?php
	}
}
?>
