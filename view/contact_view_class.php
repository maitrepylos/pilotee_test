<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'lib/sessionbase.domain.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');
class ContactView extends BaseView
{
	
	public function Search($closeCallBack)
	{
		$user = Session::GetInstance()->getCurrentUser();
	?>
	<script type="text/javascript">
		function SetEtablissements(objectsArray, close) 
		{
			var ctrl = document.getElementById("etablissement");
			if (objectsArray.length > 0)
			{
				var selectedEts = document.getElementById("selectedEts");
			
				var selectedName = document.getElementById("selectedName");
				
				for (var i=0; i<objectsArray.length; i++)
				{
					key = select_search(objectsArray[i].id, ctrl);
					if(key==-1)
					{
						selectedEts.value += (selectedEts.value == "" ? "" : ";") + objectsArray[i].id;
						selectedName.value += (selectedName.value == "" ? "" : ";") + objectsArray[i].text; 
						ctrl.options[ctrl.length] = new Option(unescape(objectsArray[i].text), objectsArray[i].id);
					}
				}
			}

			if (close)
			{
				 document.getElementById("frmEtablissements").src = "about:blank";
				 Popup.hide('etablissements');
			} 
		}

		function SearchContactViewClass() {}
		SearchContactViewClass.prototype.Close = function(action)
		{
			var ets = new Array();
			var chks = document.getElementsByTagName("input");
		
			if (action == 'select')
			{
				for (var i = 0; i < chks.length; i++)
				{
					if (chks[i].name.match("^chk_contact_"))
					{
						if (chks[i].checked)
						{
							var id = chks[i].name.replace("chk_contact_", "");
							var nom = document.getElementById("chk_contact_name_" + id).innerHTML;
							ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
						}
					}
				}
			}
			<?php echo 'parent.' . $closeCallBack . "(ets, true);"; ?>
		}
		window.searchContact = new SearchContactViewClass();
		
		
	</script>
		<div class="boxGen">
			<br />
			<?php if(isset($closeCallBack)) $closeCallBack = '&close='.$closeCallBack ;else $closeCallBack='';?>
			<form id="SearchContact" action="<?php echo $this->BuildURL($_REQUEST['module']).$closeCallBack; ?>" method="post">
				<div class="boxPan" style="padding:5px;">	
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<colgroup>
							<col width="175"/>
							<col width="10"/>
							<col width="150"/>
							<col width="70" />
							<col width="175" />
							<col width="10" />
							<col width="*"/>
						</colgroup>
						<tr>
							<td height="25">Nom </td>
							<td>:</td>
							<td><input size="40" maxlength="100" class ="notauto" type="text" name="nom" id = "nom" value="<?php if(isset($_POST["nom"])) echo ToHTML($_POST["nom"]) ?>"/></td>
							<td></td>
							<td height="25">Pr�nom </td>
							<td>:</td>
							<td><input  size="40" maxlength="100" class ="notauto" type="text" name="prenom" id = "prenom" value="<?php if(isset($_POST["prenom"])) echo ToHTML($_POST["prenom"]) ?>"/></td>
						</tr>
						<tr>
							<td height="25">Email</td>
							<td>:</td>
							<td><input size="40" maxlength="100" class ="notauto" type="text" name="email" id = "email" value="<?php if(isset($_POST["email"])) echo ToHTML($_POST["email"]) ?>"/></td>
							<td></td>
							<td></td>
							<td></td>
							<td class="left"><input type="checkbox" id="ActifContact" <?php if (isset($_POST["swActif"])) echo 'checked="checked"'; ?> name="swActif" class="notauto" value='1'> Actif &nbsp; <input type="checkbox" <?php if (isset($_POST["swInactif"])) echo 'checked="checked"'; ?> name="swInactif" class="notauto" value='0'> Inactif </td>
						</tr>
						<tr valign="top">
							<td height="25">�tablissement</td>
							<td>:</td>
							<td>
								<input type="hidden" id="selectedEts" name="selectedEts" value="<?php if(isset($_POST["selectedEts"])) echo ToHTML($_POST["selectedEts"]) ?>"></input>
								<input type="hidden" id="selectedName" name="selectedName" value="<?php if(isset($_POST["selectedName"])) echo ToHTML($_POST["selectedName"]) ?>"></input>
								<select disabled id="etablissement" size="3" style="width:230px;">
								<?php
									if((isset($_POST["selectedEts"]) && isset($_POST["selectedName"]))
										&& (!empty($_POST["selectedEts"]) && !empty($_POST["selectedName"])))
									
									{
										$ids = explode(";",$_POST["selectedEts"]);
										$names = explode(";",$_POST["selectedName"]);
										for ($i=0; $i<count($ids); $i++)
											echo '<option value="'.$ids[$i].'">'.urldecode($names[$i]).'</option>';											
									}
								?>
								</select> 
							</td>
							<td style="padding-left:10px;">
								<img src="./styles/img/etablissement.png" id="searchEtablissements" onclick="ShowEtablissement('&close=SetEtablissements');return false" class='expand'/>
								<br />
								<br />
								<img src="./styles/img/eraser.gif" onclick="javascript:EmptyList('etablissement','selectedEts','selectedName')" class='expand' title = "vider"/>
							</td>
							<td>Autre acteur</td>
							<td>:</td>
							<td><input type="checkbox" id="etabGlob" <?php if (isset($_POST["etabGlob"])) echo 'checked="checked"'; ?> name="etabGlob" class="notauto" value='1'></td>
						</tr>
					</table>
				</div>				
				<div class="boxBtn">
					<input type="hidden" name="searchContact"/>
					<table class="buttonTable">
						<tr>
							<td onclick="submitForm('SearchContact')" >Rechercher</td>
							<td class="separator"></td>
							<td onclick="clearForm('SearchContact')">Effacer</td>
							<?php if($_REQUEST['module']=='contactPopup'){?>
							<td class="separator"></td>
							<td onclick="javascript:window.searchContact.Close('select');">Selectionner</td>
							<td class="separator"></td>
							<td onclick="javascript:window.searchContact.Close('cancel');">Annuler</td>
							<?php }?>
						</tr>
					</table>
				</div>
			</form>
		</div>		
	<?php
	}
	
	public function framePopup()
	{
	?>
	<script>	
		function ShowEtablissement(closeCallBack)
		{			
			var frm = document.getElementById("frmEtablissements");
			url = '<?php echo SCRIPTPATH . 'index.php?module=etablissementPopup'; ?>';
			frm.src = url + closeCallBack;
			Popup.showModal('etablissements');
		}		
		function ShowAction(closeCallBack)
		{
			
			var frm = document.getElementById("frmActions");
			url = '<?php echo SCRIPTPATH . 'index.php?module=actionPopup'; ?>';
			frm.src = url + closeCallBack;
			Popup.showModal('actions');
		}
		function ShowFormation(closeCallBack)
		{
			
			var frm = document.getElementById("frmFormations");
			url = '<?php echo SCRIPTPATH . 'index.php?module=formationPopup'; ?>';
			frm.src = url + closeCallBack;
			Popup.showModal('formations');
		}
		function ShowBourse(closeCallBack)
		{
			var frm = document.getElementById("frmBourses");
			url = '<?php echo SCRIPTPATH . 'index.php?module=boursePopup'; ?>';
			frm.src = url + closeCallBack;
			Popup.showModal('bourses');
		}				
		function ShowFormAddContact()
		{			
			var frm = document.getElementById("frmFormAddContact");
			frm.src = '<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add'; ?>';			
			Popup.showModal('formAddContact');
		}
	</script>
		<div id="etablissements" class="popup" style="width:984px">
			<div class="popupPanel" style="width:984px">
				<div class="popupPanelHeader">
					<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
						<tr>
							<td width="95%">Recherche sur �tablissements</td>
							<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('etablissements');reloadFrameBlank('frmEtablissements');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
						</tr>
					</table>
				</div>
				<div class="bd" style="width:984px"><iframe id="frmEtablissements" frameborder=0 src="" style="width:984px;height:512px;border:0px;"></iframe></div>
			</div>
		</div>
		<div id="actions" class="popup" style="width:984px">
			<div class="popupPanel" style="width:984px">
				<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Recherche sur actions</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('actions');reloadFrameBlank('frmActions');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
				</div>
				<div class="bd" style="width:984px"><iframe id="frmActions" frameborder=0 src="" style="width:984px;height:512px;border:0px;"></iframe></div>
			</div>
		</div>
		<div id="formations" class="popup" style="width:984px">
			<div class="popupPanel" style="width:984px">
				<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Recherche sur formations</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formations');reloadFrameBlank('frmFormations');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
				</div>
				<div class="bd" style="width:984px"><iframe id="frmFormations" frameborder=0 src="" style="width:984px;height:512px;border:0px;"></iframe></div>
			</div>
		</div>
		<div id="bourses" class="popup" style="width:984px">
			<div class="popupPanel" style="width:984px">
				<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Recherche sur bourses</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('bourses');reloadFrameBlank('frmBourses');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
				</div>
				<div class="bd" style="width:984px"><iframe id="frmBourses" frameborder=0 src="" style="width:984px;height:512px;border:0px;"></iframe></div>
			</div>
		</div>
		<div id="formAddContact" class="popup">
			<div class="popupPanel">
				<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Ajouter un contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddContact');reloadFrameBlank('frmFormAddContact');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
				</div>
				<div class="bd"><iframe id="frmFormAddContact" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
			</div>
		</div>
	<?php
	}
	
	public function ContactList($elt=null, $closeCallBack = null)
	{
	?>
	<script language="javascript" type="text/javascript">
		function ContactListViewClass() {}
		ContactListViewClass.prototype.Close = function(action)
		{
			var ets = new Array();
			var chks = document.getElementsByTagName("input");
				
			if (action == 'select')
			{
				for (var i = 0; i < chks.length; i++)
				{
					if (chks[i].name.match("^chk_contact_"))
					{
						if (chks[i].checked)
						{
							var id = chks[i].name.replace("chk_contact_", "");
							var nom = document.getElementById("chk_contact_name_" + id).innerHTML;
							ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
						}
					}
				}
			}
			<?php if(isset($closeCallBack)) echo 'parent.' . $closeCallBack . "(ets, true);"; ?>
		}
		window.clv = new ContactListViewClass();

		function selectAll(selected)
		{
			var liste = document.getElementsByTagName("input");
			var re = new RegExp("^chk_contact_");

			for (var i = 0; i < liste.length; i++)
			{
				if (re.exec(liste[i].name)) liste[i].checked = selected;  
			} 
		}	
		function ShowContactInfo(contactId)
		{
			var frm = document.getElementById("frmContactDetail");
			url = "<?php echo $this->BuildURL('contactDetailPopup') . '&id='; ?>" + contactId;
			frm.src =url;
			Popup.showModal('contactDetail');
		}
		function ShowContactInfo2(contactId)
		{
			url = "<?php echo $this->BuildURL('contactDetail') . '&id='; ?>" + contactId;
			window.location.href  = url;
		}
	</script>
	<div id="contactDetail" class="popup" style="width:944px;">
		<div class="popupPanel" style="width:944px;">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:934px;">
					<tr>
						<td width="95%">D�tail du contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('contactDetail');reloadFrameBlank('frmContactDetail');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:944px;">
				<iframe id="frmContactDetail" frameborder=0 src="" style="width: 944px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>		
	<div class="boxGen" style="border:0px;">	
		<br />
			<div class="boxPan" style="padding: 5px;">
			<span style="padding-left: 5px;" class="title">Liste des contacts :&nbsp; <a class="resultLineRedirection" href="javascript:void(0);" onclick="selectAll(true);">tout s�lectionner</a> - <a class="resultLineRedirection" href="javascript:void(0);" onclick="selectAll(false);">tout d�selectionner</a></span>	
				<table  width = "98%" cellspacing="0"  cellpadding="2" border="0" style="margin-bottom: 4px; margin:10px;" class="result">
				<tbody>
					  <tr>					
						<td class="resultHeader" width="2%"></td>
						 <td class="resultHeader" width="14%">Nom - Pr�nom</td>
						 <td class="resultHeader" width="17%">Email</td>
						 <td class="resultHeader" width="0%">T�l�phone</td>
						 <td class="resultHeader" width="0%"></td>
					   </tr>
						
						<tr><td>&nbsp;</td></tr>
						<?php 
						if(isset($elt) && $elt->count()>0)
						{	
							for($i=0;$i<$elt->count(); $i++)
							{
								$adresse = $elt->items($i)->GetAdresse();	
							  	$class='class="trResult"';
								echo'<tr '.$class.'>';
								echo sprintf('<td><input type="checkbox" name="chk_contact_%s"></td>', $elt->items($i)->getContactId());
								
								if($_REQUEST['module']!='contactListPopup' && $_REQUEST['module']!='contactPopup') 
								{
									echo'<td class="resultLine resultLineRedirection" id="chk_contact_name_'.$elt->items($i)->getContactId().'" onclick="ShowContactInfo2(\''.$elt->items($i)->GetContactId().'\');">'. $elt->items($i)->GetNom().' '.$elt->items($i)->GetPrenom().'</td>';
								}
								elseif($_REQUEST['module']=='contactPopup')
								{
									echo'<td class="resultLine resultLineRedirection" id="chk_contact_name_'.$elt->items($i)->getContactId().'" onclick="ShowContactInfo(\''.$elt->items($i)->GetContactId().'\');">'. $elt->items($i)->GetNom().' '.$elt->items($i)->GetPrenom().'</td>';
								}
								else
									echo'<td class="resultLine" id="chk_contact_name_'.$elt->items($i)->getContactId().'">'. $elt->items($i)->GetNom().' '.$elt->items($i)->GetPrenom().'</td>';
									
								if($adresse->GetEmail1()!=null)
									echo'<td class="resultLine resultLineRedirection" onclick="javascript:window.location=\'mailto:' . $adresse->GetEmail1() .'\';">'. $adresse->GetEmail1().'</td>';
								else 
									echo'<td class="resultLine">/</td>';
								/*	
								if($adresse->GetEmail2()!=null)  
									echo'<td class="resultLine resultLineRedirection" onclick="javascript:window.location=\'mailto:' . $adresse->GetEmail2() .'\';">'. $adresse->GetEmail2().'</td>';
 							    else  
 							    	echo'<td class="resultLine">/</td>';
								*/
 							    if($adresse->GetTel1()!=null)	
									echo'<td class="resultLine">'. $adresse->GetTel1() . '</td>';
								else
									echo'<td class="resultLine">/</td>';
								/*
 							    if($adresse->GetTel2()!=null)	
									echo'<td class="resultLine">'. $adresse->GetTel2() . '</td>';
								else
									echo'<td class="resultLine">/</td>';									
								*/	
								echo '<td class="resultLine" ></td>
									</tr>';
							}					
						}
						else
							echo'<tr><td colspan="2"><i>Il n\'y a aucun r�sultat pour votre requ�te ...</i></td></tr>';
						?>				
					</tbody>							
				</table>
			</div>
			<?php if(isset($_REQUEST['action']) && $_REQUEST['action']=='contactList'){ ?>
			<div class="boxBtn">
					<table class="buttonTable">
						<tr>
							<td onclick="javascript:window.clv.Close('select');">Selectionner</td>
							<td class="separator"></td>
							<td onclick="javascript:window.clv.Close('cancel');">Annuler</td>
						</tr>
					</table>
				</div>
			<?php }?>
		</div>
	<?php
	}

	public function formContact($add=true, $lastIdContact = null, $boExitPopup=false)
	{
		
		$user = Session::GetInstance()->getCurrentUser();	
		$act = '';
		if($boExitPopup)
		{
			$act = '&exitPopup=ok';	
		}
		
	?>
	<div id="addContact"></div>
	
	<?php $action= 'add'; if($add!=true){$action= 'update';?>
	<span style="padding-left: 5px;" class="title">&gt;&gt; Modifier le contact</span>
	<br/><br/>
	<?php }?>
		<div class="boxGen">
		<form id="formContact" action="<?php echo $this->BuildURL($_REQUEST['module']).'&action='.$action.$act; ?>" method="post">
		<input name="ignoreDoublon" id="ignoreDoublon" type='hidden' value='0'>
		<input name="selectDoublon" id="selectDoublon" type='hidden' value='0'>
		<input name="origine_global_old" id="origine_global_old" type='hidden' value="<?php if(isset($_POST['origine_global_old'])) echo $_POST['origine_global_old']?>">
		<input name="origine_old" id="origine_old" type='hidden' value="<?php if(isset($_POST['origine_old'])) echo $_POST['origine_old']?>">
		<input name="idContact" id="idContact" type='hidden' value="<?php if(isset($_POST['idContact'])) echo $_POST['idContact']?>">
		<script type="text/javascript">
		
		function ShowDoublon(nom, prenom)
		{
			var frm = document.getElementById("frmDoublons");
			url = "<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=doublon'; ?>";
			url+="&nom="+nom+"&prenom="+prenom;
			frm.src = url;
			Popup.showModal('doublons');
		}
		
		function FormContactViewClass() {}

		FormContactViewClass.prototype.Close = function()
		{
			reloadFrameBlank('frmFormAddContact');
			parent.Popup.hide("formAddContact");
		}
		window.fcvc = new FormContactViewClass();
		
		
		function SetEtablissementsAddContact(objectsArray, close) 
		{		 
		  if (objectsArray.length > 0)		  
		  {
		  	var nbLignes = 0;
			var divGlobal = document.getElementById("divEtablissementsContact");	
			var tab = document.getElementById('tableEtablissementsContact');
			
			if(tab==null)
			{
				document.getElementById('etabIds').value = '';
 				tab = divGlobal.appendChild(document.createElement("table"));
 				tab.setAttribute("id","tableEtablissementsContact");
 				tab.setAttribute("style","width:80%");
			
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id",-1);
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>�tablissement</center>";
 			
 				newCell = newRow.insertCell(1);
 				newCell.innerHTML = "<center>Titre</center>";
 			
 				style='';
				<?php if($user->isOperateur()){ ?>
	 				style = "display:none";
	 			<?php } ?>
 				newCell = newRow.insertCell(2);
 				newCell.innerHTML = "<center>Personne ressource</center>";
 				newCell.setAttribute('style',style);
	 			newCell = newRow.insertCell(3);
 				newCell.innerHTML = "<center>Sp�cialit�</center>";
 			}
 			else
 			{
 				nbLignes = document.getElementById("nbLignes").value;
 			}	

			autoid = nbLignes;
			
		 	for(j=0;j<objectsArray.length;j++)
 			{
				var etabSelect = document.getElementById("etabIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;					
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);
	 				newRow.setAttribute("id",nbLignes);

					 for(i=0;i<5;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
 							case 0 :
 								tmp = '<input type="text" name="etabName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 									  '<input name="etabId_'+nbLignes+'" id="etabId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 								newCell.innerHTML = tmp;
 								break;
							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getTitreContactList());?>";
								newCell.innerHTML = "<select name='titre_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
							case 2 :
								style='';
								<?php if($user->isOperateur()){ ?>
	 								style = "display:none";
	 							<?php } ?>
	 							newCell.setAttribute('style',style);
						    	tmp = "<?php echo createSelectOptions(Dictionnaries::getParticipationList());?>";
								newCell.innerHTML = "<select name='participation_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
 							
	 						case 3 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getSpecialiteList());?>";
								newCell.innerHTML = "<select name='specialite_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
 							
 							case 4 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\""+nbLignes+"\", \"nbLignes\")'/>";
 								break; 							
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }
 			  }
 			  document.getElementById("nbLignes").value = nbLignes;
 			  
		   }
		    
			if (close)
			{ 
				document.getElementById("frmEtablissements").src = "about:blank"; 
				Popup.hide('etablissements');
			} 
		}
		
		function addRowGlobal(checkboxId)
		{	
				var divGlobal = document.getElementById("divEtablissementsContact");	
				var tab = document.getElementById('tableEtablissementsContact');
				var nbLignes = 0;
				checkbox = document.getElementById(checkboxId);
				if(checkbox.checked)//Add row
				{	
					if(tab==null)
					{
 						tab = divGlobal.appendChild(document.createElement("table"));
 						tab.setAttribute("id","tableEtablissementsContact");
 						tab.setAttribute("style","width:80%");
 						
			 			//	Premi�re ligne du tableau
 						newRow = tab.insertRow(-1);
		 				newRow.setAttribute("id",-1);
 						newCell = newRow.insertCell(0);
 						newCell.innerHTML = "<center>�tablissement</center>";
	 			
 						newCell = newRow.insertCell(1);
 						newCell.innerHTML = "<center>Titre</center>";
 						var style = '';
	 					<?php if($user->isOperateur()){ ?>
	 					style = "display:none";
	 					<?php } ?>
	 					
 						newCell = newRow.insertCell(2);
 						newCell.setAttribute('style',style);
 						newCell.innerHTML = "<center>Personne ressource</center>";
 						newCell = newRow.insertCell(3);
 						newCell.innerHTML = "<center>Sp�cialit�</center>";
 					}
 					else
 					{
 						nbLignes = tab.rows.length-1;
 					}	
	 			
 					newRow = tab.insertRow(-1);
		 			newRow.setAttribute("id","EtabGlobal");
 					
 					for(i=0;i<5;i++)
 		 			{
 		 	 			newCell = newRow.insertCell(i);
 		 	 			var tmp = "";
 						switch(i)
 						{
 							case 0 :
 								tmp = "<input type='text' style='width:200px'   value='Autre acteur' readonly='readonly' />"+
 							  		"<input name='etabId_global' type='hidden' value='Global' />";
 								newCell.innerHTML = tmp;	
 								break;
							case 1 :
					    		tmp = "<?php echo createSelectOptions(Dictionnaries::getTitreContactList());?>";					    		
								newCell.innerHTML = "<select name='titre_global' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
							case 2 :
								style='';
								 <?php if($user->isOperateur()){ ?>
	 								style="display:none";
	 							<?php } ?>

					    		tmp = "<?php echo createSelectOptions(Dictionnaries::getParticipationList());?>";
								newCell.innerHTML = "<select name='participation_global' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
								newCell.setAttribute('style',style);							
 								break;
	 							
 							case 3 :
						    	tmp = "<?php echo createSelectOptions(Dictionnaries::getSpecialiteList());?>";
								newCell.innerHTML = "<select name='specialite_global' style='width:175px;'>"+
												"<option value='-1'></option>"+tmp+"</select>";
 								break;	 						
 							case 4 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRowGlobal(\"EtabGlobal\",\"global_contact\")'/>";
 								break; 							
 						}	
 					
 						with(this.newCell.style)
						{
				 			width = '100px';
				 			textAlign = 'center';
				 			padding = '5px';
						} 
 		 			}
			}
			else
			{
				removeRowGlobal('EtabGlobal','global_contact');
			}
		}
		
		function SetActionContact(objectsArray, close)
		{
			
		  if (objectsArray.length > 0)		  
		  {
		  	var nbLignes = 0;
			var divGlobal = document.getElementById('divActionContact');			
			var tab = document.getElementById('tableActionsContact');
			
			
			if(tab==null)
			{
 				tab = divGlobal.appendChild(document.createElement('table'));
 				tab.setAttribute('id','tableActionsContact');
 				tab.setAttribute('style','width:80%');
 				document.getElementById("nbLignesAction").value = '0';
				document.getElementById('actionIds').value = '';
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute('id','headAction_-1');
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>Action</center>";
 				newCell = newRow.insertCell(1);
 				newCell.innerHTML = "<center>R�le</center>";
 				newCell = newRow.insertCell(2);
 				newCell.innerHTML = "";
 			}
 			else
 			{
 				nbLignes = document.getElementById("nbLignesAction").value;
 			}	

			autoid = nbLignes;
			
		 	for(j=0;j<objectsArray.length;j++)
 			{
				var actionSelect = document.getElementById("actionIds");
				var reg=new RegExp("[;]+", "g");
				tableActionSelect = actionSelect.value.split(reg);
				var isPresent = false;					
				for(var x=0; x<tableActionSelect.length; x++)
				{
					if(tableActionSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				
				if(!isPresent)
				{
					actionSelect.value += (actionSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);
	 				newRow.setAttribute('id','action_'+nbLignes);

					 for(i=0;i<5;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
							case 0 :
							    tmp = '<input type="text" name="actionName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />';
							    tmp += "<input name='actionId_"+nbLignes+"' id='actionId_"+nbLignes+"' type='hidden' value='"+objectsArray[j].id+"' />";
								newCell.innerHTML = tmp;
 								break;
 						
							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>";
								newCell.innerHTML = "<select name='roleAction_"+nbLignes+"' id='roleAction_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
 							case 2 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"action_"+nbLignes+"\", \"nbLignesAction\")'/>";
 								break; 							
 								
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }
 			  }
 			  document.getElementById("nbLignesAction").value = nbLignes;
 			  
		   }
			if (close)
			{
				 document.getElementById("frmActions").src = "about:blank";
				 Popup.hide("actions");
			} 
		}

		function SetFormationContact(objectsArray, close)
		{
			
		  if (objectsArray.length > 0)		  
		  {
		  	var nbLignes = 0;
			var divGlobal = document.getElementById('divFormationContact');			
			var tab = document.getElementById('tableFormationsContact');
			
			
			if(tab==null)
			{
 				tab = divGlobal.appendChild(document.createElement('table'));
 				tab.setAttribute('id','tableFormationsContact');
 				tab.setAttribute('style','width:80%');
 				document.getElementById("nbLignesFormation").value = '0';
				document.getElementById('formationIds').value = '';
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute('id','headFormation_-1');
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>Formation</center>";
 				newCell = newRow.insertCell(1);
 				newCell.innerHTML = "<center>R�le</center>";
 				newCell = newRow.insertCell(2);
 				newCell.innerHTML = "";
 			}
 			else
 			{
 				nbLignes = document.getElementById("nbLignesFormation").value;
 			}	

			autoid = nbLignes;
			
		 	for(j=0;j<objectsArray.length;j++)
 			{
				var actionSelect = document.getElementById("formationIds");
				var reg=new RegExp("[;]+", "g");
				tableActionSelect = actionSelect.value.split(reg);
				var isPresent = false;					
				for(var x=0; x<tableActionSelect.length; x++)
				{
					if(tableActionSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				
				if(!isPresent)
				{
					actionSelect.value += (actionSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);
	 				newRow.setAttribute('id','formation_'+nbLignes);

					 for(i=0;i<5;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
							case 0 :
							    tmp = '<input type="text" name="formationName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />';
							    tmp += "<input name='formationId_"+nbLignes+"' id='formationId_"+nbLignes+"' type='hidden' value='"+objectsArray[j].id+"' />";
								newCell.innerHTML = tmp;
 								break;
 						
							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>";
								newCell.innerHTML = "<select name='roleFormation_"+nbLignes+"' id='roleFormation_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
 							case 2 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"formation_"+nbLignes+"\", \"nbLignesFormation\")'/>";
 								break; 							
 								
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }
 			  }
 			  document.getElementById("nbLignesFormation").value = nbLignes;
 			  
		   }
			if (close)
			{
				 document.getElementById("frmFormations").src = "about:blank";
				 Popup.hide("formations");
			} 
		}
			
		function SetBouPiContact(objectsArray, close)
		{
		  if (objectsArray.length > 0)		  
		  {
		  	var nbLignes = 0;
			var divGlobal = document.getElementById('divBourseContact');			
			var tab = document.getElementById('tableBoursesContact');
			
			
			if(tab==null)
			{
 				tab = divGlobal.appendChild(document.createElement('table'));
 				tab.setAttribute('id','tableBoursesContact');
 				tab.setAttribute('style','width:80%');
				document.getElementById('bourseIds').value = '';
				document.getElementById("nbLignesBourse").value = '0';
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute('id','headBourse_-1');
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>Projet innovant</center>";
 				newCell = newRow.insertCell(1);
 				newCell.innerHTML = "<center>R�le</center>";
 				newCell = newRow.insertCell(2);
 				newCell.innerHTML = "";
 			}
 			else
 			{
 				nbLignes = document.getElementById("nbLignesBourse").value;
 			}	

			autoid = nbLignes;
			
		 	for(j=0;j<objectsArray.length;j++)
 			{
				var actionSelect = document.getElementById("bourseIds");
				var reg=new RegExp("[;]+", "g");
				tableActionSelect = actionSelect.value.split(reg);
				var isPresent = false;					
				for(var x=0; x<tableActionSelect.length; x++)
				{
					if(tableActionSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				
				if(!isPresent)
				{
					actionSelect.value += (actionSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);
	 				newRow.setAttribute('id','bourse_'+nbLignes);

					 for(i=0;i<5;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
							case 0 :
								
							    tmp = '<input type="text" name="bourseName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />';
							    tmp += "<input name='bourseId_"+nbLignes+"' id='bourseId_"+nbLignes+"' type='hidden' value='"+objectsArray[j].id+"' />";
								newCell.innerHTML = tmp;
 								break;
 						
							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>";
								newCell.innerHTML = "<select name='roleBourse_"+nbLignes+"' id='roleBourse_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
 							case 2 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"bourse_"+nbLignes+"\",\"nbLignesBourse\")'/>";
 								break; 							
 								
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }
 			  }
 			  document.getElementById("nbLignesBourse").value = nbLignes;
 			  
		   }		
			if (close)
			{
				 document.getElementById("frmBourses").src = "about:blank";
				 Popup.hide("bourses");
			}
		}

		function submitFormContact(idForm, action)
		{
			res1 = true; 
			var nom = document.getElementById("nom_contact");
			var prenom = document.getElementById("prenom_contact");
			var email1 = document.getElementById("mail1");
			var email2 = document.getElementById("mail2");
			var tel1 = document.getElementById("tel1");
			var tel2 = document.getElementById("tel2");
			var global = document.getElementById("global_contact");
			var genre = document.getElementById("genre");
			var age= document.getElementById("age");
			//var statut= document.getElementById("statut");
						
			var chk = new CheckFunction();
			chk.Add(nom, 'TEXT', true); chk.Add(prenom, 'TEXT', true);
			chk.Add(email1, 'EMAIL', false); chk.Add(email2, 'EMAIL', false);
			chk.Add(tel1, 'PHONE', false); chk.Add(tel2, 'PHONE', false);


			//ContactAction
			var nbLigneAction = document.getElementById('nbLignesAction');
			for(i=0; i<nbLigneAction.value; i++)
			{
				var role = document.getElementById('roleAction_'+i);
				if(role)
				{
					if(role.value == '-1')
					{
						SetInputError(role);
						res1 = false;
					}
				}
			}
			//ContactBourse
			var nbLigneBourse = document.getElementById('nbLignesBourse');
			for(i=0; i<nbLigneBourse.value; i++)
			{
				var role = document.getElementById('roleBourse_'+i);
				if(role)
				{
					if(role.value=='-1')
					{
						SetInputError(role);
						res1 = false;
					}
				}
			}

			var nbLignes = document.getElementById('nbLignes');
			
			var nb=0;
			for(i=0; i<nbLignes.value; i++)
			{
				var etabId = document.getElementById('etabId_'+i);
				if(etabId)
				{
					nb++;	
				}
			}
			
			if(nb==0)
			{
				if(global.checked==false)
				{
					alert("Veuillez associer le contact � un/des �tablissement(s) ou mentionnez le contact comme �tant global");
					res1=false;
				}
			}


			if(genre.value == '-1')
			{
				SetInputError(genre);
				res1 = false;
			}
			else { SetInputValid(genre);}

			if(age.value == '-1')
			{
				SetInputError(age);
				res1 = false;
			}
			else {SetInputValid(age);}

			/*
			if(statut.value == '-1')
			{
				SetInputError(statut);
				res1 = false;
			}
			else {SetInputValid(statut); }	
			*/
			
			var res = chk.IsValid();
			if (res && res1)
			{
				
				if(action!='update')
				{
					jx.load('./index.php?module=ajax&doublon=1&nom='+nom.value+'&prenom='+prenom.value,
					function(data)
					{	//alert(data);
						if(data=='1')
						{
							ShowDoublon(nom.value, prenom.value);
						}
						else
						{
							submitForm(idForm);
						}
							
					},'text');
				}
				else
				{
					//update
					submitForm(idForm);
				}
			}
			else return false;
		}		
		</script>	
			<div class="boxPan" style="padding: 5px;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<colgroup>
							<col width="175"/>
							<col width="10"/>
							<col width="150"/>
							<col width="70" />
							<col width="175" />
							<col width="10" />
							<col width="*"/>
						</colgroup>
						<tr>
							<td height="20">Nom</td>
							<td>:</td>
							<td><input size="40" maxlength="100" class ="notauto" type="text" name="nom_contact" id = "nom_contact" value="<?php if(isset($_POST["nom_contact"])) echo ToHTML($_POST["nom_contact"]) ?>"/></td>
							<td><input type="hidden" name = "contact_id" value="<?php if(isset($_POST["contact_id"])) echo $_POST['contact_id']?>"></td>
							<td height="20">Pr�nom</td>
							<td>:</td>
							<td><input  size="40" maxlength="100" class ="notauto" type="text" name="prenom_contact" id = "prenom_contact" value="<?php if(isset($_POST["prenom_contact"])) echo ToHTML($_POST["prenom_contact"]) ?>"/></td>
						</tr>
						<tr>
							<td height="20">T�l�phone</td>
							<td>:</td>
							<td><input  size="40" maxlength="100" class ="notauto" type="text" name="tel1" id="tel1" value="<?php if(isset($_POST["tel1"])) echo ToHTML($_POST["tel1"]) ?>"/></td>
							<td><input type="hidden" name = "adr_id" value="<?php if(isset($_POST["adr_id"])) echo $_POST['adr_id']?>"></td>
							<td height="20">T�l�phone(2)</td>
							<td>:</td>
							<td><input  size="40" maxlength="100" class ="notauto" type="text" name="tel2" id="tel2" value="<?php if(isset($_POST["tel2"])) echo ToHTML($_POST["tel2"]) ?>"/></td>
						</tr>
						<tr>
							<td height="20">Email</td>
							<td>:</td>
							<td><input  size="40" maxlength="100" class ="notauto" type="text" name="mail1" id="mail1" value="<?php if(isset($_POST["mail1"])) echo ToHTML($_POST["mail1"]) ?>"/></td>
							<td></td>
							<td height="20">Email(2)</td>
							<td>:</td>
							<td><input  size="40" maxlength="100" class ="notauto" type="text" name="mail2" id="mail2" value="<?php if(isset($_POST["mail2"])) echo ToHTML($_POST["mail2"]) ?>"/></td>
						</tr>
						<tr>
							<td height="20">Genre</td>
							<td>:</td>
							<td>
								<select id="genre" name="genre" style="width: 245px;">
								<?php
									echo '<option value="-1"></option>';
									if (isset($_POST['genre'])) {
										echo createSelectOptions(Dictionnaries::getGenreList(), $_POST['genre']);			
									}
									else {
										echo createSelectOptions(Dictionnaries::getGenreList());	
									}
										?>
								</select>							
							</td>
							<td></td>
							<td height="20">Tranche d'�ge</td>
							<td>:</td>
							<td>
								<select id="age" name="age" style="width: 245px;">
								<?php
									echo '<option value="-1"></option>';
									if (isset($_POST['age'])) {
										echo createSelectOptions(Dictionnaries::getAgeList(), $_POST['age']);
									}
									else {
										echo createSelectOptions(Dictionnaries::getAgeList());
									}
								?>
								</select>							
							</td>
						</tr>						
						<tr>
						<!-- 
							<td height="20">Statut</td>
							<td>:</td>
							<td>
								<select id="statut" name="statut" style="width: 245px;">
								<?php
									echo '<option value="-1"></option>';
									if (isset($_POST['statut'])) {
										echo createSelectOptions(Dictionnaries::getStatutList(), $_POST['statut']);
									}
									else {
										echo createSelectOptions(Dictionnaries::getStatutList());
									}
								?>
								</select>							
							</td>
							<td></td>
						 -->	
							<td height="20">Autre acteur</td>
							<td>:</td>
							<td><input type="checkbox" <?php if (isset($_POST["global_contact"])) echo 'checked="checked"'; ?> name="global_contact" id="global_contact" class="notauto" onclick="addRowGlobal('global_contact')" value='1'></td>
						</tr>							
					</table>
					<!-- Etablissement -->
					<br />
					<div>
						<u>Associer � un/des �tablissement(s) et d�crire le r�le</u> &nbsp;&nbsp;
						<img src="./styles/img/etablissement.png" id="searchEtablissements" onclick="ShowEtablissement('&close=SetEtablissementsAddContact');return false" class='expand'/>
						<input type="hidden" id="etabIds" name="etabIds" value="<?php echo !isset($_POST['etabIds']) ? "" : $_POST['etabIds']; ?>">
						<?php $nbL=0; if(isset($_POST['nbLignes'])) $nbL = $_POST['nbLignes'];?>
						<input name='nbLignes' id='nbLignes' type='hidden' value='<?php echo $nbL ?>' />
					</div>
					<br/>
					<div id="divEtablissementsContact">
						<?php 
							if((isset($_POST['nbLignes']) && $_POST['nbLignes']>0) || (isset($_POST['global_contact']) && $_POST['global_contact']=="1" ) )
							{
								$this->createTableEtablissementContact();
							}
						?>
					</div>
					<br/>
					
					<!-- action -->
					<div>
						<u>Associer � une/des action(s)</u> &nbsp;&nbsp;
						<img src="./styles/img/folder.gif" id="searchActions" onclick="ShowAction('&close=SetActionContact');return false" class='expand'/>
						<input type="hidden" id="actionIds" name="actionIds" value="<?php echo !isset($_POST['actionIds']) ? "" : $_POST['actionIds']; ?>">
						<input name='nbLignesAction' id='nbLignesAction' type='hidden' value='<?php echo !isset($_POST['nbLignesAction']) ? "" : $_POST['nbLignesAction']; ?>' />
					</div>
					<br/>
					<div id="divActionContact">
						<?php 

							if(isset($_POST['nbLignesAction']) && $_POST['nbLignesAction']>0) 
							{
								$this->createTableActionContact();
							}
						?>
					</div>
					<br/>
					<!-- formation -->
					<div>
						<u>Associer � une/des formation(s)</u> &nbsp;&nbsp;
						<img src="./styles/img/folder.gif" id="searchFormations" onclick="ShowFormation('&close=SetFormationContact');return false" class='expand'/>
						<input type="hidden" id="formationIds" name="formationIds" value="<?php echo !isset($_POST['formationIds']) ? "" : $_POST['formationIds']; ?>">
						<input name='nbLignesFormation' id='nbLignesFormation' type='hidden' value='<?php echo !isset($_POST['nbLignesFormation']) ? "" : $_POST['nbLignesFormation']; ?>' />
					</div>
					<br/>
					<div id="divFormationContact">
						<?php 

							if(isset($_POST['nbLignesFormation']) && $_POST['nbLignesFormation']>0) 
							{
								// todo : change in formationContact
								$this->createTableFormationContact();
							}
						?>
					</div>
					<br/>
					<!-- Projet innovant -->
					<div>
						<u>Associer � un/des projet(s) innovant(s)</u> &nbsp;&nbsp;
						<img src="./styles/img/create_space.gif" id="searchBourse" onclick="ShowBourse('&close=SetBouPiContact');return false" class='expand'/>
						<input type="hidden" id="bourseIds" name="bourseIds" value="<?php echo !isset($_POST['bourseIds']) ? "" : $_POST['bourseIds']; ?>">
						<input name='nbLignesBourse' id='nbLignesBourse' type='hidden' value='<?php echo !isset($_POST['nbLignesBourse']) ? "" : $_POST['nbLignesBourse']; ?>' />
					</div>
					<br/>
					<div id="divBourseContact">
						<?php 
							if(isset($_POST['nbLignesBourse']) && $_POST['nbLignesBourse']>0)
							{
								$this->createTableBourseContact();
							}
						?>					
					</div>
					<br/>
			</div>
			<div class="boxBtn">
			<?php if($add==true) echo '<input type="hidden" name="addForm"/>'; else echo '<input type="hidden" name="updateForm"/>'?>
					<table class="buttonTable">
						<tr>
						<?php if($add==true){?> 
							<td onclick="submitFormContact('formContact', 'add')" >Ajouter</td><td class="separator"></td>
						<?php }else{ ?>
							<td onclick="submitFormContact('formContact', 'update')" >Modifier</td><td class="separator"></td>
						<?php }?>
							<td onclick="clearForm('formContact')">Effacer</td>
						<?php if($_REQUEST['module']=='contactPopup'){?>
								<td class="separator"></td>
								<td onclick="javascript:window.fcvc.Close();">Fermer</td>
						<?php }?>							
						</tr>
					</table>
				</div>
			</form>
		</div>
		
		<div id="doublons" class="popup" style="width:984px">
			<div class="popupPanel" style="width:984px">
				<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Doublons trouv�s</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('doublons');reloadFrameBlank('frmDoublons');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
				</div>
				<div class="bd" style="width:984px"><iframe id="frmDoublons" frameborder=0 src="" style="width:984px;height:512px;border:0px;"></iframe></div>
			</div>
		</div>
	<?php	
	if($boExitPopup && isset($_POST['addForm']))
	{
	?>
	<script>
		var frm = parent.document.getElementById("frmFormAddContact");
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add&exitPopup=ok' ?>';
		parent.Popup.hide("formAddContact");
	</script>
	<?php 
	}elseif(isset($lastIdContact))
	{
	?>
	<script>
		url = "<?php echo SCRIPTPATH . 'index.php?module=contactDetail&id='.$lastIdContact; ?>";
		parent.window.location.href = url;
		window.fcvc.Close();
	</script>
<?php }
	
	
	}

	
	public function createTableEtablissementContact()
	{
	$user = Session::GetInstance()->getCurrentUser();
		if(isset($_POST['global_contact']) || (isset($_POST['nbLignes']) && $_POST['nbLignes']>0)){
	?>
		<table id="tableEtablissementsContact" style="width: 50%;">
			<tbody>
				<tr id="-1">
					<td><center>�tablissement</center></td>
					<td><center>Titre</center></td>
					<?php if(!$user->isOperateur()){ ?>
					<td><center>Personne ressource</center></td>
					<?php } ?>
					<td><center>Sp�cialit�</center></td>
					<td />
				</tr>
	
	<?php }if(isset($_POST['global_contact'])){?>
				<tr id="EtabGlobal">
					<td style="padding: 5px; width: 100px; text-align: center;">
						<input type="text" readonly="readonly" value="Autre acteur" style="width: 200px;"/>
						<input type="hidden" value="Global" name="etabId_global"/>
					</td>
					<td style="padding: 5px; width: 100px; text-align: center;">
						<select name="titre_global" style="width:175px;">
							<option value="-1"></option>
							<?php echo createSelectOptions(Dictionnaries::getTitreContactList(), $_POST['titre_global']);?>
						</select> 
					</td>
					<?php if(!$user->isOperateur()){ ?>
					<td style="padding: 5px; width: 100px; text-align: center;">
						<select name="participation_global" style="width:175px;">
							<option value="-1"></option>
							<?php echo createSelectOptions(Dictionnaries::getParticipationList(), $_POST['participation_global']);?>
						</select> 
					</td>
					<?php } ?>
					<td style="padding: 5px; width: 100px; text-align: center;">
						<select name="specialite_global" style="width:175px;">
							<option value="-1"></option>
							<?php echo createSelectOptions(Dictionnaries::getSpecialiteList(), $_POST['specialite_global']);?>
						</select> 
					
					</td>
					<td style="padding: 5px; width: 100px; text-align: center;"> 
						<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRowGlobal('EtabGlobal','global_contact')"/>
					</td>				
		
				</tr>			
	<?php } ?>	
	<?php 
		if(isset($_POST['nbLignes'])){
			for($i=0; $i< $_POST['nbLignes'];$i++)
			{
				if(isset($_POST['etabName_'.$i])){
	?>		
					<tr id="<?php echo $i; ?>">
					<td style="padding: 5px; width: 100px; text-align: center;">
						<input type="text" name="<?php echo 'etabName_'.$i; ?>" id="<?php echo 'etabName_'.$i; ?>" readonly="readonly" value="<?php echo ToHTML( $_POST['etabName_'.$i]); ?>" style="width: 200px;"/>
						<input type="hidden" value="<?php echo $_POST['etabId_'.$i]; ?>" name="<?php echo 'etabId_'.$i; ?>" id="<?php echo 'etabId_'.$i; ?>"/>
					</td>
					<td style="padding: 5px; width: 100px; text-align: center;">
						<select name="<?php echo 'titre_'.$i; ?>" style="width:175px;">
							<option value="-1"></option>
							<?php echo createSelectOptions(Dictionnaries::getTitreContactList(), $_POST['titre_'.$i]);?>
						</select> 
					</td>
					<?php if(!$user->isOperateur()){ ?>
					<td style="padding: 5px; width: 100px; text-align: center;">
						<select name="<?php echo 'participation_'.$i; ?>" style="width:175px;">
							<option value="-1"></option>
							<?php echo createSelectOptions(Dictionnaries::getParticipationList(), $_POST['participation_'.$i]);?>
						</select> 
					</td>					
					<?php } ?>
					<td style="padding: 5px; width: 100px; text-align: center;">
						<select name="<?php echo 'specialite_'.$i; ?>" style="width:175px;">
							<option value="-1"></option>
							<?php echo createSelectOptions(Dictionnaries::getSpecialiteList(), $_POST['specialite_'.$i]);?>
						</select>
					</td>
					<td style="padding: 5px; width: 100px; text-align: center;"> 
						<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRow(<?php echo $i; ?>,'nbLignes')"/>
					</td>						
				</tr>			
	<?php 		
			}
		}
	 } ?>	
			</tbody>
		</table>
	<?php
	}
	
	public function createTableActionContact()
	{
		$idTable = 'tableActionsContact';
		$idHeadTr = 'headAction_-1';
		$headLabel='Action';
		$nbLignes = $_POST['nbLignesAction'];
		
		
	?>
		<table id="<?php echo $idTable; ?>" style="width: 50%;">
			<tbody>
				<tr id="<?php echo $idHeadTr; ?>">
					<td><center><?php echo ToHTML($headLabel); ?></center></td>
					<td><center>R�le</center></td>
				</tr>
			
		<?php
		for($i=0; $i<$nbLignes; $i++)
		{
			if(isset($_POST['actionName_'.$i]))
			{
				echo '<tr id="action_'.$i.'"><td style="padding: 5px; width: 100px; text-align: center;">';
				echo '<input type="text" readonly="readonly" value="'.$_POST['actionName_'.$i].'" style="width: 200px;" name="actionName_'.$i.'"/>';
				echo '<input type="hidden" value="'.$_POST['actionId_'.$i].'" id="actionId_'.$i.'" name="actionId_'.$i.'"/>';
				echo '</td><td style="padding: 5px; width: 100px; text-align: center;">';
				echo'<select name="roleAction_'.$i.'" id="roleAction_'.$i.'" style="width:175px;">';
				echo'<option value="-1"></option>';
				echo createSelectOptions(Dictionnaries::getRoleContactActionList(), $_POST['roleAction_'.$i]);
				echo'</select></td>';
				echo '<td style="padding: 5px; width: 100px; text-align: center;">'; 
				echo '<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRow(\'action_'.$i.'\', \'nbLignesAction\')"/>';
				echo '</td>';						
				
				echo'</tr>'; 
			}		
		}
		?>	
		</tbody>
	</table>
	
	<?php
	}
	
	public function createTableFormationContact()
	{
		$idTable = 'tableFormationsContact';
		$idHeadTr = 'headFormation_-1';
		$headLabel='Formation';
		$nbLignes = $_POST['nbLignesFormation'];
		
		
	?>
		<table id="<?php echo $idTable; ?>" style="width: 50%;">
			<tbody>
				<tr id="<?php echo $idHeadTr; ?>">
					<td><center><?php echo ToHTML($headLabel); ?></center></td>
					<td><center>R�le</center></td>
				</tr>
			
		<?php
		for($i=0; $i<$nbLignes; $i++)
		{
			if(isset($_POST['actionName_'.$i]))
			{
				echo '<tr id="formation_'.$i.'"><td style="padding: 5px; width: 100px; text-align: center;">';
				echo '<input type="text" readonly="readonly" value="'.$_POST['formationName_'.$i].'" style="width: 200px;" name="formationName_'.$i.'"/>';
				echo '<input type="hidden" value="'.$_POST['formationId_'.$i].'" id="formationId_'.$i.'" name="formationId_'.$i.'"/>';
				echo '</td><td style="padding: 5px; width: 100px; text-align: center;">';
				echo'<select name="roleFormation_'.$i.'" id="roleFormation_'.$i.'" style="width:175px;">';
				echo'<option value="-1"></option>';
				echo createSelectOptions(Dictionnaries::getRoleContactActionList(), $_POST['roleFormation_'.$i]);
				echo'</select></td>';
				echo '<td style="padding: 5px; width: 100px; text-align: center;">'; 
				echo '<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRow(\'formation_'.$i.'\', \'nbLignesFormation\')"/>';
				echo '</td>';						
				
				echo'</tr>'; 
			}		
		}
		?>	
		</tbody>
	</table>
		
	<?php
	}
	
	public function createTableBourseContact()
	{
		$idTable = 'tableBoursesContact';
		$idHeadTr = 'headBourse_-1';
		$headLabel='Projet innovant';
		$nbLignes = $_POST['nbLignesBourse'];
		 
	?>
		<table id="<?php echo $idTable; ?>" style="width: 30%;">
			<tbody>
				<tr id="<?php echo $idHeadTr; ?>">
					<td><center><?php echo ToHTML($headLabel); ?></center></td>
					<td><center>R�le</center></td>
				</tr>
			
		<?php
		for($i=0; $i<$nbLignes; $i++)
		{
			if(isset($_POST['bourseName_'.$i]))
			{
				echo '<tr id="bourse_'.$i.'"><td style="padding: 5px; width: 100px; text-align: center;">';
				echo '<input type="text" readonly="readonly" value="'.$_POST['bourseName_'.$i].'" style="width: 200px;" name="bourseName_'.$i.'"/>';
				echo '<input type="hidden" value="'.$_POST['bourseId_'.$i].'" id="bourseId_'.$i.'" name="bourseId_'.$i.'"/>';
				echo '</td><td style="padding: 5px; width: 100px; text-align: center;">';
				echo'<select name="roleBourse_'.$i.'" id="roleBourse_'.$i.'" style="width:175px;">';
				echo'<option value="-1"></option>';
				echo createSelectOptions(Dictionnaries::getRoleContactActionList(), $_POST['roleBourse_'.$i]);
				echo'</select></td>';
				echo '<td style="padding: 5px; width: 100px; text-align: center;">'; 
				echo '<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRow(\'bourse_'.$i.'\', \'nbLignesBourse\')"/>';
				echo '</td>';						
				echo'</tr>'; 
			}		
		}
		?>
		</tbody>
	</table>
	<?php 
	}
	
	public function doublonContacts($doublonContacts)
	{
	?>
	
	<script>
		function ShowContactInfo(contactId)
		{

			var frm = document.getElementById("frmContactDetail");
			url = '<?php echo $this->BuildURL('contactDetailPopup') . '&id='; ?>' + contactId;
			frm.src =url;
			Popup.showModal("contactDetail");
		}
		function ignoreDoublon()
		{
			parent.document.getElementById('ignoreDoublon').value="1";
			parent.document.getElementById('formContact').submit();
		}		
	</script>
	<div id="contactDetail" class="popup" style="width:944px">
		<div class="popupPanel" style="width:944px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:934px">
					<tr>
						<td width="95%">D�tail du contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('contactDetail');reloadFrameBlank('frmContactDetail');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:944px">
				<iframe id="frmContactDetail" frameborder=0 src="" style="width: 944px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
		<!--<span style="padding-left: 5px;" class="title">&gt;&gt; Gestion des doublons</span> -->
		<div class="boxGen">
		<br />
			<form>
				<div class="boxPan" style="padding:5px;">
				  <table  width = "99%;" cellspacing="0"  cellpadding="2" border="0" style="margin-bottom: 4px; margin:10px;">
					<thead>
					  <tr style="border-bottom : 1px black solid">
					  	 <!-- <th class="thResult" width="3%"></th>  -->					
						 <th class="thResult" width="15%">Nom - Pr�nom</th>
						 <th class="thResult" width="21%">Email (1)</th>
						 <th class="thResult" width="21%">Email (2)</th>
						 <th class="thResult" width="16%">T�l�phone (1)</th>
						 <th class="thResult" width="16%">T�l�phone (2)</th>
						 <th class="thResult" width="*"></th>
					   </tr>
					</thead>
					<tbody>			
						<tr> 
							<td>&nbsp;</td><td>&nbsp;</td> <td>&nbsp;</td>
						</tr>
						<?php
						for($i=0; $i<count($doublonContacts); $i++)
						{

							$contact = $doublonContacts[$i]; 
							$adr = $contact->GetAdresse();
							echo '<tr>';
								//echo '<td><input type=\'radio\' value=\''.$contact->GetContactId().'\' name =\'idContact\'></td>';
								echo sprintf('<td class="resultLine" onclick="ShowContactInfo(\''.$contact->GetContactId().'\');">%s</td>', '<a href=\'javascript:void(0);\'>'. $contact->GetNom().' '.$contact->GetPrenom().'</a>');
								echo sprintf('<td class="resultLine">%s</td>', $adr ->GetEmail1());
								echo sprintf('<td class="resultLine">%s</td>', $adr ->GetEmail2());
								echo sprintf('<td class="resultLine">%s</td>', $adr ->GetTel1());
								echo sprintf('<td class="resultLine">%s</td>', $adr ->GetTel2());
								echo '<td></td>';
							echo '</tr>';		
						}
						?>
						</tbody>
					</table>
				</div>	
				<div class="boxBtn">
					<input type="hidden" name="searchContact"/>
					<i>
					 Ignorer les doublons et cr�er le nouveau contact : [Valider ]<br/>
					 Un doublon correspond. Pour annuler la cr�ation, revenir au formulaire d'ajo�t que vous pourrez fermer : [ Retour ]<br/>
					</i>					 
					<table class="buttonTable">
						<tr>
							<td onclick="javascript:ignoreDoublon();">Valider</td>
							<td class="separator"></td>							
							<td onclick="document.getElementById('frmContactDetail').src = 'about:blank'; parent.Popup.hide('doublons');">Retour</td>
						</tr> 
					</table>
				</div>
			</form>
		</div>
	<?php
	}
}
?>