<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class EntiteAdminDetailView extends BaseView
{
	private $user = null;
	
	public function Render($entite)
	{
		$this->user = Session::GetInstance()->getCurrentUser();		
		$adr = $entite->getAdresse();
		$cp = $adr->GetCodepostal(); 
?>
	<script>
	function ShowFormAddEntiteAdmin()
	{
		
		var frm = document.getElementById("frmFormAddEntiteAdmin");		
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=entiteAdminPopup&action=add';?>';
		Popup.showModal('formAddEntiteAdmin');		
	}
	function updateEntiteAdmin(id)
	{
		document.location.href = '<?php echo $this->BuildURL('entiteAdmin') .'&action=update&updateId='; ?>' + id;
	}

	function deleteEntiteAdmin(id)
	{
		if (sure("Supprimer l\'entit� administrative ?") == true)	
			parent.document.location.href = '<?php echo $this->BuildURL('entiteAdmin') .'&action=delete&deleteId='; ?>' + id;
	}
	
	</script>
	<div id="formAddEntiteAdmin" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er une entit� administrative</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddEntiteAdmin');reloadFrameBlank('frmFormAddEntiteAdmin');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddEntiteAdmin" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=entiteAdminPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div class="boxGen">
		<div class="boxPan" style="padding:5px;">
			<form id="ficheSuiviDetail" action="<?php echo $this->BuildURL($_REQUEST['module']) . '&id=' . $entite->getEntiteAdminId(); ?>" method="post">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td width="50%" style="vertical-align:top;">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
								<tr>
									<td class="label">Nom :</td>
									<td class="detail"><?php echo $entite->getNom(); ?></td>
								</tr>
								<tr>
									<td class="label">Statut :</td>
										<?php
											if ($entite->GetSwActif() == 1) echo '<td style="color: Green;" class="detail">Actif</td>';
											else echo '<td style="color: red;" class="detail">Inactif</td>';
										?>									
								</tr>
								
								<tr>
									<td class="label">Adresse :</td>
									<td class="detail"><?php echo ToHTML($adr->getRue())?></td>
								</tr>
								<tr>
									<td class="label">Code postal/Localit� :</td>
									<td class="detail"><?php echo ToHTML($cp->GetCodepostal()).' '.ToHTML($adr->GetVille());?></td>
								</tr>	
								<tr>
									<td class="label">Arrondissement/Province :</td>
									<td class="detail"><?php echo ToHTML($cp->GetArrondissement()->getLabel()).' / '.ToHTML($cp->GetProvince()->getLabel());?></td>
								</tr>	
								<tr>
									<td class="label">Cr�ation :</td>
									<td class="detail">
									<?php
										$user  = $entite->GetCreatedByUser();
										echo 'le '.$entite->GetDateUpdateString(false). ' par '. $user->GetNom().' '.$user->GetPrenom();
			
									?>
									</td>
								</tr>																														
							</table>
						</td>
						<td width="50%" style="vertical-align:top;">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
								<tr>
									<td class="label">Identification :</td>
									<td class="detail"><?php echo $entite->getEntiteAdminId(); ?></td>
								</tr>
								<tr>
									<td class="label">Site internet :</td>
									<td class="detail"><?php echo $entite->GetWebsiteAsAnchor();?></td>
								</tr>
								<tr>
									<td class="label">T�l�phone(s) :</td>
									<td class="detail">
										<?php
											$tel1 = $adr->GetTel1();
											$tel2 = $adr->GetTel2();
			
											$tel = null;
											
											if (isset($tel1) && isset($tel2)) $tel = sprintf('%s / %s', $tel1, $tel2);
											elseif (isset($tel1)) $tel = $tel1;
											elseif (isset($tel2)) $tel = $tel2;
											
											echo $tel;
										?>									
									</td>
								</tr>
								<tr>
									<td class="label">T�l�copie :</td>
									<td class="detail"><?php echo $adr->GetFax();?></td>
								</tr>	
								<tr>
									<td class="label">Courriel(s):</td>
									<td class="detail">
										<?php
											$email1 = $adr->GetEmail1AsAnchor();
											$email2 = $adr->GetEmail2AsAnchor();
			
											$email = null;
											
											if (isset($email1) && isset($email2)) $email = sprintf('%s / %s', $email1, $email2);
											elseif (isset($email1)) $email = $email1;
											elseif (isset($email2)) $email = $email2;
											
											echo $email;
										?>									
									</td>
								</tr>	
								<tr>
									<td class="label">Modification :</td>
									<td class="detail">
									<?php
										$user  = $entite->GetUpdatedByUser();
										echo 'le '.$entite->GetDateUpdateString(false).' par '.$user->GetNom().' '.$user->GetPrenom();
									?>
									</td>
								</tr>																																														
							</table>
						</td>
					</tr>
				</table>


						<script>
							function ActionDetailViewClass() 
							{
								this.current = 'All';
								this.tabs = new Array('Etablissement2');
							}
	
							ActionDetailViewClass.prototype.onMouseOver = function(liID)
							{
								document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
								document.getElementById("spn" + liID).style.color = "#FFFFFF";
								document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
							}
	
							ActionDetailViewClass.prototype.onMouseOut = function(liID)
							{
								if (this.current != liID)
								{
									document.getElementById("spn" + liID).style.backgroundPosition = "100% 0%";
									document.getElementById("spn" + liID).style.color = "#737373";
									document.getElementById("a" + liID).style.backgroundPosition = "0% 0%";
								}
							}
							
							ActionDetailViewClass.prototype.onLIClick = function(liID)
							{
								this.current = liID;
								
								for (var i = 0; i < this.tabs.length; i++)
								{
									document.getElementById("spn" + this.tabs[i]).style.backgroundPosition = "100% 0%";
									document.getElementById("spn" + this.tabs[i]) .style.color = "#737373";
									document.getElementById("a" + this.tabs[i]).style.backgroundPosition = "0% 0%";
								}
	
								document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
								document.getElementById("spn" + liID).style.color = "#FFFFFF";
								document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
								
								var contact = document.getElementById('Contact');
								var etablissement = document.getElementById('Etablissement');
								
								if (liID == 'Etablissement2')
								{
									if (contact) contact.style.display = '';
									if (etablissement) etablissement.style.display = '';
								}
							}
							window.edvc = new ActionDetailViewClass();
							
							function ShowEtablissementInfo(id)
							{
								window.location = '<?php echo $this->BuildURL('etablissementDetail') . '&id='; ?>' + id;
							}
					</script>
			
					<div class="header" style="border-bottom:3px solid #EAC3C3;">
						<ul>
							<li OnMouseOver="javascript:window.edvc.onMouseOver('Etablissement2');" OnMouseOut="javascript:window.edvc.onMouseOut('Etablissement2');"><a id="aEtablissement2" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Etablissement2');"><span id="spnEtablissement2" class="expand">Etablissements d'enseignement</span></a></li>
						</ul>
					</div>
					<script>
						document.getElementById("spnEtablissement2").style.backgroundPosition = "100% -250px";
						document.getElementById("spnEtablissement2").style.color = "#FFFFFF";
						document.getElementById("aEtablissement2").style.backgroundPosition = "0% -250px";
					</script>
					<?php 
					$etabs = $entite->getEtablissements();
					?>
						
					<div style="margin-top:5px; width:100%;">
						<div id="Etablissement">
							<div class="resultDetailHeader" style="width:275px;"><span class="title">Etablissements d'enseignement</span></div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="resultDetailPanel">
										<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
											<colgroup>
												<col width="300" />
											</colgroup>
											<tr>
												<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Nom</td>
											</tr>
											<?php for($i=0; $i<$etabs->count(); $i++)
											{
											echo '	
											<tr class="trResult">
												<td style="padding-top:5px;padding-bottom:5px;"><a href="'.$this->BuildURL('etablissementDetail') .'&id='.$etabs->items($i)->GetEtablissementId().'">'.
													$etabs->items($i)->getNom()	
												.'</a></td>
											</tr>';	
											}
											?>
										</table>
										</td>
										</tr>									
									</table>
								</div>
							</div>
						</div>
						<div class="boxBtn">
							<table class="buttonTable">
								<tr>
									<td onclick="updateEntiteAdmin('<?php echo $entite->getEntiteAdminId();?>')">Modifier</td>
									<td class="separator"></td>
									<td onclick="deleteEntiteAdmin('<?php echo $entite->getEntiteAdminId();?>')">Supprimer</td>
								</tr>
							</table>
						</div>					
 			</form>
		</div>	
	</div>
<?php
	}
}
?>
