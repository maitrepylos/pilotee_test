<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class EntiteAdminView extends BaseView
{
	private $user = null;
	
	public function search()
	{
?>
<?php if($_REQUEST['module']!='entiteAdminPopup'){?>
	<script>
		function ShowFormAddEntiteAdmin()
		{
			var frm = document.getElementById("frmFormAddEntiteAdmin");		
			frm.src = '<?php echo SCRIPTPATH . 'index.php?module=entiteAdminPopup&action=add';?>';
			Popup.showModal('formAddEntiteAdmin');		
		}

	</script>
	<div id="formAddEntiteAdmin" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er une entit� administrative</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddEntiteAdmin');reloadFrameBlank('frmFormAddEntiteAdmin');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddEntiteAdmin" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=entiteAdminPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<?php }else{?>
	<script>
	function EntiteAdminSearchClass() {}
	EntiteAdminSearchClass.prototype.Close = function(action)
	{
		var ets = new Array();
		var chks = document.getElementsByTagName("input");
			
		if (action == 'select')
		{
			for (var i = 0; i < chks.length; i++)
			{
				if (chks[i].name.match("^chk_entiteAdmin_"))
				{
					if (chks[i].checked)
					{
						var id = chks[i].name.replace("chk_entiteAdmin_", "");
						var nom = document.getElementById("chk_entiteAdmin_name_" + id).innerHTML;
						ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
					}
				}
			}

			if(ets.length>1)
			{
				alert('Un �tablissement est li� � au plus une entit� administrative');
				return false;
			}
				
			
			
		}
		parent.setEntiteAdmin(ets, true);
		
	}
	window.eas = new EntiteAdminSearchClass();	
	</script>
	<?php }?>
	<div class="boxGen">
		<form id="SearchEntiteAdmin" action="<?php echo $this->BuildURL($_REQUEST['module']); ?>" method="post">
			<input type="hidden" name="searchForm"></input>
			<div class="boxPan" style="padding:5px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<colgroup>
					<col width="200" />
					<col width="10" />
					<col width="200"/>
					<col width="10" />
					<col width="175" />
					<col width="*" />
					<col width="*"/>
				</colgroup>
				<tr>
					<td>Nom de l'entit� administrative</td>
					<td>:</td>
					<td><input type="text" maxlength="60" class ="notauto" size="60" name="nom_entiteAdmin" value="<?php if(isset($_POST["nom_entiteAdmin"])) echo ToHTML($_POST["nom_entiteAdmin"]) ?>"></input></td>
					<td></td>
					<td>
					
					</td>
					<td>
						<input type="checkbox"  name="swActif"  <?php if(isset($_POST['swActif'])) echo 'checked="checked"'; else echo ''; ?> id="swActif">&nbsp;Actif
						&nbsp;
						<input type="checkbox" name="swInactif"  <?php if(isset($_POST['swInactif'])) echo 'checked="checked"'; else echo ''; ?>>&nbsp;Inactif					
					</td>
					<td></td>
				</tr>
			</table>
			</div>
			<div class="boxBtn" style="padding-top:5px;text-align:left;">
				<table class="buttonTable" border="0" align="left">
					<tr>
						<td onclick="submitForm('SearchEntiteAdmin')">Rechercher</td>
						<?php if($_REQUEST['module']=='entiteAdminPopup'){?>
							<td class="separator"></td>
							<td onclick="javascript:window.eas.Close('select');">S�lectionner</td>
						<?php }?>
						<td class="separator"></td>
						<td onclick="clearForm('SearchEntiteAdmin')">Effacer</td>
						<?php if($_REQUEST['module']=='entiteAdminPopup'){?>
							<td class="separator"></td>
							<td onclick="javascript:window.eas.Close('close');">Fermer</td>
						<?php }?>						
					</tr>
				</table>
			</div>				
		</form>
	</div>
<?php 
	}
	
	
	public function NoResult()
	{
	?> 
	<div id="DivNoResult" class="boxGen" style="border:0px;">
		<br/>
		<div class="boxPan" style="padding:5px;border:0px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
			<tr>
				<td style="padding-top:10px;padding-bottom:10px;padding-left:3px;">
					Il n'y a aucun r�sultat pour votre requ�te ...
				</td>
			</tr>
			</table>
		</div>
	</div>	
	<?php
	}	
	
	public function renderList($entites)
	{
	?>
	<br/><br/>
		<div id="DivResult" class="boxGen" style="border:0px;">
			<div class="boxPan" style="padding:5px;border:0px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
				<tr>
					<td colspan="9" class="title" style="padding-top:10px;padding-bottom:10px;padding-left:3px;">Liste des entit�s administratives</td>
				</tr>
				</table>
			<table cellpadding="0" cellspacing="0" border="0" width="80%" class="result">
			<tr>
				<?php if($_REQUEST['module']=='entiteAdminPopup'){echo '<td class="resultHeader">&nbsp;</td>';}?>
				<td class="resultHeader">Nom</td>
				<td class="resultHeader">Province</td>
				<td class="resultHeader">Arrondissement</td>
				<td class="resultHeader">CP</td>
				<td class="resultHeader">Ville</td>
			</tr>
			<?php 
				for($i=0; $i<$entites->count(); $i++)
				{
					
					echo '<tr><td colspan="9" style="font-size:1px;height:2px;"></td></tr>';
					echo '<tr class="trResult">';
					if($_REQUEST['module']=='entiteAdminPopup'){echo sprintf('<td><input type="checkbox" name="chk_entiteAdmin_%s"></td>', $entites->items($i)->getId());}
					if($_REQUEST['module']!='entiteAdminPopup')
						echo sprintf('<td onclick="javascript:window.location=\'' . $this->BuildURL('entiteAdminDetail') .'&id='.$entites->items($i)->getId().'\';" class="resultLine resultLineRedirection">%s</td>', $entites->items($i)->getNom());
					else
						echo sprintf('<td id="chk_entiteAdmin_name_'.$entites->items($i)->getId().'" class="resultLine">%s</td>', $entites->items($i)->getNom());
						echo sprintf('<td class="resultLine">%s</td>', $entites->items($i)->getProvince());
						echo sprintf('<td class="resultLine">%s</td>', $entites->items($i)->getArrondissement());
						echo sprintf('<td class="resultLine">%s</td>', $entites->items($i)->getCodePostal());
						echo sprintf('<td class="resultLine">%s</td>', $entites->items($i)->getVille());			
					echo '</tr>';
				}
			?>
			
			</table>				
			</div>
		</div>
	<?php 
	}
	
	public function render($boUpdate=false)
	{
	?>
		<script>
		<?php if($boUpdate){?>
		function ShowFormAddEntiteAdmin()
		{
			
			var frm = document.getElementById("frmFormAddEntiteAdmin");		
			frm.src = '<?php echo SCRIPTPATH . 'index.php?module=entiteAdminPopup&action=add';?>';
			Popup.showModal('formAddEntiteAdmin');		
		}
		<?php }?>
		function EntiteAdminViewClass() {}
		EntiteAdminViewClass.prototype.Close = function(action,operation)
		{
			if(action=='valider')
			{
				var nom = document.getElementById("nom_entiteAdmin");
				var actif = document.getElementById("swActif");
				var inActif = document.getElementById("swInactif");
				var adresse = document.getElementById("rue");
				var localite = document.getElementById("localite");
				var cp = document.getElementById("cp");
				var website = document.getElementById("website");
				var fax = document.getElementById("fax");
				var tel = document.getElementById("tel");
				var tel2 = document.getElementById("tel2");
				var mail = document.getElementById("mail");
				var mail2 = document.getElementById("mail2");

				var chk = new CheckFunction();
				chk.Add(nom, 'TEXT', true); 
				chk.Add(adresse, 'TEXT', true);
				chk.Add(localite, 'TEXT', true);
				
				if(cp.value=='')
				{
					SetInputError(cp);
				}
				else
				{
					if(cp.value.length!=4)
					{
						SetInputError(cp);
					}
					else
					{
						if (isNaN(cp.value)==false)
						{
							SetInputValid(cp);
						} 
						else
						{
							SetInputError(cp);
						}
					}
				}
				
				
				chk.Add(mail, 'EMAIL', false); chk.Add(mail2, 'EMAIL', false);
				chk.Add(tel, 'PHONE', false); chk.Add(tel2, 'PHONE', false);
				chk.Add(fax, 'PHONE', false);
				var res = chk.IsValid();
				if(res)
				{
					var urlAction = '<?php echo $this->BuildURL($_REQUEST['module']);  ?>';
					if(operation=='add')
						urlAction += "&action=add";
					else
						urlAction += "&action=update";
					submitForm('formEntiteAdmin');
					
				}
				else return false;					
				  
			}
			else
			{
				parent.Popup.hide("formAddEntiteAdmin");
			}
		}
		window.fea = new EntiteAdminViewClass();
		</script>	
	<?php if($boUpdate){?>
	<div id="formAddEntiteAdmin" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er une entit� administrative</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddEntiteAdmin');reloadFrameBlank('frmFormAddEntiteAdmin');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddEntiteAdmin" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=entiteAdminPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<?php }?>		
		<form id="formEntiteAdmin" action="" method="post">		
		<div class="boxGen">
			<input type="hidden" name ="idEntiteAdmin" value="<?php if(isset($_POST['idEntiteAdmin'])) echo $_POST['idEntiteAdmin']?>"></input>
			<input type="hidden" name ="idAdresse" value="<?php if(isset($_POST['idAdresse'])) echo $_POST['idAdresse']?>"></input>
			<div class="boxPan" style="padding:5px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
				<colgroup>
					<col width="175">
					<col width="10">
					<col width="150">
					<col width="70">
					<col width="175">
					<col width="10">
					<col width="*">
				</colgroup>				
				<tr>
					<td>Nom de l'entit�</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" name="nom_entiteAdmin" id="nom_entiteAdmin" value="<?php if(isset($_POST["nom_entiteAdmin"])) echo ToHTML($_POST["nom_entiteAdmin"]) ?>"></input></td>
					<td></td>
					<td>Statut</td>
					<td>:</td>
					<td>
						<input type="checkbox" id="swActif"  name="swActif" <?php if(isset($_POST['swActif'])) echo 'checked="checked"'; else echo ''; ?> id="swActif">&nbsp;Actif
						&nbsp;
						<input type="checkbox" id="swInactif" name="swInactif" <?php if(isset($_POST['swInactif'])) echo 'checked="checked"'; else echo ''; ?>>&nbsp;Inactif					
					</td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>				
				<tr>
					<td>Adresse</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="rue" name="rue" value="<?php if(isset($_POST["rue"])) echo ToHTML($_POST["rue"]) ?>"></input></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
					</td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>				
				<tr>
					<td>Code postal</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="cp" name="cp" value="<?php if(isset($_POST["cp"])) echo ToHTML($_POST["cp"]) ?>"></input></td>
					<td></td>
					<td>Localit�</td>
					<td>:</td>
					<td>
					<input type="text"  class ="notauto" size="40" name="localite" id="localite" value="<?php if(isset($_POST["localite"])) echo ToHTML($_POST["localite"]) ?>"></input>
					</td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>
				<tr>
					<td>Site internet</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" name="website" id="website" value="<?php if(isset($_POST["website"])) echo ToHTML($_POST["website"]) ?>"></input></td>
					<td></td>
					<td>T�l�copie</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="fax" name="fax" value="<?php if(isset($_POST["fax"])) echo ToHTML($_POST["fax"]) ?>"></input></td>
				</tr>	
				<tr><td colspan="2" class="separator"></td></tr>
				<tr>
					<td>T�l�phone</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="tel" name="tel" value="<?php if(isset($_POST["tel"])) echo ToHTML($_POST["tel"]) ?>"></input></td>
					<td></td>
					<td>T�l�phone(2)</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="tel2" name="tel2" value="<?php if(isset($_POST["tel2"])) echo ToHTML($_POST["tel2"]) ?>"></input></td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>
				<tr>
					<td>Courriel</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="mail" name="mail" value="<?php if(isset($_POST["mail"])) echo ToHTML($_POST["mail"]) ?>"></input></td>
					<td></td>
					<td>Courriel(2)</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="mail2" name="mail2" value="<?php if(isset($_POST["mail2"])) echo ToHTML($_POST["mail2"]) ?>"></input></td>
				</tr>											
				</table>
			</div>
			<?php if($boUpdate){ $operation = 'update';?>
				<input type="hidden" name="updateForm" />
			<?php }else{$operation = 'add';?>
				<input type="hidden" name="addForm" />
			<?php }?>			
			<div class="boxBtn" style="padding-top:5px;text-align:left;">
				<table class="buttonTable" border="0" align="left">
					<tr>
						<?php if(!$boUpdate && !isset($_POST['addForm']) || $boUpdate){?>
						<td onclick="window.fea.Close('valider','<?php echo $operation?>')">Sauvegarder</td>
						<td class="separator"></td>
						<td onclick="clearForm('formEntiteAdmin')">Effacer</td>
						<?php }?>
						<?php if($_REQUEST['module']=='entiteAdminPopup'){?>
						<td class="separator"></td>
						<td onclick="window.fea.Close('fermer', '<?php echo $operation?>' )">Fermer</td>
						<?php }?>
					</tr>
				</table>
			</div>			
		</div>
		</form>
	<?php 
	}
}
?>
