<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');
class EtablissementDetailView extends BaseView
{
	private $user = null;
	
	public function Render($etablissement)
	{
		$this->user = Session::GetInstance()->getCurrentUser();		
?>
	<script>
	function ShowFormAddContact()
	{
		var ets = new Array();
		var id = document.getElementById("etablissementId").value;
		var nom = document.getElementById("etablissementNom").value;
		ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
		var frm = document.getElementById("frmFormAddContact");
						
		Popup.showModal('formAddContact');
		frmContent = frm.contentWindow.document || frm.contentDocument;
		
		if(ets.length>0)
		{
			SetEtablissementContact(ets, frmContent);
		}
		else
		{
			frmContent.getElementById('divEtablissementsContact').innerHTML = '';
		}
	}

	function ShowFormAddFicheSuivi()
	{
		var frm = document.getElementById("frmFormAddFicheSuivi");		
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=ficheSuiviEditPopup&action=add&etabId='.$etablissement->GetEtablissementId(); ?>';
		Popup.showModal('formAddFicheSuivi');
	}

	function ShowFormAddRemise()
	{
		var frm = document.getElementById("frmFormAddRemise");		
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=materielPedagEditPopup&action=add&etabId='.$etablissement->GetEtablissementId(); ?>';
		Popup.showModal('formAddRemise');
	}
	
	function SetEtablissementContact(objectsArray, frmContent)
	{
		if (objectsArray.length > 0)		  
		{
			var nbLignes = 0;
			var divGlobal = frmContent.getElementById("divEtablissementsContact");	
			var tab = frmContent.getElementById('tableEtablissementsContact');
			
			if(tab==null)
			{
				frmContent.getElementById('etabIds').value = '';
 				tab = divGlobal.appendChild(frmContent.createElement("table"));
 				tab.setAttribute("id","tableEtablissementsContact");
 				tab.setAttribute("style","width:80%");
			
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id",-1);
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>�tablissement</center>";
 			
 				newCell = newRow.insertCell(1);
 				newCell.innerHTML = "<center>Titre</center>";
 			
 				style='';
				<?php if($this->user->isOperateur()){ ?>
	 				style = "display:none";
	 			<?php } ?>
 				newCell = newRow.insertCell(2);
 				newCell.innerHTML = "<center>Personne ressource</center>";
 				newCell.setAttribute('style',style);
	 			newCell = newRow.insertCell(3);
 				newCell.innerHTML = "<center>Sp�cialit�</center>";
 			}
 			else
 			{
				nbLignes = frmContent.getElementById("nbLignes").value;
		}	
		 autoid = nbLignes;
		 for(j=0;j<objectsArray.length;j++)
		 {
			var etabSelect = frmContent.getElementById("etabIds");
			var reg=new RegExp("[;]+", "g");
			tableEtabSelect = etabSelect.value.split(reg);
			var isPresent = false;					
			for(var x=0; x<tableEtabSelect.length; x++)
			{
				if(tableEtabSelect[x] == objectsArray[j].id)
				{
					isPresent = true;
					break;
				}
			}
				
			if(!isPresent)
			{
				etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 				newRow = tab.insertRow(-1);
				newRow.setAttribute("id",nbLignes);

				 for(i=0;i<5;i++)
 		 		 { 		 		 
 	 			 	newCell = newRow.insertCell(i);
 	 			 	var tmp = "";
					switch(i)
 					{
 						case 0 :
 							tmp = '<input type="text" name="etabName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 								  '<input name="etabId_'+nbLignes+'" id="etabId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 							newCell.innerHTML = tmp;
 							break;
						case 1 :
						    tmp = "<?php echo createSelectOptions(Dictionnaries::getTitreContactList());?>";
							newCell.innerHTML = "<select name='titre_"+nbLignes+"' style='width:175px;'>"+
												"<option value='-1'></option>"+tmp+"</select>";
							break;
						case 2 :
							style='';
							<?php if($this->user->isOperateur()){ ?>
 								style = "display:none";
							<?php } ?>
 							newCell.setAttribute('style',style);
					    	tmp = "<?php echo createSelectOptions(Dictionnaries::getParticipationList());?>";
							newCell.innerHTML = "<select name='participation_"+nbLignes+"' style='width:175px;'>"+
												"<option value='-1'></option>"+tmp+"</select>";
							break;
 						case 3 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getSpecialiteList());?>";
							newCell.innerHTML = "<select name='specialite_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
							break;
 						case 4 :
							newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\""+nbLignes+"\", \"nbLignes\")'/>";
 							break; 							
 					}
 				
 					with(this.newCell.style)
					{	
				 		width = '100px';
				 		textAlign = 'center';
				 		padding = '5px';
					} 
					autoid++;
			 	}
			 	nbLignes++;
			 }
		  }
		  frmContent.getElementById("nbLignes").value = nbLignes;
	   }
	} 	
	function ShowFormAddFormation()
	{
		var ets = new Array();
		var id = document.getElementById("etablissementId").value;
		var nom = document.getElementById("etablissementNom").value;
		ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
		
		var frm = document.getElementById("frmFormAddFormation");		
		Popup.showModal('formAddFormation');		
		frmContent = frm.contentWindow.document || frm.contentDocument;	
		if(ets.length>0)
		{
			SetEtablissementAction(ets, frmContent);
		}
		else
		{
			frmContent.getElementById('divEtablissementsAction').innerHTML = '';
		}
	}
	function ShowFormAddAction()
	{
		var ets = new Array();
		var id = document.getElementById("etablissementId").value;
		var nom = document.getElementById("etablissementNom").value;
		ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
		
		var frm = document.getElementById("frmFormAddAction");		
		Popup.showModal('formAddAction');		
		frmContent = frm.contentWindow.document || frm.contentDocument;	
		if(ets.length>0)
		{
			SetEtablissementAction(ets, frmContent);
		}
		else
		{
			frmContent.getElementById('divEtablissementsAction').innerHTML = '';
		}
	}

	function SetEtablissementAction(objectsArray, frmContent)
	{
		
		if (objectsArray.length > 0)		  
		{
			var nbLignes = 0;
			var divGlobal = frmContent.getElementById("divEtablissementsAction");
			var tab = frmContent.getElementById('tableEtablissementsAction');
			if(tab==null)
			{
				frmContent.getElementById('etabIds').value = '';
 				tab = divGlobal.appendChild(frmContent.createElement("table"));
 				tab.setAttribute("id","tableEtablissementsAction");
			
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id",-1);
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>�tablissement</center>";
 			}
 			else
 			{
 				nbLignes = frmContent.getElementById("nbLignes").value;
 			}
			autoid = nbLignes;			
		 	for(j=0;j<objectsArray.length;j++)
 			{
				var etabSelect = frmContent.getElementById("etabIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);
	 				newRow.setAttribute("id",nbLignes);

					 for(i=0;i<2;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
 							case 0 :
 								tmp = '<input type="text" name="etabName_'+nbLignes+'" style="width:300px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 									  '<input name="etabId_'+nbLignes+'" id="etabId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 								newCell.innerHTML = tmp;
 								break;
 							case 1 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\""+nbLignes+"\", \"nbLignes\")'/>";
 								break; 							
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }
 			  }
		 	frmContent.getElementById("nbLignes").value = nbLignes;
		}
	}
	
	function ShowFormAddBourse()
	{
		var ets = new Array();
		var id = document.getElementById("etablissementId").value;
		var nom = document.getElementById("etablissementNom").value;
		ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
		
		var frm = document.getElementById("frmFormAddBourse");		
		Popup.showModal('formAddBourse');		
		frmContent = frm.contentWindow.document || frm.contentDocument;		
		if(ets.length>0)
		{
			SetEtablissementAction(ets, frmContent);
		}
		else
		{
			frmContent.getElementById('divEtablissementsAction').innerHTML = '';
		}
		
	}	
	
	</script>
	<?php 
		include_once SCRIPTPATH . 'view/partial/popup_addFormation.php';
	?>
	<div id="popupGoogleMap" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cartographie</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('popupGoogleMap');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmGoogleMaps" frameborder=0 src="" style="width:1024px;height:850px;border:0px;" scrolling="auto"></iframe></div>
		</div>
	</div>
	<div id="popupSelectAgent" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="384">
					<tr>
						<td width="95%">S�lection de l'agent</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('popupSelectAgent');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmAgents" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=popup_select_agent' ?>" style="width:384px;height:96px;border:0px;" scrolling="no"></iframe></div>
		</div>
	</div>
	<div id="formAddFicheSuivi" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er une fiche de suivi</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddFicheSuivi');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddFicheSuivi" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div id="formAddRemise" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er une remise de mat�riel</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddRemise');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddRemise" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div id="formAddContact" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er un contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddContact');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddContact" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div id="formAddAction" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er une action</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddAction');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddAction" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=actionPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div id="formAddBourse" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er un projet entrepreneurial</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddBourse');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd">
				<iframe id="frmFormAddBourse" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=boursePopup&action=add'; ?>" style="width: 1024px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	

	<div class="boxGen">
		<div class="boxPan" style="padding:5px;">
			<input type="hidden" id='etablissementId' value= "<?php echo $etablissement->GetEtablissementId();?>"></input>
			<input type="hidden" id='etablissementNom' value= "<?php echo $etablissement->GetNom();?>"></input>
			<form id="EtablissementDetail" action="<?php echo $this->BuildURL($_REQUEST['module']) . '&id=' . $etablissement->GetEtablissementId(); ?>" method="post">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td width="50%" style="vertical-align:top;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
						<tr>
							<td class="label">Nom :</td>
							<td class="detail"><?php echo $etablissement->GetNom(); ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Entit� administrative :</td>
							<td class="detail"><?php echo $etablissement->GetEntiteAdminLabel(); ?></td>
						</tr>						
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Visit� :</td>
							<td class="detail">
								<?php
									$visited = $etablissement->getVisited();

									switch ($visited)
									{
										case -1 :
											echo '<span style="padding:2px;background-color:orange;display:block;width:75px;text-align:center;color:white;"><b>Pass�</b></span>';
											break;
										case 0 :
											echo '<span style="padding:2px;background-color:gray;display:block;width:75px;text-align:center;color:white;"><b>Jamais</b></span>';
											break;
										case 1 :
											echo '<span style="padding:2px;background-color:green;display:block;width:75px;text-align:center;color:white;"><b>OK</b></span>';
											break;
									}
								?>
							</td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Activit� :</td>
							<td class="detail">
								<?php
									$activity = $etablissement->getActivity();

									switch ($activity)
									{
										case -1 :
											echo '<span style="padding:2px;background-color:orange;display:block;width:75px;text-align:center;color:white;"><b>Pass�</b></span>';
											break;
										case 0 :
											echo '<span style="padding:2px;background-color:gray;display:block;width:75px;text-align:center;color:white;"><b>Jamais</b></span>';
											break;
										case 1 :
											echo '<span style="padding:2px;background-color:green;display:block;width:75px;text-align:center;color:white;"><b>OK</b></span>';
											break;
									}
								?>
							</td>
						</tr>	
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Adresse :</td>
							<td class="detail">
								<?php echo $etablissement->GetAdresse()->GetRue(); ?><br/>
							</td>
						</tr>
						<tr>
							<td class="label">Code postal/Localit� :</td>
							<td class="detail">
								<?php 
									$cp = $etablissement->GetAdresse()->GetCodePostal();
									
									$display = isset($cp) ? $cp->GetCodePostal() : null;
									$display .= (isset($display) ? '&nbsp;' : '') . $etablissement->GetAdresse()->GetVille();  

									echo $display;
								?>
							</td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Arrondissement/Province :</td>
							<td class="detail"><?php $cp = $etablissement->GetAdresse()->GetCodePostal(); echo isset($cp) ? $cp->GetArrondissement()->getLabel() . " / " . $cp->GetProvince()->getLabel() : '&nbsp;' ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Agent responsable :</td>
							<td class="detail">
								<?php 
									$agent = $etablissement->GetUtilisateur();
									
									if (isset($agent))
										echo sprintf('<a class="resultLineRedirection" href="%s">%s</a>', $this->BuildURL('agentDetail') . '&id=' . $agent->getId(), $agent->getLabel()); 
								?>
							</td>
						</tr>
						<?php /*
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Cr�ation :</td>
							<td class="detail">le <?php echo $etablissement->GetCreated(); ?> par <?php echo sprintf('<a href="%s">%s</a>', $this->BuildURL('contact'), $etablissement->GetCreatedBy()->getLabel()); ?></td>
						</tr>
						*/ ?>
						</table>
					</td>
					<td width="50%" style="vertical-align:top;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
						<tr>
							<td class="label">Identification :</td>
							<td class="detail"><?php echo $etablissement->GetEtablissementId(); ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<?php if (!$this->user->isOperateur()) { ?>
						<tr>
							<td class="label">Statut :</td>
							
								<?php
									if ($etablissement->GetSwActif() == 1) echo '<td style="color: Green;" class="detail">Actif</td>';
									else echo '<td style="color: red;" class="detail">Inactif</td>';
								?>
							
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<?php } else { ?>
						<tr>
							<td class="label">&nbsp;</td>
							<td class="detail" style="background-color:inherit;">&nbsp;</td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<?php } ?>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Niveau :</td>
							<td class="detail"><?php echo $etablissement->GetNiveauEtablissement(); ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">R�seau :</td>
							<td class="detail"><?php echo $etablissement->GetReseauEtablissement(); ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Site internet :</td>
							<td class="detail"><?php echo $etablissement->GetWebsiteAsAnchor(); ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">T�l�phone(s) :</td>
							<td class="detail">
								<?php
									$tel1 = $etablissement->GetAdresse()->GetTel1();
									$tel2 = $etablissement->GetAdresse()->GetTel2();
	
									$tel = null;
									
									if (isset($tel1) && isset($tel2)) $tel = sprintf('%s / %s', $tel1, $tel2);
									elseif (isset($tel1)) $tel = $tel1;
									elseif (isset($tel2)) $tel = $tel2;
									
									echo $tel;
								?>
							</td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">T�l�copie :</td>
							<td class="detail"><?php echo $etablissement->GetAdresse()->GetFax(); ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Courriel(s) :</td>
							<td class="detail">
								<?php
									$email1 = $etablissement->GetAdresse()->GetEmail1AsAnchor();
									$email2 = $etablissement->GetAdresse()->GetEmail2AsAnchor();
	
									$email = null;
									
									if (isset($email1) && isset($email2)) $email = sprintf('%s / %s', $email1, $email2);
									elseif (isset($email1)) $email = $email1;
									elseif (isset($email2)) $email = $email2;
									
									echo $email;
								?>
							</td>
						</tr>						
						<?php /*
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Derni�re mise-�-jour :</td>
							<td class="detail">le <?php echo $etablissement->GetUpdated(); ?> par <?php echo sprintf('<a href="%s">%s</a>', $this->BuildURL('contact'), $etablissement->GetUpdatedBy()->getLabel()); ?></td>
						</tr>
						*/ ?>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<script language="javascript" type="text/javascript">
							function EtablissementDetailViewClass() 
							{
								this.current = 'All';
								this.tabs = new Array('All', 'CahierDeSuivi', 'Actions','Formations2', 'ProjetsInnovants', 'DepotMateriel', 'PersonnesContact');
							};

							EtablissementDetailViewClass.prototype.onMouseOver = function(liID)
							{
								document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
								document.getElementById("spn" + liID).style.color = "#FFFFFF";
								document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
							}

							EtablissementDetailViewClass.prototype.onMouseOut = function(liID)
							{
								if (this.current != liID)
								{
									document.getElementById("spn" + liID).style.backgroundPosition = "100% 0%";
									document.getElementById("spn" + liID).style.color = "#737373";
									document.getElementById("a" + liID).style.backgroundPosition = "0% 0%";
								}
							}
							
							EtablissementDetailViewClass.prototype.onLIClick = function(liID)
							{
								this.current = liID;
								
								for (var i = 0; i < this.tabs.length; i++)
								{
									if(document.getElementById("spn" + this.tabs[i]))
									{
										document.getElementById("spn" + this.tabs[i]).style.backgroundPosition = "100% 0%";
										document.getElementById("spn" + this.tabs[i]) .style.color = "#737373";
										document.getElementById("a" + this.tabs[i]).style.backgroundPosition = "0% 0%";
									}
								}

								document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
								document.getElementById("spn" + liID).style.color = "#FFFFFF";
								document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
								
								var cahierDeSuivi = document.getElementById('CahierDeSuivi');
								var actions = document.getElementById('Actions');
								var formations2 = document.getElementById('Formations2');
								var projetsInnovants = document.getElementById('ProjetsInnovants');
								var depotMateriel = document.getElementById('DepotMateriel');
								var personnesContact = document.getElementById('PersonnesContact');
								
								if (liID == 'All')
								{
									if (cahierDeSuivi) cahierDeSuivi.style.display = '';
									if (actions) actions.style.display = '';
									if (projetsInnovants) projetsInnovants.style.display = '';								
									if (depotMateriel) depotMateriel.style.display = '';
									if (personnesContact) personnesContact.style.display = '';
									if (formations2) formations2.style.display = '';
								}
								else if (liID == 'CahierDeSuivi')
								{
									if (cahierDeSuivi) cahierDeSuivi.style.display = '';
									if (actions) actions.style.display = 'none';
									if (projetsInnovants) projetsInnovants.style.display = 'none';
									if (depotMateriel) depotMateriel.style.display = 'none';
									if (personnesContact) personnesContact.style.display = 'none';
									if (formations2) formations2.style.display = 'none';
								}
								else if (liID == 'Actions')
								{
									if (cahierDeSuivi) cahierDeSuivi.style.display = 'none';
									if (actions) actions.style.display = '';
									if (projetsInnovants) projetsInnovants.style.display = 'none';
									if (depotMateriel) depotMateriel.style.display = 'none';
									if (personnesContact) personnesContact.style.display = 'none';
									if (formations2) formations2.style.display = 'none';
								}
								else if (liID == 'ProjetsInnovants')
								{
									if (cahierDeSuivi) cahierDeSuivi.style.display = 'none';
									if (actions) actions.style.display = 'none';
									if (projetsInnovants) projetsInnovants.style.display = '';
									if (depotMateriel) depotMateriel.style.display = 'none';
									if (personnesContact) personnesContact.style.display = 'none';
									if (formations2) formations2.style.display = 'none';
								}
								else if (liID == 'DepotMateriel')
								{
									if (cahierDeSuivi) cahierDeSuivi.style.display = 'none';
									if (actions) actions.style.display = 'none';
									if (projetsInnovants) projetsInnovants.style.display = 'none';
									if (depotMateriel) depotMateriel.style.display = '';
									if (personnesContact) personnesContact.style.display = 'none';
									if (formations2) formations2.style.display = 'none';
								}
								else if (liID == 'PersonnesContact')
								{
									if (cahierDeSuivi) cahierDeSuivi.style.display = 'none';
									if (actions) actions.style.display = 'none';
									if (projetsInnovants) projetsInnovants.style.display = 'none';
									if (depotMateriel) depotMateriel.style.display = 'none';
									if (personnesContact) personnesContact.style.display = '';
									if (formations2) formations2.style.display = 'none';
								}
								else if (liID == 'Formations2')
								{
									if (cahierDeSuivi) cahierDeSuivi.style.display = 'none';
									if (actions) actions.style.display = 'none';
									if (projetsInnovants) projetsInnovants.style.display = 'none';
									if (depotMateriel) depotMateriel.style.display = 'none';
									if (personnesContact) personnesContact.style.display = 'none';
									if (formations2) formations2.style.display = '';
								}
							};
	
							EtablissementDetailViewClass.prototype.onFicheSuiviClick = function(ficheSuiviId)
							{
								window.location = "<?php echo $this->BuildURL('ficheSuiviDetail')?>" + "&id=" + ficheSuiviId;
							};
	
							EtablissementDetailViewClass.prototype.onMaterielPedagClick = function(materielPedagId)
							{
								window.location = "<?php echo $this->BuildURL('materielPedagDetail')?>" + "&id=" + materielPedagId;
							};
	
							EtablissementDetailViewClass.prototype.closeAttribuerAgent = function(agentId, selected)
							{
								if (selected)
								{
									var submitActionInput = document.getElementById("SubmitAction");
									var agentIdInput = document.getElementById("AgentId");
	
									submitActionInput.value = "UPDATEAGENT";
									agentIdInput.value = agentId;
								}

								Popup.hide('popupSelectAgent');

								var frm = document.getElementById("frmAgents");
								frm.src = "about:blank";
	
								if (selected) submitForm('EtablissementDetail');							
							};
							
							EtablissementDetailViewClass.prototype.attribuerAgent = function()
							{
								var frm = document.getElementById("frmAgents");
								frm.src += "&close=EtablissementDetailViewClass.prototype.closeAttribuerAgent";
	
								Popup.showModal('popupSelectAgent');
							};

							EtablissementDetailViewClass.prototype.showGoogleMap = function()
							{
								var frm = document.getElementById("frmGoogleMaps");
								frm.src = "<?php echo SCRIPTPATH . 'index.php?module=popup_google_maps' ?>";
								frm.src += "&close=EtablissementDetailViewClass.prototype.hideGoogleMap";
								frm.src += "&ids=<?php echo $etablissement->GetEtablissementId(); ?>";


								Popup.showModal('popupGoogleMap');
							};

							EtablissementDetailViewClass.prototype.hideGoogleMap = function()
							{
								Popup.hide('popupGoogleMap');

								var frm = document.getElementById("frmGoogleMaps");
								frm.src = "about:blank";
							};
							
							window.edvc = new EtablissementDetailViewClass();
	
							function attribuerAgent()
							{
								window.edvc.attribuerAgent();
							}

							function showGoogleMap()
							{
								window.edvc.showGoogleMap();
							}
							
							/*
							function hideAttribuerAgent(agentId, selected)
							{
								window.edvc.closeAttribuerAgent(agentId, selected);
							}
							*/
							function updateEtablissement(id)
							{
								document.location.href = '<?php echo $this->BuildURL('etablissement') .'&action=update&updateId='; ?>' + id;
							}							
							
						</script>
						
						<input type="hidden" id="SubmitAction" name="SubmitAction" value="none"></input>
						<input type="hidden" id="AgentId" name="AgentId"></input>
						
						<div class="header" style="border-bottom:3px solid #EAC3C3;">
							<ul>
								<li OnMouseOver="javascript:window.edvc.onMouseOver('All');" OnMouseOut="javascript:window.edvc.onMouseOut('All');"><a id="aAll" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('All');"><span id="spnAll" class="expand">Tout</span></a></li>
								<?php if (!$this->user->isOperateur()) { ?>
								<li OnMouseOver="javascript:window.edvc.onMouseOver('CahierDeSuivi');" OnMouseOut="javascript:window.edvc.onMouseOut('CahierDeSuivi');"><a id="aCahierDeSuivi" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('CahierDeSuivi');"><span id="spnCahierDeSuivi" class="expand">Cahier de suivi</span></a></li>
								<?php } ?>
								<li OnMouseOver="javascript:window.edvc.onMouseOver('Actions');" OnMouseOut="javascript:window.edvc.onMouseOut('Actions');"><a id="aActions" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Actions');"><span id="spnActions" class="expand">Actions</span></a></li>
								<li OnMouseOver="javascript:window.edvc.onMouseOver('Formations2');" OnMouseOut="javascript:window.edvc.onMouseOut('Formations2');"><a id="aFormations2" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Formations2');"><span id="spnFormations2" class="expand">Formations</span></a></li>
								<?php if (!$this->user->isOperateur()) { ?>
								<li OnMouseOver="javascript:window.edvc.onMouseOver('ProjetsInnovants');" OnMouseOut="javascript:window.edvc.onMouseOut('ProjetsInnovants');"><a id="aProjetsInnovants" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('ProjetsInnovants');"><span id="spnProjetsInnovants" class="expand">Projets entrepreneuriaux</span></a></li>
								<li OnMouseOver="javascript:window.edvc.onMouseOver('DepotMateriel');" OnMouseOut="javascript:window.edvc.onMouseOut('DepotMateriel');"><a id="aDepotMateriel" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('DepotMateriel');"><span id="spnDepotMateriel" class="expand">D�p�ts de mat�riels</span></a></li>
								<?php } ?>
								<li OnMouseOver="javascript:window.edvc.onMouseOver('PersonnesContact');" OnMouseOut="javascript:window.edvc.onMouseOut('PersonnesContact');"><a id="aPersonnesContact" href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('PersonnesContact');"><span id="spnPersonnesContact" class="expand">Contacts</span></a></li>
							</ul>
						</div>
						<div style="margin-top:5px;width:100%;">
							<?php if (!$this->user->isOperateur()) { ?>
							<div id="CahierDeSuivi" style="width:100%;">
								<div class="resultDetailHeader" style="width:275px;"><span class="title">Cahier de suivi</span></div>
								<div class="resultDetailPanel">
									<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
									<colgroup>
									<col width="150" />
									<col width="125" />
									<col width="125" />
									<col width="125" />
									<col width="*" />
									</colgroup>
									
									<?php
										$ficheSuivis = $etablissement->GetCahierDeSuivi();
										
										if (isset($ficheSuivis))
										{
											echo '<tr>';
											echo '<td class="resultHeader">Date de rencontre</td>';
											echo '<td class="resultHeader">Timing</td>';
											echo '<td class="resultHeader">Modalit�</td>';
											echo '<td class="resultHeader">Intention</td>';
											echo '<td class="resultHeader">Contacts</td>';
											echo '</tr>';
											
											for ($i = 0; $i < $ficheSuivis->count(); $i++)
											{
												echo '<tr class="trResult">';
												
												if (!$this->user->isOperateur())
													echo '<td class="resultLine resultLineRedirection" onClick="javascript:window.edvc.onFicheSuiviClick(' . $ficheSuivis->items($i)->GetFicheSuiviId() . ')">' . $ficheSuivis->items($i)->GetDateRencontre(true) . '</td>';
												else
													echo '<td class="resultLine">' . $ficheSuivis->items($i)->GetDateRencontre(true) . '</td>';
																									
												$ts = $ficheSuivis->items($i)->GetTimingSuivi();
												echo '<td>' . (isset($ts) ? $ts->GetLabel() : '&nbsp;') . '</td>';
	
												$ms = $ficheSuivis->items($i)->GetModeSuivi();
												echo '<td>' . (isset($ms) ? $ms->GetLabel() : '&nbsp;') . '</td>';
	
												$is = $ficheSuivis->items($i)->GetIntentionSuivi();
												echo '<td>' . (isset($is) ? $is->GetLabel() : '&nbsp;') . '</td>';
												
												$contacts = $ficheSuivis->items($i)->GetContactList();
												$contactsHtml = '';
												
												for ($j = 0; $j < $contacts->count(); $j++)
												{
													if ($contactsHtml != '') $contactsHtml .= ' / ';
													
													$contactsHtml .= '<a class="resultLineRedirection" href="' . $this->BuildURL('contactDetail') . '&id=' . $contacts->items($j)->GetContactId() . '">' . $contacts->items($j)->GetPrenom() . ' ' . $contacts->items($j)->GetNom() . '</a>';
												}
												
												echo '<td>' . $contactsHtml . '</td>';
												echo '</tr>';
											}
										} 
									?>
									</table>
								</div>
								<p style="height:5px;font-size:5px;">&nbsp;</p>							
							</div>				
							<?php } ?>
							
							<div id="Actions">
								<div class="resultDetailHeader" style="width:275px;"><span class="title">Actions</span></div>
								<div class="resultDetailPanel">
									<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
									<colgroup>
									<col width="125" />
									<col width="125" />
									<col width="125" />
									<col width="125" />
									<col width="*" />
									</colgroup>
									
									<?php
										$acts = $this->user->isOperateur() ? $etablissement->GetActions($this->user->getUtilisateurId()) : $etablissement->GetActions();
										
										if (isset($acts))
										{
											echo '<tr>';
											echo '<td class="resultHeader">Ann�e scolaire</td>';
											echo '<td class="resultHeader">Date d\'action</td>';
											echo '<td class="resultHeader">Action</td>';
											echo '<td class="resultHeader">Op�rateur</td>';	
											echo '<td class="resultHeader">Contacts</td>';
											echo '</tr>';
											
											for ($i = 0; $i < $acts->count(); $i++)
											{
												echo '<tr class="trResult">';
												echo sprintf('<td>%s</td>', $acts->items($i)->GetAction()->GetAnneeId());
												echo sprintf('<td>%s</td>', $acts->items($i)->GetAction()->GetDateAction(true));
												echo sprintf('<td><a class="resultLineRedirection" href="%s">%s</a></td>', $this->BuildURL('actionDetail') . '&id=' . $acts->items($i)->GetAction()->GetActionId(), $acts->items($i)->GetAction()->GetNom());
												//echo sprintf('<td><a class="resultLineRedirection" href="%s">%s</a></td>', $this->BuildURL('operateurDetail') . '&id=' . $acts->items($i)->GetAction()->GetOperateur()->GetOperateurId(), $acts->items($i)->GetAction()->GetOperateur()->GetNom());
												if($acts->items($i)->GetAction()->GetOperateurId()!=null)
												{	
													$operateur = $acts->items($i)->GetAction()->GetOperateur();
													echo'<td class="resultLineRedirection"><a href=\''.$this->BuildURL('operateurDetail').'&id='.$operateur->GetOperateurId().'\'>'. $operateur->GetNom().'</a></td>';
												}
												else 
													echo'<td></td>';
												$contacts = $acts->items($i)->GetAction()->GetActionContact();
												$contactsHtml = '';
														
												for ($j = 0; $j < $contacts->count(); $j++)
												{
													if ($contactsHtml != '') $contactsHtml .= ' / ';
	
													//if ($etablissement->GetContacts()->Exists($contacts->items($j)->GetContactId()))
														$contactsHtml .= '<a class="resultLineRedirection" href="' . $this->BuildURL('contactDetail') . '&id=' . $contacts->items($j)->GetContactId() . '">' . $contacts->items($j)->GetContact()->GetPrenom() . ' ' . $contacts->items($j)->GetContact()->GetNom() . '</a>';
												}
														
												echo '<td>' . $contactsHtml . '</td>';
												echo '</tr>';
											}
										}
									?>
									
									</table>
								</div>
								<p style="height:5px;font-size:5px;">&nbsp;</p>
							</div>			
							
							
							<div id="Formations2">
								<div class="resultDetailHeader" style="width:275px;"><span class="title">Formations</span></div>
								<div class="resultDetailPanel">
									<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
									<colgroup>
									<col width="125" />
									<col width="125" />
									<col width="125" />
									<col width="125" />
									<col width="*" />
									</colgroup>
									
									<?php
										$acts = $this->user->isOperateur() ? $etablissement->GetFormations($this->user->getUtilisateurId()) : $etablissement->GetFormations();
										
										if (isset($acts))
										{
											echo '<tr>';
											echo '<td class="resultHeader">Ann�e scolaire</td>';
											echo '<td class="resultHeader">Date de formation</td>';
											echo '<td class="resultHeader">Formation</td>';
											echo '<td class="resultHeader">Formateur responsable</td>';	
											echo '<td class="resultHeader">Contacts</td>';
											echo '</tr>';
											
											for ($i = 0; $i < $acts->count(); $i++)
											{
												echo '<tr class="trResult">';
												echo sprintf('<td>%s</td>', $acts->items($i)->GetAction()->GetAnneeId());
												echo sprintf('<td>%s</td>', $acts->items($i)->GetAction()->GetDateAction(true));
												echo sprintf('<td><a class="resultLineRedirection" href="%s">%s</a></td>', $this->BuildURL('formationDetail') . '&id=' . $acts->items($i)->GetAction()->GetActionId(), $acts->items($i)->GetAction()->GetNom());
												//echo sprintf('<td><a class="resultLineRedirection" href="%s">%s</a></td>', $this->BuildURL('operateurDetail') . '&id=' . $acts->items($i)->GetAction()->GetOperateur()->GetOperateurId(), $acts->items($i)->GetAction()->GetOperateur()->GetNom());
												/*
												 * MODIF octobre 2013 : request from Chr. Tissot by email
												 * if($acts->items($i)->GetAction()->GetOperateurId()!=null)
												{	
													$operateur = $acts->items($i)->GetAction()->GetOperateur();
													echo'<td class="resultLineRedirection"><a href=\''.$this->BuildURL('operateurDetail').'&id='.$operateur->GetOperateurId().'\'>'. $operateur->GetNom().'</a></td>';
												}*/
												$formateur = $acts->items($i)->GetAction()->GetFormateurOperateur();
												
												if (!empty($formateur) ) {
													echo'<td>' . ToHTML($formateur->getLabel()) . '</td>';
												}							
												else 
													echo'<td></td>';
												$contacts = $acts->items($i)->GetAction()->GetActionContact();
												$contactsHtml = '';
														
												for ($j = 0; $j < $contacts->count(); $j++)
												{
													if ($contactsHtml != '') $contactsHtml .= ' / ';
	
													//if ($etablissement->GetContacts()->Exists($contacts->items($j)->GetContactId()))
														$contactsHtml .= '<a class="resultLineRedirection" href="' . $this->BuildURL('contactDetail') . '&id=' . $contacts->items($j)->GetContactId() . '">' . $contacts->items($j)->GetContact()->GetPrenom() . ' ' . $contacts->items($j)->GetContact()->GetNom() . '</a>';
												}
														
												echo '<td>' . $contactsHtml . '</td>';
												echo '</tr>';
											}
										}
									?>
									
									</table>
								</div>
								<p style="height:5px;font-size:5px;">&nbsp;</p>
							</div>			
	
							<?php if (!$this->user->isOperateur()) { ?>
							<div id="ProjetsInnovants">
								<div class="resultDetailHeader" style="width:275px;"><span class="title">Projets entrepreneuriaux</span></div>
								<div class="resultDetailPanel">
									<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
									<colgroup>
									<col width="125" />
									<col width="125" />
									<col width="300" />
									</colgroup>
									
									<?php
										$pi = $etablissement->GetProjetsInnovants();
										
										if (isset($pi))
										{
											echo '<tr>';
											echo '<td class="resultHeader">Ann�e scolaire</td>';
											echo '<td class="resultHeader">Date du projet</td>';
											echo '<td class="resultHeader">Projet</td>';
											echo '<td class="resultHeader">Subvention</td>';
											echo '</tr>';
											
											for ($i = 0; $i < $pi->count(); $i++)
											{
												echo '<tr class="trResult">';
												echo sprintf('<td>%s</td>', $pi->items($i)->GetAction()->GetAnneeId());
												echo sprintf('<td>%s</td>', $pi->items($i)->GetAction()->GetDateAction(true));
												echo sprintf('<td><a class="resultLineRedirection" href="%s">%s</a></td>', $this->BuildURL('bourseDetail') . '&id=' . $pi->items($i)->GetAction()->GetActionId(), $pi->items($i)->GetAction()->GetNom());
												echo sprintf('<td>%s</td>', $pi->items($i)->GetAction()->GetStatutSubvention());
												echo '</tr>';
											}
										}
									?>
									
									</table>
								</div>
								<p style="height:5px;font-size:5px;">&nbsp;</p>
							</div>				
	
							<div id="DepotMateriel">
								<div class="resultDetailHeader" style="width:275px;"><span class="title">D�p�ts de mat�riels</span></div>
								<div class="resultDetailPanel">
									<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
									<colgroup>
									<col width="125" />
									<col width="125" />
									<col width="250" />
									<col width="*" />
									</colgroup>
									
									<?php
										$materielPedag = $etablissement->GetMaterielPedag();
											
										if (isset($materielPedag))
										{
											echo '<tr>';
											echo '<td class="resultHeader">Date</td>';
											//echo '<td class="resultHeader">Quantit�</td>';
											echo '<td class="resultHeader">Mat�riel remis (quantit�)</td>';
											echo '<td class="resultHeader">Personne de contact</td>';
											echo '</tr>';
											
											for ($i = 0; $i < $materielPedag->count(); $i++)
											{
												echo '<tr class="trResult">';
												
												if (!$this->user->isOperateur())
													echo '<td class="resultLine resultLineRedirection" onClick="javascript:window.edvc.onMaterielPedagClick(' . $materielPedag->items($i)->GetRemiseMaterielId() . ')">' . $materielPedag->items($i)->GetDate(true) . '</td>';
												else
													echo '<td class="resultLine">' . $materielPedag->items($i)->GetDate(true) . '</td>';
													
												$materiaux = $materielPedag->items($i)->getMateriaux();													
												echo '<td>';	
												for($b=0; $b<$materiaux->count(); $b++)
												{
													echo Dictionnaries::GetMaterielPedag($materiaux->items($b)->getMaterielId())->items(0)->getLabel().'('.$materiaux->items($b)->getQuantite().')<br/>';
												}
												echo  '</td>';
												
												//echo '<td>' . $materielPedag->items($i)->GetQuantite(true) . '</td>';
												//echo '<td>' . $materielPedag->items($i)->GetMateriel()->getLabel() . '</td>';
												
												
												
												if($materielPedag->items($i)->GetContact())
													echo '<td><a class="resultLineRedirection" href="' . $this->BuildURL('contactDetail') . '&id=' . $materielPedag->items($i)->GetContact()->GetContactId() . '">' . $materielPedag->items($i)->GetContact()->GetPrenom() . ' ' . $materielPedag->items($i)->GetContact()->GetNom() . '</a></td>';
												else
													echo '<td></td>';
												echo '</tr>';
											}
										}
									?>
									
									</table>
								</div>
								<p style="height:5px;font-size:5px;">&nbsp;</p>
							</div>				
							<?php } ?>
	
							<div id="PersonnesContact">
								<div class="resultDetailHeader" style="width:275px;"><span class="title">Contacts</span></div>
								<div class="resultDetailPanel">
									<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
									<colgroup>
									<col width="200" />
									<col width="110" />
									<?php if (!$this->user->isOperateur()) { ?>
									<col width="110" />
									<col width="110" />
									<?php } ?>
									<col width="175" />
									<col width="175" />
									<col width="*" />
									</colgroup>
									
									<?php
										$contacts = $etablissement->GetContacts();
										
										if (isset($contacts))
										{
											echo '<tr>';
											echo '<td class="resultHeader">Titre</td>';
											echo '<td class="resultHeader">Sp�cialit�</td>';
											
											if (!$this->user->isOperateur())
											{
												echo '<td class="resultHeader">Personne Ressource</td>';
												echo '<td class="resultHeader">Origine</td>';
											}
											
											echo '<td class="resultHeader">Nom</td>';
											echo '<td class="resultHeader">T�l�phone</td>';
											echo '<td class="resultHeader">Email</td>';
											echo '</tr>';
																					
											for ($i = 0; $i < $contacts->count(); $i++)
											{
												echo '<tr class="trResult">';
												
												$tc = $contacts->items($i)->getTitreContact();
												echo '<td>' . ($contacts->items($i)->GetTitreContactId() && isset($tc) ? $tc->getLabel() : '&nbsp;') . '</td>';
												
												$sc = $contacts->items($i)->getSpecialiteContact();
												echo '<td>' . ($contacts->items($i)->GetSpecialiteContactId() && isset($sc)? $sc->getLabel() : '&nbsp;') . '</td>';
												
												if (!$this->user->isOperateur())
												{
													$p = $contacts->items($i)->getParticipation();
													echo '<td>' . (isset($p) && $contacts->items($i)->GetParticipationId() ? $p->getLabel() : '&nbsp;') . '</td>';
													if($contacts->items($i)->getOrigine()!=null)
													echo '<td>' . $contacts->items($i)->getOrigine()->getLabel() . '</td>';
													else echo '<td></td>';
												}
												if($contacts->items($i)->getContact()!=null)
													echo '<td><a class="resultLineRedirection" href="' . $this->BuildURL('contactDetail') . '&id=' . $contacts->items($i)->getContact()->GetContactId() . '">' . $contacts->items($i)->getContact()->GetPrenom() . ' ' . $contacts->items($i)->getContact()->GetNom() . '</a></td>';
												else
													echo '<td>/</td>';
												
												if($contacts->items($i)->getContact()!=null)
												{
													$tel1 = $contacts->items($i)->getContact()->GetAdresse()->GetTel1(); 
													$tel2 = $contacts->items($i)->getContact()->GetAdresse()->GetTel2();
												}
												$sep = null;
												
												if (isset($tel1) && strlen($tel1) > 0 && isset($tel2) && strlen($tel2) > 0) $sep =  '<br/>';
												
												echo '<td>' . $tel1 . $sep . $tel2 . '</td>';
												if($contacts->items($i)->getContact()!=null)
												{
													$email1 = $contacts->items($i)->getContact()->GetAdresse()->GetEmail1AsAnchor();
													$email2 = $contacts->items($i)->getContact()->GetAdresse()->GetEmail2AsAnchor();
												}
												$sep = null;
												
												if (isset($email1) && strlen($email1) > 0 && isset($email2) && strlen($email2) > 0) $sep =  '<br/>';
												
												echo '<td>' . $email1 . $sep . $email2 . '</td>';
												echo '</tr>';
												
											}
										}
									?>
									
									</table>
								</div>
								<p style="height:5px;font-size:5px;">&nbsp;</p>
							</div>				
						</div>
					</td>
				</tr>
				</table>
				
				<script language="javascript" type="text/javascript">
					document.getElementById("spnAll").style.backgroundPosition = "100% -250px";
					document.getElementById("spnAll").style.color = "#FFFFFF";
					document.getElementById("aAll").style.backgroundPosition = "0% -250px";
				</script>
				<?php if($this->user->isAdmin()){?>
			<div class="boxBtn">
				<table class="buttonTable">
					<tr>
						<td onclick="updateEtablissement('<?php echo $etablissement->GetEtablissementId()?>')">Modifier</td>
					</tr>
				</table>
			</div>
			<?php }?>				
			</form>
		</div>
	</div>
<?php
	}
	
	public function Wait()
	{
?>
	<div class="boxGen">
		<div class="boxPan" style="padding:5px;">
			Updating ...
		</div>
	</div>	
<?php
	}
}
?>
