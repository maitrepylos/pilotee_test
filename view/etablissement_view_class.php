<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');
REQUIRE_ONCE(SCRIPTPATH.'lib/util.php');

class EtablissementView extends BaseView
{
	public function Search($closeCallBack)
	{
		$user = Session::GetInstance()->getCurrentUser();

	?>
	<script>
	
	function attribuerAgent()
	{
		alert('Vous devez commencer par effectuer une recherche !');
	}

	function showGoogleMap()
	{
		alert('Vous devez commencer par effectuer une recherche !');
	}
	
	function ShowFormAddContact()
	{
		var ets = new Array();
		var chks = document.getElementsByTagName("input");
		for (var i = 0; i < chks.length; i++)
		{
			if (chks[i].name.match("^chk_etab_"))
			{
				if (chks[i].checked)
				{
					var id = chks[i].name.replace("chk_etab_", "");
					var nom = document.getElementById("chk_etab_name_" + id).innerHTML;
					ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
				}
			}
		}
		var frm = document.getElementById("frmFormAddContact");
						
		Popup.showModal('formAddContact');
		frmContent = frm.contentWindow.document || frm.contentDocument;
		
		if(ets.length>0)
		{
			SetEtablissementContact(ets, frmContent);
		}
		else
		{
			frmContent.getElementById('divEtablissementsContact').innerHTML = '';
		}
	}

	function ShowFormAddFormation()
	{
		var ets = new Array();
		var chks = document.getElementsByTagName("input");
		for (var i = 0; i < chks.length; i++)
		{
			if (chks[i].name.match("^chk_etab_"))
			{
				if (chks[i].checked)
				{
					var id = chks[i].name.replace("chk_etab_", "");
					var nom = document.getElementById("chk_etab_name_" + id).innerHTML;
					ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
				}
			}
		}
		var frm = document.getElementById("frmFormAddFormation");
		Popup.showModal('formAddFormation');
		frmContent = frm.contentWindow.document || frm.contentDocument;
		if(ets.length>0)
		{			
			SetEtablissementAction(ets, frmContent);
		}
		else
		{
			frmContent.getElementById('divEtablissementsAction').innerHTML = '';
		}
	}

	function ShowFormAddAction()
	{
		var ets = new Array();
		var chks = document.getElementsByTagName("input");
		for (var i = 0; i < chks.length; i++)
		{
			if (chks[i].name.match("^chk_etab_"))
			{
				if (chks[i].checked)
				{
					var id = chks[i].name.replace("chk_etab_", "");
					var nom = document.getElementById("chk_etab_name_" + id).innerHTML;
					ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
				}
			}
		}
		var frm = document.getElementById("frmFormAddAction");
		Popup.showModal('formAddAction');
		frmContent = frm.contentWindow.document || frm.contentDocument;
		if(ets.length>0)
		{			
			SetEtablissementAction(ets, frmContent);
		}
		else
		{
			frmContent.getElementById('divEtablissementsAction').innerHTML = '';
		}
	}

	function ShowFormAddBourse()
	{
		var ets = new Array();
		var chks = document.getElementsByTagName("input");
		for (var i = 0; i < chks.length; i++)
		{
			if (chks[i].name.match("^chk_etab_"))
			{
				if (chks[i].checked)
				{
					var id = chks[i].name.replace("chk_etab_", "");
					var nom = document.getElementById("chk_etab_name_" + id).innerHTML;
					ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
				}
			}
		}
		
		var frm = document.getElementById("frmFormAddBourse");		
		Popup.showModal('formAddBourse');		
		frmContent = frm.contentWindow.document || frm.contentDocument;		
		if(ets.length>0)
		{
			SetEtablissementAction(ets, frmContent);
		}
		else
		{
			frmContent.getElementById('divEtablissementsAction').innerHTML = '';
		}
		
	}	

	function SetEtablissementAction(objectsArray, frmContent)
	{
		
		if (objectsArray.length > 0)		  
		{
			var nbLignes = 0;
			var divGlobal = frmContent.getElementById("divEtablissementsAction");
			var tab = frmContent.getElementById('tableEtablissementsAction');
			if(tab==null)
			{
				frmContent.getElementById('etabIds').value = '';
 				tab = divGlobal.appendChild(frmContent.createElement("table"));
 				tab.setAttribute("id","tableEtablissementsAction");
			
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id",-1);
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>�tablissement</center>";
 			}
 			else
 			{
 				nbLignes = frmContent.getElementById("nbLignes").value;
 			}
			autoid = nbLignes;			
		 	for(j=0;j<objectsArray.length;j++)
 			{
				var etabSelect = frmContent.getElementById("etabIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);
	 				newRow.setAttribute("id",nbLignes);

					 for(i=0;i<2;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
 							case 0 :
 								tmp = '<input type="text" name="etabName_'+nbLignes+'" style="width:300px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 									  '<input name="etabId_'+nbLignes+'" id="etabId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 								newCell.innerHTML = tmp;
 								break;
 							case 1 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\""+nbLignes+"\", \"nbLignes\")'/>";
 								break; 							
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }
 			  }
		 	frmContent.getElementById("nbLignes").value = nbLignes;
		}
	}		
	
	function SetEtablissementContact(objectsArray, frmContent)
	{
		if (objectsArray.length > 0)		  
		{
			var nbLignes = 0;
			var divGlobal = frmContent.getElementById("divEtablissementsContact");	
			var tab = frmContent.getElementById('tableEtablissementsContact');
			
			if(tab==null)
			{
				frmContent.getElementById('etabIds').value = '';
 				tab = divGlobal.appendChild(frmContent.createElement("table"));
 				tab.setAttribute("id","tableEtablissementsContact");
 				tab.setAttribute("style","width:80%");
			
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id",-1);
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>�tablissement</center>";
 			
 				newCell = newRow.insertCell(1);
 				newCell.innerHTML = "<center>Titre</center>";
 			
 				style='';
 				
				<?php if($user->isOperateur()){ ?>
	 				style = "display:none";
	 			<?php } ?>

 				newCell = newRow.insertCell(2);
 				newCell.innerHTML = "<center>Personne Ressource</center>";
 				newCell.setAttribute('style',style);
	 			newCell = newRow.insertCell(3);
 				newCell.innerHTML = "<center>Sp�cialit�</center>";
 			}
 			else
 			{
				nbLignes = frmContent.getElementById("nbLignes").value;
		}	
		 autoid = nbLignes;
		 for(j=0;j<objectsArray.length;j++)
		 {
			var etabSelect = frmContent.getElementById("etabIds");
			var reg=new RegExp("[;]+", "g");
			tableEtabSelect = etabSelect.value.split(reg);
			var isPresent = false;					
			for(var x=0; x<tableEtabSelect.length; x++)
			{
				if(tableEtabSelect[x] == objectsArray[j].id)
				{
					isPresent = true;
					break;
				}
			}
				
			if(!isPresent)
			{
				etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 				newRow = tab.insertRow(-1);
				newRow.setAttribute("id",nbLignes);

				 for(i=0;i<5;i++)
 		 		 { 		 		 
 	 			 	newCell = newRow.insertCell(i);
 	 			 	var tmp = "";
					switch(i)
 					{
 						case 0 :
 							tmp = '<input type="text" name="etabName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 								  '<input name="etabId_'+nbLignes+'" id="etabId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 							newCell.innerHTML = tmp;
 							break;
						case 1 :
						    tmp = "<?php echo createSelectOptions(Dictionnaries::getTitreContactList());?>";
							newCell.innerHTML = "<select name='titre_"+nbLignes+"' style='width:175px;'>"+
												"<option value='-1'></option>"+tmp+"</select>";
							break;
						case 2 :
							style='';
							<?php if($user->isOperateur()){ ?>
 								style = "display:none";
							<?php } ?>
 							newCell.setAttribute('style',style);
					    	tmp = "<?php echo createSelectOptions(Dictionnaries::getParticipationList());?>";
							newCell.innerHTML = "<select name='participation_"+nbLignes+"' style='width:175px;'>"+
												"<option value='-1'></option>"+tmp+"</select>";
							break;
 						case 3 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getSpecialiteList());?>";
							newCell.innerHTML = "<select name='specialite_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
							break;
 						case 4 :
							newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\""+nbLignes+"\", \"nbLignes\")'/>";
 							break; 							
 					}
 				
 					with(this.newCell.style)
					{	
				 		width = '100px';
				 		textAlign = 'center';
				 		padding = '5px';
					} 
					autoid++;
			 	}
			 	nbLignes++;
			 }
		  }
		  frmContent.getElementById("nbLignes").value = nbLignes;
	   }
	}

	function ShowFormAddEtab()
	{
		var frm = document.getElementById("frmFormAddEtablissement");		
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=etablissementPopup&action=add';?>';
		Popup.showModal('formAddEtablissement');	
	}		
	</script>
		
	<?php if($_REQUEST['module']=='etablissement'){?>
	<?php 
		include_once SCRIPTPATH . 'view/partial/popup_addFormation.php';
	?>
	<div id="popupSelectAgent" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="384">
					<tr>
						<td width="95%">S�lection de l'agent</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('popupSelectAgent');reloadFrameBlank('frmAgents');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmAgents" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=popup_select_agent' ?>" style="width:384px;height:96px;border:0px;" scrolling="no"></iframe></div>
		</div>
	</div>
	<div id="popupGoogleMap" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cartographie</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('popupGoogleMap');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmGoogleMaps" frameborder=0 src="" style="width:1024px;height:850px;border:0px;" scrolling="auto"></iframe></div>
		</div>
	</div>
	<div id="formAddContact" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er un contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="reloadFrameBlank('frmFormAddContact');javascript:Popup.hide('formAddContact');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddContact" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div id="formAddAction" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
							<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er une action</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddAction');reloadFrameBlank('frmFormAddAction');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddAction" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=actionPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div id="formAddEtablissement" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
							<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er un �tablissement</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddEtablissement');reloadFrameBlank('frmFormAddEtablissement');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFormAddEtablissement" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=etablissementPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>	
	
	<div id="formAddBourse" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Cr�er projet entrepreneurial</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddBourse');reloadFrameBlank('frmFormAddBourse');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd">
				<iframe id="frmFormAddBourse" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=boursePopup&action=add'; ?>" style="width: 1024px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	<?php }?>
	<div class="boxGen" <?php if ($_REQUEST['module'] == 'etablissementPopup') { ?> style="padding-right:5px;padding-top:10px;" <?php } ?>>
		<form id="SearchEtablissement" action="<?php echo $this->BuildURL($_REQUEST['module']); ?>" method="post">
			<input type="hidden" name="SearchEtablissement"></input>
			<div class="boxPan" style="padding:5px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<colgroup>
					<col width="175" />
					<col width="10" />
					<col width="200"/>
					<col width="10" />
					<col width="175" />
					<col width="10" />
					<col width="*"/>
				</colgroup>
				<tr>
					<td>Nom de l'�tablissement</td>
					<td>:</td>
					<td><input type="text" maxlength="60" class ="notauto" size="60" name="nom_etablissement" value="<?php if(isset($_POST["nom_etablissement"])) echo ToHTML($_POST["nom_etablissement"]) ?>"></input></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<?php if (($user->isAdmin()) || ($user->isCoordinateur()) || ($user->isAgent()) || ($user->isAgentCoordinateur()) ) { ?>
						<input id="swActif" name="swActif" type="checkbox" <?php if (isset($_POST["swActif"])) echo 'checked="checked"'; ?>>&nbsp;Actif
						&nbsp;
						<input name="swInactif" type="checkbox" <?php if (isset($_POST["swInactif"])) echo 'checked="checked"'; ?>>&nbsp;Inactif
						<?php } ?>
						<?php if ($user->isAgent() ||  ($user->isAgentCoordinateur()) ) { ?>
						&nbsp;
						<input type="checkbox" name="swOnlyMyPortFolio" <?php if (isset($_POST["swOnlyMyPortFolio"])) echo 'checked="checked"'; ?>>&nbsp;Uniquement mon portfolio						
						<?php } ?>				
					</td>
				</tr>
				
				<?php if (($user->isAdmin()) || ($user->isCoordinateur()  || ($user->isAgentCoordinateur()))) { ?>
					<tr><td colspan="3" style="font-size:1px;height:2px;"></td></tr>
					<tr>
						<td>Agent</td>
						<td>:</td>
						<td>
							<select name='agent' style="width:175px;">
								<?php
									echo '<option value="-1">Tous</option>';

									if (isset($_POST['agent']))
									{
										if ($_POST['agent'] == -2)
										{
											echo '<option value="-2" selected="selected">Pas attribu�</option>';
											echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT));
										}
										else
										{
											echo '<option value="-2">Pas attribu�</option>';
											echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT), $_POST['agent']);
										}
									}
									else
									{
										echo '<option value="-2">Pas attribu�</option>';
										echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT));
									}
								?>
							</select>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				<?php } ?>

				<tr><td colspan="3" style="font-size:1px;height:2px;"></td></tr>
				<tr>
					<td>Province</td>
					<td>:</td>
					<td>
						<select name="province" id="province" style="width:175px;" onchange="updateSelect('province', 'arrondissement')">
							<?php
								echo '<option value="-1">Toutes</option>';
								if (isset($_POST['province'])) {
									echo createSelectOptions(Dictionnaries::getProvincesList(), $_POST['province']);
								}
								else {
									echo createSelectOptions(Dictionnaries::getProvincesList());		
								}
							?>
						</select>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr><td colspan="3" style="font-size:1px;height:2px;"></td></tr>
				<tr>
					<td>Arrondissement</td>
					<td>:</td>
					<td>
						<select name="arrondissement" id="arrondissement" style="width:175px;">
							<?php
								echo '<option value="-1">Tous</option>';
								if (isset($_POST['arrondissement'])) {
									echo createSelectOptions(Dictionnaries::getArrondissementList(), $_POST['arrondissement']);
								}
								else {
									echo createSelectOptions(Dictionnaries::getArrondissementList());
								}
							?>
						</select>
					</td>
					<td></td>
					<td>R�seau</td>
					<td>:</td>
					<td>
						<select name="reseau" style="width:175px;">
							<?php
								echo '<option value="-1">Tous</option>';
								if (isset($_POST['reseau'])) {
									echo createSelectOptions(Dictionnaries::getReseauxList(), $_POST['reseau']);
								}
								else {
									echo createSelectOptions(Dictionnaries::getReseauxList());
								}
							?>
						</select>
					</td>
				</tr>
				<tr><td colspan="3" style="font-size:1px;height:2px;"></td></tr>
				<tr>
					<td>Code postal</td>
					<td>:</td>
					<td>
						<input type="text" maxlength="4" class ="notauto" size="10" name="code_postal_1" value="<?php if (isset($_POST["code_postal_1"])) echo ToHTML($_POST["code_postal_1"]) ?>"></input>
						&nbsp;�&nbsp;
						<input type="text" maxlength="4" class ="notauto" size="10" name="code_postal_2" value="<?php if (isset($_POST["code_postal_2"])) echo ToHTML($_POST["code_postal_2"]) ?>"></input>
					</td>
					<td></td>
					<td>Niveau</td>
					<td>:</td>
					<td>
						<select name="niveau" style="width:175px;">
							<?php
								echo '<option value="-1">Tous</option>';
								if (isset($_POST['niveau'])) {
									echo createSelectOptions(Dictionnaries::getNiveauxList(), $_POST['niveau']);
								}
								else {
									echo createSelectOptions(Dictionnaries::getNiveauxList());
								}
							?>
						</select>
					</td>
				</tr>
				</table>
			</div>
			
			<input type="hidden" id="SubmitAction" name="SubmitAction" value="none"></input>
			<input type="hidden" id="CloseCallBack" name="CloseCallBack" value="<?php echo $closeCallBack; ?>"></input>
			<input type="hidden" id="EtsForAgent" name="EtsForAgent"></input>
			<input type="hidden" id="AgentId" name="AgentId"></input>
			
			<script language="javascript" type="text/javascript">
			function EtablissementViewClass() {}

			EtablissementViewClass.prototype.Close = function(action)
			{
			
				var submitAction = document.getElementById("SubmitAction");
				if (submitAction) submitAction.value = "none";

				var divNoResult = document.getElementById("DivNoResult");
				if (divNoResult) divNoResult.style.display = "none";

				var divResult = document.getElementById("DivResult");
				if (divResult) divResult.style.display = "none";
				
				clearForm('SearchEtablissement');

				var swActif = document.getElementById("swActif");
				if (swActif) swActif.checked = true;

				var ets = new Array();
				var chks = document.getElementsByTagName("input");

				if (action == 'select')
				{
					for (var i = 0; i < chks.length; i++)
					{
						if (chks[i].name.match("^chk_etab_"))
						{
							if (chks[i].checked)
							{
								var id = chks[i].name.replace("chk_etab_", "");
								var nom = document.getElementById("chk_etab_name_" + id).innerHTML;
								ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
							}
						}
					}
				}
				<?php echo 'parent.' . $closeCallBack . "(ets, true);"; ?>
			}

			window.evc = new EtablissementViewClass();
			</script>
			
			<div class="boxBtn" style="padding:5px;">
				<table class="buttonTable">
					<tr>
						<td onclick="document.getElementById('SubmitAction').value='search';submitForm('SearchEtablissement')">Rechercher</td>
						<td class="separator"></td>
						<td onclick="clearForm('SearchEtablissement')">Effacer</td>
						<?php if ($_REQUEST['module'] == 'etablissementPopup') { ?>
							<td class="separator"></td>
							<td onclick="javascript:window.evc.Close('select');">Selectionner</td>
							<td class="separator"></td>
							<td onclick="javascript:window.evc.Close('cancel');">Annuler</td>
						<?php }?>
					</tr>
				</table>
			</div>
		</form>
	</div>
	<?php
	}

	public function NoResult()
	{
	?> 
	<div id="DivNoResult" class="boxGen" style="border:0px;">
		<br/>
		<div class="boxPan" style="padding:5px;border:0px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
			<tr>
				<td style="padding-top:10px;padding-bottom:10px;padding-left:3px;">
					Il n'y a aucun r�sultat pour votre requ�te ...
				</td>
			</tr>
			</table>
		</div>
	</div>	
	<?php
	}
	
	public function RenderList($ets)
	{
	?>
	<script language="javascript" type="text/javascript">
		function EtablissementListClass() {}

		EtablissementListClass.prototype.SelectAll = function(selected)
		{
			var liste = document.getElementsByTagName("input");
			var re = new RegExp("^chk_etab_");

			for (var i = 0; i < liste.length; i++)
			{
				if (re.exec(liste[i].name)) liste[i].checked = selected;  
			} 
		};

		EtablissementListClass.prototype.AttribuerAgent = function(selected)
		{
			var atLeastOne = false;
			
			var liste = document.getElementsByTagName("input");
			var re = new RegExp("^chk_etab_");

			for (var i = 0; i < liste.length; i++)
			{
				if (re.exec(liste[i].name))
				{
					if (atLeastOne = liste[i].checked) break;
				}  
			}

			if (!atLeastOne)
				alert('Veuillez s�lectionner au moins un �tablissement !');
			else
			{
				var frm = document.getElementById("frmAgents");
				frm.src += "&close=EtablissementListClass.prototype.closeAttribuerAgent";

				Popup.showModal('popupSelectAgent');
			}
		};

		EtablissementListClass.prototype.ShowGoogleMap = function(selected)
		{
			var atLeastOne = false;
			
			var liste = document.getElementsByTagName("input");
			var re = new RegExp("^chk_etab_");

			for (var i = 0; i < liste.length; i++)
			{
				if (re.exec(liste[i].name))
				{
					if (atLeastOne = liste[i].checked) break;
				}  
			}

			if (!atLeastOne)
				alert('Veuillez s�lectionner au moins un �tablissement !');
			else
			{
				var lst = null;
				var nbEtablissement = 0;
				
				for (var i = 0; i < liste.length; i++)
				{
					if (re.exec(liste[i].name)) {
						if (liste[i].checked) {
							lst = lst == null ? liste[i].name.replace(re, "") : lst + ", " + liste[i].name.replace(re, "");
							nbEtablissement++;
						}
					}
				}
				//v�rification pour �viter qu'on affiche une map avec plus de 1000 �tablissement 
				// sinon g�n�re 'erreur Request-URI Too Long' voir constante pour changer ce nombre
				//var nbEtablissement = parseInt(document.getElementsByTagName("input").length);
				var maxEtablissement = parseInt(<?php echo GOOGLE_NB_MAX_MAP ?>);
				if (nbEtablissement > maxEtablissement) {
					 alert('Votre requ�te contient trop d\'�tablissements (maximum ' + maxEtablissement + ' �tablissements)');
				}
				else {
					var frm = document.getElementById("frmGoogleMaps");
					frm.src = "<?php echo SCRIPTPATH . 'index.php?module=popup_google_maps' ?>";
					frm.src += "&close=EtablissementListClass.prototype.hideGoogleMap";
					frm.src += "&ids=" + lst;
					Popup.showModal('popupGoogleMap');
				}
				
			}
		}

		EtablissementListClass.prototype.hideGoogleMap = function()
		{
			Popup.hide('popupGoogleMap');

			var frm = document.getElementById("frmGoogleMaps");
			frm.src = "about:blank";
		};

		EtablissementListClass.prototype.closeAttribuerAgent = function(agentId, selected)
		{
			if (selected)
			{
				var liste = document.getElementsByTagName("input");
				var re = new RegExp("^chk_etab_");
				var lst = null;

				for (var i = 0; i < liste.length; i++)
				{
					if (re.exec(liste[i].name) && liste[i].checked)
						lst = lst == null ? liste[i].name.replace(re, "") : lst + ", " + liste[i].name.replace(re, "");   
				}

				var submitActionInput = document.getElementById("SubmitAction");
				var agentIdInput = document.getElementById("AgentId");
				var etsForAgent = document.getElementById("EtsForAgent");

				submitActionInput.value = "updateagent";
				agentIdInput.value = agentId;
				etsForAgent.value = lst;
			}

			Popup.hide('popupSelectAgent');

			var frm = document.getElementById("frmAgents");
			frm.src = "about:blank";

			if (selected) submitForm('SearchEtablissement');							
		};
		
		EtablissementListClass.prototype.ShowEtablissementInfo = function(id)
		{
			var submitAction = document.getElementById("SubmitAction");
			submitAction.value = 'displayEtablissement';
			
			var etablissementId = document.getElementById("etablissementId");
			etablissementId.value = id;

			window.location = '<?php echo $this->BuildURL('etablissementDetail') . '&id='; ?>' + id;
		}

		window.elc = new EtablissementListClass();

		function attribuerAgent()
		{
			window.elc.AttribuerAgent();
		}

		function showGoogleMap()
		{
			window.elc.ShowGoogleMap();
		}
		
	</script>
	
	<input type="hidden" value="0" name="etablissementId" id="etablissementId"></input>
		
	<div id="DivResult" class="boxGen" style="border:0px;">
		<div class="boxPan" style="padding:5px;border:0px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
			<tr>
				<td colspan="9" class="title" style="padding-top:10px;padding-bottom:10px;padding-left:3px;">Liste des �tablissements : <a class="resultLineRedirection" href="javascript:void(0);" onclick="window.elc.SelectAll(true);">tout s�lectionner</a> - <a class="resultLineRedirection" href="javascript:void(0);" onclick="window.elc.SelectAll(false);">tout d�selectionner</a></td>
			</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
			<tr>
				<?php /*if ($_REQUEST['module'] == 'etablissementPopup') { */ ?>
				<td class="resultHeader">&nbsp;</td>
				<?php /* } */ ?>
				
				<?php if ($_REQUEST['module'] == 'etablissement') { ?>
				<td class="resultHeader">Agent</td>
				<?php } ?>
				
				<td class="resultHeader">Etablissement</td>
				<td class="resultHeader">Niveau</td>
				
				<?php if ($_REQUEST['module'] == 'etablissement') { ?>
				<td class="resultHeader">Reseau</td>
				<td class="resultHeader">Province</td>
				<td class="resultHeader">Arrondissement</td>
				<?php } ?>
				
				<td class="resultHeader">CP</td>
				<td class="resultHeader">Ville</td>
			</tr>
			<?php
				for ($i = 0; $i < $ets->Count(); $i++)
				{
					echo '<tr><td colspan="9" style="font-size:1px;height:2px;"></td></tr>';
					echo '<tr class="trResult">';

					echo sprintf('<td><input type="checkbox" name="chk_etab_%s"></td>', $ets->items($i)->getId());
					
					if ($_REQUEST['module'] == 'etablissement')
					{
						if($ets->items($i)->getAgentId()!=null)
							echo sprintf('<td onclick="javascript:window.location=\'' . $this->BuildURL('agentDetail') .'&id='.$ets->items($i)->getAgentId().'\';" class="resultLine resultLineRedirection">%s&nbsp;%s</td>', $ets->items($i)->getAgentPrenom(), $ets->items($i)->getAgentNom());
						else
							echo sprintf('<td class="resultLine">%s&nbsp;%s</td>', $ets->items($i)->getAgentPrenom(), $ets->items($i)->getAgentNom());	
						echo sprintf('<td id="chk_etab_name_%s" onclick="javascript:window.elc.ShowEtablissementInfo(\'' . $ets->items($i)->getId() . '\');" class="resultLine resultLineRedirection">%s</td>',$ets->items($i)->getId(), $ets->items($i)->getNom());
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->getNiveau());
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->getReseau());
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->getProvince());
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->getArrondissement());
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->getCodePostal());
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->getVille());
					}
					elseif ($_REQUEST['module'] == 'etablissementPopup')
					{
						echo sprintf('<td id="chk_etab_name_%s">%s</td>', $ets->items($i)->getId(), $ets->items($i)->getNom()); 
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->getNiveau());
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->getCodePostal());
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->getVille());
					}

					echo '<tr>';
				} 
			?>
			</table>
		</div>
	</div>	
	
	<?php
	} 
	
	public function render($boUpdate=false)
	{
?>
	<script>
	function EtabViewClass() {}
		EtabViewClass.prototype.Close = function(action,operation)
		{
			if(action=='valider')
			{
				var nom = document.getElementById("nom");
				var actif = document.getElementById("swActif");
				var inActif = document.getElementById("swInactif");
				var adresse = document.getElementById("rue");
				var localite = document.getElementById("localite");
				var cp = document.getElementById("cp");
				var website = document.getElementById("website");
				var fax = document.getElementById("fax");
				var tel = document.getElementById("tel");
				var tel2 = document.getElementById("tel2");
				var mail = document.getElementById("mail");
				var mail2 = document.getElementById("mail2");
				var reseau = document.getElementById("reseau");
				var niveau = document.getElementById("niveau");

				var chk = new CheckFunction();
				chk.Add(nom, 'TEXT', true); 
				chk.Add(adresse, 'TEXT', true);
				chk.Add(localite, 'TEXT', true);
				var res2 = true;
				if(reseau.value=='-1'){ SetInputError(reseau); res2 = false;}else{ SetInputValid(reseau); }
				if(niveau.value=='-1'){ SetInputError(niveau); res2 = false;}else{ SetInputValid(niveau); }
				
				
				if(cp.value=='')
				{
					SetInputError(cp);
					res2 = false;
				}
				else
				{
					if(cp.value.length!=4)
					{
						SetInputError(cp);
						res2 = false;
					}
					else
					{
						if (isNaN(cp.value)==false)
						{
							SetInputValid(cp);
						} 
						else
						{
							SetInputError(cp);
							res2 = false;
						}
					}
				}
				
				
				chk.Add(mail, 'EMAIL', false); chk.Add(mail2, 'EMAIL', false);
				chk.Add(tel, 'PHONE', false); chk.Add(tel2, 'PHONE', false);
				chk.Add(fax, 'PHONE', false);
				var res = chk.IsValid();
			
				if(res && res2)
				{
					var urlAction = '<?php echo $this->BuildURL($_REQUEST['module']);  ?>';
					if(operation=='add')
						urlAction += "&action=add";
					else
						urlAction += "&action=update";
					submitForm('formEtablissement');
					
				}
				else
				 return false;					
				  
			}
			else
			{
				parent.Popup.hide("formAddEtablissement");
			}
		}
		window.fetab = new EtabViewClass();

		function ShowEntiteAdminSearch()
		{
			var frm = document.getElementById("frmEntiteAdmins");
			url = '<?php echo SCRIPTPATH . 'index.php?module=entiteAdminPopup'; ?>';
			frm.src = url ;
			Popup.showModal('entiteAdmins');
		}

		function setEntiteAdmin(objectsArray,close)
		{
			
			if (objectsArray.length > 0)
			{
				for (var i=0; i<objectsArray.length; i++)
				{
					var name = document.getElementById("entite");
					var id = document.getElementById("entiteId");
					id.value = objectsArray[i].id;
					name.value = unescape(objectsArray[i].text); 
				}
			}			
			
			if(close)
			{
				 document.getElementById("frmEntiteAdmins").src = "about:blank";
				 Popup.hide('entiteAdmins');
			}
			
		}
		<?php if($_REQUEST['module']=='etablissement'){?>
		function ShowFormAddEtab()
		{
			var frm = document.getElementById("frmFormAddEtablissement");		
			frm.src = '<?php echo SCRIPTPATH . 'index.php?module=etablissementPopup&action=add';?>';
			Popup.showModal('formAddEtablissement');	
		}
		<?php }?>
		</script>
		<?php if($_REQUEST['module']=='etablissement'){?>
		<div id="formAddEtablissement" class="popup">
			<div class="popupPanel">
				<div class="popupPanelHeader">
								<table cellspacing="0" cellspacing="0" border="0" width="1024">
						<tr>
							<td width="95%">Cr�er un �tablissement</td>
							<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddEtablissement');reloadFrameBlank('frmFormAddEtablissement');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
						</tr>
					</table>
				</div>
				<div class="bd"><iframe id="frmFormAddEtablissement" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=etablissementPopup&action=add'; ?>" style="width:1024px;height:512px;border:0px;"></iframe></div>
			</div>
		</div>
		<?php }?>
			
		<div id="entiteAdmins" class="popup" style="width:984px">
			<div class="popupPanel" style="width:984px">
				<div class="popupPanelHeader">
					<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
						<tr>
							<td width="95%">Recherche sur les entit�s administratives</td>
							<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('entiteAdmins');reloadFrameBlank('frmEntiteAdmins');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
						</tr>
					</table>
				</div>
				<div class="bd" style="width:984px"><iframe id="frmEntiteAdmins" frameborder=0 src="" style="width:984px;height:512px;border:0px;"></iframe></div>
			</div>
		</div>		
		<form id="formEtablissement" action="" method="post">		
		<div class="boxGen">
			<input type="hidden" name ="idEtablissement" value="<?php if(isset($_POST['idEtablissement'])) echo $_POST['idEtablissement']?>"></input>
			<input type="hidden" name ="idAdresse" value="<?php if(isset($_POST['idAdresse'])) echo $_POST['idAdresse']?>"></input>
			<div class="boxPan" style="padding:5px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
				<colgroup>
					<col width="160">
					<col width="10">
					<col width="300">
					<col width="45">
					<col width="120">
					<col width="10">
					<col width="*">
				</colgroup>				
				<tr>
					<td>Nom de l'�tablissement</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" name="nom" id="nom" value="<?php if(isset($_POST["nom"])) echo ToHTML($_POST["nom"]) ?>"></input></td>
					<td></td>
					<td>Statut</td>
					<td>:</td>
					<td>
						<input type="checkbox" id="swActif"  name="swActif" <?php if(isset($_POST['swActif'])) echo 'checked="checked"'; else echo ''; ?> id="swActif">&nbsp;Actif
						&nbsp;
						<input type="checkbox" id="swInactif" name="swInactif" <?php if(isset($_POST['swInactif'])) echo 'checked="checked"'; else echo ''; ?>>&nbsp;Inactif					
					</td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>
				<tr>
					<td>Entit� administrative</td>
					<td>:</td>
					<td>
						<input type="text"  class ="notauto" size="40" readonly="readonly" name="entite" id="entite" value="<?php if(isset($_POST["entite"])) echo ToHTML($_POST["entite"]) ?>"></input>
						<input type="hidden" name="entiteId" id="entiteId" value="<?php if(isset($_POST["entiteId"])) echo ToHTML($_POST["entiteId"]) ?>"></input>
						<img class="expand" onclick="ShowEntiteAdminSearch();return false" id="searchEntiteAdmin" src="./styles/img/etablissement.png">
						<img title="vider" class="expand" onclick="document.getElementById('entite').value=''; document.getElementById('entiteId').value='';" src="./styles/img/eraser.gif">
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>				
				<tr>
					<td>Adresse</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="rue" name="rue" value="<?php if(isset($_POST["rue"])) echo ToHTML($_POST["rue"]) ?>"></input></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
					</td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>				
				<tr>
					<td>Code postal</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="cp" name="cp" value="<?php if(isset($_POST["cp"])) echo ToHTML($_POST["cp"]) ?>"></input></td>
					<td></td>
					<td>Localit�</td>
					<td>:</td>
					<td>
					<input type="text"  class ="notauto" size="40" name="localite" id="localite" value="<?php if(isset($_POST["localite"])) echo ToHTML($_POST["localite"]) ?>"></input>
					</td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>				
				<tr>
					<td>Niveau</td>
					<td>:</td>
					<td>
						<select name="niveau" id="niveau" style="width:245px">
							<option value="-1"></option>
							<?php 
								echo createSelectOptions(Dictionnaries::getNiveauxList(),  !isset($_POST['niveau']) ? "" : $_POST['niveau']);
							?>
						</select>
					</td>
					<td></td>
					<td>R�seau</td>
					<td>:</td>
					<td>
						<select name="reseau" id="reseau" style="width:245px">
							<option value="-1"></option>
							<?php 
								echo createSelectOptions(Dictionnaries::getReseauxList(),  !isset($_POST['reseau']) ? "" : $_POST['reseau']);
							?>
						</select>					
					</td>
				</tr>				
				<tr><td colspan="2" class="separator"></td></tr>
				<tr>
					<td>Site internet</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" name="website" id="website" value="<?php if(isset($_POST["website"])) echo ToHTML($_POST["website"]) ?>"></input></td>
					<td></td>
					<td>T�l�copie</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="fax" name="fax" value="<?php if(isset($_POST["fax"])) echo ToHTML($_POST["fax"]) ?>"></input></td>
				</tr>	
				<tr><td colspan="2" class="separator"></td></tr>
				<tr>
					<td>T�l�phone</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="tel" name="tel" value="<?php if(isset($_POST["tel"])) echo ToHTML($_POST["tel"]) ?>"></input></td>
					<td></td>
					<td>T�l�phone(2)</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="tel2" name="tel2" value="<?php if(isset($_POST["tel2"])) echo ToHTML($_POST["tel2"]) ?>"></input></td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>
				<tr>
					<td>Courriel</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="mail" name="mail" value="<?php if(isset($_POST["mail"])) echo ToHTML($_POST["mail"]) ?>"></input></td>
					<td></td>
					<td>Courriel(2)</td>
					<td>:</td>
					<td><input type="text"  class ="notauto" size="40" id="mail2" name="mail2" value="<?php if(isset($_POST["mail2"])) echo ToHTML($_POST["mail2"]) ?>"></input></td>
				</tr>											
				</table>
			</div>
			<?php if($boUpdate){ $operation = 'update';?>
				<input type="hidden" name="updateForm" />
			<?php }else{$operation = 'add';?>
				<input type="hidden" name="addForm" />
			<?php }?>			
			<div class="boxBtn" style="padding-top:5px;text-align:left;">
				<table class="buttonTable" border="0" align="left">
					<tr>
						<?php if(!$boUpdate && !isset($_POST['addForm']) || $boUpdate){?>
						<td onclick="window.fetab.Close('valider','<?php echo $operation?>')">Sauvegarder</td>
						<td class="separator"></td>
						
						<td onclick="clearForm('formEtablissement')">Effacer</td>
						<?php }?>
						<?php if($_REQUEST['module']=='etablissementPopup'){?>
						<td class="separator"></td>
						<td onclick="window.fetab.Close('fermer', '<?php echo $operation?>' )">Fermer</td>
						<?php }?>
					</tr>
				</table>
			</div>			
		</div>
		</form>




<?php	
	}
}
?>