<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/excelwriter.class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');

function renderElt($w, $action, $etablissement, $actionContact) {
	
	$etsNom = null;
	$etsCp = null;
	$etsVille = null;
	$etsRue = null;
	//$userNom = null;
	//$userPrenom = null;
	$userTel = null;
	$userEmail = null;
	$userLabel= null;
	$contactNom = null;
	$contactPrenom = null;
	$contactsEtablissement = null;
	$personneRessource = null;
	$dateAction = null;
	
	$dateAction = $action->GetDateActionString(false);
	
			
	if ($etablissement != null) {
		$estNom = $etablissement->GetNom();
		
		$contactsEtablissement = $etablissement->GetContacts();
		
		if ($etablissement->GetUtilisateur() != null)
		{
			//$userNom = $etablissement->GetUtilisateur()->GetNom();
			//$userPrenom = $etablissement->GetUtilisateur()->GetPrenom();
			//$userTel = $etablissement->GetUtilisateur()->GetTel();
			//$userEmail = $etablissement->GetUtilisateur()->GetEmail();
			$userLabel = $etablissement->GetUtilisateur()->GetLabel();
		}
		$etsCp = $etablissement->GetAdresse()->GetCodePostal();
		$etsVille = $etablissement->GetAdresse()->GetVille();
		$etsRue = $etablissement->GetAdresse()->GetRue();
	}
	
	if ($actionContact != null) {
		$contact = $actionContact->GetContact();
		
		if ($contact) {
			$contactNom = $contact->GetNom();
			$contactPrenom = $contact->GetPreNom();
			$userTel = $contact->GetAdresse()->GetTel1();
			$userEmail = $contact->GetAdresse()->GetEmail1();
			
			if ($contactsEtablissement != null && $contactsEtablissement->Count() > 0) {
				
				for ($i=0; $i < $contactsEtablissement->Count(); $i++ ) {
					$contactEtablissement = $contactsEtablissement->items($i);
					$tempEtablissementContact = $contactEtablissement->getContact();
					if ($tempEtablissementContact && $tempEtablissementContact->GetContactId() ==  $contact->GetContactId()) {
						$tempParticipation = $contactEtablissement->getParticipation();
						if ($tempParticipation) $personneRessource = $tempParticipation->getLabel();
						break;
					}
					
				}
				
			}			
		}
	}

	$w->newRow(null, null);		
		
	$c = $w->setString($action->GetNom(), 'cell');
	$c = $w->setString($action->GetTypeActionId(), 'cell');	
	$c = $w->setString(isset($estNom) ? $estNom : '&nbsp;', 'cell');
	$c = $w->setString(isset($etsRue) ? $etsRue : '&nbsp;', 'cell');
	$c = $w->setString(isset($etsCp) ? $etsCp->GetCodepostal() : '&nbsp;', 'cell');
	$c = $w->setString(isset($etsVille) ? $etsVille : '&nbsp;', 'cell');	
	$c = $w->setString(isset($contactNom) ? $contactNom : '&nbsp;', 'cell');
	$c = $w->setString(isset($contactPrenom) ? $contactPrenom : '&nbsp;', 'cell');	
	$c = $w->setString(isset($userEmail) ? $userEmail : '&nbsp;', 'cell');	
	$c = $w->setString(isset($userTel) ? $userTel : '&nbsp;', 'cell');
	$c = $w->setString(isset($dateAction) ? $dateAction : '&nbsp;', 'cell');	
	$c = $w->setString(isset($userLabel) ? $userLabel : '&nbsp;', 'cell');
	// $c = $w->setString(isset($personneRessource) && $personneRessource == 1 ? ' OUI ' : ' - ', 'cell');	
		

}

$e = new ExcelWriter('ASE');
$colSize = '';
	$e->setStyle('
  <Style ss:ID="numeric">
   <NumberFormat ss:Format="0.0"/>
  </Style>
  <Style ss:ID="simple_bold">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  
  <Style ss:ID="filter_title">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  
  <Style ss:ID="filter_title_red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style>  
  
    <Style ss:ID="red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style> 
  
  
  <Style ss:ID="center_cell">
   <Alignment ss:Horizontal="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>
  
  <Style ss:ID="cell">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>

  
    <Style ss:ID="cell_orange">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#FF9966" ss:Pattern="Solid"/>
  </Style>  
  
   <Style ss:ID="cell_vert">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#33CC33" ss:Pattern="Solid"/>
  </Style>
  
     <Style ss:ID="cell_gris">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#DDDDDD" ss:Pattern="Solid"/>
  </Style>
  
    
  <Style ss:ID="last_line">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  ');
	
	$w = $e->initWorksheet(1, 'Listing sur les activit�s');
	$w->newRow(null, null);
	$c = $w->setString('Listing sur les activit�s','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	
	$w->newRow(null, null);
	
	$w->newRow(null, null);
	$agent = '';
	if($postEtab['agent']=='-1')$agent = 'Tous';else if($postEtab['agent']=='-2')$agent = 'Pas attribu�';
	else
	{
		$dbUser = new UtilisateurDatabase();
		$dbUser->open();
		$agent = new Utilisateurs($dbUser->get($postEtab['agent'], USER_TYPE_AGENT));
		$dbUser->close();
		$agent = $agent->items(0)->getNom(). ' ' .$agent->items(0)->getPrenom();
	}
	
	
	$c = $w->setString('Agent : '.$agent);
	
	$w->newRow(null, null);
	$activite = (isset($postEtab["intitule"]) ? $postEtab["intitule"] : '&nbsp;'); 
	$c = $w->setString('Intitul� : ' . $activite);
	
	$w->newRow(null, null);	
	$annee = (isset($postEtab["anneesScolaire"]) ? $postEtab["anneesScolaire"] : '&nbsp;'); 
	if ($annee == "-1") $annee = "Toutes";
	$c = $w->setString('Ann�e scolaire : ' . $annee);
	
	$w->newRow(null, null);
	if(!empty($postEtab['code_postal_1']) || !empty($postEtab['code_postal_2']))
	{
		$c = $w->setString('Codes postaux : '.'De '.$postEtab['code_postal_1'].' � '.$postEtab['code_postal_2']);
	}
	else
		$c = $w->setString('Codes postaux : ');
	
	$w->newRow(null, null);
	$arrLab = "";	
	foreach ($postEtab as $key => $value)
	{
		if (strpos($key, 'chk_') === 0)
		{
			
			$k = str_replace('chk_', '', $key);
			
			if (strpos($k, 'arr_') === 0)
			{
				$k = str_replace('arr_','',$k);
				$arrLab .= Dictionnaries::getArrondissementList($k)->items(0)->GetLabel().'; ';
			}
		}
	}
	if(strlen($arrLab)<=0)
		$arrLab = 'Tous';
	$c = $w->setString('Arrondissements : '.$arrLab);
	
		
	$w->newRow(null, null);
	$typeActivite = '';
	
	if(isset($postEtab['actionLab'])) {
		$typeActivite .= "Actions labellis�es";		
	}
	
	if(isset($postEtab['actionNonLab'])) {
		$typeActivite .= (strlen($typeActivite) > 0 ? ", " : "") . "Actions Non labellis�es";		
	}
	
	if(isset($postEtab['formations'])) {
		$typeActivite .= (strlen($typeActivite) > 0 ? ", " : "") . "Formations";		
	}
	
	if(isset($postEtab['stac'])) {
		$typeActivite .= (strlen($typeActivite) > 0 ? ", " : "") . "(STAC) Acculturation";		
	}
	
	if(isset($postEtab['ateliers'])) {
		$typeActivite .= (strlen($typeActivite) > 0 ? ", " : "") . "Ateliers";		
	}
	
	if(isset($postEtab['projEntr'])) {
		$typeActivite .= (strlen($typeActivite) > 0 ? ", " : "") . "Projets entrepreneuriaux";		
	}
	
	$c = $w->setString('Type d\'activit� : '. $typeActivite);
	
	
	/*$w->newRow(null, null);
	if(strlen($reseauLab)<=0)
		$reseauLab = 'Tous';
		
	$c = $w->setString('R�seaux : '.$reseauLab);*/	

	$w->newRow(null, null);
		
	$w->newRow(null, null);
	$c = $w->setString('R�sultats', 'red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');	
	$c = $w->setString('','red');
	
	$w->newRow(null, null);
	$c = $w->setString('Intitul�', 'filter_title');
	$c = $w->setString('Type', 'filter_title');
	$c = $w->setString('Etablissement', 'filter_title');
	$c = $w->setString('Adresse', 'filter_title');
	$c = $w->setString('Code postal', 'filter_title');
	$c = $w->setString('Ville', 'filter_title');
	$c = $w->setString('Nom', 'filter_title');
	$c = $w->setString('Pr�nom', 'filter_title');
	$c = $w->setString('Email', 'filter_title');
	$c = $w->setString('Tel', 'filter_title');
	$c = $w->setString('Date', 'filter_title');
	$c = $w->setString('Agent', 'filter_title');
	
	
						
	for ($i = 0; $i < $ets->Count(); $i++)
	{
		$currentAction = $ets->items($i);
		$currentsEtablissement = $currentAction->GetActionEtablissment((isset($postEtab['agent'])&& $postEtab['agent'] !='-1' && $postEtab['agent'] != '-2') ? $postEtab['agent'] : null); //Etablissement[]
		$currentsActionContact= $currentAction->GetActionContact(); // ActionContact[]
		
		// $etsIds .= ($etsIds == null ? '' : ',') . $currentAction->GetActionId();
		
		if ($currentsActionContact->Count() == 0 && $currentsEtablissement->Count()==0) { 
			renderElt($w, $currentAction, null, null);
		} else if ($currentsActionContact->Count() == 0) {
			for ($k=0; $k < $currentsEtablissement->Count() ; $k++) {
				$currentEtablissement = $currentsEtablissement->items($k);
				renderElt($w, $currentAction, $currentEtablissement, null);
			}
		} else {
		
			for ($j=0; $j < $currentsActionContact->Count(); $j++ ) {
				
				$currentActionContact = $currentsActionContact->items($j); // ->GetContact();
				
				$listEtablissementsWithoutContacts = array();
				
				if ($currentsEtablissement->Count() == 0) {
					renderElt($w, $currentAction, null, $currentActionContact);								
				} else {
					
					for ($k=0; $k < $currentsEtablissement->Count() ; $k++) {
						$currentEtablissement = $currentsEtablissement->items($k);
						$currentsContactEtablissement = $currentEtablissement->GetContacts(); // ContactEtablissement[]
						
						$foundContactEtablissement = false;
						
						for ($l=0; $l < $currentsContactEtablissement->Count() ; $l++ ) {
																		
							$tempcontact = $currentActionContact->GetContact();
							$tempcontact2 = $currentsContactEtablissement->items($l)->getContact();
							if (! isset($tempcontact) || ! isset($tempcontact2)) break;
			
							if ($currentActionContact->GetContact()->GetContactId() == $currentsContactEtablissement->items($l)->getContact()->GetContactId()) {
									// le contact de l'action correspond � un contat de l'etablissement
									// => on affiche l'action, le contact, et l'�tablissement li�
									renderElt($w,$currentAction, $currentEtablissement, $currentActionContact);
									$foundContactEtablissement = true;
									break;
							} 
							
						}
						
						if (! $foundContactEtablissement) {
							// on n'a pas trouv� d'�tablissements qui ont le meme contact que celui de l'action
							// => on affiche l'action, le contact mais pas l'�tablissement
							// $this->renderElt($currentAction, null, $currentActionContact);
							// on enregistre tous les Etablissements pour lesquels on n'a pas trouve de contact...
							$listEtablissementsWithoutContacts[$currentEtablissement->GetNom()] = $currentEtablissement;
						}
						
					}
					
					for ($k=0; $k < $currentsEtablissement->Count() ; $k++) {
						// on parcourt tous les etablissements de l'action 
						// et on regarde si un ne serait pas li� aux actionContact
						// si oui -> on affiche l'�tablissement sans contact
						$currentEtablissement = $currentsEtablissement->items($k);
						$nomTemp = $currentEtablissement->GetNom();
						if (! isset($listEtablissementsWithoutContacts[$nomTemp])) {
							// $this->renderElt($currentAction, $currentEtablissement, null);									
						}
					}
				}
				
			}
		}
	}

	$w->setColumns('
   <Column ss:AutoFitWidth="1" ss:Width="200"/>
   <Column ss:AutoFitWidth="1" ss:Width="100"/>
   <Column ss:AutoFitWidth="1" ss:Width="200"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
  '.
	$colSize
	);
	$e->outputAsFile('Listing_actions.xls');
?>