<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/excelwriter.class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
$e = new ExcelWriter('ASE');

function renderElt($w, $contact, $contactEtablissement, $etablissement, $action) {

	$userNom = null;
	$userPrenom = null;
	$userTel = null;
	$userEmail = null;
	$userLabel = null;
	// $currentAction = null;
	$contactNom = null;
	$contactPrenom = null;
	$etablissementNom = null;
	$titreContact = null;
	$titreContact_label = null;
	$matiereContact = null;
	$matiereContact_label = null;
	$nomAction = null;
	$typeAction = null;
	$cpEts = null;
	$villeEts = null;
	$rueEts = null;
	
	$personneRessource = null;
	
	$contactNom = $contact->GetNom();
	$contactPrenom =  $contact->GetPrenom();
	$userTel = $contact->GetAdresse()->GetTel1();
	$userEmail = $contact->GetAdresse()->GetEmail1();
	
	if ($contactEtablissement != null) { 
		$titreContact = $contactEtablissement->getTitreContact();
		
		if ($titreContact != null) $titreContact_label = $titreContact->getLabel();
		
		$matiereContact = $contactEtablissement->getSpecialiteContact();
		if ($matiereContact != null) $matiereContact_label = $matiereContact->getLabel();
		
		
		$tempParticipation = $contactEtablissement->getParticipation();
		if ($tempParticipation) $personneRessource = $tempParticipation->getLabel();
	}
	
	if ($etablissement != null) {
		$etablissementNom = $etablissement->GetNom();
		
		$utilisateur = $etablissement->GetUtilisateur() ;
		
		if($utilisateur != null)
		{
			//$userNom = $utilisateur->GetNom();
			//$userPrenom = $utilisateur->GetPrenom();
			//$userTel = $utilisateur->GetTel();
			//$userEmail = $utilisateur->GetEmail();
			$userLabel = $utilisateur->GetLabel();
		}
		
		$adresse_temp = $etablissement->GetAdresse();
		
		if ($adresse_temp != null) {
			$cpEts = $adresse_temp->GetCodePostal();
			$villeEts = $adresse_temp->GetVille();
			$rueEts = $adresse_temp->GetRue();
		}
	}
	
	if ($action != null) {	
		$nomAction = $action->GetNomGen() ; 
		$typeAction = $action->GetTypeActionId();
	}
	
	$w->newRow(null, null);		
	
	$c = $w->setString(isset($contactNom) ? $contactNom : '&nbsp;', 'cell');
	$c = $w->setString(isset($contactPrenom) ? $contactPrenom : '&nbsp;', 'cell');
	$c = $w->setString(isset($titreContact_label) ? $titreContact_label : '&nbsp;', 'cell');
	$c = $w->setString(isset($matiereContact_label) ? $matiereContact_label : '&nbsp;', 'cell');
	$c = $w->setString(isset($userEmail) ? $userEmail : '&nbsp;', 'cell');
	$c = $w->setString(isset($userTel) ? $userTel : '&nbsp;', 'cell');
	$c = $w->setString(isset($personneRessource) && $personneRessource == 1 ? ' OUI ' : ' NON ', 'cell');
	$c = $w->setString(isset($etablissementNom) ? $etablissementNom : '&nbsp;', 'cell');
	$c = $w->setString(isset($rueEts) ? $rueEts : '&nbsp;', 'cell');
	$c = $w->setString(isset($cpEts) ? $cpEts->GetCodepostal() : '&nbsp;', 'cell');
	$c = $w->setString(isset($villeEts) ? $villeEts : '&nbsp;', 'cell');
	$c = $w->setString(isset($userLabel) ? $userLabel : '&nbsp;', 'cell');
	
	
	//$c = $w->setString(isset($nomAction) ? $nomAction : '&nbsp;', 'cell');
	//$c = $w->setString(isset($typeAction) ? $typeAction : '&nbsp;', 'cell');
	
	// isset($cpEts) ? $cpEts->GetCodepostal() : '&nbsp;');
	
}

$colSize = '';
	$e->setStyle('
  <Style ss:ID="numeric">
   <NumberFormat ss:Format="0.0"/>
  </Style>
  <Style ss:ID="simple_bold">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  
  <Style ss:ID="filter_title">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  
  <Style ss:ID="filter_title_red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style>  
  
    <Style ss:ID="red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style> 
  
  
  <Style ss:ID="center_cell">
   <Alignment ss:Horizontal="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>
  
  <Style ss:ID="cell">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>

  
    <Style ss:ID="cell_orange">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#FF9966" ss:Pattern="Solid"/>
  </Style>  
  
   <Style ss:ID="cell_vert">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#33CC33" ss:Pattern="Solid"/>
  </Style>
  
     <Style ss:ID="cell_gris">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#DDDDDD" ss:Pattern="Solid"/>
  </Style>
  
    
  <Style ss:ID="last_line">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  ');
	
	$w = $e->initWorksheet(1, 'Listing sur les contacts');
	$w->newRow(null, null);
	$c = $w->setString('Listing sur les contacts','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');	
	
	$w->newRow(null, null);
	
	$w->newRow(null, null);
	$agent = '';
	if($postEtab['agent']=='-1')$agent = 'Tous';else if($postEtab['agent']=='-2')$agent = 'Pas attribu�';
	elseif ($postEtab['agent']=='') $agent = "Tous";
	else
	{
		$dbUser = new UtilisateurDatabase();
		$dbUser->open();
		$agent = new Utilisateurs($dbUser->get($postEtab['agent'], USER_TYPE_AGENT));
		$dbUser->close();
		$agent = $agent->items(0)->getNom(). ' ' .$agent->items(0)->getPrenom();
	}
	
	
	$c = $w->setString('Agent : '.$agent);
	
	//$w->newRow(null, null);
	//$activite = (isset($postEtab["intitule"]) ? $postEtab["intitule"] : '&nbsp;'); 
	//$c = $w->setString('Activit� : ' . $activite);
	
	$w->newRow(null, null);
	if(!empty($postEtab['code_postal_1']) || !empty($postEtab['code_postal_2']))
	{
		$c = $w->setString('Codes postaux : '.'De '.$postEtab['code_postal_1'].' � '.$postEtab['code_postal_2']);
	}
	else
		$c = $w->setString('Codes postaux : ');
	
	$w->newRow(null, null);
	if(!empty($postEtab['annee_ante']) || !empty($postEtab['annee_post']))
	{
		$c = $w->setString('Dates : '.'Entre '.$postEtab['annee_ante'].' et '.$postEtab['annee_post']);
	}
	else
		$c = $w->setString('Dates : ');
	
	$w->newRow(null, null);
	$titre = '&nbsp;';
	if (isset($postEtab["titre"]) && $postEtab["titre"] != -1) {
		$titres = Dictionnaries::getTitreContactList($postEtab["titre"]);
		if ($titres != null && $titres->count() > 0 && $titres->items(0) != null)
			$titre = $titres->items(0)->getLabel();
	}
	
	// $titre = (isset($postEtab["titre"]) && $postEtab["titre"] != -1 ? $postEtab["titre"] : '&nbsp;');
	
	$c = $w->setString('Titre : ' . $titre);
	
	$w->newRow(null, null);
	$matiere = '&nbsp;';
	if (isset($postEtab["matiere"]) && $postEtab["matiere"] != -1) {
		$matieres = Dictionnaries::getSpecialiteList(($postEtab["matiere"]));
		if ($matieres != null && $matieres->count() > 0 && $matieres->items(0) != null)
			$matiere = $matieres->items(0)->getLabel();
	}
	
	
	// $matiere = (isset($postEtab["matiere"]) ? $postEtab["matiere"] : '&nbsp;');
	 
	$c = $w->setString('Mati�re : ' . $matiere);
	
		
	$w->newRow(null, null);
	$typePersonne= '';
	if(isset($postEtab['personneRes'])) {
		$typePersonne .= "Personnes Resources";		
	}
	if(isset($postEtab['personneResNot'])) {
		$typePersonne .= (strlen($typePersonne) > 0 ? ", " : "") . "Contacts";		
	}
	
	$c = $w->setString('Type de personnes : '. $typePersonne);
	
	
	/*$w->newRow(null, null);
	if(strlen($reseauLab)<=0)
		$reseauLab = 'Tous';
		
	$c = $w->setString('R�seaux : '.$reseauLab);*/	

	$w->newRow(null, null);
		
	$w->newRow(null, null);
	$c = $w->setString('R�sultats', 'red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');		
	
	$w->newRow(null, null);
	$c = $w->setString('Nom', 'filter_title');
	$c = $w->setString('Pr�nom', 'filter_title');
	$c = $w->setString('Titre', 'filter_title');
	$c = $w->setString('Mati�re', 'filter_title');
	$c = $w->setString('Email', 'filter_title');
	$c = $w->setString('Tel', 'filter_title');
	$c = $w->setString('Pers Res', 'filter_title');
	$c = $w->setString('Etablissement', 'filter_title');
	$c = $w->setString('Adresse', 'filter_title');	
	$c = $w->setString('Code postal', 'filter_title');
	$c = $w->setString('Ville', 'filter_title');
	$c = $w->setString('Agent', 'filter_title');
	
	//$c = $w->setString('Activit�', 'filter_title');
	// $c = $w->setString('Type', 'filter_title');
		
						
	for ($i = 0; $i < $ets->Count(); $i++)
	{
		$currentContact = $ets->items($i);
		$currentsContactEtablissement = $currentContact->GetEtablissementsContacts(); // ContactEtablissement[]
		
		// $etsIds .= ($etsIds == null ? '' : ',') . $currentContact->GetContactId();

		if ($currentsContactEtablissement->Count() == 0) {
			renderElt($w, $currentContact, null, null, null);
			
		} else {

			for ($j=0; $j < $currentsContactEtablissement->Count(); $j++) {
				$currentContactEtablissement = $currentsContactEtablissement->items($j);
				$currentEtablissement = $currentContactEtablissement->GetEtablissement(); //Etablissement !
				if ($currentEtablissement == null) {
					renderElt($w, $currentContact, $currentContactEtablissement, null, null);									
				} else {
					// $currentsAction = $currentEtablissement->GetActions(null,null); // ActionEtablissement[]
					
					// if ( $currentsAction->Count() ==0 ) {
						renderElt($w, $currentContact, $currentContactEtablissement, $currentEtablissement, null);
					/*} else {		
						for ($k=0; $k < $currentsAction->Count(); $k++) {
							$currentAction = $currentsAction->items($k)->GetAction(); // Action !
							renderElt($w, $currentContact, $currentContactEtablissement, $currentEtablissement, $currentAction);
							
						}
					}*/
				}
				
			}
		}
		
	}


	$w->setColumns('
   <Column ss:AutoFitWidth="1" ss:Width="200"/>
   <Column ss:AutoFitWidth="1" ss:Width="100"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
  '.
	$colSize
	);
	$e->outputAsFile('Listing_contacts.xls');
?>