<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/excelwriter.class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');

function renderElt($w, $ets) {
	
	$w->newRow(null, null);
		
	$userLabel = null;
	$adresse = null;
	$cp = null;
	$ville = null;
	$rue = null;
	$province = null;
	$arrondissement = null;
	
	$adresse = $ets->GetAdresse();
		
	if (isset($adresse)) {
		$cp_temp = $adresse->GetCodePostal();
		if (isset ($cp_temp)) {
			$cp = $cp_temp->GetCodePostal();
			$arrondissement_temp =  $cp_temp->GetArrondissement();
			if (isset($arrondissement_temp)) $arrondissement = $arrondissement_temp->getLabel();				
		}
		$ville = $adresse->GetVille();
		$rue = $adresse->GetRue();
		$province_tmp = $adresse->GetProvince();
		if (isset($province_tmp)) $province = $province_tmp->getLabel();
	}
	
	
	if($ets->GetUtilisateur() != null)
	{
		$userLabel = $ets->GetUtilisateur()->GetLabel();
	}

	$c = $w->setString($ets->GetNom(), 'cell');
	$c = $w->setString(isset($rue) ? $rue : '&nbsp;', 'cell');
	$c = $w->setString(isset($cp) ? $cp : '&nbsp;', 'cell');
	$c = $w->setString(isset($ville) ? $ville : '&nbsp;', 'cell');
	$c = $w->setString(isset($arrondissement) ? $arrondissement : '&nbsp;', 'cell');
	$c = $w->setString(isset($province) ? $province : '&nbsp;', 'cell');
	$c = $w->setString($ets->GetReseauEtablissement(), 'cell');
	$c = $w->setString($ets->GetNiveauEtablissement(), 'cell');
	
	$c = $w->setString(isset($userLabel) ? $userLabel : '&nbsp;', 'cell');

}

$e = new ExcelWriter('ASE');
$colSize = '';
	$e->setStyle('
  <Style ss:ID="numeric">
   <NumberFormat ss:Format="0.0"/>
  </Style>
  <Style ss:ID="simple_bold">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  
  <Style ss:ID="filter_title">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  
  <Style ss:ID="filter_title_red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style>  
  
    <Style ss:ID="red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style> 
  
  
  <Style ss:ID="center_cell">
   <Alignment ss:Horizontal="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>
  
  <Style ss:ID="cell">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>

  
    <Style ss:ID="cell_orange">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#FF9966" ss:Pattern="Solid"/>
  </Style>  
  
   <Style ss:ID="cell_vert">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#33CC33" ss:Pattern="Solid"/>
  </Style>
  
     <Style ss:ID="cell_gris">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#DDDDDD" ss:Pattern="Solid"/>
  </Style>
  
    
  <Style ss:ID="last_line">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  ');
	
	$w = $e->initWorksheet(1, 'Listing sur les �tablissements scolaires');
	$w->newRow(null, null);
	$c = $w->setString('Listing sur les �tablissements scolaires','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	
	$w->newRow(null, null);
	
	$w->newRow(null, null);
	$agent = '';
	if($postEtab['agent']=='-1')$agent = 'Tous';else if($postEtab['agent']=='-2')$agent = 'Pas attribu�';
	else
	{
		$dbUser = new UtilisateurDatabase();
		$dbUser->open();
		$agent = new Utilisateurs($dbUser->get($postEtab['agent'], USER_TYPE_AGENT));
		$dbUser->close();
		$agent = $agent->items(0)->getNom(). ' ' .$agent->items(0)->getPrenom();
	}
	
	
	$c = $w->setString('Agent : '.$agent);
	
	
	$w->newRow(null, null);
	$activite = (isset($postEtab["activite"]) ? $postEtab["activite"] : '&nbsp;'); 
	$c = $w->setString('Activit� : ' . $activite);
	
	$arrLab = "";
	
	foreach ($postEtab as $key => $value)
	{
		
		if (strpos($key, 'chk_') === 0)
		{
			
			$k = str_replace('chk_', '', $key);
			
			if (strpos($k, 'arr_') === 0)
			{
				$k = str_replace('arr_','',$k);
				$arrLab .= Dictionnaries::getArrondissementList($k)->items(0)->GetLabel().'; ';
			}
		}
	}
	
	/*$w->newRow(null, null);
	if(strlen($provinceLab)<=0)
		$provinceLab = 'Tous';
		
	$c = $w->setString('Provinces : '.$provinceLab);
	*/
	
	$w->newRow(null, null);
	if(strlen($arrLab)<=0)
		$arrLab = 'Tous';
		
	$c = $w->setString('Arrondissements : '.$arrLab);
	
	$w->newRow(null, null);
	if(!empty($postEtab['code_postal_1']) || !empty($postEtab['code_postal_2']))
	{
		$c = $w->setString('Codes postaux : '.'De '.$postEtab['code_postal_1'].' � '.$postEtab['code_postal_2']);
	}
	else
		$c = $w->setString('Codes postaux : ');
	
	/*$w->newRow(null, null);
	if(strlen($reseauLab)<=0)
		$reseauLab = 'Tous';
		
	$c = $w->setString('R�seaux : '.$reseauLab);*/	

	$w->newRow(null, null);
		
	$w->newRow(null, null);
	$c = $w->setString('R�sultats', 'red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');	
	
	$w->newRow(null, null);
	$c = $w->setString('Etablissement', 'filter_title');
	$c = $w->setString('Adresse', 'filter_title');
	$c = $w->setString('Code postal', 'filter_title');
	$c = $w->setString('Ville', 'filter_title');
	$c = $w->setString('Arrondissement', 'filter_title');
	$c = $w->setString('Province', 'filter_title');
	$c = $w->setString('R�seau', 'filter_title');
	$c = $w->setString('Niveau', 'filter_title');
	$c = $w->setString('Agent', 'filter_title');
	
	$statutActiviteOK = 0;
	$statutActivitePasse = 0;
	$statutActiviteJamais = 0;
	$statutActiviteGlobal = 0;
	$statutVisiteOK = 0;
	$statutVisitePasse = 0;
	$statutVisiteJamais = 0;
	$statutVisiteGlobal = 0;
						
	for ($i = 0; $i < $ets->Count(); $i++)
	{
		
		renderElt($w, $ets->items($i));	
		
	}
	
		
		/*
		  
		$c = $w->setString(isset($cp) ? $cp->GetProvince()->getLabel() : '&nbsp;', 'cell');
		$c = $w->setString($ets->items($i)->GetReseauEtablissement(), 'cell');
		$c = $w->setString($ets->items($i)->GetNiveauEtablissement(), 'cell');
		$activity = $ets->items($i)->getActivity();
		
		switch ($activity)
		{
			case -1 :
				
				//orange
				$c = $w->setString('Avant', 'cell_orange');
				$statutActivitePasse++;
				break;
			case 0 :
				//gris
				$c = $w->setString('Jamais', 'cell_gris');
				$statutActiviteJamais++;
				break;
			case 1 :
				//vert
				$c = $w->setString('OK', 'cell_vert');
				$statutActiviteOK++;
				break;
		}
		$statutActiviteGlobal++;
		$visited = $ets->items($i)->getVisited();
		
		switch ($visited)
		{
			case -1 :
				//orange
				$c = $w->setString('Avant', 'cell_orange');
				$statutVisitePasse++;
				break;
			case 0 :
				//gris
				$c = $w->setString('Jamais', 'cell_gris');
				$statutVisiteJamais++;
				break;
			case 1 :
				//vert
				$c = $w->setString('OK', 'cell_vert');
				$statutVisiteOK++;
				break;
		}
		$statutVisiteGlobal++;
		$agent = $ets->items($i)->GetUtilisateur();
		$c = $w->setString(isset($agent) ? $agent->GetLabel() : '&nbsp;', 'cell');
		*/


	/*

	$w->newRow(null, null);
	$c = $w->setString('', 'filter_title');	
	$c = $w->setString('', 'filter_title');
	$c = $w->setString('', 'filter_title');
	$c = $w->setString('', 'filter_title');
	$c = $w->setString('', 'filter_title');
	$c = $w->setString('', 'filter_title');
	$c = $w->setString('Total', 'filter_title');
	$c = $w->setNumber($statutActiviteGlobal, 'filter_title');
	$c = $w->setNumber($statutVisiteGlobal, 'filter_title');
	
	
	$w->newRow(null, null);
	$c = $w->setString('', '');	
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('OK', 'cell_vert');
	$c = $w->setNumber($statutActiviteOK, 'cell_vert');
	$c = $w->setNumber($statutVisiteOK, 'cell_vert');
	
	$w->newRow(null, null);
	$c = $w->setString('', '');	
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('Avant', 'cell_orange');
	$c = $w->setNumber($statutActivitePasse, 'cell_orange');
	$c = $w->setNumber($statutVisitePasse, 'cell_orange');

	$w->newRow(null, null);
	$c = $w->setString('', '');	
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('', '');
	$c = $w->setString('Jamais', 'cell_gris');	
	$c = $w->setNumber($statutActiviteJamais, 'cell_gris');
	$c = $w->setNumber($statutVisiteJamais, 'cell_gris');
	
	*/
	
//$etab;
//$postEtab;
	$w->setColumns('
   <Column ss:AutoFitWidth="1" ss:Width="200"/>
   <Column ss:AutoFitWidth="1" ss:Width="100"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   
  '.
	$colSize
	);
	$e->outputAsFile('Listing_etablissements_scolaires.xls');
?>