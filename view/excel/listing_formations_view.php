<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/excelwriter.class.php');
REQUIRE_ONCE(SCRIPTPATH.'model/utilisateur_database_model_class.php');
$e = new ExcelWriter('ASE');

function renderElt($w, $formation, $formationParticipant) {

	$userNom = null;
	$userPrenom = null;
	$userGenre = null;
	$userAge = null;
	$userSpecialite = null;
	$formateur = null;
	$typeFormation = null;
	$dateFormation = null;
	
	if ($formationParticipant != null) {
					
		$userNom = $formationParticipant->nom;
		$userPrenom =  $formationParticipant->prenom;
		$userGenre = $formationParticipant->getGenreName();
		$tempAge = $formationParticipant->getAgeName();
		$userAge = ($tempAge ? $tempAge : null);
		$userSpecialite = '???';
	}
	
	$formateurtemp = $formation->getFormateur();
	if ($formateurtemp != null) $formateur = $formateurtemp->getLabel();
	
	$typeFormation = $formation->getTypeFormationId();
	
	$dateFormation = $formation->getDateFormationString(false);
	
	$w->newRow(null, null);	
	
	$c = $w->setString(isset($userNom) ? $userNom : '&nbsp;', 'cell');
	$c = $w->setString(isset($userPrenom) ? $userPrenom : '&nbsp;', 'cell');
	$c = $w->setString(isset($userGenre) ? $userGenre : '&nbsp;', 'cell');	
	$c = $w->setString(isset($userAge) ? $userAge : '&nbsp;', 'cell');
	$c = $w->setString(isset($userSpecialite) ? $userSpecialite : '&nbsp;', 'cell');
	$c = $w->setString(isset($formateur) ? $formateur : '&nbsp;', 'cell');
	$c = $w->setString(isset($typeFormation) ? $typeFormation : '&nbsp;', 'cell');
	$c = $w->setString(isset($dateFormation) ? $dateFormation : '&nbsp;', 'cell');
	
	
	//$c = $w->setString(isset($nomAction) ? $nomAction : '&nbsp;', 'cell');
	//$c = $w->setString(isset($typeAction) ? $typeAction : '&nbsp;', 'cell');
	
	// isset($cpEts) ? $cpEts->GetCodepostal() : '&nbsp;');
	
}

$colSize = '';
	$e->setStyle('
  <Style ss:ID="numeric">
   <NumberFormat ss:Format="0.0"/>
  </Style>
  <Style ss:ID="simple_bold">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  
  <Style ss:ID="filter_title">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  
  <Style ss:ID="filter_title_red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style>  
  
    <Style ss:ID="red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style> 
  
  
  <Style ss:ID="center_cell">
   <Alignment ss:Horizontal="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>
  
  <Style ss:ID="cell">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>

  
    <Style ss:ID="cell_orange">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#FF9966" ss:Pattern="Solid"/>
  </Style>  
  
   <Style ss:ID="cell_vert">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#33CC33" ss:Pattern="Solid"/>
  </Style>
  
     <Style ss:ID="cell_gris">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#DDDDDD" ss:Pattern="Solid"/>
  </Style>
  
    
  <Style ss:ID="last_line">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  ');
	
	$w = $e->initWorksheet(1, 'Listing FSE');
	$w->newRow(null, null);
	$c = $w->setString('Listing FSE','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	
	$w->newRow(null, null);
	
	/*$w->newRow(null, null);
	$agent = '';
	if($postEtab['agent']=='-1')$agent = 'Tous';else if($postEtab['agent']=='-2')$agent = 'Pas attribu�';
	elseif ($postEtab['agent']=='') $agent = "Tous";
	else
	{
		$dbUser = new UtilisateurDatabase();
		$dbUser->open();
		$agent = new Utilisateurs($dbUser->get($postEtab['agent'], USER_TYPE_AGENT));
		$dbUser->close();
		$agent = $agent->items(0)->getNom(). ' ' .$agent->items(0)->getPrenom();
	}*/
	
	
	// $c = $w->setString('Agent : '.$agent);
	
	$w->newRow(null, null);
	$activite = (isset($postEtab["intitule"]) ? $postEtab["intitule"] : '&nbsp;'); 
	$c = $w->setString('Formation : ' . $activite);
	
	$w->newRow(null, null);
	if(!empty($postEtab['annee_ante']) || !empty($postEtab['annee_post']))
	{
		$c = $w->setString('Dates : '.'Entre '.$postEtab['annee_ante'].' et '.$postEtab['annee_post']);
	}
	else
		$c = $w->setString('Dates : ');
		
	
	/*$w->newRow(null, null);
	if(strlen($reseauLab)<=0)
		$reseauLab = 'Tous';
		
	$c = $w->setString('R�seaux : '.$reseauLab);*/	

	$w->newRow(null, null);
		
	$w->newRow(null, null);
	$c = $w->setString('R�sultats', 'red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	
	$w->newRow(null, null);
	$c = $w->setString('Nom', 'filter_title');
	$c = $w->setString('Pr�nom', 'filter_title');
	$c = $w->setString('Genre', 'filter_title');
	$c = $w->setString('Tranche d\'age', 'filter_title');
	$c = $w->setString('Sp�cialit�', 'filter_title');
	$c = $w->setString('Formateur', 'filter_title');
	$c = $w->setString('Type de formation', 'filter_title');
	$c = $w->setString('Date', 'filter_title');	
		
	//$c = $w->setString('Activit�', 'filter_title');
	// $c = $w->setString('Type', 'filter_title');
		
	for ($i = 0; $i < $ets->Count(); $i++)
	{
		$currentFormation = $ets->items($i);
		$currentParticipants = $currentFormation->getParticipants();
		
		if ($currentParticipants->Count() == 0) { 
			// renderElt($w, $currentFormation, null);
		} else {
			for ($j=0; $j < $currentParticipants->Count(); $j++) {
				$currentParticipant = $currentParticipants->items($j);								
				renderElt($w, $currentFormation, $currentParticipant);
			}
		}												
	}						


	$w->setColumns('
   <Column ss:AutoFitWidth="1" ss:Width="200"/>
   <Column ss:AutoFitWidth="1" ss:Width="100"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>
   <Column ss:AutoFitWidth="1" ss:Width="120"/>

  '.
	$colSize
	);
	$e->outputAsFile('Listing_formations.xls');
?>