<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/excelwriter.class.php');
$e = new ExcelWriter('ASE');
/*
 * D�finition de styles, sans doute le plus ennuyant
 */
$colSize = '';
	$e->setStyle('
  <Style ss:ID="numeric">
   <NumberFormat ss:Format="0.0"/>
  </Style>
  <Style ss:ID="simple_bold">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  
  <Style ss:ID="filter_title">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  
  <Style ss:ID="filter_title_red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style>  
  
    <Style ss:ID="red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style> 
  
  <Style ss:ID="center_cell">
   <Alignment ss:Horizontal="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>
  
  <Style ss:ID="cell">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>  
  
    
  <Style ss:ID="last_line">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  ');

	$w = $e->initWorksheet(1, 'Rapport sur les projets entrepreneuriaux');
	$w->newRow(null, null);
	$c = $w->setString('Rapport sur les projets entrepreneuriaux','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	
	$w->newRow(null, null);
	$c = $w->setString('Ann�e scolaire : '.$postAction['anneesScolaire']);
	//$colSize .='<Column ss:AutoFitWidth="1" ss:Width="100"/>'; 
	
	
	
	$statutSubvention = '';
	$statutSubvention .= (isset($postAction['sub_obtenu'])) ? 'Obtenu;' : '';
	$statutSubvention .= (isset($postAction['sub_refus'])) ? ' Refus�;' : ''; 
	$statutSubvention .= (isset($postAction['sub_eval'])) ? ' En analyse;' : '';
	$w->newRow(null, null);
	$c = $w->setString('Statut de subvention : '.$statutSubvention);
	//$colSize .='<Column ss:AutoFitWidth="1" ss:Width="100"/>'; 

	$w->newRow(null, null);
	$c = $w->setString('pour les �tablissements pris en compte selon les crit�res suivants :','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	

	$provincesArray = array();
	$arrondissementsArray = array();
	$reseauxArray = array();
	$niveauxArray = array();
	$provinceLab = '';		
	$niveauLab = '';
	$arrLab = '';
	$reseauLab = '';
	foreach ($postAction as $key => $value)
	{
		
		if (strpos($key, 'chk_') === 0)
		{
			
			$k = str_replace('chk_', '', $key);
			
			if (strpos($k, 'pro_') === 0)
			{
				
				$k = str_replace('pro_','',$k);
				$provinceLab .= Dictionnaries::getProvincesList($k)->items(0)->GetLabel().'; ';
			}
			elseif (strpos($k, 'niv_') === 0) 
			{
				$k = str_replace('niv_','',$k);
				$niveauLab .= Dictionnaries::getNiveauxList($k)->items(0)->GetLabel().'; ';
			}
			elseif (strpos($k, 'arr_') === 0)
			{
				$k = str_replace('arr_','',$k);
				$arrLab .= Dictionnaries::getArrondissementList($k)->items(0)->GetLabel().'; ';
			}
			elseif (strpos($k, 'res_') === 0)
			{
				$k = str_replace('res_','',$k);
				$reseauLab .= Dictionnaries::getReseauxList($k)->items(0)->GetLabel().'; ';
			}
		}
	}
	
	$w->newRow(null, null);
	if(strlen($provinceLab)<=0)
		$provinceLab = 'Tous';
		
	$c = $w->setString('Provinces : '.$provinceLab);
	
	$w->newRow(null, null);
	if(strlen($arrLab)<=0)
		$arrLab = 'Tous';
		
	$c = $w->setString('Arrondissements : '.$arrLab);
	
	$w->newRow(null, null);
	if(!empty($postAction['code_postal_1']) || !empty($postAction['code_postal_2']))
	{
		$c = $w->setString('Codes postaux : '.'De '.$postAction['code_postal_1'].' � '.$postAction['code_postal_2']);
	}
	else
		$c = $w->setString('Codes postaux : ');
	
	$w->newRow(null, null);
	if(strlen($reseauLab)<=0)
		$reseauLab = 'Tous';
		
	$c = $w->setString('R�seaux : '.$reseauLab);	

	$w->newRow(null, null);
	if(strlen($niveauLabLab)<=0)
		$niveauLabLab = 'Tous';
		
	$c = $w->setString('Niveaux : '.$niveauLabLab);	
	
	$w->newRow(null, null);
	$c = $w->setString('R�sultats', 'filter_title_red');
	$c = $w->setString('', 'filter_title_red');
	$c = $w->setString('', 'filter_title_red');

	$w->newRow(null, null);
	$c = $w->setString('Chiffres globaux','cell');
	$c = $w->setString('Total','center_cell');
	$c = $w->setString('Moyenne','center_cell');

	$w->newRow(null, null);
	$c = $w->setString('Nombre de projets pris en compte','cell');
	$c = $w->setString($actions->count(),'center_cell');
	$c = $w->setString('','center_cell');

	$w->newRow(null, null);
	$c = $w->setString('Nombre global d\'apprenants participant','cell');
	$c = $w->setNumber($actions->getNbApprenantsGlobal(),'center_cell');
	$c = $w->setNumber(round($actions->getNbApprenantsGlobal()/$actions->count(),2),'center_cell');

	$w->newRow(null, null);
	$c = $w->setString('Nombre global d\'enseignants participant','cell');
	$c = $w->setNumber($actions->getNbEnseignantsGlobal(),'center_cell');
	$c = $w->setNumber(round($actions->getNbEnseignantsGlobal()/$actions->count(),2),'center_cell');

	$w->newRow(null, null);
	$c = $w->setString('R�partition par niveau-degr�-fili�re','filter_title');
	$c = $w->setString('','filter_title');
	$c = $w->setString('','filter_title');

	$fdn = Dictionnaries::getFiliereDegreNiveauList();
	for($i=0; $i<$fdn->Count(); $i++)
	{
		$array  = $actions->getArrayFiliere();
		$w->newRow(null, null);
		$c = $w->setString($fdn->items($i)->GetLabel(),'cell');
		if(isset($array[$fdn->items($i)->GetFiliereDegreNiveauId()]))
			$c = $w->setNumber($array[$fdn->items($i)->GetFiliereDegreNiveauId()],'center_cell');
		else
			$c = $w->setString('','center_cell');
			
		if(isset($array[$fdn->items($i)->GetFiliereDegreNiveauId()]))			
			$c = $w->setNumber(round($array[$fdn->items($i)->GetFiliereDegreNiveauId()]/$actions->count(),2),'center_cell');
		else
			$c = $w->setString('','center_cell');
	}
	
	$w->newRow(null, null);
	$c = $w->setString('R�partition par section','filter_title');
	$c = $w->setString('','filter_title');
	$c = $w->setString('','filter_title');
	
	$section = Dictionnaries::getSectionActionList();
	for($i=0; $i<$section->Count(); $i++)
	{	
		$array  = $actions->getArraySection();
		$w->newRow(null, null);
		$c = $w->setString($section->items($i)->GetLabel(),'cell');
		if(isset($array[$section->items($i)->GetSectionActionId()]))
			$c = $w->setNumber($array[$section->items($i)->GetSectionActionId()],'center_cell');
		else
			$c = $w->setString('','center_cell');
		if(isset($array[$section->items($i)->GetSectionActionId()]))	
			$c = $w->setNumber(round($array[$section->items($i)->GetSectionActionId()]/$actions->count(),2),'center_cell');
		else
			$c = $w->setString('','center_cell');
	}
	
	$w->newRow(null, null);
	$c = $w->setString('R�partition par finalit� p�dagogique','filter_title');
	$c = $w->setString('','filter_title');
	$c = $w->setString('','filter_title');
	
	$peda = Dictionnaries::getFinalitePedagogList();
	for($i=0; $i<$peda->Count(); $i++)
	{
		$array = $actions->getArrayFinalite();
		$w->newRow(null, null);
		$c = $w->setString($peda->items($i)->GetLabel(),'cell');
		if(isset($array[$peda->items($i)->GetFinalitePedagogId()]))
			$c = $w->setNumber($array[$peda->items($i)->GetFinalitePedagogId()],'center_cell');
		else
			$c = $w->setString('','center_cell');
		if(isset($array[$peda->items($i)->GetFinalitePedagogId()]))
			$c = $w->setNumber(round($array[$peda->items($i)->GetFinalitePedagogId()]/$actions->count(),2),'center_cell');
		else
			$c = $w->setString('','center_cell');
	}	
	
	$w->newRow(null, null);
	$c = $w->setString('Montants','filter_title');
	$c = $w->setString('','filter_title');
	$c = $w->setString('','filter_title');	
	$w->newRow(null, null);
	$c = $w->setString('Montant octroy�','cell');
	$c = $w->setNumber(round($actions->getMontantGlobal(),2),'center_cell');
	$c = $w->setNumber(round($actions->getMontantGlobal()/$actions->count(),2),'center_cell');
	$w->newRow(null, null);
	$c = $w->setString('Montant attribu� par apprenant','cell');
	$c = $w->setString('','center_cell');
	
	if($actions->getNbApprenantsGlobal()!=0)
		$c = $w->setNumber(round($actions->getMontantGlobal()/$actions->getNbApprenantsGlobal(),2),'center_cell');
		
	else
		$c = $w->setNumber(0,'center_cell');
	
	
	
	$w->setColumns('
   <Column ss:AutoFitWidth="0" ss:Width="500"/>
  '.
	$colSize
	);
	
	$e->outputAsFile('Rapport_projets_entrepreneuriaux.xls');
	
?>