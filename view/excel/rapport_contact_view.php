<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/excelwriter.class.php');
$e = new ExcelWriter('ASE');
$colSize = '';
	$e->setStyle('
  <Style ss:ID="numeric">
   <NumberFormat ss:Format="0.0"/>
  </Style>
  <Style ss:ID="simple_bold">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  
  <Style ss:ID="filter_title">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  
  <Style ss:ID="filter_title_red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style>

  <Style ss:ID="red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style>   
  
  <Style ss:ID="center_cell">
   <Alignment ss:Horizontal="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>
  
  <Style ss:ID="cell">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>  
  <Style ss:ID="last_line">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  ');
	
		//total
		$total_bxl = 0; $total_bwa = 0; $total_hai = 0; $total_lie = 0; $total_lux = 0; $total_nam = 0;
		
		//*****************************************************GENRE**********************************************************************
		//Masculin
		$masculin_bxl = 0; $masculin_bwa = 0; $masculin_hai = 0; $masculin_lie = 0; $masculin_lux = 0; $masculin_nam = 0;
		//f�minin
		$feminin_bxl = 0; $feminin_bwa = 0; $feminin_hai = 0; $feminin_lie = 0; $feminin_lux = 0; $feminin_nam = 0;
		//sans valeur
		$sv_genre_bxl = 0; $sv_genre_bwa = 0; $sv_genre_hai = 0; $sv_genre_lie = 0; $sv_genre_lux = 0; $sv_genre_nam = 0;
		//********************************************************************************************************************************
		
		//******************************************************AGE***********************************************************************
		//Entre 15 et 24
		$age1_bxl = 0; $age1_bwa = 0; $age1_hai = 0; $age1_lie = 0; $age1_lux = 0; $age1_nam = 0;
		//Entre 25 et 54
		$age2_bxl = 0; $age2_bwa = 0; $age2_hai = 0; $age2_lie = 0; $age2_lux = 0; $age2_nam = 0;
		//Entre 55 et 64		
		$age3_bxl = 0; $age3_bwa = 0; $age3_hai = 0; $age3_lie = 0; $age3_lux = 0; $age3_nam = 0;
		//64 et plus
		$age4_bxl = 0; $age4_bwa = 0; $age4_hai = 0; $age4_lie = 0; $age4_lux = 0; $age4_nam = 0;
		//Sans valeur
		$sv_age_bxl = 0; $sv_age_bwa = 0; $sv_age_hai = 0; $sv_age_lie = 0; $sv_age_lux = 0; $sv_age_nam = 0;
		//********************************************************************************************************************************		
		
		//******************************************************STATUT*********************************************************************
		//travailleur occup�
		$trav_bxl = 0; $trav_bwa = 0; $trav_hai = 0; $trav_lie = 0; $trav_lux = 0; $trav_nam = 0;
		//Etudiant
		$etud_bxl = 0; $etud_bwa = 0; $etud_hai = 0; $etud_lie = 0; $etud_lux = 0; $etud_nam = 0;
		//autre
		$autre_bxl = 0; $autre_bwa = 0; $autre_hai = 0; $autre_lie = 0; $autre_lux = 0; $autre_nam = 0;
		//Sans valeur
		$sv_statut_bxl = 0; $sv_statut_bwa = 0; $sv_statut_hai = 0; $sv_statut_lie = 0; $sv_statut_lux = 0; $sv_statut_nam = 0;
		//*********************************************************************************************************************************		
		
		for($i=0; $i<count($res['contact']); $i++)
		{
			//echo $res['contact'][$i]['dic_provinceId'].'<br/>';
			//echo $res['contact'][$i]['contactId'].'<br/>';
			switch($res['contact'][$i]['dic_provinceId'])
			{
				case 'BXL' :					
					$total_bxl++;
					if($res['contact'][$i]['genreId']=='M')$masculin_bxl ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_bxl++;
					else $sv_genre_bxl++;
					
					if($res['contact'][$i]['statutId']=='TRAV')$trav_bxl ++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_bxl++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_bxl++;
					else $sv_statut_bxl++;
					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_bxl ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_bxl++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_bxl++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_bxl++;
					else $sv_age_bxl++;
					
					
					break;
				case 'BWA' :					
					$total_bwa++;
					if($res['contact'][$i]['genreId']=='M')$masculin_bwa ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_bwa++;
					else $sv_genre_bwa++;	

					if($res['contact'][$i]['statutId']=='TRAV')$trav_bwa ++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_bwa++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_bwa++;
					else $sv_statut_bwa++;					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_bwa ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_bwa++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_bwa++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_bwa++;
					else $sv_age_bwa++;
					
					break;
				case 'HAI' :					
					$total_hai++;
					if($res['contact'][$i]['genreId']=='M')$masculin_hai ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_hai++;
					else $sv_genre_hai++;				

					if($res['contact'][$i]['statutId']=='TRAV')$trav_hai ++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_hai++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_hai++;
					else $sv_statut_hai++;					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_hai ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_hai++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_hai++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_hai++;
					else $sv_age_hai++;	
					
					break;
				case 'LIE' :					
					$total_lie++;
					if($res['contact'][$i]['genreId']=='M')$masculin_lie ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_lie++;
					else $sv_genre_lie++;		

					if($res['contact'][$i]['statutId']=='TRAV')$trav_lie++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_lie++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_lie++;
					else $sv_statut_lie++;					
					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_lie ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_lie++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_lie++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_lie++;
					else $sv_age_lie++;
					
					break;	
				case 'LUX' :					
					$total_lux++;
					if($res['contact'][$i]['genreId']=='M')$masculin_lux ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_lux++;
					else $sv_genre_lux++;	

					if($res['contact'][$i]['statutId']=='TRAV')$trav_lux++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_lux++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_lux++;
					else $sv_statut_lux++;					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_lux ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_lux++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_lux++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_lux++;
					else $sv_age_lux++;				
					
					break;
				case 'NAM' :					
					$total_nam++;
					if($res['contact'][$i]['genreId']=='M')$masculin_nam ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_nam++;
					else $sv_genre_nam++;	

					if($res['contact'][$i]['statutId']=='TRAV')$trav_nam++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_nam++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_nam++;
					else $sv_statut_nam++;					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_nam ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_nam++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_nam++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_nam++;
					else $sv_age_nam++;					
					
					break;
			}	
		}			
	
		$w = $e->initWorksheet(1, 'Rapport Qualification des contacts');
		
		$w->newRow(null, null);
		$c = $w->setString('Contacts r�pondant aux crit�res','filter_title');
		$c = $w->setString('Total','filter_title');
		$provinces = Dictionnaries::getProvincesList();
		$colsize=''; 
		for($i=0; $i<$provinces->count(); $i++)
		{
			$c = $w->setString($provinces->items($i)->getLabel(),'filter_title');
			$colsize.='<Column ss:AutoFitWidth="0" ss:Width="200"/>';
		} 	
		
		$w->newRow(null, null);
		$c = $w->setString('Nombre de contacts pris en compte','');
		$c = $w->setString(($total_bxl + $total_bwa + $total_hai+$total_lie +$total_lux+$total_nam),'');
		$c = $w->setString($total_bxl,'');
		$c = $w->setString($total_bwa,'');
		$c = $w->setString($total_hai,'');
		$c = $w->setString($total_lie,'');
		$c = $w->setString($total_lux,'');
		$c = $w->setString($total_nam,'');
						
		$w->newRow(null, null);
		$c = $w->setString('Genre','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		
		$w->newRow(null, null);
		$c = $w->setString('Homme','');
		$c = $w->setString(($masculin_bxl + $masculin_bwa + $masculin_hai+$masculin_lie +$masculin_lux+$masculin_nam),'');
		$c = $w->setString($masculin_bxl,'');
		$c = $w->setString($masculin_bwa,'');
		$c = $w->setString($masculin_hai,'');
		$c = $w->setString($masculin_lie,'');
		$c = $w->setString($masculin_lux,'');
		$c = $w->setString($masculin_nam,'');
		
		$w->newRow(null, null);
		$c = $w->setString('Femme','');
		$c = $w->setString(($feminin_bxl + $feminin_bwa + $feminin_hai+$feminin_lie +$feminin_lux+$feminin_nam),'');
		$c = $w->setString($feminin_bxl,'');
		$c = $w->setString($feminin_bwa,'');
		$c = $w->setString($feminin_hai,'');
		$c = $w->setString($feminin_lie,'');
		$c = $w->setString($feminin_lux,'');
		$c = $w->setString($feminin_nam,'');
		
		$w->newRow(null, null);
		$c = $w->setString('Sans valeur','');
		$c = $w->setString(($sv_genre_bxl + $sv_genre_bwa + $sv_genre_hai+$sv_genre_lie +$sv_genre_lux+$sv_genre_nam),'');
		$c = $w->setString($sv_genre_bxl,'');
		$c = $w->setString($sv_genre_bwa,'');
		$c = $w->setString($sv_genre_hai,'');
		$c = $w->setString($sv_genre_lie,'');
		$c = $w->setString($sv_genre_lux,'');
		$c = $w->setString($sv_genre_nam,'');
		
		$w->newRow(null, null);
		$c = $w->setString('Age','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		
		$w->newRow(null, null);
		$c = $w->setString('15 � 24 ans','');
		$c = $w->setString(($age1_bxl + $age1_bwa + $age1_hai+$age1_lie +$age1_lux+$age1_nam),'');
		$c = $w->setString($age1_bxl,'');
		$c = $w->setString($age1_bwa,'');
		$c = $w->setString($age1_hai,'');
		$c = $w->setString($age1_lie,'');
		$c = $w->setString($age1_lux,'');
		$c = $w->setString($age1_nam,'');
		
		$w->newRow(null, null);
		$c = $w->setString('25 � 54 ans','');
		$c = $w->setString(($age2_bxl + $age2_bwa + $age2_hai+$age2_lie +$age2_lux+$age2_nam),'');
		$c = $w->setString($age2_bxl,'');
		$c = $w->setString($age2_bwa,'');
		$c = $w->setString($age2_hai,'');
		$c = $w->setString($age2_lie,'');
		$c = $w->setString($age2_lux,'');
		$c = $w->setString($age2_nam,'');
		
		$w->newRow(null, null);
		$c = $w->setString('55 � 64 ans','');
		$c = $w->setString(($age3_bxl + $age3_bwa + $age3_hai+$age3_lie +$age3_lux+$age3_nam),'');
		$c = $w->setString($age3_bxl,'');
		$c = $w->setString($age3_bwa,'');
		$c = $w->setString($age3_hai,'');
		$c = $w->setString($age3_lie,'');
		$c = $w->setString($age3_lux,'');
		$c = $w->setString($age3_nam,'');
		
		$w->newRow(null, null);
		$c = $w->setString('64 ans et plus','');
		$c = $w->setString(($age4_bxl + $age4_bwa + $age4_hai+$age4_lie +$age4_lux+$age4_nam),'');
		$c = $w->setString($age4_bxl,'');
		$c = $w->setString($age4_bwa,'');
		$c = $w->setString($age4_hai,'');
		$c = $w->setString($age4_lie,'');
		$c = $w->setString($age4_lux,'');
		$c = $w->setString($age4_nam,'');
		
		
		$w->newRow(null, null);
		$c = $w->setString('Sans valeur','');
		$c = $w->setString(($sv_age_bxl + $sv_age_bwa + $sv_age_hai+$sv_age_lie +$sv_age_lux+$sv_age_nam),'');
		$c = $w->setString($sv_age_bxl,'');
		$c = $w->setString($sv_age_bwa,'');
		$c = $w->setString($sv_age_hai,'');
		$c = $w->setString($sv_age_lie,'');
		$c = $w->setString($sv_age_lux,'');
		$c = $w->setString($sv_age_nam,'');
		
		/*
		$w->newRow(null, null);
		$c = $w->setString('Statut','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');
		$c = $w->setString('','filter_title');		

		$w->newRow(null, null);
		$c = $w->setString('Travailleur occup�','');
		$c = $w->setString(($trav_bxl + $trav_bwa + $trav_hai+$trav_lie +$trav_lux+$trav_nam),'');
		$c = $w->setString($trav_bxl,'');
		$c = $w->setString($trav_bwa,'');
		$c = $w->setString($trav_hai,'');
		$c = $w->setString($trav_lie,'');
		$c = $w->setString($trav_lux,'');
		$c = $w->setString($trav_nam,'');		

		$w->newRow(null, null);
		$c = $w->setString('Etudiant','');
		$c = $w->setString(($etud_bxl + $etud_bwa + $etud_hai+$etud_lie +$etud_lux+$etud_nam),'');
		$c = $w->setString($etud_bxl,'');
		$c = $w->setString($etud_bwa,'');
		$c = $w->setString($etud_hai,'');
		$c = $w->setString($etud_lie,'');
		$c = $w->setString($etud_lux,'');
		$c = $w->setString($etud_nam,'');					

		$w->newRow(null, null);
		$c = $w->setString('Autre','');
		$c = $w->setString(($autre_bxl + $autre_bwa + $autre_hai+$autre_lie +$autre_lux+$autre_nam),'');
		$c = $w->setString($autre_bxl,'');
		$c = $w->setString($autre_bwa,'');
		$c = $w->setString($autre_hai,'');
		$c = $w->setString($autre_lie,'');
		$c = $w->setString($autre_lux,'');
		$c = $w->setString($autre_nam,'');		

		$w->newRow(null, null);
		$c = $w->setString('Sans valeur','');
		$c = $w->setString(($sv_statut_bxl + $sv_statut_bwa + $sv_statut_hai+$sv_statut_lie +$sv_statut_lux+$sv_statut_nam),'');
		$c = $w->setString($sv_statut_bxl,'');
		$c = $w->setString($sv_statut_bwa,'');
		$c = $w->setString($sv_statut_hai,'');
		$c = $w->setString($sv_statut_lie,'');
		$c = $w->setString($sv_statut_lux,'');
		$c = $w->setString($sv_statut_nam,'');
		*/
		
	$w->setColumns('
   <Column ss:AutoFitWidth="0" ss:Width="250"/>
   <Column ss:AutoFitWidth="0" ss:Width="100"/>
   '.
	$colSize
	);
	
	$e->outputAsFile('Rapport_Qualification_contacts .xls');	
	
?>