<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/excelwriter.class.php');
$e = new ExcelWriter('ASE');
/*
 * D�finition de styles, sans doute le plus ennuyant
 */

	$e->setStyle('
  <Style ss:ID="numeric">
   <NumberFormat ss:Format="0.0"/>
  </Style>
  <Style ss:ID="simple_bold">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  
  <Style ss:ID="filter_title">
   <Alignment ss:Horizontal="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  
  <Style ss:ID="filter_title_red">
   <Alignment ss:Horizontal="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style>  
  
  <Style ss:ID="center_cell">
   <Alignment ss:Horizontal="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>
  
  <Style ss:ID="cell">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>  
  
    
  <Style ss:ID="last_line">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  ');

	$w = $e->initWorksheet(1, 'Rapport d\'activit� d\'encodage');
	$w->newRow(null, 'filter_title_red');
	$c = $w->setString('Rapport d\'encodage');

	$w->newRow(null, null);
	$c = $w->setString($dateEncodage);
	
	$w->newRow(null, null);
	$objet = 'Objet : ';
	foreach($typeObjet as $obj)
	{
		switch($obj)
	    {
	    	case 'contact' : $obj = 'Contacts'; break;
	    	case 'action' : $obj = 'Actions'; break;
	    	case 'suivis' : $obj = 'Fiches de suivis'; break;
	    	case 'materiel' : $obj = 'Mat�riels p�dagogiques'; break; 
	   	}
		
		$objet.= $obj.'; ';
	}
	$objet = substr($objet, 0, strlen($objet)-2);
	$c = $w->setString($objet);

	$w->newRow(null, null);
	$objet = 'Op�rations : ';
	foreach($typeEncodage as $t)
	{
		switch($t)
	    {
	    	case 'ajout' : $t = 'Ajo�ts'; break;
	    	case 'update' : $t = 'Mises � jour'; break;
	    	case 'delete' : $t = 'Suppressions'; break;
	   	}
		
		$objet.= $t.'; ';
	}
	$objet = substr($objet, 0, strlen($objet)-2);
	$c = $w->setString($objet);
	$w->newRow(null, 'filter_title_red');
	$c = $w->setString("R�sultats"); 
	$boContact = false; $boAction = false; $boSuivi = false; $boMateriel = false;
	$boAjout = false; $boUpdate = false; $boDelete = false; 
	$colSize = '';
	
	//Ent�te
	$w->newRow(null, 'filter_title'); //applique un style sur la ligne
	$c = $w->setString('Type');
	$c = $w->setString('Utilisateur');
	
	foreach($typeObjet as $obj)
	{
		$value = '';
					
		switch ($obj)
		{
	    	case 'contact' : $value = 'Contact'; $boContact = true; break;
	    	case 'action' : $value = 'Action'; $boAction = true; break;
	    	case 'suivis' : $value = 'Fiche suivi'; $boSuivi= true; break;
	    	case 'materiel' : $value = 'Mat�riel p�d.'; $boMateriel = true; break; 					
		}
					
		foreach($typeEncodage as $encode)
		{
			$val = '';
			switch ($encode)
			{
				case 'ajout' : $val = '+'; $boAjout = true; break;
				case 'update' : $val = '~'; $boUpdate = true; break;
				case 'delete' : $val = '-'; $boDelete=true; break;   
			}
			$c = $w->setString($value.'('.$val.')');
			$colSize .='<Column ss:AutoFitWidth="1" ss:Width="100"/>'; 
		}
	}
	
	$userId = 0; $lastUserId = 0;			
	$nbAjoutContact = 0; $nbAjoutAction = 0; $nbAjoutSuivis = 0; $nbAjoutMateriel = 0;
	$nbUpdateContact = 0; $nbUpdateAction = 0; $nbUpdateSuivis = 0; $nbUpdateMateriel = 0;
	$nbDeleteContact = 0; $nbDeleteAction = 0; $nbDeleteSuivis = 0; $nbDeleteMateriel = 0;
	$nbAjoutContactTot = 0; $nbAjoutActionTot = 0; $nbAjoutSuivisTot = 0; $nbAjoutMaterielTot = 0;
	$nbUpdateContactTot = 0; $nbUpdateActionTot = 0; $nbUpdateSuivisTot = 0; $nbUpdateMaterielTot = 0;
	$nbDeleteContactTot = 0; $nbDeleteActionTot = 0; $nbDeleteSuivisTot = 0; $nbDeleteMaterielTot = 0;
	
	for($i=0; $i<$histo->count();$i++)
	{
		if($histo->items($i)->GetUtilisateurId()!= $userId)
		{
			$nbAjoutContact = 0; $nbAjoutAction = 0; $nbAjoutSuivis = 0; $nbAjoutMateriel = 0;
			$nbUpdateContact = 0; $nbUpdateAction = 0; $nbUpdateSuivis = 0; $nbUpdateMateriel = 0;
			$nbDeleteContact = 0; $nbDeleteAction = 0; $nbDeleteSuivis = 0; $nbDeleteMateriel = 0;
			$lastUserId = $userId; 
			$userId = $histo->items($i)->GetUtilisateurId();
			$user = $histo->items($i)->GetUtilisateur();
			$w->newRow(null, "center_cell"); 
			$c = $w->setString($user->getTypeUtilisateurLabel(),"cell");
			$c = $w->setString($user->getNom().' '.$user->getPrenom(),"cell");
		}

		if($boContact)
		{
			if($histo->items($i)->GetTypeObjetEncodeId()=='CONTACT')
			{

				if($boAjout)
				{
					if($histo->items($i)->GetTypeEncodageId()=='AJOUT')
					{
						$nbAjoutContact ++;
						$nbAjoutContactTot ++;
					}
				}
					
				if($boUpdate)
				{
					if($histo->items($i)->GetTypeEncodageId()=='MAJ')
					{
						$nbUpdateContact ++;
						$nbUpdateContactTot ++;
					}
				}
						
				if($boDelete)
				{
					if($histo->items($i)->GetTypeEncodageId()=='SUP')
					{
						$nbDeleteContact ++;
						$nbDeleteContactTot ++;
					}
				}
			}
		}
		
		if($boAction)
		{
			if($histo->items($i)->GetTypeObjetEncodeId()=='ACTION')
			{
				if($boAjout)
				{
					if($histo->items($i)->GetTypeEncodageId()=='AJOUT')
					{
						$nbAjoutAction ++;
						$nbAjoutActionTot ++;
					}
				}
					
				if($boUpdate)
				{
					if($histo->items($i)->GetTypeEncodageId()=='MAJ')
					{
						$nbUpdateAction ++;
						$nbUpdateActionTot ++;
					}
				}
						
				if($boDelete)
				{
					if($histo->items($i)->GetTypeEncodageId()=='SUP')
					{
						$nbDeleteAction ++;
						$nbDeleteActionTot ++;
					}
				}
			}
		}
		
		if($boSuivi)
		{
			if($histo->items($i)->GetTypeObjetEncodeId()=='FSUIVI')
			{
				
				if($boAjout)
				{
					if($histo->items($i)->GetTypeEncodageId()=='AJOUT')
					{
						$nbAjoutSuivis ++;
						$nbAjoutSuivisTot ++;
					}
				}
					
				if($boUpdate)
				{
					if($histo->items($i)->GetTypeEncodageId()=='MAJ')
					{
						$nbUpdateSuivis ++;
						$nbUpdateSuivisTot ++;
					}
				}
						
				if($boDelete)
				{
					if($histo->items($i)->GetTypeEncodageId()=='SUP')
					{
						$nbDeleteSuivis ++;
						$nbDeleteSuivisTot ++;
					}
				}
			}
		}				

		if($boMateriel)
		{
					
			if($histo->items($i)->GetTypeObjetEncodeId()=='MATPEDA')
			{
				if($boAjout)
				{
					if($histo->items($i)->GetTypeEncodageId()=='AJOUT')
					{
						$nbAjoutMateriel ++;
						$nbAjoutMaterielTot ++;
					}
				}
					
				if($boUpdate)
				{
					if($histo->items($i)->GetTypeEncodageId()=='MAJ')
					{
						$nbUpdateMateriel ++;
						$nbUpdateMaterielTot ++;
					}
				}
						
				if($boDelete)
				{
					if($histo->items($i)->GetTypeEncodageId()=='SUP')
					{
						$nbDeleteMateriel ++;
						$nbDeleteMaterielTot ++;
					}
				}
			}
		}				
				
		if($histo->items($i+1)==null || $histo->items($i+1)->GetUtilisateurId()!= $userId)
		{
			
			if($boContact)
			{
				if($boAjout)
					$c = $w->setNumber($nbAjoutContact);
					
				if($boUpdate)
					$c = $w->setNumber($nbUpdateContact);

				if($boDelete)
					$c = $w->setNumber($nbDeleteContact);
			}
			
			if($boAction)
			{
				if($boAjout)
					$c = $w->setNumber($nbAjoutAction);
					
				if($boUpdate)
					$c = $w->setNumber($nbUpdateAction);
					
				if($boDelete)
					$c = $w->setNumber($nbDeleteAction);
			}
			
			if($boSuivi)
			{
				if($boAjout)
					$c = $w->setNumber($nbAjoutSuivis);
					
				if($boUpdate)
					$c = $w->setNumber($nbUpdateSuivis);
					
				if($boDelete)
					$c = $w->setNumber($nbDeleteSuivis);
			}
			
			if($boMateriel)
			{
				if($boAjout)
					$c = $w->setNumber($nbAjoutMateriel);
				if($boUpdate)
					$c = $w->setNumber($nbUpdateMateriel);
				if($boDelete)
					$c = $w->setNumber($nbDeleteMateriel);
			}
		}
	}

	$w->newRow(null, 'filter_title');	
	$c = $w->setString("");
	$c = $w->setString("Total :");
	if($boContact)
	{
		if($boAjout)
			$c = $w->setNumber($nbAjoutContactTot);
		if($boUpdate)
			$c = $w->setNumber($nbUpdateContactTot);
		if($boDelete)
			$c = $w->setNumber($nbDeleteContactTot);
	}
	if($boAction)
	{
		if($boAjout)
			$c = $w->setNumber($nbAjoutActionTot);
		if($boUpdate)
			$c = $w->setNumber($nbUpdateActionTot);
		if($boDelete)
			$c = $w->setNumber($nbDeleteActionTot);
	}
	if($boSuivi)
	{
		if($boAjout)
			$c = $w->setNumber($nbAjoutSuivisTot);
		if($boUpdate)
			$c = $w->setNumber($nbUpdateSuivisTot);
		if($boDelete)
			$c = $w->setNumber($nbDeleteSuivisTot);
	}
	if($boMateriel)
	{
		if($boAjout)
			$c = $w->setNumber($nbAjoutMaterielTot);
		if($boUpdate)
			$c = $w->setNumber($nbUpdateMaterielTot);
		if($boDelete)
			$c = $w->setNumber($nbDeleteMaterielTot);
	}
	
	$w->setColumns('
   <Column ss:AutoFitWidth="0" ss:Width="100"/>
   <Column ss:AutoFitWidth="1" ss:Width="100"/>'.
	$colSize
	);
	
	$e->outputAsFile('Rapport_activite_encodage.xls');
	
?>