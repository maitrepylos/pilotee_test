<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/excelwriter.class.php');
$e = new ExcelWriter('ASE');
/*
 * D�finition de styles, sans doute le plus ennuyant
 */
$colSize = '';
	$e->setStyle('
  <Style ss:ID="numeric">
   <NumberFormat ss:Format="0.0"/>
  </Style>
  <Style ss:ID="simple_bold">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  
  <Style ss:ID="filter_title">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  
  <Style ss:ID="filter_title_red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style>

  <Style ss:ID="red">
   <Alignment ss:Horizontal="Left"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#EAC3C3" ss:Pattern="Solid"/>
  </Style>   
  
  <Style ss:ID="center_cell">
   <Alignment ss:Horizontal="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>
  
  <Style ss:ID="cell">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="0"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>   
  </Style>  
  <Style ss:ID="last_line">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  ');

	$w = $e->initWorksheet(1, 'Rapport journal de classe');
	$w->newRow(null, null);
	$c = $w->setString('Rapport journal de classe','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	$c = $w->setString('','red');
	//$fiches;
	$w->newRow(null, null);
	$c = $w->setString('Date : Du '.$postJ['annee_ante'].' au '.$postJ['annee_post']);
	
	$agt = Dictionnaries::getUtilisateursList(USER_TYPE_AGENT,$postJ['agent']);
	$w->newRow(null, null);
	$c = $w->setString('Agent : '.$agt->items(0)->getNom().' '.$agt->items(0)->getPrenom());
	
	$w->newRow(null, null);
	$c = $w->setString('R�sultats', 'red');
	$c = $w->setString('', 'red');
	$c = $w->setString('', 'red');
	$c = $w->setString('', 'red');
	$c = $w->setString('', 'red');
	$c = $w->setString('', 'red');	
	
	$w->newRow(null, null);
	$c = $w->setString('Date de rencontre','filter_title');
	$c = $w->setString('Etablissement','filter_title');
	$c = $w->setString('Timing','filter_title');
	$c = $w->setString('Modalit�','filter_title');
	$c = $w->setString('Intention','filter_title');
	$c = $w->setString('Contacts','filter_title');
	

	
	for($i=0; $i<$fiches->count();$i++)
	{  
		$fiche = $fiches->items($i);
		$etab = $fiche->GetEtablissement(); 
		$contacts = $fiche->GetContactList();
		$w->newRow(null, null);
		$c = $w->setString($fiche->GetDateRencontre(true),'cell');
		$c = $w->setString($etab->GetNom(),'cell');
		$c = $w->setString(($fiche->GetTimingSuivi()?$fiche->GetTimingSuivi()->getLabel():''),'cell');
		$c = $w->setString(($fiche->GetModeSuivi() ? $fiche->GetModeSuivi()->getLabel():''),'cell');
		$c = $w->setString(($fiche->GetIntentionSuivi()?$fiche->GetIntentionSuivi()->getLabel():''),'cell');
		$cont='';
		for($c=0;$c<$contacts->count();$c++)
		{
			$cont.= $contacts->items($c)->GetNom().' '.$contacts->items($c)->GetPrenom().'<br/>';
		}
		$c = $w->setString($cont,'cell');
	}
	
	$w->setColumns('
   <Column ss:AutoFitWidth="0" ss:Width="150"/>
   <Column ss:AutoFitWidth="0" ss:Width="200"/>
   <Column ss:AutoFitWidth="0" ss:Width="150"/>
   <Column ss:AutoFitWidth="0" ss:Width="100"/>
   <Column ss:AutoFitWidth="0" ss:Width="250"/>
   <Column ss:AutoFitWidth="0" ss:Width="200"/>
   '.
	$colSize
	);
	
	$e->outputAsFile('Rapport_journalDeClasse.xls');	
?>