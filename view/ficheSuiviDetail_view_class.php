<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class FicheSuiviDetailView extends BaseView
{
	private $user = null;
	
	public function Render($ficheSuivi)
	{
		$this->user = Session::GetInstance()->getCurrentUser();		
?>
	<script language="javascript" type="text/javascript">
		function FicheSuiviViewClass() {}

		FicheSuiviViewClass.prototype.onEdit = function()
		{
			var frm = document.getElementById("frmFicheSuivi");
			frm.src = "<?php echo SCRIPTPATH . 'index.php?module=ficheSuiviEdit&id=' . $ficheSuivi->GetFicheSuiviId() . '&close=FicheSuiviViewClass.prototype.closeEdit'; ?>"; 
			
			Popup.showModal('popupEditFicheSuivi');
		};

		FicheSuiviViewClass.prototype.closeEdit = function()
		{
			Popup.hide('popupEditFicheSuivi');
			submitForm('ficheSuiviDetail');
		};

		FicheSuiviViewClass.prototype.onDelete = function()
		{
			if (window.confirm("Cette op�ration est irr�versible !\nVoulez-vous continuer ?"))
			{
				document.getElementById("submitAction").value = "delete";
				submitForm('ficheSuiviDetail');
			}
		};		
		window.fsvc = new FicheSuiviViewClass();
		
        function ShowHistoriqueDetail()
        {
         	var frm = document.getElementById("frmHistoriqueDetail");
            frm.src = '<?php echo SCRIPTPATH . 'index.php?module=historiquePopup&id='.$ficheSuivi->GetFicheSuiviId().'&objet=fichesuivi';?>';              
            Popup.showModal('HistoriqueDetail');
        }	
        function updateFiche(id, etabId)
        {
            
        	document.location.href = '<?php echo $this->BuildURL('ficheSuiviEdit') .'&action=update&updateId='; ?>' + id+'&etabId='+etabId;
        }

        function deleteFiche(id)
        {
			if (sure("Supprimer la fiche de suivi ?") == true) 
			{	
				parent.document.location.href = '<?php echo $this->BuildURL('ficheSuiviEdit') .'&action=delete&etab='.$ficheSuivi->GetEtablissementId().'&deleteId='; ?>' + id;
			} 
        }
        function ShowFormAddContact()
        {
			var frm = document.getElementById("frmFormAddContact");
			frm.src = '<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add'; ?>';			
			Popup.showModal('formAddContact');
        }
	</script>

	<div id="HistoriqueDetail" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Historique : fiche suivi n� <?php echo $ficheSuivi->GetFicheSuiviId(); ?></td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('HistoriqueDetail');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmHistoriqueDetail" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>      
	
	<div id="popupEditFicheSuivi" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Editer la fiche de suivi</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('popupEditFicheSuivi');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmFicheSuivi" frameborder=0 src="" style="width:1024px;height:768px;border:0px;" scrolling="auto"></iframe></div>
		</div>
	</div>
	
	<div id="formAddContact" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Ajouter un contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddContact');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd">
				<iframe id="frmFormAddContact" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add' ?>" style="width: 1024px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>

	<form id="ficheSuiviDetail" action="<?php echo $this->BuildURL($_REQUEST['module']) . '&id=' . $ficheSuivi->GetFicheSuiviId(); ?>" method="post">
		<input type="hidden" id="submitAction" name="submitAction" />
	
		<div class="boxGen">
			<div class="boxPan" style="padding:5px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td width="50%" style="vertical-align:top;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
						<tr>
							<td class="label">Etablissement :</td>
							<td class="detail"><?php echo sprintf('<a class="resultLineRedirection" href="%s&id=%s">%s</a>', $this->BuildURL('etablissementDetail'), $ficheSuivi->GetEtablissement()->GetEtablissementId(), $ficheSuivi->GetEtablissement()->GetNom()); ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Date :</td>
							<td class="detail"><?php echo $ficheSuivi->GetDateRencontre(true); ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Modalit� :</td>
							<td class="detail"><?php $ms = $ficheSuivi->GetModeSuivi(); echo isset($ms) ? $ms->GetLabel() : '&nbsp;'; ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr style="vertical-align:top;">
							<td class="label">Contacts :</td>
							<td class="detail">
								<div style="height:150px;overflow:auto;">
								<?php
									$contacts = $ficheSuivi->GetContactList();
									$contactsHtml = '';
														
									for ($j = 0; $j < $contacts->count(); $j++)
									{
										if ($contactsHtml != '') $contactsHtml .= '<br/>';
										
										$contactsHtml .= '<a class="resultLineRedirection" href="' . $this->BuildURL('contactDetail') . '&id=' . $contacts->items($j)->GetContactId() . '">' . $contacts->items($j)->GetPrenom() . ' ' . $contacts->items($j)->GetNom() . '</a>';
									}
									
									echo $contactsHtml;
								?>
								</div>
							</td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Cr�ation :</td>
							<td class="detail"><?php echo $ficheSuivi->getCreationString(); ?></td>
						</tr>
						</table>
					</td>
					<td width="50%" style="vertical-align:top;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
						<tr>
							<td class="label">&nbsp;</td>
							<td class="detail" style="background-color:inherit;">&nbsp;</td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Timing :</td>
							<td class="detail"><?php $ts = $ficheSuivi->GetTimingSuivi(); echo isset($ts) ? $ts->GetLabel() : '&nbsp;'; ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Intention :</td>
							<td class="detail"><?php $is = $ficheSuivi->GetIntentionSuivi(); echo isset($is) ? $is->GetLabel() : '&nbsp;'; ?></td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr style="vertical-align:top;">
							<td class="label">Commentaire :</td>
							<td class="detail">
								<div style="height:150px;overflow:auto;">
								<?php echo $ficheSuivi->GetCommentaire(); ?>
								</div>
							</td>
						</tr>
						<tr><td colspan="2" class="separator"></td></tr>
						<tr>
							<td class="label">Modification :</td>
							<td class="detail"><?php echo $ficheSuivi->getUpdatedString(); ?></td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</div>
			<div class="boxBtn">
				<table class="buttonTable">
					<tr>
						<td onclick="updateFiche('<?php echo $ficheSuivi->GetFicheSuiviId();?>','<?php echo $ficheSuivi->GetEtablissementId();?>')">Modifier</td>
						<td class="separator"></td>
						<td onclick="deleteFiche('<?php echo $ficheSuivi->GetFicheSuiviId();?>')">Supprimer</td>
						<td class="separator"></td>
					</tr>
				</table>
			</div>
		</div>
	</form>
<?php
	}
}
?>
