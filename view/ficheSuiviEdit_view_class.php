<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class FicheSuiviEditView extends BaseView
{
	private $user = null;
	
	public function render($boUpdate=false)
	{
		$this->user = Session::GetInstance()->getCurrentUser();
?>
	<script>
		function ficheSuiviEditViewClass() {}
		ficheSuiviEditViewClass.prototype.Close = function(action,operation)
		{
			if(action=='valider')
			{
				var dateRencontre = document.getElementById('dateRencontre');
				res1 = true;
				if (isDate(dateRencontre.value)==false)
				{
					SetInputError(dateRencontre);
					res1=false;
				}
				else SetInputValid(dateRencontre);
					
			    
				
				//V�rification si au moins un contact
				var res= true;
				var nbLigne = document.getElementById('nbLignesContact');
				var nbC = 0;
				for(i=0; i<nbLigne.value; i++)
				{
					var name = document.getElementById('contactId_'+i);
					if(name)
					{
						nbC ++;
					}
				}

				if(nbC<=0)
				{
					alert('La fiche suivi doit �tre li�e � au moins un contact.');
					res=false;
				}
				
				if(res1 && res)
				{
					var urlAction = '<?php echo $this->BuildURL($_REQUEST['module']);  ?>';
					if(operation=='add')
						urlAction += "&action=add";
					else
						 urlAction += "&action=update";
					submitForm('ficheSuiviEdit');
				}
				else return false;	
			}
			else
			{
				parent.window.location.href = "<?php echo SCRIPTPATH . 'index.php?module=etablissementDetail&id='.$_REQUEST['etabId']; ?>";
				parent.Popup.hide("formAddFicheSuivi");
			}
		}
		window.fse = new ficheSuiviEditViewClass();

		function ShowContactList(closeCallBack)
		{
			var frm = document.getElementById("frmContacts");
			frm.src = 'about:blank';
			var url = '<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=contactList&etabs='.$_REQUEST['etabId']; ?>';
			//var frm = document.getElementById("frmContacts");
			frm.src = url+closeCallBack;
			Popup.showModal('contacts');						
		}
		
		function ShowContactForm(closeCallBack)
		{
			
			var frm = document.getElementById("frmFormAddContact");
			Popup.showModal('formAddContact');
			var ets = new Array();
			
			if(parent.parent.document.getElementById("etablissementId"))
			{
				var id = parent.parent.document.getElementById("etablissementId").value;
				var nom = parent.parent.document.getElementById("etablissementNom").value;
			}
			else
			{
				var id = document.getElementById("etablissementId").value;
				var nom = document.getElementById("etablissementNom").value;
			}
			
			ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
			frmContent = frm.contentWindow.document || frm.contentDocument;
			
			if(ets.length>0)
			{
				SetEtablissementContact(ets, frmContent);
			}
			else
			{
				frmContent.getElementById('divEtablissementsContact').innerHTML = '';
			}
		}

		function SetEtablissementContact(objectsArray, frmContent)
		{
			if (objectsArray.length > 0)		  
			{
				var nbLignes = 0;
				var divGlobal = frmContent.getElementById("divEtablissementsContact");	
				var tab = frmContent.getElementById('tableEtablissementsContact');
				
				if(tab==null)
				{
					frmContent.getElementById('etabIds').value = '';
	 				tab = divGlobal.appendChild(frmContent.createElement("table"));
	 				tab.setAttribute("id","tableEtablissementsContact");
	 				tab.setAttribute("style","width:80%");
				
		 			//Premi�re ligne du tableau
	 				newRow = tab.insertRow(-1);
	 				newRow.setAttribute("id",-1);
	 				newCell = newRow.insertCell(0);
	 				newCell.innerHTML = "<center>�tablissement</center>";
	 			
	 				newCell = newRow.insertCell(1);
	 				newCell.innerHTML = "<center>Titre</center>";
	 			
	 				style='';
					<?php if($this->user->isOperateur()){ ?>
		 				style = "display:none";
		 			<?php } ?>
	 				newCell = newRow.insertCell(2);
	 				newCell.innerHTML = "<center>Participation</center>";
	 				newCell.setAttribute('style',style);
		 			newCell = newRow.insertCell(3);
	 				newCell.innerHTML = "<center>Sp�cialit�</center>";
	 			}
	 			else
	 			{
					nbLignes = frmContent.getElementById("nbLignes").value;
			}	
			 autoid = nbLignes;
			 for(j=0;j<objectsArray.length;j++)
			 {
				var etabSelect = frmContent.getElementById("etabIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;					
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
					
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
	 				newRow = tab.insertRow(-1);
					newRow.setAttribute("id",nbLignes);

					 for(i=0;i<5;i++)
	 		 		 { 		 		 
	 	 			 	newCell = newRow.insertCell(i);
	 	 			 	var tmp = "";
						switch(i)
	 					{
	 						case 0 :
	 							tmp = '<input type="text" name="etabName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
	 								  '<input name="etabId_'+nbLignes+'" id="etabId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
	 							newCell.innerHTML = tmp;
	 							break;
							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getTitreContactList());?>";
								newCell.innerHTML = "<select name='titre_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
								break;
							case 2 :
								style='';
								<?php if($this->user->isOperateur()){ ?>
	 								style = "display:none";
								<?php } ?>
	 							newCell.setAttribute('style',style);
						    	tmp = "<?php echo createSelectOptions(Dictionnaries::getParticipationList());?>";
								newCell.innerHTML = "<select name='participation_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
								break;
	 						case 3 :
								    tmp = "<?php echo createSelectOptions(Dictionnaries::getSpecialiteList());?>";
								newCell.innerHTML = "<select name='specialite_"+nbLignes+"' style='width:175px;'>"+
														"<option value='-1'></option>"+tmp+"</select>";
								break;
	 						case 4 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\""+nbLignes+"\", \"nbLignes\")'/>";
	 							break; 							
	 					}
	 				
	 					with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
						autoid++;
				 	}
				 	nbLignes++;
				 }
			  }
			  frmContent.getElementById("nbLignes").value = nbLignes;
		   }
		}
		
		function SetContacts (objectsArray, close)
		{

			if (objectsArray.length > 0)		  
			{
				var nbLignes = 0;
				var divGlobal = document.getElementById("divContacts");
				var tab = document.getElementById('tableContacts');

				if(tab==null)
				{
					document.getElementById('contactIds').value = '';
	 				tab = divGlobal.appendChild(document.createElement("table"));
	 				tab.setAttribute("id","tableContacts");
	 				newRow = tab.insertRow(-1);
	 				newRow.setAttribute("id","tr_head_contact");
	 				newCell = newRow.insertCell(0);
	 				newCell = newRow.insertCell(1);
	 			}
	 			else
	 				nbLignes = document.getElementById("nbLignesContact").value;
 				
				autoid = nbLignes;		
				for(j=0;j<objectsArray.length;j++)
				{
					var etabSelect = document.getElementById("contactIds");
					var reg=new RegExp("[;]+", "g");
					tableEtabSelect = etabSelect.value.split(reg);
					var isPresent = false;
					for(var x=0; x<tableEtabSelect.length; x++)
					{
						if(tableEtabSelect[x] == objectsArray[j].id)
						{
							isPresent = true;
							break;
						}
					}
					if(!isPresent)
					{
						etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
	 					newRow = tab.insertRow(-1);	 				
		 				newRow.setAttribute("id","id_contact_tr_"+nbLignes);
		 				
						 for(i=0;i<3;i++)
	 			 		 { 		 		 
	 		 			 	newCell = newRow.insertCell(i);
	 		 			 	var tmp = "";
		 					switch(i)
	 						{
	 							case 0 :
	 								tmp = '<input type="text" name="contactName_'+nbLignes+'" style="width:300px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
	 									  '<input name="contactId_'+nbLignes+'" id="contactId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
	 								newCell.innerHTML = tmp;
	 								break;
	 							case 1 :
									newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"id_contact_tr_"+nbLignes+"\", \"nbLignesContact\")'/>";
	 								break; 							
	 						}
	 					
	 						with(this.newCell.style)
							{	
						 		width = '100px';
						 		textAlign = 'center';
						 		padding = '5px';
							} 
	 						autoid++;
	 				 	}
	 				 	nbLignes++;
	 				 }				

				}
				 document.getElementById("nbLignesContact").value = nbLignes;
			}
			if(close)Popup.hide('contacts');
		}		
		
	</script>
	
	<div id="contacts" class="popup" style="width:984px">
		<div class="popupPanel" style="width:984px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Liste des contacts</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="reloadFrameBlank('frmContacts'); Popup.hide('contacts');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:984px">
				<iframe id="frmContacts" frameborder=0 src="" style="width: 984px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	<div id="formAddContact" class="popup" style="width:944px">
		<div class="popupPanel" style="width:944px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:934px">
					<tr>
						<td width="95%">Cr�er un contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="reloadFrameBlank('frmFormAddContact'); Popup.hide('formAddContact');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:944px">
				<iframe id="frmFormAddContact" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add&exitPopup=ok&etabs='.$_REQUEST['etabId']; ?>" style="width: 944px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>	
	<?php if($boUpdate){?>
	<br/>
	<span style="padding-left: 5px;" class="title">&gt;&gt; Modifier la fiche de suivi</span>
	<br/><br/>
	<?php }?>
	
	<form id="ficheSuiviEdit" action="" method="post">
		<input type="hidden" id="contactsToDelete" name="contactsToDelete" />
		<input type="hidden" id="submitAction" name="submitAction" />
		<input type="hidden" name="ficheSuiviId" value="<?php if(isset($_POST['ficheSuiviId']))echo $_POST['ficheSuiviId']; ?>" />
		
		<input type="hidden" id="etablissementId" name="etablissementId" value="<?php if(isset($_POST['etablissementId']))echo $_POST['etablissementId']; ?>" />
		<input type="hidden" id="etablissementNom" name="etablissementNom" value="<?php if(isset($_POST['etablissementNom']))echo $_POST['etablissementNom']; ?>" />

	
		<div class="boxGen">
			<div class="boxPan" style="padding:5px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
				<tr>
					<td class="label">Date :</td>
					<td class="detailEdit">
						<input type="text" id="dateRencontre" maxlength="10" name="dateRencontre" value="<?php if(isset($_POST['dateRencontre']))echo $_POST['dateRencontre']; ?>" style="width:175px;"></input>
						<img src='./styles/img/calendar3.gif' id='date_ante_pic'  class="expand" title='Calendrier' />
						<img src="./styles/img/eraser.gif" onclick="document.getElementById('dateRencontre').value='';" class="expand" title = "vider"/>
				            <script type='text/javascript'>
 									Calendar.setup({
        								inputField     :    'dateRencontre',     
        								ifFormat       :    '%d/%m/%Y',      
        								button         :    'date_ante_pic',  
        								align          :    'Tr'            
									});
							</script>								
					</td>
				</tr>
				<tr>
					<td class="label">Modalit� :</td>
					<td class="detailEdit">
						<select name="modeSuivi" style="width:178px;">
						<?php
							echo '<option value="--NULL--" selected="selected"></option>';
							if (isset($_POST['modeSuivi'])) {
								echo createSelectOptions(Dictionnaries::GetModeSuivis(), $_POST['modeSuivi']);
							}
							else {
								echo createSelectOptions(Dictionnaries::GetModeSuivis());
							}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Timing :</td>
					<td class="detailEdit">
						<select name="timingSuivi" style="width:178px;">
						<?php
							echo '<option value="--NULL--" selected="selected"></option>';
							if (isset($_POST['timingSuivi'])) {
								echo createSelectOptions(Dictionnaries::GetTimingSuivis(), $_POST['timingSuivi']);
							}
							else {
								echo createSelectOptions(Dictionnaries::GetTimingSuivis());
							}
						?>
						</select>
					</td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>
				<tr>
					<td class="label">Intention :</td>
					<td class="detailEdit">
						<select name="intentionSuivi" style="width:178px;">
						<?php
							echo '<option value="--NULL--" selected="selected"></option>';
							if (isset($_POST['intentionSuivi'])) {
								echo createSelectOptions(Dictionnaries::GetIntentionSuivis(), $_POST['intentionSuivi']);
							}
							else {
								echo createSelectOptions(Dictionnaries::GetIntentionSuivis());
							}
						?>
						</select>
					</td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>
				<tr valign="top">
					<td class="label">Commentaire :</td>
					<td class="detailEdit"><textarea name="comments" rows="5" cols="50"><?php if(isset($_POST['comments']))echo $_POST['comments']; ?></textarea></td>
				</tr>
				<tr><td colspan="2" class="separator"></td></tr>
				<tr valign="top">
					<td class="label">Contact :</td>
					<td class="detailEdit">
						<img src="./styles/img/add_contact.gif" id="searchContacts" onclick="ShowContactList('&close=SetContacts');return false" title="Rechercher un contact" class='expand' />
						&nbsp;&nbsp;/&nbsp;&nbsp;<img src="./styles/img/edit_group.gif" id="createContact" onclick="ShowContactForm('&close=SetContacts');return false" title="Cr�er un contact" class='expand' />
					</td>
				</tr>
				
				<tr>
					<td class="label"></td>
					<td class="detailEdit">
						<input type="hidden" id="contactIds" name="contactIds" value="<?php echo  !isset($_POST['contactIds']) ? "" : $_POST['contactIds']; ?>">
						<?php $nbL=0; if(isset($_POST['nbLignesContact'])) $nbL = $_POST['nbLignesContact'];?>
						<input name='nbLignesContact' id='nbLignesContact' type='hidden' value='<?php echo $nbL ?>' />

						<div id="divContacts">
							<?php 
							if((isset($_POST['nbLignesContact']) && $_POST['nbLignesContact']>0))
								$this->createTableContact();
							?>
						</div>
					</td>
				</tr>				
				</table>
			</div>
			<?php if($boUpdate){ $operation = 'update';?>
				<input type="hidden" name="updateForm" />
			<?php }else{$operation = 'add';?>
				<input type="hidden" name="addForm" />
			<?php }?>
			
			<div class="boxBtn" style="padding-top:5px;text-align:left;">
				<table class="buttonTable" border="0" align="left">
					<tr>
						<?php if(!$boUpdate && !isset($_POST['addForm']) || $boUpdate){?>
						<td onclick="window.fse.Close('valider','<?php echo $operation?>')">Sauvegarder</td>
						<?php }?>
						<?php if($_REQUEST['module']=='ficheSuiviEditPopup'){?>
						<td class="separator"></td>
						<td onclick="window.fse.Close('fermer', '<?php echo $operation?>' )">Fermer</td>
						<?php }?>
					</tr>
				</table>
			</div>
		</div>
	</form>
	<?php
	}
	public function createTableContact()
	{
		$idTable = 'tableContacts';
		$idHeadTr = 'tr_head_contact';
		$headLabel='Contact';
		$nbLignes = $_POST['nbLignesContact'];
		?>
		<table id="<?php echo $idTable; ?>" style="width: 20%;">
			<tbody>
				<tr id="<?php echo $idHeadTr; ?>">
					<td></td>
					<td></td>
				</tr>
				<?php
				for($i=0; $i<$nbLignes; $i++)
				{
					if(isset($_POST['contactName_'.$i]))
					{
						echo '<tr id="id_contact_tr_'.$i.'"><td style="padding: 5px; width: 100px; text-align: center;">';
						echo '<input type="text" readonly="readonly" value="'.$_POST['contactName_'.$i].'" style="width: 300px;" name="contactName_'.$i.'"/>';
						echo '<input type="hidden" value="'.$_POST['contactId_'.$i].'" id="contactId_'.$i.'" name="contactId_'.$i.'"/>';
						echo '</td>';
						echo '<td style="padding: 5px; width: 100px; text-align: center;">';
						echo '<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRow(\'id_contact_tr_'.$i.'\', \'nbLignesContact\')"/>';
						echo '</td>';
						echo'</tr>';
					}
				}
				?>
			</tbody>
		</table>
	<?php
	}
}
?>
