<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class FormationDetailView extends BaseView
{
	private $user = null;
	private $formation = null;
	
	public $degreNiveauList = null;
	public $pedagogList = null;
	public $formateurs = null;	
	public $formationTypes = null;
	
	public $filieresDegree = null;
	public $finalitePedago = null;
	
	public $isUpdate = null; //?
	
	public function _getUser() {
		return $this->user;		
	}
	
	protected function _commonJsAndPopup()
	{
		echo '<script type="text/javascript">';
		include_once SCRIPTPATH . 'view/partial/js_formation.php';
		include_once SCRIPTPATH . 'view/partial/js_contact.php';
		echo '</script>';

		include_once SCRIPTPATH . 'view/partial/popup_addFormation.php';
		include_once SCRIPTPATH . 'view/partial/popup_addContact.php';
	}
	
	public function Render($action)
	{
		$this->user = Session::GetInstance()->getCurrentUser();
		
		$this->formation = $action;
		
		$this->editAction();
		
		$this->_commonJsAndPopup();
		
	}
	
	private function editAction() {
?>
	<script type="text/javascript">
		
	function submitFormFormation(formId, action, operation)
	{
		var errorMsg = "";
		var res1 = true;
		var chk = new CheckFunction();
		
		var organisateur = document.getElementById("organisateur");
		if(organisateur)
		{
			organisateur.disabled = false;
			if(organisateur.value=='-1')
			{
				SetInputError(organisateur);
				res1 = false;
			}
			else
			{
				SetInputValid(organisateur);
			}
		}

		var type = document.getElementById('formationType');
		if(type)
		{
			type.disabled = false;
			if(type.value=='-1')
			{
				SetInputError(type);
				res1 = false;
			}
			else
			{
				SetInputValid(type);
			}
		}

		var nbAppTest = document.getElementById('nbGlobalApp');

		if(nbAppTest) {
			if (nbAppTest.value == '') {
				SetInputError(nbAppTest);
				res1 = false;
			} else {
				SetInputValid(nbAppTest);
			} 
				
		}

		var intitule = document.getElementById('intitule');
		if (intitule.value=='')
		{
			SetInputError(intitule);
			res1 = false;
		}
		else
		{
			SetInputValid(intitule);
		}

		var assocformateurs = document.getElementById('assocFormateur');
		
		if (assocformateurs.length == 0)
		{
			SetInputError(assocformateurs);
			res1 = false;
		}
		else
		{
			SetInputValid(assocformateurs);
		}

		var nbAppTest = document.getElementById('nbGlobalApp');

		if(nbAppTest) {
			if (nbAppTest.value == '') {
				SetInputError(nbAppTest);
				res1 = false;
			} else {
				SetInputValid(nbAppTest);
			} 
				
		}
		
		var nbEnsTest = document.getElementById('nbGlobalEns');

		if(nbEnsTest) {
			if (nbEnsTest.value == '') {
				SetInputError(nbEnsTest);
				res1 = false;
			} else {
				SetInputValid(nbEnsTest);
			} 
				
		}

		// debug FFI : ajouter l'initialisation des variables
		var nbApprenantsFiliere = 0;
		var nbApprenantsPeda = 0; 

		//Verif fili�re
		<?php
		$niveauDegreFiliere = array();
		if (count($this->degreNiveauList) > 0)
		{
			foreach($this->degreNiveauList as $v)
			{
			?>
				var val = null;
				val = document.getElementById('<?php echo $v->GetFiliereDegreNiveauId() ?>');
				chk.Add(val, 'INT', false);	
				nbApprenantsFiliere += intval(val.value);
			<?php
			}
		}
		
		//P�dagogie
		if (count($this->pedagogList) > 0)
		{
			foreach($this->pedagogList as $v)
			{
			?>
				var val = null;
				val = document.getElementById('<?php echo $v->GetFinalitePedagogId() ?>');
				chk.Add(val, 'INT', false);	
				nbApprenantsPeda += intval(val.value);
			<?php
			}
		}
		?>
		var global = document.getElementById("swGlobal");
		var nbEtab = document.getElementById("nbLignes");
		var nbContact = document.getElementById("nbLignesContact");

		if(!global && nbEtab.value <= 0)
		{
			res1=false;
			errorMsg += 'Veuillez associer la formation � un/des �tablissement(s) !\n';
		}
		else if(global && !global.checked && nbEtab.value <= 0)
		{
			res1=false;
			errorMsg += 'Veuillez associer la formation � un/des �tablissement(s) ou mentionnez la formation comme �tant globale !\n';
		}

		 if(nbContact.value<=0)
		{
			res1= false;
			errorMsg += 'Veuillez associer l\'action � un/des contact(s)\n';
		}
		else
		{
			for(i=0; i<nbContact.value; i++)
			{
				role = document.getElementById('roleActionContact_'+i);
				if(role)
				{
					if(role.value == '-1')
					{
						res1 = false;
						SetInputError(role);
					}
					else
					{
						SetInputValid(role);
					}
				}
			}
		}

		var date = document.getElementById('date');
		if (isDate(date.value)==false)
		{
			SetInputError(date);
			res1=false;
		}
		else
		{
			SetInputValid(date);
		}

		var res = chk.IsValid();
		if (res && res1)
		{
			// alert("valid");
			if(action == 'valider')
			{
				boMsg = false;
				message=''; 
				if(intval(nbApprenantsFiliere) != intval(nbGlobalApp.value) && nbApprenantsFiliere != 0)
				{
					message += 'Le nombre global d\'apprenants participant � la formation pour la r�partition par niveau-degr�-fili�re est diff�rent de '+nbGlobalApp.value+'\n';
					boMsg = true;
				}
		
				if(intval(nbApprenantsPeda) != intval(nbGlobalApp.value) && nbApprenantsPeda!=0)
				{
					message +='Le nombre global d\'apprenants participant � la formation pour la r�partition par finalit� p�dagogique est diff�rent de '+nbGlobalApp.value+'\n';
					boMsg=true;
				}
				
				if(nbApprenantsFiliere==0 && nbApprenantsPeda==0)
				{
					message+='Veuillez d�tailler la r�partition du nombre d\'apprenants participant selon les cl�s de r�partition';
					boMsg=true;
				}					

				if(boMsg)
				{
					alert(message);
					location.href='#top';
					return false;
				}
			}

			var form;
			form = document.getElementById ? document.getElementById(formId): document.forms[formId];
			var act = "<?php echo $this->BuildURL('formations') ?>";
			if(operation=='update')
				act += "&action=updateFormation";
			
			form.setAttribute("action", act);
			// alert(act);
			form.submit();
		} 
		else 
		{
			// alert("pas valid");
			if(errorMsg.length>0)
				alert(errorMsg);

			location.href='#top';
			return false;
		}
	}

	function deleteAction(actionId)
	{
		if (sure("Supprimer l'action ?") == true)	
			parent.document.location.href = '<?php echo $this->BuildURL('formations') .'&action=deleteFormation&deleteId='; ?>' + actionId; 
	}
	
	function ShowHistoriqueDetail()
	{
		var frm = document.getElementById("frmHistoriqueDetail");
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=historiquePopup&id='.$this->formation->getFormationId().'&objet=formation';?>';			
		Popup.showModal('HistoriqueDetail');
	}


	function ShowParticipantsFormation()
	{
		var frm = document.getElementById("frmParticipantsFormation");
		frm.src = '<?php echo SCRIPTPATH . 'index.php?module=formationParticipants&formation_id='.$this->formation->getFormationId(). '';?>';			
		Popup.showModal('ParticipantsFormation');
	}

	function ShowContactList(closeCallBack)
	{
		nbLignesEtab = document.getElementById("nbLignes").value;
		swGlobal = document.getElementById("swGlobal");
		
		if(nbLignesEtab>0 || (swGlobal && swGlobal.checked))
		{
			var urlEtab = "";
			if(nbLignesEtab>0)
			{
				var etabSelect = document.getElementById("etabIds");
				urlEtab += "&etabs="+etabSelect.value;
			}

			if(swGlobal && swGlobal.checked)
			{
				urlEtab += "&global=1";
			}
			url = '<?php echo SCRIPTPATH; ?>index.php?module=contactPopup&action=contactList';
			var frm = document.getElementById("frmContacts");
			frm.src = url+closeCallBack+urlEtab;
			Popup.showModal('contacts');						
		}
		else
			alert('Veuillez associer l\'action � un �tablissement avant de l\'associer � un contact'); 
	}
	function SetContactsAction(objectsArray, close)
	{
		if (objectsArray.length > 0)		  
		{
			var nbLignes = 0;
			var divGlobal = document.getElementById("divContactsAction");
			var tab = document.getElementById('tableContactsAction');
			if(tab==null)
			{
				document.getElementById('contactIds').value = '';
 				tab = divGlobal.appendChild(document.createElement("table"));
 				tab.setAttribute("id","tableContactsAction");
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id","tr_head_contact");
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>Contact</center>";
 				newCell = newRow.insertCell(1);
 				newCell.innerHTML = "<center>R�le</center>";
 				
 			}
 			else
 				nbLignes = document.getElementById("nbLignesContact").value;
				
			autoid = nbLignes;			
			for(j=0;j<objectsArray.length;j++)
			{
				var etabSelect = document.getElementById("contactIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);	 				
	 				newRow.setAttribute("id","id_contact_tr_"+nbLignes);
	 				
					 for(i=0;i<3;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
 							case 0 :
 								tmp = '<input type="text" name="contactName_'+nbLignes+'" style="width:300px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 									  '<input name="contactId_'+nbLignes+'" id="contactId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 								newCell.innerHTML = tmp;
 								break;
 							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>"; // FFI Juin 2012 : participants � une formation pas introduit par ici !!!!
								newCell.innerHTML = "<select name='roleActionContact_"+nbLignes+"' id='roleActionContact_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
	 							
 							case 2 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"id_contact_tr_"+nbLignes+"\", \"nbLignesContact\")'/>";
 								break; 							
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }				

			}
			 document.getElementById("nbLignesContact").value = nbLignes;
		}
		
		if(close)Popup.hide('contacts');
	}
	
	function SetEtablissementsAction(objectsArray, close)
	{
		if (objectsArray.length > 0)		  
		{
			var nbLignes = 0;
			var divGlobal = document.getElementById("divEtablissementsAction");
			var tab = document.getElementById('tableEtablissementsAction');
			if(tab==null)
			{
				document.getElementById('etabIds').value = '';
 				tab = divGlobal.appendChild(document.createElement("table"));
 				tab.setAttribute("id","tableEtablissementsAction");
			
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id","tr_head_etab");
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>�tablissement</center>";
 			}
 			else
 			{
 				nbLignes = document.getElementById("nbLignes").value;
 			}
			autoid = nbLignes;			
		 	for(j=0;j<objectsArray.length;j++)
 			{
				var etabSelect = document.getElementById("etabIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);
	 				newRow.setAttribute("id",nbLignes);

					 for(i=0;i<2;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
 							case 0 :
 								tmp = '<input type="text" name="etabName_'+nbLignes+'" style="width:300px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 									  '<input name="etabId_'+nbLignes+'" id="etabId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 								newCell.innerHTML = tmp;
 								break;
 							case 1 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\""+nbLignes+"\", \"nbLignes\")'/>";
 								break; 							
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }
 			  }
 			  document.getElementById("nbLignes").value = nbLignes;
 			}
		if(close)Popup.hide('etablissements');
	}
	
	</script>
	<div id="top"></div>
		<div id="top"></div>
	
		<div id="etablissements" class="popup" style="width:984px">
		<div class="popupPanel" style="width:984px">
			<div class="popupPanelHeader">
							<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Recherche sur �tablissements</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('etablissements');reloadFrameBlank('frmEtablissements');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:984px">
				<iframe id="frmEtablissements" frameborder=0 src="" style="width: 984px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	<div id="contacts" class="popup" style="width:984px">
		<div class="popupPanel" style="width:984px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Liste des contacts</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('contacts');reloadFrameBlank('frmContacts');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:984px">
				<iframe id="frmContacts" frameborder=0 src="" style="width: 984px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	<?php
		include_once SCRIPTPATH . 'view/partial/popup_listFormateur.php';
		
		echo '<script type="text/javascript">';
		include_once SCRIPTPATH . 'view/partial/js_etablissement.php';
		echo '</script>';
		
		$disabled = '';		
		if($this->isUpdate):
			$disabled = 'disabled=\'disabled\'';
		?>
		<span style="padding-left: 5px;" class="title">&gt;&gt; Modifier la formation</span><br/><br/>
		<?php endif; ?>

	<div id="ParticipantsFormation" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Participants � la formation : <?php echo $this->formation->getIntitule(); ?></td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('ParticipantsFormation');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmParticipantsFormation" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
			
	<div id="HistoriqueDetail" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Historique : <?php echo $this->formation->getIntitule(); ?></td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('HistoriqueDetail');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmHistoriqueDetail" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>
	<div class="boxGen">
		<form id="formFormation" name="formFormation" action="" method="post">
			<?php
				// $hidden_field['selectedFormateurs'] = ToHTML($this->data["selectedFormateurs"]);
				// $hidden_field['selectedFormateursName'] = ToHTML($this->data["selectedFormateursName"]);
				// $hidden_field['formation_id'] = ToHTML($this->formation->getFormationId());
			?>
			<input type="hidden" name="formation_id" value="<?php echo $this->formation->getFormationId();?>">
			
			
			<?php
				$formateursSelectedIds = "";
				$formateursSelectedNames = "";
				$selectedFormateur = $this->formation->getFormateur();
				/*
				if(!empty($selectedFormateur))
				{
					for ($i=0; $i < count($selectedFormateur); $i++) {
						if ($i>0) { 
							$formateursSelectedIds .= ";";
							$formateursSelectedNames .= ";";
						}
						$formateursSelectedIds .= $selectedFormateur[$i]->getId();
						$formateursSelectedNames .= $formateursSelectedNames[$i]->getLabel();
					}
				}
				echo '<input type="hidden" value="selectedFormateurs" value="' . $formateursSelectedIds . '" />' ;
				echo '<input type="hidden" value="selectedFormateursName" value="' . $formateursSelectedNames . '" />' ;
				
				*/
				if(!empty($selectedFormateur)) {
					echo '<input type="hidden" id="selectedFormateurs" name="selectedFormateurs" value="' . $selectedFormateur->getId() . '" />' ;
					echo '<input type="hidden" id="selectedFormateursName" name="selectedFormateursName" value="' . $selectedFormateur->getLabel() . '" />' ;
				} else {
					echo '<input type="hidden" id="selectedFormateurs" name="selectedFormateurs" value="" />' ;
					echo '<input type="hidden" id="selectedFormateursName" name="selectedFormateursName" value="" />' ;					
					
				 }
				
				
				
				
			?>
			<div class="boxPan" style="padding: 5px;">
			
				<table>
					<tr>
						<td>Statut de la formation</td>
						<td>:</td>
						<td>						
							<select id="formationStatut" name="statut" style="width: 230px;">
								<option value="ENCOU" <?php if ($this->formation->getStatutFormationId() == "ENCOU") echo 'SELECTED';?> >Brouillon</option>
								<option value="valider" <?php if ($this->formation->getStatutFormationId() == "VALID") echo 'SELECTED';?> >Publi�</option>
								
							</select>
						</td>
					</tr>
					<tr>
						<td>Type de formation</td>
						<td>:</td>
						<td>						
							<select <?php echo $disabled;?> id="formationType" name="formationType" style="width: 230px;">
							<?php
								echo '<option value="-1"></option>';
								// echo createSelectOptions(Dictionnaries::getTypeActionList($user->isOperateur(),$user->isAgent(),false), $_POST['formationType']);
								// echo createSelectOptions(Dictionnaries::getTypeActionList($this->_getUser()->isOperateur(),$this->_getUser()->isAgent(),false), $_POST['formationType']);
								// TODO : change the type action list into the type formation list
								// TODO : change the user : is formateur ?
								echo createSelectOptions($this->formationTypes, ($this->formation->getTypeFormationId() ? $this->formation->getTypeFormationId() : -1));
								
							?>
							</select>
						</td>
					</tr>
					<?php
						if($this->_getUser()->isFormateur())
						{
							// $hidden_field['organisateur'] = ToHTML($this->_getUser()->getFormteurId()); // TODO : check getFormteurId
					?>
							<input type="hidden" name='organisateur' value="<?php echo ToHTML($this->_getUser()->getFormateurId())?>">
					
					<?php						 
						}
						else
						{
					?>
					<tr id="tr_formateur">
						<td>Formateur organisateur</td>
						<td>:</td>
						<td>
							<select <?php echo $disabled;?>  id="organisateur" name="organisateur" style="width: 230px;">
							<?php
							echo '<option value="-1"></option>';
							echo createSelectOptions($this->formateurs, $this->formation->getFormateurId());
							?>
							</select>
						</td>
					</tr>
					<?php } ?>
					<tr>
						<td>Ann�e scolaire</td>
						<td>:</td>
						<td>
							
							<select id="anneesScolaire" name="anneesScolaire" style="width: 230px;">
							<?php							
							echo createSelectOptions(Dictionnaries::getAnneeList(), $this->formation->getAnneeId());
							?>
							</select>
						</td>
						<td>
							(<input style="width: 100px" type="text" maxlength="10" value="<?php echo $this->formation->getDateFormationString(false) ?>" name="date" id="date" />
					 		<img src='./styles/img/calendar3.gif' id='date_pic' class="expand" title='Calendrier' />)
			 		 		<img src="./styles/img/eraser.gif" onclick="document.getElementById('date').value='';" class="expand" title="vider" />
					 		<script type='text/javascript'>
									Calendar.setup({
        								inputField     :    'date',     
        								ifFormat       :    '%d/%m/%Y',      
        								button         :    'date_pic',  
        								align          :    'Tr'            
									});
							</script>
						</td>
					</tr>
					<tr>
						<td>Intitul�</td>
						<td>:</td>
						<td>
							<input type="text" name="intitule" id="intitule" value="<?php echo $this->formation->getIntitule(); ?>"></input>
						</td>
					</tr>
					<tr>
						<td valign="top">Commentaire</td>
						<td valign="top">:</td>
						<td colspan="2">
							<textarea name="commentaire" id="commentaire" rows="5" cols="50"><?php echo $this->formation->getCommentaire(); ?></textarea>
						</td>
					</tr>
					<tr>
						<td>Site internet</td>
						<td>:</td>
						<td>
							<input type="text" name="website" id="website" value="<?php echo $this->formation->getWebsite(); ?>"></input>
						</td>
					</tr>
					<tr id="tr_assOp">
						<td valign="top">Associer un/des formateur(s)</td>
						<td valign="top">:</td>
						<td valign="top">
							<select style="width: 230px;" size="3" id="assocFormateur" disabled="disabled">
							<?php
							$formateursAssoc = $this->formation->getFormateursAssocies();
							if(!empty($formateursAssoc))
							{
								foreach ($formateursAssoc as $formateurAssoc) {
									echo '<option value="'.$formateurAssoc->getId().'">'.urldecode($formateurAssoc->getLabel()).'</option>';
									
								}								
							}
							?>
							</select>
						</td>
						<td>
							<img src="./styles/img/invite.gif" id="addFormateur" class="expand" onclick="ShowListFormateur('&close=SetListFormateur')" title="Associer un/des formateur(s)" /><br /><br />
							<img src="./styles/img/eraser.gif" onclick="EmptyList('assocFormateur','selectedFormateurs','selectedFormateursName');" class="expand" title="vider" />
						</td>
					</tr>
					
							<?php
						if ($this->_getUser()->isCoordinateur() || $this->_getUser()->isAdmin()):
						$_disabled = !($this->_getUser()->isCoordinateur() || $this->_getUser()->isAdmin());
					?>
					<tr>
						<td>Global</td>
						<td>:</td>
						<td>
							<input type="checkbox" name="swGlobal" <?php echo (($_disabled)?'disabled="disabled" ':''); if($this->formation->getGlobal() == 1) echo 'checked="checked"'; ?> id="swGlobal" value="1">
						</td>
					</tr>
					<?php else: ?>
						<input type="hidden" name="swGlobal" value="<?php if($this->formation->getGlobal() == 1){echo '1';} else { echo '0';} ?>">
					<?php endif; ?>
			</table>
			
			<!-- Etablissement -->
			<div>
				<u>Associer � un/des �tablissement(s)</u> &nbsp;&nbsp; 
				<img src="./styles/img/etablissement.png" id="searchEtablissements" onclick="ShowEtablissement('&close=SetEtablissementsAction');return false" class='expand' />				
				<?php 
					//$nbL=0; if(isset($_POST['nbLignes'])) $nbL = $_POST['nbLignes'];
					$nbL=0; 
					$etabIds_temp = "";
					if($this->formation->getActionEtablissements() != null && $this->formation->getActionEtablissements()->count() >0 ) { 
						$nbL = $this->formation->getActionEtablissements()->count();
						for ($i=0; $i < $this->formation->getActionEtablissements()->count();$i++) {
							$etabIds_temp .= (strlen($etabIds_temp) != 0 ? ';' : '') . $this->formation->getActionEtablissements()->items($i)->GetEtablissementId();							
						}
					}
				?>
				<input type="hidden" id="etabIds" name="etabIds" value='<?php echo $etabIds_temp; ?>' />
				<input name='nbLignes' id='nbLignes' type='hidden' value='<?php echo $nbL ?>' />
			</div><br /> <br />
			<div id="divEtablissementsAction">
			<?php 
			// if((isset($_POST['nbLignes']) && $_POST['nbLignes']>0))
			if ($this->formation->getActionEtablissements() != null && $this->formation->getActionEtablissements()->count() >0  )
			{
				$this->createTableEtablissementAction();
			}
			?>
			</div><br />
			<!-- Contact -->
			<div>
				<u>Associer � un/des contact(s)</u> &nbsp;&nbsp; 
				<img src="./styles/img/add_contact.gif" id="searchContacts" onclick="ShowContactList('&close=SetContactsAction');return false" class='expand' /> 
				<?php 
					// $nbL=0; if(isset($_POST['nbLignesContact'])) $nbL = $_POST['nbLignesContact'];
					$nbL=0; 
					$contactIds_temp = "";
					if($this->formation->getActionContacts() != null && $this->formation->getActionContacts()->count() >0) {
						$nbL = $this->formation->getActionContacts()->count();						
						for ($i=0; $i < $this->formation->getActionContacts()->count();$i++ ) {
							$contactIds_temp = (strlen($contactIds_temp) != 0 ? ';' : '') . $this->formation->getActionContacts()->items($i)->GetContactId();
							
						}
					}	
				?>
				<input type="hidden" id="contactIds" name="contactIds" value='<?php echo $contactIds_temp ?>' />				
				<input name='nbLignesContact' id='nbLignesContact' type='hidden' value='<?php echo $nbL ?>' />
			</div><br /><br/>
			<div id="divContactsAction">
			<?php 
				if($this->formation->getActionContacts() != null && $this->formation->getActionContacts()->count() >0)
					$this->createTableContactAction();
			?>
			</div>
			<br /><br />
			<div>
				<hr>
			</div>

		<table>
			<tr>
				<td><i>Nombre global d'apprenants participant � la formation</i></td>
				<td>:</td>
				<td>
					<input type="text" value="<?php echo $this->formation->getNbApprenants(); ?>" name="nbGlobalApp" id="nbGlobalApp" size="5"></input>
				</td>
			</tr>
			<tr>	
				<td><i>Nombre global d'enseignants encadrant la formation</i></td>
				<td>:</td>
				<td>
					<input type="text" value="<?php echo $this->formation->getNbEnseignants(); ?>" name="nbGlobalEns" id="nbGlobalEns" size="5"></input>
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">
					<u>Veuillez d�tailler la r�partition du nombre d'apprenants participant selon <br />les cl�s de r�partition ci-dessous.</u>
				</td>
			<td></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>R�partition par niveau-degr�-fili�re</b></td>
			<td align="center">Nb Appr</td>
		</tr>
		
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<?php
		//var_dump($this->filieresDegree);
		if (count($this->degreNiveauList) > 0)
		{
			foreach($this->degreNiveauList as $v)
			{
				echo '<tr>';
				echo '<td>'.$v->GetLabel().'</td>';
				echo '<td>:</td>';
				echo '<td><input type="text" value="'. (isset($this->filieresDegree[$v->GetFiliereDegreNiveauId()]) ? $this->filieresDegree[$v->GetFiliereDegreNiveauId()] : '') .'" name="'.$v->GetFiliereDegreNiveauId().'" id="'.$v->GetFiliereDegreNiveauId().'" size="5"></input></td>';
				echo '</tr>';
				// $this->data[ $v->GetFiliereDegreNiveauId() ]
				// $filieresDegree				
	
			}
		}
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<?php
		//var_dump($this->finalitePedago);
		if (count($this->pedagogList) > 0)
		{
			foreach($this->pedagogList as $i => $v)
			{
				echo '<tr id="tr_finalite_'.$i.'">';
				echo '<td>'.$v->GetLabel().'</td>';
				echo '<td>:</td>';
				echo '<td><input type="text" value="'. (isset($this->finalitePedago[$v->GetFinalitePedagogId()]) ? $this->finalitePedago[$v->GetFinalitePedagogId()] : '') .'" name="'.$v->GetFinalitePedagogId().'" id="'.$v->GetFinalitePedagogId().'" size="5"></input></td>';
				echo '</tr>';
				// $this->data[$v->GetFinalitePedagogId()]
				// public $finalitePedago
			}
		}
		?>
	</table>
	<input type="hidden" value="<?php echo count($this->pedagogList); ?>" id="nbFinalitePeda" />
		
	<input type="hidden" name="updateForm" />
			  
		<?php  if((	$this->_getUser()->isAdmin()||
					$this->_getUser()->isAgentCoordinateur() || 
					$this->_getUser()->isCoordinateur())||
					((!$this->formation->isValid()) && $this->formation->checkAnneeEncodage()  && (!$this->_getUser()->isFormateur() || ($this->_getUser()->isFormateur() && $this->formation->GetCreatedBy() == $this->_getUser()->getUtilisateurId())))){?>
		<div class="boxBtn">
			<table class="buttonTable">
				<tr>
					<td onclick="submitFormFormation('formFormation', 'en_cours', 'update')">Modifier</td>
					<td class="separator"></td>
					<td onclick="deleteAction('<?php echo $this->formation->getFormationId(); ?>')">Supprimer</td>
					<td class="separator"></td>
				</tr>
			</table>
		</div>
		<?php }?>
	</div>
	</form>
	</div>

<?php
	}
	
	public function createTableEtablissementAction()
	{
		$idTable = 'tableEtablissementsAction';
		$idHeadTr = 'tr_head_etab';
		$headLabel='�tablissement';
		$nbLignes = $this->formation->getActionEtablissements()->count();
		
		?>
		<table id="<?php echo $idTable; ?>" style="width: 20%;">
			<tbody>
				<tr id="<?php echo $idHeadTr; ?>">
					<td>
						<center><?php echo ToHTML($headLabel); ?></center>
					</td>
					<td></td>
				</tr>
				<?php
				
				for($i=0; $i<$nbLignes; $i++)
				{
					// var_dump($this->formation->getActionEtablissements()->items($i)->GetNom());
					
					$etabName = $this->formation->getActionEtablissements()->items($i)->GetNom();
					
					$etabId = $this->formation->getActionEtablissements()->items($i)->GetEtablissementId();
					
					echo '<tr id="'.$i.'"><td style="padding: 5px; width: 100px; text-align: center;">';
					echo '<input type="text" readonly="readonly" value="' . $etabName . '" style="width: 300px;" name="etabName_'.$i.'"/>';
					echo '<input type="hidden" value="'.$etabId.'" id="etabId_'.$i.'" name="etabId_'.$i.'"/>';
					echo '</td><td style="padding: 5px; width: 100px; text-align: center;">';
					echo '<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRow(\''.$i.'\', \'nbLignes\')"/>';
					echo '</td>';
					echo'</tr>';
					
				}
				?>
			</tbody>
		</table>
	<?php
	}
	
	public function createTableContactAction()
	{
		$idTable = 'tableContactsAction';
		$idHeadTr = 'tr_head_contact';
		$headLabel='Contact';
		//$nbLignes = $_POST['nbLignesContact'];
		$nbLignes = $this->formation->getActionContacts()->count();
		?>
		<table id="<?php echo $idTable; ?>" style="width: 20%;">
			<tbody>
				<tr id="<?php echo $idHeadTr; ?>">
					<td>
						<center><?php echo ToHTML($headLabel); ?></center>
					</td>
					<td>
						<center>R�le</center>
					</td>
				</tr>
				<?php
				for($i=0; $i<$nbLignes; $i++)
				{
					$contactName = $this->formation->getActionContacts()->items($i)->GetContact()->GetNom();
					$contactId = $this->formation->getActionContacts()->items($i)->GetContactId();
					$roleContactId = $this->formation->getActionContacts()->items($i)->GetRoleContactActionId();
					
					echo '<tr id="id_contact_tr_'.$i.'"><td style="padding: 5px; width: 100px; text-align: center;">';
					echo '<input type="text" readonly="readonly" value="'.$contactName.'" style="width: 300px;" name="contactName_'.$i.'"/>';
					echo '<input type="hidden" value="'.$contactId . '" id="contactId_'.$i.'" name="contactId_'.$i.'"/>';
					echo '</td><td style="padding: 5px; width: 100px; text-align: center;">';
					echo '<select name="roleActionContact_'.$i.'" id="roleActionContact_'.$i.'" style="width:175px;">';
					echo '<option value="-1"></option>';
					echo createSelectOptions(Dictionnaries::getRoleContactActionList(), $roleContactId);
					echo '</select>';
					echo '</td><td style="padding: 5px; width: 100px; text-align: center;">';
					echo '<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRow(\'id_contact_tr_'.$i.'\', \'nbLignesContact\')"/>';
					echo '</td>';
					echo'</tr>';
				
				}
				?>
			</tbody>
		</table>
		
<?php }
		
	
}
?>
