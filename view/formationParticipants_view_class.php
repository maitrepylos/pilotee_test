<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class FormationParticipantsView extends BaseView
{
	public $participants;
	public $etablissements;
	public $tranchesdage;
	
	
	public function Render()
	{
?>
		
		<script>
			function ParticipantsViewClass() {}

			ParticipantsViewClass.prototype.Close = function()
			{
				parent.Popup.hide("ParticipantsFormation");
			};
			
			window.histo = new ParticipantsViewClass();
		</script>
		
		<style type="text/css" title="currentStyle">
			@import "media/css/demo_page.css";
			@import "media/css/demo_table.css";
			@import "media/css/demo_validation.css";
			@import "media/css/themes/base/jquery-ui.css";
			@import "media/css/themes/smoothness/jquery-ui-1.7.2.custom.css";
		</style>

        <script src="media/js/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="media/js/jquery.jeditable.js" type="text/javascript"></script>
        <script src="media/js/jquery-ui.js" type="text/javascript"></script>
        <script src="media/js/jquery.validate.js" type="text/javascript"></script>
        <script src="media/js/jquery.dataTables.editable.js" type="text/javascript"></script>
		
		<?php
			$etablissements_tmp = array(); 
			foreach ($this->etablissements as $et) {
				$etablissements_tmp[$et->getId()] = htmlentities($et->getNom(), ENT_QUOTES) ;
			}
			
			$tranchesdage_tmp = array();
			foreach ($this->tranchesdage as $age) {
				$tranchesdage_tmp[$age->getId()] = htmlentities($age->getLabel(), ENT_QUOTES) ;
			}
			
		?>
		
		<script type="text/javascript">
			$(document).ready(function () {
                $('#example').dataTable(
						{
						"bInfo": false,
						"bPaginate": false,
						"bFilter": false,
						"bSort": true
						}).makeEditable({
						
						"aoColumns": [
                        {
							tooltip: 'Cliquez pour modifier',
							cssclass: "required"
						},
						{
							tooltip: 'Cliquez pour modifier',
							cssclass: "required"
						},
						{
							tooltip: 'Cliquez pour modifier'
						},
						{
							tooltip: 'Cliquez pour modifier'
						},
						{
							tooltip: 'Cliquez pour modifier',
							cssclass: "email"
						},{
							tooltip: 'Cliquez pour modifier',
							cssclass: "email"
						},
						{
                                tooltip: 'Cliquez pour modifier',
								type: 'select',
                                onblur: 'submit',
                                data: "{'2':'F','1':'M','selected':'F'}"
                        },
						{
							tooltip: 'Cliquez pour modifier',
							type: 'select',
							onblur: 'submit',
							data: '<?php print json_encode($tranchesdage_tmp);?>'
						},
						{
							tooltip: 'Cliquez pour modifier',
							type: 'select',
							onblur: 'submit',
							data: '<?php print json_encode($etablissements_tmp);?>'
							
						}
						],
						
						fnOnDeleting: function (tr, id, fnDeleteRow) {
							return confirm("Voulez-vous vraiment supprimer ce participant ?");
						}, 
						
						sUpdateURL: "index.php?action=updateformationparticipants&formation_id=<?php echo $_REQUEST['formation_id'];?>",
						sAddURL: "index.php?action=addformationparticipants",
						sAddHttpMethod: "POST", //Used only on google.code live example because google.code server do not support POST request
						sDeleteURL: "<?php echo $this->BuildURL('formationParticipantsNone') ?>&action=delete"
				
				});
            });
			
		</script>
		
	</head>
			
		<div class="boxGen" style="border:0px;">
			<div class="boxPan" style="padding: 5px;">
			<div id="container"><h3>Pour �diter un champ, il suffit de double-cliquer dessus !</h3><br /><br />


 <form id="formAddNewRow" title="Add new record" style="text-align:left">
    <label for="engine">Nom</label><br />
	<input type="text" name="nom" id="nom" class="required" rel="0" />
    <br />
    <label for="browser">Prenom</label><br />
	<input type="text" name="prenom" id="prenom" class="required" rel="1" />
    <br />
    <label for="platforms">Tel1</label><br />
	<input type="text" name="tel1" id="tel1" rel="2" />	
    <br />
	<label for="platforms">Tel2</label><br />
	<input type="text" name="tel2" id="tel2" rel="3" />
	<br />
	<label for="platforms">Mail1</label><br />
	<input type="text" name="mail1" id="mail1" class="email" rel="4" />
	<br />
	<label for="platforms">Mail2</label><br />
	<input type="text" name="mail2" id="mail2" class="email" rel="5" />
	<br />
	<label for="platforms">Genre</label><br />
	<select name="genre" id="genre" rel="6" style="width: auto;">
                <option value="2" selected>F</option>
                <option value="1">M</option>                
        </select>
	<br />
	<label for="platforms">age</label><br />
	<select name="age" id="age" rel="7" style="width: auto;">
	<?php
		foreach ($this->tranchesdage as $age) {
			echo '<option value="' . $age->getId()  . '">' . $age->getLabel() . '</option>';
		}
	?>
	</select>
	<br />
	<label for="platforms">Etablissement</label><br />
	<select name="ets" id="ets" rel="8">
	<?php
		foreach ($this->etablissements as $et) {
			echo '<option value="' . $et->getId()  . '">' . $et->getNom() . '</option>';
		}
	?>
	</select>
	<br />
	<br />
	<input type="hidden" name="formation_id" value="<?php echo $_REQUEST['formation_id'];?>" />
</form>

<div id="demo">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>Nom</th>
			<th>Pr�nom</th>

			<th>Tel1</th>
			<th>Tel2</th>
			<th>Email1</th>
			<th>Email2</th>
			<th>Genre</th>
			<th>Age</th>
			<th>Etablissement</th>
		</tr>
	</thead>
	<tfoot>
		<tr>

			<th>Nom</th>
			<th>Pr�nom</th>

			<th>Tel1</th>
			<th>Tel2</th>
			<th>Email1</th>
			<th>Email2</th>
			<th>Genre</th>
			<th>Age</th>
			<th>Etablissement</th>
		</tr>

	</tfoot>
	<tbody>
	<?php
	
		for ($i=0;$i<count($this->participants); $i++) {
			if ( ($i%2) == 0 ) {
				echo '<tr class="odd_gradeX" id="'. $this->participants[$i]->id  .'">';
			} else {
				echo '<tr class="even_gradeC" id="'. $this->participants[$i]->id  .'">';
			}
			
			echo '<td>' . $this->participants[$i]->nom . '</td>';
			echo '<td>' . $this->participants[$i]->prenom . '</td>';
			echo '<td>' . $this->participants[$i]->telephone1 . '</td>';
			echo '<td>' . $this->participants[$i]->telephone2 . '</td>';
			echo '<td>' . $this->participants[$i]->email1 . '</td>';
			echo '<td>' . $this->participants[$i]->email2 . '</td>';
			echo '<td>' . $this->participants[$i]->getGenreName() . '</td>';
			echo '<td>' . $this->participants[$i]->getAgeName() . '</td>';
			echo '<td>' . $this->participants[$i]->getEtablissementName()->GetNom() . '</td>';
			
			echo '</tr>';
		}
	?>
	</tbody>
</table>
<div class="add_delete_toolbar">
<button id="btnDeleteRow">Effacer un participant</button>
<button id="btnAddNewRow">Ajouter un participant</button>
</div>
</div>

<div class="spacer"></div>	
			</div>
			<div class="boxBtn">
				<table class="buttonTable">
					<tr>
						<td class="separator"></td>
						<td onclick="javascript:window.histo.Close();">Fermer</td>
					</tr>
				</table>
			</div>
		</div>

<?php 		
	}
}
?>
