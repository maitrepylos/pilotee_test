<?php

REQUIRE_ONCE(SCRIPTPATH.'lib/abstractView.php');

class FormationView extends AbstractView
{
	protected $_results;
	protected $_actionId;
	
	/**
	 * Define result of search
	 * @param Iterable $results
	 */
	public function setResults($results)
	{
		
		$this->_results = $results;
	}

	//
	// Render for actions
	//

	protected function _addRender()
	{
		$this->_commonJsAndPopup();
		
		?>
	<script type="text/javascript">
	
	function FormActionViewClass() {}
	
	FormActionViewClass.prototype.Close = function()
	{
		parent.Popup.hide("formAddFormation");
	}
	window.favc = new FormActionViewClass();
	
	function submitFormFormation(formId, action, operation)
	{
		var errorMsg = "";
		var res1 = true;
		var chk = new CheckFunction();
		
		var organisateur = document.getElementById("organisateur");
		if(organisateur)
		{
			organisateur.disabled = false;
			if(organisateur.value=='-1')
			{
				SetInputError(organisateur);
				res1 = false;
			}
			else
			{
				SetInputValid(organisateur);
			}
		}

		var type = document.getElementById('formationType');
		if(type)
		{
			type.disabled = false;
			if(type.value=='-1')
			{
				SetInputError(type);
				res1 = false;
			}
			else
			{
				SetInputValid(type);
			}
		}

		var intitule = document.getElementById('intitule');
		if (intitule.value=='')
		{
			SetInputError(intitule);
			res1 = false;
		}
		else
		{
			SetInputValid(intitule);
		}

		var assocformateurs = document.getElementById('assocFormateur');
		
		if (assocformateurs.length == 0)
		{
			SetInputError(assocformateurs);
			res1 = false;
		}
		else
		{
			SetInputValid(assocformateurs);
		}

		
		var nbAppTest = document.getElementById('nbGlobalApp');

		if(nbAppTest) {
			if (nbAppTest.value == '') {
				SetInputError(nbAppTest);
				res1 = false;
			} else {
				SetInputValid(nbAppTest);
			} 
				
		}

		var nbEnsTest = document.getElementById('nbGlobalEns');

		if(nbEnsTest) {
			if (nbEnsTest.value == '') {
				SetInputError(nbEnsTest);
				res1 = false;
			} else {
				SetInputValid(nbEnsTest);
			} 
				
		}
		// debug FFI : ajouter l'initialisation des variables
		var nbApprenantsFiliere = 0;
		var nbApprenantsPeda = 0; 

		//Verif fili�re
		<?php
		$niveauDegreFiliere = array();
		if (count($this->degreNiveauList) > 0)
		{
			foreach($this->degreNiveauList as $v)
			{
			?>
				var val = null;
				val = document.getElementById('<?php echo $v->GetFiliereDegreNiveauId() ?>');
				chk.Add(val, 'INT', false);	
				nbApprenantsFiliere += intval(val.value);
			<?php
			}
		}
		
		//P�dagogie
		if (count($this->pedagogList) > 0)
		{
			foreach($this->pedagogList as $v)
			{
			?>
				var val = null;
				val = document.getElementById('<?php echo $v->GetFinalitePedagogId() ?>');
				chk.Add(val, 'INT', false);	
				nbApprenantsPeda += intval(val.value);
			<?php
			}
		}
		?>

		var global = document.getElementById("swGlobal");
		var nbEtab = document.getElementById("nbLignes");
		var nbContact = document.getElementById("nbLignesContact");

		if(!global && nbEtab.value <= 0)
		{
			res1=false;
			errorMsg += 'Veuillez associer la formation � un/des �tablissement(s) !\n';
		}
		else if(global && !global.checked && nbEtab.value <= 0)
		{
			res1=false;
			errorMsg += 'Veuillez associer la formation � un/des �tablissement(s) ou mentionnez la formation comme �tant globale !\n';
		}

		 if(nbContact.value<=0)
		{
			res1= false;
			errorMsg += 'Veuillez associer l\'action � un/des contact(s)\n';
		}
		else
		{
			for(i=0; i<nbContact.value; i++)
			{
				role = document.getElementById('roleActionContact_'+i);
				if(role)
				{
					if(role.value == '-1')
					{
						res1 = false;
						SetInputError(role);
					}
					else
					{
						SetInputValid(role);
					}
				}
			}
		}

		var date = document.getElementById('date');
		if (isDate(date.value)==false)
		{
			SetInputError(date);
			res1=false;
		}
		else
		{
			SetInputValid(date);
		}

		var res = chk.IsValid();
		if (res && res1)
		{
			// alert("valid");
			if(action == 'valider')
			{
				boMsg = false;
				message=''; 
				if(intval(nbApprenantsFiliere) != intval(nbGlobalApp.value) && nbApprenantsFiliere != 0)
				{
					message += 'Le nombre global d\'apprenants participant � la formation pour la r�partition par niveau-degr�-fili�re est diff�rent de '+nbGlobalApp.value+'\n';
					boMsg = true;
				}
		
				if(intval(nbApprenantsPeda) != intval(nbGlobalApp.value) && nbApprenantsPeda!=0)
				{
					message +='Le nombre global d\'apprenants participant � la formation pour la r�partition par finalit� p�dagogique est diff�rent de '+nbGlobalApp.value+'\n';
					boMsg=true;
				}
				
				if(nbApprenantsFiliere==0 && nbApprenantsPeda==0)
				{
					message+='Veuillez d�tailler la r�partition du nombre d\'apprenants participant selon les cl�s de r�partition';
					boMsg=true;
				}					

				if(boMsg)
				{
					alert(message);
					location.href='#top';
					return false;
				}
			}

			var form;
			form = document.getElementById ? document.getElementById(formId): document.forms[formId];
			var act = "<?php echo $this->BuildURL($_REQUEST['module']) ?>";
			if(operation=='add')
				act += "&action=addFormation";
			else
				act += "&action=edit";

			if(action=='valider')
				act += "&statut=valider";
			else
				act += "&statut=enCours";
			
			form.setAttribute("action", act);
			
			// alert(act);
			form.submit();
			// parent.Popup.hide("formAddFormation");
		} 
		else 
		{
			// alert("pas valid");
			if(errorMsg.length>0)
				alert(errorMsg);

			location.href='#top';
			return false;
		}
	}

	function ShowContactList(closeCallBack)
	{
		nbLignesEtab = document.getElementById("nbLignes").value;
		swGlobal = document.getElementById("swGlobal");
		
		if(nbLignesEtab>0 || (swGlobal && swGlobal.checked))
		{
			var urlEtab = "";
			if(nbLignesEtab>0)
			{
				var etabSelect = document.getElementById("etabIds");
				urlEtab += "&etabs="+etabSelect.value;
			}

			if(swGlobal && swGlobal.checked)
			{
				urlEtab += "&global=1";
			}
			url = '<?php echo SCRIPTPATH; ?>index.php?module=contactPopup&action=contactList';
			var frm = document.getElementById("frmContacts");
			frm.src = url+closeCallBack+urlEtab;
			Popup.showModal('contacts');						
		}
		else
			alert('Veuillez associer l\'action � un �tablissement avant de l\'associer � un contact'); 
	}
	function SetContactsAction(objectsArray, close)
	{
		if (objectsArray.length > 0)		  
		{
			var nbLignes = 0;
			var divGlobal = document.getElementById("divContactsAction");
			var tab = document.getElementById('tableContactsAction');
			if(tab==null)
			{
				document.getElementById('contactIds').value = '';
 				tab = divGlobal.appendChild(document.createElement("table"));
 				tab.setAttribute("id","tableContactsAction");
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id","tr_head_contact");
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>Contact</center>";
 				newCell = newRow.insertCell(1);
 				newCell.innerHTML = "<center>R�le</center>";
 				
 			}
 			else
 				nbLignes = document.getElementById("nbLignesContact").value;
				
			autoid = nbLignes;			
			for(j=0;j<objectsArray.length;j++)
			{
				var etabSelect = document.getElementById("contactIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);	 				
	 				newRow.setAttribute("id","id_contact_tr_"+nbLignes);
	 				
					 for(i=0;i<3;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
 							case 0 :
 								tmp = '<input type="text" name="contactName_'+nbLignes+'" style="width:300px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 									  '<input name="contactId_'+nbLignes+'" id="contactId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 								newCell.innerHTML = tmp;
 								break;
 							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>"; // FFI Juin 2012 : participants � une formation pas introduit par ici !!!!
								newCell.innerHTML = "<select name='roleActionContact_"+nbLignes+"' id='roleActionContact_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
 								break;
	 							
 							case 2 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"id_contact_tr_"+nbLignes+"\", \"nbLignesContact\")'/>";
 								break; 							
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }				

			}
			 document.getElementById("nbLignesContact").value = nbLignes;
		}
		
		if(close)Popup.hide('contacts');
	}
	
	function SetEtablissementsAction(objectsArray, close)
	{
		if (objectsArray.length > 0)		  
		{
			var nbLignes = 0;
			var divGlobal = document.getElementById("divEtablissementsAction");
			var tab = document.getElementById('tableEtablissementsAction');
			if(tab==null)
			{
				document.getElementById('etabIds').value = '';
 				tab = divGlobal.appendChild(document.createElement("table"));
 				tab.setAttribute("id","tableEtablissementsAction");
			
	 			//Premi�re ligne du tableau
 				newRow = tab.insertRow(-1);
 				newRow.setAttribute("id","tr_head_etab");
 				newCell = newRow.insertCell(0);
 				newCell.innerHTML = "<center>�tablissement</center>";
 			}
 			else
 			{
 				nbLignes = document.getElementById("nbLignes").value;
 			}
			autoid = nbLignes;			
		 	for(j=0;j<objectsArray.length;j++)
 			{
				var etabSelect = document.getElementById("etabIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
 					newRow = tab.insertRow(-1);
	 				newRow.setAttribute("id",nbLignes);

					 for(i=0;i<2;i++)
 			 		 { 		 		 
 		 			 	newCell = newRow.insertCell(i);
 		 			 	var tmp = "";
	 					switch(i)
 						{
 							case 0 :
 								tmp = '<input type="text" name="etabName_'+nbLignes+'" style="width:300px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
 									  '<input name="etabId_'+nbLignes+'" id="etabId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
 								newCell.innerHTML = tmp;
 								break;
 							case 1 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\""+nbLignes+"\", \"nbLignes\")'/>";
 								break; 							
 						}
 					
 						with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
 						autoid++;
 				 	}
 				 	nbLignes++;
 				 }
 			  }
 			  document.getElementById("nbLignes").value = nbLignes;
 			}
		if(close)Popup.hide('etablissements');
	}


	</script>
	<div id="top"></div>
	
		<div id="etablissements" class="popup" style="width:984px">
		<div class="popupPanel" style="width:984px">
			<div class="popupPanelHeader">
							<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Recherche sur �tablissements</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('etablissements');reloadFrameBlank('frmEtablissements');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:984px">
				<iframe id="frmEtablissements" frameborder=0 src="" style="width: 984px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	<div id="contacts" class="popup" style="width:984px">
		<div class="popupPanel" style="width:984px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Liste des contacts</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('contacts');reloadFrameBlank('frmContacts');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:984px">
				<iframe id="frmContacts" frameborder=0 src="" style="width: 984px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>
	
	<?php
		
		include_once SCRIPTPATH . 'view/partial/popup_listFormateur.php';
		
		echo '<script type="text/javascript">';
		include_once SCRIPTPATH . 'view/partial/js_etablissement.php';
		echo '</script>';
	
		
		$disabled = '';
		$hidden_field = array(); // key : value
		if($this->isUpdate):
			$disabled = 'disabled=\'disabled\'';
		?>
		<span style="padding-left: 5px;" class="title">&gt;&gt; Modifier la formation</span><br/><br/>
		<?php endif; ?>
	<div class="boxGen">
		<form id="formFormation" name="formFormation" action="" method="post">
			<?php
				/*$hidden_field['selectedFormateurs'] = ToHTML($this->data["selectedFormateurs"]);
				$hidden_field['selectedFormateursName'] = ToHTML($this->data["selectedFormateursName"]);*/
				$hidden_field['selectedFormateurs'] = '';
				$hidden_field['selectedFormateursName'] = '';
				$hidden_field['formation_id'] = ToHTML($this->data["formation_id"]);
			?>
			<div class="boxPan" style="padding: 5px;">
				<table>
					<tr>
						<td>Type de formation</td>
						<td>:</td>
						<td>						
							<select <?php echo $disabled;?> id="formationType" name="formationType" style="width: 230px;">
							<?php
								echo '<option value="-1"></option>';
								// echo createSelectOptions(Dictionnaries::getTypeActionList($user->isOperateur(),$user->isAgent(),false), $_POST['formationType']);
								// echo createSelectOptions(Dictionnaries::getTypeActionList($this->_getUser()->isOperateur(),$this->_getUser()->isAgent(),false), $_POST['formationType']);
								// TODO : change the type action list into the type formation list
								// TODO : change the user : is formateur ?
								echo createSelectOptions($this->formationTypes, (isset($_POST['formationType']) ? $_POST['formationType'] : -1));
								
							?>
							</select>
						</td>
					</tr>
					<?php
						if($this->_getUser()->isFormateur())
						{
							$hidden_field['organisateur'] = ToHTML($this->_getUser()->getFormateurId()); // TODO : check getFormteurId
						}
						else
						{
					?>
					<tr id="tr_formateur">
						<td>Formateur organisateur</td>
						<td>:</td>
						<td>
							<select <?php echo $disabled;?>  id="organisateur" name="organisateur" style="width: 230px;">
							<?php
							echo '<option value="-1"></option>';
							echo createSelectOptions($this->formateurs, $this->data['organisateur']);
							?>
							</select>
						</td>
					</tr>
					<?php } ?>
					<tr>
						<td>Ann�e scolaire</td>
						<td>:</td>
						<td>
							
							<select id="anneesScolaire" name="anneesScolaire" style="width: 230px;">
							<?php		
							if (isset($this->anneeEnCours)) {
								echo createSelectOptions(Dictionnaries::getAnneeList(), $this->anneeEnCours);
							}
							else
							{
								echo createSelectOptions(Dictionnaries::getAnneeList());			
							}
							?>
							</select>
						</td>
						<td>
							(<input style="width: 100px" type="text" maxlength="10" value="<?php echo $this->data['date']; ?>" name="date" id="date" />
					 		<img src='./styles/img/calendar3.gif' id='date_pic' class="expand" title='Calendrier' />)
			 		 		<img src="./styles/img/eraser.gif" onclick="document.getElementById('date').value='';" class="expand" title="vider" />
					 		<script type='text/javascript'>
									Calendar.setup({
        								inputField     :    'date',     
        								ifFormat       :    '%d/%m/%Y',      
        								button         :    'date_pic',  
        								align          :    'Tr'            
									});
							</script>
						</td>
					</tr>
					<tr>
						<td>Intitul�</td>
						<td>:</td>
						<td>
							<input type="text" name="intitule" id="intitule" value="<?php echo $this->data['intitule']; ?>"></input>
						</td>
					</tr>
					<tr>
						<td valign="top">Commentaire</td>
						<td valign="top">:</td>
						<td colspan="2">
							<textarea name="commentaire" id="commentaire" rows="5" cols="50"><?php echo $this->data['commentaire']; ?></textarea>
						</td>
					</tr>
					<tr>
						<td>Site internet</td>
						<td>:</td>
						<td>
							<input type="text" name="website" id="website" value="<?php echo $this->data['website']; ?>"></input>
						</td>
					</tr>
					<tr id="tr_assOp">
						<td valign="top">Associer un/des formateur(s)</td>
						<td valign="top">:</td>
						<td valign="top">
							<select style="width: 230px;" size="3" id="assocFormateur" disabled="disabled">
							<?php
							if(!empty($this->data["selectedFormateurs"]) && !empty($this->data["selectedFormateursName"]))
							{
								$ids = explode(";", $this->data["selectedFormateurs"]);
								$names = explode(";", $this->data["selectedFormateursName"]);
								for ($i=0; $i<count($ids); $i++)
									echo '<option value="'.$ids[$i].'">'.urldecode($names[$i]).'</option>';
							}
							?>
							</select>
						</td>
						<td>
							<img src="./styles/img/invite.gif" id="addFormateur" class="expand" onclick="ShowListFormateur('&close=SetListFormateur')" title="Associer un/des formateur(s)" /><br /><br />
							<img src="./styles/img/eraser.gif" onclick="EmptyList('assocFormateur','selectedFormateurs','selectedFormateursName');" class="expand" title="vider" />
						</td>
					</tr>
			
					<?php
						if ($this->_getUser()->isCoordinateur() || $this->_getUser()->isAdmin()): // || $boUpdate):
						$_disabled = !($this->_getUser()->isCoordinateur() || $this->_getUser()->isAdmin());
					?>
					<tr>
						<td>Global</td>
						<td>:</td>
						<td>
							<input type="checkbox" name="swGlobal" <?php echo (($_disabled)?'disabled="disabled" ':''); if(isset($_POST['swGlobal'])) echo 'checked="checked"'; ?> id="swGlobal" value="1">
						</td>
					</tr>
					<?php else: ?>
						<input type="hidden" name="swGlobal" value="<?php if(isset($_POST['swGlobal'])){echo '1';} else { echo '0';} ?>">
					<?php endif; ?>		
			</table>
						
			<!-- Etablissement -->
			<div>
				<u>Associer � un/des �tablissement(s)</u> &nbsp;&nbsp; 
				<img src="./styles/img/etablissement.png" id="searchEtablissements" onclick="ShowEtablissement('&close=SetEtablissementsAction');return false" class='expand' />
				<input type="hidden" id="etabIds" name="etabIds" value="<?php if(isset($_POST['nbLignes'])) echo $_POST['etabIds']; ?>">
				<?php $nbL=0; if(isset($_POST['nbLignes'])) $nbL = $_POST['nbLignes'];?>
				<input name='nbLignes' id='nbLignes' type='hidden' value='<?php echo $nbL ?>' />
			</div><br /> <br />
			<div id="divEtablissementsAction">
			<?php 
			if((isset($_POST['nbLignes']) && $_POST['nbLignes']>0))
			{
				$this->createTableEtablissementAction();
			}
			?>
			</div><br />
			<!-- Contact -->
			<div>
				<u>Associer � un/des contact(s)</u> &nbsp;&nbsp; 
				<img src="./styles/img/add_contact.gif" id="searchContacts" onclick="ShowContactList('&close=SetContactsAction');return false" class='expand' /> 
				<input type="hidden" id="contactIds" name="contactIds" value="<?php echo  !isset($_POST['contactIds']) ? "" : $_POST['contactIds']; ?>">
				<?php $nbL=0; if(isset($_POST['nbLignesContact'])) $nbL =  !isset($_POST['nbLignesContact']) ? "" : $_POST['nbLignesContact'];?>
				<input name='nbLignesContact' id='nbLignesContact' type='hidden' value='<?php echo $nbL ?>' />
			</div><br /><br/>
			<div id="divContactsAction">
			<?php 
				if((isset($_POST['nbLignesContact']) && $_POST['nbLignesContact']>0))
					$this->createTableContactAction();
			?>
			</div>
			<br /><br />
			<div>
				<hr>
			</div>
		<table>
			<tr>
				<td><i>Nombre global d'apprenants participant � la formation</i></td>
				<td>:</td>
				<td>
					<input type="text" value="<?php echo $this->data['nbGlobalApp']; ?>" name="nbGlobalApp" id="nbGlobalApp" size="5"></input>
				</td>
			</tr>
			<tr>	
				<td><i>Nombre global d'enseignants encadrant la formation</i></td>
				<td>:</td>
				<td>
					<input type="text" value="<?php echo $this->data['nbGlobalEns']; ?>" name="nbGlobalEns" id="nbGlobalEns" size="5"></input>
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">
					<u>Veuillez d�tailler la r�partition du nombre d'apprenants participant selon <br />les cl�s de r�partition ci-dessous.</u>
				</td>
			<td></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>R�partition par niveau-degr�-fili�re</b></td>
			<td align="center">Nb Appr</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<?php
		if (count($this->degreNiveauList) > 0)
		{
			foreach($this->degreNiveauList as $v)
			{
				echo '<tr>';
				echo '<td>'.$v->GetLabel().'</td>';
				echo '<td>:</td>';
				echo '<td><input type="text" value="'.$this->data[ $v->GetFiliereDegreNiveauId() ].'" name="'.$v->GetFiliereDegreNiveauId().'" id="'.$v->GetFiliereDegreNiveauId().'" size="5"></input></td>';
				echo '</tr>';
			}
		}
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<?php
		if (count($this->pedagogList) > 0)
		{
			foreach($this->pedagogList as $i => $v)
			{
				echo '<tr id="tr_finalite_'.$i.'">';
				echo '<td>'.$v->GetLabel().'</td>';
				echo '<td>:</td>';
				echo '<td><input type="text" value="'.$this->data[$v->GetFinalitePedagogId()].'" name="'.$v->GetFinalitePedagogId().'" id="'.$v->GetFinalitePedagogId().'" size="5"></input></td>';
				echo '</tr>';
			}
		}
		?>
	</table>
	<input type="hidden" value="<?php echo count($this->pedagogList); ?>" id="nbFinalitePeda" />
		<?php
			if($this->isUpdate):
				$operation = 'update';
		?>
				<input type="hidden" name="updateForm" />
		<?php
			else:
				$operation = 'add';
		?>
				<input type="hidden" name="addForm" />
		<?php endif; ?>
	</div>
	<div class="boxBtn">
		<table class="buttonTable">
			<tr>
			<?php if(($this->isUpdate && $this->data['statutFormationId'] != 'VALID') || !$this->isUpdate): ?>
				<td style="width:180px;" onclick="submitFormFormation('formFormation', 'en_cours', '<?php echo $operation;?>')">Sauvegarder en Brouillon</td>
				<td class="separator"></td>
			<?php endif; ?>
				<td style="width:180px;" onclick="submitFormFormation('formFormation', 'valider', '<?php echo $operation;?>')">Sauvegarder en Publi�</td>
				<td class="separator"></td>
				<td onclick="clearForm('formFormation')">Effacer</td>
				<?php if(!$this->isUpdate): ?>
				<td class="separator"></td>
				<td onclick="window.favc.Close();">Fermer</td>
				<?php endif; ?>
			</tr>
		</table>
	</div>
	<?php
		foreach($hidden_field as $k => $v)
		{
			echo '<input type="hidden" value="' . $v . '" name="' . $k . '" id="' . $k . '" />';
		}
	?>
</form>
</div>
<?php 
		if(isset($this->_actionId))
		{
	?>
	<script>
		url = '<?php echo SCRIPTPATH . 'index.php?module=formationDetail&id='.$this->_actionId; ?>';
		parent.window.location.href = url;
		window.favc.Close();
	</script>
	<?php
		}
	}

	protected function _searchRender()
	{
		
		$this->_commonJsAndPopup();
		?>
<script type="text/javascript">
	
	function submitF(formId)
	{
		var date1 = document.getElementById('annee_ante');
		var date2 = document.getElementById('annee_post');
		var datedate1 = new Date();
		var datedate2 = new Date();
		var errormsg = "";

		res1 = true;
		if(date1.value!='')
		{
			if (isDate(date1.value)==false)
			{
				SetInputError(date1);
				res1=false;
			}
			else
			{
				var datetmp = date1.value.split("/");				
				//datedate1.setFullYear(intval(datetmp[2]),intval(datetmp[1])-1,intval(datetmp[0]));
				datedate1 = new Date(intval(datetmp[2]),intval(datetmp[1])-1,intval(datetmp[0]));				
				SetInputValid(date1);
			}
		}

		if(date2.value!='')
		{
			if (isDate(date2.value)==false)
			{
				SetInputError(date2);
				res1=false;
			}
			else
			{
				var datetmp = date2.value.split("/");				
				//datedate2.setFullYear(intval(datetmp[2]),intval(datetmp[1])-1,intval(datetmp[0]));
				datedate2 = new Date(intval(datetmp[2]),intval(datetmp[1])-1,intval(datetmp[0]));				
				SetInputValid(date2);
			}
		}
		
		if (date1.value!='' && date2.value!='' && datedate2 < datedate1) {
			
			SetInputError(date1);
			SetInputError(date2);
			res1 = false;
			errormsg = "La date de d�but doit �tre ant�rieure � la date de fin";
		} else {
			SetInputValid(date1);
			SetInputValid(date2);
		}

		if(res1)
		{
			//alert("valid");
			var form = document.getElementById ? document.getElementById(formId): document.forms[ formId ];
			// alert(form.action);
			form.submit();
		}
		else
		{
			if (errormsg != "") alert(errormsg);
			return false;
		}
	}

	</script>

<div class="boxGen"<?php if ($_REQUEST['module'] == 'formationPopup'): ?>	style="padding-right: 5px; padding-top: 10px;" <?php endif; ?>>
	<br />
	<form id="SearchFormation"
		action="<?php echo $this->BuildURL($_REQUEST['module']) ?>"
		method="post">
		<div class="boxPan" style="padding: 5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<colgroup>
					<col width="175"></col>
					<col width="10"></col>
					<col width="150"></col>
					<col width="30"></col>
					<col width="175"></col>
					<col width="10"></col>
					<col width="*"></col>
				</colgroup>
				<tr>
					<td height="25">Ann�e scolaire</td>
					<td>:</td>
					<td><select id="anneesScolaire" name="anneesScolaire"
						style="width: 230px;">
							<?php
							echo '<option value="-1"></option>';
							echo createSelectOptions(Dictionnaries::getAnneeList(), $this->anneeEnCours);
							
							?>
					</select>
					</td>
					<td></td>
					<td colspan="6">(Entre <input size="20" maxlength="10"
						class="notauto" type="text" name="annee_ante" id="annee_ante"
						value="<?php if(isset($_POST["annee_ante"])) echo ToHTML($_POST["annee_ante"]) ?>" />
						<img src='./styles/img/calendar3.gif' id='date_ante_pic'
						class="expand" title='Calendrier' /> <script
							type='text/javascript'>
					Calendar.setup({
        				inputField     :    'annee_ante',     
        				ifFormat       :    '%d/%m/%Y',      
        				button         :    'date_ante_pic',  
        				align          :    'Tr'            
					});
					</script> et <input size="20" maxlength="10" class="notauto"
						type="text" name="annee_post" id="annee_post"
						value="<?php if(isset($_POST["annee_post"])) echo ToHTML($_POST["annee_post"]) ?>" />
						<img src='./styles/img/calendar3.gif' id='date_post_pic'
						class="expand" title='Calendrier' />) <script
							type='text/javascript'>
   					Calendar.setup({
        				inputField     :    'annee_post',     
        				ifFormat       :    '%d/%m/%Y',      
        				button         :    'date_post_pic',  
        				align          :    'Tr'            
					});
					</script> <img src="./styles/img/eraser.gif"
						onclick="document.getElementById('annee_ante').value='';document.getElementById('annee_post').value='';"
						class="expand" title="vider" />
					</td>
					<td class="left"></td>
				</tr>
				<tr>
					<td height="25">Intitul�</td>
					<td>:</td>
					<td><input size="40" maxlength="100" class="notauto" type="text"
						name="intitule" id="intitule"
						value="<?php if(isset($_POST["intitule"])) echo ToHTML($_POST["intitule"]) ?>" />
					</td>
					<td></td>
					<td height="25" colspan="6"><?php  foreach($this->formationTypes as $formationType):
					$checked = $this->formationTypesChecked[ $formationType->getTypeFormationId() ];
					$label = $formationType->getLabel();
					$name = 'formationType[' . $formationType->getTypeFormationId() . ']';
					?> <input type="checkbox"
					<?php if ($checked) echo 'checked="checked"'; ?>
						name="<?php echo $name; ?>" class="notauto" value="1" /> <?php echo $label; ?>&nbsp;
						<?php endforeach; ?>
					</td>
				</tr>
				<tr>
					<td>Encodage</td>
					<td>:</td>
					<td height="25" colspan="5"><input type="checkbox"
					<?php if (isset($_POST['formationEnCours'])) echo 'checked="checked"';?>
						name="formationEnCours" class="notauto" value="ENCOU">
						Brouillon&nbsp; <input type="checkbox"
						<?php if (isset($_POST['formationValide'])) echo 'checked="checked"';?>
						name="formationValide" class="notauto" value="VALID"> Publi�</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Niveau</td>
					<td>:</td>
					<td height="25"><select id="niveauFormation" name="niveauFormation"
						style="width: 230px;">
							<?php
							echo '<option value="-1"></option>';
							if (isset($_POST['niveauFormation'])) {
								echo createSelectOptions(Dictionnaries::getNiveauList(), $_POST['niveauFormation']);
							}
							else{
								echo createSelectOptions(Dictionnaries::getNiveauList());
							}
							?>
					</select>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Niveau/Degr�</td>
					<td>:</td>
					<td height="25"><select id="niveauDegreFormation"
						name="niveauDegreFormation" style="width: 230px;">
							<?php
							echo '<option value="-1"></option>';
							if (isset($_POST['niveauDegreFormation'])) {
								echo createSelectOptions(Dictionnaries::getNiveauDegreList(), $_POST['niveauDegreFormation']);
							}
							else{
								echo createSelectOptions(Dictionnaries::getNiveauDegreList());
							}
							?>
					</select>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Niveau/Degr�/Fili�re</td>
					<td>:</td>
					<td height="25"><select id="niveauDegreFilereFormation"
						name="niveauDegreFilereFormation" style="width: 230px;">
							<?php
							echo '<option value="-1"></option>';
							if (isset($_POST['niveauDegreFilereFormation'])) {
								echo createSelectOptions(Dictionnaries::getFiliereDegreNiveauList(), $_POST['niveauDegreFilereFormation']);
							}
							else{
								echo createSelectOptions(Dictionnaries::getFiliereDegreNiveauList());
							}
							?>
					</select>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<?php if(!$this->_getUser()->isFormateur()): ?>
				<tr>
					<td height="25">Prestataire</td>
					<td>:</td>
					<td><select id="prestataire" name="prestataire"
						style="width: 230px;">
							<option value="-1">Tous</option>
							<?php
							if (isset($_POST['prestataire'])) {
								echo createSelectOptions($this->formateurs, $_POST['prestataire']);
							}
							else {
								echo createSelectOptions($this->formateurs);
							}
							?>
					</select>
					</td>
					<td></td>					
					<td>					
					Global : <input type="checkbox" id="actionGlob" <?php if (isset($_POST["actionGlob"])) echo 'checked="checked"'; ?>name="actionGlob" class="notauto" value='1'>
					</td>
					<td></td>
				</tr>
				<?php
				else:
				echo '<input type="hidden" id="prestataire" name="prestataire" value="' . $this->_getUser()->getFormateurId() . '" />';
				endif;
				?>
			</table>
		</div>
		<input type="hidden" id="CloseCallBack" name="CloseCallBack"
			value="<?php echo $this->closeCallBack; ?>"></input>
		<script language="javascript" type="text/javascript">
		function ActionViewClass() {}
		ActionViewClass.prototype.Close = function(action)
		{
			var ets = new Array();
			var chks = document.getElementsByTagName("input");
		
			if (action == 'select')
			{
				for (var i = 0; i < chks.length; i++)
				{
					if (chks[i].name.match("^chk_formation_"))
					{
						if (chks[i].checked)
						{
							var id = chks[i].name.replace("chk_formation_", "");
							var nom = document.getElementById("chk_formation_name_" + id).innerHTML;
							ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
						}
					}
				}
			}
			<?php echo 'parent.' . $this->closeCallBack . '(ets, true);'; ?>
		}
		window.avc = new ActionViewClass();
	</script>
		<div class="boxBtn">
			<input type="hidden" name="searchFormations" />
			<table class="buttonTable">
				<tr>
					<td onclick="submitF('SearchFormation')">Rechercher</td>
					<td class="separator"></td>
					<td onclick="clearForm('SearchFormation')">Effacer</td>
					<?php if ($_REQUEST['module'] == 'formationPopup'): ?>
					<td class="separator"></td>
					<td onclick="javascript:window.avc.Close('select');">Selectionner</td>
					<td class="separator"></td>
					<td onclick="javascript:window.avc.Close('cancel');">Annuler</td>
					<?php endif; ?>
				</tr>
			</table>
		</div>
	</form>
</div>
<?php
// include_once SCRIPTPATH . 'view/partial/popup_etablissement.php';
		$this->_renderResultList();
	}

	protected function _addresultRender() { //FFI
		$this->_actionId = $this->getIdAction();
	
?>
	<script language="javascript" type="text/javascript">
		function submitF(formId) 
		{
			url = "<?php echo $this->BuildURL('formationDetail').'&id='.$this->_actionId ?>";
			parent.window.location.href = url;
			window.favc.Close();
		}
	</script>
	<div class="boxBtn">
		<form id="formFormation" name="formFormation" action="" method="post">			
			<table class="buttonTable">
				<tr>
					<td onclick="submitF('formFormation')">Retour</td>
					<td class="separator"></td>					
				</tr>
			</table>
		</form>
		</div>
	
<?php
	}
	
	protected function _editRender()
	{
		$this->_commonJsAndPopup();
		$this->_actionId = $this->getIdAction();
?>
		<script language="javascript" type="text/javascript">
			function submitF(formId)
			{
				var form;
				form = document.getElementById ? document.getElementById(formId): document.forms[formId];
				var act = "<?php echo $this->BuildURL('formationDetail').'&id='.$this->_actionId ?>";
				form.setAttribute("action", act);
				form.submit();
			}
		</script>
		<div class="boxBtn">
		<form id="formFormation" name="formFormation" action="" method="post">			
			<table class="buttonTable">
				<tr>
					<td onclick="submitF('formFormation')">Retour</td>
					<td class="separator"></td>					
				</tr>
			</table>
		</form>
		</div>

<?php
		//$this->_searchRender();

	}

	protected function _renderResultList()
	{
		?>
		<script language="javascript" type="text/javascript">
		function ShowFormationInfo(formationId)
		{
			window.location = '<?php echo $this->BuildURL('formationDetail') . '&id='; ?>' + formationId;
		}

		function ShowFormateurInfo(formateurId)
		{
			window.location = '<?php echo $this->BuildURL('formateurDetail') . '&id='; ?>' + formateurId;
		}

		function selectAll(selected)
		{
			var liste = document.getElementsByTagName("input");
			var re = new RegExp("^chk_action_");
			
			for (var i = 0; i < liste.length; i++)
			{
				if (re.exec(liste[i].name)) liste[i].checked = selected;
			}
		}

		function clearForm(formId)
		{
			var form, elements, i, elm;
			form = document.getElementById ? document.getElementById(formId) : document.forms[formId];

			if (document.getElementsByTagName)
			{
				elements = form.getElementsByTagName('input');
				for (i = 0, elm; elm = elements.item(i++);)
				{
					if (elm.getAttribute('type') == "text")
					{
						elm.value = '';
					}
					else if (elm.getAttribute('type') == "checkbox")
					{
						if ((elm.getAttribute('name') == 'action_lab' && elm.checked)
								|| (elm.getAttribute('name') == 'evaluation' && elm.checked)
								|| (elm.getAttribute('name') == 'swActif' && elm.checked))

							elm.checked = true;
						else
							elm.checked = false;
					}
				}
				elements = form.getElementsByTagName('textarea');
				for (i = 0, elm; elm = elements.item(i++);)
				{
					elm.value = '';
				}
				elements = form.getElementsByTagName('select');

				for (i = 0, elm; elm = elements.item(i++);)
				{
					elm.value = '-1';
				}
			}
			document.getElementById('anneesScolaire').selectedIndex= document.getElementById('anneesScolaire').length - 1;
		}
		function ShowEtablissementInfo(id)
		{
			window.location = '<?php echo $this->BuildURL('etablissementDetail') . '&id='; ?>' + id;
		}
		</script>

<div class="boxGen" style="border: 0px;">
	<br />
	<div class="boxPan" style="padding: 5px;">
		<span style="padding-left: 5px;" class="title">Liste des formations
			:&nbsp; <a class="resultLineRedirection" href="javascript:void(0);"
			onclick="selectAll(true);">tout s�lectionner</a> - <a
			class="resultLineRedirection" href="javascript:void(0);"
			onclick="selectAll(false);">tout d�selectionner</a>
		</span>
		<table width="98%;" cellspacing="0" cellpadding="2" border="1"
			style="margin-bottom: 4px; margin: 10px;" class="result">
			<tr>
				<td class="resultHeader" width="2%"></td>
				<td class="resultHeader" width="5%">Type</td>
				<td class="resultHeader">Formation</td>
				<td class="resultHeader">Etablissement</td>
				<?php if ($_REQUEST['module'] == 'formations' ): ?>
				<td class="resultHeader" width="8%">Nb d'apprenants<br />enseignants
				</td>
				<?php endif; ?>
				<td class="resultHeader" width="13%">Ann�e scolaire<br />(Date)
				</td>
				<?php if ($_REQUEST['module'] == 'formations' || $_REQUEST['module'] == 'formationPopup'): ?>
				<td class="resultHeader" width="17%">Formateur responsable</td>
				<?php endif; ?>
				<td class="resultHeader" width="3%">Encodage</td>
			</tr>
			<tbody>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<?php
				if (empty($this->_results) || count($this->_results) <= 0)
				{
					echo '<tr><td colspan="8"><i>Il n\'y a aucun r�sultat pour votre requ�te ...</i></td></tr>';
				}
				else
				{
					$intitule_simple = '<td id="chk_formation_name_%s">%s</td>';
					$intitule_avance = '<td id="chk_formation_name_%s" class="resultLine resultLineRedirection" valign="top" onclick="ShowFormationInfo(\'%s\')">%s</td>';
					
					foreach($this->_results as $result)
					{
						$class = 'class="trResult"';
						echo '<tr '.$class.'>';
						echo sprintf('<td><input type="checkbox" name="chk_formation_%s"></td>', $result->getFormationId());
						
						echo '<td class="resultLine" valign="top">' . $result->getTypeFormationId() . '</td>';
						
						if ($_REQUEST['module'] == 'formations' ) {
							echo sprintf($intitule_avance, $result->getFormationId(), $result->getFormationId(), ToHTML($result->getIntitule()));
						} else {
							echo sprintf($intitule_simple, $result->getFormationId(), ToHTML($result->getIntitule()));
						}

									
						
						$etabName = '';
						if ($result->getActionEtablissements() != null && $result->getActionEtablissements()->count() >0 ) {
							for ($i=0; $i < $result->getActionEtablissements()->count(); $i++ ) {
								if ($result->getActionEtablissements()->items($i) != null) {
									// = $result->getActionEtablissements()->items($i)->GetNom();
									if ($_REQUEST['module'] == 'formations' ) {
										$temp_etsName = '<span class="resultLineRedirection" onclick="ShowEtablissementInfo(\''.$result->getActionEtablissements()->items($i)->GetEtablissementId().'\');">'.$result->getActionEtablissements()->items($i)->GetNom().'</span>';
									} else {
										$temp_etsName = '<span class="resultLineRedirection">' . $result->getActionEtablissements()->items($i)->GetNom() . '</span>';
									}
									if ($temp_etsName != null && strlen($temp_etsName) > 0 ) $etabName .= (strlen($etabName) > 0 ? '<br>' : '') . $temp_etsName;
		
								}
							}
						}
												
						echo '<td valign="top">' . (strlen($etabName) > 0 ? $etabName : '&nbsp;') . '</td>';
						
						if ($_REQUEST['module'] == 'formations')
						{
							//echo sprintf($intitule_avance, $result->getFormationId(), $result->getFormationId(), ToHTML($result->getIntitule()));
							echo '<td class="resultLine" valign="top">'.$result->getNbApprenants().'/'.$result->getNbEnseignants().'</td>';
						}
						else
						{
							
							echo sprintf($intitule_simple, $result->getFormationId(), ToHTML($result->getIntitule()));
						}

						echo '<td class="resultLine" valign="top">'. $result->getAnneeId().' <br/>'.$result->getDateFormationString().'</td>';

						if ($_REQUEST['module'] == 'formations')
						{
							$formateurid = $result->getFormateurId();
							$formateur = $result->getFormateur();
							if (!empty($formateurid) && !empty($formateur) )
							{
								// echo'<td class="resultLine resultLineRedirection" valign="top" onclick="ShowFormateurInfo(\''.$formateurid.'\');">'. ToHTML($formateur->getLabel()).'</td>';
								echo'<td class="resultLine" valign="top">'. ToHTML($formateur->getLabel()).'</td>';
							}
							else
							{
								echo'<td class="resultLine" valign="top"></td>';
							}
						}
							
						echo '<td class="resultLine" valign="top">';
						if ($result->getStatutFormationId() == 'ENCOU') {
							echo 'Brouillon';
						}
						else {
							echo 'Publi�';
						}
						echo '</td>';
						echo '</tr>', "\r\n";
					}
				}
				?>
			</tbody>
		</table>
	</div>
</div>
<div class="ligneHeaderPan"></div>
<?php
	}

	protected function _commonJsAndPopup()
	{
		echo '<script type="text/javascript">';
		include_once SCRIPTPATH . 'view/partial/js_formation.php';
		include_once SCRIPTPATH . 'view/partial/js_contact.php';
		echo '</script>';

		include_once SCRIPTPATH . 'view/partial/popup_addFormation.php';
		include_once SCRIPTPATH . 'view/partial/popup_addContact.php';
	}
}

# EOF
