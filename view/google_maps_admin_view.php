<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class GoogleMapsAdminView extends BaseView
{
	private $user = null;

	public function render($countAll, $countMissing, $addressesList)
	{
		$this->user = Session::GetInstance()->getCurrentUser();
?>

<head>
	<?php if (isset($addressesList)) { ?>
	<script src="http://maps.google.com/maps?file=api&v=2&key=<?php echo GOOGLE_MAP_KEY; ?>" type="text/javascript"></script>
	
	<script language="javascript" type="text/javascript">
		var geocoder;
		var map;
	
		var points = new Array();
		var infos = new Array();
		var infosIdx = 0;

		var __continue = true;
	
		function initialize()
		{
			if (GBrowserIsCompatible())
			{
	        	geocoder = new GClientGeocoder();
	
		        <?php
		        	$adresses = null;
		        	
					for ($i = 0; $i < $addressesList->count(); $i++)
					{
						$rue = $addressesList->items($i)->GetRue();
						
						$cpObj = $addressesList->items($i)->GetCodepostal();
						
						$cp = isset($cpObj) ? $cpObj->GetCodePostal() : null;
						$ville = $addressesList->items($i)->GetVille(); 
	
						$adresse = $rue;
						$adresse .= ((isset($adresse) && (strlen(trim($adresse)) > 0) && (isset($ville)) && (strlen(trim($ville)) > 0)) ? ',' : '') . $ville; 
						$adresse .= ((isset($adresse) && (strlen(trim($adresse)) > 0) && (isset($cp)) && (strlen(trim($cp)) > 0)) ? ',' : '') . $cp; 
			
						echo sprintf('infos.push({id:\'%s\',adresse:\'%s\',lat:999,lng:999,updated:0});', $addressesList->items($i)->GetAdresseId(), addslashes($adresse));  
					}
		        ?>
	
				geoCodeAll();
	   		}
		}
	
		function getLatLong(response)
		{
			var delay = 0;
			
			if (!response || response.Status.code != 200)
			{
				if (response.Status.code == 620) delay = 250;
				else
				{
					document.getElementById("errors").innerHTML += "Adresse n� " + infos[infosIdx].id + " : code retour = " + response.Status.code + " -> " + GGeoStatusCode(response.Status.code) + "<br/>";
					infosIdx++;
				}
			}
	        else
			{
	          	var place = response.Placemark[0];

	        	if ((infos[infosIdx].lat != place.Point.coordinates[1]) || (infos[infosIdx].lng != place.Point.coordinates[0]))
	        	{
					infos[infosIdx].lat = place.Point.coordinates[1]; 
					infos[infosIdx].lng = place.Point.coordinates[0];
					infos[infosIdx].updated = 1;
	        	}

				infosIdx++;
	        }
	
	        setTimeout(geoCodeAll, delay);
		}
	
		function geoCodeAll() 
		{
			if (__continue)
			{
				document.getElementById("statut").innerHTML = "G�olocalisation : " + (infosIdx + 1) + " / " + infos.length + " adresse" + (infosIdx > 1 ? "s" : "") + " trait�e" + (infosIdx > 1 ? "s" : "");
				 
				if (infosIdx < infos.length)
				{
					if (infos[infosIdx].adresse == '')
					{
						document.getElementById("errors").innerHTML += "Adresse n� " + infos[infosIdx].id + " non sp�cifi�e<br/>";
						infosIdx++;
						setTimeout(geoCodeAll, 0);
					}
					else
						geocoder.getLocations(infos[infosIdx].adresse, getLatLong);
				}
				else
				{
					var geoValues = document.getElementById("GeoValues");

					geoValues.value = "";

					for (var i = 0; i < infos.length; i++)
					{
						if (infos[i].updated == 1)
						{
							var address = infos[i].id + "/" + infos[i].lat + "/" + infos[i].lng;

							geoValues.value += (geoValues.value != "" ? ";" : "") + address;
						}
					}

					document.getElementById("TdStart").style.display = "";
					document.getElementById("TdCancel").style.display = "none";

					onSubmit("update");
				}
			}
			else
			{
				document.getElementById("statut").innerHTML = "Op�ration annul�e par l'utilisateur";
				document.getElementById("TdStart").style.display = "";
				document.getElementById("TdCancel").style.display = "none";
			}
		}

		function GGeoStatusCode(code)
		{
			switch (code)
			{
			case 200 :	return "No errors occurred; the address was successfully parsed and its geocode has been returned.";
			case 400 :	return "A directions request could not be successfully parsed. For example, the request may have been rejected if it contained more than the maximum number of waypoints allowed.";
			case 500 :	return "A geocoding, directions or maximum zoom level request could not be successfully processed, yet the exact reason for the failure is not known.";
			case 601 :	return "The HTTP q parameter was either missing or had no value. For geocoding requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.";
			case 602 :	return "No corresponding geographic location could be found for the specified address. This may be due to the fact that the address is relatively new, or it may be incorrect.";
			case 603 :	return "The geocode for the given address or the route for the given directions query cannot be returned due to legal or contractual reasons.";
			case 604 :	return "The GDirections object could not compute directions between the points mentioned in the query. This is usually because there is no route available between the two points, or because we do not have data for routing in that region.";
			case 610 :	return "The given key is either invalid or does not match the domain for which it was given.";
			case 620 :	return "The given key has gone over the requests limit in the 24 hour period or has submitted too many requests in too short a period of time. If you're sending multiple requests in parallel or in a tight loop, use a timer or pause in your code to make sure you don't send the requests too quickly.";
			}
		} 
	</script>
	<?php } ?>
	
	<script language="javascript" type="text/javascript">
		function onChkClick(type)
		{
		}

		function onLoad()
		{
			var all = <?php echo $countAll; ?>;
			var missing = <?php echo $countMissing; ?>;

			var chkAll = document.getElementById("chkAll");
			var chkMissing = document.getElementById("chkMissing");
			
			if (all < 1) chkAll.disabled = "disabled"; 
			if (missing < 1) chkMissing.disabled = "disabled";

			<?php if (isset($addressesList)) { ?>
				document.getElementById("TdStart").style.display = "none";
				document.getElementById("TdCancel").style.display = "";
				initialize(); 
			<?php } ?>
		}

		function onSubmit(save)
		{
			var submitAction = document.getElementById("SubmitAction");

			if (save)
			{
				submitAction.value = 'update';
				document.getElementById("HidErrors").value = document.getElementById("errors").innerHTML;								
			}
			else
			{
				submitAction.value = 'start';
				document.getElementById("HidErrors").value = "";
			}

			submitForm('GoogleMapsAdministration');
		}

		function onCancel() { __continue = false; }

		window.onload = onLoad;
	</script>
</head>

<body>
	<form id="GoogleMapsAdministration" action="<?php echo $this->BuildURL($_REQUEST['module']); ?>" method="post">
		
		<input type="hidden" id="SubmitAction" name="SubmitAction" value="none"></input>
		<input type="hidden" id="GeoValues" name="GeoValues"></input>
		<input type="hidden" id="HidErrors" name="HidErrors"></input>
	
		<table>
			<tr>
				<td>Nombre total d'adresses</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $countAll; ?></td>
			</tr>
			<tr>
				<td>Nombre total de latitude ou longitude manquantes</td>
				<td>&nbsp;:&nbsp;</td>
				<td><?php echo $countMissing; ?></td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
				<td>Traiter toutes les adresses</td>
				<td>&nbsp;:&nbsp;</td>
				<td><input type="checkbox" name="chkAll" id="chkAll" onclick="javascript:onChkClick('all');" <?php if (isset($_POST['chkAll'])) echo 'checked'; ?>></input></td>
			</tr>
			<tr>
				<td>Traiter les latitudes ou longitudes manquantes</td>
				<td>&nbsp;:&nbsp;</td>
				<td><input type="checkbox" name="chkMissing" id="chkMissing" onclick="javascript:onChkClick('missing');" <?php if (isset($_POST['chkMissing'])) echo 'checked'; ?>></input></td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
				<td colspan="3">
					<div class="boxBtn" style="text-align:left;">
						<table class="buttonTable" border="0" align="left">
							<tr>
								<td id="TdStart" onclick="javascript:onSubmit(false);">D�marrer</td>
								<td id="TdCancel" onclick="javascript:onCancel();" style="display:none;">Stopper</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
				<td colspan="3"><div id="statut" style="padding-bottom:5px;font-style:italic;font-weight:bold;"></div></td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
		</table>
		<div id="errors" style="font-style:italic;"><?php if (isset($_POST['HidErrors'])) echo $_POST['HidErrors']; ?></div>
	</form>
</body>

<?php
	}
} 
?>