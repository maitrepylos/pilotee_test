<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class HistoriqueDetailView extends BaseView
{
	public function Render($historique)
	{
?>
		<script>
			function HistoriqueViewClass() {}

			HistoriqueViewClass.prototype.Close = function()
			{
				parent.Popup.hide("HistoriqueDetail");
			};
			
			window.histo = new HistoriqueViewClass();
		</script>	
		<div class="boxGen" style="border:0px;">
			<div class="boxPan" style="padding: 5px;">
				<table  width = "98%" cellspacing="0"  cellpadding="2" border="0" style="margin-bottom: 4px; margin:10px;" class="result">
					<tr>					
					 <td class="resultHeader" width="15%">Date</td>
					 <td class="resultHeader" width="25%">Utilisateur</td>
					 <td class="resultHeader" width="25%">Type d'utilisateur</td>
					 <td class="resultHeader" width="*">Opération</td>
					 </tr>
					 <tbody>
					 <?php 
						for($i=0; $i<$historique->count(); $i++)
						{
							$user = $historique->items($i)->GetUtilisateur();
							echo '<tr>';
							echo '<td>'.$historique->items($i)->GetDateHistorique(true).'</td>';
							echo '<td>'.$user->GetNom().' '.$user->GetPrenom().'</td>';
							echo '<td>'.$user->getTypeUtilisateurLabel().'</td>';
							echo '<td>'.$historique->items($i)->GetTypeEncodageLabel().'</td>';
							
							
							echo '</tr>';
						}	
					 ?>					   		
					 </tbody>
				</table>
			</div>
			<div class="boxBtn">
				<table class="buttonTable">
					<tr>
						<td class="separator"></td>
						<td onclick="javascript:window.histo.Close();">Fermer</td>
					</tr>
				</table>
			</div>
		</div>
<?php 		
	}
}
?>
