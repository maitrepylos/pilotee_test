<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class HomeView extends BaseView
{
	public function render()
	{
?>
		<br/><div style="font-family: Verdana; font-size: 13px; text-align: justify; font-style: italic; padding-left: 20px; padding-right: 60px;">
			<p>Bonjour,</p>

<p>PilotEE est une application de suivi concernant le Programme Wallon Esprit d'Entreprendre qui tient compte des proc�dures organisationnelles mises en place entre l'agence et ses Agents, ainsi que les proc�dures conventionnelles mises en place entre l'agence et les op�rateurs de l'EE.</p>
<br/>
PilotEE, c'est :
<ul>					
<li>un outil de travail quotidien pour les Agents de sensibilisation : enregistrement de leur travail journalier (commentaires sur les appels � projets innovants, personnes de contacts, �tablissements visit�s, etc.). En tant qu'op�rateurs, vous b�n�ficier d'une visibilit� totale sur ce qui se passe en mati�re d'Esprit d'Entreprendre dans tous les �tablissements de Wallonie ;</li><br/>
<li>un outil d'encodage des indicateurs, �tablissement par �tablissement, le nombre d'�coliers/�tudiants concern�s par les actions, r�partissant ce nombre dans les diff�rentes cat�gories</li><br/>
<li>un outil de reporting pour l'ensemble des acteurs associ�s � cet outil : les op�rateurs, les Agents de sensibilisation, les responsables du Programme Esprit d'Entreprendre. Il permet une vision globale et d�taill�e des r�alisations sur l'ensemble du territoire.</li><br/>
</ul>
<br/>
Pratiquement, les utilisateurs disposent de diff�rents onglets sur le haut de la page ainsi que d'outils situ�s � gauche de l'�cran.
Un tutoriel (<a href="tutoriel/TUTORIEL_PILOTEE_OPE.pdf">cliquez-ici</a>) est � disposition pour toute aide dans la navigation de l'application.
<br/><br/>
Bon travail,
<br/><br/>
L'Equipe AT et I&E


			
		
		
		</div>		
		
<?php 
	}
}
?>