<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class InscriptionView extends BaseView
{
	public function formulaireOubli($email,$errors)
	{
		?>
		
	<div class="box">	
	<fieldset><legend><i>DEMANDE DE RAPPEL</i></legend>
	<?php echo $this->Errors($errors); ?>
		<form action="<?php echo $this->BuildURL('forgot') ?>" method="post">
			<div class="part">
				<p>Indiquez l'adresse de courriel associ�e � votre compte ci-dessous, un email de rappel vous sera alors envoy� avec votre identifiant et votre nouveau mot de passe</p>
				<table width="100%">
					<tr>
						<td class="right short">Login (courriel) :</td>
						<td><input type="text" style="width:300px;" name="email" id="email" value="<?php echo ToHTML($email) ?>" /></td>
						<td><input type="submit" name="commit" id="commit" value="Demander le rappel" class="notauto" /> <input type="submit" name="back" id="back" value="Retour" class="notauto" /></td>
					</tr>
				</table>
			</div>
		</form>
	</fieldset>
	</div>	
	
		<?php
	}
	
	protected function loginBox($email, $password,$errors,$notices)
	{
		?>
	
	<center>
		<div class="box">
		
		<fieldset><legend><i>IDENTIFICATION</i></legend>
		
		<?php if(!empty($errors)){echo $this->Errors($errors);}?>
		<?php if(count($notices)>1){echo $this->Notices($notices);} ?>		
		
			<form action="<?php echo $this->BuildURL('login') ?>" method="post">
				<div >
					
					<table>
						<tr>
							<td class="right">Login (courriel) : </td>
							<td class="left"><input type="text" name="email" id="email" value="<?php echo ToHTML($email) ?>" /></td>
						</tr>
						<tr>
							<td class="right">Mot de passe :</td>
							<td class="left"><input type="password" name="password" id="password" value="<?php echo ToHTML($password) ?>" /></td>
						</tr>
						<!--<tr>
							<td class="right">&nbsp;</td>
							 <td class="left"><a href="<?php echo $this->BuildURL('forgot') ?>">J'ai oubli� mon mot de passe</a></td>
						</tr>
						-->
						<tr>
							<td>&nbsp;</td>
							<td>
								<table>
									<tr>
										<td>
											<a href="<?php echo $this->BuildURL('forgot') ?>">Mot de passe oubli� ?</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="right">&nbsp;</td>
							<td class="left"><input type="submit" name="login_commit" id="login_commit" value="S'identifier" /></td>
						</tr>
					</table>
				</div>
			</form>
		   </fieldset>
		</div>
	</center>
	
		 <?php
	}
	
	public function registerBox($email,$nom,$prenom,$mdp, $mdp_conform,$errors)
	{
		?>
		
		<div class="box">
		<fieldset><legend><i>S'ENREGISTRER</i> (r�serv� aux op�rateurs agr��s)</legend>
		<br />
		<?php echo $this->Errors($errors);?>
		<center>
			<form action="<?php echo $this->BuildURL('register') ?>" method="post">
				<div>					
					<table>
						<tr>
							<td colspan="2" class="info"></td>
						</tr>
						<tr>
							<td class="right"><label for="email">Courriel :</label></td>
							<td class="left"><input type="text" name="email" id="email" value="<?php echo ToHTML($email) ?>" /></td>
						</tr>
						<tr>
							<td class="right"><label for="email">Nom :</label></td>
							<td class="left"><input type="text" name="nom" id="nom" value="<?php echo ToHTML($nom) ?>" /></td>
						</tr>
						<tr>
							<td class="right"><label for="email">Pr�nom :</label></td>
							<td class="left"><input type="text" name="prenom" id="prenom" value="<?php echo ToHTML($prenom) ?>" /></td>
						</tr>
						<tr>
							<td class="right"><label for="email">Mot de passe :</label></td>
							<td class="left"><input type="password" name="mdp" id="mdp" value="<?php echo ToHTML($mdp) ?>" /></td>
						</tr>
						<tr>
							<td class="right"><label for="email">Confirmer le mot de passe :</label></td>
							<td class="left"><input type="password" name="confirmn_mdp" id="confirmn_mdp" value="<?php echo ToHTML($mdp_conform) ?>" /></td>
						</tr>						
												
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td class="right">&nbsp;</td>
							<td class="left"><input type="submit" name="register_commit" id="register_commit" value="S'enregistrer" class="notauto" /><input type="submit" name="back" id="back" value="Retour" class="notauto" /></td>
						</tr>
					</table>
				</div>
			</form>
			</center>
			</fieldset>
		</div>
		
		<?php
	}
	
	public function loginScreen($login, $password, $errors, $notices)
	{
		echo '		
		<div class="login">';
		$this->loginBox($login, $password,$errors,$notices);
		echo '</div>';
	}
}

?>