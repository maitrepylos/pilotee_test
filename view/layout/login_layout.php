<?php
	header('Content-Type: text/html; charset=ISO-8859-1');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo LANGUAGE ?>" lang="<?php echo LANGUAGE ?>">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<link href="<?php echo STYLEPATH ?>style.css" rel="stylesheet" type="text/css" />		
		<title><?php echo ToHTML($title) ?></title>
		<script type="text/javascript" src="scripts/fonctions.js"></script>
	</head>
	<body>
	  <div class="container">
		<div class="content">	
		<!-- HEADER START --> 
			<div class="header">
			  <table width="100%" cellspacing="0" cellpadding="0" border="0">
			  	<tbody>	
			  		<tr>		
						<td>
							<div class="header_logo"></div>
						</td>
					</tr>
				</tbody>
			  </table>
			  <br/>
			  <table width="100%">
			  	<tr>
			  		<td class="ligneHeader">
			  		</td>
			  	</tr>
			  </table>
			</div>
		<!-- HEADER END -->
			
		<!-- CONTENT START -->	
			<div class="content">
				<?php echo $content; ?>
			</div>
		<!-- CONTENT END -->
			<!-- FOOTER START -->			
			<div class="footer">
				<hr class="ligne" />
				<center>Agence de Stimulation Economique - Rue du Vertbois, 13b - 4000 Li�ge
						<br/>T�l. : 04 220 51 00 - Fax : 04 220 51 19 - <a href="mailto:entreprendre@as-e.be">Courriel</a>  </center>			
			</div>
			<!-- FOOTER END -->
		</div>	
	  </div>
	</body>
</html>
