<?php
header('Content-Type: text/html; charset=ISO-8859-1');

function menu_250px($id)
{
	echo 'document.getElementById("spn' . $id . '").style.backgroundPosition = "100% -250px";';
	echo 'document.getElementById("spn' . $id . '").style.color = "#FFFFFF";';
	echo 'document.getElementById("a' . $id . '").style.backgroundPosition = "0% -250px";';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo LANGUAGE ?>" lang="<?php echo LANGUAGE ?>" <?php echo 'style="overflow:auto;"'; ?>>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo STYLEPATH ?>style.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo STYLEPATH ?>calendar-system.css" />

		<!--[if gte IE 7]>
		<link rel="stylesheet" type="text/css" href="<?php echo STYLEPATH ?>style.ie7.css" />
		<![endif]-->
		
		<title><?php echo ToHTML($title) ?></title>
		<script src="scripts/fonctions.js" type="text/javascript"></script>
		<script src="scripts/PopupWindow.js" type="text/javascript"></script>
		<script src="scripts/calendar.js" type="text/javascript"></script>
		<script src="scripts/calendar-fr.js" type="text/javascript"></script>
		<script src="scripts/calendar-setup.js" type="text/javascript"></script>
		<script src="scripts/CheckTools.js" type="text/javascript"></script>
		<script src="scripts/jx.js" type="text/javascript"></script>
		<script src="scripts/jquery-1.8.3.min.js" type="text/javascript"></script>
		<script src="scripts/session.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="header" style="text-align: left; border-bottom: 3px solid #EAC3C3;">
			<div class="header_logo login" style="text-align: right;"><?php echo $login; ?></div>
			<div>
				<!-- D�but menu horizontal -->
				<?php echo $menu ?>
				<!-- Fin menu horizontal -->

				<script language="javascript" type="text/javascript">
					function onMouseOver(id)
					{
						if (id.toLowerCase() != 'report')
						{
							document.getElementById("spn" + id).style.backgroundPosition = "100% -250px";
							document.getElementById("spn" + id).style.color = "#FFFFFF";
							document.getElementById("a" + id).style.backgroundPosition = "0% -250px";
							
						}
						else
						{
							document.getElementById("spn" + id).style.backgroundPosition = "100% -500px";
							document.getElementById("spn" + id).style.color = "#FFFFFF";
							document.getElementById("a" + id).style.backgroundPosition = "0% -500px";
						}
					}

					function switchMouseOut(id)
					{
						document.getElementById("spn" + id).style.backgroundPosition = "100% 0%";
						document.getElementById("spn" + id).style.color = "#737373";
						document.getElementById("a" + id).style.backgroundPosition = "0% 0%";
					}

					function onMouseOut(id)
					{
						var module = gup("module");

						switch (module)
						{
						
							case 'accueil' :
								if (id.toLowerCase() != 'accueil')
								{
									switchMouseOut(id);
								}
								break;
							
							case 'etablissement' :
							case 'etablissementDetail' :
							case 'ficheSuiviDetail' :
							case 'materielPedagDetail' :
								if (id.toLowerCase() != 'etablissement')
								{
									switchMouseOut(id);
								}
								break;									
							case 'contact' :
							case 'contactDetail' :
								if (id.toLowerCase() != 'contact')
								{
									switchMouseOut(id);
								}
								break;
							case 'action' :
							case 'actionDetail' :
								if (id.toLowerCase() != 'action')
								{
									switchMouseOut(id);
								}
								break;
							case 'formations' :
							case 'formationDetail' :
								if (id.toLowerCase() != 'formations')
								{
									switchMouseOut(id);
								}
								break;
							case 'bourse' :
							case 'bourseDetail' :
							case 'commenterBourse' :
								if (id.toLowerCase() != 'bourse')
								{
									switchMouseOut(id);
								}
								break;									
							case 'report' :
								if (id.toLowerCase() != 'report')
								{
									switchMouseOut(id);
								}
								break;
							default :
								switchMouseOut(id);
						}
					}
					
					<?php
						if (isset($_REQUEST['module']) && strlen(trim($_REQUEST['module'])) > 0)
						{
							$module = trim($_REQUEST['module']);
							
							$modulesArray = array();
							
							$user = Session::GetInstance()->getCurrentUser();
							
							array_push($modulesArray, 'Accueil');
							array_push($modulesArray, 'Etablissement');
							if(!$user->isOperateur())
							{
								array_push($modulesArray, 'Contact');
							}
							array_push($modulesArray, 'Action');
							array_push($modulesArray, 'Formations');
							if(!$user->isOperateur())
							{
								array_push($modulesArray, 'Bourse');
							}
							array_push($modulesArray, 'Report');

							for ($i = 0; $i < count($modulesArray); $i++)
							{
								echo 'document.getElementById("spn' . $modulesArray[$i] . '").style.backgroundPosition = "100% 0%";';
								echo 'document.getElementById("spn' . $modulesArray[$i] . '").style.color = "#737373";';
								echo 'document.getElementById("a' . $modulesArray[$i] . '").style.backgroundPosition = "0% 0%";';
							}
							
							switch ($module)
							{
								case 'accueil' :
									menu_250px('Accueil');
									break;
								case 'etablissement' :
								case 'etablissementDetail' :
								case 'ficheSuiviDetail' :
								case 'materielPedagDetail' :
									menu_250px('Etablissement');
									break;									
								case 'contact' :
								case 'contactDetail' :
									menu_250px('Contact');
									
									break;
								case 'action' :
								case 'actionDetail' :
									menu_250px('Action');
									break;
								case 'formations' :
								case 'formationDetail' :
									menu_250px('Formations');
									break;
								case 'bourse' :
								case 'bourseDetail' :
								case 'commenterBourse' :
									menu_250px('Bourse');
									break;									
								case 'report' :
									echo 'document.getElementById("spnReport").style.backgroundPosition = "100% -500px";';
									echo 'document.getElementById("spnReport").style.color = "#FFFFFF";';
									echo 'document.getElementById("aReport").style.backgroundPosition = "0% -500px";';
									break;
							}
						}
					?>
				</script>

			</div>
		</div>
		<div class="container" style="text-align: left;">
			<table cellpadding="0" cellspacing="0" border="0" style="border-collapse: separate; width: 100%;">
				<tr>
					<td style="width: 10px; vertical-align: top;">
						<img id="open_close" src="./styles/img/hide.gif" class="expand" onclick="hideShortcutPanel('shortcut')" title="Fermer le panneau de raccourcis" />
					</td>
					<td style="width: 175px; border-right: solid #EAC3C3 1px;" id="shortcut" class="shortcut_head" onclick="hideShortcutPanel('shortcut')">Raccourcis</td>
					<td class="title" style="padding-left: 5px;"><?php echo ToHTML($title); ?></td>
				</tr>
				<tr>
					<td style="width: 10px; vertical-align: top;">&nbsp;</td>
					<td style="width: 125px; border-right: solid #EAC3C3 1px; vertical-align: top;" id="shortcut_items">
						<div style="height: 1px; font-size: 1px; width: 95%; border: solid #EAC3C3 1px; background-color: #EAC3C3;">&nbsp;</div>
						<?php echo $shortcut; ?>
					</td>
					<td style="vertical-align: top; padding-left: 5px;" align="left">
						<div style="height: 1px; font-size: 1px; width: 95%; border: solid #EAC3C3 1px; background-color: #EAC3C3;">&nbsp;</div>
						<?php echo $content; ?>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
