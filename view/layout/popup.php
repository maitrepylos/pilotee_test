<?php
	header('Content-Type: text/html; charset=ISO-8859-1');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo LANGUAGE ?>" lang="<?php echo LANGUAGE ?>" <?php echo 'style="height:100%;"' ?>>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<link rel="stylesheet" type="text/css" href="<?php echo STYLEPATH ?>style.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo STYLEPATH ?>calendar-system.css" />
		
		
<!--[if gte IE 7]>
<link rel="stylesheet" type="text/css" href="<?php echo STYLEPATH ?>style.ie7.css" />
<![endif]-->

		<title></title>
		<script src="scripts/PopupWindow.js" type="text/javascript"></script>
		<script src="scripts/calendar.js" type="text/javascript"></script>
		<script src="scripts/calendar-fr.js" type="text/javascript"></script>
		<script src="scripts/calendar-setup.js" type="text/javascript"></script>
		<script src="scripts/fonctions.js" type="text/javascript"></script>
		<script src="scripts/CheckTools.js" type="text/javascript"></script>
		<script src="scripts/jx.js" type="text/javascript"></script>
	</head>
	<body style="height:100%;">
		<table style="width:100%;height:100%;">
			<tr>
				<td height="95%" valign="top">
					<div class="container">
						<div class="sub_container" style="padding:0px;padding-top:2px;">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr valign="top">
								<td style="padding-left:3px;"><?php echo $content; ?></td>
							</tr>
							</table>
						</div>
					</div>				
				</td>
			</tr>
		</table>
	</body>
</html>
