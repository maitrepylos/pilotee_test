<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
class ListingActionsView extends BaseView
{
	private $user = null;

	public function Render()
	{
		$this->user = Session::GetInstance()->getCurrentUser();
?>
	<script>
	function checkTypeAction(action, checkbox)
	{
		var tr_categorie = document.getElementById('tr_categorie');
		var tr_operateur = document.getElementById('tr_operateur'); 

		if(action=='lab')
		{
			if(checkbox.checked)
			{
				tr_operateur.style.display = '';
			}
			else
			{
				tr_operateur.style.display = 'none';
				
			}
		}
		else if(action=='ent')
		{
			if(checkbox.checked)
			{
				tr_categorie.style.display='';	
			}
			else
			{
				tr_categorie.style.display='none';
			}
		}
	}

	function updateActionLabelise(anneeScolaire)
	{
		jx.load('./index.php?module=ajax&actionLabelise=ok&val=' + anneeScolaire, function(data)
			{
				var div = document.getElementById('operateurs_liste');
				div.innerHTML = '';

				var operateursCpt = 0;

				var tabElt = data.split("/#-#/");
				if (data != '')
				{
					for (var i = 0; i < tabElt.length; i++)
					{
						var tabOp = tabElt[i].split('||');
						var id = tabOp[0];
						var value = tabOp[1];
						div.innerHTML += '<input type="checkbox" name="operateur_' + i + '" value="' + id + '" />&nbsp;' + value + '&nbsp;&nbsp;<br />';
						operateursCpt++;
					}
				}
				div.innerHTML += '<input type="hidden" value="' + operateursCpt + '" name="nbOperateur" />';
			}, 'text'
		);
	}
	</script>
	<div class="boxGen">
		<form id="ReportAction" action="<?php echo $this->BuildURL($_REQUEST['module']).'&type=listing_action' ?>" method="post">
		
		<input type="hidden" id="SubmitAction" name="SubmitAction"
			value="none"></input>
		
			<div class="boxPan" style="padding: 5px;">
				<table cellpadding="0" cellspacing="0" border="0" width="85%">
				<colgroup>
					<col width="175"></col>
					<col width="10"></col>
					<col width="150"></col>
					<col width="45"></col>
					<col width="175"></col>
					<col width="10"></col>
					<col width="*"></col>
				</colgroup>
				<tr>
					<td height="25">Intitul�</td>
					<td>:</td>
					<td>
						<input size="40" maxlength="100" class="notauto" type="text" name="intitule" id="intitule" value="<?php if(isset($_POST["intitule"])) echo ToHTML($_POST["intitule"]) ?>" />
					</td>
					<td></td>
					<td height="25" colspan="6">
						<input type="checkbox"  <?php if (isset	($_POST["actionLab"])) {echo 'checked="checked"';}?>  name="actionLab" id="actionLab" class="notauto" onclick="checkTypeAction('actionLab',this)" value="1" > Actions labellis�es&nbsp;<br />
						<input type="checkbox"  <?php if (isset	($_POST["actionNonLab"])) {echo 'checked="checked"';}?>  name="actionNonLab" id="actionNonLab" class="notauto" onclick="checkTypeAction('actionNonLab',this)" value="1" > Actions Non labellis�es&nbsp;<br />
						<input type="checkbox" <?php if (isset($_POST["formations"])) echo 'checked="checked"';?> name="formations" id="formations" class="notauto" onclick="checkTypeAction('formations',this)" value="2"> Formations&nbsp;<br />
					 	<input type="checkbox" <?php if (isset($_POST["stac"])) echo 'checked="checked"';?> name="stac" id="stac" class="notauto" onclick="checkTypeAction('stac',this)" value="3"> (STAC) Acculturation&nbsp;<br />
					 	<input type="checkbox" <?php if (isset($_POST["ateliers"])) echo 'checked="checked"';?> name="ateliers" id="ateliers" class="notauto" onclick="checkTypeAction('ateliers',this)" value="4"> Ateliers&nbsp;<br />
					 	<input type="checkbox" <?php if (isset($_POST["projEntr"])) echo 'checked="checked"';?> name="projEntr" id="projEntr" class="notauto" onclick="checkTypeAction('projEntr',this)" value="5"> Projets Entrepreneriaux&nbsp;<br />
					 	
					 	
					</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td height="25">Ann�e scolaire</td>
					<td>:</td>					
					<td>
						<!--  <select id="anneesScolaire" name="anneesScolaire" style="width: 230px;" onchange="updateActionLabelise(this.value)"> -->
						<?php
						// echo createSelectOptions(Dictionnaries::getAnneeList(), $_POST['anneesScolaire']);
						?>
						<!--  </select>-->
					<select id="anneesScolaire" name="anneesScolaire" style="width: 230px;">
					<?php
						echo '<option value="-1"></option>';
						echo createSelectOptions(Dictionnaries::getAnneeList(), $_POST['anneesScolaire']);
					?>
					</select>
						
					</td>
					<td></td>
					<td></td>
				</tr>			
				<tr>
					<td>Code postal</td>
					<td>:</td>
					<td>
						<input type="text" maxlength="4" class ="notauto" size="10" name="code_postal_1" value="<?php if (isset($_POST["code_postal_1"])) echo ToHTML($_POST["code_postal_1"]) ?>"></input>
						&nbsp;�&nbsp;
						<input type="text" maxlength="4" class ="notauto" size="10" name="code_postal_2" value="<?php if (isset($_POST["code_postal_2"])) echo ToHTML($_POST["code_postal_2"]) ?>"></input>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td height="85" valign="top">Arrondissements</td>
					<td valign="top">:</td>
					<td  colspan="2">
						<div id="divArr" style="width:225px;height:75px;overflow:auto;background-color:white;border:1px solid #EAC3C3;padding:2px;">
						<?php 
							$arrondissements = Dictionnaries::getArrondissementList();
							for ($i = 0; $i < $arrondissements->count(); $i++)
							{
								$check='';
								if(isset($_POST['chk_arr_'.$arrondissements->items($i)->getId()])) $check='checked="checked"';
								echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="chk_arr_' . $arrondissements->items($i)->getId() . '"></input>&nbsp;' . $arrondissements->items($i)->getLabel() . '</div>';
							}
						?>
					</div>
		
					</td>
					<td height="85" valign="top"></td>
					<td valign="top"></td>
					<td  colspan="5"></td>
				 </tr>
				 <?php if (($this->user->isAdmin()) || ($this->user->isCoordinateur()) || ($this->user->isAgent())  || ($this->user->isAgentCoordinateur())) { ?>
				<tr>
					<td>Agent</td>
					<td>:</td>
					<td><select name='agent' style="width: 175px;">
							<?php
							echo '<option value="-1">Tous</option>';

							if (isset($_POST['agent']))
							{
								if ($_POST['agent'] == -2)
								{
									echo '<option value="-2" selected="selected">Pas attribu�</option>';
									echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT));
									
								}
								else
								{
									echo '<option value="-2">Pas attribu�</option>';
									echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT), $_POST['agent']);
									
								}
							}
							else
							{
								echo '<option value="-2">Pas attribu�</option>';
								if($this->user->isAgent()) {
									$_POST['agent'] = $this->user->getUtilisateurId();
									
								}

								echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT, null), $_POST['agent']);
							}
							?>
					</select>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 1px; height: 2px;"></td>
				</tr>
				<?php } ?>
				</table>
				<!-- FFI <input type="hidden" name="report_action"></input>-->
			</div>
			<div class="boxBtn" style="padding:5px;">
				<table class="buttonTable">
					<tr>
						<td onclick="document.getElementById('SubmitAction').value='search';submitForm('ReportAction')">Rechercher</td>						
						<td class="separator"></td>
						<td onclick="clearForm('ReportAction')">Effacer</td>
					</tr>
				</table>
			</div>			
		</form>
	</div>

<?php
	}

	public function noResult()
	{
		?>
<div id="DivNoResult" class="boxGen" style="border: 0px;">
	<br />
	<div class="boxPan" style="padding: 5px; border: 0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			class="result">
			<tr>
				<td
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 3px;">
					Il n'y a aucun r�sultat pour votre requ�te ...</td>
			</tr>
		</table>
	</div>
</div>
<?php
	}
	
	public function renderElt($action, $etablissement, $actionContact) {
						
		$etsNom = null;
		$etsCp = null;
		$etsVille = null;
		$etsRue = null;
		//$userNom = null;
		//$userPrenom = null;
		$userTel = null;
		$userEmail = null;
		$userLabel= null;
		$contactNom = null;
		$contactPrenom = null;
		$contactsEtablissement = null;
		$personneRessource = null;
		$dateAction = null;
		
		$dateAction = $action->GetDateActionString(false);
		
				
		if ($etablissement != null) {
			$estNom = $etablissement->GetNom();
			
			$contactsEtablissement = $etablissement->GetContacts();
			
			if ($etablissement->GetUtilisateur() != null)
			{
				//$userNom = $etablissement->GetUtilisateur()->GetNom();
				//$userPrenom = $etablissement->GetUtilisateur()->GetPrenom();
				//$userTel = $etablissement->GetUtilisateur()->GetTel();
				//$userEmail = $etablissement->GetUtilisateur()->GetEmail();
				$userLabel = $etablissement->GetUtilisateur()->GetLabel();
			}
			$etsCp = $etablissement->GetAdresse()->GetCodePostal();
			$etsVille = $etablissement->GetAdresse()->GetVille();
			$etsRue = $etablissement->GetAdresse()->GetRue();
		}
		
		if ($actionContact != null) {
			$contact = $actionContact->GetContact();
			
			if ($contact) {
				$contactNom = $contact->GetNom();
				$contactPrenom = $contact->GetPreNom();
				$userTel = $contact->GetAdresse()->GetTel1();
				$userEmail = $contact->GetAdresse()->GetEmail1();
				
				if ($contactsEtablissement != null && $contactsEtablissement->Count() > 0) {
					
					for ($i=0; $i < $contactsEtablissement->Count(); $i++ ) {
						$contactEtablissement = $contactsEtablissement->items($i);
						$tempEtablissementContact = $contactEtablissement->getContact();
						if ($tempEtablissementContact && $tempEtablissementContact->GetContactId() ==  $contact->GetContactId()) {
							$tempParticipation = $contactEtablissement->getParticipation();
							if ($tempParticipation) $personneRessource = $tempParticipation->getLabel();
							break;
						}
						
					}
					
				}			
			}
		}
	
		
		echo '<tr>';
		echo sprintf('<td class="resultLine">%s</td>', $action->GetNom());						
		echo sprintf('<td class="resultLine">%s</td>', $action->GetTypeActionId());
		// $current->GetTypeActionLabel();
		echo sprintf('<td class="resultLine">%s</td>', isset($estNom) ? $estNom : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($etsRue) ? $etsRue : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($etsCp) ? $etsCp->GetCodepostal() : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($etsVille) ? $etsVille : '&nbsp;');

		echo sprintf('<td class="resultLine">%s</td>', isset($contactNom) ? $contactNom : '&nbsp;'); // idem
		echo sprintf('<td class="resultLine">%s</td>', isset($contactPrenom) ? $contactPrenom : '&nbsp;'); // idem
	
		echo sprintf('<td class="resultLine">%s</td>', isset($userEmail) ? $userEmail : '&nbsp;'); // idem
		echo sprintf('<td class="resultLine">%s</td>', isset($userTel) ? $userTel : '&nbsp;') ; //? contact ou ets
		
		
		echo sprintf('<td class="resultLine">%s</td>', isset($dateAction) ? $dateAction : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($userLabel) ? $userLabel : '&nbsp;'); // idem
		// echo sprintf('<td class="resultLine">%s</td>', isset($personneRessource) && $personneRessource == 1 ? " OUI " : ' - '); // idem

		echo '</tr>';

		
	}

	public function renderList($ets)
	{
		?>
<script language="javascript" type="text/javascript">
	function ListingActionViewClass() 
	{
		this.current = 'Results';
		this.tabs = new Array('Results', 'Map');
	};

	ListingActionViewClass.prototype.onMouseOver = function(liID)
	{
		document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
		document.getElementById("spn" + liID).style.color = "#FFFFFF";
		document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
	};

	ListingActionViewClass.prototype.onMouseOut = function(liID)
	{
		if (this.current != liID)
		{
			document.getElementById("spn" + liID).style.backgroundPosition = "100% 0%";
			document.getElementById("spn" + liID).style.color = "#737373";
			document.getElementById("a" + liID).style.backgroundPosition = "0% 0%";
		}
	};

	ListingActionViewClass.prototype.onLIClick = function(liID)
	{
		this.current = liID;
		
		for (var i = 0; i < this.tabs.length; i++)
		{
			if (document.getElementById("spn" + this.tabs[i]))
			{
				document.getElementById("spn" + this.tabs[i]).style.backgroundPosition = "100% 0%";
				document.getElementById("spn" + this.tabs[i]) .style.color = "#737373";
				document.getElementById("a" + this.tabs[i]).style.backgroundPosition = "0% 0%";
			}
		}

		document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
		document.getElementById("spn" + liID).style.color = "#FFFFFF";
		document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
		
		var results = document.getElementById('Results');
		var map = document.getElementById('Map');
		
		if (liID == 'Results')
		{
			
			if (results) results.style.display = '';
			if (map) map.style.display = 'none';

			var frm = document.getElementById("frmGoogleMaps");
			frm.src = 'about:blank';
		}
		else if (liID == 'Map')
		{
			if (results) results.style.display = 'none';
			if (map) map.style.display = '';

			this.renderMap();
		}
	};

	ListingActionViewClass.prototype.renderMap = function()
	{
		var frm = document.getElementById("frmGoogleMaps");
		frm.src = "<?php echo SCRIPTPATH . 'index.php?module=popup_google_maps&anneeId=' . $_POST['anneesScolaire'] ?>";
		frm.src += "&ids=" + document.getElementById("EtsIdList").value;
	};
	
	window.revc = new ListingActionViewClass();
	
	function getexcel()
	{
		window.location.replace("<?php echo $this->BuildURL('exportXLS').'&type=listing_action';?>");
	}
	
	</script>

<div id="DivResult" class="boxGen" style="border: 0px;">
	<div class="boxPan" style="padding: 5px; border: 0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			class="result">
			<tr>
				<td colspan="9" class="title"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 3px;">Listing
					sur les actions &nbsp; <img class="expand"
					src="./styles/img/csv.gif" title="T�l�charger la version Excel"
					onclick="getexcel();" />
				</td>
			</tr>
		</table>

		<div class="header" style="border-bottom: 3px solid #EAC3C3;">
			<ul>
				<li OnMouseOver="javascript:window.revc.onMouseOver('Results');"
					OnMouseOut="javascript:window.revc.onMouseOut('Results');"><a
					id="aResults" href="javascript:void(0)"
					onclick="javascript:window.revc.onLIClick('Results');"><span
						id="spnResults" class="expand">Liste de r�sultats</span> </a></li>
			</ul>
		</div>

		<div style="width: 100%;">
			<div id="Results" style="width: 100%;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="result">
					<?php ob_start(); ?>
					<tr>
						<td class="resultHeader">Intitul�</td>
						<td class="resultHeader">Type</td>
						<td class="resultHeader">Etablissement</td>
						<td class="resultHeader">Adresse</td>
						<td class="resultHeader">CP</td>
						<td class="resultHeader">Ville</td>
						<td class="resultHeader">Nom</td>
						<td class="resultHeader">Pr�nom</td>
						<td class="resultHeader">Email</td>
						<td class="resultHeader">Tel</td>						
						<td class="resultHeader">Date</td>
						<td class="resultHeader">Agent</td>
						
					</tr>
					<?php
					$statutActiviteOK = 0;
					$statutActivitePasse = 0;
					$statutActiviteJamais = 0;
					$statutActiviteGlobal = 0;
					$statutVisiteOK = 0;
					$statutVisitePasse = 0;
					$statutVisiteJamais = 0;
					$statutVisiteGlobal = 0;
					
					$etsIds = null;
					
					// echo "Nombre d'activit�s : " . $ets->Count();

					for ($i = 0; $i < $ets->Count(); $i++)
					{
						$currentAction = $ets->items($i);
						$currentsEtablissement = $currentAction->GetActionEtablissment((isset($_POST['agent'])&& $_POST['agent'] !='-1' && $_POST['agent'] != '-2') ? $_POST['agent'] : null); //Etablissement[]
						$currentsActionContact= $currentAction->GetActionContact(); // ActionContact[]
						
						$etsIds .= ($etsIds == null ? '' : ',') . $currentAction->GetActionId();
						
						if ($currentsActionContact->Count() == 0 && $currentsEtablissement->Count()==0) { 
							$this->renderElt($currentAction, null, null);
						} else if ($currentsActionContact->Count() == 0) {
							for ($k=0; $k < $currentsEtablissement->Count() ; $k++) {
								$currentEtablissement = $currentsEtablissement->items($k);
								$this->renderElt($currentAction, $currentEtablissement, null);
							}
						} else {
						
							for ($j=0; $j < $currentsActionContact->Count(); $j++ ) {
								
								$currentActionContact = $currentsActionContact->items($j); // ->GetContact();
								
								$listEtablissementsWithoutContacts = array();
								
								if ($currentsEtablissement->Count() == 0) {
									$this->renderElt($currentAction, null, $currentActionContact);								
								} else {
									
									for ($k=0; $k < $currentsEtablissement->Count() ; $k++) {
										$currentEtablissement = $currentsEtablissement->items($k);
										$currentsContactEtablissement = $currentEtablissement->GetContacts(); // ContactEtablissement[]
										
										$foundContactEtablissement = false;
										
										for ($l=0; $l < $currentsContactEtablissement->Count() ; $l++ ) {
											
											
											$tempcontact = $currentActionContact->GetContact();
											$tempcontact2 = $currentsContactEtablissement->items($l)->getContact();
											if (! isset($tempcontact) || ! isset($tempcontact2)) break;
											
											if ($currentActionContact->GetContact()->GetContactId() == $currentsContactEtablissement->items($l)->getContact()->GetContactId()) {
													// le contact de l'action correspond � un contat de l'etablissement
													// => on affiche l'action, le contact, et l'�tablissement li�
													$this->renderElt($currentAction, $currentEtablissement, $currentActionContact);
													$foundContactEtablissement = true;
													break;
											} 
											
										}
										
										if (! $foundContactEtablissement) {
											// on n'a pas trouv� d'�tablissements qui ont le meme contact que celui de l'action
											// => on affiche l'action, le contact mais pas l'�tablissement
											// $this->renderElt($currentAction, null, $currentActionContact);
											// on enregistre tous les Etablissements pour lesquels on n'a pas trouve de contact...
											$listEtablissementsWithoutContacts[$currentEtablissement->GetNom()] = $currentEtablissement;
										}
										
									}
									
									for ($k=0; $k < $currentsEtablissement->Count() ; $k++) {
										// on parcourt tous les etablissements de l'action 
										// et on regarde si un ne serait pas li� aux actionContact
										// si oui -> on affiche l'�tablissement sans contact
										$currentEtablissement = $currentsEtablissement->items($k);
										$nomTemp = $currentEtablissement->GetNom();
										if (! isset($listEtablissementsWithoutContacts[$nomTemp])) {
											// $this->renderElt($currentAction, $currentEtablissement, null);									
										}
									}
								}
								
							}
						}
					}
	
					echo sprintf('<input type="hidden" value="%s" id="EtsIdList" />', $etsIds);
					$buffer = ob_get_clean();
					echo $buffer; 
					
					?>
				</table>
			</div>

			<div id="Map" style="width: 100%; display: none;">
				<div id="googleMap">
					<div class="popupPanel" style="border: none;">
						<div class="bd" style="text-align: center;">
							<br /> <br />
							<iframe id="frmGoogleMaps" frameborder=0 src=""
								style="width: 1024px; height: 850px; border: 0px;"
								scrolling="auto"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
		document.getElementById("spnResults").style.backgroundPosition = "100% -250px";
		document.getElementById("spnResults").style.color = "#FFFFFF";
		document.getElementById("aResults").style.backgroundPosition = "0% -250px";
	</script>

<?php
	}
} 
?>