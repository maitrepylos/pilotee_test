<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class ListingContactView extends BaseView
{
	private $user = null;

	public function Render()
	{
		$this->user = Session::GetInstance()->getCurrentUser();
		?>
		
<div class="boxGen" style="padding-right: 5px; padding-top: 10px;">
	<form id="ReportContacts" 
	 	action="<?php echo $this->BuildURL($_REQUEST['module']) . '&type=listing_contact' ?>" method="post" >
		<!-- FFI remove = $_REQUEST['type'] -->
		<input type="hidden" id="SubmitContact" name="SubmitContact"
			value="none"></input>

		<div class="boxPan" style="padding: 5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<colgroup>
					<col width="175" />
					<col width="10" />
					<col width="200" />
					<col width="10" />
					<col width="175" />
					<col width="10" />
					<col width="*" />
				</colgroup>
				
				<tr>
					<td colspan="3" style="font-size: 1px; height: 2px;"></td>
				</tr>
				<?php if (($this->user->isAdmin()) || ($this->user->isCoordinateur()) || ($this->user->isAgent())  || ($this->user->isAgentCoordinateur())) { ?>
				<tr>
					<td>Agent</td>
					<td>:</td>
					<td><select name='agent' style="width: 175px;">
							<?php
							echo '<option value="-1">Tous</option>';

							if (isset($_POST['agent']))
							{
								if ($_POST['agent'] == -2)
								{
									echo '<option value="-2" selected="selected">Pas attribu�</option>';
									echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT));
								}
								else
								{
									echo '<option value="-2">Pas attribu�</option>';
									echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT), $_POST['agent']);
								}
							}
							else
							{
								echo '<option value="-2">Pas attribu�</option>';
								if($this->user->isAgent())
									$_POST['agent'] = $this->user->getUtilisateurId();

								echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT, null), $_POST['agent']);
								
							}
							?>
					</select>
					</td>
					<td></td>
					<td height="25" colspan="6">
						<input type="checkbox"  <?php if (isset($_POST["personneRes"])) {echo 'checked="checked"';}?>  name="personneRes" id="personneRes" class="notauto" value="1" > Personnes Ressource&nbsp;<br />
						<input type="checkbox"  <?php if (isset($_POST["personneResNot"])) {echo 'checked="checked"';}?>  name="personneResNot" id="personneResNot" class="notauto" value="1" > Contacts&nbsp;<br />
						<!-- TODO : FFI : to remove <input type="checkbox"  <?php if (isset($_POST["actionLab"])) {echo 'checked="checked"';}?>  name="actionLab" id="actionLab" class="notauto" onclick="checkTypeAction('lab',this)" value="1" > Actions labellis�es&nbsp;<br />
						<input type="checkbox" <?php if (isset($_POST["projEntr"])) echo 'checked="checked"';?> name="projEntr" id="projEntr" class="notauto" onclick="checkTypeAction('ent',this)" value="2"> Projets entrepreneuriaux&nbsp;<br />
					 	<input type="checkbox" <?php if (isset($_POST["form"])) echo 'checked="checked"';?> name="form" id="form" class="notauto" onclick="checkTypeAction('form',this)" value="3"> Formation
					 	-->
					</td>					
					<td></td>
					<td></td>
				</tr>
				<!-- // todo : FFI : to remove ! <tr>
					<td height="25">Ann�e scolaire</td>
					<td>:</td>					
					<td>
						<select id="anneesScolaire" name="anneesScolaire" style="width: 230px;" onchange="updateActionLabelise(this.value)">
						<?php
						echo createSelectOptions(Dictionnaries::getAnneeList(), $_POST['anneesScolaire']);
						?>
						</select>
					</td>
					<td></td>
					<td></td>
				</tr>
				-->
				<tr>
					<td colspan="3" style="font-size: 1px; height: 2px;"></td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="3" style="font-size: 1px; height: 5px;"></td>
				</tr>
				<tr style="vertical-align: top;">
					<td>Code postal</td>
					<td>:</td>
					<td><input type="text" maxlength="4" class="notauto" size="10"
						name="code_postal_1"
						value="<?php if (isset($_POST["code_postal_1"])) echo ToHTML($_POST["code_postal_1"]) ?>"></input>
						&nbsp;�&nbsp; <input type="text" maxlength="4" class="notauto"
						size="10" name="code_postal_2"
						value="<?php if (isset($_POST["code_postal_2"])) echo ToHTML($_POST["code_postal_2"]) ?>"></input>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td height="25">Dates</td>
					<td>:</td>				
					<td colspan="3">
						
						Entre <input size="20" maxlength="10" class="notauto" type="text" name="annee_ante" id="annee_ante" value="<?php if(isset($_POST["annee_ante"])) echo ToHTML($_POST["annee_ante"]) ?>" />
						<img src='./styles/img/calendar3.gif' id='date_ante_pic' class="expand" title='Calendrier' />
						<script type='text/javascript'>
						Calendar.setup({
	        				inputField     :    'annee_ante',     
	        				ifFormat       :    '%d/%m/%Y',      
	        				button         :    'date_ante_pic',  
	        				align          :    'Tr'            
						});
						</script>
						et
						<input size="20" maxlength="10" class="notauto" type="text" name="annee_post" id="annee_post" value="<?php if(isset($_POST["annee_post"])) echo ToHTML($_POST["annee_post"]) ?>" />
						<img src='./styles/img/calendar3.gif' id='date_post_pic' class="expand" title='Calendrier' />
						<img src="./styles/img/eraser.gif" onclick="document.getElementById('annee_ante').value='';document.getElementById('annee_post').value='';" class="expand" title="vider" />
						<script type='text/javascript'>
	   					Calendar.setup({
	        				inputField     :    'annee_post',     
	        				ifFormat       :    '%d/%m/%Y',      
	        				button         :    'date_post_pic',  
	        				align          :    'Tr'            
						});
						</script> 
						
					</td>
					
					<td></td>
					<td></td>	
				</tr>			
				<tr>
					<td height="25">Titre</td>
					<td>:</td>					
					<td>
						<!-- <input size="40" maxlength="100" class="notauto" type="text" name="titre" id="titre" value="<?php if(isset($_POST["titre"])) echo ToHTML($_POST["titre"]) ?>" /> -->
						
						<select id="titre" name="titre" style="width: 230px;">
						<option value="-1">Tous</option>
						<?php
						echo createSelectOptions(Dictionnaries::getTitreContactList(), $_POST['titre']);
						?>
						</select>
					</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td height="25">Mati�re</td>
					<td>:</td>					
					<td>
						<!-- <input size="40" maxlength="100" class="notauto" type="text" name="matiere" id="matiere" value="<?php if(isset($_POST["matiere"])) echo ToHTML($_POST["matiere"]) ?>" /> -->
						<select id="matiere" name="matiere" style="width: 230px;">
						<option value="-1">Tous</option>
						<?php
						echo createSelectOptions(Dictionnaries::getSpecialiteList(), $_POST['matiere']);
						?>
						</select>
					
					</td>
					
					<td></td>
					<td></td>
				</tr>
				
				<tr>
					<td colspan="3" style="font-size: 1px; height: 5px;"></td>
				</tr>
			</table>
		</div>
	</form>
</div>

<div class="boxBtn" style="padding: 5px;">
	<table class="buttonTable">
		<tr>
			<td
				onclick="document.getElementById('SubmitContact').value='search';submitForm('ReportContacts')">Rechercher</td>
			<td class="separator"></td>
			<td onclick="clearForm('ReportContact')">Effacer</td>
		</tr>
	</table>
</div>

<?php
	}

	public function noResult()
	{
		?>
<div id="DivNoResult" class="boxGen" style="border: 0px;">
	<br />
	<div class="boxPan" style="padding: 5px; border: 0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			class="result">
			<tr>
				<td
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 3px;">
					Il n'y a aucun r�sultat pour votre requ�te ...</td>
			</tr>
		</table>
	</div>
</div>
<?php
	}
	
	public function renderElt($contact, $contactEtablissement, $etablissement, $action) {
		
		$userNom = null;
		$userPrenom = null;
		$userTel = null;
		$userEmail = null;
		$userLabel = null;
		// $currentAction = null;
		$contactNom = null;
		$contactPrenom = null;
		$etablissementNom = null;
		$titreContact = null;
		$titreContact_label = null;
		$matiereContact = null;
		$matiereContact_label = null;
		$nomAction = null;
		$typeAction = null;
		$cpEts = null;
		$villeEts = null;
		$rueEts = null;
		
		$personneRessource = null;
		
		$contactNom = $contact->GetNom();
		$contactPrenom =  $contact->GetPrenom();
		$userTel = $contact->GetAdresse()->GetTel1();
		$userEmail = $contact->GetAdresse()->GetEmail1();
		
		if ($contactEtablissement != null) { 
			$titreContact = $contactEtablissement->getTitreContact();
			if ($titreContact != null) $titreContact_label = $titreContact->getLabel();
			
			$matiereContact = $contactEtablissement->getSpecialiteContact();
			if ($matiereContact != null) $matiereContact_label = $matiereContact->getLabel();
			
			$tempParticipation = $contactEtablissement->getParticipation();
			if ($tempParticipation) $personneRessource = $tempParticipation->getLabel();
		}
		
		if ($etablissement != null) {
			$etablissementNom = $etablissement->GetNom();
			
			$utilisateur = $etablissement->GetUtilisateur() ;
			
			if($utilisateur != null)
			{
				//$userNom = $utilisateur->GetNom();
				//$userPrenom = $utilisateur->GetPrenom();
				//$userTel = $utilisateur->GetTel();
				//$userEmail = $utilisateur->GetEmail();
				$userLabel = $utilisateur->GetLabel();
			}
			
			$adresse_temp = $etablissement->GetAdresse();
			
			if ($adresse_temp != null) {
				$cpEts = $adresse_temp->GetCodePostal();
				$villeEts = $adresse_temp->GetVille();
				$rueEts = $adresse_temp->GetRue();
			}
		}
		
		if ($action != null) {	
			$nomAction = $action->GetNomGen() ; 
			$typeAction = $action->GetTypeActionId();
		}
		
		
		echo '<tr>';
		echo sprintf('<td class="resultLine">%s</td>', isset($contactNom) ? $contactNom : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($contactPrenom) ? $contactPrenom : '&nbsp;' );
		echo sprintf('<td class="resultLine">%s</td>', isset($titreContact_label) ? $titreContact_label : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($matiereContact_label) ? $matiereContact_label : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($userEmail) ? $userEmail : '&nbsp;'); // idem
		echo sprintf('<td class="resultLine">%s</td>', isset($userTel) ? $userTel : '&nbsp;') ; //? contact ou ets
		echo sprintf('<td class="resultLine">%s</td>', isset($personneRessource) && $personneRessource == 1 ? ' OUI ' : ' NON '); // idem
		
		
		echo sprintf('<td class="resultLine">%s</td>', isset($etablissementNom) ? $etablissementNom : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($rueEts) ? $rueEts : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($cpEts) ? $cpEts->GetCodepostal() : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($villeEts) ? $villeEts : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($userLabel) ? $userLabel : '&nbsp;'); // idem
		
		//echo sprintf('<td class="resultLine">%s</td>', isset($nomAction) ? $nomAction : '&nbsp;');
		//echo sprintf('<td class="resultLine">%s</td>', isset($typeAction) ? $typeAction : '&nbsp;');
		
		// isset($cpEts) ? $cpEts->GetCodepostal() : '&nbsp;');
		
		echo '</tr>';
		
	}

	public function renderList($ets)
	{
		?>
<script language="javascript" type="text/javascript">
	function ListingContactViewClass() 
	{
		this.current = 'Results';
		this.tabs = new Array('Results', 'Map');
	};

	ListingContactViewClass.prototype.onMouseOver = function(liID)
	{
		document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
		document.getElementById("spn" + liID).style.color = "#FFFFFF";
		document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
	};

	ListingContactViewClass.prototype.onMouseOut = function(liID)
	{
		if (this.current != liID)
		{
			document.getElementById("spn" + liID).style.backgroundPosition = "100% 0%";
			document.getElementById("spn" + liID).style.color = "#737373";
			document.getElementById("a" + liID).style.backgroundPosition = "0% 0%";
		}
	};

	ListingContactViewClass.prototype.onLIClick = function(liID)
	{
		this.current = liID;
		
		for (var i = 0; i < this.tabs.length; i++)
		{
			if (document.getElementById("spn" + this.tabs[i]))
			{
				document.getElementById("spn" + this.tabs[i]).style.backgroundPosition = "100% 0%";
				document.getElementById("spn" + this.tabs[i]) .style.color = "#737373";
				document.getElementById("a" + this.tabs[i]).style.backgroundPosition = "0% 0%";
			}
		}

		document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
		document.getElementById("spn" + liID).style.color = "#FFFFFF";
		document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
		
		var results = document.getElementById('Results');
		var map = document.getElementById('Map');
		
		if (liID == 'Results')
		{
			if (results) results.style.display = '';
			if (map) map.style.display = 'none';

			var frm = document.getElementById("frmGoogleMaps");
			frm.src = 'about:blank';
		}
		else if (liID == 'Map')
		{
			if (results) results.style.display = 'none';
			if (map) map.style.display = '';

			this.renderMap();
		}
	};

	ListingContactViewClass.prototype.renderMap = function()
	{
		var frm = document.getElementById("frmGoogleMaps");
		frm.src = "<?php echo SCRIPTPATH . 'index.php?module=popup_google_maps&anneeId=' . $_POST['anneesScolaire'] ?>";
		frm.src += "&ids=" + document.getElementById("EtsIdList").value;
	};
	
	window.revc = new ListingContactViewClass();
	
	function getexcel()
	{
		window.location.replace("<?php echo $this->BuildURL('exportXLS').'&type=listing_contact';?>");
	}
	
	</script>

<div id="DivResult" class="boxGen" style="border: 0px;">
	<div class="boxPan" style="padding: 5px; border: 0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			class="result">
			<tr>
				<td colspan="9" class="title"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 3px;">Listing
					sur les contacts &nbsp; <img class="expand"
					src="./styles/img/csv.gif" title="T�l�charger la version Excel"
					onclick="getexcel();" />
				</td>
			</tr>
		</table>

		<div class="header" style="border-bottom: 3px solid #EAC3C3;">
			<ul>
				<li OnMouseOver="javascript:window.revc.onMouseOver('Results');"
					OnMouseOut="javascript:window.revc.onMouseOut('Results');"><a
					id="aResults" href="javascript:void(0)"
					onclick="javascript:window.revc.onLIClick('Results');"><span
						id="spnResults" class="expand">Liste de r�sultats</span> </a></li>
				<!--  <li OnMouseOver="javascript:window.revc.onMouseOver('Map');"
					OnMouseOut="javascript:window.revc.onMouseOut('Map');"><a id="aMap"
					href="javascript:void(0)"
					onclick="javascript:window.revc.onLIClick('Map');"><span
						id="spnMap" class="expand">Cartographie</span> </a></li> -->
			</ul>
		</div>

		<div style="width: 100%;">
			<div id="Results" style="width: 100%;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="result">
					<?php ob_start(); ?>
					<tr>
						<td class="resultHeader">Nom</td>
						<td class="resultHeader">Pr�nom</td>
						<td class="resultHeader">Titre</td>
						<td class="resultHeader">Mati�re</td>
						<td class="resultHeader">Mail</td>
						<td class="resultHeader">Tel</td>
						<td class="resultHeader">Pers Res</td>
						<td class="resultHeader">Etablissement</td>
						<td class="resultHeader">Adresse</td>
						<td class="resultHeader">CP</td>
						<td class="resultHeader">Ville</td>
						
						
						<td class="resultHeader">Agent</td>
						
						
						<!-- <td class="resultHeader">Activit�</td>
						<td class="resultHeader">Type</td> -->
					</tr>
					<?php
					$statutActiviteOK = 0;
					$statutActivitePasse = 0;
					$statutActiviteJamais = 0;
					$statutActiviteGlobal = 0;
					$statutVisiteOK = 0;
					$statutVisitePasse = 0;
					$statutVisiteJamais = 0;
					$statutVisiteGlobal = 0;

					$etsIds = null;
					
					for ($i = 0; $i < $ets->Count(); $i++)
					{
						$currentContact = $ets->items($i);
						$currentsContactEtablissement = $currentContact->GetEtablissementsContacts(); // ContactEtablissement[]
						
						$etsIds .= ($etsIds == null ? '' : ',') . $currentContact->GetContactId();

						if ($currentsContactEtablissement->Count() == 0) {
							$this->renderElt($currentContact, null, null, null);
							
						} else {

							for ($j=0; $j < $currentsContactEtablissement->Count(); $j++) {
								$currentContactEtablissement = $currentsContactEtablissement->items($j);
								$currentEtablissement = $currentContactEtablissement->GetEtablissement(); //Etablissement !
								if ($currentEtablissement == null) {
									$this->renderElt($currentContact, $currentContactEtablissement, null, null);									
								} else {
									// $currentsAction = $currentEtablissement->GetActions(null,null); // ActionEtablissement[]
									
									//if ( $currentsAction->Count() ==0 ) {
										$this->renderElt($currentContact, $currentContactEtablissement, $currentEtablissement, null);
									//} else {		
									//	for ($k=0; $k < $currentsAction->Count(); $k++) {
									//		$currentAction = $currentsAction->items($k)->GetAction(); // Action !
									//		$this->renderElt($currentContact, $currentContactEtablissement, $currentEtablissement, $currentAction);
											
									//	}
									// }
								}
								
							}
						}
						
					}

					echo sprintf('<input type="hidden" value="%s" id="EtsIdList" />', $etsIds);
					$buffer = ob_get_clean();
					?>
					<!-- FFI Comment <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="resultHeader" style="color: white; border: ">Total</td>
						<td class="resultHeader" style="color: white; border: "
							align="right"><?php echo $statutActiviteGlobal; ?></td>
						<td class="resultHeader" style="color: white; border: "
							align="right"><?php echo $statutVisiteGlobal; ?></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="resultHeader"
							style="background-color: green; color: white;">OK</td>
						<td class="resultHeader" align="right"
							style="background-color: green; color: white;"><?php echo $statutActiviteOK; ?>
						</td>
						<td class="resultHeader" align="right"
							style="background-color: green; color: white;"><?php echo $statutVisiteOK; ?>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="resultHeader"
							style="background-color: orange; color: white;">Avant</td>
						<td class="resultHeader" align="right"
							style="background-color: orange; color: white;"><?php echo $statutActivitePasse; ?>
						</td>
						<td class="resultHeader" align="right"
							style="background-color: orange; color: white;"><?php echo $statutVisitePasse; ?>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="resultHeader"
							style="background-color: gray; color: white;">Jamais</td>
						<td class="resultHeader" align="right"
							style="background-color: gray; color: white;"><?php echo $statutActiviteJamais; ?>
						</td>
						<td class="resultHeader" align="right"
							style="background-color: gray; color: white;"><?php echo $statutVisiteJamais; ?>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="10">&nbsp;</td>
					</tr>
					-->
					<?php echo $buffer; ?>
				</table>
			</div>

			<div id="Map" style="width: 100%; display: none;">
				<div id="googleMap">
					<div class="popupPanel" style="border: none;">
						<div class="bd" style="text-align: center;">
							<br /> <br />
							<iframe id="frmGoogleMaps" frameborder=0 src=""
								style="width: 1024px; height: 850px; border: 0px;"
								scrolling="auto"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
		document.getElementById("spnResults").style.backgroundPosition = "100% -250px";
		document.getElementById("spnResults").style.color = "#FFFFFF";
		document.getElementById("aResults").style.backgroundPosition = "0% -250px";
	</script>

<?php
	}
} 
?>
