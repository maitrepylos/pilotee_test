<?php
error_reporting(~E_NOTICE); // permet d'emp�cher l'apparition des Warning/Notice.
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class ListingEtablissementsView extends BaseView
{
	private $user = null;

	public function Render()
	{
		$this->user = Session::GetInstance()->getCurrentUser();
		?>
<div class="boxGen" style="padding-right: 5px; padding-top: 10px;">
	<form id="ListingEtablissement"
		action="<?php echo $this->BuildURL($_REQUEST['module']) . '&type=' . $_REQUEST['type'];?>"
		method="post">
		<input type="hidden" id="SubmitAction" name="SubmitAction"
			value="none"></input>

		<div class="boxPan" style="padding: 5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<colgroup>
					<col width="175" />
					<col width="10" />
					<col width="200" />
					<col width="10" />
					<col width="175" />
					<col width="10" />
					<col width="*" />
				</colgroup>
				<?php if (($this->user->isAdmin()) || ($this->user->isCoordinateur()) || ($this->user->isAgent())  || ($this->user->isAgentCoordinateur()) ) { ?>
				<tr>
					<td>Agent</td>
					<td>:</td>
					<td>
						<select name='agent' style="width: 175px;">
							<?php
							echo '<option value="-1">Tous</option>';

							if (isset($_POST['agent']))
							{
								if ($_POST['agent'] == -2)
								{
									echo '<option value="-2" selected="selected">Pas attribu�</option>';
									echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT));
								}
								else
								{
									echo '<option value="-2">Pas attribu�</option>';
									echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT), $_POST['agent']);
								}
							}
							else
							{
								echo '<option value="-2">Pas attribu�</option>';
								if($this->user->isAgent())
									$_POST['agent'] = $this->user->getUtilisateurId();

								echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT, null), $_POST['agent']);
							}
							?>
						</select>
					</td>
					<?php } ?>
					<td></td>
					<td>Activit�</td>
					<td>:</td>
					<td>
					<input type="text" class="notauto" size="41"
						name="activite"
						value="<?php if (isset($_POST["activite"])) echo ToHTML($_POST["activite"]) ?>">
					</input>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 1px; height: 2px;"></td>
				</tr>
				<tr style="vertical-align: top;">
					<td>Arrondissements</td>
					<td>:</td>
					<td>
						<div id="divArr"
							style="width: 225px; height: 75px; overflow: auto; background-color: white; border: 1px solid #EAC3C3; padding: 2px;">
							<?php 
							$arrondissements = Dictionnaries::getArrondissementList();

							for ($i = 0; $i < $arrondissements->count(); $i++)
								echo '<div style="vertical-align:bottom;"><input type="checkbox" name="chk_arr_' . $arrondissements->items($i)->getId() . '"' . (isset($_POST['chk_arr_' . $arrondissements->items($i)->getId()]) ? 'checked' : null) . '></input>&nbsp;' . $arrondissements->items($i)->getLabel() . '</div>';
							?>
						</div>
					</td>
					<td></td>
					<!-- <td>R�seaux</td>
					<td>:</td>
					<td>
						<div
							style="width: 225px; height: 75px; overflow: auto; background-color: white; border: 1px solid #EAC3C3; padding: 2px;">
							<?php 
							$reseaux = Dictionnaries::getReseauxList();

							for ($i = 0; $i < $reseaux->count(); $i++)
								echo '<div style="vertical-align:bottom;"><input type="checkbox" name="chk_res_' . $reseaux->items($i)->getId() . '"' . $reseaux->items($i)->getId() . '"' . (isset($_POST['chk_res_' . $reseaux->items($i)->getId()]) ? 'checked' : null) . '></input>&nbsp;' . $reseaux->items($i)->getLabel() . '</div>';
							?>
						</div>
					</td> -->
				</tr>
				<tr>
					<td colspan="3" style="font-size: 1px; height: 5px;"></td>
				</tr>
				<tr style="vertical-align: top;">
					<td>Code postal</td>
					<td>:</td>
					<td><input type="text" maxlength="4" class="notauto" size="10"
						name="code_postal_1"
						value="<?php if (isset($_POST["code_postal_1"])) echo ToHTML($_POST["code_postal_1"]) ?>"></input>
						&nbsp;�&nbsp; <input type="text" maxlength="4" class="notauto"
						size="10" name="code_postal_2"
						value="<?php if (isset($_POST["code_postal_2"])) echo ToHTML($_POST["code_postal_2"]) ?>"></input>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</form>
</div>

<div class="boxBtn" style="padding: 5px;">
	<table class="buttonTable">
		<tr>
			<td
				onclick="document.getElementById('SubmitAction').value='search';submitForm('ListingEtablissement')">Rechercher</td>
			<td class="separator"></td>
			<td onclick="clearForm('ListingEtablissement')">Effacer</td>
		</tr>
	</table>
</div>

<?php
	}

	public function noResult()
	{
		?>
<div id="DivNoResult" class="boxGen" style="border: 0px;">
	<br />
	<div class="boxPan" style="padding: 5px; border: 0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			class="result">
			<tr>
				<td
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 3px;">
					Il n'y a aucun r�sultat pour votre requ�te ...</td>
			</tr>
		</table>
	</div>
</div>
<?php
	}
	
	public function renderElt($ets) {   // renderElt($ets, $action, $actionContact) {
		
		
		$userLabel = null;
		$adresse = null;
		$cp = null;
		$ville = null;
		$rue = null;
		$province = null;
		$arrondissement = null;
		
		$adresse = $ets->GetAdresse();
			
		if (isset($adresse)) {
			$cp_temp = $adresse->GetCodePostal();
			if (isset ($cp_temp)) {
				$cp = $cp_temp->GetCodePostal();
				$arrondissement_temp =  $cp_temp->GetArrondissement();
				if (isset($arrondissement_temp)) $arrondissement = $arrondissement_temp->getLabel();				
			}
			$ville = $adresse->GetVille();
			$rue = $adresse->GetRue();
			$province_tmp = $adresse->GetProvince();
			if (isset($province_tmp)) $province = $province_tmp->getLabel();
		}
		
			
	
		/*
		$userNom = null;
		$userPrenom = null;
		$userTel = null;
		$userEmail = null;
		$userLabel = null;
		
		if ($actionContact != null) {
			$contact = $actionContact->GetContact();
			
			if ($contact) {
				$userNom = $contact->GetNom();
				$userPrenom = $contact->GetPreNom();
				$userTel = $contact->GetAdresse()->GetTel1();
				$userEmail = $contact->GetAdresse()->GetEmail1();
			}
		}*/
		
		if($ets->GetUtilisateur() != null)
		{
			//$userNom = $ets->items($i)->GetUtilisateur()->GetNom();
			//$userPrenom = $ets->items($i)->GetUtilisateur()->GetPrenom();
			///$userTel = $ets->items($i)->GetUtilisateur()->GetTel();
			//$userEmail = $ets->items($i)->GetUtilisateur()->GetEmail();
			$userLabel = $ets->GetUtilisateur()->GetLabel();
		}
		
		//echo sprintf('<td class="resultLine">%s</td>', $userNom);
		//echo sprintf('<td class="resultLine">%s</td>', $userPrenom);
		
		//echo sprintf('<td class="resultLine">%s</td>', $userTel);
		//echo sprintf('<td class="resultLine">%s</td>', $userEmail);
		
		
		/*$nomGen = null;
		$typeAction = null;
		
		if ($action != null) {
			$nomGen = $action->GetAction()->GetNomGen(); // . ' (... + ' . (count($tempFFI) - 1) .  ' !!!'; // TODO : more than one action ? list ?
			$typeAction = 	$action->GetAction()->GetTypeActionId();
		}
		
		*/
		
		// if (! $nomGen) $nomGen = "&nbsp;";
		// if (! $typeAction) $typeAction = "&nbsp;";
		
		
		
		echo '<tr>';
		
		echo sprintf('<td class="resultLine">%s</td>', $ets->GetNom());
		echo sprintf('<td class="resultLine">%s</td>', isset($rue) ? $rue : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($cp) ? $cp : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($ville) ? $ville : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($arrondissement) ? $arrondissement : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($province) ? $province : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', $ets->GetReseauEtablissement());
		echo sprintf('<td class="resultLine">%s</td>', $ets->GetNiveauEtablissement());
		
		echo sprintf('<td class="resultLine">%s</td>', $userLabel);
		
		
		//echo sprintf('<td class="resultLine">%s</td>', $nomGen);
		//echo sprintf('<td class="resultLine">%s</td>', $typeAction);

		echo '</tr>';
		
	
		$rue = null;
		
	}

	public function renderList($ets)
	{
		?>
<script language="javascript" type="text/javascript">
	function ListingEtablissementViewClass() 
	{
		this.current = 'Results';
		this.tabs = new Array('Results', 'Map');
	};

	ListingEtablissementViewClass.prototype.onMouseOver = function(liID)
	{
		document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
		document.getElementById("spn" + liID).style.color = "#FFFFFF";
		document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
	};

	ListingEtablissementViewClass.prototype.onMouseOut = function(liID)
	{
		if (this.current != liID)
		{
			document.getElementById("spn" + liID).style.backgroundPosition = "100% 0%";
			document.getElementById("spn" + liID).style.color = "#737373";
			document.getElementById("a" + liID).style.backgroundPosition = "0% 0%";
		}
	};

	ListingEtablissementViewClass.prototype.onLIClick = function(liID)
	{
		this.current = liID;
		
		for (var i = 0; i < this.tabs.length; i++)
		{
			if (document.getElementById("spn" + this.tabs[i]))
			{
				document.getElementById("spn" + this.tabs[i]).style.backgroundPosition = "100% 0%";
				document.getElementById("spn" + this.tabs[i]) .style.color = "#737373";
				document.getElementById("a" + this.tabs[i]).style.backgroundPosition = "0% 0%";
			}
		}

		document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
		document.getElementById("spn" + liID).style.color = "#FFFFFF";
		document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
		
		var results = document.getElementById('Results');
		var map = document.getElementById('Map');
		
		if (liID == 'Results')
		{
			if (results) results.style.display = '';
			if (map) map.style.display = 'none';

			var frm = document.getElementById("frmGoogleMaps");
			frm.src = 'about:blank';
		}
		else if (liID == 'Map')
		{
			if (results) results.style.display = 'none';
			if (map) map.style.display = '';

			this.renderMap();
		}
	};

	ListingEtablissementViewClass.prototype.renderMap = function()
	{
		var frm = document.getElementById("frmGoogleMaps");
		frm.src = "<?php echo SCRIPTPATH . 'index.php?module=popup_google_maps&anneeId=' . $_POST['anneesScolaire'] ?>";
		frm.src += "&ids=" + document.getElementById("EtsIdList").value;
	};
	
	window.revc = new ListingEtablissementViewClass();
	
	function getexcel()
	{
		window.location.replace("<?php echo $this->BuildURL('exportXLS').'&type=listing_etablissement';?>");
	}
	
	</script>

<div id="DivResult" class="boxGen" style="border: 0px;">
	<div class="boxPan" style="padding: 5px; border: 0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			class="result">
			<tr>
				<td colspan="9" class="title"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 3px;">Listing
					sur les �tablissements scolaires &nbsp; <img class="expand"
					src="./styles/img/csv.gif" title="T�l�charger la version Excel"
					onclick="getexcel();" />
				</td>
			</tr>
		</table>

		<div class="header" style="border-bottom: 3px solid #EAC3C3;">
			<ul>
				<li OnMouseOver="javascript:window.revc.onMouseOver('Results');"
					OnMouseOut="javascript:window.revc.onMouseOut('Results');"><a
					id="aResults" href="javascript:void(0)"
					onclick="javascript:window.revc.onLIClick('Results');"><span
						id="spnResults" class="expand">Liste de r�sultats</span> </a></li>
			</ul>
		</div>
<?php //FFI var_dump($ets);
	// FFI 
		echo "Nombre d'�tablissements : " . $ets->Count();
?>
		<div style="width: 100%;">
			<div id="Results" style="width: 100%;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="result">
					<?php ob_start(); ?>
					<tr>
						<td class="resultHeader">Etablissement</td>
						<td class="resultHeader">Adresse</td>
						<td class="resultHeader">Code postal</td>
						<td class="resultHeader">Ville</td>
						<td class="resultHeader">Arrondissement</td>
						<td class="resultHeader">Province</td>
						<td class="resultHeader">R�seau</td>
						<td class="resultHeader">Niveau</td>
						<td class="resultHeader">Agent</td>
						
					</tr>
					<?php
					$statutActiviteOK = 0;
					$statutActivitePasse = 0;
					$statutActiviteJamais = 0;
					$statutActiviteGlobal = 0;
					$statutVisiteOK = 0;
					$statutVisitePasse = 0;
					$statutVisiteJamais = 0;
					$statutVisiteGlobal = 0;

					$etsIds = null;

					for ($i = 0; $i < $ets->Count(); $i++)
					{
						$etsIds .= ($etsIds == null ? '' : ',') . $ets->items($i)->GetEtablissementId(); // ?
						
						$this->renderElt($ets->items($i));
						
						/*
						$tempActions = $ets->items($i)->GetActions(null,null);
						
						if ($tempActions->Count() == 0) {
							$this->renderElt($ets->items($i), null, null);
						} else {
						
							for ($j = 0; $j < $tempActions->Count();$j++ ) {
								
								$tempActionContact = $tempActions->items($j)->GetAction()->GetActionContact();
								
								if ($tempActionContact->Count() == 0) {
									$this->renderElt($ets->items($i), $tempActions->items($j), null);
								} else {
		
									for ($k = 0; $k < $tempActionContact->Count();$k++) {
									
										$this->renderElt($ets->items($i), $tempActions->items($j), $tempActionContact->items($k));
									}
								}
								
							}
						}*/
					}
					
					/*		$etsIds .= ($etsIds == null ? '' : ',') . $ets->items($i)->GetEtablissementId();
	
							echo '<tr>';
							echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->GetNom());
								
							$cp = $ets->items($i)->GetAdresse()->GetCodePostal();
								
							echo sprintf('<td class="resultLine">%s</td>', isset($cp) ? $cp->GetCodePostal() : '&nbsp;');
							echo sprintf('<td class="resultLine">%s</td>', isset($cp) ? $cp->GetArrondissement()->getLabel() : '&nbsp;');
							
							$userNom = null;
							$userPrenom = null;
							$userTel = null;
							$userEmail = null;
							$userLabel = null;
							
						
							$tempContact = $tempActionContact->items($k)->GetContact();
							$userNom = $tempContact->GetNom();
							$userPrenom = $tempContact->GetPreNom();
							$userTel = $tempContact->GetAdresse()->GetTel1();
							$userEmail = $tempContact->GetAdresse()->GetEmail1();
						
							
							if($ets->items($i)->GetUtilisateur() != null)
							{
								//$userNom = $ets->items($i)->GetUtilisateur()->GetNom();
								//$userPrenom = $ets->items($i)->GetUtilisateur()->GetPrenom();
								///$userTel = $ets->items($i)->GetUtilisateur()->GetTel();
								//$userEmail = $ets->items($i)->GetUtilisateur()->GetEmail();
								$userLabel = $ets->items($i)->GetUtilisateur()->GetLabel();
							}
							
							echo sprintf('<td class="resultLine">%s</td>', $userNom);
							echo sprintf('<td class="resultLine">%s</td>', $userPrenom);
							
							echo sprintf('<td class="resultLine">%s</td>', $userTel);
							echo sprintf('<td class="resultLine">%s</td>', $userEmail);
							echo sprintf('<td class="resultLine">%s</td>', $userLabel); // quel sens �a a ???
							
							$nomGen = null;
								// FFI : var_dump($ets->items($i)->GetActions(null, null));
								
							// FFI question : pourquoi prendre les actions li�es � l'utilisateur id uniquement ???
							/* before FFI 
							if(count($ets->items($i)->GetActions($ets->items($i)->GetUtilisateurId(), null)) > 0)
							{
								$nomGen = 	$ets->items($i)->GetActions($ets->items($i)->GetUtilisateurId(), null)->items(0)->GetAction()->GetNomGen();
							}
							else 
							{
								$nomGen = '&nbsp';
							}	
							$nomGen = $tempFFI->items($j)->GetAction()->GetNomGen(); // . ' (... + ' . (count($tempFFI) - 1) .  ' !!!'; // TODO : more than one action ? list ?
							
							if (! $nomGen) $nomGen = "&nbsp;";
							
							echo sprintf('<td class="resultLine">%s</td>', $nomGen);
						
							$typeAction = 	$tempFFI->items($j)->GetAction()->GetTypeActionId();
														
							echo sprintf('<td class="resultLine">%s</td>', $typeAction);
	//							
		
							echo '</tr>';
						}
					} */

					echo sprintf('<input type="hidden" value="%s" id="EtsIdList" />', $etsIds);
					$buffer = ob_get_clean();
					echo $buffer; ?>
				</table>
			</div>

			<div id="Map" style="width: 100%; display: none;">
				<div id="googleMap">
					<div class="popupPanel" style="border: none;">
						<div class="bd" style="text-align: center;">
							<br /> <br />
							<iframe id="frmGoogleMaps" frameborder=0 src=""
								style="width: 1024px; height: 850px; border: 0px;"
								scrolling="auto"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
		document.getElementById("spnResults").style.backgroundPosition = "100% -250px";
		document.getElementById("spnResults").style.color = "#FFFFFF";
		document.getElementById("aResults").style.backgroundPosition = "0% -250px";
	</script>

<?php
	}
} 
?>
