<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class ListingFormationView extends BaseView
{
	private $user = null;

	public function Render()
	{
		$this->user = Session::GetInstance()->getCurrentUser();
		?>
		
<div class="boxGen" style="padding-right: 5px; padding-top: 10px;">
	<form id="ReportFormations" 
	 	action="<?php echo $this->BuildURL($_REQUEST['module']) . '&type=listing_formation' ?>" method="post" >
		<!-- FFI remove = $_REQUEST['type'] -->
		<input type="hidden" id="SubmitFormation" name="SubmitFormation"
			value="none"></input>

		<div class="boxPan" style="padding: 5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<colgroup>
					<col width="175" />
					<col width="10" />
					<col width="200" />
					<col width="10" />
					<col width="175" />
					<col width="10" />
					<col width="*" />
				</colgroup>
				<tr>
					<td height="25">Intitul� de formation</td>
					<td>:</td>
					<td>
						<input size="40" maxlength="100" class="notauto" type="text" name="intitule" id="intitule" value="<?php if(isset($_POST["intitule"])) echo ToHTML($_POST["intitule"]) ?>" />
					</td>
					<td></td>
					<td height="25" colspan="6">
					</td>					
					<td></td>
					<td></td>
				</tr>
				
				
				<tr>
					<td colspan="3" style="font-size: 1px; height: 2px;"></td>
				</tr>
				<tr>
					<td height="25">Dates</td>
					<td>:</td>				
					<td colspan="3">
						
						Entre <input size="20" maxlength="10" class="notauto" type="text" name="annee_ante" id="annee_ante" value="<?php if(isset($_POST["annee_ante"])) echo ToHTML($_POST["annee_ante"]) ?>" />
						<img src='./styles/img/calendar3.gif' id='date_ante_pic' class="expand" title='Calendrier' />
						<script type='text/javascript'>
						Calendar.setup({
	        				inputField     :    'annee_ante',     
	        				ifFormat       :    '%d/%m/%Y',      
	        				button         :    'date_ante_pic',  
	        				align          :    'Tr'            
						});
						</script>
						et
						<input size="20" maxlength="10" class="notauto" type="text" name="annee_post" id="annee_post" value="<?php if(isset($_POST["annee_post"])) echo ToHTML($_POST["annee_post"]) ?>" />
						<img src='./styles/img/calendar3.gif' id='date_post_pic' class="expand" title='Calendrier' />
						<img src="./styles/img/eraser.gif" onclick="document.getElementById('annee_ante').value='';document.getElementById('annee_post').value='';" class="expand" title="vider" />
						<script type='text/javascript'>
	   					Calendar.setup({
	        				inputField     :    'annee_post',     
	        				ifFormat       :    '%d/%m/%Y',      
	        				button         :    'date_post_pic',  
	        				align          :    'Tr'            
						});
						</script> 
						
					</td>
					
					<td></td>
					<td></td>	
				</tr>			
				
				<tr>
					<td colspan="3" style="font-size: 1px; height: 5px;"></td>
				</tr>
			</table>
		</div>
	</form>
</div>

<div class="boxBtn" style="padding: 5px;">
	<table class="buttonTable">
		<tr>
			<td
				onclick="document.getElementById('SubmitFormation').value='search';submitForm('ReportFormations')">Rechercher</td>
			<td class="separator"></td>
			<td onclick="clearForm('ReportFormations')">Effacer</td>
		</tr>
	</table>
</div>

<?php
	}

	public function noResult()
	{
		?>
<div id="DivNoResult" class="boxGen" style="border: 0px;">
	<br />
	<div class="boxPan" style="padding: 5px; border: 0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			class="result">
			<tr>
				<td
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 3px;">
					Il n'y a aucun r�sultat pour votre requ�te ...</td>
			</tr>
		</table>
	</div>
</div>
<?php
	}
	
	public function renderElt($formation, $formationParticipant) {
		
		$userNom = null;
		$userPrenom = null;
		$userGenre = null;
		$userAge = null;
		$userSpecialite = null;
		$formateur = null;
		$typeFormation = null;
		$dateFormation = null;
		
		if ($formationParticipant != null) {
						
			$userNom = $formationParticipant->nom;
			$userPrenom =  $formationParticipant->prenom;
			$userGenre = $formationParticipant->getGenreName();
			$tempAge = $formationParticipant->getAgeName();
			$userAge = ($tempAge ? $tempAge : null);
			$userSpecialite = $formationParticipant->GetSpecialite($formation->getFormationId());
		}
		
		$formateurtemp = $formation->getFormateur();
		if ($formateurtemp != null) $formateur = $formateurtemp->getLabel();
		
		$typeFormation = $formation->getTypeFormationId();
		
		$dateFormation = $formation->getDateFormationString(false);
		
		echo '<tr>';
		echo sprintf('<td class="resultLine">%s</td>', isset($userNom) ? $userNom : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($userPrenom) ? $userPrenom : '&nbsp;' );
		echo sprintf('<td class="resultLine">%s</td>', isset($userGenre) ? $userGenre : '&nbsp;');		
		echo sprintf('<td class="resultLine">%s</td>', isset($userAge) ? $userAge : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($userSpecialite) ? $userSpecialite : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($formateur) ? $formateur : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($typeFormation) ? $typeFormation : '&nbsp;');
		echo sprintf('<td class="resultLine">%s</td>', isset($dateFormation) ? $dateFormation : '&nbsp;');
		
		echo '</tr>';
		
	}

	public function renderList($ets)
	{
		?>
<script language="javascript" type="text/javascript">
	function ListingFormationViewClass() 
	{
		this.current = 'Results';
		this.tabs = new Array('Results', 'Map');
	};

	ListingFormationViewClass.prototype.onMouseOver = function(liID)
	{
		document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
		document.getElementById("spn" + liID).style.color = "#FFFFFF";
		document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
	};

	ListingFormationViewClass.prototype.onMouseOut = function(liID)
	{
		if (this.current != liID)
		{
			document.getElementById("spn" + liID).style.backgroundPosition = "100% 0%";
			document.getElementById("spn" + liID).style.color = "#737373";
			document.getElementById("a" + liID).style.backgroundPosition = "0% 0%";
		}
	};

	ListingFormationViewClass.prototype.onLIClick = function(liID)
	{
		this.current = liID;
		
		for (var i = 0; i < this.tabs.length; i++)
		{
			if (document.getElementById("spn" + this.tabs[i]))
			{
				document.getElementById("spn" + this.tabs[i]).style.backgroundPosition = "100% 0%";
				document.getElementById("spn" + this.tabs[i]) .style.color = "#737373";
				document.getElementById("a" + this.tabs[i]).style.backgroundPosition = "0% 0%";
			}
		}

		document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
		document.getElementById("spn" + liID).style.color = "#FFFFFF";
		document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
		
		var results = document.getElementById('Results');
		var map = document.getElementById('Map');
		
		if (liID == 'Results')
		{
			if (results) results.style.display = '';
			if (map) map.style.display = 'none';

			var frm = document.getElementById("frmGoogleMaps");
			frm.src = 'about:blank';
		}
		else if (liID == 'Map')
		{
			if (results) results.style.display = 'none';
			if (map) map.style.display = '';

			this.renderMap();
		}
	};

	ListingFormationViewClass.prototype.renderMap = function()
	{
		var frm = document.getElementById("frmGoogleMaps");
		frm.src = "<?php echo SCRIPTPATH . 'index.php?module=popup_google_maps&anneeId=' . $_POST['anneesScolaire'] ?>";
		frm.src += "&ids=" + document.getElementById("EtsIdList").value;
	};
	
	window.revc = new ListingFormationViewClass();
	
	function getexcel()
	{
		window.location.replace("<?php echo $this->BuildURL('exportXLS').'&type=listing_formation';?>");
	}
	
	</script>

<div id="DivResult" class="boxGen" style="border: 0px;">
	<div class="boxPan" style="padding: 5px; border: 0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			class="result">
			<tr>
				<td colspan="9" class="title"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 3px;">Listing FSE&nbsp; <img class="expand"
					src="./styles/img/csv.gif" title="T�l�charger la version Excel"
					onclick="getexcel();" />
				</td>
			</tr>
		</table>

		<div class="header" style="border-bottom: 3px solid #EAC3C3;">
			<ul>
				<li OnMouseOver="javascript:window.revc.onMouseOver('Results');"
					OnMouseOut="javascript:window.revc.onMouseOut('Results');"><a
					id="aResults" href="javascript:void(0)"
					onclick="javascript:window.revc.onLIClick('Results');"><span
						id="spnResults" class="expand">Liste de r�sultats</span> </a></li>
				<!--  <li OnMouseOver="javascript:window.revc.onMouseOver('Map');"
					OnMouseOut="javascript:window.revc.onMouseOut('Map');"><a id="aMap"
					href="javascript:void(0)"
					onclick="javascript:window.revc.onLIClick('Map');"><span
						id="spnMap" class="expand">Cartographie</span> </a></li> -->
			</ul>
		</div>

		<div style="width: 100%;">
			<div id="Results" style="width: 100%;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="result">
					<?php ob_start(); ?>
					<tr>
						<td class="resultHeader">Nom</td>
						<td class="resultHeader">Pr�nom</td>
						<td class="resultHeader">Genre</td>
						<td class="resultHeader">Tranche d'�ge</td>
						<td class="resultHeader">Sp�cialit�</td>
						<td class="resultHeader">Formateur</td>
						<td class="resultHeader">Type de formation</td>
						<td class="resultHeader">Date</td>
						
						<!-- <td class="resultHeader">Activit�</td>
						<td class="resultHeader">Type</td> -->
					</tr>
					<?php

					$etsIds = null;
					
					for ($i = 0; $i < $ets->Count(); $i++)
					{
						$currentFormation = $ets->items($i);
						$currentParticipants = $currentFormation->getParticipants();
						
						if ($currentParticipants->Count() == 0) { 
							// $this->renderElt($currentFormation, null);
						} else {
							for ($j=0; $j < $currentParticipants->Count(); $j++) {
								$currentParticipant = $currentParticipants->items($j);								
								$this->renderElt($currentFormation, $currentParticipant);
							}
						}												
					}

					echo sprintf('<input type="hidden" value="%s" id="EtsIdList" />', $etsIds);
					$buffer = ob_get_clean();
					?>
					
					<?php echo $buffer; ?>
				</table>
			</div>

			<div id="Map" style="width: 100%; display: none;">
				<div id="googleMap">
					<div class="popupPanel" style="border: none;">
						<div class="bd" style="text-align: center;">
							<br /> <br />
							<iframe id="frmGoogleMaps" frameborder=0 src=""
								style="width: 1024px; height: 850px; border: 0px;"
								scrolling="auto"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
		document.getElementById("spnResults").style.backgroundPosition = "100% -250px";
		document.getElementById("spnResults").style.color = "#FFFFFF";
		document.getElementById("aResults").style.backgroundPosition = "0% -250px";
	</script>

<?php
	}
} 
?>
