<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class MaterielPedagView extends BaseView
{
	private $user = null;
	
	public function Render($materielPedag)
	{
		$this->user = Session::GetInstance()->getCurrentUser();		
?>
	<script>
    function updateMateriel(id, etabId)
    {
    	document.location.href = '<?php echo $this->BuildURL('materielPedagEdit') .'&action=update&updateId='; ?>' + id+'&etabId='+etabId;
    }

    function deleteMateriel(id)
    {
		if (sure("Supprimer la remise de mat�riel ?") == true) 
		{	
			parent.document.location.href = '<?php echo $this->BuildURL('materielPedagEdit') .'&action=delete&etab='.$materielPedag->GetEtablissementId().'&deleteId='; ?>' + id;
		} 
    }

    function ShowHistoriqueDetail()
    {
     	var frm = document.getElementById("frmHistoriqueDetail");
        frm.src = '<?php echo SCRIPTPATH . 'index.php?module=historiquePopup&id='.$materielPedag->GetRemiseMaterielId().'&objet=remisemateriel';?>';              
        Popup.showModal('HistoriqueDetail');
    }
	</script>
	<div id="HistoriqueDetail" class="popup">
		<div class="popupPanel">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" width="1024">
					<tr>
						<td width="95%">Historique : Remise de mat�riel n� <?php echo $materielPedag->GetEtablissementId(); ?></td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('HistoriqueDetail');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd"><iframe id="frmHistoriqueDetail" frameborder=0 src="" style="width:1024px;height:512px;border:0px;"></iframe></div>
		</div>
	</div>      
	<div class="boxGen">
		<div class="boxPan" style="padding:5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Etablissement :</td>
						<td class="detail"><?php echo sprintf('<a class="resultLineRedirection" href="%s&id=%s">%s</a>', $this->BuildURL('etablissementDetail'), $materielPedag->GetEtablissement()->GetEtablissementId(), $materielPedag->GetEtablissement()->GetNom()); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Mat�riel remis (quantit�) :</td>
						<td class="detail">
						<?php 	
							$materiaux = $materielPedag->getMateriaux();													
								
							for($b=0; $b<$materiaux->count(); $b++)
							{
								echo Dictionnaries::GetMaterielPedag($materiaux->items($b)->getMaterielId())->items(0)->getLabel().'('.$materiaux->items($b)->getQuantite().')<br/>';
								
							}
													
						?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Contact :</td>
						<td class="detail"><?php if($materielPedag->GetContact()) echo '<a class="resultLineRedirection" href="' . $this->BuildURL('contactDetail') . '&id=' . $materielPedag->GetContact()->GetContactId() . '">' .  $materielPedag->GetContact()->GetPrenom() . ' ' . $materielPedag->GetContact()->GetNom() . '&nbsp;(' . ($materielPedag->getContactEtablissement()->getTitreContact()!=null?$materielPedag->getContactEtablissement()->getTitreContact()->GetLabel():'/') . ')</a>'; ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Cr�ation :</td>
						<td class="detail"><?php echo $materielPedag->getCreationString(); ?></td>
					</tr>
					</table>
				</td>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Date :</td>
						<td class="detail"><?php echo $materielPedag->GetDate(true); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
	
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">&nbsp;</td>
						<td class="detail" style="background-color:inherit;">&nbsp;</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Modification :</td>
						<td class="detail"><?php echo $materielPedag->getUpdatedString(); ?></td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</div>
			<div class="boxBtn">
				<table class="buttonTable">
					<tr>
						<td onclick="updateMateriel('<?php echo $materielPedag->GetRemiseMaterielId();?>','<?php echo $materielPedag->GetEtablissementId();?>')">Modifier</td>
						<td class="separator"></td>
						<td onclick="deleteMateriel('<?php echo $materielPedag->GetRemiseMaterielId();?>')">Supprimer</td>
						<td class="separator"></td>
					</tr>
				</table>
			</div>
		
	</div>
<?php
	}
}
?>
