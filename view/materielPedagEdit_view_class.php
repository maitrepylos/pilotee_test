<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class MaterielPedagEditView extends BaseView
{
private $user = null;
	
public function render($boUpdate=false)
{
	$this->user = Session::GetInstance()->getCurrentUser();
		
	?>
	<?php if($boUpdate){?>
	<br/>
	<span style="padding-left: 5px;" class="title">&gt;&gt; Modifier la remise de mat�riel</span>
	<br/><br/>
	<?php }?>
	<script>
		function materielPedagEditViewClass() {}
		materielPedagEditViewClass.prototype.Close = function(action,operation)
		{
			if(action=='valider')
			{
				var res1 = true;
				var dateRemise = document.getElementById('dateRemise');
				
				
				var chk = new CheckFunction();
				
				
				if (isDate(dateRemise.value)==false)
				{
					SetInputError(dateRemise);
					res1=false;
				}
				else
				{
					SetInputValid(dateRemise);
				}

				var nbLigneMat = document.getElementById('nbLigneMat');
				nbLigneMat.value++;
				for(i=0; i<nbLigneMat.value; i++)
				{
					materiel = document.getElementById('materiel_'+i);
					if(materiel)
					{
						quantite = document.getElementById('quantite_'+i);
						chk.Add(quantite, 'INT', true);
				
						if(materiel.value == '-1')
						{
							SetInputError(materiel);
							res1 = false;
						}else 
							SetInputValid(materiel);
					}
				}

				var nbLigne = document.getElementById('nbLignesContact');
				var nbC = 0;
				for(i=0; i<nbLigne.value; i++)
				{
					var name = document.getElementById('contactId_'+i);
					if(name)
					{
						nbC ++;
					}
				}

				if(nbC==0 || nbC>1)
				{
					//alert('La remise de mat�riel doit �tre li�e � au moins un contact.');
					alert('La remise de mat�riel doit �tre li�e � un et un seul contact.');
					res1=false;
				}

				var res = chk.IsValid();
				if(res && res1)
				{
					var urlAction = '<?php echo $this->BuildURL($_REQUEST['module']);  ?>';
					if(operation=='add')
						urlAction += "&action=add";
					else
						urlAction += "&action=update";

					submitForm('materielPedagEdit');

				}
			}
			else
			{
				parent.window.location.href = "<?php echo SCRIPTPATH . 'index.php?module=etablissementDetail&id='.$_REQUEST['etabId']; ?>";
				parent.Popup.hide("formAddRemise");
			}
		}
		window.matV = new materielPedagEditViewClass();
	
		function ShowContactList(closeCallBack)
		{
			var frm = document.getElementById("frmContacts");
			frm.src = 'about:blank';
			var url = '<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=contactList&etabs='.$_REQUEST['etabId']; ?>';
			//var frm = document.getElementById("frmContacts");
			frm.src = url+closeCallBack;
			Popup.showModal('contacts');						
		}

		function SetEtablissementContact(objectsArray, frmContent)
		{
			if (objectsArray.length > 0)		  
			{
				var nbLignes = 0;
				var divGlobal = frmContent.getElementById("divEtablissementsContact");	
				var tab = frmContent.getElementById('tableEtablissementsContact');
				
				if(tab==null)
				{
					frmContent.getElementById('etabIds').value = '';
	 				tab = divGlobal.appendChild(frmContent.createElement("table"));
	 				tab.setAttribute("id","tableEtablissementsContact");
	 				tab.setAttribute("style","width:80%");
				
		 			//Premi�re ligne du tableau
	 				newRow = tab.insertRow(-1);
	 				newRow.setAttribute("id",-1);
	 				newCell = newRow.insertCell(0);
	 				newCell.innerHTML = "<center>�tablissement</center>";
	 			
	 				newCell = newRow.insertCell(1);
	 				newCell.innerHTML = "<center>Titre</center>";
	 			
	 				style='';
					<?php if($this->user->isOperateur()){ ?>
		 				style = "display:none";
		 			<?php } ?>
	 				newCell = newRow.insertCell(2);
	 				newCell.innerHTML = "<center>Participation</center>";
	 				newCell.setAttribute('style',style);
		 			newCell = newRow.insertCell(3);
	 				newCell.innerHTML = "<center>Sp�cialit�</center>";
	 			}
	 			else
	 			{
					nbLignes = frmContent.getElementById("nbLignes").value;
			}	
			 autoid = nbLignes;
			 for(j=0;j<objectsArray.length;j++)
			 {
				var etabSelect = frmContent.getElementById("etabIds");
				var reg=new RegExp("[;]+", "g");
				tableEtabSelect = etabSelect.value.split(reg);
				var isPresent = false;					
				for(var x=0; x<tableEtabSelect.length; x++)
				{
					if(tableEtabSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
					
				if(!isPresent)
				{
					etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
	 				newRow = tab.insertRow(-1);
					newRow.setAttribute("id",nbLignes);

					 for(i=0;i<5;i++)
	 		 		 { 		 		 
	 	 			 	newCell = newRow.insertCell(i);
	 	 			 	var tmp = "";
						switch(i)
	 					{
	 						case 0 :
	 							tmp = '<input type="text" name="etabName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
	 								  '<input name="etabId_'+nbLignes+'" id="etabId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
	 							newCell.innerHTML = tmp;
	 							break;
							case 1 :
							    tmp = "<?php echo createSelectOptions(Dictionnaries::getTitreContactList());?>";
								newCell.innerHTML = "<select name='titre_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
								break;
							case 2 :
								style='';
								<?php if($this->user->isOperateur()){ ?>
	 								style = "display:none";
								<?php } ?>
	 							newCell.setAttribute('style',style);
						    	tmp = "<?php echo createSelectOptions(Dictionnaries::getParticipationList());?>";
								newCell.innerHTML = "<select name='participation_"+nbLignes+"' style='width:175px;'>"+
													"<option value='-1'></option>"+tmp+"</select>";
								break;
	 						case 3 :
								    tmp = "<?php echo createSelectOptions(Dictionnaries::getSpecialiteList());?>";
								newCell.innerHTML = "<select name='specialite_"+nbLignes+"' style='width:175px;'>"+
														"<option value='-1'></option>"+tmp+"</select>";
								break;
	 						case 4 :
								newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\""+nbLignes+"\", \"nbLignes\")'/>";
	 							break; 							
	 					}
	 				
	 					with(this.newCell.style)
						{	
					 		width = '100px';
					 		textAlign = 'center';
					 		padding = '5px';
						} 
						autoid++;
				 	}
				 	nbLignes++;
				 }
			  }
			  frmContent.getElementById("nbLignes").value = nbLignes;
		   }
		}

		function removeRowMat(id, nbLigneId)
		{
			
			elt=document.getElementById(id);
			var p = elt.parentNode;
			var nbLigneMat = document.getElementById('nbLigneMat');
			var nb = 0;
			for(i=0; i<=nbLigneMat.value; i++)
			{
				materiel = document.getElementById('materiel_'+i);
				if(materiel)
				{
					nb++;
				}
			}
			
			if(nb>1)			
				p.removeChild(elt);

		}

		
		function SetContacts (objectsArray, close)
		{
			if (objectsArray.length > 0)		  
			{
				var nbLignes = 0;
				var divGlobal = document.getElementById("divContacts");
				var tab = document.getElementById('tableContacts');

				if(tab==null)
				{
					document.getElementById('contactIds').value = '';
	 				tab = divGlobal.appendChild(document.createElement("table"));
	 				tab.setAttribute("id","tableContacts");
	 				newRow = tab.insertRow(-1);
	 				newRow.setAttribute("id","tr_head_contact");
	 				newCell = newRow.insertCell(0);
	 				newCell = newRow.insertCell(1);
	 			}
	 			else
	 				nbLignes = document.getElementById("nbLignesContact").value;
 				
				autoid = nbLignes;		
				for(j=0;j<objectsArray.length;j++)
				{
					var etabSelect = document.getElementById("contactIds");
					var reg=new RegExp("[;]+", "g");
					tableEtabSelect = etabSelect.value.split(reg);
					var isPresent = false;
					for(var x=0; x<tableEtabSelect.length; x++)
					{
						if(tableEtabSelect[x] == objectsArray[j].id)
						{
							isPresent = true;
							break;
						}
					}
					if(!isPresent)
					{
						etabSelect.value += (etabSelect.value == "" ? "" : ";") + objectsArray[j].id;
	 					newRow = tab.insertRow(-1);	 				
		 				newRow.setAttribute("id","id_contact_tr_"+nbLignes);
		 				
						 for(i=0;i<3;i++)
	 			 		 { 		 		 
	 		 			 	newCell = newRow.insertCell(i);
	 		 			 	var tmp = "";
		 					switch(i)
	 						{
	 							case 0 :
	 								tmp = '<input type="text" name="contactName_'+nbLignes+'" style="width:300px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />'+
	 									  '<input name="contactId_'+nbLignes+'" id="contactId_'+nbLignes+'" type="hidden" value="'+objectsArray[j].id+'" />';
	 								newCell.innerHTML = tmp;
	 								break;
	 							case 1 :
									newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"id_contact_tr_"+nbLignes+"\", \"nbLignesContact\")'/>";
	 								break; 							
	 						}
	 					
	 						with(this.newCell.style)
							{	
						 		width = '100px';
						 		textAlign = 'center';
						 		padding = '5px';
							} 
	 						autoid++;
	 				 	}
	 				 	nbLignes++;
	 				 }				

				}
				 document.getElementById("nbLignesContact").value = nbLignes;
			}
			if(close)Popup.hide('contacts');
		}	
		function ShowContactForm(closeCallBack)
		{
			
			var frm = document.getElementById("frmFormAddContact");
			Popup.showModal('formAddContact');
			var ets = new Array();
			if(parent.parent.document.getElementById("etablissementId"))
			{
				var id = parent.parent.document.getElementById("etablissementId").value;
				var nom = parent.parent.document.getElementById("etablissementNom").value;
			}
			else
			{
				var id = document.getElementById("etablissementId").value;
				var nom = document.getElementById("etablissementNom").value;
			}
			ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
			frmContent = frm.contentWindow.document || frm.contentDocument;
			
			
			if(ets.length>0)
			{
				SetEtablissementContact(ets, frmContent);
			}
			else
			{
				frmContent.getElementById('divEtablissementsContact').innerHTML = '';
			}
		}

		function addMateriel()
		{
			nbLigne = document.getElementById('nbLigneMat').value;
			document.getElementById('nbLigneMat').value++;
			nbLigne++;
			var newRow = document.getElementById('matTable').insertRow(3);
			newRow.setAttribute("id","mat_"+nbLigne);
			var newCell = newRow.insertCell(0);
			if (document.all) 
			{
				newCell.setAttribute('className','label');
			}
			else
			{
				newCell.setAttribute('class','label');
			}
			newCell.innerHTML = 'Mat�riel :';
			newCell = newRow.insertCell(1);
			var temp = "&nbsp;<select name='materiel_"+nbLigne+"' id='materiel_"+nbLigne+"' style='width:178px;'>";
			temp+="<option value='-1' selected='selected'></option>";
			temp+="<?php echo createSelectOptions(Dictionnaries::GetMaterielPedag()); ?>";
			temp+="</select>";
			temp+=" <span style='color:gray;font-weight:bold;'>Qte : </span>";

			temp+=" <input type='text' id='quantite_"+nbLigne+"' name='quantite_"+nbLigne+"' value='' style='width:175px;'></input> <img class='expand' onclick='addMateriel();' src='./styles/img/plus.gif'></img> <img class='expand' onclick='removeRowMat(\"mat_"+nbLigne+"\",\"nbLigneMat\");' src='./styles/img/close_panel.gif'></img>";
			newCell.innerHTML = temp;
		}
	</script>
	<div id="contacts" class="popup" style="width:984px">
		<div class="popupPanel" style="width:984px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:974px">
					<tr>
						<td width="95%">Liste des contacts</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('contacts');reloadFrameBlank('frmContacts');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:984px"><iframe id="frmContacts" frameborder=0 src="" style="width: 984px; height: 512px; border: 0px;"></iframe></div>
		</div>
	</div>
	<div id="formAddContact" class="popup" style="width:944px">
		<div class="popupPanel" style="width:944px">
			<div class="popupPanelHeader">
				<table cellspacing="0" cellspacing="0" border="0" style="width:934px">
					<tr>
						<td width="95%">Cr�er un contact</td>
						<td width="5%" align="right"><a href="javascript:void(0);" onclick="javascript:Popup.hide('formAddContact');reloadFrameBlank('frmFormAddContact');"><img class="popupClose" width="16" src="./styles/img/close_panel.gif"></img></a></td>
					</tr>
				</table>
			</div>
			<div class="bd" style="width:944px">
				<iframe id="frmFormAddContact" frameborder=0 src="<?php echo SCRIPTPATH . 'index.php?module=contactPopup&action=add&exitPopup=ok&etabs='.$_REQUEST['etabId']; ?>" style="width: 944px; height: 512px; border: 0px;"></iframe>
			</div>
		</div>
	</div>	
	<form id="materielPedagEdit" action="" method="post">
		<input type="hidden" id="materielPedagId" name="materielPedagId" value="<?php if(isset($_POST['materielPedagId']))echo $_POST['materielPedagId']; ?>" />
		<input type="hidden" id="etablissementId" name="etablissementId" value="<?php if(isset($_POST['etablissementId']))echo $_POST['etablissementId']; ?>" />
		<input type="hidden" id="etablissementNom" name="etablissementNom" value="<?php if(isset($_POST['etablissementNom']))echo $_POST['etablissementNom']; ?>" />		
		<input type="hidden" id="nbLigneMat" name="nbLigneMat" value="<?php if(isset($_POST['nbLigneMat']))echo $_POST['nbLigneMat']; else echo '1';?>" />
		
		<div class="boxGen">
			<div class="boxPan" style="padding:5px;">
				<table cellpadding="0" id="matTable" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Date :</td>
						<td class="detailEdit">
							<input type="text" id="dateRemise" maxlength="10" name="dateRemise" value="<?php if(isset($_POST['dateRemise']))echo $_POST['dateRemise']; ?>" style="width:175px;"></input>
							<img src='./styles/img/calendar3.gif' id='date_ante_pic'  class="expand" title='Calendrier' />
							<img src="./styles/img/eraser.gif" onclick="document.getElementById('dateRemise').value='';" class="expand" title = "vider"/>
					            <script type='text/javascript'>
 										Calendar.setup({
        									inputField     :    'dateRemise',     
        									ifFormat       :    '%d/%m/%Y',      
        									button         :    'date_ante_pic',  
        									align          :    'Tr'            
										});
								</script>								
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<?php if((!isset($_POST['nbLigneMat']))){?>
					<tr id="mat_0">
						<td class="label">Mat�riel :</td>
						<td class="detailEdit">  
						
							<select name="materiel_0" id="materiel_0" style="width:178px;">
								<?php
									echo '<option value="-1" selected="selected"></option>';
									if (isset($_POST['materiel'])) {
										echo createSelectOptions(Dictionnaries::GetMaterielPedag(), $_POST['materiel']);
									}
									else {
										echo createSelectOptions(Dictionnaries::GetMaterielPedag());
									}
								?>
							</select>
							<span style="color:gray;font-weight:bold;">Qte : </span>
							<input type="text" id="quantite_0" name="quantite_0" value="" style="width:175px;"></input> <img class="expand" onclick="addMateriel();" src="./styles/img/plus.gif"></img>
							<img src="./styles/img/close_panel.gif" onclick="removeRowMat('mat_0','nbLigneMat');" class="expand">
						</td>
					</tr>
					<?php }else $this->createTableMateriel();?>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr valign="top">
						<td class="label">Contact :</td>
						<td class="detailEdit">
							<img src="./styles/img/add_contact.gif" id="searchContacts" onclick="ShowContactList('&close=SetContacts');return false" title="Rechercher un contact" class="expand" />
							&nbsp;&nbsp;/&nbsp;&nbsp;<img src="./styles/img/edit_group.gif" id="createContact" onclick="ShowContactForm('&close=SetContacts');return false" title="Cr�er un contact" class='expand' />
						</td>
					</tr>
					<tr>
						<td class="label"></td>
						<td class="detailEdit">
							<input type="hidden" id="contactIds" name="contactIds" value="<?php isset($_POST['contactIds']) ? $_POST['contactIds'] : null ?>">
							<?php $nbL=0; if(isset($_POST['nbLignesContact'])) $nbL = $_POST['nbLignesContact'];?>
							<input name='nbLignesContact' id='nbLignesContact' type='hidden' value='<?php echo $nbL ?>' />
							<div id="divContacts">
								<?php 
								if((isset($_POST['nbLignesContact']) && $_POST['nbLignesContact']>0))
									$this->createTableContact();
								?>
							</div>
						</td>
					</tr>				
				</table>
			</div>
			<?php if($boUpdate){ $operation = 'update';?>
				<input type="hidden" name="updateForm" />
			<?php }else{$operation = 'add';?>
				<input type="hidden" name="addForm" />
			<?php }?>
			<div class="boxBtn" style="padding-top:5px;text-align:left;">
				<table class="buttonTable" border="0" align="left">
					<tr>
						<?php if(!$boUpdate && !isset($_POST['addForm']) || $boUpdate){?>
						<td onclick="window.matV.Close('valider','<?php echo $operation?>')">Sauvegarder</td>
						<?php }?>
						<?php if($_REQUEST['module']=='materielPedagEditPopup'){?>
						<td class="separator"></td>
						<td onclick="window.matV.Close('fermer', '<?php echo $operation?>' )">Fermer</td>
						<?php }?>
					</tr>
				</table>
			</div>
		</div>		
	</form>
	<?php 	
}


public function createTableMateriel()
{
	$nbLignes = $_POST['nbLigneMat'];
	for($i=0; $i<$nbLignes; $i++)
	{
		if(isset($_POST['materiel_'.$i]))
		{	
			echo '<tr id="mat_'.$i.'">';
			echo '<td class="label">Mat�riel :</td>';
			echo '<td class="detailEdit">';  
			echo '<select name="materiel_'.$i.'" id="materiel_'.$i.'" style="width:178px;">';
			echo '<option value="-1" selected="addMaterielselected"></option>';
			echo createSelectOptions(Dictionnaries::GetMaterielPedag(), $_POST['materiel_'.$i]);
			echo '</select>';						
			echo '<span style="color:gray;font-weight:bold;">Qte : </span>';
			if($i==0)
			{
				echo '<input type="text" id="quantite_'.$i.'" name="quantite_'.$i.'" value="'.$_POST['quantite_'.$i].'" style="width:175px;"></input> <img class="expand" onclick="addMateriel();" src="./styles/img/plus.gif"></img> <img class="expand" onclick="removeRowMat(\'mat_'.$i.'\',\'nbLigneMat\');" src="./styles/img/close_panel.gif"></img>';
			}
			else
			{
				echo '<input type="text" id="quantite_'.$i.'" name="quantite_'.$i.'" value="'.$_POST['quantite_'.$i].'" style="width:175px;"></input> <img class="expand" onclick="addMateriel();" src="./styles/img/plus.gif"></img> <img class="expand" onclick="removeRowMat(\'mat_'.$i.'\',\'nbLigneMat\');" src="./styles/img/close_panel.gif"></img>';
			}
			echo '</td>';
			echo '</tr>';
		}
	}
}

public function createTableContact()
{
		$idTable = 'tableContacts';
		$idHeadTr = 'tr_head_contact';
		$headLabel='Contact';
		$nbLignes = $_POST['nbLignesContact'];
		?>
		<table id="<?php echo $idTable; ?>" style="width: 20%;">
			<tbody>
				<tr id="<?php echo $idHeadTr; ?>">
					<td></td>
					<td></td>
				</tr>
				<?php
				for($i=0; $i<$nbLignes; $i++)
				{
					if(isset($_POST['contactName_'.$i]))
					{
						echo '<tr id="id_contact_tr_'.$i.'"><td style="padding: 5px; width: 100px; text-align: center;">';
						echo '<input type="text" readonly="readonly" value="'.$_POST['contactName_'.$i].'" style="width: 300px;" name="contactName_'.$i.'"/>';
						echo '<input type="hidden" value="'.$_POST['contactId_'.$i].'" id="contactId_'.$i.'" name="contactId_'.$i.'"/>';
						echo '</td>';
						echo '<td style="padding: 5px; width: 100px; text-align: center;">';
						echo '<img src="./styles/img/close_panel.gif" title="Supprimer" class="expand" onclick="removeRow(\'id_contact_tr_'.$i.'\', \'nbLignesContact\')"/>';
						echo '</td>';
						echo'</tr>';
					}
				}
				?>
			</tbody>
		</table>
	<?php
	}
}
?>