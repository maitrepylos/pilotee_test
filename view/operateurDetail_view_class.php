<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class OperateurDetailView extends BaseView
{
	private $user = null;
	
	public function Render($operateur)
	{
		$this->user = Session::GetInstance()->getCurrentUser();
		$adr = $operateur->GetAdresse();
		$actions = $operateur->GetActions();
		$cp = null;
		if($adr!=null)
			$cp = $adr->GetCodePostal();			
?>
	<div class="boxGen">
	<script language="javascript" type="text/javascript">
		function ShowActionInfo(actionId)
		{
			window.location = '<?php echo $this->BuildURL('actionDetail') . '&id='; ?>' + actionId;
		}
	
		function ShowEtablissementInfo(id)
		{
			window.location = '<?php echo $this->BuildURL('etablissementDetail') . '&id='; ?>' + id;
		}
	</script>	
		<div class="boxPan" style="padding:5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Nom :</td>
						<td class="detail"><?php echo $operateur->GetNom();?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Adresse :</td>
						<td class="detail">
							<?php
								if($adr!=null)
									echo $adr->GetRue();
							?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">T�l�phone(s) :</td>
						<td class="detail">
							<?php
							if($adr!=null)
							{
								$tel1 = $adr->GetTel1();
								$tel2 = $adr->GetTel2();
								$tel = null;
								if (isset($tel1) && isset($tel2)) $tel = sprintf('%s / %s', $tel1, $tel2);
								elseif (isset($tel1)) $tel = $tel1;
								elseif (isset($tel2)) $tel = $tel2;
								echo $tel;
							}
							?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Couriel(s) :</td>
						<td class="detail">
							<?php
							if($adr!=null)
							{							
								$email1 = $adr->GetEmail1AsAnchor();
								$email2 = $adr->GetEmail2AsAnchor();

								$email = null;
								
								if (isset($email1) && isset($email2)) $email = sprintf('%s / %s', $email1, $email2);
								elseif (isset($email1)) $email = $email1;
								elseif (isset($email2)) $email = $email2;
								
								echo $email;
							}
							?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Site internet :</td>
						<td class="detail">
							<?php
								 echo $operateur->GetWebsiteAsAnchor();
							?>
						</td>
					</tr>					
  				 </table>
				</td>
				<td width="50%" style="vertical-align:top;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="record">
					<tr>
						<td class="label">Statut :</td>
						<td class="detail" style="color:<?php echo $operateur->GetSwActifColor(); ?>"><?php echo $operateur->GetSwActifLabel(); ?></td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">Code postal / Localit� :</td>
						<td class="detail">
							<?php
							if($adr!=null){
								echo $cp->GetCodePostal(); ?>&nbsp;<?php echo $adr->GetVille();
							}
							?>
						</td>
					</tr>	
					<tr><td colspan="2" class="separator"></td></tr>	
					<tr>
						<td class="label">Arrondissement / Province :</td>
						<td class="detail">
							<?php
							if($cp!=null)
								echo $cp->GetArrondissement()->getLabel() . " / " . $cp->GetProvince()->getLabel(); 
							?>
						</td>
					</tr>
					<tr><td colspan="2" class="separator"></td></tr>
					<tr>
						<td class="label">T�l�copie :</td>
						<td class="detail">
							<?php
							if($adr!=null)
								echo $adr->GetFax();
							?>
						</td>
					</tr>					
  				  </table>
				</td>
			</tr>
			<?php if($actions->Count()>0){ ?>
			<tr>
				<td colspan="2">
					<div class="header" style="border-bottom:3px solid #EAC3C3;">
						<ul>
							<li><a href="javascript:void(0)" onclick="javascript:window.edvc.onLIClick('Action');"><span class="expand">Actions</span></a></li>
						</ul>
					</div>
					<div style="margin-top:5px;">
						<div id="Action">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<col width="20%" />
							<col width="80%" />
							<tr>
								<td colspan="2" style="border:1px solid #EAC3C3;padding:5px;">
									<table class="result" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:white;">
										<colgroup>
											<col width="200" />
											<col width="150" />
											<col width="250" />
											<col width="250" />
											<col width="*" />
										</colgroup>
										<?php
											echo '<tr>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Ann�e scolaire (Date)</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Type d\'action</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">Action</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">R�f. chez l\'op�rateur</td>';
												echo '<td class="resultHeader" style="padding-top:5px;padding-bottom:5px;">�tablissements</td>';
												
											echo '</tr>';
											
											//echo '<tr><td>&nbsp;</td></tr>';
											
											for($i=0; $i<$actions->Count(); $i++)
											{
												$etabs = $actions->items($i)->GetActionEtablissment();
												$nomSpec = '';
												if($actions->items($i)->GetNomSpec()!=null)
													$nomSpec = '('.$actions->items($i)->GetNomSpec().')';
											
												echo '<tr class="trResult">';
													echo '<td valign = "top" class="resultLine">'. $actions->items($i)->GetAnneeId().' '.$actions->items($i)->GetDateActionString().'</td>';
													echo '<td valign = "top" class="resultLine">'. $actions->items($i)->GetTypeActionLabel().'</td>';
													echo '<td valign = "top" class="resultLine resultLineRedirection" onclick="ShowActionInfo(\''.$actions->items($i)->GetActionId().'\');">'. $actions->items($i)->GetNomGen().' '.$nomSpec.'</td>';
													
													if($actions->items($i)->GetRefChezOperateur()!=null)
													{	
														echo'<td valign = "top" class="resultLine ">'. ToHTML($actions->items($i)->GetRefChezOperateur()).'</td>';
													}
													else 
														echo'<td valign = "top" class="resultLine">/</td>';
			
													if($etabs->Count()>0)
													{
														echo '<td valign = "top" class="resultLine">';
														for($x=0; $x<$etabs->Count(); $x++)
														{
															echo '<span class="resultLineRedirection" onclick="ShowEtablissementInfo(\''.$etabs->items($x)->GetEtablissementId().'\');">'. $etabs->items($x)->GetNom().'</span>';
															if($x<$etabs->Count()-1)
																echo ',';			
															echo '<br/>';													  	
														}
														echo '</td>';
													}
													else	
														echo'<td valign = "top" class="resultLine">/</td>';
												echo '</tr>';	
											}
										?>
									</table>
								</td>
							</tr>
							</table>
							<p style="height:5px;font-size:5px;">&nbsp;</p>
						</div>	
					</div>					
				</td>
			</tr>
			<?php } ?>
		  </table>
		</div>
	</div>
<?php
	}
}
?>
