<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class OperateurView extends BaseView
{

	public function viewOperateurList($ops, $closeCallBack)
	{
		?>
		
			<script language="javascript" type="text/javascript">
			function OperateurListViewClass() {}

			OperateurListViewClass.prototype.Close = function(action)
			{
				var ets = new Array();
				
				var chks = document.getElementsByTagName("input");

				if (action == 'select')
				{
					for (var i = 0; i < chks.length; i++)
					{
						if (chks[i].name.match("^chk_op_"))
						{
							if (chks[i].checked)
							{
								var id = chks[i].name.replace("chk_op_", "");
								var nom = document.getElementById("chk_op_name_" + id).innerHTML;
								ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
							}
						}
					}
				}
				
				<?php echo 'parent.' . $closeCallBack . "(ets, true);"; ?>
			}

			window.opL = new OperateurListViewClass();
			</script>
				<div class="boxGen">
					<div class="boxPan" style="padding: 5px;">
						<table   cellspacing="0"  cellpadding="2" border="0" style="margin-bottom: 4px; margin:10px;" class="result">
						  <tr>
						  	 <td class="resultHeader" width="10px"></td>
							 <td class="resultHeader" width="100%">Nom</td>
						   </tr>
						   <?php 
						   for($i=0; $i<$ops->count(); $i++)
						   {	
						   		echo '<tr>';
						   			echo sprintf('<td><input type="checkbox" name="chk_op_%s"></td>', $ops->items($i)->GetOperateurId());
						   			echo '<td id="chk_op_name_'.$ops->items($i)->GetOperateurId().'">'.$ops->items($i)->GetNom().'</td>';
						   		echo '</tr>';
						   }
						   
						   
						   ?>
						  </table>
					</div>
				</div>
				
				
				<div class="boxBtn">
					<table class="buttonTable">
						<tr>
							<td onclick="javascript:window.opL.Close('select');" >Selectionner</td><td class="separator"></td>
							<td onclick="javascript:window.opL.Close('close');">Fermer</td>
						</tr>
					</table>
				</div>
				
<?php 
	}
	
	
	
	public function uploader($error)
	{
		?>
		<div class="boxGen">
		 <br/>
		<?php echo $this->Errors($error); ?>
		<form id="importForm" action="<?php echo $this->BuildURL('import') ?>" method="post" enctype="multipart/form-data">			
			<div class="boxPan">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
				   <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="130"><label for="annexe">Fichier (*) </label></td>
						<td>:</td>
						<td><input type="file" name="annexe" id="annexe"/></td>
					</tr>
				    <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>						
				</table>
			</div>
			<input type="hidden" name="searchAction"/>
			<div class="boxBtn">
				<input type="hidden" name="upload"/>
				<table class="buttonTable">
					<tr>
						<td onclick="submitForm('importForm')" >Importer</td>
					</tr>
				</table>
			</div>			
		</form>
		</div>
		 <?php
	}
	
	
	
	
}
?>