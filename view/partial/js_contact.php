function ShowFormAddContactFormation()
{
	var formations = new Array();
	var chksformations = document.getElementsByTagName("input");
	for (var i = 0; i < chksformations.length; i++)
	{
		if (chksformations[i].name.match("^chk_formation_"))
		{
			if (chksformations[i].checked)
			{
				var id = chksformations[i].name.replace("chk_formation_", "");
				var nom = document.getElementById("chk_formation_name_" + id).innerHTML;
				formations.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
			}
		}
	}
	var frm = document.getElementById("frmFormAddContact");
			
	Popup.showModal('formAddContact');
	frmContent = frm.contentWindow.document || frm.contentDocument;
	
	if(formations.length>0)
	{
		SetFormationContact(formations, frmContent);
	}
	else
	{
		frmContent.getElementById('divFormationContact').innerHTML = '';
		
	}
}

function ShowFormAddContact()
{
	var ets = new Array();
	var chks = document.getElementsByTagName("input");
	for (var i = 0; i < chks.length; i++)
	{
		if (chks[i].name.match("^chk_action_"))
		{
			if (chks[i].checked)
			{
				var id = chks[i].name.replace("chk_action_", "");
				var nom = document.getElementById("chk_action_name_" + id).innerHTML;
				ets.push(eval("t={id:" + id + ",text:'" + escape(nom) + "'};"));
			}
		}
	}
	var frm = document.getElementById("frmFormAddContact");
			
	Popup.showModal('formAddContact');
	frmContent = frm.contentWindow.document || frm.contentDocument;
	
	if(ets.length>0)
	{
		SetActionContact(ets, frmContent);
	}
	else
	{
		frmContent.getElementById('divActionContact').innerHTML = '';
	}

}

	function SetActionContact(objectsArray, frmContent)
	{
		
	  if (objectsArray.length > 0)		  
	  {
	  	var nbLignes = 0;
		var divGlobal = frmContent.getElementById('divActionContact');			
		var tab = frmContent.getElementById('tableActionsContact');
		if(tab==null)
		{
				tab = divGlobal.appendChild(frmContent.createElement('table'));
				tab.setAttribute('id','tableActionsContact');
				tab.setAttribute('style','width:80%');
				frmContent.getElementById("nbLignesAction").value = '0';
				frmContent.getElementById('actionIds').value = '';
				
				newRow = tab.insertRow(-1);
				newRow.setAttribute('id','headAction_-1');
				newCell = newRow.insertCell(0);
				newCell.innerHTML = "<center>Action</center>";
				newCell = newRow.insertCell(1);
				newCell.innerHTML = "<center>R�le</center>";
				newCell = newRow.insertCell(2);
				newCell.innerHTML = "";
			}
			else
			{
				nbLignes = frmContent.getElementById("nbLignesAction").value;
			}	

			autoid = nbLignes;
		
	 		for(j=0;j < objectsArray.length;j++)
			{
				var actionSelect = frmContent.getElementById("actionIds");
				var reg=new RegExp("[;]+", "g");
				tableActionSelect = actionSelect.value.split(reg);
				var isPresent = false;					
				for(var x=0; x < tableActionSelect.length; x++)
				{
					if(tableActionSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
			
				if(!isPresent)
				{
					actionSelect.value += (actionSelect.value == "" ? "" : ";") + objectsArray[j].id;
					newRow = tab.insertRow(-1);
 					newRow.setAttribute('id','action_'+nbLignes);

				 	for(i=0;i<5;i++)
			 		{ 		 		 
		 				newCell = newRow.insertCell(i);
		 			 	var tmp = "";
 						switch(i)
						{
							case 0 :
						    	tmp = '<input type="text" name="actionName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />';
				    	tmp += "<input name='actionId_"+nbLignes+"' id='actionId_"+nbLignes+"' type='hidden' value='"+objectsArray[j].id+"' />";
						newCell.innerHTML = tmp;
						break;
					case 1 :
					    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>";
						newCell.innerHTML = "<select name='roleAction_"+nbLignes+"' id='roleAction_"+nbLignes+"' style='width:175px;'>"+
										"<option value='-1'></option>"+tmp+"</select>";
						break;
					case 2 :
						newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"action_" + nbLignes + "\", \"nbLignesAction\")'/>";
							break; 							
					}
				
					with(this.newCell.style)
					{	
			 			width = '100px';
			 			textAlign = 'center';
			 			padding = '5px';
					} 
					autoid++;
			 	}
			 	nbLignes++;
			 }
		  }
 		frmContent.getElementById("nbLignesAction").value = nbLignes;
   }
}

	function SetFormationContact(objectsArray, frmContent)
	{
		
	  if (objectsArray.length > 0)		  
	  {
	  	var nbLignes = 0;
		var divGlobal = frmContent.getElementById('divFormationContact');			
		var tab = frmContent.getElementById('tableFormationsContact');
		if(tab==null)
		{
				tab = divGlobal.appendChild(frmContent.createElement('table'));
				tab.setAttribute('id','tableFormationsContact');
				tab.setAttribute('style','width:80%');
				frmContent.getElementById("nbLignesFormation").value = '0';
				frmContent.getElementById('formationIds').value = '';
				
				newRow = tab.insertRow(-1);
				newRow.setAttribute('id','headFormation_-1');
				newCell = newRow.insertCell(0);
				newCell.innerHTML = "<center>Formation</center>";
				newCell = newRow.insertCell(1);
				newCell.innerHTML = "<center>R�le</center>";
				newCell = newRow.insertCell(2);
				newCell.innerHTML = "";
			}
			else
			{
				nbLignes = frmContent.getElementById("nbLignesFormation").value;
			}	

			autoid = nbLignes;
		
	 		for(j=0;j < objectsArray.length;j++)
			{
				var actionSelect = frmContent.getElementById("formationIds");
				var reg=new RegExp("[;]+", "g");
				tableActionSelect = actionSelect.value.split(reg);
				var isPresent = false;					
				for(var x=0; x < tableActionSelect.length; x++)
				{
					if(tableActionSelect[x] == objectsArray[j].id)
					{
						isPresent = true;
						break;
					}
				}
			
				if(!isPresent)
				{
					actionSelect.value += (actionSelect.value == "" ? "" : ";") + objectsArray[j].id;
					newRow = tab.insertRow(-1);
 					newRow.setAttribute('id','formation_'+nbLignes);

				 	for(i=0;i<5;i++)
			 		{ 		 		 
		 				newCell = newRow.insertCell(i);
		 			 	var tmp = "";
 						switch(i)
						{
							case 0 :
						    	tmp = '<input type="text" name="formationName_'+nbLignes+'" style="width:200px"   value="'+unescape(objectsArray[j].text)+'" readonly="readonly" />';
				    	tmp += "<input name='formationId_"+nbLignes+"' id='formationId_"+nbLignes+"' type='hidden' value='"+objectsArray[j].id+"' />";
						newCell.innerHTML = tmp;
						break;
					case 1 :
					    tmp = "<?php echo createSelectOptions(Dictionnaries::getRoleContactActionList());?>";
						newCell.innerHTML = "<select name='roleFormation_"+nbLignes+"' id='roleFormation_"+nbLignes+"' style='width:175px;'>"+
										"<option value='-1'></option>"+tmp+"</select>";
						break;
					case 2 :
						newCell.innerHTML = "<img src='./styles/img/close_panel.gif' title='Supprimer' class='expand' onclick='removeRow(\"formation_" + nbLignes + "\", \"nbLignesFormation\")'/>";
							break; 							
					}
				
					with(this.newCell.style)
					{	
			 			width = '100px';
			 			textAlign = 'center';
			 			padding = '5px';
					} 
					autoid++;
			 	}
			 	nbLignes++;
			 }
		  }
 		frmContent.getElementById("nbLignesFormation").value = nbLignes;
   }
}

