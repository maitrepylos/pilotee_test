function ShowEtablissement(closeCallBack)
{
	url = '<?php echo SCRIPTPATH . 'index.php?module=etablissementPopup'; ?>';
	var frm = document.getElementById('frmEtablissements');
	frm.src =url + closeCallBack;
	Popup.showModal('etablissements');
}

function SetEtablissements(objectsArray, close) 
{
	var ctrl = document.getElementById('etablissement');
	if (objectsArray.length > 0)
	{
		var selectedEts = document.getElementById("selectedEts");
		var selectedName = document.getElementById("selectedName");
		
		for (var i=0; i < objectsArray.length; i++)
		{
			key = select_search(objectsArray[i].id, ctrl);
			if(key == -1)
			{
				selectedEts.value += (selectedEts.value == '' ? '' : ';') + objectsArray[i].id;
				selectedName.value += (selectedName.value == '' ? '' : ';') + objectsArray[i].text; 
				ctrl.options[ctrl.length] = new Option(unescape(objectsArray[i].text), objectsArray[i].id);
			}
		}
	}
	if (close)
	{
		 document.getElementById('frmEtablissements').src = 'about:blank';
		 Popup.hide('etablissements');
	} 
}
