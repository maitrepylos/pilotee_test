<script type="text/javascript">
function ShowListFormateur(closeCallBack)
{
	var frm = document.getElementById("frmFormateurs");
	url = '<?php echo SCRIPTPATH . 'index.php?module=formateurPopup'; ?>';
	frm.src = url + closeCallBack;
	//alert(frm.src);
	// var test = document.getElementById("intitule");
	// test.value=frm.src;
	Popup.showModal('fomateurList');
}

function SetListFormateur(objectsArray, close)
{
	
	var ctrl = document.getElementById("assocFormateur");
	if (objectsArray.length > 0)
	{
		
		var selectedOps = document.getElementById("selectedFormateurs");
		var selectedOpsName = document.getElementById("selectedFormateursName");
		
		for (var i=0; i<objectsArray.length; i++)
		{
			key = select_search(objectsArray[i].id, ctrl);
			if(key==-1)
			{
				selectedOps.value += (selectedOps.value == "" ? "" : ";") + objectsArray[i].id;
				selectedOpsName.value += (selectedOpsName.value == "" ? "" : ";") + objectsArray[i].text; 
				ctrl.options[ctrl.length] = new Option(unescape(objectsArray[i].text), objectsArray[i].id);
			}
		}
	}
	
	if (close)
	{
		 document.getElementById("frmFormateurs").src = "about:blank";
		 Popup.hide('fomateurList');
	}
}


</script>
<div id="fomateurList" class="popup" style="width: 984px">
	<div class="popupPanel" style="width: 984px">
		<div class="popupPanelHeader">
			<table cellspacing="0" cellspacing="0" border="0"
				style="width: 974px">
				<tr>
					<td width="95%">Liste des formateurs</td>
					<td width="5%" align="right"><a href="javascript:void(0);"
						onclick="javascript:Popup.hide('fomateurList'); reloadFrameBlank('fomateurList');"><img
							class="popupClose" width="16" src="./styles/img/close_panel.gif"></img>
					</a></td>
				</tr>
			</table>
		</div>
		<div class="bd" style="width: 984px">
			<iframe id="frmFormateurs" frameborder=0 src=""
				style="width: 984px; height: 512px; border: 0px;"></iframe>
		</div>
	</div>
</div>