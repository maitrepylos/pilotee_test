<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class PopupGoogleMapsView extends BaseView
{
	private $user = null;
	
	public function Render($ets)
	{
		$this->user = Session::GetInstance()->getCurrentUser();
?>
<head>
	<script src="http://maps.google.com/maps?file=api&v=2&key=<?php echo GOOGLE_MAP_KEY; ?>" type="text/javascript"></script>
	
	<script language="javascript" type="text/javascript">
		var geocoder;
		var map;
	
		var points = new Array();
		var infos = new Array();
		var infosIdx = 0;

    	var baseIcon = new GIcon();
    	baseIcon.iconSize = new GSize(32,32);
    	baseIcon.shadowSize = new GSize(56,32);
    	baseIcon.iconAnchor = new GPoint(16,32);
    	baseIcon.infoWindowAnchor = new GPoint(16,0);

		var gris_gris = new GIcon(baseIcon, "./styles/img/map/ase.gris-gris.png", null, "./styles/img/map/ase.shadow.png");
		var gris_orange = new GIcon(baseIcon, "./styles/img/map/ase.gris-orange.png", null, "./styles/img/map/ase.shadow.png");
		var gris_vert = new GIcon(baseIcon, "./styles/img/map/ase.gris-vert.png", null, "./styles/img/map/ase.shadow.png");
		var orange_gris = new GIcon(baseIcon, "./styles/img/map/ase.orange-gris.png", null, "./styles/img/map/ase.shadow.png");
		var orange_orange = new GIcon(baseIcon, "./styles/img/map/ase.orange-orange.png", null, "./styles/img/map/ase.shadow.png");
		var orange_vert = new GIcon(baseIcon, "./styles/img/map/ase.orange-vert.png", null, "./styles/img/map/ase.shadow.png");
		var vert_gris = new GIcon(baseIcon, "./styles/img/map/ase.vert-gris.png", null, "./styles/img/map/ase.shadow.png");
		var vert_orange = new GIcon(baseIcon, "./styles/img/map/ase.vert-orange.png", null, "./styles/img/map/ase.shadow.png");
		var vert_vert = new GIcon(baseIcon, "./styles/img/map/ase.vert-vert.png", null, "./styles/img/map/ase.shadow.png");
		
		function initialize()
		{
			if (GBrowserIsCompatible())
			{
	        	geocoder = new GClientGeocoder();
	        	
	        	map = new GMap2(document.getElementById("map"));
	        	map.addControl(new GLargeMapControl());
	        	map.addControl(new GMapTypeControl());
				map.setCenter(new GLatLng(50.467, 4.867), 8);
	
		        <?php
		        	$fullInfo = null;
		        	$adresses = null;
		        	
					for ($i = 0; $i < $ets->count(); $i++)
					{
						$nom = '<b>' . $ets->items($i)->GetNom() . '</b>';
			
						$rue = $ets->items($i)->GetAdresse()->GetRue();
						
						$cpObj = $ets->items($i)->GetAdresse()->GetCodepostal();
						
						$cp = isset($cpObj) ? $cpObj->GetCodePostal() : null;
						$ville = $ets->items($i)->GetAdresse()->GetVille(); 
	
						$adresse = $rue;
						$adresse .= ((isset($adresse) && (strlen(trim($adresse)) > 0) && (isset($ville)) && (strlen(trim($ville)) > 0)) ? ',' : '') . $ville; 
						$adresse .= ((isset($adresse) && (strlen(trim($adresse)) > 0) && (isset($cp)) && (strlen(trim($cp)) > 0)) ? ',' : '') . $cp; 
			
						$adresseInfoP1 = $rue;
						$adresseInfoP2 = $cp;
						$adresseInfoP2 .= ((isset($adresseInfoP2) && (strlen(trim($adresseInfoP2)) > 0) && (isset($ville)) && (strlen(trim($ville)) > 0)) ? '&nbsp;' : '') . $ville; 
						
						$adresseInfo = $adresseInfoP1; 
						$adresseInfo .= ((isset($adresseInfo) && (strlen(trim($adresseInfo)) > 0) && (isset($adresseInfoP2)) && (strlen(trim($adresseInfoP2)) > 0)) ? '<br/>' : '') . $adresseInfoP2; 
			
						$email1 = $ets->items($i)->GetAdresse()->GetEmail1AsAnchor();
						$email2 = $ets->items($i)->GetAdresse()->GetEmail2AsAnchor();
						$emails = $email1 . ((isset($email1) && (strlen($email1) > 0) && isset($email2) && (strlen($email2) > 0)) ? ' / ' : '') . $email2;
			
						$tel1 = $ets->items($i)->GetAdresse()->GetTel1();
						$tel2 = $ets->items($i)->GetAdresse()->GetTel2();
						$tels = $tel1 . ((isset($tel1) && (strlen($tel1) > 0) && isset($tel2) && (strlen($tel2) > 0)) ? ' / ' : '') . $tel2;
			
						$reseau = $ets->items($i)->GetReseauEtablissement();
						$niveau = $ets->items($i)->GetNiveauEtablissement();
						$niveauReseau = $reseau . (((isset($reseau) && strlen($reseau) > 0) || (isset($niveau) && strlen($niveau) > 0)) ? ' - ' : '') . $niveau;
	
						$usr = $ets->items($i)->GetUtilisateur();
						
						$agent = 'Agent titulaire : ' . (isset($usr) ? $usr->getLabel() : '');
			
						$info = $nom . '<br/>';
						if (isset($adresseInfo) && strlen($adresseInfo) > 0) $info .= '<br/>' . $adresseInfo . '<br/>';
						if (isset($niveauReseau) && strlen($niveauReseau) > 0) $info .= '<br/>' . $niveauReseau . '<br/>';
						if (isset($emails) && strlen($emails) > 0) $info .= '<br/>' . $emails . '<br/>';
						if (isset($tels) && strlen($tels) > 0) $info .= '<br/>' . $tels . '<br/>';
						if (isset($agent) && strlen($agent) > 0) $info .= '<br/><hr style="width:20px;" />' . $agent;
						
						$lat = $ets->items($i)->GetAdresse()->getLat();
						$lng = $ets->items($i)->GetAdresse()->getLng();
						
						if (!isset($lat)) $lat = 999;
						if (!isset($lng)) $lng = 999;
						
						$activity = 'gris';
						
						$anneeId = isset($_REQUEST['anneeId']) ? $_REQUEST['anneeId'] : null;
						
						switch ($ets->items($i)->getActivity($anneeId))
						{
							case -1 :
								$activity = 'orange';
								break;
							case 0 :
								$activity = 'gris';
								break;
							case 1 :
								$activity = 'vert';
								break;
						}
							
						$visited = 'gris';
		
						switch ($ets->items($i)->getVisited($anneeId))
						{
							case -1 :
								$visited = 'orange';
								break;
							case 0 :
								$visited = 'gris';
								break;
							case 1 :
								$visited = 'vert';
								break;
						}
						
						echo sprintf('infos.push({nom:\'%s\',adresse:\'%s\',info:\'%s\',lat:%s,lng:%s,icon:%s});', addslashes($ets->items($i)->GetNom()), addslashes($adresse), addslashes($info), $lat, $lng, $activity . '_' . $visited);  
					}
		        ?>
	
		        afterInitialize();
	   		}
		}
	
		function afterInitialize()
		{
			var bounds = new GLatLngBounds();
			
			for (var i = 0; i < infos.length; i++)
			{
				if (infos[i].lat != 999 && infos[i].lng != 999)
				{
					var point = new GLatLng(infos[i].lat, infos[i].lng);
	
					bounds.extend(point);
					
					map.addOverlay(createMarker(point, infos[i].info, infos[i].icon, infos[i].nom));
				}
			}
			
			map.setCenter(bounds.getCenter(), map.getBoundsZoomLevel(bounds));

			<?php if (isset($_REQUEST['close'])) { ?>
			document.getElementById("TdClose").style.display = "";
			<?php }?>
		}

		function createMarker(point, html, icon, title)
		{
			var marker = new GMarker(point, {icon: icon, title: title});
	
	    	GEvent.addListener(marker, "click", function() { marker.openInfoWindowHtml(html); });
	
	    	return marker;
		}

		/*
		function getLatLong(response)
		{
			var delay = 0;
			
			if (!response || response.Status.code != 200)
			{
				if (response.Status.code == 602 || response.Status.code == 603)
				{
					alert('Impossible de géolocaliser l\'adresse : ' + infos[0].adresse);
					infosIdx++;
				}
				else if (response.Status.code == 620) delay = 500;
				else 
				{
					alert('Erreur inconnue lors de la géolocalisation de l\'adresse : ' + infos[0].adresse);
					infosIdx++;
				}
			}
	        else
			{
	          	var place = response.Placemark[0];
	
				infos[infosIdx].lat = place.Point.coordinates[1]; 
				infos[infosIdx].lng = place.Point.coordinates[0];

				infosIdx++;
	        }
	
	        setTimeout(geoCodeAll, delay);
		}
	
		function geoCodeAll() 
		{
			if (infos.length > 1) document.getElementById("statut").style.display = "";
	
			document.getElementById("statut").innerHTML = "Géolocalisation : " + (infosIdx + 1) + " / " + infos.length + " adresse" + (infosIdx > 1 ? "s" : "") + " traitée" + (infosIdx > 1 ? "s" : "");
			 
			if (infosIdx < infos.length) geocoder.getLocations(infos[infosIdx].adresse, getLatLong);
			else document.getElementById("statut").style.display = "none";
		}
		*/
	</script>
</head>

<body onload="initialize()" onunload="GUnload()" style="padding:0px;">
	<div id="statut" style="padding-bottom:5px;font-style:italic;font-weight:bold;display:none;"></div>
	
	<div>
	<center>
	 <table cellspacing="10">
	 <tr>
	 <td align="left" valign="top">		
		<i>Actions/Projets organisés dans les établissements d'enseignement :</i>
		<table>
		
		<tr><td><img src="./styles/img/map/legend.a.vert.png"/></td> <td>: dans l'année scolaire en cours</td></tr>
		<tr><td><img src="./styles/img/map/legend.a.orange.png"/></td> <td> : dans les années précédentes</td></tr>
		<tr><td><img src="./styles/img/map/legend.a.gris.png"/></td> <td> : aucune action</td></tr>
		</table>
	</td>
	<td align="left" valign="top">	
		<i>Suivi dans les établissements d'enseignement : </i>
		<table>
		<tr><td><img src="./styles/img/map/legend.s.vert.png"/></td> <td> : dans l'année scolaire en cours</td></tr>
		<tr><td><img src="./styles/img/map/legend.s.orange.png"/></td> <td>: dans les années précédentes</td></tr>
		<tr><td><img src="./styles/img/map/legend.s.gris.png"/></td> <td> : aucun suivi</td></tr>
		</table>
	</td>
	</tr>
	 </table>
	 </center>
	</div>	
	
	<div id="map" style="padding:0px;padding-top:12px;padding-bottom:12px; width:990px; height:650px"></div>
	<div class="boxBtn" style="padding-top:5px;padding-bottom:5px;text-align:center;">
		<table class="buttonTable" border="0" align="left">
			<tr>
				<td id="TdClose" style="display:none;" onclick="javascript:<?php echo 'parent.' . $_REQUEST['close'] .'();' ?>;">Fermer</td>
				
			</tr>
		</table>
		
	</div>
</body>
<?php
	}
}
?>