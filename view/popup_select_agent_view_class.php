<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class PopupSelectAgentView extends BaseView
{
	private $user = null;
	
	public function Render()
	{
		$this->user = Session::GetInstance()->getCurrentUser();		
?>
	<script language="javascript" type="text/javascript">
		function PopupSelectAgentClass() {};

		PopupSelectAgentClass.prototype.close = function(action)
		{
			var agentId = null;
			
			if (action == "select")
			{
				var agentSelect = document.getElementById("agent");
				agentId = agentSelect.options[agentSelect.selectedIndex].value;
			}
			
			<?php if (isset($_REQUEST['close'])) echo 'parent.' . $_REQUEST['close'] . '(agentId, action != "cancel");'; ?>			
		};

		window.psac = new PopupSelectAgentClass();
	</script> 

	<div style="padding-top:12px;padding-bottom:12px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td width="15%">Agent :</td>
			<td width="85%">
				<select id="agent" name="agent">
				<?php
					echo '<option value="-2">Pas attribu�</option>';
					echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT));
					echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT_COORDINATEUR));
				?>
				</select>
			</td>
		</tr>
		</table>
	</div>
	<div class="boxBtn" style="padding:5px;text-align:center;">
		<table class="buttonTable" border="0" align="center">
			<tr>
				<td onclick="javascript:window.psac.close('select');">Confirmer</td>
				<td class="separator"></td>
				<td onclick="javascript:window.psac.close('cancel');">Annuler</td>
			</tr>
		</table>
	</div>
<?php
	}
}
?>