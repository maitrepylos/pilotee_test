<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
class ReportActionsView extends BaseView
{
	private $user = null;

	public function Render()
	{
		$this->user = Session::GetInstance()->getCurrentUser();
?>
	<script>
	function checkTypeAction(action, checkbox)
	{
		var tr_categorie = document.getElementById('tr_categorie');
		var tr_operateur = document.getElementById('tr_operateur'); 
		
		if(action=='lab')
		{
			if(checkbox.checked)
			{
				tr_operateur.style.display = '';
			}
			else
			{
				tr_operateur.style.display = 'none';
				
			}
		}
		else if(action=='Nlab')
		{
			if(checkbox.checked)
			{
				tr_categorie.style.display='';	
			}
			else
			{
				tr_categorie.style.display='none';
			}
		}
	}

	function updateActionLabelise(anneeScolaire)
	{
		jx.load('./index.php?module=ajax&actionLabelise=ok&val=' + anneeScolaire, function(data)
			{
				var div = document.getElementById('operateurs_liste');
				div.innerHTML = '';

				var operateursCpt = 0;

				var tabElt = data.split("/#-#/");
				if (data != '')
				{
					for (var i = 0; i < tabElt.length; i++)
					{
						var tabOp = tabElt[i].split('||');
						var id = tabOp[0];
						var value = tabOp[1];
						div.innerHTML += '<input type="checkbox" name="operateur_' + i + '" value="' + id + '" />&nbsp;' + value + '&nbsp;&nbsp;<br />';
						operateursCpt++;
					}
				}
				div.innerHTML += '<input type="hidden" value="' + operateursCpt + '" name="nbOperateur" />';
			}, 'text'
		);
	}
	</script>
	<div class="boxGen">
		<form id="ReportAction" action="<?php echo $this->BuildURL($_REQUEST['module']).'&type=action' ?>" method="post">
			<div class="boxPan" style="padding: 5px;">
				<table cellpadding="0" cellspacing="0" border="0" width="85%">
				<colgroup>
					<col width="175"></col>
					<col width="10"></col>
					<col width="150"></col>
					<col width="45"></col>
					<col width="175"></col>
					<col width="10"></col>
					<col width="*"></col>
				</colgroup>
				<tr>
					<td height="25">Nom</td>
					<td>:</td>
					<td>
						<input size="40" maxlength="100" class="notauto" type="text" name="nom" id="nom" value="<?php if(isset($_POST["nom"])) echo ToHTML($_POST["nom"]) ?>" />
					</td>
					<td></td>
					<td height="25" colspan="6">
						<input type="checkbox"  <?php if (isset($_POST["actionLab"])) {echo 'checked="checked"';}?>  <?php if($this->user->isOperateur()){ echo 'disabled="disabled" checked="checked"'; }?>  name="actionLab" id="actionLab" class="notauto" onclick="checkTypeAction('lab',this)" value="1" > Actions labellis�es&nbsp;
						<?php if(!$this->user->isOperateur()){?> 
						<input type="checkbox" <?php if (isset($_POST["actionNonLab"])) echo 'checked="checked"';?> name="actionNonLab" id="actionNonLab" class="notauto" onclick="checkTypeAction('Nlab',this)" value="2"> Actions non labellis�es&nbsp;
					 	<input type="checkbox" <?php if (isset($_POST["actionMicro"])) echo 'checked="checked"';?> name="actionMicro" id="actionMicro" class="notauto" onclick="checkTypeAction('micro',this)" value="3"> Actions micro
					 	<?php }?> 
					</td>
					<td></td>
					<td></td>
				</tr>
				<?php
				$style=''; 
				if (!isset($_POST["actionNonLab"]) || ($this->user->isOperateur()))
					$style='style="display:none"';
				
				?>
				<tr id="tr_categorie" <?php echo $style;?>>
					<td height="25">Cat�gorie</td>
					<td>:</td>
					<td colspan="3">
						<?php
						$categorieList = Dictionnaries::getCategorieActionNonLabList();
						echo '<input type="hidden" value="'.$categorieList->count().'" name="nbCategorie" />';
						for($i=0; $i<$categorieList->count(); $i++)
						{
							$check = '';
							if (isset($_POST["categorie_".$i])) $check =  'checked="checked"';
								echo '<input '.$check.' type="checkbox" value="'.$categorieList->items($i)->GetCategActionNonLabelId().'" name="categorie_'.$i.'" /> '.$categorieList->items($i)->getLabel().'&nbsp;&nbsp;';
						}
						?>
					</td>
					<td></td><td></td><td></td><td></td>
				</tr>
				<?php
				$style=''; 
				 if (!isset($_POST["actionLab"]) || ($this->user->isOperateur()))
					 $style='style="display:none"';
				?>				
				
				<tr id="tr_operateur" <?php echo $style?>>
					<td height="25">Op�rateur</td>
					<td>:</td>
					<td colspan="2">
						<div style="width:225px;height:75px;overflow:auto;background-color:white;border:1px solid #EAC3C3;padding:2px;" id="operateurs_liste">
						<?php
							$operateurList = $this->operateurs; //Dictionnaries::getOperateurList();
							echo '<input type="hidden" value="'.$operateurList->count().'" name="nbOperateur" />';
							for($i=0; $i<$operateurList->count(); $i++)
							{
								$check = '';
								if (isset($_POST["operateur_".$i])) $check =  'checked="checked"';
								echo '<input '.$check.' type="checkbox" value="'.$operateurList->items($i)->getOperateurId().'" name="operateur_'.$i.'" /> '.$operateurList->items($i)->getNom().'&nbsp;&nbsp;';
								echo '<br/>';
							}
						?>
						</div>
					</td>
					<td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td height="25">Ann�e scolaire</td>
					<td>:</td>					
					<td>
						<select id="anneesScolaire" name="anneesScolaire" style="width: 230px;" onchange="updateActionLabelise(this.value)">
						<?php
						echo createSelectOptions(Dictionnaries::getAnneeList(), $_POST['anneesScolaire']);
						?>
						</select>
					</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td height="25">Entit� administrative</td>
					<td>:</td>
					<td>
						<input size="40" maxlength="100" class="notauto" type="text" name="entiteAdmin" id="entiteAdmin" value="<?php if(isset($_POST["entiteAdmin"])) echo ToHTML($_POST["entiteAdmin"]) ?>" />
					</td>
					<td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td height="25">�tablissement</td>
					<td>:</td>
					<td>
						<input size="40" maxlength="100" class="notauto" type="text" name="etablissement" id="etablissement" value="<?php if(isset($_POST["etablissement"])) echo ToHTML($_POST["etablissement"]) ?>" />
					</td>
					<td></td>
					<td>Actions globales : <input type="checkbox" value='1' <?php if (isset($_POST["global"])) echo 'checked="checked"';?> name="global" /></td>
					<td></td>
					<td></td>
				</tr>			
				<tr>
					<td>Code postal</td>
					<td>:</td>
					<td>
						<input type="text" maxlength="4" class ="notauto" size="10" name="code_postal_1" value="<?php if (isset($_POST["code_postal_1"])) echo ToHTML($_POST["code_postal_1"]) ?>"></input>
						&nbsp;�&nbsp;
						<input type="text" maxlength="4" class ="notauto" size="10" name="code_postal_2" value="<?php if (isset($_POST["code_postal_2"])) echo ToHTML($_POST["code_postal_2"]) ?>"></input>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td height="90" valign="top">Provinces</td>
					<td valign="top">:</td>
					<td  colspan="2">
						<div style="width:225px;height:75px;overflow:auto;background-color:white;border:1px solid #EAC3C3;padding:2px;">
						<?php 
							$provinces = Dictionnaries::getProvincesList();
							for ($i = 0; $i < $provinces->count(); $i++)
							{
								$check='';
								if(isset($_POST['chk_pro_'.$provinces->items($i)->getId()])) $check='checked="checked"';
								echo '<div style="vertical-align:bottom;"><input onclick = "updateDivCheckBox()" type="checkbox" '.$check.' name="chk_pro_' . $provinces->items($i)->getId() . '"></input>&nbsp;' . $provinces->items($i)->getLabel() . '</div>';
							}
						?>
						</div>
					</td>
					<td height="50" valign="top">Niveau</td>
					<td valign="top">:</td>
					<td  colspan="5">
						<div style="width:225px;height:75px;overflow:auto;background-color:white;border:1px solid #EAC3C3;padding:2px;">
						<?php 
							$niveaux = Dictionnaries::getNiveauxList();
							for ($i = 0; $i < $niveaux->count(); $i++)
							{
								$check='';
								if(isset($_POST['chk_niv_'.$niveaux->items($i)->getId()])) $check='checked="checked"';
								echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="chk_niv_' . $niveaux->items($i)->getId() . '"></input>&nbsp;' . $niveaux->items($i)->getLabel() . '</div>';
							}
						?>
						</div>
					</td>
				</tr>
				<tr>
					<td height="85" valign="top">Arrondissements</td>
					<td valign="top">:</td>
					<td  colspan="2">
						<div id="divArr" style="width:225px;height:75px;overflow:auto;background-color:white;border:1px solid #EAC3C3;padding:2px;">
						<?php 
							$arrondissements = Dictionnaries::getArrondissementList();
							for ($i = 0; $i < $arrondissements->count(); $i++)
							{
								$check='';
								if(isset($_POST['chk_arr_'.$arrondissements->items($i)->getId()])) $check='checked="checked"';
								echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="chk_arr_' . $arrondissements->items($i)->getId() . '"></input>&nbsp;' . $arrondissements->items($i)->getLabel() . '</div>';
							}
						?>
					</div>
		
					</td>
					<td height="85" valign="top">R�seau</td>
					<td valign="top">:</td>
					<td  colspan="5">
						<div style="width:225px;height:75px;overflow:auto;background-color:white;border:1px solid #EAC3C3;padding:2px;">
						<?php 
							$reseaux = Dictionnaries::getReseauxList();
							for ($i = 0; $i < $reseaux->count(); $i++)
							{
								$check='';
								if(isset($_POST['chk_res_'.$reseaux->items($i)->getId()])) $check='checked="checked"';
								echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="chk_res_' . $reseaux->items($i)->getId() . '"></input>&nbsp;' . $reseaux->items($i)->getLabel() . '</div>';
							}
						?>
						</div>
					</td>
				 </tr>
				</table>
				<input type="hidden" name="report_action"></input>
			</div>
			<div class="boxBtn" style="padding:5px;">
				<table class="buttonTable">
					<tr>
						<td onclick="submitForm('ReportAction')">Rechercher</td>
						<td class="separator"></td>
						<td onclick="clearForm('ReportAction')">Effacer</td>
					</tr>
				</table>
			</div>			
		</form>
	</div>
<?php }

	public function renderNoResult()
	{
?>
		<div id="DivNoResult" class="boxGen" style="border:0px;">
			<br/>
			<div class="boxPan" style="padding:5px;border:0px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
				<tr>
					<td style="padding-top:10px;padding-bottom:10px;padding-left:3px;">
						Il n'y a aucun r�sultat pour votre requ�te ...
					</td>
				</tr>
				</table>
			</div>
		</div>	
<?php 
	}

	public function renderList($obj)
	{
?>
		<script>
			function getexcel()
			{
				window.location.replace("<?php echo $this->BuildURL('exportXLS').'&type=action';?>");
			}
		</script>
		<div id="DivResult" class="boxGen" style="border:0px;">
			<div class="boxPan" style="padding:5px;border:0px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
					<tr>
						<td colspan="9" class="title" style="padding-top:10px;padding-bottom:10px;padding-left:3px;">Rapport sur les actions &nbsp; <img class="expand" src="./styles/img/csv.gif" title = "T�l�charger la version Excel" onclick="getexcel();"/></td>
					</tr>
				</table>
				<br/>
				<table cellpadding="0" cellspacing="0" border="0" width="700px">
					<colgroup>
						<col width="500"></col>
						<col width="100"></col>
						<col width="100"></col>
					</colgroup>
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Chiffres globaux</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Total</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Moyenne</b></td>
					</tr>
					<tr>
						<td style="height:25px;">Nombre d'actions prises en compte</td><td><?php echo $obj->count();?></td><td></td>
					</tr>
					<tr>
						<td style="height:25px;">Nombre global d'apprenants participant</td><td><?php echo $obj->getNbApprenantsGlobal();?></td><td><?php echo round($obj->getNbApprenantsGlobal()/$obj->count(),2);?></td>
					</tr>									
					<tr>
						<td style="height:25px;">Nombre global d'enseignants participant</td><td><?php echo $obj->getNbEnseignantsGlobal();?></td><td><?php echo round($obj->getNbEnseignantsGlobal()/$obj->count(),2);?></td>
					</tr>
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>R�partition par niveau-degr�-fili�re</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
					</tr>
					<?php
						$fdn = Dictionnaries::getFiliereDegreNiveauList();
						for($i=0; $i<$fdn->Count(); $i++)
						{
							$array  = $obj->getArrayFiliere();
							
							echo '<tr>';
							echo '<td style="height:25px;">'.$fdn->items($i)->GetLabel().'</td>';
							echo '<td style="height:25px;">'.(isset($array[$fdn->items($i)->GetFiliereDegreNiveauId()])?$array[$fdn->items($i)->GetFiliereDegreNiveauId()]:'').'</td>';
							echo '<td style="height:25px;">'.(isset($array[$fdn->items($i)->GetFiliereDegreNiveauId()])?round($array[$fdn->items($i)->GetFiliereDegreNiveauId()]/$obj->count(),2):'').'</td>';
							echo '</tr>';
						}
					?>					
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>R�partition par section</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
					</tr>
					<?php
						$section = Dictionnaries::getSectionActionList();
						for($i=0; $i<$section->Count(); $i++)
						{	
							$array  = $obj->getArraySection();
							echo '<tr>';
							echo '<td style="height:25px;">'.$section->items($i)->GetLabel().'</td>';
							echo '<td style="height:25px;">'.(isset($array[$section->items($i)->GetSectionActionId()])?$array[$section->items($i)->GetSectionActionId()]:'').'</td>';
							echo '<td style="height:25px;">'.(isset($array[$section->items($i)->GetSectionActionId()])? round($array[$section->items($i)->GetSectionActionId()]/$obj->count(),2) : '').'</td>';
							echo '</tr>';
						}
					?>
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>R�partition par finalit� p�dagogique</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
					</tr>															
					<?php
						$peda = Dictionnaries::getFinalitePedagogList();
						for($i=0; $i<$peda->Count(); $i++)
						{
							$array = $obj->getArrayFinalite();
							echo '<tr>';
							echo '<td style="height:25px;">'.$peda->items($i)->GetLabel().'</td>';	
							echo '<td style="height:25px;">'.(isset($array[$peda->items($i)->GetFinalitePedagogId()])?$array[$peda->items($i)->GetFinalitePedagogId()]:'').'</td>';
							echo '<td style="height:25px;">'.(isset($array[$peda->items($i)->GetFinalitePedagogId()])? round($array[$peda->items($i)->GetFinalitePedagogId()]/$obj->count(),2) : '').'</td>';
							echo '</tr>';
						}
					?>																				
				</table>
			</div>
		</div>
<?php 	
	}

}?>