<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
REQUIRE_ONCE(SCRIPTPATH.'domain/dictionnaries_domain_class.php');

class ReportActiviteEncodageView extends BaseView
{
	private $user = null;
//Formulaire de recherche
function reportActivite()
{
	$this->user = Session::GetInstance()->getCurrentUser();
?>	
<script>
function valideFormEncodage(idForm)
{
	var anneeAnte = document.getElementById('annee_ante');
	var anneePost = document.getElementById('annee_post');

	var res1=true;
	if (isDate(anneeAnte.value)==false)
	{
		SetInputError(anneeAnte);
		res1=false;
	}
	else
	{
		SetInputValid(anneeAnte);
	}

	
	if (isDate(anneePost.value)==false)
	{
		SetInputError(anneePost);
		res1=false;
	}
	else
	{
		
		SetInputValid(anneePost);
	}

	if(res1)
	{

		boEcart = true;
		if(anneeAnte.value!='' && anneePost.value!='')
		{
			var post = anneePost.value.split('/'); 
			var ante = anneeAnte.value.split('/');
		
			var ecart = getEcartDate(new Date(post[2], post[1], post[0]),new Date(ante[2], ante[1], ante[0]));
			if(ecart>365)
			{
				boEcart = false;
				alert('L\'�cart de date ne peut pas �tre sup�rieur � une ann�e !');
			}
		}
	
		if(boEcart)
		{
			submitForm(idForm);
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}	
}

</script>

<div class="boxGen">
	<form id="SearchEncodage" action="<?php echo $this->BuildURL($_REQUEST['module']).'&type=activite'; ?>" method="post">
		<div class="boxPan" style="padding:5px;">	
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<colgroup>
				<col width="175" />
				<col width="10" />
				<col width="200"/>
				<col width="10" />
				<col width="175" />
				<col width="10" />
				<col width="*"/>
			</colgroup>
			<?php if (($this->user->isAdmin()) || ($this->user->isCoordinateur()|| ($this->user->isCoordinateur()))) { ?>
				<tr>
					<td>Agent</td>
					<td>:</td>
					<td>	
						<select name='agent' style="width:175px;">
						<?php
							echo '<option value="-1">Tous</option>';
							echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT), $_POST['agent']);
						?>
						</select>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr><td colspan="3" style="font-size:1px;height:5px;"></td></tr>
				<tr>
					<td>Op�rateur</td>
					<td>:</td>
					<td>
						<select name='operateur' style="width:175px;">
						<?php
							echo '<option value="-1">Tous</option>';
							echo createSelectOptions(Dictionnaries::getOperateurList(), $_POST['operateur']);
						?>
						</select>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			<?php } ?>
				<tr><td colspan="3" style="font-size:1px;height:5px;"></td></tr>
				<tr>
					<td>Dates d'encodage</td>
					<td>:</td>
					<td colspan="4">
						De <input size="20" class="notauto" type="text" name="annee_ante" maxlength="10" id="annee_ante" value="<?php if(isset($_POST["annee_ante"])) echo ToHTML($_POST["annee_ante"]) ?>" />
						<img src='./styles/img/calendar3.gif' id='date_ante_pic' class="expand" title='Calendrier' />
						<script type='text/javascript'>
						Calendar.setup({
	        				inputField     :    'annee_ante',     
        					ifFormat       :    '%d/%m/%Y',      
        					button         :    'date_ante_pic',  
        					align          :    'Tr'            
						});
						</script>
						�
						<input size="20" class="notauto" type="text" name="annee_post" id="annee_post" maxlength="10" value="<?php if(isset($_POST["annee_post"])) echo ToHTML($_POST["annee_post"]) ?>" />
						<img src='./styles/img/calendar3.gif' id='date_post_pic' class="expand" title='Calendrier' />)
						<script type='text/javascript'>
   						Calendar.setup({
	        				inputField     :    'annee_post',     
        					ifFormat       :    '%d/%m/%Y',      
        					button         :    'date_post_pic',  
        					align          :    'Tr'            
						});
						</script> 
						<img src="./styles/img/eraser.gif" onclick="document.getElementById('annee_ante').value='';document.getElementById('annee_post').value='';" class="expand" title="vider" />
					</td>
				</tr>
				<tr><td colspan="3" style="font-size:1px;height:5px;"></td></tr>
				<tr>
					<td>Types d'objets</td>
					<td>:</td>
					<td colspan="4">
						<input type="checkbox" <?php if (isset($_POST['contactCheck'])) echo 'checked="checked"'; ?> name="contactCheck" id="contactCheck" class="notauto" value='contact'>&nbsp;Contacts&nbsp;/&nbsp;
						<input type="checkbox" <?php if (isset($_POST['actionCheck'])) echo 'checked="checked"'; ?> name="actionCheck" id="actionCheck" class="notauto" value='action'>&nbsp;Actions&nbsp;/&nbsp;
						<input type="checkbox" <?php if (isset($_POST['suivisCheck'])) echo 'checked="checked"'; ?> name="suivisCheck" id="suivisCheck" class="notauto" value='suivis'>&nbsp;Suivis&nbsp;/&nbsp;
						<input type="checkbox" <?php if (isset($_POST['materielCheck'])) echo 'checked="checked"'; ?> name="materielCheck" id="materielCheck" class="notauto" value='materiel'>&nbsp;Mat�riels
					</td>	
					<td></td>
				</tr>
				<tr><td colspan="3" style="font-size:1px;height:5px;"></td></tr>
				<tr>
					<td>Types d'encodage</td>
					<td>:</td>
					<td colspan="4">
						<input type="checkbox" <?php if (isset($_POST['ajoutCheck'])) echo 'checked="checked"'; ?> name="ajoutCheck" id="ajoutCheck" class="notauto" value='ajout'>&nbsp;Ajo�ts&nbsp;/&nbsp;
						<input type="checkbox" <?php if (isset($_POST['updateCheck'])) echo 'checked="checked"'; ?> name="updateCheck" id="updateCheck" class="notauto" value='update'>&nbsp;Mises � jour&nbsp;/&nbsp;
						<input type="checkbox" <?php if (isset($_POST['deleteCheck'])) echo 'checked="checked"'; ?> name="deleteCheck" id="deleteCheck" class="notauto" value='delete'>&nbsp;Suppressions
					</td>	
					<td></td>
				</tr>
			</table>
		</div>
		<div class="boxBtn"><input type="hidden" name="searchEncodage" />
			<table class="buttonTable">
				<tr>
					<td onclick="valideFormEncodage('SearchEncodage')">Rechercher</td>
					<td class="separator"></td>
					<td onclick="clearForm('SearchEncodage')">Effacer</td>
				</tr>
			</table>
		</div>
	</form>
</div>
<?php 
}

//************************************
//R�sultats		
//************************************
public function ResultActivite($histo, $typeObjet, $typeEncodage)
{
?>
<script>
function getexcel()
{
	window.location.replace("<?php echo $this->BuildURL('exportXLS').'&type=encodage';?>");
}
</script>
<div id="DivResult" class="boxGen" style="border:0px;">
	<div class="boxPan" style="padding:5px;border:0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
			<tr>
				<td colspan="9" class="title" style="padding-top:10px;padding-bottom:10px;padding-left:3px;">Rapport d'activit� d'encodage &nbsp; <img class="expand" src="./styles/img/csv.gif" title = "T�l�charger la version Excel" onclick="getexcel();"/></td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
		<colgroup>
		<col width="110"></col>
		<col width="160"></col>
		<col width="*"></col>
		</colgroup>
			<tr>
				<td class="resultHeader">Type</td>
				<td class="resultHeader">Utilisateur</td>
				<?php
				//Construction du header du tableau 
				$boContact = false; $boAction = false; $boSuivi = false; $boMateriel = false;
				$boAjout = false; $boUpdate = false; $boDelete = false; 
				foreach($typeObjet as $obj)
				{
					$value = '';
					
					switch ($obj)
					{
	    				case 'contact' : $value = 'Contact'; $boContact = true; break;
	    				case 'action' : $value = 'Action'; $boAction = true; break;
	    				case 'suivis' : $value = 'Fiche suivi'; $boSuivi= true; break;
	    				case 'materiel' : $value = 'Mat�riel p�d.'; $boMateriel = true; break; 					
					}
					
					foreach($typeEncodage as $encode)
					{
						$val = '';
						switch ($encode)
						{
							case 'ajout' : $val = '+'; $boAjout = true; break;
							case 'update' : $val = '~'; $boUpdate = true; break;
							case 'delete' : $val = '-'; $boDelete=true; break;   
						}
						echo '<td align="center" class="resultHeader">'.$value.'<br/>'.$val.'</td>';	
					}
					
				}
				?>
			</tr>
			<?php
			$userId = 0; $lastUserId = 0;			
			$nbAjoutContact = 0; $nbAjoutAction = 0; $nbAjoutSuivis = 0; $nbAjoutMateriel = 0;
			$nbUpdateContact = 0; $nbUpdateAction = 0; $nbUpdateSuivis = 0; $nbUpdateMateriel = 0;
			$nbDeleteContact = 0; $nbDeleteAction = 0; $nbDeleteSuivis = 0; $nbDeleteMateriel = 0;
			$nbAjoutContactTot = 0; $nbAjoutActionTot = 0; $nbAjoutSuivisTot = 0; $nbAjoutMaterielTot = 0;
			$nbUpdateContactTot = 0; $nbUpdateActionTot = 0; $nbUpdateSuivisTot = 0; $nbUpdateMaterielTot = 0;
			$nbDeleteContactTot = 0; $nbDeleteActionTot = 0; $nbDeleteSuivisTot = 0; $nbDeleteMaterielTot = 0;
			
			for($i=0; $i<$histo->count();$i++)
			{
				if($histo->items($i)->GetUtilisateurId()!= $userId)
				{
					$nbAjoutContact = 0; $nbAjoutAction = 0; $nbAjoutSuivis = 0; $nbAjoutMateriel = 0;
					$nbUpdateContact = 0; $nbUpdateAction = 0; $nbUpdateSuivis = 0; $nbUpdateMateriel = 0;
					$nbDeleteContact = 0; $nbDeleteAction = 0; $nbDeleteSuivis = 0; $nbDeleteMateriel = 0;
					
					$lastUserId = $userId; 
					$userId = $histo->items($i)->GetUtilisateurId();
					
					//echo '<br/>'.$histo->items($i)->GetUtilisateurId().'<br/>';
					
					
					$user = $histo->items($i)->GetUtilisateur();
					//var_dump($user);
					echo '<tr class="trResult">';
					echo '<td class="resultLine">'.$user->getTypeUtilisateurLabel().'</td>';
					echo '<td class="resultLine">'.$user->getNom().' '.$user->getPrenom().'</td>';
				}

				if($boContact)
				{
					
					if($histo->items($i)->GetTypeObjetEncodeId()=='CONTACT')
					{

						if($boAjout)
						{
							if($histo->items($i)->GetTypeEncodageId()=='AJOUT')
							{
								$nbAjoutContact ++;
								$nbAjoutContactTot ++;
							}
						}
					
						if($boUpdate)
						{
							if($histo->items($i)->GetTypeEncodageId()=='MAJ')
							{
								$nbUpdateContact ++;
								$nbUpdateContactTot ++;
							}
						}
						
						if($boDelete)
						{
							if($histo->items($i)->GetTypeEncodageId()=='SUP')
							{
								$nbDeleteContact ++;
								$nbDeleteContactTot ++;
							}
						}
					}
				}

				if($boAction)
				{
					if($histo->items($i)->GetTypeObjetEncodeId()=='ACTION')
					{

						if($boAjout)
						{
							if($histo->items($i)->GetTypeEncodageId()=='AJOUT')
							{
								$nbAjoutAction ++;
								$nbAjoutActionTot ++;
							}
						}
					
						if($boUpdate)
						{
							if($histo->items($i)->GetTypeEncodageId()=='MAJ')
							{
								$nbUpdateAction ++;
								$nbUpdateActionTot ++;
							}
						}
						
						if($boDelete)
						{
							if($histo->items($i)->GetTypeEncodageId()=='SUP')
							{
								$nbDeleteAction ++;
								$nbDeleteActionTot ++;
							}
						}
					}
				}
				if($boSuivi)
				{
					if($histo->items($i)->GetTypeObjetEncodeId()=='FSUIVI')
					{

						if($boAjout)
						{
							if($histo->items($i)->GetTypeEncodageId()=='AJOUT')
							{
								$nbAjoutSuivis ++;
								$nbAjoutSuivisTot ++;
							}
						}
					
						if($boUpdate)
						{
							if($histo->items($i)->GetTypeEncodageId()=='MAJ')
							{
								$nbUpdateSuivis ++;
								$nbUpdateSuivisTot ++;
							}
						}
						
						if($boDelete)
						{
							if($histo->items($i)->GetTypeEncodageId()=='SUP')
							{
								$nbDeleteSuivis ++;
								$nbDeleteSuivisTot ++;
							}
						}
					}
				}				

				if($boMateriel)
				{
					
					if($histo->items($i)->GetTypeObjetEncodeId()=='MATPEDA')
					{

						if($boAjout)
						{
							if($histo->items($i)->GetTypeEncodageId()=='AJOUT')
							{
								$nbAjoutMateriel ++;
								$nbAjoutMaterielTot ++;
							}
						}
					
						if($boUpdate)
						{
							if($histo->items($i)->GetTypeEncodageId()=='MAJ')
							{
								$nbUpdateMateriel ++;
								$nbUpdateMaterielTot ++;
							}
						}
						
						if($boDelete)
						{
							if($histo->items($i)->GetTypeEncodageId()=='SUP')
							{
								$nbDeleteMateriel ++;
								$nbDeleteMaterielTot ++;
							}
						}
					}
				}				
				
				if($histo->items($i+1)==null || $histo->items($i+1)->GetUtilisateurId()!= $userId)
				{
					
					if($boContact)
					{
						if($boAjout)
							echo '<td class="resultLine" align="center">'.$nbAjoutContact.'</td>';
						if($boUpdate)
							echo '<td class="resultLine" align="center">'.$nbUpdateContact.'</td>';
						if($boDelete)
							echo '<td class="resultLine" align="center">'.$nbDeleteContact.'</td>';
					}
					if($boAction)
					{
						if($boAjout)
							echo '<td class="resultLine" align="center">'.$nbAjoutAction.'</td>';
						if($boUpdate)
							echo '<td class="resultLine" align="center">'.$nbUpdateAction.'</td>';
						if($boDelete)
							echo '<td class="resultLine" align="center">'.$nbDeleteAction.'</td>';
						
					}
					if($boSuivi)
					{
						if($boAjout)
							echo '<td class="resultLine" align="center">'.$nbAjoutSuivis.'</td>';
						if($boUpdate)
							echo '<td class="resultLine" align="center">'.$nbUpdateSuivis.'</td>';
						if($boDelete)
							echo '<td class="resultLine" align="center">'.$nbDeleteSuivis.'</td>';
					}
					if($boMateriel)
					{
						if($boAjout)
							echo '<td class="resultLine" align="center">'.$nbAjoutMateriel.'</td>';
						if($boUpdate)
							echo '<td class="resultLine" align="center">'.$nbUpdateMateriel.'</td>';
						if($boDelete)
							echo '<td class="resultLine" align="center">'.$nbDeleteMateriel.'</td>';
					}
					echo '<tr>';
				}
			}
			//Total
			echo '<tr><td>&nbsp;</td></tr>';
			echo '<tr> <td></td> <td><b>Total :</b></td>';
			if($boContact)
			{
				if($boAjout)
					echo '<td class="resultLine" align="center">'.$nbAjoutContactTot.'</td>';
				if($boUpdate)
					echo '<td class="resultLine" align="center">'.$nbUpdateContactTot.'</td>';
				if($boDelete)
					echo '<td class="resultLine" align="center">'.$nbDeleteContactTot.'</td>';
			}
			if($boAction)
			{
				if($boAjout)
					echo '<td class="resultLine" align="center">'.$nbAjoutActionTot.'</td>';
				if($boUpdate)
					echo '<td class="resultLine" align="center">'.$nbUpdateActionTot.'</td>';
				if($boDelete)
					echo '<td class="resultLine" align="center">'.$nbDeleteActionTot.'</td>';
					
			}
			if($boSuivi)
			{
				if($boAjout)
					echo '<td class="resultLine" align="center">'.$nbAjoutSuivisTot.'</td>';
				if($boUpdate)
					echo '<td class="resultLine" align="center">'.$nbUpdateSuivisTot.'</td>';
				if($boDelete)
					echo '<td class="resultLine" align="center">'.$nbDeleteSuivisTot.'</td>';
			}
			if($boMateriel)
			{
				if($boAjout)
					echo '<td class="resultLine" align="center">'.$nbAjoutMaterielTot.'</td>';
				if($boUpdate)
					echo '<td class="resultLine" align="center">'.$nbUpdateMaterielTot.'</td>';
				if($boDelete)
					echo '<td class="resultLine" align="center">'.$nbDeleteMaterielTot.'</td>';
			}
			?>					
		</table>
	</div>
</div>	
<?php 
}
	
}
?>