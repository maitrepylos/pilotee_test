<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
class ReportBoursesView extends BaseView
{
	private $user = null;

	public function render()
	{
		$this->user = Session::GetInstance()->getCurrentUser();
?>
		<div class="boxGen">
		<form id="ReportBourse" action="<?php echo $this->BuildURL($_REQUEST['module']).'&type=bourse' ?>" method="post">
			<div class="boxPan" style="padding: 5px;">
				<table cellpadding="0" cellspacing="0" border="0" width="85%">
				<colgroup>
					<col width="175"></col>
					<col width="10"></col>
					<col width="150"></col>
					<col width="45"></col>
					<col width="175"></col>
					<col width="10"></col>
					<col width="*"></col>
				</colgroup>
				<tr>
					<td height="25">Projet entrepreneurial</td>
					<td>:</td>
					<td><input size="40" maxlength="100" class ="notauto" type="text" name="projetInnovant" id = "projetInnovant" value="<?php if(isset($_POST["projetInnovant"])) echo ToHTML($_POST["projetInnovant"]) ?>"/></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>				
				<tr>
					<td height="25">Statut de subvention</td>
					<td>:</td>
					<td colspan="3">
						<input type="checkbox" <?php if (isset($_POST["sub_obtenu"])) echo 'checked="checked"';?> name="sub_obtenu" class="notauto" value="1"> Obtenu &nbsp;
					 	<input type="checkbox" <?php if (isset($_POST["sub_refus"])) echo 'checked="checked"';?> name="sub_refus" class="notauto" value="2"> Refus�&nbsp;
					 	<input type="checkbox" <?php if (isset($_POST["sub_eval"])) echo 'checked="checked"';?> name="sub_eval" class="notauto" value="3"> En analyse 
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>				
				</tr>
				<tr>
					<td height="25">Ann�e scolaire</td>
					<td>:</td>					
					<td>
						<select id="anneesScolaire" name="anneesScolaire" style="width: 230px;">
						<?php
						echo createSelectOptions(Dictionnaries::getAnneeList(), $_POST['anneesScolaire']);
						?>
						</select>
					</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td height="25">Entit� administrative</td>
					<td>:</td>
					<td>
						<input size="40" maxlength="100" class="notauto" type="text" name="entiteAdmin" id="entiteAdmin" value="<?php if(isset($_POST["entiteAdmin"])) echo ToHTML($_POST["entiteAdmin"]) ?>" />
					</td>
					<td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td height="25">�tablissement</td>
					<td>:</td>
					<td>
						<input size="40" maxlength="100" class="notauto" type="text" name="etablissement" id="etablissement" value="<?php if(isset($_POST["etablissement"])) echo ToHTML($_POST["etablissement"]) ?>" />
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>			
				<tr>
					<td>Code postal</td>
					<td>:</td>
					<td>
						<input type="text" maxlength="4" class ="notauto" size="10" name="code_postal_1" value="<?php if (isset($_POST["code_postal_1"])) echo ToHTML($_POST["code_postal_1"]) ?>"></input>
						&nbsp;�&nbsp;
						<input type="text" maxlength="4" class ="notauto" size="10" name="code_postal_2" value="<?php if (isset($_POST["code_postal_2"])) echo ToHTML($_POST["code_postal_2"]) ?>"></input>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td height="90" valign="top">Provinces</td>
					<td valign="top">:</td>
					<td  colspan="2">
						<div style="width:225px;height:75px;overflow:auto;background-color:white;border:1px solid #EAC3C3;padding:2px;">
						<?php 
							$provinces = Dictionnaries::getProvincesList();
							/*
							$check='';
							if(isset($_POST['Chk_pro_tous'])) $check='checked="checked"';
							echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="Chk_pro_tous"></input>&nbsp;Tous</div>';
							*/
							for ($i = 0; $i < $provinces->count(); $i++)
							{
								$check='';
								if(isset($_POST['Chk_pro_'.$provinces->items($i)->getId()])) $check='checked="checked"';
								echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' onclick = "updateDivCheckBox()" name="chk_pro_' . $provinces->items($i)->getId() . '"></input>&nbsp;' . $provinces->items($i)->getLabel() . '</div>';
							}
						?>
						</div>
					</td>
					<td height="50" valign="top">Niveau</td>
					<td valign="top">:</td>
					<td  colspan="5">
						<div style="width:225px;height:75px;overflow:auto;background-color:white;border:1px solid #EAC3C3;padding:2px;">
						<?php 
							$niveaux = Dictionnaries::getNiveauxList();
							/*
							$check='';
							if(isset($_POST['chk_niv_tous'])) $check='checked="checked"';
							echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="chk_niv_tous"></input>&nbsp;Tous</div>';
							*/
							for ($i = 0; $i < $niveaux->count(); $i++)
							{
								$check='';
								if(isset($_POST['chk_niv_'.$niveaux->items($i)->getId()])) $check='checked="checked"';
								echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="chk_niv_' . $niveaux->items($i)->getId() . '"></input>&nbsp;' . $niveaux->items($i)->getLabel() . '</div>';
							}
						?>
						</div>
					</td>
				</tr>
				<tr>
					<td height="85" valign="top">Arrondissements</td>
					<td valign="top">:</td>
					<td  colspan="2">
						<div id="divArr" style="width:225px;height:75px;overflow:auto;background-color:white;border:1px solid #EAC3C3;padding:2px;">
						<?php 
							$arrondissements = Dictionnaries::getArrondissementList();
							/*
							$check='';
							if(isset($_POST['chk_arr_tous'])) $check='checked="checked"';
							echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="chk_arr_tous"></input>&nbsp;Tous</div>';
							*/							
							for ($i = 0; $i < $arrondissements->count(); $i++)
							{
								$check='';
								if(isset($_POST['chk_arr_'.$arrondissements->items($i)->getId()])) $check='checked="checked"';
								echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="chk_arr_' . $arrondissements->items($i)->getId() . '"></input>&nbsp;' . $arrondissements->items($i)->getLabel() . '</div>';
							}
						?>
					</div>
		
					</td>
					<td height="85" valign="top">R�seau</td>
					<td valign="top">:</td>
					<td  colspan="5">
						<div style="width:225px;height:75px;overflow:auto;background-color:white;border:1px solid #EAC3C3;padding:2px;">
						<?php 
							$reseaux = Dictionnaries::getReseauxList();
							/*
							$check='';
							if(isset($_POST['chk_res_tous'])) $check='checked="checked"';
							echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="chk_res_tous"></input>&nbsp;Tous</div>';
							*/
							for ($i = 0; $i < $reseaux->count(); $i++)
							{
								$check='';
								if(isset($_POST['chk_res_'.$reseaux->items($i)->getId()])) $check='checked="checked"';
								echo '<div style="vertical-align:bottom;"><input type="checkbox" '.$check.' name="chk_res_' . $reseaux->items($i)->getId() . '"></input>&nbsp;' . $reseaux->items($i)->getLabel() . '</div>';
							}
						?>
						</div>
					</td>
				 </tr>
				</table>
				<input type="hidden" name="report_bourse"></input>
			</div>
			<div class="boxBtn" style="padding:5px;">
				<table class="buttonTable">
					<tr>
						<td onclick="submitForm('ReportBourse')">Rechercher</td>
						<td class="separator"></td>
						<td onclick="clearForm('ReportBourse')">Effacer</td>
					</tr>
				</table>
			</div>				
		</form>
		</div>
<?php 
	} 

	public function renderNoResult()
	{
?>
		<div id="DivNoResult" class="boxGen" style="border:0px;">
		<br/>
			<div class="boxPan" style="padding:5px;border:0px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
				<tr>
					<td style="padding-top:10px;padding-bottom:10px;padding-left:3px;">
						Il n'y a aucun r�sultat pour votre requ�te ...
					</td>
				</tr>
				</table>
			</div>
		</div>	
<?php 
	}
	
	public function renderList($obj)
	{
?>
		<script>
			function getexcel()
			{
				window.location.replace("<?php echo $this->BuildURL('exportXLS').'&type=bourse';?>");
			}
		</script>		
		<div id="DivResult" class="boxGen" style="border:0px;">
			<div class="boxPan" style="padding:5px;border:0px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
					<tr>
						<td colspan="9" class="title" style="padding-top:10px;padding-bottom:10px;padding-left:3px;">Rapport sur les projets entrepreneuriaux &nbsp; <img class="expand" src="./styles/img/csv.gif" title = "T�l�charger la version Excel" onclick="getexcel();"/></td>
					</tr>
				</table>
				<br/>
				<table cellpadding="0" cellspacing="0" border="0" width="700px">
					<colgroup>
						<col width="500"></col>
						<col width="100"></col>
						<col width="100"></col>
					</colgroup>
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Chiffres globaux</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Total</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Moyenne</b></td>
					</tr>
					<tr>
						<td style="height:25px;">Nombre de projets pris en compte</td><td><?php echo $obj->count();?></td><td></td>
					</tr>
					<tr>
						<td style="height:25px;">Nombre global d'apprenants participant</td><td><?php echo $obj->getNbApprenantsGlobal();?></td><td><?php echo round($obj->getNbApprenantsGlobal()/$obj->count(),2);?></td>
					</tr>									
					<tr>
						<td style="height:25px;">Nombre global d'enseignants participant</td><td><?php echo $obj->getNbEnseignantsGlobal();?></td><td><?php echo round($obj->getNbEnseignantsGlobal()/$obj->count(),2);?></td>
					</tr>
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>R�partition par niveau-degr�-fili�re</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
					</tr>
					<?php
						$fdn = Dictionnaries::getFiliereDegreNiveauList();
						for($i=0; $i<$fdn->Count(); $i++)
						{
							$array  = $obj->getArrayFiliere();
							
							echo '<tr>';
							echo '<td style="height:25px;">'.$fdn->items($i)->GetLabel().'</td>';
							echo '<td style="height:25px;">'.(isset($array[$fdn->items($i)->GetFiliereDegreNiveauId()])?$array[$fdn->items($i)->GetFiliereDegreNiveauId()]:'').'</td>';
							echo '<td style="height:25px;">'.(isset($array[$fdn->items($i)->GetFiliereDegreNiveauId()])?round($array[$fdn->items($i)->GetFiliereDegreNiveauId()]/$obj->count(),2):'').'</td>';
							echo '</tr>';
						}
					?>					
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>R�partition par section</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
					</tr>
					<?php
						$section = Dictionnaries::getSectionActionList();
						for($i=0; $i<$section->Count(); $i++)
						{	
							$array  = $obj->getArraySection();
							echo '<tr>';
							echo '<td style="height:25px;">'.$section->items($i)->GetLabel().'</td>';
							echo '<td style="height:25px;">'.(isset($array[$section->items($i)->GetSectionActionId()])?$array[$section->items($i)->GetSectionActionId()]:'').'</td>';
							echo '<td style="height:25px;">'.(isset($array[$section->items($i)->GetSectionActionId()])? round($array[$section->items($i)->GetSectionActionId()]/$obj->count(),2) : '').'</td>';
							echo '</tr>';
						}
					?>
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>R�partition par finalit� p�dagogique</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
					</tr>															
					<?php
						$peda = Dictionnaries::getFinalitePedagogList();
						for($i=0; $i<$peda->Count(); $i++)
						{
							$array = $obj->getArrayFinalite();
							echo '<tr>';
							echo '<td style="height:25px;">'.$peda->items($i)->GetLabel().'</td>';	
							echo '<td style="height:25px;">'.(isset($array[$peda->items($i)->GetFinalitePedagogId()])?$array[$peda->items($i)->GetFinalitePedagogId()]:'').'</td>';
							echo '<td style="height:25px;">'.(isset($array[$peda->items($i)->GetFinalitePedagogId()])? round($array[$peda->items($i)->GetFinalitePedagogId()]/$obj->count(),2) : '').'</td>';
							echo '</tr>';
						}
					?>																				
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Montants</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
						<td style="background-color: #969696; color : #fff; height:25px;"></td>
					</tr>
					<?php
							echo '<tr>';
							echo '<td style="height:25px;">Montant octroy�</td>';	
							echo '<td style="height:25px;">'.round($obj->getMontantGlobal(),2).'</td>';
							echo '<td style="height:25px;">'.round($obj->getMontantGlobal()/$obj->count(),2).'</td>';
							echo '</tr>';
							
							echo '<tr>';
							echo '<td style="height:25px;">Montant attribu� par apprenant</td>';	
							echo '<td style="height:25px;"></td>';
							if($obj->getNbApprenantsGlobal()!=0)
								echo '<td style="height:25px;">'.round($obj->getMontantGlobal()/$obj->getNbApprenantsGlobal(),2).'</td>';
							else
								echo '<td style="height:25px;">0</td>';
							echo '</tr>';							
					?>											
				</table>
			</div>
		</div>
		
<?php 		
	}
	
}
?> 