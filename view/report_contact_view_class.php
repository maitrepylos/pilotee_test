<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
class ReportContactView extends BaseView
{

	public function renderList($res)
	{
		
		//total
		$total_bxl = 0; $total_bwa = 0; $total_hai = 0; $total_lie = 0; $total_lux = 0; $total_nam = 0;
		
		//*****************************************************GENRE**********************************************************************
		//Masculin
		$masculin_bxl = 0; $masculin_bwa = 0; $masculin_hai = 0; $masculin_lie = 0; $masculin_lux = 0; $masculin_nam = 0;
		//f�minin
		$feminin_bxl = 0; $feminin_bwa = 0; $feminin_hai = 0; $feminin_lie = 0; $feminin_lux = 0; $feminin_nam = 0;
		//sans valeur
		$sv_genre_bxl = 0; $sv_genre_bwa = 0; $sv_genre_hai = 0; $sv_genre_lie = 0; $sv_genre_lux = 0; $sv_genre_nam = 0;
		//********************************************************************************************************************************
		
		//******************************************************AGE***********************************************************************
		//Entre 15 et 24
		$age1_bxl = 0; $age1_bwa = 0; $age1_hai = 0; $age1_lie = 0; $age1_lux = 0; $age1_nam = 0;
		//Entre 25 et 54
		$age2_bxl = 0; $age2_bwa = 0; $age2_hai = 0; $age2_lie = 0; $age2_lux = 0; $age2_nam = 0;
		//Entre 55 et 64		
		$age3_bxl = 0; $age3_bwa = 0; $age3_hai = 0; $age3_lie = 0; $age3_lux = 0; $age3_nam = 0;
		//64 et plus
		$age4_bxl = 0; $age4_bwa = 0; $age4_hai = 0; $age4_lie = 0; $age4_lux = 0; $age4_nam = 0;
		//Sans valeur
		$sv_age_bxl = 0; $sv_age_bwa = 0; $sv_age_hai = 0; $sv_age_lie = 0; $sv_age_lux = 0; $sv_age_nam = 0;
		//********************************************************************************************************************************		
		
		//******************************************************STATUT*********************************************************************
		//travailleur occup�
		$trav_bxl = 0; $trav_bwa = 0; $trav_hai = 0; $trav_lie = 0; $trav_lux = 0; $trav_nam = 0;
		//Etudiant
		$etud_bxl = 0; $etud_bwa = 0; $etud_hai = 0; $etud_lie = 0; $etud_lux = 0; $etud_nam = 0;
		//autre
		$autre_bxl = 0; $autre_bwa = 0; $autre_hai = 0; $autre_lie = 0; $autre_lux = 0; $autre_nam = 0;
		//Sans valeur
		$sv_statut_bxl = 0; $sv_statut_bwa = 0; $sv_statut_hai = 0; $sv_statut_lie = 0; $sv_statut_lux = 0; $sv_statut_nam = 0;
		//*********************************************************************************************************************************		
				
		for($i=0; $i<count($res['contact']); $i++)
		{
			//echo $res['contact'][$i]['dic_provinceId'].'<br/>';
			//echo $res['contact'][$i]['contactId'].'<br/>';
			switch($res['contact'][$i]['dic_provinceId'])
			{
				case 'BXL' :					
					$total_bxl++;
					if($res['contact'][$i]['genreId']=='M')$masculin_bxl ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_bxl++;
					else $sv_genre_bxl++;
					
					if($res['contact'][$i]['statutId']=='TRAV')$trav_bxl ++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_bxl++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_bxl++;
					else $sv_statut_bxl++;
					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_bxl ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_bxl++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_bxl++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_bxl++;
					else $sv_age_bxl++;
					
					break;
				case 'BWA' :					
					$total_bwa++;
					if($res['contact'][$i]['genreId']=='M')$masculin_bwa ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_bwa++;
					else $sv_genre_bwa++;	

					if($res['contact'][$i]['statutId']=='TRAV')$trav_bwa ++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_bwa++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_bwa++;
					else $sv_statut_bwa++;					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_bwa ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_bwa++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_bwa++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_bwa++;
					else $sv_age_bwa++;
					
					break;
				case 'HAI' :					
					$total_hai++;
					if($res['contact'][$i]['genreId']=='M')$masculin_hai ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_hai++;
					else $sv_genre_hai++;				

					if($res['contact'][$i]['statutId']=='TRAV')$trav_hai ++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_hai++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_hai++;
					else $sv_statut_hai++;					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_hai ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_hai++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_hai++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_hai++;
					else $sv_age_hai++;					
					
					break;
				case 'LIE' :					
					$total_lie++;
					if($res['contact'][$i]['genreId']=='M')$masculin_lie ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_lie++;
					else $sv_genre_lie++;		

					if($res['contact'][$i]['statutId']=='TRAV')$trav_lie++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_lie++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_lie++;
					else $sv_statut_lie++;					
					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_lie ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_lie++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_lie++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_lie++;
					else $sv_age_lie++;
					
					break;	
				case 'LUX' :					
					$total_lux++;
					if($res['contact'][$i]['genreId']=='M')$masculin_lux ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_lux++;
					else $sv_genre_lux++;	

					if($res['contact'][$i]['statutId']=='TRAV')$trav_lux++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_lux++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_lux++;
					else $sv_statut_lux++;					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_lux ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_lux++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_lux++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_lux++;
					else $sv_age_lux++;					
					
					break;
				case 'NAM' :					
					$total_nam++;
					if($res['contact'][$i]['genreId']=='M')$masculin_nam ++;						
					elseif($res['contact'][$i]['genreId']=='F')$feminin_nam++;
					else $sv_genre_nam++;	

					if($res['contact'][$i]['statutId']=='TRAV')$trav_nam++;						
					elseif($res['contact'][$i]['statutId']=='ETUD')$etud_nam++;
					elseif($res['contact'][$i]['statutId']=='AUTRE')$autre_nam++;
					else $sv_statut_nam++;					
					
					if($res['contact'][$i]['ageId']=='15-24')$age1_nam ++;						
					elseif($res['contact'][$i]['ageId']=='25-54')$age2_nam++;
					elseif($res['contact'][$i]['ageId']=='55-64')$age3_nam++;
					elseif($res['contact'][$i]['ageId']=='64-pl')$age4_nam++;
					else $sv_age_nam++;					
					
					break;
			}	
		}
		
		
		 
?>
		<script>
			function getexcel()
			{
				window.location.replace("<?php echo $this->BuildURL('exportXLS').'&type=contact';?>");
			}
		</script>
		<div id="DivResult" class="boxGen" style="border:0px;">
			<div class="boxPan" style="padding:5px;border:0px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
					<tr>
						<td colspan="9" class="title" style="padding-top:10px;padding-bottom:10px;padding-left:3px;">Rapport sur les qualifications des contacts &nbsp; <img class="expand" src="./styles/img/csv.gif" title = "T�l�charger la version Excel" onclick="getexcel();"/></td>
					</tr>
				</table>
				<br/>
				<table cellpadding="0" cellspacing="0" border="0" width="950px">
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Contacts r�pondant aux crit�res</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Total</b></td>
						<?php
							$provinces = Dictionnaries::getProvincesList(); 
							for($i=0; $i<$provinces->count(); $i++)
							{
								echo '<td style="background-color: #969696; color : #fff; height:25px;"><b>'.$provinces->items($i)->getLabel().'</b></td>';
							} 
						?>
					</tr>
					<tr>
						<td>Nombre de contacts pris en compte</td>
						<td><?php echo ($total_bxl + $total_bwa + $total_hai+$total_lie +$total_lux+$total_nam);?></td>
						<td><?php echo $total_bxl; ?></td>
						<td><?php echo $total_bwa; ?></td>
						<td><?php echo $total_hai;?></td>
						<td><?php echo $total_lie;?></td>
						<td><?php echo $total_lux;?></td>
						<td><?php echo $total_nam;?></td>
					</tr>
					<tr>
						<td style="background-color: #969696; color : #fff;">Genre</td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
					</tr>
					<tr>
						<td>Homme</td>
						<td><?php echo ($masculin_bxl + $masculin_bwa + $masculin_hai+$masculin_lie +$masculin_lux+$masculin_nam);?></td>
						<td><?php echo $masculin_bxl; ?></td>
						<td><?php echo $masculin_bwa; ?></td>
						<td><?php echo $masculin_hai;?></td>
						<td><?php echo $masculin_lie;?></td>
						<td><?php echo $masculin_lux;?></td>
						<td><?php echo $masculin_nam;?></td>
					</tr>
					<tr>
						<td>Femme</td>
						<td><?php echo ($feminin_bxl + $feminin_bwa + $feminin_hai+$feminin_lie +$feminin_lux+$feminin_nam);?></td>
						<td><?php echo $feminin_bxl; ?></td>
						<td><?php echo $feminin_bwa; ?></td>
						<td><?php echo $feminin_hai;?></td>
						<td><?php echo $feminin_lie;?></td>
						<td><?php echo $feminin_lux;?></td>
						<td><?php echo $feminin_nam;?></td>
					</tr>					
					<tr>
						<td>Sans valeur</td>
						<td><?php echo ($sv_genre_bxl + $sv_genre_bwa + $sv_genre_hai+$sv_genre_lie +$sv_genre_lux+$sv_genre_nam);?></td>
						<td><?php echo $sv_genre_bxl; ?></td>
						<td><?php echo $sv_genre_bwa; ?></td>
						<td><?php echo $sv_genre_hai;?></td>
						<td><?php echo $sv_genre_lie;?></td>
						<td><?php echo $sv_genre_lux;?></td>
						<td><?php echo $sv_genre_nam;?></td>
					</tr>
					<tr>
						<td style="background-color: #969696; color : #fff;">Age</td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
					</tr>
					<tr>
						<td>15 � 24 ans</td>
						<td><?php echo ($age1_bxl + $age1_bwa + $age1_hai+$age1_lie +$age1_lux+$age1_nam);?></td>
						<td><?php echo $age1_bxl; ?></td>
						<td><?php echo $age1_bwa; ?></td>
						<td><?php echo $age1_hai;?></td>
						<td><?php echo $age1_lie;?></td>
						<td><?php echo $age1_lux;?></td>
						<td><?php echo $age1_nam;?></td>
					</tr>
					<tr>
						<td>25 � 54 ans</td>
						<td><?php echo ($age2_bxl + $age2_bwa + $age2_hai+$age2_lie +$age2_lux+$age2_nam);?></td>
						<td><?php echo $age2_bxl; ?></td>
						<td><?php echo $age2_bwa; ?></td>
						<td><?php echo $age2_hai;?></td>
						<td><?php echo $age2_lie;?></td>
						<td><?php echo $age2_lux;?></td>
						<td><?php echo $age2_nam;?></td>
					</tr>		
					<tr>
						<td>55 � 64 ans</td>
						<td><?php echo ($age3_bxl + $age3_bwa + $age3_hai+$age3_lie +$age3_lux+$age3_nam);?></td>
						<td><?php echo $age3_bxl; ?></td>
						<td><?php echo $age3_bwa; ?></td>
						<td><?php echo $age3_hai;?></td>
						<td><?php echo $age3_lie;?></td>
						<td><?php echo $age3_lux;?></td>
						<td><?php echo $age3_nam;?></td>
					</tr>
					<tr>
						<td>64 ans et plus</td>
						<td><?php echo ($age4_bxl + $age4_bwa + $age4_hai+$age4_lie +$age4_lux+$age4_nam);?></td>
						<td><?php echo $age4_bxl; ?></td>
						<td><?php echo $age4_bwa; ?></td>
						<td><?php echo $age4_hai;?></td>
						<td><?php echo $age4_lie;?></td>
						<td><?php echo $age4_lux;?></td>
						<td><?php echo $age4_nam;?></td>
					</tr>	
						
					<tr>
						<td>Sans valeur</td>
						<td><?php echo ($sv_age_bxl + $sv_age_bwa + $sv_age_hai+$sv_age_lie +$sv_age_lux+$sv_age_nam);?></td>
						<td><?php echo $sv_age_bxl; ?></td>
						<td><?php echo $sv_age_bwa; ?></td>
						<td><?php echo $sv_age_hai;?></td>
						<td><?php echo $sv_age_lie;?></td>
						<td><?php echo $sv_age_lux;?></td>
						<td><?php echo $sv_age_nam;?></td>
					</tr>												
					<tr style="display:none">
						<td style="background-color: #969696; color : #fff;">Statut</td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
						<td style="background-color: #969696; color : #fff;"></td>
					</tr>
					<tr style="display:none">
						<td>Travailleur occup�</td>
						<td><?php echo ($trav_bxl + $trav_bwa + $trav_hai+$trav_lie +$trav_lux+$trav_nam);?></td>
						<td><?php echo $trav_bxl; ?></td>
						<td><?php echo $trav_bwa; ?></td>
						<td><?php echo $trav_hai;?></td>
						<td><?php echo $trav_lie;?></td>
						<td><?php echo $trav_lux;?></td>
						<td><?php echo $trav_nam;?></td>
					</tr>					
					<tr style="display:none">
						<td>Etudiant</td>
						<td><?php echo ($etud_bxl + $etud_bwa + $etud_hai+$etud_lie +$etud_lux+$etud_nam);?></td>
						<td><?php echo $etud_bxl; ?></td>
						<td><?php echo $etud_bwa; ?></td>
						<td><?php echo $etud_hai;?></td>
						<td><?php echo $etud_lie;?></td>
						<td><?php echo $etud_lux;?></td>
						<td><?php echo $etud_nam;?></td>
					</tr>						
					<tr style="display:none">
						<td>Autre</td>
						<td><?php echo ($autre_bxl + $autre_bwa + $autre_hai+$autre_lie +$autre_lux+$autre_nam);?></td>
						<td><?php echo $autre_bxl; ?></td>
						<td><?php echo $autre_bwa; ?></td>
						<td><?php echo $autre_hai;?></td>
						<td><?php echo $autre_lie;?></td>
						<td><?php echo $autre_lux;?></td>
						<td><?php echo $autre_nam;?></td>
					</tr>					
					<tr style="display:none">
						<td>Sans valeur</td>
						<td><?php echo ($sv_statut_bxl + $sv_statut_bwa + $sv_statut_hai+$sv_statut_lie +$sv_statut_lux+$sv_statut_nam);?></td>
						<td><?php echo $sv_statut_bxl; ?></td>
						<td><?php echo $sv_statut_bwa; ?></td>
						<td><?php echo $sv_statut_hai;?></td>
						<td><?php echo $sv_statut_lie;?></td>
						<td><?php echo $sv_statut_lux;?></td>
						<td><?php echo $sv_statut_nam;?></td>
					</tr>					
				</table>
			</div>
		</div>
		
<?php 		
	}
}