<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class ReportEtablissementsView extends BaseView
{
	private $user = null;

	public function render()
	{
		$this->user = Session::GetInstance()->getCurrentUser();
?>
<div class="boxGen" style="padding-right:5px;padding-top:10px;">
	<form id="SearchEtablissement" action="<?php echo $this->BuildURL($_REQUEST['module']); ?>" method="post">
		<div class="boxPan" style="padding:5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<colgroup>
				<col width="175" />
				<col width="10" />
				<col width="200"/>
				<col width="10" />
				<col width="175" />
				<col width="10" />
				<col width="*"/>
			</colgroup>
			<tr>
				<td>Nom de l'�tablissement</td>
				<td>:</td>
				<td><input type="text" maxlength="60" class ="notauto" size="60" name="nom_etablissement" value="<?php if(isset($_POST["nom_etablissement"])) echo ToHTML($_POST["nom_etablissement"]) ?>"></input></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
					<?php if (($this->user->isAdmin()) || ($this->user->isCoordinateur()) || ($this->user->isAgent())  || ($user->isAgentCoordinateur()) ) { ?>
					<input id="swActif" name="swActif" type="checkbox" <?php if (isset($_POST["swActif"])) echo 'checked="checked"'; ?>>&nbsp;Actif
					&nbsp;
					<input name="swInactif" type="checkbox" <?php if (isset($_POST["swInactif"])) echo 'checked="checked"'; ?>>&nbsp;Inactif
					<?php } ?>
				</td>
			</tr>
			<tr><td colspan="3" style="font-size:1px;height:2px;"></td></tr>
			<?php if (($this->user->isAdmin()) || ($this->user->isCoordinateur()) || ($this->user->isAgent())  || ($user->isAgentCoordinateur())) { ?>			
			<tr>
				<td>Agent</td>
				<td>:</td>
				<td>
					<select name='agent' style="width:175px;">
						<?php
							echo '<option value="-1">Tous</option>';
							
							if (isset($_POST['agent']))
							{
								if ($_POST['agent'] == -2)
								{
									echo '<option value="-2" selected="selected">Pas attribu�</option>';
									echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT));
								}
								else
								{
									echo '<option value="-2">Pas attribu�</option>';
									echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT), $_POST['agent']);
								}
							}
							else
							{
								echo '<option value="-2">Pas attribu�</option>';
								echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT));
							}
						?>
					</select>
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<?php } ?>
			
			
			</table>
		</div>
	</form>
</div>
<?php
	}
} 
?>