<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');

class ReportEtablissementsView extends BaseView
{
	private $user = null;

	public function Render()
	{
		$this->user = Session::GetInstance()->getCurrentUser();
		?>
<div class="boxGen" style="padding-right: 5px; padding-top: 10px;">
	<form id="ReportEtablissement"
		action="<?php echo $this->BuildURL($_REQUEST['module']) . '&type=' . $_REQUEST['type'];?>"
		method="post">
		<input type="hidden" id="SubmitAction" name="SubmitAction"
			value="none"></input>

		<div class="boxPan" style="padding: 5px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<colgroup>
					<col width="175" />
					<col width="10" />
					<col width="200" />
					<col width="10" />
					<col width="175" />
					<col width="10" />
					<col width="*" />
				</colgroup>
				<tr>
					<td>Nom de l'�tablissement</td>
					<td>:</td>
					<td><input type="text" maxlength="60" class="notauto" size="60"
						name="nom_etablissement"
						value="<?php if(isset($_POST["nom_etablissement"])) echo ToHTML($_POST["nom_etablissement"]) ?>"></input>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td><?php if (($this->user->isAdmin()) || ($this->user->isCoordinateur()) || ($this->user->isAgent())  || ($this->user->isAgentCoordinateur()) ) { ?>
						<input id="swActif" name="swActif" type="checkbox"
						<?php if (isset($_POST["swActif"])) echo 'checked="checked"'; ?>>&nbsp;Actif
						&nbsp; <input name="swInactif" type="checkbox"
						<?php if (isset($_POST["swInactif"])) echo 'checked="checked"'; ?>>&nbsp;Inactif
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 1px; height: 2px;"></td>
				</tr>
				<tr>
					<td>Entit� administrative</td>
					<td>:</td>
					<td><input type="text" maxlength="60" class="notauto" size="60"
						name="entite_administrative"
						value="<?php if(isset($_POST["entite_administrative"])) echo ToHTML($_POST["entite_administrative"]) ?>"></input>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 1px; height: 2px;"></td>
				</tr>
				<?php if (($this->user->isAdmin()) || ($this->user->isCoordinateur()) || ($this->user->isAgent())  || ($this->user->isAgentCoordinateur()) ) { ?>
				<tr>
					<td>Agent</td>
					<td>:</td>
					<td><select name='agent' style="width: 175px;">
							<?php
							echo '<option value="-1">Tous</option>';

							if (isset($_POST['agent']))
							{
								if ($_POST['agent'] == -2)
								{
									echo '<option value="-2" selected="selected">Pas attribu�</option>';
									echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT));
								}
								else
								{
									echo '<option value="-2">Pas attribu�</option>';
									echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT), $_POST['agent']);
								}
							}
							else
							{
								echo '<option value="-2">Pas attribu�</option>';
								if($this->user->isAgent())
									$_POST['agent'] = $this->user->getUtilisateurId();

								echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT, null), $_POST['agent']);
							}
							?>
					</select>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 1px; height: 2px;"></td>
				</tr>
				<?php } ?>
				<tr style="vertical-align: top;">
					<td>Provinces</td>
					<td>:</td>
					<td>
						<div
							style="width: 225px; height: 75px; overflow: auto; background-color: white; border: 1px solid #EAC3C3; padding: 2px;">
							<?php 
							$provinces = Dictionnaries::getProvincesList();

							for ($i = 0; $i < $provinces->count(); $i++)
								echo '<div style="vertical-align:bottom;"><input type="checkbox" onclick = "updateDivCheckBox()" name="chk_pro_' . $provinces->items($i)->getId() . '"' . (isset($_POST['chk_pro_' . $provinces->items($i)->getId()]) ? 'checked' : null) . '></input>&nbsp;' . $provinces->items($i)->getLabel() . '</div>';
							?>
						</div>
					</td>
					<td></td>
					<td>Niveaux</td>
					<td>:</td>
					<td>
						<div
							style="width: 225px; height: 75px; overflow: auto; background-color: white; border: 1px solid #EAC3C3; padding: 2px;">
							<?php 
							$niveaux = Dictionnaries::getNiveauxList();

							for ($i = 0; $i < $niveaux->count(); $i++)
								echo '<div style="vertical-align:bottom;"><input type="checkbox" name="chk_niv_' . $niveaux->items($i)->getId() . '"' . (isset($_POST['chk_niv_' . $niveaux->items($i)->getId()]) ? 'checked' : null) . '></input>&nbsp;' . $niveaux->items($i)->getLabel() . '</div>';
							?>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 1px; height: 5px;"></td>
				</tr>
				<tr style="vertical-align: top;">
					<td>Arrondissements</td>
					<td>:</td>
					<td>
						<div id="divArr"
							style="width: 225px; height: 75px; overflow: auto; background-color: white; border: 1px solid #EAC3C3; padding: 2px;">
							<?php 
							$arrondissements = Dictionnaries::getArrondissementList();

							for ($i = 0; $i < $arrondissements->count(); $i++)
								echo '<div style="vertical-align:bottom;"><input type="checkbox" name="chk_arr_' . $arrondissements->items($i)->getId() . '"' . (isset($_POST['chk_arr_' . $arrondissements->items($i)->getId()]) ? 'checked' : null) . '></input>&nbsp;' . $arrondissements->items($i)->getLabel() . '</div>';
							?>
						</div>
					</td>
					<td></td>
					<td>R�seaux</td>
					<td>:</td>
					<td>
						<div
							style="width: 225px; height: 75px; overflow: auto; background-color: white; border: 1px solid #EAC3C3; padding: 2px;">
							<?php 
							$reseaux = Dictionnaries::getReseauxList();

							for ($i = 0; $i < $reseaux->count(); $i++)
								echo '<div style="vertical-align:bottom;"><input type="checkbox" name="chk_res_' . $reseaux->items($i)->getId() . '"' . $reseaux->items($i)->getId() . '"' . (isset($_POST['chk_res_' . $reseaux->items($i)->getId()]) ? 'checked' : null) . '></input>&nbsp;' . $reseaux->items($i)->getLabel() . '</div>';
							?>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 1px; height: 5px;"></td>
				</tr>
				<tr style="vertical-align: top;">
					<td>Code postal</td>
					<td>:</td>
					<td><input type="text" maxlength="4" class="notauto" size="10"
						name="code_postal_1"
						value="<?php if (isset($_POST["code_postal_1"])) echo ToHTML($_POST["code_postal_1"]) ?>"></input>
						&nbsp;�&nbsp; <input type="text" maxlength="4" class="notauto"
						size="10" name="code_postal_2"
						value="<?php if (isset($_POST["code_postal_2"])) echo ToHTML($_POST["code_postal_2"]) ?>"></input>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 1px; height: 5px;"></td>
				</tr>
				<tr style="vertical-align: top;">
					<td>Calculer les statuts pour</td>
					<td>:</td>
					<td><select id="anneesScolaire" name="anneesScolaire"
						style="width: 175px;">
							<?php
							/* echo '<option value="-1"></option>'; */

							$annee = null;

							if (isset($_POST['anneesScolaire'])) $annee = $_POST['anneesScolaire'];
							else
							{
								$tmp = Dictionnaries::getAnneeEnCours();
									
								if (isset($tmp) && $tmp->Count() > 0) $annee = $tmp->items(0)->getId();
							}

							echo createSelectOptions(Dictionnaries::getAnneeList(), $annee);
							?>
					</select>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</form>
</div>

<div class="boxBtn" style="padding: 5px;">
	<table class="buttonTable">
		<tr>
			<td
				onclick="document.getElementById('SubmitAction').value='search';submitForm('ReportEtablissement')">Rechercher</td>
			<td class="separator"></td>
			<td onclick="clearForm('ReportEtablissement')">Effacer</td>
		</tr>
	</table>
</div>

<?php
	}

	public function noResult()
	{
		?>
<div id="DivNoResult" class="boxGen" style="border: 0px;">
	<br />
	<div class="boxPan" style="padding: 5px; border: 0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			class="result">
			<tr>
				<td
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 3px;">
					Il n'y a aucun r�sultat pour votre requ�te ...</td>
			</tr>
		</table>
	</div>
</div>
<?php
	}

	public function renderList($ets)
	{
		?>
<script language="javascript" type="text/javascript">
	function ReportEtablissementViewClass() 
	{
		this.current = 'Results';
		this.tabs = new Array('Results', 'Map');
	};

	ReportEtablissementViewClass.prototype.onMouseOver = function(liID)
	{
		document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
		document.getElementById("spn" + liID).style.color = "#FFFFFF";
		document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
	};

	ReportEtablissementViewClass.prototype.onMouseOut = function(liID)
	{
		if (this.current != liID)
		{
			document.getElementById("spn" + liID).style.backgroundPosition = "100% 0%";
			document.getElementById("spn" + liID).style.color = "#737373";
			document.getElementById("a" + liID).style.backgroundPosition = "0% 0%";
		}
	};

	ReportEtablissementViewClass.prototype.onLIClick = function(liID)
	{
		this.current = liID;
		
		for (var i = 0; i < this.tabs.length; i++)
		{
			if (document.getElementById("spn" + this.tabs[i]))
			{
				document.getElementById("spn" + this.tabs[i]).style.backgroundPosition = "100% 0%";
				document.getElementById("spn" + this.tabs[i]) .style.color = "#737373";
				document.getElementById("a" + this.tabs[i]).style.backgroundPosition = "0% 0%";
			}
		}

		document.getElementById("spn" + liID).style.backgroundPosition = "100% -250px";
		document.getElementById("spn" + liID).style.color = "#FFFFFF";
		document.getElementById("a" + liID).style.backgroundPosition = "0% -250px";
		
		var results = document.getElementById('Results');
		var map = document.getElementById('Map');
		
		if (liID == 'Results')
		{
			if (results) results.style.display = '';
			if (map) map.style.display = 'none';

			var frm = document.getElementById("frmGoogleMaps");
			frm.src = 'about:blank';
		}
		else if (liID == 'Map')
		{
			if (results) results.style.display = 'none';
			if (map) map.style.display = '';
			//v�rification pour �viter qu'on affiche une map avec plus de 1000 �tablissement 
			// sinon g�n�re 'erreur Request-URI Too Long' voir constante pour changer ce nombre
			var nbEtablissement = parseInt(document.getElementById('nbEtablissement').innerHTML);
			var maxEtablissement = parseInt(<?php echo GOOGLE_NB_MAX_MAP ?>);
			if (nbEtablissement > maxEtablissement)
				 alert('Votre requ�te contient trop d\'�tablissements (maximum ' + maxEtablissement + ' �tablissements)');
			else
				this.renderMap();
		}
	};

	ReportEtablissementViewClass.prototype.renderMap = function()
	{
		var frm = document.getElementById("frmGoogleMaps");
		frm.src = "<?php echo SCRIPTPATH . 'index.php?module=popup_google_maps&anneeId=' . $_POST['anneesScolaire'] ?>";
		frm.src += "&ids=" + document.getElementById("EtsIdList").value;
	};
	
	window.revc = new ReportEtablissementViewClass();
	
	function getexcel()
	{
		window.location.replace("<?php echo $this->BuildURL('exportXLS').'&type=etablissement';?>");
	}
	
	</script>

<div id="DivResult" class="boxGen" style="border: 0px;">
	<div class="boxPan" style="padding: 5px; border: 0px;">
		<table cellpadding="0" cellspacing="0" border="0" width="100%"
			class="result">
			<tr>
				<td colspan="9" class="title"
					style="padding-top: 10px; padding-bottom: 10px; padding-left: 3px;">Rapport
					sur les �tablissements scolaires &nbsp; <img class="expand"
					src="./styles/img/csv.gif" title="T�l�charger la version Excel"
					onclick="getexcel();" />
				</td>
			</tr>
		</table>

		<div class="header" style="border-bottom: 3px solid #EAC3C3;">
			<ul>
				<li OnMouseOver="javascript:window.revc.onMouseOver('Results');"
					OnMouseOut="javascript:window.revc.onMouseOut('Results');"><a
					id="aResults" href="javascript:void(0)"
					onclick="javascript:window.revc.onLIClick('Results');"><span
						id="spnResults" class="expand">Liste de r�sultats</span> </a></li>
				<li OnMouseOver="javascript:window.revc.onMouseOver('Map');"
					OnMouseOut="javascript:window.revc.onMouseOut('Map');"><a id="aMap"
					href="javascript:void(0)"
					onclick="javascript:window.revc.onLIClick('Map');"><span
						id="spnMap" class="expand">Cartographie</span> </a></li>
			</ul>
		</div>

		<div style="width: 100%;">
			<div id="Results" style="width: 100%;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					class="result">
					<?php ob_start(); ?>
					<tr>
						<td class="resultHeader">Etablissement</td>
						<td class="resultHeader">Code postal</td>
						<td class="resultHeader">Ville</td>
						<td class="resultHeader">Arrondissement</td>
						<td class="resultHeader">Province</td>
						<td class="resultHeader">R�seau</td>
						<td class="resultHeader">Niveau</td>
						<td class="resultHeader">Activit�</td>
						<td class="resultHeader">Visite</td>
						<td class="resultHeader">Agent</td>
					</tr>
					<?php
					$statutActiviteOK = 0;
					$statutActivitePasse = 0;
					$statutActiviteJamais = 0;
					$statutActiviteGlobal = 0;
					$statutVisiteOK = 0;
					$statutVisitePasse = 0;
					$statutVisiteJamais = 0;
					$statutVisiteGlobal = 0;

					$etsIds = null;

					for ($i = 0; $i < $ets->Count(); $i++)
					{
						$etsIds .= ($etsIds == null ? '' : ',') . $ets->items($i)->GetEtablissementId();

						echo '<tr>';
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->GetNom());
							
						$cp = $ets->items($i)->GetAdresse()->GetCodePostal();
							
						echo sprintf('<td class="resultLine">%s</td>', isset($cp) ? $cp->GetCodePostal() : '&nbsp;');
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->GetAdresse()->GetVille());
						echo sprintf('<td class="resultLine">%s</td>', isset($cp) ? $cp->GetArrondissement()->getLabel() : '&nbsp;');
						echo sprintf('<td class="resultLine">%s</td>', isset($cp) ? $cp->GetProvince()->getLabel() : '&nbsp;');
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->GetReseauEtablissement());
						echo sprintf('<td class="resultLine">%s</td>', $ets->items($i)->GetNiveauEtablissement());
							
						$activity = $ets->items($i)->getActivity(isset($_POST['anneesScolaire']) ? $_POST['anneesScolaire'] : null);

						switch ($activity)
						{
							case -1 :
								echo '<td class="resultLine"><span style="padding:2px;background-color:orange;display:block;width:95%;text-align:center;color:white;"><b>Avant</b></span></td>';
								$statutActivitePasse++;
								break;
							case 0 :
								echo '<td class="resultLine"><span style="padding:2px;background-color:gray;display:block;width:95%;text-align:center;color:white;"><b>Jamais</b></span></td>';
								$statutActiviteJamais++;
								break;
							case 1 :
								echo '<td class="resultLine"><span style="padding:2px;background-color:green;display:block;width:95%;text-align:center;color:white;"><b>OK</b></span></td>';
								$statutActiviteOK++;
								break;
						}
						$statutActiviteGlobal++;
							
						$visited = $ets->items($i)->getVisited(isset($_POST['anneesScolaire']) ? $_POST['anneesScolaire'] : null);

						switch ($visited)
						{
							case -1 :
								echo '<td class="resultLine"><span style="padding:2px;background-color:orange;display:block;width:95%;text-align:center;color:white;"><b>Avant</b></span></td>';
								$statutVisitePasse++;
								break;
							case 0 :
								echo '<td class="resultLine"><span style="padding:2px;background-color:gray;display:block;width:95%;text-align:center;color:white;"><b>Jamais</b></span></td>';
								$statutVisiteJamais++;
								break;
							case 1 :
								echo '<td class="resultLine"><span style="padding:2px;background-color:green;display:block;width:95%;text-align:center;color:white;"><b>OK</b></span></td>';
								$statutVisiteOK++;
								break;
						}
						$statutVisiteGlobal++;
							
						$agent = $ets->items($i)->GetUtilisateur();
						echo sprintf('<td class="resultLine">%s</td>', isset($agent) ? $agent->GetLabel() : '&nbsp;');
							
						echo '</tr>';
					}

					echo sprintf('<input type="hidden" value="%s" id="EtsIdList" />', $etsIds);
					$buffer = ob_get_clean();
					?>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="resultHeader" style="color: white; border: ">Total</td>
						<td id="nbEtablissement" class="resultHeader" style="color: white; border: "
							align="right"><?php echo $statutActiviteGlobal; ?></td>
						<td class="resultHeader" style="color: white; border: "
							align="right"><?php echo $statutVisiteGlobal; ?></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="resultHeader"
							style="background-color: green; color: white;">OK</td>
						<td class="resultHeader" align="right"
							style="background-color: green; color: white;"><?php echo $statutActiviteOK; ?>
						</td>
						<td class="resultHeader" align="right"
							style="background-color: green; color: white;"><?php echo $statutVisiteOK; ?>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="resultHeader"
							style="background-color: orange; color: white;">Avant</td>
						<td class="resultHeader" align="right"
							style="background-color: orange; color: white;"><?php echo $statutActivitePasse; ?>
						</td>
						<td class="resultHeader" align="right"
							style="background-color: orange; color: white;"><?php echo $statutVisitePasse; ?>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="resultHeader"
							style="background-color: gray; color: white;">Jamais</td>
						<td class="resultHeader" align="right"
							style="background-color: gray; color: white;"><?php echo $statutActiviteJamais; ?>
						</td>
						<td class="resultHeader" align="right"
							style="background-color: gray; color: white;"><?php echo $statutVisiteJamais; ?>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="10">&nbsp;</td>
					</tr>
					<?php echo $buffer; ?>
				</table>
			</div>

			<div id="Map" style="width: 100%; display: none;">
				<div id="googleMap">
					<div class="popupPanel" style="border: none;">
						<div class="bd" style="text-align: center;">
							<br /> <br />
							<iframe id="frmGoogleMaps" frameborder=0 src=""
								style="width: 1024px; height: 850px; border: 0px;"
								scrolling="auto"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
		document.getElementById("spnResults").style.backgroundPosition = "100% -250px";
		document.getElementById("spnResults").style.color = "#FFFFFF";
		document.getElementById("aResults").style.backgroundPosition = "0% -250px";
	</script>

<?php
	}
} 
?>
