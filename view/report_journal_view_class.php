<?php
REQUIRE_ONCE(SCRIPTPATH.'lib/base.view.class.php');
class ReportJournalView extends BaseView
{
	private $user = null;

	public function render()
	{
		$this->user = Session::GetInstance()->getCurrentUser();
?>
		<script>
		function submitF(idForm)
		{

			var anneeAnte = document.getElementById('annee_ante');
			var anneePost = document.getElementById('annee_post');
			var res1=true;
			if (isDate(anneeAnte.value)==false)
			{
				SetInputError(anneeAnte);
				res1=false;
			}
			else SetInputValid(anneeAnte);
			
			if (isDate(anneePost.value)==false)
			{
				SetInputError(anneePost);
				res1=false;
			}
			else SetInputValid(anneePost);
			
			if(res1)
			{
				boEcart = true;
				if(anneeAnte.value!='' && anneePost.value!='')
				{
					var post = anneePost.value.split('/'); 
					var ante = anneeAnte.value.split('/');

					var ecart = getEcartDate(new Date(post[2], post[1], post[0]),new Date(ante[2], ante[1], ante[0]));
					if(ecart>365)
					{
						boEcart = false;
						alert('L\'�cart de date ne peut pas �tre sup�rieur � une ann�e !');
					}
				}
				if(boEcart) submitForm(idForm);
				else return false;
			}
		}
		
		</script>
		<div class="boxGen">
		<form id="ReportJournal" action="<?php echo $this->BuildURL($_REQUEST['module']).'&type=journal' ?>" method="post">
			<div class="boxPan" style="padding: 5px;">
				<table cellpadding="0" cellspacing="0" border="0" width="85%">
				<colgroup>
					<col width="175"></col>
					<col width="10"></col>
					<col width="150"></col>
					<col width="45"></col>
					<col width="175"></col>
					<col width="10"></col>
					<col width="*"></col>
				</colgroup>
				<?php if(!$this->user->isAgent()){?>
				<tr>
					<td height="25">Agent</td>
					<td>:</td>
					<td>
						<select id="agent" name="agent" style="width: 230px;">
						<?php
						echo createSelectOptions(Dictionnaries::getUtilisateursList(USER_TYPE_AGENT), $_POST['agent']);
						?>
						</select>						
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<?php }?>				
				<tr>
					<td height="25">Date de rencontre</td>
					<td>:</td>					
					<td colspan="4">
						De <input size="20" class="notauto" type="text" name="annee_ante" maxlength="10" id="annee_ante" value="<?php if(isset($_POST["annee_ante"])) echo ToHTML($_POST["annee_ante"]) ?>" />
						<img src='./styles/img/calendar3.gif' id='date_ante_pic' class="expand" title='Calendrier' />
						<script type='text/javascript'>
						Calendar.setup({
	        				inputField     :    'annee_ante',     
        					ifFormat       :    '%d/%m/%Y',      
        					button         :    'date_ante_pic',  
        					align          :    'Tr'            
						});
						</script>
						�
						<input size="20" class="notauto" type="text" name="annee_post" id="annee_post" maxlength="10" value="<?php if(isset($_POST["annee_post"])) echo ToHTML($_POST["annee_post"]) ?>" />
						<img src='./styles/img/calendar3.gif' id='date_post_pic' class="expand" title='Calendrier' />)
						<script type='text/javascript'>
   						Calendar.setup({
	        				inputField     :    'annee_post',     
        					ifFormat       :    '%d/%m/%Y',      
        					button         :    'date_post_pic',  
        					align          :    'Tr'            
						});
						</script> 
						<img src="./styles/img/eraser.gif" onclick="document.getElementById('annee_ante').value='';document.getElementById('annee_post').value='';" class="expand" title="vider" />
					</td>
					<td></td>
					<td></td>
				</tr>
				</table>
				<input type="hidden" name="report_journal"></input>
			</div>
			<div class="boxBtn" style="padding:5px;">
				<table class="buttonTable">
					<tr>
						<td onclick="submitF('ReportJournal')">Rechercher</td>
						<td class="separator"></td>
						<td onclick="clearForm('ReportJournal')">Effacer</td>
					</tr>
				</table>
			</div>				
		</form>
		</div>
<?php 
	} 

	public function renderNoResult()
	{
?>
		<div id="DivNoResult" class="boxGen" style="border:0px;">
		<br/>
			<div class="boxPan" style="padding:5px;border:0px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
				<tr>
					<td style="padding-top:10px;padding-bottom:10px;padding-left:3px;">
						Il n'y a aucun r�sultat pour votre requ�te ...
					</td>
				</tr>
				</table>
			</div>
		</div>	
<?php 
	}
	
	public function renderList($fiches)
	{
?>
		<script>
			function getexcel()
			{
				window.location.replace("<?php echo $this->BuildURL('exportXLS').'&type=journal';?>");
			}
		</script>		
		<div id="DivResult" class="boxGen" style="border:0px;">
			<div class="boxPan" style="padding:5px;border:0px;">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="result">
					<tr>
						<td colspan="9" class="title" style="padding-top:10px;padding-bottom:10px;padding-left:3px;">Rapport journal de classe &nbsp; <img class="expand" src="./styles/img/csv.gif" title = "T�l�charger la version Excel" onclick="getexcel();"/></td>
					</tr>
				</table>
				<br/>
				<table cellpadding="0" cellspacing="0" border="0" width="950px">
					<colgroup>
						<col width="150"></col>
						<col width="200"></col>
						<col width="150"></col>
						<col width="100"></col>
						<col width="150"></col>
						<col width="200"></col>						
					</colgroup>
					<tr>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Date de rencontre</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Etablissement</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Timing</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Modalit�</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Intention</b></td>
						<td style="background-color: #969696; color : #fff; height:25px;"><b>Contacts</b></td>
					</tr>
					<?php
					for($i=0; $i<$fiches->count();$i++)
					{  
						$fiche = $fiches->items($i);
						$etab = $fiche->GetEtablissement(); 
						$contacts = $fiche->GetContactList();
						echo '<tr>';
							echo '<td>'.$fiche->GetDateRencontre(true).'</td>';
							echo '<td>'.($etab ?$etab->GetNom():'').'</td>';
							echo '<td>'.($fiche->GetTimingSuivi()?$fiche->GetTimingSuivi()->getLabel():'').'</td>';
							echo '<td>'.($fiche->GetModeSuivi()?$fiche->GetModeSuivi()->getLabel():'').'</td>';
							echo '<td>'.($fiche->GetIntentionSuivi()?$fiche->GetIntentionSuivi()->getLabel():'').'</td>';
							echo '<td>';
							for($c=0;$c<$contacts->count();$c++)
							{
								echo $contacts->items($c)->GetNom().' '.$contacts->items($c)->GetPrenom().'<br/>';
							}
							echo'</td>';
						echo '</tr>';
					}
					?>
				</table>
			</div>
		</div>
		
<?php 		
	}
	
}
?> 